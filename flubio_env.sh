
export home_dir_flubio=/home/edo/flubio-code-fvm
export build_type_flubio=Release  # Release/Debug

export home_dir_thirdparty=$home_dir_flubio/thirdparty
export home_dir_scripts=$home_dir_flubio/scripts

export thirdparty_install_dir=${home_dir_thirdparty}/0---install
export njobs=4

# not currently used: 
#export build_type_thirdparty=Release  # Release/Debug

# Executables path
export PATH=$home_dir_flubio/0---install/bin:$PATH
export PATH=$home_dir_scripts/:$PATH
export PATH=$home_dir_scripts/0---install/bin:$PATH
export PATH=$home_dir_thirdparty/0---install/kahip/bin:$PATH
export PATH=$home_dir_thirdparty/0---install/metis/bin:$PATH

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$home_dir_thirdparty/0---install/coretran/lib
# optionally used in cmake/thirdparty/configure_*
#export hint_dir_boost=/home/edo/software_repo/boost_1_81_0/build
#export hint_dir_petsc=/home/edo/software_repo/petsc
#export hint_dir_mpi=/home/edo/software_repo/openmpi-4.1.4/build
