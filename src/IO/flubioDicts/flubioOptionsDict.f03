!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module options

!==================================================================================================================
! Description:
!! this module implements the dictionary storing the entries of settings/options and associated flags.
!==================================================================================================================

    use generalDictionaries

    implicit none

    !! Flag for steady state simulations
    integer :: steadySim

    type, extends(generalDict) :: optionsDict

        integer :: nonOrthMethod
        !! Flag for the non-orthogonal corretion option

        integer :: interpOpt
        !! Flag for the interpolation option

        integer :: gradOpt
        !! Flag for the gradient option, will set the defaults for all fields

        integer :: convOpt
        !! Flag for the convection option, will set the defaults for all the equations

        integer :: nCorr
        !! Number of non-orthogonal corrections

        integer :: git
        !! Number of skewness corrections for the Green-Gauss Gradient

        integer :: skwOpt
        !! Falg for the Skewness-Corrections

        integer :: forceOpt
        !! Flag for the type of momentum forcing

        integer :: gradLim
        !! Flag for the limited version of the gradients

        integer :: gradientFaceLimiter
        !! gradient limiter for tvd Green-Gauss gradient

        integer :: relaxVelocityCorrection
        !! relax the velocity correction with urf*gradPp

        integer :: corrected_interpolation
        !! Gradient interpolation flag

        integer :: kdtreeType
        !! kdtree option

        real :: kLim
        !! For the limited gradients, this varaible expresses the amount of limitation (0: not limited, 1:fully limited)

        real :: lambda
        !! Cross diffusion limiter

        real :: sfactor
        !! over-relaxed stabilisation factor

        integer :: numberOfMomCorr
        !! number of piso momentum corrections

        integer :: consistent
        !! flag for SIMPEC

        integer :: urfCorr
        !! flag for the under relaxation correction in the momentum interpolation

        integer :: volumeSources
        !! flag for volume sources

        character(len=50), dimension(:), allocatable :: volumeSourceNames
        !! volume sources names to be added in the equation rhs

    contains
        procedure :: setOptions

        procedure :: setTimeSchemeOption
        procedure :: setConvectionImplicit
        procedure :: setConvectionOption
        procedure :: setBoundedCorrection
        procedure :: setPrimeCorrections

    end type optionsDict

contains

    subroutine setOptions(this)

    !==================================================================================================================
    ! Description:
    !! setOptions fills the dictionary and ssociated flags.
    !==================================================================================================================

        class(optionsDict) :: this
    !   ------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName
        !! dictionary name

        character(len=:), allocatable :: optName
        !! option name

        character(len=:), allocatable :: opt
        !! option value

        character(len=:), dimension(:), allocatable :: acvec
    !------------------------------------------------------------------------------------------------------------------

        integer :: i

        integer, dimension(:), allocatable :: ilen
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        ! Set some defualts flag
        this%interpOpt = -1
        this%gradOpt = -1
        this%convOpt = -1

        ! Read dictionary
        dictName = 'settings/discretizationOptions'
        call this%initJSON(dictName)

        call this%json%get('Default%FaceInterpolation', opt, found)
        if(.not. found) opt = 'linear'

        if(lowercase(opt)=='linear') then
            this%interpOpt=0
        elseif(lowercase(opt)=='skewcorrected') then
            this%interpOpt=1
        elseif(lowercase(opt)=='harmonic') then
            this%interpOpt=2
        elseif(lowercase(opt)=='gradientbased') then
            this%interpOpt=3
        elseif(lowercase(opt)=='tvd') then
            this%interpOpt=4
        else
            if(id==0) then
                write(*,*) 'FLUBIO: unknown interpolation method: ', trim(opt)
                write(*,*) 'Available interpolation methods are:'
                write(*,*) 'harmonic'
                write(*,*) 'linear'
                write(*,*) 'gradientBased'
                write(*,*) 'tvd'
            end if
            call flubioStop()
        endif

        !-----------!
        ! Gradients !
        !-----------!

        call this%json%get('Default%gradient', opt, found)
        call raiseKeyErrorJSON('Default%gradient', found, dictName)

        if (lowercase(opt)=='greengauss') then
            this%gradOpt=0

        elseif (lowercase(opt)=='leastsquares') then
            this%gradOpt=1

        elseif(lowercase(opt)=='greengausstvd') then
            this%gradOpt=2

            ! Retrive the face limiter
            call this%json%get('Default%faceLimiter', opt, found)
            call raiseKeyErrorJSON('faceLimiter', found, dictName)
            call getConvectionOptionFromName(opt, this%gradientFaceLimiter)

        else
            if(id==0) then
                write(*,*) 'FLUBIO: unknown gradient method: ', trim(opt)
                write(*,*) 'Available gradient schemes are:'
                write(*,*) 'greenGauss'
                write(*,*) 'leastSquares'
                write(*,*) 'greenGaussTVD'
            end if
            call flubioStop()
        endif

        call this%json%get('Default%gradientLimiter', opt, found)
        if(.not. found) opt='none'

        if(lowercase(opt)=='celllimitedmd') then
            this%gradLim = 1
        elseif(lowercase(opt)=='facelimitedmd') then
            this%gradLim = 2
        elseif(lowercase(opt)=='celllimited') then
            this%gradLim = 3
        elseif(lowercase(opt)=='none' .or. opt=='no') then
            this%gradLim = 0
        else
            if(id==0) then
                write(*,*) 'FLUBIO: unknown gradient limiter: ', trim(opt)
                write(*,*) 'Available gradient limiters are:'
                write(*,*) 'cellLimitedMD'
                write(*,*) 'faceLimitedMD'
                write(*,*) 'cellLimited'
            end if
            call flubioStop()
        endif

        if(opt/='none') then
            call this%json%get('Default%clipping', this%kLim, found)
            call raiseKeyErrorJSON('Default%clipping', found,  dictName)
        end if

        !---------------------!
        ! Skweness correction !
        !---------------------!

        if (this%interpOpt==1) then

            call this%json%get('Default%skewCorrection', opt, found)
            call raiseKeyErrorJSON('Default%skewCorrection', found,  dictName)

            if(lowercase(opt)=='faceintersection') then
                ! Minimum distance from gemoetric face center
                this%skwOpt=0
            elseif(lowercase(opt)=='segmentcentered') then
                ! Segment centered
                this%skwOpt=1
            elseif(lowercase(opt)=='minimumdistance') then
                this%skwOpt=2
            else
                this%skwOpt=2
            endif

            call this%json%get('Default%numberOfSkewCorr', this%git, found)
            call raiseKeyErrorJSON('Default%numberOfSkewCorr', found, dictName)

        endif

        call this%json%get('NonOrthogonalCorr',  this%nCorr, found)
        call raiseKeyErrorJSON('NonOrthogonalCorr', found, dictName)

        !----------------!
        ! Diffusion term !
        !----------------!

        call this%json%get('Default%diffusionTerm', opt, found)
        call raiseKeyErrorJSON('diffusionTerm', found, dictName)

        if(lowercase(opt)=='overrelaxed') then
            this%nonOrthMethod = 0
        elseif(lowercase(opt)=='orthogonalcorrection') then
            this%nonOrthMethod = 1
        elseif(lowercase(opt)=='minimumcorrection') then
            this%nonOrthMethod = 2
        elseif(lowercase(opt)=='stabilisedoverrelaxed') then
            this%nonOrthMethod = 3

            ! Get the stabilsation factor, OF sets it at 0.05
            call this%json%get('Default%stabilisationFactor', this%sfactor, found)
            if(.not. found) this%sfactor = 0.05

        else
            if(id==0) then
                write(*,*) 'FLUBIO: unknown diffusion term ', trim(opt)
                write(*,*) 'Available diffusion term discretizations are:'
                write(*,*) 'overRelaxed'
                write(*,*) 'orthogonalCorrection'
                write(*,*) 'minimumCorrection'
                write(*,*) 'stabilisedOverRelaxed'
            end if
            call flubioStop()
        endif

        ! Corrected gradient interpolation
        call this%json%get('Default%gradientInterpolationCorrected', opt, found)
        if(.not. found) opt='yes'

        if(opt=='no' .or. opt=='off' .or. opt=='false') then
            this%corrected_interpolation = 0
        else
            this%corrected_interpolation = 1
        endif

        ! Cross diffuion limiter if any
        call this%json%get('Default%crossDiffusionLimiter', opt, found)
        if(.not. found) opt='none'

        if(opt=='none') then
            this%lambda = 1.0
        else
            call this%json%get('Default%crossDiffusionLimiter', this%lambda, found)
        endif

        !-----------------!
        ! Convective term !
        !-----------------!

        call this%json%get('Default%convectiveScheme', opt, found)
        if(.not. found) opt='none'
        call this%setConvectionOption(lowercase(opt), this%convOpt)

        ! Check if the simulation is steady
        call this%json%get('SteadyState', opt, found)
        if(.not. found) opt='none'

        if(opt=='yes' .or. opt=='on') then
            steadySim=1
         else
            steadySim=0
        end if

        ! Check if there are volume sources
        call this%json%get('VolumeSources%active', opt, found)
        if(.not. found) opt='no'

        if(lowercase(opt)=='yes' .or. lowercase(opt)=='on') then
            this%volumeSources = 1
        else
            this%volumeSources = 0
        end if

        if(this%volumeSources==1) then

            ! get the sources names
            call this%json%get('VolumeSources%sources', acvec, ilen, found)
            allocate(this%volumeSourceNames(size(acvec)))

            do i=1,size(acvec)
                this%volumeSourceNames(i) = adjustl(trim(acvec(i)))
            end do
        end if

        ! Check terms to add in te momentum interpolation
        call this%json%get('urfCorrection', opt, found)
        if(.not. found) opt='none'

        if(lowercase(opt)=='yes' .or. lowercase(opt)=='on') then
            this%urfCorr=1
        else
            this%urfCorr=0
        end if

        call this%json%get('RelaxVelocityCorrection', opt, found)
        if(.not. found) opt='none'

        if(lowercase(opt)=='yes' .or. lowercase(opt)=='on') then
            this%relaxVelocityCorrection=1
        else
            this%relaxVelocityCorrection=0
        end if

        ! Check kdtree option
        call this%json%get('KDtreeType',  opt, found)
        if (.not. found)  then
            this%kdtreeType = 0
        else
            if(lowercase(opt)=='coretran') then
                this%kdtreeType = 0
            elseif(lowercase(opt)=='mk2') then
                this%kdtreeType = 2
            endif
        endif

    end subroutine setOptions

!----------------------------------------------------------------------------------------------------------------------!
!                                               AUXILIARY SUBROUTINE                                                   !
!----------------------------------------------------------------------------------------------------------------------!

    subroutine setTimeSchemeOption(this, eqName, timeOptFlag, timeOpt)

    !==================================================================================================================
    ! Description:
    !! setTimeSchemeOption sets the time scheme option according to the value in the dictionary
    !==================================================================================================================

        class(optionsDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: key, timeOpt, timeOptDefault
    !------------------------------------------------------------------------------------------------------------------

        character(len = *) :: eqName
    !------------------------------------------------------------------------------------------------------------------

        integer :: timeOptFlag

        logical :: found, foundDefault
    !------------------------------------------------------------------------------------------------------------------

        call this%json%get('Default%timeScheme', timeOptDefault, foundDefault)
        call this%json%get(eqName//'%timeScheme', timeOpt, found)
        if(.not. found) timeOpt = 'none'

        if(lowercase(timeOpt)=='euler' .or. lowercase(timeOpt)=='implicitEuler') then
            timeOptFlag = 0
        elseif(lowercase(timeOpt)=='soue') then
            timeOptFlag = 1
        elseif(lowercase(timeOpt)=='cn') then
            timeOptFlag = 2
        elseif(lowercase(timeOpt)=='steady') then
            timeOptFlag = -1
            steadySim = 1

        ! Fractional step wildcards
        elseif(lowercase(timeOpt)=='rk3ssp') then
            timeOptFlag = -1
        elseif(lowercase(timeOpt)=='ark3') then
            timeOptFlag = -1

        ! Assign default value if present
        elseif(.not. found .and. foundDefault) then

            timeOpt = timeOptDefault
            if(lowercase(timeOptDefault)=='euler' .or. lowercase(timeOpt)=='implicitEuler') then
                timeOptFlag = 0
            elseif(lowercase(timeOptDefault)=='soue') then
                timeOptFlag = 1
            elseif(lowercase(timeOptDefault)=='cn') then
                timeOptFlag = 2
            elseif(lowercase(timeOptDefault)=='steady') then
                timeOptFlag = -1
                steadySim = 1

            ! Fractional step wildcards
            elseif(lowercase(timeOptDefault)=='rk3ssp') then
                timeOptFlag = -1
            elseif(lowercase(timeOptDefault)=='ark3') then
                timeOptFlag = -1

            else
                if(id==0) then
                    write(*,*) 'ERROR: unknown default time scheme ', trim(timeOpt)
                    write(*,*) 'Available time schemes are:'
                    write(*,*) 'euler'
                    write(*,*) 'cn'
                    write(*,*) 'soue'
                    write(*,*) 'steady'
                    write(*,*) 'For fractional step method:'
                    write(*,*) 'euler'
                    write(*,*) 'rk3ssp'
                    write(*,*) 'ark3'
                    write(*,*) 'soue'
                    write(*,*) 'implicitEuler'
                end if
                call flubioStop()
            end if

        else
            if(id==0) then
                write(*,*) 'ERROR: unknown time scheme ', trim(timeOpt)
                write(*,*) 'Available time schemes are:'
                write(*,*) 'euler'
                write(*,*) 'cn'
                write(*,*) 'soue'
                write(*,*) 'steady'
                write(*,*) 'For fractional step method:'
                write(*,*) 'euler'
                write(*,*) 'rk3ssp'
                write(*,*) 'ark3'
            end if
            call flubioStop()
        endif

    end subroutine setTimeSchemeOption

! *********************************************************************************************************************

    subroutine setConvectionImplicit(this, eqName, convImp)

    !===================================================================================================================
    ! Description:
    !! setConvectionImplicit sets the flag to active the fully implict thretment of the convective term
    !! according to the value in the dictionary
    !===================================================================================================================

        class(optionsDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: key, conv_implicit
    !------------------------------------------------------------------------------------------------------------------

        character(*) :: eqName
    !------------------------------------------------------------------------------------------------------------------

        integer :: convImp
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        call this%json%get(eqName//'%convectionImplicit', conv_implicit, found)
        if(.not. found) conv_implicit = 'none'

        if(conv_implicit=='yes' .or. conv_implicit=='on') then
            convImp = 1
        else
            convImp = 0
        endif

    end subroutine setConvectionImplicit

! *********************************************************************************************************************

    subroutine setConvectionOption(this, localConvOpt, convOptFlag)

    !===================================================================================================================
    ! Description:
    !! setConvectionOption sets convective scheme option according to the value in the dictionary.
    !===================================================================================================================

        class(optionsDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: key

        character(len=*) :: localConvOpt
    !------------------------------------------------------------------------------------------------------------------

        integer :: convOptFlag
    !------------------------------------------------------------------------------------------------------------------

        if (localConvOpt=='upwind') then
            convOptFlag = 0
        elseif (localConvOpt=='cd') then
            convOptFlag = 1
        elseif (localConvOpt=='sou') then
            convOptFlag = 2
        elseif (localConvOpt=='quick') then
            convOptFlag = 3
        elseif (localConvOpt=='fromm') then
            convOptFlag = 4
        elseif (localConvOpt=='downwind') then
            convOptFlag = 5
        elseif (localConvOpt=='vanleer') then
            convOptFlag = 6
        elseif (localConvOpt=='charm') then
            convOptFlag = 7
        elseif (localConvOpt=='hcus') then
            convOptFlag = 8
        elseif (localConvOpt=='hquick') then
            convOptFlag = 9
        elseif (localConvOpt=='koren') then
            convOptFlag = 10
        elseif(localConvOpt=='minmod') then
            convOptFlag = 11
        elseif(localConvOpt=='mc') then
            convOptFlag = 12
        elseif(localConvOpt=='osher') then
            convOptFlag = 13
        elseif(localConvOpt=='ospre') then
            convOptFlag = 14
        elseif(localConvOpt=='smart') then
            convOptFlag = 15
        elseif(localConvOpt=='sweeby') then
            convOptFlag = 16
        elseif(localConvOpt=='superbee') then
            convOptFlag = 17
        elseif(localConvOpt=='umist') then
            convOptFlag = 18
        elseif(localConvOpt=='vanalbada1') then
            convOptFlag = 19
        elseif(localConvOpt=='vanalbada2') then
            convOptFlag = 20
        elseif(localConvOpt=='osher-i') then
            convOptFlag = 21
        elseif(localConvOpt=='superbee-I') then
            convOptFlag = 22
        elseif(localConvOpt=='vanleer-I') then
            convOptFlag = 23
        elseif(localConvOpt=='stoic') then
            convOptFlag = 24
        elseif(localConvOpt=='m-stoic') then
            convOptFlag = 25
        elseif(localConvOpt=='m-superbee') then
            convOptFlag = 26
        elseif(localConvOpt=='stacs') then
            convOptFlag = 27
        elseif(localConvOpt=='hyperc') then
            convOptFlag = 28
        elseif(localConvOpt=='mhric') then
            convOptFlag = 29
        elseif(localConvOpt=='fbics') then
            convOptFlag = 30
        elseif(localConvOpt=='muscl') then
            convOptFlag = 31
        elseif(localConvOpt=='boundedcd') then
            convOptFlag = 32
        elseif(localConvOpt=='limitedlinear') then
            convOptFlag = 33
        elseif(localConvOpt=='venkatakrishnan') then
            convOptFlag = 34
        elseif(localConvOpt=='rounda') then
            convOptFlag = 35
        elseif(localConvOpt=='roundaplus') then
            convOptFlag = 36
        elseif(localConvOpt=='roundf') then
            convOptFlag = 37
        elseif(localConvOpt=='roundl') then
            convOptFlag = 38
        else
            convOptFlag = -1
        endif

    end subroutine setConvectionOption

! *********************************************************************************************************************

    subroutine getConvectionOptionFromName(localConvOpt, convOptFlag)

    !==================================================================================================================
    ! Description:
    !! getConvectionOptionFromName retrives the convective scheme option according to the input name.
    !==================================================================================================================

        character(len=*) :: localConvOpt
    !------------------------------------------------------------------------------------------------------------------

        integer :: convOptFlag
    !------------------------------------------------------------------------------------------------------------------

        if (localConvOpt=='upwind') then
            convOptFlag = 0
        elseif (localConvOpt=='cd') then
            convOptFlag = 1
        elseif (localConvOpt=='sou') then
            convOptFlag = 2
        elseif (localConvOpt=='quick') then
            convOptFlag = 3
        elseif (localConvOpt=='fromm') then
            convOptFlag = 4
        elseif (localConvOpt=='downwind') then
            convOptFlag = 5
        elseif (localConvOpt=='vanleer') then
            convOptFlag = 6
        elseif (localConvOpt=='charm') then
            convOptFlag = 7
        elseif (localConvOpt=='hcus') then
            convOptFlag = 8
        elseif (localConvOpt=='hquick') then
            convOptFlag = 9
        elseif (localConvOpt=='koren') then
            convOptFlag = 10
        elseif(localConvOpt=='minmod') then
            convOptFlag = 11
        elseif(localConvOpt=='mc') then
            convOptFlag = 12
        elseif(localConvOpt=='osher') then
            convOptFlag = 13
        elseif(localConvOpt=='ospre') then
            convOptFlag = 14
        elseif(localConvOpt=='smart') then
            convOptFlag = 15
        elseif(localConvOpt=='sweeby') then
            convOptFlag = 16
        elseif(localConvOpt=='superbee') then
            convOptFlag = 17
        elseif(localConvOpt=='umist') then
            convOptFlag = 18
        elseif(localConvOpt=='vanalbada1') then
            convOptFlag = 19
        elseif(localConvOpt=='vanalbada2') then
            convOptFlag = 20
        elseif(localConvOpt=='osher-i') then
            convOptFlag = 21
        elseif(localConvOpt=='superbee-i') then
            convOptFlag = 22
        elseif(localConvOpt=='vanleer-i') then
            convOptFlag = 23
        elseif(localConvOpt=='stoic') then
            convOptFlag = 24
        elseif(localConvOpt=='m-stoic') then
            convOptFlag = 25
        elseif(localConvOpt=='m-superbee') then
            convOptFlag = 26
        elseif(localConvOpt=='stacs') then
            convOptFlag = 27
        elseif(localConvOpt=='hyperc') then
            convOptFlag = 28
        elseif(localConvOpt=='mhric') then
            convOptFlag = 29
        elseif(localConvOpt=='fbics') then
            convOptFlag = 30
        elseif(localConvOpt=='muscl') then
            convOptFlag = 31
        elseif(localConvOpt=='boundedcd') then
            convOptFlag = 32
        elseif(localConvOpt=='limitedlinear') then
            convOptFlag = 33
        elseif(localConvOpt=='venkatakrishnan') then
            convOptFlag = 34
        elseif(localConvOpt=='rounda') then
            convOptFlag = 35
        elseif(localConvOpt=='roundaplus') then
            convOptFlag = 36
        elseif(localConvOpt=='roundf') then
            convOptFlag = 37
        elseif(localConvOpt=='roundl') then
            convOptFlag = 38
        else
            convOptFlag = -1
        endif

    end subroutine getConvectionOptionFromName

! *********************************************************************************************************************

    subroutine setBoundedCorrection(this, eqName, boundedCorr)

    !==================================================================================================================
    ! Description:
    !! setBoundedCorrection sets the flag to add the bounded correction according to the value in the dictionary
    !==================================================================================================================

        class(optionsDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: eqName
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: key, bounded
    !------------------------------------------------------------------------------------------------------------------

        integer :: boundedCorr
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        call this%json%get(eqName//'%boundedCorrection', bounded, found)
        if(.not.found) bounded = 'none'

        if(bounded=='yes' .or. bounded=='on') then
            boundedCorr = 1
        else
            boundedCorr = 0
        endif

    end subroutine setBoundedCorrection

! *********************************************************************************************************************

    subroutine setDevTerm(this, eqName, devTerm)

    !===================================================================================================================
    ! Description:
    !! setDevTerm sets the flag to add the deviatoric term according to the value in the dictionary.
    !===================================================================================================================

        class(optionsDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(*) :: eqName
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: key, dev
    !------------------------------------------------------------------------------------------------------------------

        integer :: devTerm
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        call this%json%get(eqName//'%deviatoricTerm', dev, found)
        if(.not.found) dev = 'none'

        if(dev=='yes' .or. dev=='on') then
            devTerm = 1
        else
            devTerm = 0
        endif

    end subroutine setDevTerm

! *********************************************************************************************************************

    subroutine setPrimeCorrections(this)

    !==================================================================================================================
    ! Description:
    !! setPrimeCorrections sets the number of prime correcions according to the value in the dictionary.
    !==================================================================================================================

        class(optionsDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: key
    !------------------------------------------------------------------------------------------------------------------

        integer :: primeCorr
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        call this%json%get('PrimeCorrections', primeCorr, found)
        call raiseKeyErrorJSON('PrimeCorrections', found, this%dictName)
        this%numberOfMomCorr = primeCorr

    end subroutine setPrimeCorrections

! *********************************************************************************************************************

    subroutine printConvectiveSchemes(eqName)

    !==================================================================================================================
    ! Description:
    !! printConvective prints to screen the available convective schemes.
    !==================================================================================================================

        character(*) eqName

        character(len=10), dimension(39) :: list
    !------------------------------------------------------------------------------------------------------------------

        integer :: opt, scheme
    !------------------------------------------------------------------------------------------------------------------

        if(id==0) then
            write(*,*) 'FLUBIO: unknown or unspecified convective scheme: ', eqName
            write(*,*) 'Available schemes are:'
        end if

        call flubioMsg('*** Gradient based schemes ***')

        list(1) = 'cd'
        list(2) = 'downwind'
        list(3) = 'froom'
        list(4) = 'quick'
        list(5) = 'sou'
        list(6) = 'upwind'

        if(id==0) then
            do scheme=1,6
                write(*,*) trim(list(scheme))
            end do
        end if

        call flubioMsg('*** TVD schemes ***')

        list(7) = 'vanleer'
        list(8) = 'charm'
        list(9) = 'hcus'
        list(10) = 'hquick'
        list(11) = 'koren'
        list(12) = 'minmod'
        list(13) = 'mc'
        list(14) = 'osher'
        list(15) = 'ospre'
        list(16) = 'smart'
        list(17) = 'sweeby'
        list(18) = 'superbee'
        list(19) = 'umist'
        list(20) = 'vanalbada1'
        list(21) = 'vanalbada2'
        list(22) = 'osher-i'
        list(23) = 'superbee-I'
        list(24) = 'vanleer-I'
        list(25) = 'stoic'
        list(26) = 'm-stoic'
        list(27) = 'm-superbee'
        list(28) = 'stacs'
        list(29) = 'hyperc'
        list(30) = 'mhric'
        list(31) = 'fbics'
        list(32) = 'muscl'
        list(33) = 'boundedcd'
        list(34) = 'limitedlinear'
        list(35) = 'venkatakrishnan'
        list(36) = 'rounda'
        list(37) = 'roundaplus'
        list(38) = 'roundf'
        list(39) = 'roundl'

        if(id==0) then
            do scheme=7,size(list)
                write(*,*) trim(list(scheme))
            end do
        end if

        call flubioStop()

    end subroutine printConvectiveSchemes

! *********************************************************************************************************************
end module options