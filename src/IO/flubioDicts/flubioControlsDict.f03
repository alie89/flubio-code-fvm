!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module controls

!===================================================================================================================
! Description:
!! This moudle implements the dictionary storing the entries of settings/controls.
!===================================================================================================================

    use generalDictionaries
    use globalTimeVar

    implicit none

    type, extends(generalDict) :: controlsDict

        integer :: itmax
        !! Maxium number of iteration in SIMPLE algorithm

        integer :: steadySim
        !! Flag for a stedy state simulation

        integer :: resetPC
        !! Recompute pressure-correction preconditioner every restPC time steps

        integer :: iflds
        !! Save a field every iflds time-steps

        integer :: nflds
        !! Number of fields to keep

        integer :: restFrom
        !! Restart a simulation from the restart file number restFrom

        integer :: convergence
        !! Flag to stop the simulation id convergece is reached

        integer :: saveResiduals
        !! Flag to save residuals in postProc/monitors

        integer :: resOutputFrequency
        !! Flag to save residuals in postProc/monitors

        integer :: verbosityLevel
        !! Flag to save residuals in postProc/monitors

        integer :: activateEnergyEquation
        !! flag to solve the energy equation

    contains
        procedure :: set => setControls

    end type controlsDict

contains

    subroutine setControls(this)

    !===================================================================================================================
    ! Description:
    !! setControls parses and stores the control parameters from settings/controls.
    !==================================================================================================================

        class(controlsDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName
        !! dictionary name

        character(len=:), allocatable :: optName
        !! option name

        character(len=:), allocatable :: val
        !! value
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        integer :: error_cnt
    !------------------------------------------------------------------------------------------------------------------

        dictName = 'settings/simulationControls'

        ! Initialise the JSON dictionary
        call this%initJSON(dictName)

        ! Check if the keyword exists and set its value
        call this%json%get('TimeStep', dtime, found)
        call raiseKeyErrorJSON('TimeStep', found, dictName)

        call this%json%get('AdjustTimeStep', val, found)
        if(found .and. val=='yes') dtAdj=1

        if(dtAdj==1) then
            call this%json%get('MaxCFL', cflmax, found)
            call raiseKeyErrorJSON('MaxCFL', found, dictName)
        endif

        call this%json%get('MaxiumTime', tmax, found)
        call raiseKeyErrorJSON('MaxiumTime', found, dictName)

        call this%json%get('FieldsToKeep', this%nflds, found)
        call raiseKeyErrorJSON('FieldsToKeep', found, dictName)

        call this%json%get('SaveEvery', this%iflds, found)
        call raiseKeyErrorJSON('SaveEvery', found, dictName)

        call this%json%get('RestartFrom', this%restFrom, found)
        call raiseKeyErrorJSON('RestartFrom', found, dictName)

        call this%json%get('SaveResiduals', val, found)
        if(.not. found) then
            this%saveResiduals = 1
        else
            if(lowercase(val)=='yes' .or. lowercase(val)=='on') then
                this%saveResiduals = 1
            else
                this%saveResiduals = 0
            end if
        end if

        call this%json%get('SaveResidualsEvery', this%resOutputFrequency, found)
        if(.not. found) this%resOutputFrequency = 1

        call this%json%get('Verbosity', val, found)
        if(lowercase(val)=='low')then
            this%verbosityLevel = 0
        elseif(lowercase(val)=='high') then
            this%verbosityLevel = 1
        else
            this%verbosityLevel = 1
        end if

        call this%json%get('SolveEnergyEqn', val, found)
        if(lowercase(val)=='yes' .or. lowercase(val)=='true' .or. lowercase(val)=='on') then
            this%activateEnergyEquation = 1
        else
            this%activateEnergyEquation = 0
        end if
    end subroutine setControls

end module controls