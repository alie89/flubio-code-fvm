!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module generalDictionaries

!==================================================================================================================
! Description:
!! This module implements the base class used to derive all dictionaries used in FLUBIO.
!==================================================================================================================

    use flubioMpi
    use qhashtbl_m
    use strings
    use m_strings
    use json_module

    use, intrinsic :: iso_fortran_env , only: error_unit

    implicit none

    type, public :: generalDict
        type(qhashtbl_t) :: dict
        type(json_file) :: json
        type(json_value), pointer :: jsonDict
        character(len=:), allocatable:: dictName
        logical :: hasKey
        logical :: dictFound
    contains
        procedure :: setDictName
        procedure :: getValFromKey
        procedure :: getFlagFromKey
        procedure :: getNumberFromKey
        procedure :: read => readDict
        procedure :: initJSON
        procedure :: parseSplitOption
    end type generalDict

contains

    subroutine setDictName(this, name)

    !==================================================================================================================
    ! Description:
    !! setDictName ets the name of the dictionary.
    !==================================================================================================================

        class(generalDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: name
        !! dictionary name

    !------------------------------------------------------------------------------------------------------------------

        this%dictName = name

    end subroutine setDictName

! *********************************************************************************************************************

    subroutine readDict(this)

    !==================================================================================================================
    ! Description:
    !! readDict reads a file and fills te dictionary.
    !==================================================================================================================

        class(generalDict) :: this

        character(50) :: key
        !! dictionary key

        character(50) :: val
        !! dictionary value
    !------------------------------------------------------------------------------------------------------------------

        integer :: ios
        !! error flag to mark the EOF
    !------------------------------------------------------------------------------------------------------------------

        open(1,file=trim(this%dictName))

        ios=0

        do
            call readline(1, val, ios)

            if(ios==0) then
                call split(val,' ', key)

                ! convert to lower case
                key = lowercase(key)
                val = lowercase(val)

                call this%dict%putstr(trim(key), trim(val))

            else
                exit
            endif

        enddo

        close(1)

    end subroutine readDict

!**********************************************************************************************************************

    subroutine getValFromKey(this, keyword, val)

    !==================================================================================================================
    ! Description:
    !! getValFromKey gets a value from the dictionary and returns it as a string.
    !==================================================================================================================

        class(GeneralDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: keyword
        !! dictionary keyword

        character(len=:), allocatable :: val
        !! dictionary value

    !------------------------------------------------------------------------------------------------------------------

        logical :: hasKey
    !------------------------------------------------------------------------------------------------------------------

        keyword = lowercase(keyword)

        call this%dict%getstr(keyword, val, hasKey)

        if(.not. hasKey) val = 'none'

        val = lowercase(val)

!       call raiseKeyError(hasKey, keyword)

    end subroutine getValFromKey

!**********************************************************************************************************************

    subroutine getNumberFromKey(this, keyword, number)

    !==================================================================================================================
    ! Description:
    !! getNumberFromKey gets a value from the dictionary and returns it as a real.
    !==================================================================================================================

        class(GeneralDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable:: keyword
        !! dictionary keyword

        character(len=:), allocatable:: val
        !! dictionary value
    !------------------------------------------------------------------------------------------------------------------

        logical :: hasKey
        !! Key checker
    !------------------------------------------------------------------------------------------------------------------

        real :: number
        !!  number to return
    !------------------------------------------------------------------------------------------------------------------

        keyword = lowercase(keyword)

        call this%dict%getstr(keyword, val, hasKey)
        call raiseKeyError(hasKey, keyword)

        read(val,*) number

    end subroutine getNumberFromKey

!**********************************************************************************************************************

    subroutine getFlagFromKey(this, keyword, flag)

    !==================================================================================================================
    ! Description: getFlagFromKey gets a value from the dictionary and returns it as a integer
    !==================================================================================================================

        class(GeneralDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable:: keyword
        !! dictionary keyword

        character(len=:), allocatable:: val
        !! dictionary value
    !------------------------------------------------------------------------------------------------------------------

        logical :: hasKey
        !! Key checker
    !------------------------------------------------------------------------------------------------------------------

        integer :: flag
        !!  integer to return

    !------------------------------------------------------------------------------------------------------------------

        keyword = lowercase(keyword)

        call this%dict%getstr(keyword, val, hasKey)
    !    call raiseKeyError(hasKey, keyword)

        if(hasKey) then
           read(val,*) flag
        else
            flag=0
        end if

    end subroutine getFlagFromKey

! *********************************************************************************************************************

    subroutine raiseKeyError(hasKey, key)

    !==================================================================================================================
    ! Description:
    !! raiseKeyError raises an error if the key is not in the dictionary.
    !==================================================================================================================

        character(len=*) :: key
        !! dictionary key
    !------------------------------------------------------------------------------------------------------------------

        logical :: hasKey
        !! key checker

        logical :: flag_false
        !! flag to stop
    !------------------------------------------------------------------------------------------------------------------

        flag_false = .false.

        if(.not. hasKey) then
            call flubioMsg('ERROR: Key "'//trim(key)//'" not found!')
            flag_false = .true.
        end if

        if(flag_false) call flubioStop()


    end subroutine raiseKeyError

! *********************************************************************************************************************

    subroutine raiseKeyErrorJSON(key, hasKey, location)

    !==================================================================================================================
    ! Description:
    !! raiseKeyError raises an error if the key is not in the dictionary.
    !==================================================================================================================

        character(len=*) :: key
        !! dictionary key

        character(len=*) :: location
        !! where is the error
    !------------------------------------------------------------------------------------------------------------------

        logical :: hasKey
        !! key checker

        logical :: flag_false
        !! flag to stop
    !------------------------------------------------------------------------------------------------------------------

        flag_false = .false.

        if(.not. hasKey) then
            call flubioMSg('ERROR: Key "'//trim(key)//'" not found in '//location) 
            flag_false = .true.
        end if

        if(flag_false) call flubioStop()


    end subroutine raiseKeyErrorJSON

!**********************************************************************************************************************

    subroutine initJSON(this, dictName)

        class(GeneralDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(*) dictName
    !------------------------------------------------------------------------------------------------------------------

        integer error_cnt
    !------------------------------------------------------------------------------------------------------------------

        error_cnt = 0
        call this%json%initialize()
        if (this%json%failed()) then
            call this%json%print_error_message(error_unit)
            error_cnt = error_cnt + 1
        end if

        ! Load JSON dictionary
        call this%json%load(filename = dictName)

        !if there was an error reading the file
        if (this%json%failed()) then
            call this%json%print_error_message(error_unit)
            error_cnt = error_cnt + 1
            call flubioStopMsg('ERROR: '// dictName //' has an uncorrect format.')
        endif

        ! use fortran-style paths
        call this%json%initialize(path_mode=1,path_separator=json_CK_'%')

    end subroutine initJSON

! ******************************************************************************************************************************

    subroutine parseSplitOption(this, optName, opt, val)

        class(GeneralDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: optName

        character(len=50), dimension(:), allocatable :: splitOption
        !! Split option for input like "name value"

        character(len=:), allocatable :: opt, stringVal
        !! option value
    !------------------------------------------------------------------------------------------------------------------

        real :: val
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        call this%json%get(optName, opt, found)
        call split_in_array(opt, splitOption,' ')
        if(.not. found) then
            opt = 'none'
        else
            opt = trim(splitOption(1))
            if(size(splitOption)> 1) then
                stringVal = splitOption(2)
                read(stringVal,*) val
            else
                val = 0.0
            end if
        end if

    end subroutine parseSplitOption

! ******************************************************************************************************************************

end module generalDictionaries