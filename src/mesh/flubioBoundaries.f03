!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module flubioBoundaries

!==================================================================================================================
! Description:
!! This module implements the data structure and methods for the mesh boundaries.
!==================================================================================================================

    use globalMeshVar
    use flubioMpi
    use generalDictionaries
    use strings
    use m_strings
    use nodes, only: skipHeader, findStreamSize, checkFormat

    implicit none

    type, public :: fvBoundaries
        
        type(generalDict) :: boundaryDict
        !! boundary dictionary

        integer :: numberOfBoundaries
        !! Number of boundaries in the domain
        
        integer :: numberOfPatches
        !! Number of boundaries in the domain
        
        integer :: numberOfRealBound
        !! Number of physical boundaries in the domain (preocessor boundaries are excluded)
            
        integer :: maxProcFaces
        !! Maxium number of faces in the processor boundaries. It is needed to  determin the buffer size during MPI communications
            
        integer :: maxWallFaces
        !! Maximum number of wall faces
        
        character(30), dimension(:), allocatable :: userName
        !! Boundary name provided by te user
        
        character(30), dimension(:), allocatable :: bcType
        !! Base type of boundary condition (wall, inlet, outlet)
        
        integer, dimension(:), allocatable :: startFace
        !! Location of the starting face in the faces array for a target patch
          
        integer, dimension(:), allocatable :: nFace
        !! Number of faces compising a target patch

        integer, dimension(:), allocatable :: realBound
        !! Array containing the position of the physical boundaries in the boundary file
        
        integer, dimension(:), allocatable :: procBound
        !! Array containing the position of the processor boundaries in the boundary file
        
        integer, dimension(:), allocatable :: myProc
        !! For a periodic boundary, myProc is the id of the rank hosting the patch

        integer, dimension(:), allocatable :: neighProc
        !! For a periodic boundary, neighProc is the id of the rank neighbouring with the patch

        integer, dimension(:), allocatable :: wallBound
        !! Array containing the position of the wall boundaries in the boundary file

        integer, dimension(:,:), allocatable :: ghostBoundaryMap
        !! Mapping array for the processor boundary.

        contains
            procedure :: readBoundAddr
            procedure :: readBoundAddrOFAscii
            procedure :: readBoundAddrOFBinary
            procedure :: readBoundAddrFlubioAscii
            procedure :: readBoundAddrFlubioBinary

            procedure :: readBoundariesFromFiles
            procedure :: readBoundariesFromFilesOFAscii
            procedure :: readBoundariesFromFilesFlubioAscii

            procedure :: setupProcessorBoundaries
            
            procedure :: findBoundaryIndices
            procedure :: findBoundaryByName
            procedure :: findWallBoundaries
            procedure :: getProcessorBoundaryMap
            procedure :: findNumberOfRealBoundaryFaces

    end type fvBoundaries

contains

    subroutine readBoundAddr(this)

    !==================================================================================================================
    ! Description:
    !! readBoundAddr reads the boundary address file, telling how the patches are decomposed across the processors.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10):: procID
        !! Processor id as string
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: fileFormat
        !! file format
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        call checkFormat('grid/processor'//trim(procID)//'/boundaryProcAddressing', fileFormat)

        if (fileFormat=='openfoam_ascii') then
            call this%readBoundAddrOFAscii()
        elseif(fileFormat=='openfoam_binary') then
            call this%readBoundAddrOFBinary()
        elseif(fileFormat=='flubio_ascii') then 
            call this%readBoundAddrFlubioAscii()
        elseif(fileFormat=='flubio_binary') then 
            call this%readBoundAddrFlubioBinary()
        else
            call flubioStopMsg('ERROR: cannot recognize file format for faces.')
        endif    

    end subroutine readBoundAddr

! **********************************************************************************************************************

    subroutine readBoundAddrOFAscii(this)

    !==================================================================================================================
    ! Description:
    !! readBoundAddr reads the boundary address file, telling how the patches are decomposed across the processors.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10):: procID

        character(len=80) :: line
        !! parsed line

        character(len=:), allocatable :: croppedLine
        !! cropped line

        character(len=:), allocatable :: oneLine
        !! cropped line

        character(len=50), allocatable :: carray(:)
        !! splitted line
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iFace, dummy, counter1, nFaces, ierr

        integer, dimension(:), allocatable :: dummyVec1
        !! auxiliary integer vector
    !------------------------------------------------------------------------------------------------------------------

        logical :: oneLineFlag
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        ! -------------------------------------------------------!
        ! Find out which boundaries are real boundaries          !
        ! (boudaries flagged with -1 are processors' boundaries) !
        ! -------------------------------------------------------!

        counter1 = 0

        open(1,file='grid/processor'//trim(procID)//'/boundaryProcAddressing')
              
            call skipHeader(aunit=1, fileFormat='ascii')
            call findStreamSize(aunit=1, fileFormat='ascii', streamSize=numberOfBoundaries, oneLine = oneLine)

            ! Allocate arrays
            allocate(dummyVec1(numberOfBoundaries))

            if (.not. allocated(oneLine)) then 
                oneLineFlag = .false.
            else
                oneLineFlag = matchw(oneLine, '*(*')
            endif   

            if(oneLineFlag) then

                call substitute(oneLine,'(',' ')
                call substitute(oneLine,')',' ')
                call split_in_array(oneLine, carray, ' ')
            
                do i=2,size(carray)
                    
                    call string_to_value(carray(i), dummy, ierr)
                    
                    if(dummy/=-1) then
                        counter1 = counter1+1
                    endif
                    dummyVec1(i-1) = dummy
                enddo
               
            else

                do iBoundary=1,numberOfBoundaries

                    call readLine(1, line, ierr)
                    croppedLine = crop(line)
    
                    call substitute(croppedLine,'(',' ')
                    call substitute(croppedLine,')',' ')
                    call split_in_array(croppedLine, carray, ' ')
                
                    call string_to_value(carray(1), dummy, ierr)
                    if(dummy/=-1) then
                        counter1 = counter1+1
                    endif
                    dummyVec1(iBoundary) = dummy
    
                enddo 

            endif        

        close(1)

        allocate(this%realBound(counter1))

        this%realBound = dummyVec1
        this%numberOfRealBound = counter1
        this%numberOfBoundaries = numberOfBoundaries
        numberOfRealBound = this%numberOfRealBound

    end subroutine readBoundAddrOFAscii

! **********************************************************************************************************************

    subroutine readBoundAddrOFBinary(this)

    !==================================================================================================================
    ! Description:
    !! readBoundAddr reads the boundary address file, telling how the patches are decomposed across the processors.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10):: procID

        character(len=:), allocatable :: oneLine
        !! cropped line
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iFace, dummy, counter1, nFaces, ierr

        integer, dimension(:), allocatable :: dummyVec1
        !! auxiliary integer vector
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        ! -------------------------------------------------------!
        ! Find out which boundaries are real boundaries          !
        ! (boudaries flagged with -1 are processors' boundaries) !
        ! -------------------------------------------------------!

        counter1 = 0

        open(1, file='grid/processor'//trim(procID)//'/boundaryProcAddressing', form = 'UNFORMATTED', access='STREAM')

            call skipHeader(aunit=1, fileFormat='binary')
            call findStreamSize(aunit=1, fileFormat='binary', streamSize=numberOfBoundaries, oneLine = oneLine)
        
            allocate(dummyVec1(numberOfBoundaries))

            do iBoundary=1,numberOfBoundaries

                read(1) dummy

                if(dummy/=-1) then
                    counter1 = counter1+1
                endif

                dummyVec1(iBoundary) = dummy
            enddo

        close(1)

        allocate(this%realBound(counter1))

        this%realBound = dummyVec1
        this%numberOfRealBound = counter1
        this%numberOfBoundaries = numberOfBoundaries

        numberOfRealBound = this%numberOfRealBound

        deallocate(dummyVec1)

    end subroutine readBoundAddrOFBinary

! **********************************************************************************************************************

    subroutine readBoundAddrFlubioAscii(this)

    !==================================================================================================================
    ! Description:
    !! readBoundAddr reads the boundary address file, telling how the patches are decomposed across the processors.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10):: procID
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iFace, dummy, counter1, nFaces

        integer, dimension(:), allocatable :: dummyVec1
        !! auxiliary integer vector
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        ! -------------------------------------------------------!
        ! Find out which boundaries are real boundaries          !
        ! (boudaries flagged with -1 are processors' boundaries) !
        ! -------------------------------------------------------!

        counter1 = 0

        open(1, file='grid/processor'//trim(procID)//'/boundaryProcAddressing')

            read(1,*) numberOfBoundaries

            allocate(dummyVec1(numberOfBoundaries))

            do iBoundary=1,numberOfBoundaries

                read(1,*) dummy

                if(dummy/=-1) then
                    counter1 = counter1+1
                endif

                dummyVec1(iBoundary) = dummy
            enddo

        close(1)

        allocate(this%realBound(counter1))

        this%realBound = dummyVec1
        this%numberOfRealBound = counter1
        this%numberOfBoundaries = numberOfBoundaries

        numberOfRealBound = this%numberOfRealBound

        deallocate(dummyVec1)

    end subroutine readBoundAddrFlubioAscii

! **********************************************************************************************************************

    subroutine readBoundAddrFlubioBinary(this)

    !==================================================================================================================
    ! Description:
    !! readBoundAddr reads the boundary address file, telling how the patches are decomposed across the processors.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10):: procID
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iFace, dummy, counter1, nFaces

        integer, dimension(:), allocatable :: dummyVec1
        !! auxiliary integer vector
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        ! -------------------------------------------------------!
        ! Find out which boundaries are real boundaries          !
        ! (boudaries flagged with -1 are processors' boundaries) !
        ! -------------------------------------------------------!

        counter1 = 0

        open(1, file='grid/processor'//trim(procID)//'/boundaryProcAddressing', form="unformatted")

            read(1) numberOfBoundaries

            allocate(dummyVec1(numberOfBoundaries))

            do iBoundary=1,numberOfBoundaries

                read(1) dummy

                if(dummy/=-1) then
                    counter1 = counter1+1
                endif

                dummyVec1(iBoundary) = dummy
            enddo

        close(1)

        allocate(this%realBound(counter1))

        this%realBound = dummyVec1
        this%numberOfRealBound = counter1
        this%numberOfBoundaries = numberOfBoundaries

        numberOfRealBound = this%numberOfRealBound

        deallocate(dummyVec1)

    end subroutine readBoundAddrFlubioBinary

! **********************************************************************************************************************

    subroutine readBoundariesFromFiles(this)

    !==================================================================================================================
    ! Description:
    !! readBoundaries reads the boundary mesh files and the boundary conditions.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10) :: procID
        !! processor ID

        character(len=:), allocatable :: fileFormat
        !! file format
    !------------------------------------------------------------------------------------------------------------------

        call readBoundAddr(this)

        write(procID,'(i0)') id

        call checkFormat('grid/processor'//trim(procID)//'/boundary', fileFormat)

        if (fileFormat=='openfoam_ascii') then
            call this%readBoundariesFromFilesOFAscii()
        elseif(fileFormat=='openfoam_binary') then
            call this%readBoundariesFromFilesOFAscii()
        elseif(fileFormat=='flubio_ascii') then 
            call this%readBoundariesFromFilesFlubioAscii()
        else
            call flubioStopMsg('ERROR: cannot recognize file format for faces.')
        endif   

        call this%findNumberOfRealBoundaryFaces()

    end subroutine readBoundariesFromFiles

! *********************************************************************************************************************

    subroutine readBoundariesFromFilesOFAscii(this)

    !==================================================================================================================
    ! Description:
    !! readBoundaries reads the boundary mesh files and the boundary conditions.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(json_value), pointer :: p

        type(json_core) :: json 
    !------------------------------------------------------------------------------------------------------------------

        character(10) :: procID
        !! processor ID

        character(len=100) :: line
        !! parsed line

        character(len=:), allocatable :: croppedLine
        !! cropped line

        character(len=:), allocatable :: oneLine
        !! cropped line

        character(len=30), allocatable :: carray(:)
        !! splitted line
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iProc

        integer :: ierr
        !! error flag

        integer :: nb 
        !! number of boundaries
    !------------------------------------------------------------------------------------------------------------------
        
        logical :: isWord
    !------------------------------------------------------------------------------------------------------------------

        write(procID, '(i0)' ) id

        call json%initialize()
        call json%create_object(this%boundaryDict%jsonDict, 'boundaryDict')

        numberOfPeriodicBound = 0
        totalPerBound = 0

        allocate(this%userName(numberOfBoundaries))
        allocate(this%bcType(numberOfBoundaries))
        allocate(this%startFace(numberOfBoundaries))
        allocate(this%nFace(numberOfBoundaries))
        allocate(this%myProc(numberOfBoundaries))
        allocate(this%neighProc(numberOfBoundaries))

        this%myProc = -1
        this%neighProc = -1

        open(1,file='grid/processor'//trim(procID)//'/boundary')

            call skipHeader(aunit=1, fileFormat='ascii')
            call findStreamSize(aunit=1, fileFormat='ascii', streamSize=nb, oneLine=oneLine)

            iBoundary = 0
            ! loop over the file to identify boundaries and keywords
            do
                call readLine(1, line, ierr)
                croppedLine = crop(line)
                call substitute(line,';','')   
                isWord = isalpha(crop(compact(line)))
                call split_in_array(line, carray, ' ')
                
                ! That's the patch name!
                if(isWord .and. size(carray) ==1) then

                    if(iBoundary>0) then
                        call json%add(this%boundaryDict%jsonDict, p) 
                        nullify(p)
                    endif

                    iBoundary = iBoundary+1
                    this%userName(iBoundary) = carray(1)
                    call json%create_object(p, crop(this%userName(iBoundary)))

                endif

                ! Find type
                if(size(carray)>1 .and. iBoundary>0) then

                    if(carray(1)=='type') this%bcType(iBoundary) = carray(2)
                    if(carray(1)=='nFaces') call string_to_value(carray(2), this%nFace(iBoundary), ierr)
                    if(carray(1)=='startFace') call string_to_value(carray(2), this%startFace(iBoundary), ierr)   
                    if(carray(1)=='myProcNo') call string_to_value(carray(2),  this%myProc(iBoundary), ierr) 
                    if(carray(1)=='neighProcNo') call string_to_value(carray(2), this%neighProc(iBoundary), ierr)

                    ! Populate boundary sub-dictionary
                    call json%add(p, crop(carray(1)), crop(carray(2)))

                endif    
            
                if(ierr /= 0) exit

            enddo   

        close(1)

        ! Set up cyclic boundaries, check if there are void patches, set maxium processor size for excahnge buffers
        call this%setupProcessorBoundaries()

    end subroutine readBoundariesFromFilesOFAscii

! **********************************************************************************************************************

    subroutine readBoundariesFromFilesFlubioAscii(this)

    !==================================================================================================================
    ! Description:
    !! readBoundaries reads the boundary mesh files and the boundary conditions.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: json
        !! json factory

        type(json_value), pointer :: dictPointer
        !! json pointer to dictionary

        type(json_value), pointer ::  boundaryPointer
        !! json pointer to boudnary sub-dictionary
    !------------------------------------------------------------------------------------------------------------------

        character(len=16) :: procID
        !! processor id as string

        character(len=:), allocatable :: tmp
        !! dummy variable

        character(len=:), allocatable :: dictName
        !! dictionary name
    !------------------------------------------------------------------------------------------------------------------

        integer :: ierr 
        !! error flag

        integer :: iBoundary, ival
    !------------------------------------------------------------------------------------------------------------------
    
        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        numberOfPeriodicBound = 0
        totalPerBound = 0

        allocate(this%userName(numberOfBoundaries))
        allocate(this%bcType(numberOfBoundaries))
        allocate(this%startFace(numberOfBoundaries))
        allocate(this%nFace(numberOfBoundaries))
        allocate(this%myProc(numberOfBoundaries))
        allocate(this%neighProc(numberOfBoundaries))

        this%myProc = -1
        this%neighProc = -1

        call value_to_string(id, procID, ierr)
        dictName = 'grid/processor'//crop(procID)//'/boundary'
        call this%boundaryDict%initJSON(dictName)

        call this%boundaryDict%json%get(dictPointer)
        do iBoundary=1,numberOfBoundaries

            call json%get_child(dictPointer, iBoundary+1, boundaryPointer, found)
            call json%info(boundaryPointer, name=tmp)

            this%userName(iBoundary) = tmp

            call json%get(boundaryPointer, 'type', tmp)
            call raiseKeyErrorJSON('type', found, dictName)
            this%bcType(iBoundary) = tmp

            call json%get(boundaryPointer, 'startFace', ival)
            call raiseKeyErrorJSON('type', found, dictName)
            this%startFace(iBoundary) = ival

            call json%get(boundaryPointer, 'nFaces', ival)
            call raiseKeyErrorJSON('type', found, dictName)
            this%nFace(iBoundary) = ival

            if(matchw(this%bcType(iBoundary), 'processor')) then 
                call json%get(boundaryPointer, 'myProcNo', ival)
                call raiseKeyErrorJSON('myProcNo', found, dictName)
                this%myProc(iBoundary) = ival

                call json%get(boundaryPointer, 'neighProcNo', ival)
                call raiseKeyErrorJSON('neighProcNo', found, dictName)
                this%neighProc(iBoundary) = ival

            endif 

            nullify(boundaryPointer)

        enddo

        ! Set up cyclic boundaries, check if there are void patches, set maxium processor size for excahnge buffers
        call this%setupProcessorBoundaries()

        ! Clean up 
        nullify(dictPointer) 

    end subroutine readBoundariesFromFilesFlubioAscii

! **********************************************************************************************************************

    subroutine setupProcessorBoundaries(this)

    !==================================================================================================================
    ! Description:
    !! readBoundaries reads the boundary mesh files and the boundary conditions.
    !==================================================================================================================
    
            class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

            integer :: i, iBoundary, iProc, is, ie, pseudoFaces, max_faces, nEmpty
    !------------------------------------------------------------------------------------------------------------------
    
            integer :: sbuf(nid)
            !! sent buffer
    
            integer :: rbuf(nid)
            !! received buffer
    
            integer :: ierr
            !! error flag
    !------------------------------------------------------------------------------------------------------------------
    
            i = 0

            pseudoFaces = 0
            numberOfPeriodicBound = 0
            totalPerBound = 0

            ! Locate processor's boundaries in the boundary vector
            allocate(this%procBound(numberOfBoundaries-this%numberOfRealBound+numberOfPeriodicBound))
            this%procBound = -1

            do iBoundary=1,numberOfBoundaries
               
                if(this%realBound(iBoundary)==-1) then
                    i = i+1
                    this%procBound(i) = iBoundary
                endif
    
            enddo
    
            this%startFace = this%startFace+1

            ! Look for the max buffer size
            sbuf = 0
            rbuf = 0
            max_faces = -1

            do iBoundary=1,numberOfBoundaries-this%numberOfRealBound
                iProc = this%procBound(iBoundary)
                if(this%nFace(iProc)>=max_faces) max_faces = this%nFace(iProc)
            enddo
                
            sbuf(id+1) = max_faces

            call mpi_allReduce(sbuf, rbuf, nid, MPI_INT, MPI_SUM, MPI_COMM_WORLD, ierr)
            this%maxProcFaces = maxval(rbuf)
            maxProcFaces = maxval(rbuf)
            
            ! Look for bimensionality and components of the problem
            bdim = 0
            nEmpty = 0
            do iBoundary=1,numberOfboundaries
                if(lowercase(this%bcType(iBoundary))=='empty' .or. lowercase(this%bcType(iBoundary))=='void') then
                    bdim = 1
                    nEmpty = nEmpty + 1
                    exit
                endif
            enddo
    
            if(nid==1) this%maxProcFaces=1
    
            ! Update numberOfBoundaries
            numberOfBoundaries = numberOfBoundaries + numberOfPeriodicBound

    end subroutine setupProcessorBoundaries

! *********************************************************************************************************************

    subroutine findBoundaryIndices(this, patchNames, is, ie, nFaces, pIndex, n)

    !==================================================================================================================
    ! Description:
    !! findBoundaryIndices finds the index of a target boundary.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=50) :: patchNames(n)
        !! name of the boundaries

        character(len=50) :: actualBoundary
        !! actual inspected boundary

        character(len=50) :: targetBoundary
        !! target boundary
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, j, n, iBoundary

        integer :: is(n)
        !! start index for each boundary

        integer :: ie(n)
        !! end index for each boundary

        integer :: nFaces(n)
        !! number of faces for each boundary

        integer :: pIndex(n)
        !! patch indices
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
        !! boolean
    !------------------------------------------------------------------------------------------------------------------

        pIndex = 0
        is = 0
        ie = -1

        !i = numberOfElements

        do j=1,n

            found = .false.

            targetBoundary = lowercase(adjustl(trim(patchNames(j))))
            i = numberOfElements
            do iBoundary=1, numberOfRealBound

                actualBoundary = lowercase(adjustl(trim(this%userName(iBoundary))))

                if(trim(actualBoundary)==trim(targetBoundary)) then

                    nFaces(j) = this%nFace(iBoundary)
                    is(j) = this%startFace(iBoundary)
                    ie(j) = is(j) + nFaces(j)-1
                    pIndex(j) = i

                    found = .true.

                else

                    i=i+this%nFace(iBoundary)

                endif

            enddo

            if(.not. found) call flubioStopMsg(' ERROR: Boundary '//trim(targetBoundary)//' not found in the boundaries list!')

        enddo

    end subroutine findBoundaryIndices

! *********************************************************************************************************************

    function findBoundaryByName(this, boundaryName) result(targetBoundary)

    !==================================================================================================================
    ! Description:
    !! findBoundaryByName returns boundary starting and ending indices.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: boundaryName
        !! name of the boundaries

        character(len=50) :: actualBoundary
        !! actual inspected boundary
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary

        integer :: is
        !! start index for each boundary

        integer :: ie
        !! end index for each boundary

        integer :: nFaces
        !! number of faces for each boundary

        integer :: targetBoundary(3)
        !! Boundary indices array (is,ie,nFaces)
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
        !! boolean
    !------------------------------------------------------------------------------------------------------------------

        is = 0
        ie = -1
        targetBoundary = -1

        found = .false.
        do iBoundary=1, numberOfRealBound

            actualBoundary = lowercase(adjustl(trim(this%userName(iBoundary))))

            if(actualBoundary==lowercase(boundaryName)) then

                nFaces = this%nFace(iBoundary)
                is = this%startFace(iBoundary)
                ie = is + nFaces-1

                targetBoundary(1) = is
                targetBoundary(2) = ie
                targetBoundary(3) = nFaces

                found = .true.

            endif

        enddo

        if(.not. found) call flubioStopMsg('ERROR: Boundary '//boundaryName// ' not found in the boundaries list!')

    end function findBoundaryByName

! *********************************************************************************************************************

    subroutine findWallBoundaries(this)

    !==================================================================================================================
    ! Description:
    !! findWallBoundaries find the index of the boundaries which are wall boundaries.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary
        !! target boundary

        integer :: iWall
        !! wall boundary index
    !------------------------------------------------------------------------------------------------------------------

        !----------------------------------------!
        ! Find out the number of wall boundaries !
        !----------------------------------------!

        allocate(this%wallBound(numberOfRealBound))

        numberOfWallBound = 0
        this%wallBound = -1
        this%maxWallFaces = -1

        do iBoundary=1,numberOfRealBound
            if(trim(this%bcType(iBoundary))=='wall') then
                numberOfWallBound = numberOfWallBound+1
                this%wallBound(numberOfWallBound) = iBoundary
            endif
        enddo

        ! Define maxWallFaces for each processor domain
        do iBoundary=1,numberOfWallBound
            iWall = this%wallBound(iBoundary)
            if(this%nFace(iWall) > this%maxWallFaces) this%maxWallFaces = this%nFace(iWall)
        enddo

    end subroutine findWallBoundaries

! *********************************************************************************************************************

    subroutine readLines(info, endLine, ninfo, nread, nskip)

    !==================================================================================================================
    ! Description:
    !! readLines parses the lines in the boundary file.
    !==================================================================================================================
    
        character(10) :: procID
        !! processor ID
    
        character(30) :: auxchar(nread+2)
        !! auxiliary string
    
        character(30) ::  info(ninfo)
        !! auxiliary string to stor info
    !------------------------------------------------------------------------------------------------------------------
    
        integer :: i, is, ie, j, ninfo, nread, nskip, endLine
    !------------------------------------------------------------------------------------------------------------------
    
        write(procID,'(i0)') id
    
        open(1,file='grid/processor'//trim(procID)//'/boundary')
    
            do is=1,nskip
                read(1,*)
            enddo
    
            is = nskip+1
            ie = is+nread
            j = 0
    
            do i=1,nread
                j=j+1
                read(1,*) auxchar(j)
            enddo
    
            ! If the bondary is a periodic, read one more line
            if (auxchar(3)=='processorCyclic' .or. auxchar(3)=='cyclic') then
                j=j+1
                read(1,*)  auxchar(j) ! keyword name
    
                j=j+1
                read(1,*) auxchar(j)  ! keyword
            endif
    
        close(1)
    
        j = 1
        info(1) = auxchar(1)
    
        do i=3,nread,2
            j = j+1
            info(j)=auxchar(i)
        enddo
    
        i = nread
    
        ! If a boundary is periodic read neighProc or refPatch
        if (auxchar(3)=='processorCyclic' .or. auxchar(3)=='cyclic') then
            j = j+1
            info(j) = auxchar(i+2)
            endLine = ie-1+2
        else
            endLine = ie-1
        endif
    
    end subroutine readLines

! *********************************************************************************************************************

    subroutine findNumberOfRealBoundaryFaces(this)

    !==================================================================================================================
    ! Description:
    !! findNumberOfRealBoundaryFaces finds the number of real boundary faces, excluding processor boundaries.
    !==================================================================================================================
        
        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------
        
        integer :: iBoundary
        !! target boundary
        
        integer :: iWall
        !! wall boundary index
    !------------------------------------------------------------------------------------------------------------------
            
        numberOfRealBFaces = 0
        do iBoundary=1,numberOfBoundaries
            if(trim(this%bcType(iBoundary)) /= 'processor') then
                numberOfRealBFaces = numberOfRealBFaces + this%nFace(iBoundary)
            endif
        enddo
        
    end subroutine findNumberOfRealBoundaryFaces
        
! *********************************************************************************************************************
    
    subroutine getProcessorBoundaryMap(this)

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------       
    
        integer :: n, iBoundary, iBFace, iProc, is, ie, pNeigh, patchFace
    !------------------------------------------------------------------------------------------------------------------

        ! Find out the size of the processor boundaries
        pNeigh = 0
        do iBoundary=1,numberOfProcBound
            iProc = this%procBound(iBoundary)
            pNeigh = pNeigh + this%nFace(iProc)  
        enddo

        allocate(this%ghostBoundaryMap(pNeigh,2))
        this%ghostBoundaryMap = -1

        ! Fill the mapping array
        n = 0
        do iBoundary=1,numberOfProcBound

            iProc = this%procBound(iBoundary)
            is = this%startFace(iProc)
            ie = is+this%nFace(iProc)-1
            patchFace = numberOfElements + (is-numberOfIntFaces-1)
            pNeigh = 0

            do iBFace=is,ie
                n = n+1
                pNeigh = pNeigh+1
                this%ghostBoundaryMap(n,1) = patchFace + pNeigh
                this%ghostBoundaryMap(n,2) = iBFace
            enddo

        enddo

    end subroutine getProcessorBoundaryMap    

! *********************************************************************************************************************

end module flubioBoundaries
