!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module nodes

!==================================================================================================================
! Description:
!! This modules implements the data structures and methods for the mesh nodes.
!==================================================================================================================

    use globalMeshVar
    use flubioMpi
    use m_strings, only: isdigit, string_to_value, crop, substitute, split_in_array, matchw, isblank 
    use strings, only: readline

    implicit none

    type, public ::  grid_nodes

        integer :: numberOfPoints
        !! Number of vertex in the mesh

        real, dimension(:,:), allocatable :: vertex
        !! Cartesian coordinates of each vertex in the mesh

    contains
        procedure :: readMeshPoints
        procedure :: readMeshPointsOFAscii
        procedure :: readMeshPointsOFBinary
        procedure :: readMeshPointsFlubioAscii
        procedure :: readMeshPointsFlubioBinary
    end type

contains

    subroutine readMeshPoints(this)

    !==================================================================================================================
    ! Description:
    !! readMeshPoints reads the nodes composing the mesh from the mesh files.
    !==================================================================================================================

        class(grid_nodes) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10) :: procID
        !! processor ID

        character(len=:), allocatable :: fileFormat
        !! file format
    !------------------------------------------------------------------------------------------------------------------

        write(procID, '(i0)' ) id

        call checkFormat('grid/processor'//trim(procID)//'/points', fileFormat)

        if (fileFormat=='openfoam_ascii') then
            call this%readMeshPointsOFAscii()
        elseif(fileFormat=='openfoam_binary') then
            call this%readMeshPointsOFBinary()
        elseif(fileFormat=='flubio_ascii') then 
            call this%readMeshPointsFlubioAscii()
        elseif(fileFormat=='flubio_binary') then 
            call this%readMeshPointsFlubioBinary()
        else
            call flubioStopMsg('ERROR: cannot recognize file format for points.')
        endif                    

    end subroutine readMeshPoints

!**********************************************************************************************************************
 
    subroutine readMeshPointsOFAscii(this)

    !==================================================================================================================
    ! Description:
    !! readMeshPointsOFAscii reads the nodes composing the mesh from the OF mesh files.
    !==================================================================================================================

        class(grid_nodes) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10) :: procID
        !! processor ID

        character(len=80) :: line
        !! parsed line

        character(len=:), allocatable :: croppedLine, oneLine
        !! cropped line

        character(len=25), allocatable :: carray(:)
        !! splitted line
    !------------------------------------------------------------------------------------------------------------------

        integer :: n
        !! loop index

        integer :: np
        !! number of mesh nodes

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        open(1,file='grid/processor'//trim(procID)//'/points')
        
            call skipHeader(aunit=1, fileFormat='ascii')
            call findStreamSize(aunit=1, fileFormat='ascii', streamSize=np, oneLine =  oneLine)

            ! Allocate vertex array
            allocate(this%vertex(np,3))

            do n =1,np

                call readLine(1, line, ierr)
                croppedLine = crop(line)
    
                call substitute(croppedLine,'(','')
                call substitute(croppedLine,')','')
                call split_in_array(croppedLine, carray, ' ')
                
                call string_to_value(carray(1), this%vertex(n,1), ierr)
                call string_to_value(carray(2), this%vertex(n,2), ierr)
                call string_to_value(carray(3), this%vertex(n,3), ierr)
    
            enddo  

        close(1)

        ! Set sizes
        this%numberOfPoints = np
        numberOfPoints = np

    end subroutine readMeshPointsOFAscii 
      
!**********************************************************************************************************************

    subroutine readMeshPointsOFBinary(this)

    !==================================================================================================================
    ! Description:
    !! readMeshPoints reads the nodes composing the mesh from the mesh files.
    !==================================================================================================================

        class(grid_nodes) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10) :: procID
        !! processor ID

        character(len=:), allocatable :: oneLine
    !------------------------------------------------------------------------------------------------------------------

        integer :: n
        !! loop index

        integer :: np
        !! number of mesh nodes
    !------------------------------------------------------------------------------------------------------------------

        write(procID, '(i0)' ) id
        
        open(1, file='grid/processor'//trim(procID)//'/points', FORM = 'UNFORMATTED', access='STREAM')

            call skipHeader(1, 'binary')

            call findStreamSize(1, fileFormat='binary', streamSize=np,  oneLine=oneLine)

            ! Allocate vertex array
            allocate(this%vertex(np,3))

            ! Read points
            do n =1,np
                read(1) this%vertex(n,1:3)
            enddo  

        close(1)

        this%numberOfPoints = np
        numberOfPoints = np

    end subroutine readMeshPointsOFBinary

!**********************************************************************************************************************

    subroutine readMeshPointsFlubioAscii(this)

    !==================================================================================================================
    ! Description:
    !! readMeshPoints reads the nodes composing the mesh from the mesh files.
    !==================================================================================================================

        class(grid_nodes) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10) :: procID
        !! processor ID

        character(15) :: fileFormat
        !! processor ID
    !------------------------------------------------------------------------------------------------------------------

        integer :: n
        !! loop index

        integer :: np
        !! number of mesh nodes
    !------------------------------------------------------------------------------------------------------------------

        write(procID, '(i0)' ) id

        ! Read Points
        open(1,file='grid/processor'//trim(procID)//'/points')

            read(1,*) np

            ! Allocate vertex array
            allocate(this%vertex(np,3))

            ! Read points
            do n=1,np
                read(1,*) this%vertex(n,1), this%vertex(n,2), this%vertex(n,3)
            enddo

        close(1)

        this%numberOfPoints = np
        numberOfPoints = np

    end subroutine readMeshPointsFlubioAscii

!**********************************************************************************************************************                
                    
    subroutine readMeshPointsFlubioBinary(this)

    !==================================================================================================================
    ! Description:
    !! readMeshPoints reads the nodes composing the mesh from the mesh files.
    !==================================================================================================================

        class(grid_nodes) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10) :: procID
        !! processor ID
    !------------------------------------------------------------------------------------------------------------------

        integer :: n
        !! loop index

        integer :: np
        !! number of mesh nodes
    !------------------------------------------------------------------------------------------------------------------

        write(procID, '(i0)' ) id

        ! Read Points
        open(1,file='grid/processor'//trim(procID)//'/points', form='unformatted')

            read(1) np

            ! Allocate vertex array
            allocate(this%vertex(np,3))

            ! Read points
            do n=1,np
                read(1) this%vertex(n,1:3)
            enddo

        close(1)

        this%numberOfPoints = np
        numberOfPoints = np

    end subroutine readMeshPointsFlubioBinary

!**********************************************************************************************************************

    subroutine skipHeader(aunit, fileFormat)

        use m_strings, only: isdigit, string_to_value, value_to_string 

        implicit none
        
        character(len=*) :: fileFormat
        !! file format

        character(len=1) :: a, b, c
        !! dummy characters
    !------------------------------------------------------------------------------------------------------------------

        integer :: aunit
        !! unit to read

        integer :: counter
        !! line counter
    !------------------------------------------------------------------------------------------------------------------
 
        counter = 0
        do
            counter = counter + 1 
            
            if(counter>3) then
                c = b 
                b = a
            endif    

            if(fileFormat=='ascii') then
                read(aunit, *) a
            else
                read(aunit) a    
            endif    

            if(a=='/' .and. b=='/' .and. c==' ') then
                exit
            elseif(a=='}') then 
                exit   
            endif    
        enddo 
        
    end subroutine skipHeader

! *********************************************************************************************************************

    subroutine findStreamSize(aunit, fileFormat, streamSize, oneLine)

        implicit none 

        character(len=*) :: fileFormat

        character(len=:), allocatable :: anumber, oneLine

        character(len=1) :: dummy_binary
        !! dummy character

        character(len=100) :: dummy_ascii
        !! dummy character

        character(len=25), allocatable :: carray(:)
        !! splitted line
    !------------------------------------------------------------------------------------------------------------------

        integer :: aunit
        !! unit to read

        integer :: counter
        !! line counter

        integer :: n
        !! character counter

        integer :: ierr
        !! error flag

        integer :: streamSize
        !! binary stream size
    !------------------------------------------------------------------------------------------------------------------

        logical :: is_num
    !------------------------------------------------------------------------------------------------------------------

        counter = 0
        n = 0
        is_num = .false.

        do
            counter = counter + 1

            if(fileFormat=='ascii') then

                call readLine(aunit, dummy_ascii, ierr)
                
                call split_in_array(dummy_ascii, carray, '(')
                
                if(size(carray)>1) then
                    anumber = carray(1)
                    oneLine = dummy_ascii
                    is_num = isdigit(anumber)
                    if(.not. is_num) call flubioStopMsg( & 
                                    'ERROR: file is corrupted as the line contains ( but the first character is not a number.')
                    exit

                endif    
                
                is_num = isdigit(dummy_ascii)
         
                if(is_num .and. n==0) then
                    anumber = dummy_ascii
                elseif(trim(dummy_ascii)=='(') then
                    exit       
                endif   

            else

                read(aunit) dummy_binary

                is_num = isdigit(dummy_binary)
      
                if(is_num .and. n==0) then
                    anumber = dummy_binary
                    n = n + 1
                elseif(is_num .and. n > 0) then
                    anumber = anumber // dummy_binary
                    n = n + 1
                elseif(dummy_binary=='(') then
                    exit       
                endif   

            endif    
              
        enddo

        call string_to_value(anumber, streamSize, ierr)

    end subroutine findStreamSize

! *********************************************************************************************************************    

    subroutine checkFormat(fileName, fileFormat)

        character(len=*) :: fileName 
        !! file name

        character(len=100) :: line
        !! parsed line

        character(len=:), allocatable :: fileFormat
        !! file format

        character(len=16), allocatable :: carray(:)
        !! splitted line
    !------------------------------------------------------------------------------------------------------------------

        integer :: n, ierr
    !------------------------------------------------------------------------------------------------------------------

        logical :: formatFound, isOpenFoam, isNumber, isFoamFile, isCpp
    !------------------------------------------------------------------------------------------------------------------

        ! Check if it is an openFoam file
        open(1, file=fileName)
            call readLine(1, line, ierr)
            isCpp = matchw(line, '*C++*')
            isFoamFile = matchw(line, 'FoamFile')
            isOpenFoam = isCpp .or. isFoamFile
        close(1)

        if(isOpenFoam) then

            open(1, file=fileName)

                do
                    call readLine(1, line, ierr)
                    formatFound = matchw(line, '*format*')
                
                    if(formatFound) then 
                        call split_in_array(line, carray, ' ')
                        call substitute(carray(2), ';', '')
                        fileFormat = trim(carray(2))
                        exit    
                    endif    

                    if (ierr /= 0) then
                        exit
                    endif    
                enddo    

            close(1)

            fileFormat = 'openfoam_'//fileFormat
        
        else

            ! Check if it is a flubio format, ascii always starts with a number
            open(1, file=fileName)

                call readLine(1, line, ierr)
                isNumber = isdigit(line)
            
                if(matchw(fileName, '*boundary')) then 
                    fileFormat = 'flubio_ascii'
                else
                    if(isNumber) then
                        fileFormat = 'flubio_ascii'
                    else
                        fileFormat = 'flubio_binary'
                    endif  
                endif  
                
            close(1)

        endif    

    end subroutine checkFormat      

! *********************************************************************************************************************

end module nodes
