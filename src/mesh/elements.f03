!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module elements

!==================================================================================================================
! Description:
!! This module implements the data structures and methods for the mesh cells.
!==================================================================================================================

    use globalMeshVar
    use faces
    use cellRegions
    use flubioContainters

    implicit none

    type, public, extends(grid_faces) :: grid_elements

        integer :: numberOfElements
        !! Number of cells in the processor boundaries

        integer :: numberOfRegions
        !! number of regions in the computational domain

        type(integerCellStruct), dimension(:), allocatable :: iFaces
        !! List of faces composing a cell

        type(integerCellStruct), dimension(:), allocatable :: elmFaceSign
        !! List of faces composing a cell, it will be deallocated when not needed anymore

        type(integerCellStruct), dimension(:), allocatable :: iNeighbours
        !! List of cells neighbouring of a target cell. The ghost neighbours have the GLOBAL index!

        type(integerCellStruct), dimension(:), allocatable :: localNeighbours
        !! List of local cells neighbouring of a target cell.

        integer, dimension(:), allocatable :: numberOfNeighbours
        !! Number of neighbours of a cell (i.e. number of faces for an internal cell)

        type(region), dimension(:), allocatable :: meshRegions
        !! List of user defined regions

        ! Geometric varaibles

        real, dimension(:), allocatable :: volume
        !! Cell volume

        real, dimension(:,:), allocatable :: centroid
        !! Cartesian coordinates of a cell cell center

        !----------------!
        ! Connectivities !
        !----------------!

        integer :: nnz
        !! number of non-zer entries for each processor

        integer, dimension(:), allocatable :: cellGlobalAddr
        !! Array linking the cell local address to the global one

        integer, dimension(:), allocatable :: d_nnz
        !! Array linking the cell local address to the global one

        integer, dimension(:), allocatable :: o_nnz
        !! Array linking the cell local address to the global one

        type(integerCellStruct), dimension(:), allocatable ::  conn
        !! mesh connectivities

        !-------------!
        ! ghost cells !
        !-------------!

        integer, dimension(:,:), allocatable :: neigh
        !! neighbour periodic boundary

        real, dimension(:,:,:), allocatable :: poffset
        !! Offset vector between periodic faces

        contains
            procedure :: readGlobalCellAddr
            procedure :: buildElements

            procedure :: createMeshRegions
            procedure :: getRegionIndexByName
            procedure :: setUpMeshRegion

            procedure :: getOwnershipRange
            procedure :: computeNonZeroEntries

    end type grid_elements

contains

    subroutine buildElements(this)

    !==================================================================================================================
    ! Description:
    !! buildElements assembles together the mesh faces to form mesh cells.
    !==================================================================================================================

        class(grid_elements) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer i, iElement, iFace, iBFace, iOwner, iNeighbour, nen
    !------------------------------------------------------------------------------------------------------------------

        integer, dimension(:), allocatable :: cind, nn, numberOfNeighs
        !! Index mapper

        integer, dimension(:), allocatable :: iFaces
        !! Faces composing one cell
    !------------------------------------------------------------------------------------------------------------------

        this%numberOfElements = numberOfElements

        allocate(this%iFaces(numberOfElements))
        allocate(this%elmFaceSign(numberOfElements))
        allocate(this%localNeighbours(numberOfElements))
        allocate(this%iNeighbours(numberOfElements))
        allocate(this%conn(numberOfElements))

        allocate(this%d_nnz(numberOfElements))
        allocate(this%o_nnz(numberOfElements))

        allocate(numberOfElementFaces(numberOfElements))
        allocate(cind(this%numberOfElements))
        allocate(nn(this%numberOfElements))
        allocate(numberOfNeighs(this%numberOfElements))
        allocate(this%numberOfNeighbours(numberOfElements))

        numberOfElementFaces = 0
        nn = 0
        numberOfNeighs = 0
        cind = 0

        ! Find out number of faces composing each elements, internal faces
        do iFace=1,numberOfIntFaces

            iOwner = this%Owner(iFace)
            nn(iOwner) = nn(iOwner)+1
            numberOfNeighs(iOwner) = numberOfNeighs(iOwner)+1

            iNeighbour = this%neighbour(iFace)
            nn(iNeighbour) = nn(iNeighbour)+1
            numberOfNeighs(iNeighbour) = numberOfNeighs(iNeighbour)+1

        end do

        ! Add boundary faces
        do iBFace=numberOfIntFaces+1,numberOfFaces
            iOwner = this%Owner(iBFace)
            nn(iOwner) = nn(iOwner)+1

            ! add one slot for processor boundaries
            numberOfNeighs(iOwner) = numberOfNeighs(iOwner)+1
        enddo

        ! Allocate structures
        do iElement=1,numberOfElements

            this%iFaces(iElement)%csize = nn(iElement)
            this%elmFaceSign(iElement)%csize = nn(iElement)
            this%localNeighbours(iElement)%csize = nn(iElement)
            this%iNeighbours(iElement)%csize = numberOfNeighs(iElement)
            this%conn(iElement)%csize = nn(iElement)

            call this%iFaces(iElement)%initIntColumn(nn(iElement), -1)
            call this%elmFaceSign(iElement)%initIntColumn(nn(iElement), 0)
            call this%localNeighbours(iElement)%initIntColumn(nn(iElement), 0)
            call this%iNeighbours(iElement)%initIntColumn(numberOfNeighs(iElement), 0)
            call this%conn(iElement)%initIntColumn(nn(iElement), -1)

        end do

        ! Assign internal faces to the owner element
        do iFace=1,numberOfIntFaces

            iOwner = this%Owner(iFace)
            iNeighbour = this%neighbour(iFace)

            cind(iOwner) = cind(iOwner)+1

            this%iFaces(iOwner)%col(cind(iOwner)) = iFace
            this%elmFaceSign(iOwner)%col(cind(iOwner)) = 1
            this%localNeighbours(iOwner)%col(cind(iOwner)) = iNeighbour
            this%iNeighbours(iOwner)%col(cind(iOwner)) = iNeighbour

            cind(iNeighbour) = cind(iNeighbour)+1

            this%iFaces(iNeighbour)%col(cind(iNeighbour)) = iFace
            this%elmFaceSign(iNeighbour)%col(cind(iNeighbour)) = -1
            this%localNeighbours(iNeighbour)%col(cind(iNeighbour)) = iOwner
            this%iNeighbours(iNeighbour)%col(cind(iNeighbour)) = iOwner

        enddo

        numberOfFaces = this%numberOfFaces

        ! Assign boundary faces (physical boundaries + processor boundaries) to the owner element
        do iBFace=numberOfIntFaces+1,numberOfFaces

            iOwner = this%Owner(iBFace)
            cind(iOwner) = cind(iOwner)+1

            this%iFaces(iOwner)%col(cind(iOwner)) = iBFace
            this%elmFaceSign(iOwner)%col(cind(iOwner)) = 1

        enddo

        this%numberOfNeighbours = 0

        ! Count the number of neighbours of the elements. Processor boundaries are taken into account later
        do iElement=1,numberOfElements
            do i=1, this%iNeighbours(iElement)%csize
                if (this%iNeighbours(iElement)%col(i)/=0) then
                    this%numberOfNeighbours(iElement) = this%numberOfNeighbours(iElement)+1
                endif
            enddo
        enddo

        numberOfBElements = numberOfFaces - numberOfIntFaces
        numberOfBFaces = numberOfFaces - numberOfIntFaces

        ! Ending message
        call flubioMsg('FLUBIO: mesh elements built!')

    end subroutine buildElements

! *********************************************************************************************************************

    subroutine readGlobalCellAddr(this)

    !==================================================================================================================
    ! Description:
    !! readGlobalCellAddr reads the global cell adress of the mesh cells.
    !==================================================================================================================

        class(grid_elements) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10) :: procID
        !! processor ID
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
        !! target cell

        integer :: dummy
        !! dummy integer

        integer :: is

        integer :: sbuf(nid), rbuf(nid)
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        sbuf = 0
        rbuf = 0
        sbuf(id+1) = numberOfElements

        allocate(this%cellGlobalAddr(numberOfElements))

        call mpi_allReduce(sbuf, rbuf, nid, MPI_INT, MPI_SUM, MPI_COMM_WORLD, dummy)


        if(id==0) then
            is = 0
        else
            is = sum(rbuf(1:id))
        endif

        do iElement=1,numberOfElements
            this%cellGlobalAddr(iElement) = is + iElement
        enddo

    end subroutine readGlobalCellAddr

! *********************************************************************************************************************

    subroutine createMeshRegions(this)

    !==================================================================================================================
    ! Description:
    !! createMeshRegions create the regions with the mesha as defined in flubioMeshRegions.
    !==================================================================================================================

        use flubioDictionaries, only : flubioMeshRegions, raiseKeyErrorJSON
        use json_module
        use m_strings

        class(grid_elements) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: typeValue, path, regionName, keyName
        !! First array element lenght

        character(len=:), dimension(:), allocatable :: acvec
        !! First array element lenght

        character(len=50), dimension(:), allocatable  :: splitPath
    !------------------------------------------------------------------------------------------------------------------

        integer :: iRegion, iElement
        !! Target cell
    !------------------------------------------------------------------------------------------------------------------

       real, dimension(:), allocatable :: rvec

       real :: radius, h, center(3), p1(3), p2(3), p3(4)
    !------------------------------------------------------------------------------------------------------------------

        logical :: regionFound, regionsFound, keyFound, typeFound, nameFound, subDictFound

        logical :: stlTransformations(4)
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, regionsPointer, regionPointer, subDictPointer
    !------------------------------------------------------------------------------------------------------------------

        ! Allocate Regions
        allocate(this%meshRegions(flubioMeshRegions%numberOfRegions))
        this%numberOfRegions = flubioMeshRegions%numberOfRegions

        ! Get the pointer to the dictionary
        call flubioMeshRegions%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Regions', regionsPointer, regionsFound)
        do iRegion = 1, flubioMeshRegions%numberOfRegions

            ! Get regions pointer
            call jcore%get_child(regionsPointer, iRegion, regionPointer, regionFound)

            ! Get region name
            call jcore%get_path(regionPointer, path, nameFound)
            if(nameFound) then
                call split_in_array(path, splitPath, '.')
                regionName = splitPath(2)
                this%meshRegions(iRegion)%regionName = regionName
            else
                call flubioStopMsg('ERROR: cannot find the region name, please check if the dictionary is correct.')
            end if

            ! Get region type
            call jcore%get(regionPointer, 'type', typeValue, typeFound)

            if(typeFound)then

                if(trim(typeValue)=='box') then
                    call jcore%get(regionPointer, 'center', rvec, keyFound)
                    call raiseKeyErrorJSON('center', keyFound, 'settings/meshRegions')
                    center = rvec

                    call jcore%get(regionPointer, 'dimensions', rvec, keyFound)
                    call raiseKeyErrorJSON('dimensions', keyFound, 'settings/meshRegions')
                    p1 = rvec

                    ! Create region
                    call this%meshRegions(iRegion)%createBoxRegion(regionName, typeValue, center, p1)

                elseif(trim(typeValue)=='cylinder') then

                    call jcore%get(regionPointer, 'p1', rvec, keyFound)
                    call raiseKeyErrorJSON('p1', keyFound, 'settings/meshRegions')
                    p1 = rvec

                    call jcore%get(regionPointer, 'p2', rvec, keyFound)
                    call raiseKeyErrorJSON('p2', keyFound, 'settings/meshRegions')
                    p2 = rvec

                    call jcore%get(regionPointer, 'radius', radius, keyFound)
                    call raiseKeyErrorJSON('radius', keyFound, 'settings/meshRegions')

                    ! Create region
                    call this%meshRegions(iRegion)%createCylindricalRegion(regionName, typeValue, p1, p2, radius)

                elseif(trim(typeValue)=='sphere') then
                    call jcore%get(regionPointer, 'center', rvec, keyFound)
                    call raiseKeyErrorJSON('center', keyFound, 'settings/meshRegions')
                    center = rvec

                    call jcore%get(regionPointer, 'radius', radius, keyFound)
                    call raiseKeyErrorJSON('radius', keyFound, 'settings/meshRegions')

                    ! Create region
                    call this%meshRegions(iRegion)%createSphericalRegion(regionName, typeValue, center, radius)

                elseif(trim(typeValue)=='cone') then
                    call jcore%get(regionPointer, 'center', rvec, keyFound)
                    call raiseKeyErrorJSON('center', keyFound, 'settings/meshRegions')
                    center = rvec

                    call jcore%get(regionPointer, 'normal', rvec, keyFound)
                    call raiseKeyErrorJSON('normal', keyFound, 'settings/meshRegions')
                    p1 = rvec

                    call jcore%get(regionPointer, 'radius', radius, keyFound)
                    call raiseKeyErrorJSON('radius', keyFound, 'settings/meshRegions')

                    call jcore%get(regionPointer, 'height', h, keyFound)
                    call raiseKeyErrorJSON('height', keyFound, 'settings/meshRegions')

                    ! Create region
                    call this%meshRegions(iRegion)%createConicalRegion(regionName, typeValue, center, p1, radius, h)

                elseif(trim(typeValue)=='stl') then
                    call jcore%get(regionPointer, 'surface', keyName, keyFound)
                    call raiseKeyErrorJSON('surface', keyFound, 'settings/meshRegions')

                    call jcore%get_child(regionPointer, 'transform', subDictPointer, subDictFound)

                    if(subDictFound) then

                        call jcore%get(subDictPointer, 'scale', rvec, keyFound)
                        if(keyFound) p1 = rvec
                        stlTransformations(1) = keyFound

                        call jcore%get(subDictPointer, 'translate', rvec, keyFound)
                        if(keyFound) p2 = rvec
                        stlTransformations(2) = keyFound

                        call jcore%get(subDictPointer, 'rotateByAxis', rvec, keyFound)
                        if(keyFound) p3 = rvec
                        stlTransformations(3) = keyFound

                        !call jcore%get(subDictPointer, 'rotateByMatrix', keyName, keyFound)

                    end if

                    ! Create STL region
                    call this%meshRegions(iRegion)%createStlRegion(regionName, typeValue, keyName, &
                                                                   stlTransformations, p1, p2, p3)

                    ! nullify the pointer
                    if(subDictFound) nullify(subDictPointer)

                else
                    call flubioStopMsg('ERROR: unknown region type: '//trim(typeValue))
                end if

                call this%setUpMeshRegion(iRegion)

            else
                call flubioStopMsg("ERROR: cannot find the region type. Please specify it in meshRegions.")
            endif

        end do

        ! Clean up
        nullify(dictPointer)
        if(regionsFound) nullify(regionsPointer)
        if(regionFound) nullify(regionPointer)
        call jcore%destroy()

    end subroutine createMeshRegions

!**********************************************************************************************************************

    subroutine setUpMeshRegion(this, iRegion)

    !==================================================================================================================
    ! Description:
    !! setUpRegion sets up the box region (cell value equals to 1 are with the region)
    !==================================================================================================================

        class(grid_elements) :: this
    !-------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iFace, iRegion, inside
    !------------------------------------------------------------------------------------------------------------------

        real :: point(3)
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements
            point = this%centroid(:,iElement)
            inside = this%meshRegions(iRegion)%isInsideRegion(point)
            this%meshRegions(iRegion)%phi(iElement,1) = real(inside)
        end do

        call this%meshRegions(iRegion)%writeRegionToFile()

    end subroutine setUpMeshRegion

! *********************************************************************************************************************

    function getRegionIndexByName(this, targetRegion) result(regionIndex)

    !==================================================================================================================
    ! Description:
    !! getRegionIndexByName return the index of the target region in the regiuon array.
    !==================================================================================================================

        class(grid_elements) :: this
    !-------------------------------------------------------------------------------------------------------------------

        character(len= *) :: targetRegion
    !------------------------------------------------------------------------------------------------------------------

        integer :: regionIndex, iRegion
    !------------------------------------------------------------------------------------------------------------------

        regionIndex = -1
        do iRegion=1, this%numberOfRegions
            if(trim(targetRegion)==trim(this%meshRegions(iRegion)%regionName)) then
                regionIndex = iRegion
            end if
        end do

        if(regionIndex==-1) call flubioStopMsg('ERROR: tried to find the region'//targetRegion//'index, but that region is not found')

    end function getRegionIndexByName

! *********************************************************************************************************************

    subroutine computeNonZeroEntries(this)

    !==================================================================================================================
    ! Description:
    !! computeNonZeroEntries computes the number of diagonal and off-diagonal connections.
    !==================================================================================================================

        use flubioDictionaries, only: flubioSolvers

        class(grid_elements) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, n

        integer :: irange(2), globalConn
    !------------------------------------------------------------------------------------------------------------------

       call flubioSolvers%setMatrixSize()

        ! Find out the exact storage needed
        irange = this%getOwnershipRange()

        this%d_nnz = 0
        this%o_nnz = 0
        do iElement=1,numberOfElements

            do n=1,this%numberOfNeighbours(iElement)

                globalConn = this%conn(iElement)%col(n)-1

                if(globalConn >= irange(1)-1 .and. globalConn <= irange(2)-1 ) then
                    this%d_nnz(iElement) = this%d_nnz(iElement) + 1
                else
                    this%o_nnz(iElement) = this%o_nnz(iElement) + 1
                endif

            enddo

            ! Add the diagonal coefficient which is never zero
            this%d_nnz(iElement) = this%d_nnz(iElement) + 1

        enddo

        this%nnz = sum(this%d_nnz) + sum(this%o_nnz)

    end subroutine computeNonZeroEntries

!**********************************************************************************************************************

    function getOwnershipRange(this) result(ownershipRange)

        class(grid_elements) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i

        integer :: ierr

        integer :: ownershipRange(2)

        integer :: sbuf(nid), rbuf(nid)
    !--------------------------------------------------------------------------------------------------------------

        ownershipRange = -1

        sbuf = 0
        sbuf(id+1) = numberOfElements
        call mpi_Allreduce(sbuf, rbuf, nid, MPI_INT, MPI_SUM, MPI_COMM_WORLD, ierr)

        if(id>0) then
            ownershipRange(1) = sum(rbuf(1:id))+1
            ownershipRange(2) = ownershipRange(1) + numberOfElements-1
        else
            ownershipRange(1) = 1
            ownershipRange(2) = numberOfElements
        endif

    end function getOwnershipRange

!**********************************************************************************************************************

end module elements
