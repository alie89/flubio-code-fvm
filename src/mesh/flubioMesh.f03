!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module flubioMesh

!==================================================================================================================
! Description:
!! flubioMesh contains the mesh data structure and the methods to operate with it.
!==================================================================================================================

    use elements
    use globalMeshVar
    use flubioBoundaries
    use mpi
    use math
    use auxilaries
    use m_unista
    use m_KdTree, only: KdTree, KdTreeSearch
    use kd_tree
    use kdtree2_module
    use dArgDynamicArray_Class
    use iDynamicArray_Class
    use flubioDictionaries, only: flubioOptions


    implicit none

    type, public, extends(grid_elements) :: fvMesh

        type(fvBoundaries) :: boundaries

        type(KdTree) :: tree

        type(tree_master_record), pointer :: tree_mk

        type(kdtree2), pointer :: tree_mk2

        contains

            procedure :: readMeshFromFiles

            procedure :: setGlobalconn
            procedure :: findNeighbours
            procedure :: createMeshSearchTree
            procedure :: findPointOwnership
            procedure :: findBoundaryPoints

            procedure :: compute_geom
            procedure :: computeAreas
            procedure :: computeVolumes
            procedure :: computeInterpolationWeights
            procedure :: nonOrthogonalCorrection
            procedure :: pressureNonOrthogonalCorrection

            procedure :: mpi_create_ghosts_I

            procedure :: getFacesAndOffsets
            procedure :: updateCentroidBoundaryMesh

            procedure :: buildNodesToElementConn

            procedure :: leastSquareMatrix

            procedure :: faceNormals

            procedure :: saveMesh

    end type fvMesh

contains

    subroutine readMeshFromFiles(this)

    !==================================================================================================================
    ! Description:
    !! readMeshFromFiles reads the mesh from the mesh files in the "grid/" folder.
    !==================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        ! Starting message
        call flubioMsg('FLUBIO: initialising the mesh')

        !---------------!
        ! Internal mesh !
        !---------------!

        ! Read Points
        call this%readMeshPoints()

        ! Read Faces, Owner, Neighbours
        call this%readMeshFaces()

        ! Read cell global adress
        call this%readGlobalCellAddr()

        ! Build elements
        call this%buildElements()

        !---------------!
        ! Boundary mesh !
        !---------------!

        call this%boundaries%readBoundariesFromFiles()

        !----------------------------------------!
        ! Compute Control Volume characteristics !
        !----------------------------------------!

        call this%compute_geom()

        !--------------------!
        ! Set connectivities !
        !--------------------!

        call this%setGlobalConn()

        call this%findBoundaryPoints()

        !----------------------!
        ! Save Decomposed Mesh !
        !----------------------!

        call this%saveMesh()

    end subroutine readMeshFromFiles

! *********************************************************************************************************************

    subroutine compute_geom(this)

    !==================================================================================================================
    ! Description:
    !! compute_geom calculates all the mesh geometrical information needed by FLUBIO (e.g. volumes, faces normals, etc. ).
    !==================================================================================================================

        use flubioDictionaries, only : flubioMeshRegions

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        ! Set number of processor boundaries (cyclic patches are recasted as processor boundaries)
        numberOfProcBound = this%boundaries%numberOfBoundaries-this%boundaries%numberOfRealBound+numberOfPeriodicBound

        ! Avoid 0-index running in serial
        if(numberOfProcBound==0) then
            numberOfProcBound1 = numberOfProcBound+1
        else
            numberOfProcBound1 = numberOfProcBound
        endif

        !------------------------------------------!
        ! Compute faces areas and elements volumes !
        !------------------------------------------!

        call this%computeAreas()
        call this%computeVolumes()

        !---------------------------------------!
        ! Create ghost nodes between processors !
        !---------------------------------------!

        call this%mpi_create_ghosts_I()

        !---------------------------------!
        ! Fill up centroid boundary field !
        !---------------------------------!

        call this%updateCentroidBoundaryMesh()

        !-------------------------------!
        ! Compute interpolation weights !
        !-------------------------------!

        call this%computeInterpolationWeights()

        !-------------------------!
        ! Find out the neighbours !
        !-------------------------!

        ! N.B: This routine is VITAL for the well beahviour of the parallelization and linear system assembling.
        ! DO NOT touch it unless you know very well what you are doing!

        call this%findNeighbours()

        !--------------------!
        ! Build Mesh Regions !
        !--------------------!

        if(flubioMeshRegions%found) call this%createMeshRegions()

    end subroutine compute_geom

! *********************************************************************************************************************

    subroutine computeVolumes(this)

    !==================================================================================================================
    ! Description:
    !! computeVolumes computes the cell volumes.
    !==================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iNode, iFace, iBFace, nFaces, numberOfiNodes, csize

        integer, dimension(:), allocatable :: iFaces
    !------------------------------------------------------------------------------------------------------------------

        real :: centre(3), centroid(3), Sf(3), Cf(3)

        real :: localFaceSign, localVolumeSum, localVolume, volume, localVolumeCentroidSum(3), localCentroid(3)
    !------------------------------------------------------------------------------------------------------------------

        ! Starting message
        call flubioMsg('FLUBIO: computing elements centroids and volumes...')

        !-------------------------------------!
        ! Compute volume and element centroid !
        !-------------------------------------!

        allocate(this%centroid(3,numberOfElements+numberOfBFaces+1))
        allocate(this%volume(numberOfElements))

        do iElement=1,numberOfElements

            csize = this%iFaces(iElement)%csize
            allocate(iFaces(csize))
            numberOfElementFaces(iElement) = csize
            iFaces = this%iFaces(iElement)%col(1:csize)

            nFaces=numberOfElementFaces(iElement)
            centre = 0.0

            do iFace=1,nFaces
                centre = centre + this%fcentroid(iFaces(iFace),:)
            enddo

            centroid = 0.0
            Sf = 0.0

            centre = centre/nFaces
            localVolumeCentroidSum = 0.0

            localVolumeSum = 0.0

            do iFace=1,nFaces

                localFaceSign = this%elmFaceSign(iElement)%col(iFace)

                Sf = this%Sf(iFaces(iFace),:)*localFaceSign
                Cf = this%fcentroid(iFaces(iFace),:)-centre

                localVolume = Sf(1)*Cf(1)+Sf(2)*Cf(2)+Sf(3)*Cf(3)
                localVolume = localVolume/3.0
                localCentroid = 0.75*this%fcentroid(iFaces(iFace),:)+0.25*centre
                localVolumeCentroidSum = localVolumeCentroidSum + localCentroid*localVolume
                localVolumeSum = localVolumeSum + localVolume

            enddo

            centroid = localVolumeCentroidSum/localVolumeSum

            volume = localVolumeSum

            this%volume(iElement) = volume
            this%centroid(:,iElement) = centroid

            deallocate(iFaces)

        enddo

        ! Clean up
        do iElement=1,numberOfElements
            call this%elmFaceSign(iElement)%clearIntColumn()
        enddo

        deallocate(this%elmFaceSign)


    end subroutine computeVolumes

! *********************************************************************************************************************

    subroutine computeAreas(this)

    !==================================================================================================================
    ! Description:
    !! computeAreas computes the normal vector (direction and magnitude) mesh faces.
    !==================================================================================================================

        class(fvMesh) :: this
    !-------------------------------------------------------------------------------------------------------------------

        integer  iFace, iBFace, iNode, iTriangle, nFaces, numberOfiNodes

        integer, dimension(:), allocatable :: iNodes
    !------------------------------------------------------------------------------------------------------------------

        real point1(3), point2(3), point3(3), centre(3), v1(3), v2(3), cross(3)

        real centroid(3), local_centroid(3), Sf(3), area, local_area, local_Sf(3)
    !------------------------------------------------------------------------------------------------------------------

        ! Starting message
        call flubioMsg('FLUBIO: computing faces area and normals...')

        !---------------------!
        ! Compute Face center !
        !---------------------!

        nFaces = this%numberOfFaces

        allocate(this%fcentroid(nFaces,3))
        allocate(this%Sf(nFaces,3))
        this%fcentroid = 0.0
        this%Sf = 0.0

        !-----------------!
        ! Loop over faces !
        !-----------------!

        do iFace=1,nFaces

            numberOfiNodes = numberOfFaceNodes(iFace)
            allocate(iNodes(numberOfiNodes))

            iNodes = this%fvertex(iFace)%col(1:numberOfiNodes)
            centre = 0.0

            !--------------!
            ! Face Centers !
            !--------------!

            do iNode=1,numberOfiNodes
                centre = centre + this%vertex(iNodes(iNode),:)
            enddo

            centre = centre/numberOfiNodes

            !-------------------!
            ! Areas and normals !
            !-------------------!

            Sf = 0.0
            area = 0.0
            centroid = 0.0

            do iTriangle=1,numberOfiNodes

                point1 = centre
                point2 = this%vertex(iNodes(iTriangle),:)

                if (iTriangle<numberOfiNodes) then
                    point3 = this%vertex(iNodes(iTriangle+1),:)
                else
                    point3 = this%vertex(iNodes(1),:)
                endif

                local_centroid = (point1+point2+point3)/3.0
                v1 = point2-point1
                v2 = point3-point1

                call cross_prod(v1, v2, cross)

                local_Sf = 0.5*cross
                local_area=sqrt(local_Sf(1)**2+local_Sf(2)**2+local_Sf(3)**2)

                centroid = centroid + local_area*local_centroid
                Sf = Sf + local_Sf
                area = area + local_area

            enddo

            centroid = centroid/area
            this%fcentroid(iFace,:) = centroid
            this%Sf(iFace,:) = Sf

            deallocate(iNodes)

        enddo

    end subroutine computeAreas

! *********************************************************************************************************************

    subroutine computeInterpolationWeights(this)

    !==================================================================================================================
    ! Description:
    !! computeInterpolationWeights computes the linear interpolation weights at each face.
    !==================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer i, iElement, iFace, iBFace, iBoundary, iProc, iOwn, iNeigh, pNeigh, is, ie, patchFace
    !------------------------------------------------------------------------------------------------------------------

        real centroid(3), local_centroid(3), Sf(3), area, q, dot1, dot2

        real  CN(3), eCN(3), Cf(3), fF(3), ffp(3), Nfp(3), nf(3), Ef(3)
    !------------------------------------------------------------------------------------------------------------------

        ! Starting message
        call flubioMsg('FLUBIO: computing interpolation weights...')

        !---------!
        ! Prepare !
        !---------!

        allocate(this%CN(numberOfFaces,3))
        allocate(this%Tf(numberOfFaces,3))
        allocate(this%gf(numberOfFaces))
        allocate(this%gDiff(numberOfFaces))

        this%gf = 0.0

        ! Loop over internal faces
        do iFace=1,numberOfIntFaces

            nf = this%Sf(iFace,:)/norm2(this%Sf(iFace,:))

            iOwn = this%owner(iFace)
            iNeigh = this%neighbour(iFace)

            CN = this%centroid(:,iNeigh)-this%centroid(:,iOwn)
            this%CN(iFace,:) = CN

            eCN = CN/norm2(CN)

            !----------------------------------!
            ! Non orthogonal correction method !
            !----------------------------------!

            dot1 = dot_product(eCN,this%Sf(iFace,:))
            call this%nonOrthogonalCorrection(iFace, Ef)

            this%Tf(iFace,:) = this%Sf(iFace,:)-Ef
            this%gDiff(iFace) = norm2(Ef)/norm2(CN)

            !-----------------------!
            ! Interpolation weigths !
            !-----------------------!

            Cf = this%fcentroid(iFace,:) - this%centroid(:,iOwn)
            fF = this%centroid(:,iNeigh) - this%fcentroid(iFace,:)

            ! Weights for skew corrected interpolation scheme
            if(flubioOptions%interpOpt==1)  then

                if(flubioOptions%skwOpt==0) then

                    ffp = (dot_product(this%fcentroid(iFace,:), nf)/dot_product(eCN, nf))*eCN
                    this%gf(iFace) = 1.0 - norm2(this%centroid(:,iNeigh)-ffp)/norm2(CN)

                elseif(flubioOptions%skwOpt==1) then
                    this%gf(iFace) = 0.5
                else

                    dot1 = dot_product(Cf,CN)
                    dot2 = dot_product(CN,CN)

                    q = dot1/dot2

                    ffp = this%centroid(:,iOwn) + q*CN ! + neigh to own
                    Nfp = this%centroid(:,iNeigh) - ffp

                    this%gf(iFace) = 1.0 - norm2(Nfp)/norm2(CN)

                endif

            ! Standard weights
            else
                dot1 = dot_product(Cf,nf)
                dot2 = dot_product(fF,nf)
                this%gf(iFace) = dot1/(dot1+dot2)
            endif

        enddo

        !----------------------------------!
        ! Boundary faces: real + processor !
        !----------------------------------!

        i = 0
        do iBoundary=1,numberOfBoundaries

            is = this%boundaries%startFace(iBoundary)
            ie = is+this%boundaries%nFace(iBoundary)-1
            patchFace = numberOfElements + (is-numberOfIntFaces-1)
            pNeigh = 0

            if(this%boundaries%realBound(iBoundary)==-1) i=i+1

            do iBFace=is, ie

                iOwn = this%owner(iBFace)

                nf = this%Sf(iBFace,:)/norm2(this%Sf(iBFace,:))

                if(this%boundaries%realBound(iBoundary)/=-1) then
                    CN = this%fcentroid(iBFace,:)-this%centroid(:,iOwn)

                    dot1 = dot_product(CN,nf)
                    CN = dot1*nf
                else
                    pNeigh = pNeigh+1
                    CN = (this%centroid(:,patchFace+pNeigh)-this%centroid(:,iOwn))
                endif

                this%CN(iBFace,:) = CN

                eCN = CN/norm2(CN)

                !----------------------------------!
                ! Non orthogonal correction method !
                !----------------------------------!

                call this%nonOrthogonalCorrection(iBFace, Ef)

                this%Tf(iBFace,:) = this%Sf(iBFace,:)-Ef
                this%gDiff(iBFace) = norm2(Ef)/norm2(CN)

                !--------------------------------!
                ! Skewness interpolation weigths !
                !--------------------------------!

                dot1 = dot_product(CN,this%Sf(iBFace,:))

                if(this%boundaries%realBound(iBoundary)/=-1) then

                    this%gf(iBFace) = 1.0

                else

                    Cf = this%fcentroid(iBFace,:) - this%centroid(:,iOwn)
                    fF = this%centroid(:,patchFace+pNeigh) - this%fcentroid(iBFace,:)

                    ! Weights for skew corrected interpolation scheme
                    if(flubioOptions%interpOpt==1) then

                        if(flubioOptions%skwOpt==0) then

                            ffp = (dot_product(this%fcentroid(iBFace,:), nf)/dot_product(eCN, nf))*eCN
                            this%gf(iBFace) = 1.0 - norm2(this%centroid(:,patchFace+pNeigh)-ffp)/norm2(CN)

                        elseif(flubioOptions%skwOpt==1) then
                            this%gf(iBFace) = 0.5
                        else
                            dot1 = dot_product(Cf,CN)
                            dot2 = dot_product(CN,CN)

                            q = dot1/dot2

                            ffp = this%centroid(:,iOwn) + q*CN ! + neigh to own
                            Nfp = this%centroid(:,patchFace+pNeigh) - ffp

                            this%gf(iBFace) = norm2(Nfp)/norm2(CN)

                        endif

                    ! Standard weights
                    else

                        dot1 = dot_product(Cf,nf)
                        dot2 = dot_product(fF,nf)
                        this%gf(iBFace) = dot1/(dot1+dot2)
                    endif

                endif

            enddo

        enddo

    end subroutine computeInterpolationWeights

! *********************************************************************************************************************

    subroutine nonOrthogonalCorrection(this, iFace, Ef)

    !==================================================================================================================
    ! Description:
    !! nonOrthogonalCorrection returns one of the non-orthogolal decomposion method.
    !==================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace
    !------------------------------------------------------------------------------------------------------------------

        real :: dot, eCN(3), Ef(3)
    !------------------------------------------------------------------------------------------------------------------

        eCN = this%CN(iFace,:)/norm2(this%CN(iFace,:))

        ! Over relaxed approach
        if(flubioOptions%nonOrthMethod==0) then

            dot = dot_product(eCN,this%Sf(iFace,:))
            Ef = (norm2(this%Sf(iFace,:))**2/dot)*eCN

        ! Orhogonal correction approach
        elseif(flubioOptions%nonOrthMethod==1) then

            Ef = norm2(this%Sf(iFace,:))*eCN

        ! Minimum correction approach
        elseif(flubioOptions%nonOrthMethod==2) then

            dot = dot_product(eCN,this%Sf(iFace,:))
            Ef = dot*eCN

        ! Stabilised over-relaxed
        elseif(flubioOptions%nonOrthMethod==3) then

            dot = max(dot_product(eCN,this%Sf(iFace,:)), flubioOptions%sfactor)
            Ef = (norm2(this%Sf(iFace,:))**2/dot)*eCN

        else

            dot = dot_product(eCN,this%Sf(iFace,:))
            Ef = (norm2(this%Sf(iFace,:))**2/dot)*eCN

        endif

    end subroutine nonOrthogonalCorrection

! *********************************************************************************************************************

    subroutine pressureNonOrthogonalCorrection(this, iFace, Sf, Df)

    !==================================================================================================================
    ! Description:
    !! nonOrthogonalCorrection returns a non-orthogolal decomposion for the pressure-correction laplace operator.
    !==================================================================================================================

        use flubioDictionaries
    !------------------------------------------------------------------------------------------------------------------

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace
    !------------------------------------------------------------------------------------------------------------------

        real :: dot, dot1, Df, Sf(3), nf(3)
    !------------------------------------------------------------------------------------------------------------------

        ! Over relaxed approach
        if(flubioOptions%nonOrthMethod==0) then

            dot = dot_product(Sf,Sf)
            dot1 = dot_product(this%CN(iFace,:),Sf)
            Df = dot/dot1

        ! Orhogonal correction approach
        elseif(flubioOptions%nonOrthMethod==1) then

            dot = dot_product(this%CN(iFace,:),Sf)
            dot1 = dot_product(this%CN(iFace,:),this%CN(iFace,:))
            Df = sqrt(dot/dot1)

        ! Mimum correction approach
        elseif(flubioOptions%nonOrthMethod==2) then

            dot = dot_product(this%CN(iFace,:),Sf)
            dot1 = dot_product(this%CN(iFace,:),this%CN(iFace,:))
            Df = dot/dot1

        ! Stabilised over-relaxed
        elseif(flubioOptions%nonOrthMethod==3) then

            nf = Sf/norm2(Sf)
            dot = dot_product(nf, nf)
            dot1 = dot_product(this%CN(iFace,:), nf)
            Df = norm2(Sf)*dot/(max(dot1, flubioOptions%sfactor*norm2(this%CN(iFace,:))))

        else

            dot = dot_product(Sf,Sf)
            dot1 = dot_product(this%CN(iFace,:),Sf)
            Df = dot/dot1

        endif

    end subroutine pressureNonOrthogonalCorrection

! *********************************************************************************************************************

    subroutine setGlobalConn(this)

    !==================================================================================================================
    ! Description:
    !! setGlobalConn build the global connectivity table between the elements.
    !==================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iElement, iFace, iBFace, iOwner, iNeigh, iProc, pNeigh

        integer :: iOwnerNeighbourCoef, localNeigh, is ,ie
    !------------------------------------------------------------------------------------------------------------------

        !--------------------------!
        ! Build connectivity table !
        !--------------------------!

        do iElement=1,numberOfElements
            do iNeigh=1, this%iNeighbours(iElement)%csize
                if(this%iNeighbours(iElement)%col(iNeigh)>0) then
                    localNeigh = this%iNeighbours(iElement)%col(iNeigh)
                    this%conn(iElement)%col(iNeigh) = this%cellGlobalAddr(localNeigh)
                endif
            enddo

        enddo

        ! Extend to Processor boundary
        i = 0
        do iBoundary=1,numberOfProcBound

            iProc = this%boundaries%procBound(iBoundary)
            is = this%boundaries%startFace(iProc)
            ie = is+this%boundaries%nFace(iProc)-1

            pNeigh = 0
            i = i+1
            do iBFace=is,ie

                pNeigh = pNeigh+1

                iOwner = this%owner(iBFace)
                iOwnerNeighbourCoef = this%iOwnerNeighbourCoef(iBFace)

                this%conn(iOwner)%col(iOwnerNeighbourCoef) = this%neigh(pNeigh,i)
                this%iNeighbours(iOwner)%col(iOwnerNeighbourCoef) = this%neigh(pNeigh,i)

            enddo

        enddo

        ! Compute the number of non-zero entries related to this mesh
        call this%computeNonZeroEntries()

        ! Clean up
        deallocate(this%neigh)
        do iElement=1,numberOfElements
            call this%iNeighbours(iElement)%clearIntColumn()
        enddo
        deallocate(this%iNeighbours)

    end subroutine setGlobalConn

! *********************************************************************************************************************

    subroutine findNeighbours(this)

    !===================================================================================================================
    ! Description:
    !! findNeighbours finds the number of neighbours for each element composing the mesh.
    !===================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, iFace, iBFace, iBoundary, iProc, iOwn, iNeigh, pNeigh, kf, is ,ie
    !------------------------------------------------------------------------------------------------------------------

        integer :: nn, nFaces, numberOfLocalInteriorFaces

        integer, dimension(:), allocatable :: iFaces, iNeighbours
    !------------------------------------------------------------------------------------------------------------------

        ! Starting message
        call flubioMsg('FLUBIO: creating mesh connectivities...')

        !---------!
        ! Prepare !
        !---------!

        allocate(this%iOwnerNeighbourCoef(numberOfFaces))
        allocate(this%iNeighbourOwnerCoef(numberOfFaces))

        this%iOwnerNeighbourCoef = 0
        this%iNeighbourOwnerCoef = 0

        ! Loop over elements
        do iElement=1,numberOfElements

            nFaces = this%iFaces(iElement)%csize
            allocate(iFaces(nFaces))
            iFaces = this%iFaces(iElement)%col(1:nFaces)

            nn = this%iNeighbours(iElement)%csize
            allocate(iNeighbours(nn))

            iNeighbours = this%iNeighbours(iElement)%col(1:nn)
            kf = 1

            numberOfLocalInteriorFaces = numberOfElementFaces(iElement)

            do i=1, nn !numberOfLocalInteriorFaces

                ! Exclude processor faces
                if(iNeighbours(i)/=0) then
                    if(this%owner(iFaces(i))==iElement ) then
                        this%iOwnerNeighbourCoef(iFaces(i)) = kf
                    elseif(this%Neighbour(iFaces(i))==iElement ) then
                        this%iNeighbourOwnerCoef(iFaces(i)) = kf
                    endif
                endif

                kf = kf+1

            enddo

            deallocate(iNeighbours)
            deallocate(iFaces)

        enddo

        !------------------------------------------------------!
        ! Increase by 1 the neighbours at processor's boundary !
        !------------------------------------------------------!

        do iBoundary=1,numberOfProcBound

            iProc = this%boundaries%procBound(iBoundary)
            is = this%boundaries%startFace(iProc)
            ie = is+this%boundaries%nFace(iProc)-1

            do iBFace=is,ie

                call getNeighSize(this, kf, iBFace)

                this%iOwnerNeighbourCoef(iBFace) = kf+1
                iOwn = this%owner(iBFace)
                this%numberOfNeighbours(iOwn) = this%numberOfNeighbours(iOwn)+1

            enddo

        enddo

    end subroutine findNeighbours

! *********************************************************************************************************************

    function leastSquareMatrix(this) result(lsq_mat)

    !==================================================================================================================
    ! Description:
    !! leastSquareMatrix computes the least square matrix to be used in least squares gradient calculation or interolation.
    !==================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, is, ie

        integer :: pNeigh, iBoundary
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iFace, iBFace, iOwner, iNeighbour, iProc
    !------------------------------------------------------------------------------------------------------------------

        real :: lsq_mat(numberOfElements,6)
        !! least squares matrix

        real :: w
        !! LSQ weight

    !------------------------------------------------------------------------------------------------------------------

        lsq_mat = 0.0

        !----------------!
        ! Internal faces !
        !----------------!

        do iFace=1,numberOfIntFaces

            iOwner = this%owner(iFace)
            iNeighbour = this%neighbour(iFace)

            ! Compute weights
            w = 1./sqrt(this%CN(iFace,1)**2 + this%CN(iFace,2)**2 + this%CN(iFace,3)**2)

            ! Compute least square matrix (ordering: xx, yy, zz, xy, xz, yz)

            ! Owner cell
            lsq_mat(iOwner,1) = lsq_mat(iOwner,1) + w*this%CN(iFace,1)**2
            lsq_mat(iOwner,2) = lsq_mat(iOwner,2) + w*this%CN(iFace,2)**2
            lsq_mat(iOwner,3) = lsq_mat(iOwner,3) + w*this%CN(iFace,3)**2
            lsq_mat(iOwner,4) = lsq_mat(iOwner,4) + w*this%CN(iFace,1)*this%CN(iFace,2)
            lsq_mat(iOwner,5) = lsq_mat(iOwner,5) + w*this%CN(iFace,1)*this%CN(iFace,3)
            lsq_mat(iOwner,6) = lsq_mat(iOwner,6) + w*this%CN(iFace,2)*this%CN(iFace,3)

            ! Neighbour cell
            lsq_mat(iNeighbour,1) = lsq_mat(iNeighbour,1) + w*this%CN(iFace,1)**2
            lsq_mat(iNeighbour,2) = lsq_mat(iNeighbour,2) + w*this%CN(iFace,2)**2
            lsq_mat(iNeighbour,3) = lsq_mat(iNeighbour,3) + w*this%CN(iFace,3)**2
            lsq_mat(iNeighbour,4) = lsq_mat(iNeighbour,4) + w*this%CN(iFace,1)*this%CN(iFace,2)
            lsq_mat(iNeighbour,5) = lsq_mat(iNeighbour,5) + w*this%CN(iFace,1)*this%CN(iFace,3)
            lsq_mat(iNeighbour,6) = lsq_mat(iNeighbour,6) + w*this%CN(iFace,2)*this%CN(iFace,3)

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary=1,numberOfProcBound

            iProc=this%boundaries%procBound(iBoundary)
            is=this%boundaries%startFace(iProc)
            ie=is+this%boundaries%nFace(iProc)-1

            pNeigh=0

            do iBFace=is,ie

                pNeigh=pNeigh+1
                iOwner=this%owner(iBFace)

                ! Compute weights
                w = 1./sqrt(this%CN(iBFace,1)**2 + this%CN(iBFace,2)**2 + this%CN(iBFace,3)**2)

                ! compute least square matrix (ordering: xx, yy, zz, xy, xz, yz)
                lsq_mat(iOwner,1) = lsq_mat(iOwner,1) + w*this%CN(iBFace,1)**2
                lsq_mat(iOwner,2) = lsq_mat(iOwner,2) + w*this%CN(iBFace,2)**2
                lsq_mat(iOwner,3) = lsq_mat(iOwner,3) + w*this%CN(iBFace,3)**2
                lsq_mat(iOwner,4) = lsq_mat(iOwner,4) + w*this%CN(iBFace,1)*this%CN(iBFace,2)
                lsq_mat(iOwner,5) = lsq_mat(iOwner,5) + w*this%CN(iBFace,1)*this%CN(iBFace,3)
                lsq_mat(iOwner,6) = lsq_mat(iOwner,6) + w*this%CN(iBFace,2)*this%CN(iBFace,3)

            enddo

        enddo

        !-----------------!
        ! Real Boundaries !
        !-----------------!

        do iBoundary=1,this%boundaries%numberOfRealBound

            is=this%boundaries%startFace(iBoundary)
            ie=is+this%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie

                iOwner=this%owner(iBFace)

                ! Compute weights
                w = 1./sqrt(this%CN(iBFace,1)**2 + this%CN(iBFace,2)**2 + this%CN(iBFace,3)**2)

                ! compute least square matrix (ordering: xx, yy, zz, xy, xz, yz)
                lsq_mat(iOwner,1) = lsq_mat(iOwner,1) + w*this%CN(iBFace,1)**2
                lsq_mat(iOwner,2) = lsq_mat(iOwner,2) + w*this%CN(iBFace,2)**2
                lsq_mat(iOwner,3) = lsq_mat(iOwner,3) + w*this%CN(iBFace,3)**2
                lsq_mat(iOwner,4) = lsq_mat(iOwner,4) + w*this%CN(iBFace,1)*this%CN(iBFace,2)
                lsq_mat(iOwner,5) = lsq_mat(iOwner,5) + w*this%CN(iBFace,1)*this%CN(iBFace,3)
                lsq_mat(iOwner,6) = lsq_mat(iOwner,6) + w*this%CN(iBFace,2)*this%CN(iBFace,3)

            enddo

        enddo

    end function leastSquareMatrix

! *********************************************************************************************************************

    function faceNormals(this) result(normals)

    !==================================================================================================================
    ! Description:
    !! nf returns mesh normals as an array
    !==================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace
    !------------------------------------------------------------------------------------------------------------------

        real :: normals(numberOfElements+numberOfBFaces, 3)
    !------------------------------------------------------------------------------------------------------------------

        do iFace=1,numberOfFaces
            normals(iFace,1:3) = this%Sf(iFace,:)/norm2(this%Sf(iFace,:))
        enddo

    end function faceNormals

! *********************************************************************************************************************

    subroutine saveMesh(this)

    !==================================================================================================================
    ! Description:
    !! save the mesh data in a binary file.
    !==================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10) :: procID
        !! processor ID

        character(len=:), allocatable :: dirName
		!! folder to save the output
    !------------------------------------------------------------------------------------------------------------------

        integer :: m, n

        integer, dimension(:,:), allocatable :: faceVertices

        integer, dimension(:,:), allocatable :: elementFaces
    !------------------------------------------------------------------------------------------------------------------

        logical :: dirExists
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        n = maxval(numberOfFaceNodes)
        allocate(faceVertices(numberOfFaces, n))

        m = maxval(numberOfElementFaces)
        allocate(elementFaces(numberOfFaces, m))

        faceVertices = cellToIntArray(this%fvertex, numberOfFaces, n)
        elementFaces = cellToIntArray(this%iFaces, numberOfElements, m)

        ! Create bin directory if it does not exists
        dirName = 'grid/bin'
        inquire(file=trim(dirName)//'/.', exist=dirExists)

        if(.not. dirExists) call execute_command_line ('mkdir -p ' // adjustl(trim(dirName)))

        open(1,file='grid/bin/mesh-d'//trim(procID)//'.bin',form='unformatted')
            write(1) numberOfPoints, numberOfFaces, numberOfIntFaces, numberOfBFaces, numberOfElements, numberOfBElements
            write(1) numberOfBoundaries
            write(1) this%vertex
            write(1) faceVertices
            write(1) elementFaces
            write(1) this%centroid

            ! boundaries
            write(1) numberOfBoundaries,  this%boundaries%numberOfRealBound, this%boundaries%startFace, this%boundaries%nFace
        close(1)

        deallocate(faceVertices)

        call flubioMsg('FLUBIO: mesh imported successfully! ')

    end subroutine saveMesh

! *********************************************************************************************************************


    function buildNodesToElementConn(this) result(evertex)

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(iDynamicArray) :: ia

        type(flubioList), dimension(:), allocatable :: evertex
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, n, iNode, iFace, iElement
    !------------------------------------------------------------------------------------------------------------------

        integer, dimension(:), allocatable :: iFaces

        integer, dimension(:), allocatable :: listAsArray
    !------------------------------------------------------------------------------------------------------------------

        integer :: nFaces, targetFace
    !------------------------------------------------------------------------------------------------------------------

        call flubioMsg('FLUBIO: setting nodes-element connectivities...')

        allocate(evertex(numberOfElements))

        do iElement=1,numberOfElements

            nFaces = numberOfElementFaces(iElement)

            ia = iDynamicArray(1)
            do iFace=1,nFaces

                targetFace = this%iFaces(iElement)%col(iFace)

                do iNode=1,numberOfFaceNodes(targetFace)
                    call ia%append(this%fvertex(targetFace)%col(iNode))
                enddo

            enddo

            allocate(listAsArray(ia%size()))
            listAsArray(1:ia%size()) = ia%values(1:ia%size())
            call I_unista_org(listAsArray, n)

            call evertex(iElement)%initList(isize=n, intialValues=-1)
            call evertex(iElement)%setList(startIndex=1, targetColumn=1, direction=2, values=listAsArray(1:n), chunkSize=n)

            call ia%deallocate()
            deallocate(listAsArray)

        enddo

    end function buildNodesToElementConn

!**********************************************************************************************************************!
!						                         Auxiliary subroutines							                                 !
!**********************************************************************************************************************!

    subroutine getNeighSize(this, k, iBFace)

    !==================================================================================================================
    ! Description:
    !! getNeighSize finds out the number of neighbours sorrounding one target cell.
    !==================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, k, iFace, iBFace, iOwner
    !------------------------------------------------------------------------------------------------------------------

        iOwner=this%owner(iBFace)

        k=0
        do i=1, numberOfElementFaces(iOwner)
            iFace = this%iFaces(iOwner)%col(i)
            if(this%iOwnerNeighbourCoef(iFace)>0) k=k+1
        enddo

    end subroutine getNeighSize

!**********************************************************************************************************************

    subroutine mpi_create_ghosts_I(this)

    !==================================================================================================================
    ! Description:
    !! mpi_create_ghosts_I creates the ghost cells and connectivities between two partions.
    !==================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: ierr
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, j, iBoundary, iElement, iFace, iOwner, is, ie

        integer :: nbuf, patchFace, nFace
    !------------------------------------------------------------------------------------------------------------------

        integer :: iSend, iRecv, istat(2*numberOfProcBound1), ireq(2*numberOfProcBound1)

        integer :: nSend, nRecv, idr, istats(MPI_STATUS_SIZE)

        integer :: buffSize

        integer, dimension(:,:), allocatable :: sbufn, rbufn

        real, dimension(:,:,:), allocatable :: sbuf, rbuf
    !------------------------------------------------------------------------------------------------------------------

        ! Starting message
        call flubioMsg('FLUBIO: creating the ghosts cells at the processor boundaries...')

        ! ---------------------------------------------------------!
        ! Send and Receive the cell center of neighbour processors !
        ! ---------------------------------------------------------!

        ! Prepare the send buffers
        buffSize = this%boundaries%maxProcFaces

        allocate(sbuf(buffSize,3,numberOfProcBound1))
        allocate(rbuf(buffSize,3,numberOfProcBound1))

        sbuf = 0.0
        rbuf = 0.0

        do iBoundary=1,numberOfProcBound

            i = this%boundaries%procBound(iBoundary)
            is = this%boundaries%startFace(i)
            ie = is+this%boundaries%nFace(i)-1

            ! Fill the send buffer
            nbuf = 0

            do iFace=is,ie
                nbuf = nbuf+1
                iOwner = this%owner(iFace)
                sbuf(nbuf,:,iBoundary) = this%centroid(:,iOwner)
            enddo

        enddo

        ! Perform the comunication between the processors
        nSend = numberOfProcBound
        nRecv = numberOfProcBound

        ! Open senders
        do iSend=1,nSend

            i = this%boundaries%procBound(iSend)
            idr = this%boundaries%neighProc(i)

            buffSize = this%boundaries%maxProcFaces*3
            call mpi_isend(sbuf(1:this%boundaries%maxProcFaces,:,iSend),buffSize,MPI_REAL8,idr,0,MPI_COMM_WORLD,ireq(iSend),ierr)

        enddo

        ! Open receivers
        do iRecv=1,nRecv

            i = this%boundaries%procBound(iRecv)
            idr = this%boundaries%neighProc(i)

            buffSize = this%boundaries%maxProcFaces*3
            call mpi_irecv(rbuf(1:this%boundaries%maxProcFaces,:,iRecv),buffSize,MPI_REAL8,idr,0,MPI_COMM_WORLD,ireq(nSend+iRecv),ierr)

        enddo

        ! Wait the end of the communications
        call mpi_waitAll(nSend+nRecv,ireq,MPI_STATUSES_IGNORE,ierr)

        ! Fill the space at the end of the centroid vector
        do iBoundary=1,numberOfProcBound

            i = this%boundaries%procBound(iBoundary)
            is = this%boundaries%startFace(i)
            ie = is+this%boundaries%nFace(i)-1
            patchFace = numberOfElements + (is-numberOfIntFaces-1)

            ! Fill the send buffer
            nbuf = 0
            do iFace=is,ie
                nbuf = nbuf+1
                this%centroid(:,patchFace+nbuf) = rbuf(nbuf,:,iBoundary)
            enddo

        enddo

        ! Wait all the processor before advancing
        call mpi_barrier(MPI_COMM_WORLD, ierr)

        deallocate(sbuf)
        deallocate(rbuf)

        !------------------------------------------------------------------!
        ! Send and Receive the cell global address of neighbour processors !
        !------------------------------------------------------------------!

        allocate(this%neigh(this%boundaries%maxProcFaces,numberOfProcBound1))
        this%neigh = 0

        ! Prepare the send buffers
        buffSize = this%boundaries%maxProcFaces

        allocate(sbufn(buffSize,numberOfProcBound1))
        allocate(rbufn(buffSize,numberOfProcBound1))

        sbufn = 0
        rbufn = 0

        do iBoundary=1,numberOfProcBound

            i = this%boundaries%procBound(iBoundary)
            is = this%boundaries%startFace(i)
            ie = is+this%boundaries%nFace(i)-1

            ! Fill the send buffer
            nbuf = 0

            do iFace=is,ie
                nbuf = nbuf+1
                iOwner = this%owner(iFace)
                sbufn(nbuf,iBoundary) = this%cellGlobalAddr(iOwner)
            enddo

        enddo

        ! Open senders
        do iSend=1,nSend

            i = this%boundaries%procBound(iSend)
            idr = this%boundaries%neighProc(i)

            buffSize = this%boundaries%maxProcFaces
            call mpi_isend(sbufn(1:this%boundaries%maxProcFaces,iSend),buffSize,MPI_INT,idr,0,MPI_COMM_WORLD,ireq(iSend),ierr)

        enddo

        ! Open receivers
        do iRecv=1,nRecv

            i = this%boundaries%procBound(iRecv)
            idr = this%boundaries%neighProc(i)

            buffSize = this%boundaries%maxProcFaces
            call mpi_irecv(rbufn(1:this%boundaries%maxProcFaces,iRecv),buffSize,MPI_INT,idr,0,MPI_COMM_WORLD,ireq(nSend+iRecv),ierr)

        enddo

        ! Wait the end of the communications
        call mpi_waitAll(nSend+nRecv,ireq,MPI_STATUSES_IGNORE,ierr)

        this%neigh = rbufn

        ! Deallocate buffers
        deallocate(sbufn)
        deallocate(rbufn)

        ! Wait all the processor before advancing
        call mpi_barrier(MPI_COMM_WORLD, ierr)

    end subroutine mpi_create_ghosts_I

!**********************************************************************************************************************

    subroutine updateCentroidBoundaryMesh(this)

    !==================================================================================================================
    ! Description:
    !! updateCentroidBoundaryMesh sets the centroid for i>numberOfElements. This is done to allow the mesh kd-tree
    !! to pick up boundary face centers (for sampling) and ghost cells (for ibm).
    !==================================================================================================================

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iBFace, is, ie, iB, iRank, pNeigh, numberOfNeighbours
    !------------------------------------------------------------------------------------------------------------------

        ! Real boundaries
        iB = 0
        do iBoundary=1,numberOfRealBound

            is = this%boundaries%startFace(iBoundary)
            ie = is+this%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie
                iB = iB+1
                this%centroid(1,numberOfElements+iB) = this%fcentroid(iBFace,1)
                this%centroid(2,numberOfElements+iB) = this%fcentroid(iBFace,2)
                this%centroid(3,numberOfElements+iB) = this%fcentroid(iBFace,3)
            end do

        end do

    end subroutine updateCentroidBoundaryMesh

! *********************************************************************************************************************

    subroutine createMeshSearchTree(this)

        implicit none

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i
    !------------------------------------------------------------------------------------------------------------------

        call flubioMsg('FLUBIO: creating mesh kd-tree...')

        ! Create processor boundary map
        call this%boundaries%getProcessorBoundaryMap()

        ! Create tree
        if(flubioOptions%kdtreeType==0) then
            this%tree = KdTree(x=this%centroid(1,:), y=this%centroid(2,:), z=this%centroid(3,:))
        elseif(flubioOptions%kdtreeType==2) then
            this%tree_mk2 => kdtree2_create(this%centroid, rearrange=.true., sort=.true.)
        else
            this%tree_mk => create_tree(this%centroid)
        endif

    end subroutine createMeshSearchTree

! *********************************************************************************************************************

    function findPointOwnership(this, point, radius) result(index)

    !==================================================================================================================
    ! Description:
    !! findPointOwnership runs the post processing task for the target line probing object.
    !==================================================================================================================

        class(fvMesh) :: this

        type(KdTreeSearch) :: search

        type(dArgDynamicArray) :: nearestNeighbour

        type(kdtree2_result), dimension(:), allocatable :: idx
    !------------------------------------------------------------------------------------------------------------------

        integer :: index

        integer :: nFound

        integer :: nSearch
    !------------------------------------------------------------------------------------------------------------------

        real :: point(3)

        real , optional :: radius

        real :: distances(1)
    !------------------------------------------------------------------------------------------------------------------

        if(flubioOptions%kdtreeType==0) then

            if(present(radius)) then
                nearestNeighbour = search%kNearest(this%tree, &
                                                x=this%centroid(1,:), y=this%centroid(2,:), z=this%centroid(3,:), &
                                                xQuery=point(1), yQuery=point(2), zQuery=point(3), k=1, radius=radius)
            else
                nearestNeighbour = search%kNearest(this%tree, &
                                                x=this%centroid(1,:), y=this%centroid(2,:), z=this%centroid(3,:), &
                                                xQuery=point(1), yQuery=point(2), zQuery=point(3), k=1)
            endif

            if(nearestNeighbour%i%values(1)>numberOfElements+sum(this%boundaries%nFace(1:numberOfRealBound))) then
                index = -1
            else
                index = nearestNeighbour%i%values(1)
            end if

        elseif(flubioOptions%kdtreeType==2) then
            this%centroid(:, numberOfElements+numberOfBFaces+1) = point

            if(present(radius)) then

                ! Be conservative, kdtree2 cannot quite play with dynamic array
                if(bdim==1) then
                    nSearch = 4*15
                else
                    nSearch = 4*35
                endif

                allocate(idx(nSearch))
                call kdtree2_r_nearest_around_point(this%tree_mk2, numberOfElements+numberOfBFaces+1, nSearch, radius**2, nFound, nSearch, idx)

            else
                allocate(idx(1))
                call kdtree2_n_nearest_around_point(this%tree_mk2, numberOfElements+numberOfBFaces+1, 1, 1, idx)
            endif

            if(idx(1)%idx>numberOfElements+sum(this%boundaries%nFace(1:numberOfRealBound))) then
                index = -1
            else
                index = idx(1)%idx
            end if

            deallocate(idx)
        endif

    end function findPointOwnership

!**********************************************************************************************************************

    subroutine findBoundaryPoints(this)

        use m_refsor
        use m_unista

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=10) :: procID
        !! processor ID as string

        character(len=:), allocatable :: dirName
        !! directory name to save the data
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iBFace, is, ie, isp, iep
        !! loop indices

        integer :: psize(numberOfBoundaries), bPointsSize(numberOfBoundaries), nEntries(numberOfBoundaries)

        integer, dimension(:,:), allocatable :: mapper
        !! point mapper

        integer, dimension(:,:), allocatable :: allPoints
        !! all the boundary points

        integer, dimension(:,:), allocatable :: bPoints
        !! boundary points
    !------------------------------------------------------------------------------------------------------------------

        real, dimension(:,:,:), allocatable :: bVertex
        !! boundary vertices
    !------------------------------------------------------------------------------------------------------------------

        logical :: dirExists
    !------------------------------------------------------------------------------------------------------------------

        call flubioMsg('FLUBIO: collecting boundary points...')

        allocate(allPoints(maxval(this%boundaries%nFace)*maxval(numberOfFaceNodes), numberOfBoundaries))
        allocate(bPoints(maxval(this%boundaries%nFace)*maxval(numberOfFaceNodes), numberOfBoundaries))
        allocate(bVertex(numberOfPoints, 3, numberOfBoundaries))
        allocate(mapper(maxval(this%boundaries%nFace)*maxval(numberOfFaceNodes), numberOfBoundaries))

        ! Build the point List
        allPoints = 2000000000
        mapper = -1
        pSize = 0

        do iBoundary=1,numberOfBoundaries

            is = this%boundaries%startFace(iBoundary)
            ie = is + this%boundaries%nFace(iBoundary)-1

            isp = 0
            iep = 0

            nEntries(iBoundary) = 0

            do iBFace=is,ie

                isp = iep+1
                iep = isp+numberOfFaceNodes(iBFace)-1

                pSize(iBoundary) = pSize(iBoundary) + numberOfFaceNodes(iBFace)
                allPoints(isp:iep,iBoundary) = this%fvertex(iBFace)%col(1:numberOfFaceNodes(iBFace))
                nEntries(iBoundary) = nEntries(iBoundary) + numberOfFaceNodes(iBFace)

            enddo

            call unista(allPoints(:,iBoundary), bPointsSize(iBoundary),  mapper(:,iBoundary))
            call refsor(allPoints(1:bPointsSize(iBoundary),iBoundary))

            if(nEntries(iBoundary) /= maxval(this%boundaries%nFace)*maxval(numberOfFaceNodes)) bPointsSize(iBoundary) = bPointsSize(iBoundary) - 1

            ! Get the needed points from the list
            do i=1,bPointsSize(iBoundary)
                bVertex(i,:,iBoundary) = this%vertex(allPoints(i,iBoundary),:)
            enddo

        enddo

        ! Create bin directory if it does not exists
        dirName = 'grid/bin'
        inquire(file=trim(dirName)//'/.', exist=dirExists)

        if(.not. dirExists) call execute_command_line ('mkdir -p ' // adjustl(trim(dirName)))

        ! Save decomposed boundary mesh
        write(procID,'(i0)') id
        open(1,file='grid/bin/boundaries-d'//trim(procID)//'.bin',form='unformatted')
            write(1) numberOfBoundaries, numberOfPoints, numberOfFaces, &
                    maxval(numberOfFaceNodes), maxval(this%boundaries%nFace)*maxval(numberOfFaceNodes)
            write(1) this%boundaries%startFace, this%boundaries%nFace, pSize, nEntries
            write(1) this%boundaries%userName
            write(1) allPoints
            write(1) mapper
            write(1) allPoints
            write(1) bPointsSize
            write(1) bVertex
            write(1) numberOfFaceNodes
        close(1)

        ! Ending message
        call flubioMsg('FLUBIO: Boundary points found!')

    end subroutine findBoundaryPoints

!**********************************************************************************************************************

    subroutine getFacesAndOffsets(this, faces, facesOffsets)

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(iDynamicArray) :: listOfFaces
    !------------------------------------------------------------------------------------------------------------------

        integer :: iNode, iFace, iElement

        integer :: nFaces, nNodes, nOffsets, targetFace, targetNode

        integer :: facesOffsets(numberOfElements)

        integer, dimension(:), allocatable :: faces, faceNodes, mapper
    !------------------------------------------------------------------------------------------------------------------

        real :: nf(3)

        real, dimension(:,:), allocatable :: points
    !------------------------------------------------------------------------------------------------------------------

        call flubioMsg("FLUBIO: build face connectivities and orient face points...")

        listOfFaces = iDynamicArray(1)

        ! Loop over elements
        do iElement=1,numberOfElements

            if(iElement==1) then
                facesOffsets(iElement) = 1
            else
                facesOffsets(iElement) = facesOffsets(iElement-1) + 1
            endif

            call listOfFaces%append(numberOfElementFaces(iElement))

            ! Loop over element faces
            do iFace=1,numberOfElementFaces(iElement)

                targetFace = this%iFaces(iElement)%col(iFace)

                ! Get face nodes
                nNodes = this%fvertex(targetFace)%csize

                allocate(faceNodes(nNodes))
                faceNodes = this%fvertex(targetFace)%getIntColumn(nNodes)

                allocate(points(nNodes,3))
                do iNode=1,nNodes
                    points(iNode,1:3) = this%vertex(faceNodes(iNode),1:3)
                enddo

                call listOfFaces%append(nNodes)
                facesOffsets(iElement) = facesOffsets(iElement) + 1

                ! Sort face nodes and append them
                nf = this%Sf(targetFace,1:3)/norm2(this%Sf(targetFace,1:3))
                mapper = orientPointsInPlane(points, nf, this%fcentroid(targetFace, 1:3), 'ascending', nNodes)

                ! Add vertices in the correct order
                do iNode=1,nNodes
                    call listOfFaces%append(faceNodes(mapper(iNode))-1)
                    facesOffsets(iElement) = facesOffsets(iElement) + 1
                enddo

                deallocate(faceNodes)
                deallocate(points)

            enddo

        enddo

        ! Transform into array
        allocate(faces(listOfFaces%size()))
        faces = listOfFaces%values(1:listOfFaces%size())

        ! Clean up
        call listOfFaces%deallocate()

    end subroutine getFacesAndOffsets

!**********************************************************************************************************************

    function orientPointsInPlane(points, normal, center, mode, np) result(sortedPointsMap)

        use math

        character(len=*) :: mode
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, j, np

        integer, dimension(:), allocatable :: sortedPointsMap
    !------------------------------------------------------------------------------------------------------------------

        real :: greatest

        real :: ex(3), ey(3), ez(3)

        real :: exn(3,3), mags(3)

        real :: p(3), q(3), normal(3), center(3), r(3), v(3), t, u

        real :: points(np,3)

        real :: theta(np)
    !------------------------------------------------------------------------------------------------------------------

        ! Prepare
        ex = (/1.0, 0.0, 0.0/)
        ey = (/0.0, 1.0, 0.0/)
        ez = (/0.0, 0.0, 1.0/)

        allocate(sortedPointsMap(np))
        sortedPointsMap = -1

        ! Get the max among the dot products between e and n
        call cross_prod(ex, normal, exn(1,:))
        call cross_prod(ey, normal, exn(2,:))
        call cross_prod(ez, normal, exn(3,:))

        call mag(exn(1,:), mags(1))
        call mag(exn(2,:), mags(2))
        call mag(exn(3,:), mags(3))

        greatest = mags(1)
        j = 1
        do i = 2,3
            if (mags(i) > greatest) then
                j = i
            endif
        enddo

        p = exn(j,:)
        call cross_prod(normal,p,q)

        do i=1,np

            r = points(i,1:3) - center

            call cross_prod(r,p,v)
            t = dot_product(normal,v)

            call cross_prod(r,q,v)
            u = dot_product(normal,v)

            theta(i) = atan2(u,t)

        enddo

        ! Sort the points in ascendig order (counter-clockwise angles)
        sortedPointsMap = sortArray(theta, np, mode)

    end function orientPointsInPlane

end module flubioMesh
