!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

    subroutine initialisePoissonSolver

    !==================================================================================================================
    ! Description:
    !! initialisePoissonSolver allocates dynamically the memory space and initializes the variables for the Poisson solver.
    !==================================================================================================================

        use bcDicts
        use fieldvar
        use gradient
        use physicalConstants
        use transportEquation
        use runTimeTasks
        use equationRegistry

        implicit none

        character(len=:), allocatable :: ksp_name

        integer :: iElement, iTask

        integer :: numberOfDerivedFields

        integer, dimension(:), allocatable :: derivedFieldPos
    !------------------------------------------------------------------------------------------------------------------

        !--------------------------------!
        ! Initialise boundary conditions !
        !--------------------------------!

        call setScalarTransportBcList()

        call removeFieldsList()

        !-----------------!
        ! Transport Field !
        !-----------------!

        call readPhysicalConstantsPoisson()

        call T%setUpField('T', 'scalar', 1)

        ! physical variables
        call nu%setUpCoeff('nu', appendToList=.false., defaultValue=dens)
        call rho%setUpCoeff('rho', appendToList=.false., defaultValue=viscos)

        ! face velocity and mass fluxes, they will be zero, but bcs needs mass fluxes
        allocate(vf(numberOfFaces,3))
        allocate(mf(numberOfFaces,1))
        vf = 0.0
        mf = 0.0

        ! Read field from file
        if(flubioControls%restFrom/=-1) then
            call loadTimeQuantities()
            call nu%readFromFile(flubioControls%restFrom)
            call rho%readFromFile(flubioControls%restFrom)
        endif

        ! compute field gradient
        call computeGradient(T)

        ! write fields to check them
        call T%writeToFile()

        !------------------!
        ! Poisson equation !
        !------------------!

        call Teqn%createEq('Teqn', T%nComp)

        ksp_name = trim(Teqn%eqName)

        call Teqn%setEqOptionsWithoutConvection(ksp_name)

        ! initialise source terms if any
        if(flubioSources%sourcesFound) call Teqn%setEquationSources()

        !---------------------------!
        ! initialise run time tasks !
        !---------------------------!

        call setMonitorsList()

        !----------------!
        ! Field registry !
        !----------------!

        numberOfRegisteredFields = 3
        numberOfDerivedFields = getNumberOfFieldTasks()
        derivedFieldPos = getFieldTasksPosition()
        allocate(fieldRegistry(numberOfRegisteredFields + numberOfDerivedFields))

        fieldRegistry(1)%field => T
        fieldRegistry(2)%field => rho
        fieldRegistry(3)%field => nu

        iTask = 0
        do iElement=numberOfRegisteredFields+1,numberOfRegisteredFields+numberOfDerivedFields
            iTask = iTask + 1
            fieldRegistry(iElement)%field => runTimeObjects(derivedFieldPos(iTask))%task%taskField
        enddo

        ! create all the tasks
        call initialiseTasks()

        !-------------------!
        ! Equation registry !
        !-------------------!

        allocate(eqnRegistry(1))
        eqnRegistry(1)%eqn => Teqn


    end subroutine initialisePoissonSolver

! *********************************************************************************************************************

    subroutine initialisePotentialSolver

    !==================================================================================================================
    ! Description:
    !! initialisePotentialSolver initializes the variables for the potential flow solver.
    !==================================================================================================================

        use bcDicts
        use fieldvar
        use gradient
        use physicalConstants
        use transportEquation
        use runTimeTasks
        use equationRegistry

        implicit none

        character(len=:), allocatable :: ksp_name

        integer :: iElement, iTask

        integer :: numberOfDerivedFields

        integer, dimension(:), allocatable :: derivedFieldPos
    !------------------------------------------------------------------------------------------------------------------

        !--------------------------------!
        ! Initialise boundary conditions !
        !--------------------------------!

        call setScalarTransportBcList()
        call setMomentumBcList()
        call setPressureBcList()
        call removeFieldsList()

        !-----------------!
        ! Transport Field !
        !-----------------!

        call readPhysicalConstantsPotential()

        ! Potential field
        call T%setUpField(fieldName='T', classType='scalar', nComp=1)

        ! Velocity field
        call velocity%setUpField(fieldName='U,V,W', classType='momentum', nComp=3)

        ! Pressure field
        call pressure%setUpField(fieldName='P', classType='pressure', nComp=1)

        ! Physical variables
        call rho%setUpCoeff('rho', appendToList=.false.,  defaultValue=dens)
        call nu%setUpCoeff('nu', appendToList=.false., defaultValue=viscos)

        ! Face velocity and mass fluxes
        allocate(vf(numberOfFaces,3))
        allocate(mf(numberOfFaces,1))
        vf = 0.0
        mf = 0.0

        ! Read field from file
        if(flubioControls%restFrom/=-1) then
            call loadTimeQuantities()
            call nu%readFromFile(flubioControls%restFrom)
            call rho%readFromFile(flubioControls%restFrom)
        endif

        ! Compute field gradient
        call computeGradient(T)

        ! Write fields to check them
        call T%writeToFile()

        !------------------!
        ! Laplace equation !
        !------------------!

        call Teqn%createEq('Teqn', T%nComp)
        ksp_name = trim(Teqn%eqName)
        call Teqn%setEqOptionsWithoutConvection(ksp_name)

        !---------------------------!
        ! initialise run time tasks !
        !---------------------------!

        call setMonitorsList()

        !----------------!
        ! Field registry !
        !----------------!

        numberOfRegisteredFields = 4
        numberOfDerivedFields = getNumberOfFieldTasks()
        derivedFieldPos = getFieldTasksPosition()
        allocate(fieldRegistry(numberOfRegisteredFields + numberOfDerivedFields))

        fieldRegistry(1)%field => T
        fieldRegistry(2)%field => rho
        fieldRegistry(3)%field => nu
        fieldRegistry(4)%field => velocity

        iTask = 0
        do iElement=numberOfRegisteredFields+1,numberOfRegisteredFields+numberOfDerivedFields
            iTask = iTask + 1
            fieldRegistry(iElement)%field => runTimeObjects(derivedFieldPos(iTask))%task%taskField
        enddo

        ! Create all the tasks
        call initialiseTasks()

        !-------------------!
        ! Equation registry !
        !-------------------!
        allocate(eqnRegistry(1))
        eqnRegistry(1)%eqn => Teqn

    end subroutine initialisePotentialSolver

! *********************************************************************************************************************

    subroutine initialiseAdvectionDiffusionSolver

    !==================================================================================================================
    ! Description:
    !! initialiseAdvectionDiffusionSolver allocates dynamically the memory space and initializes the variables
    !! for the advection-diffusion solver.
    !==================================================================================================================

        use bcDicts
        use fieldvar
        use gradient
        use physicalConstants
        use transportEquation
        use runTimeTasks
        use equationRegistry

        implicit none

        character(len=:), allocatable :: ksp_name
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iTask

        integer :: numberOfDerivedFields

        integer, dimension(:), allocatable :: derivedFieldPos
    !------------------------------------------------------------------------------------------------------------------

        !--------------------------------!
        ! Initialise boundary conditions !
        !--------------------------------!

        call setScalarTransportBcList()
        call setMomentumBcList()

        call removeFieldsList()

        !-----------------!
        ! Transport Field !
        !-----------------!

        call readPhysicalConstantsPoisson()

        call T%setUpField('T', 'scalar', 1)
        call velocity%setUpField('U,V,W', 'momentum', 3)

        ! physical variables
        call rho%setUpCoeff('rho', appendToList=.false., defaultValue=dens)
        call nu%setUpCoeff('nu', appendToList=.false., defaultValue=viscos)

        ! face velocity and mass fluxes
        allocate(vf(numberOfFaces,3))
        allocate(mf(numberOfFaces,1))
        vf = 0.0
        mf = 0.0

        ! compute mass fluxes
        call computeMassFluxesNoRC()

        ! Read field from file
        if(flubioControls%restFrom/=-1) then
            call loadTimeQuantities()
            call velocity%readFromFile(flubioControls%restFrom)
            call nu%readFromFile(flubioControls%restFrom)
            call rho%readFromFile(flubioControls%restFrom)
            call readMassFluxesFromFile(flubioControls%restFrom)
        endif

        ! Compute field gradient
        call computeGradient(T)

        ! Write fields to check them
        call T%writeToFile()
        call velocity%writeToFile()

        !------------------------------!
        ! Advection-Diffusion equation !
        !------------------------------!

        call Teqn%createEq('Teqn', T%nComp)
        ksp_name = trim(Teqn%eqName)
        call Teqn%setEqOptions(ksp_name)

        if(flubioSources%sourcesFound) call Teqn%setEquationSources()

        !---------------------------!
        ! initialise run time tasks !
        !---------------------------!

        call setMonitorsList()

        !----------------!
        ! Field registry !
        !----------------!

        numberOfRegisteredFields = 4
        numberOfDerivedFields = getNumberOfFieldTasks()
        derivedFieldPos = getFieldTasksPosition()
        allocate(fieldRegistry(numberOfRegisteredFields + numberOfDerivedFields))

        fieldRegistry(1)%field => T
        fieldRegistry(2)%field => rho
        fieldRegistry(3)%field => nu
        fieldRegistry(4)%field => velocity

        iTask = 0
        do iElement=numberOfRegisteredFields+1,numberOfRegisteredFields+numberOfDerivedFields
            iTask = iTask + 1
            fieldRegistry(iElement)%field => runTimeObjects(derivedFieldPos(iTask))%task%taskField
        enddo

        ! Create all the tasks
        call initialiseTasks()

        !-------------------!
        ! Equation registry !
        !-------------------!
        allocate(eqnRegistry(1))
        eqnRegistry(1)%eqn => Teqn

    end subroutine initialiseAdvectionDiffusionSolver

! *********************************************************************************************************************

    subroutine initialiseBurgersSolver

    !==================================================================================================================
    ! Description:
    !! initialiseAdvectionDiffusionSolver allocates dynamically the memory space and initializes the variables
    !! for the advection-diffusion solver.
    !==================================================================================================================

        use bcDicts
        use fieldvar
        use gradient
        use physicalConstants
        use transportEquation
        use runTimeTasks
        use equationRegistry

        implicit none

        character(len=:), allocatable :: ksp_name
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iTask

        integer :: numberOfDerivedFields

        integer, dimension(:), allocatable :: derivedFieldPos
    !------------------------------------------------------------------------------------------------------------------

        !--------------------------------!
        ! Initialise boundary conditions !
        !--------------------------------!

        call setScalarTransportBcList()
        call removeFieldsList()

        !-----------------!
        ! Transport Field !
        !-----------------!

        call readPhysicalConstantsPoisson()

        ! This is a vector equation, but classType=scalar just points to a set of boundary conditions
        call T%setUpField('U,V,W', 'scalar', 3)

        ! physical variables
        call rho%setUpCoeff('rho', appendToList=.false., defaultValue=dens)
        call nu%setUpCoeff('nu', appendToList=.false., defaultValue=viscos)

        ! face velocity and mass fluxes
        allocate(vf(numberOfFaces,3))
        allocate(mf(numberOfFaces,1))
        vf = 0.0
        mf = 0.0

        ! Read field from file
        if(flubioControls%restFrom/=-1) then
            call loadTimeQuantities()
            call T%readFromFile(flubioControls%restFrom)
            call nu%readFromFile(flubioControls%restFrom)
            call rho%readFromFile(flubioControls%restFrom)
        endif

        ! Compute field gradient
        call computeGradient(T)

        ! Write fields to check them
        call T%writeToFile()

        !-----------------!
        ! Burges equation !
        !-----------------!

        call Teqn%createEq('Teqn', T%nComp)
        ksp_name = trim(Teqn%eqName)
        call Teqn%setEqOptions(ksp_name)

        !---------------------------!
        ! initialise run time tasks !
        !---------------------------!

        call setMonitorsList()

        !----------------!
        ! Field registry !
        !----------------!

        numberOfRegisteredFields = 4
        numberOfDerivedFields = getNumberOfFieldTasks()
        derivedFieldPos = getFieldTasksPosition()
        allocate(fieldRegistry(numberOfRegisteredFields + numberOfDerivedFields))

        fieldRegistry(1)%field => T
        fieldRegistry(2)%field => rho
        fieldRegistry(3)%field => nu
        fieldRegistry(4)%field => velocity

        iTask = 0
        do iElement=numberOfRegisteredFields+1,numberOfRegisteredFields+numberOfDerivedFields
            iTask = iTask + 1
            fieldRegistry(iElement)%field => runTimeObjects(derivedFieldPos(iTask))%task%taskField
        enddo

        ! Create All the tasks
        call initialiseTasks()

        !-------------------!
        ! Equation registry !
        !-------------------!
        allocate(eqnRegistry(1))
        eqnRegistry(1)%eqn => Teqn


    end subroutine initialiseBurgersSolver

! *********************************************************************************************************************

    subroutine initialiseNavierStokesSolver

    !==================================================================================================================
    ! Description:
    !! initialiseNavierStokesSolver allocates initializes the data structure for the Navier-Stokes solver.
    !==================================================================================================================

        use bcDicts
        use fieldvar
        use gradient
        use physicalConstants
        use momentum
        use pressureCorrection
        use turbulenceModels
        use energyEq
        use runTimeTasks
        use equationRegistry

        implicit none

        character(len=:), allocatable :: ksp_name

        integer :: ierr

        integer :: iElement, iTask

        integer :: numberOfDerivedFields

        integer, dimension(:), allocatable :: derivedFieldPos

    !------------------------------------------------------------------------------------------------------------------

        call flubioMsg('FLUBIO: initialising fields...')

        !--------------------------------!
        ! Initialise boundary conditions !
        !--------------------------------!

        call setMomentumBcList()
        call setPressureBcList()
        call setScalarTransportBcList()
        call setTurbulentBcList()

        call removeFieldsList()

        !--------!
        ! Fields !
        !--------!

        ! Read physical constants from the dictionary
        call readPhysicalConstants()

        ! Face velocity and mass fluxes
        allocate(vf(numberOfFaces,3))
        allocate(mf(numberOfFaces,1))
        vf = 0.0
        mf = 0.0

        ! Primary variables
        call velocity%setUpField('U,V,W', 'momentum', 3)
        call pressure%setUpField('P', 'pressure', 1)
        call pcorr%copyField('pcorr', 1, pressure)

        ! Physical variables
        call rho%setUpCoeff('rho', appendToList=.false., defaultValue=dens)
        call nu%setUpCoeff('nu', appendToList=.false., defaultValue=viscos)

        ! Compute mass fluxes
        call computeMassFluxesNoRC()

        ! Update field registry
        numberOfRegisteredFields = 5

        ! Read field from file
        if(flubioControls%restFrom/=-1) then
            call loadTimeQuantities()
            call velocity%readFromFile(flubioControls%restFrom)
            call pcorr%readFromFile(flubioControls%restFrom)
            call pressure%readFromFile(flubioControls%restFrom)
            call nu%readFromFile(flubioControls%restFrom)
            call rho%readFromFile(flubioControls%restFrom)
            call readMassFluxesFromFile(flubioControls%restFrom)
        endif

        ! Check continuity in steady state
        if(steadySim==1) call checkContinuity()

        ! Compute field gradients
        call computeGradient(velocity)
        call computeGradient(pressure)
        call computeGradient(pcorr)

        call flubioMsg('FLUBIO: initialising equations...')

        !-------------------!
        ! momentum equation !
        !-------------------!

        call Ueqn%createEq('Ueqn', velocity%nComp)
        ksp_name = trim(Ueqn%eqName) // '_'
        call Ueqn%setEqOptions(ksp_name)

        ! Initialise source terms if any
        if(flubioSources%sourcesFound) call Ueqn%setEquationSources()

        !-------------------!
        ! pressure equation !
        !-------------------!

        call Peqn%createEq('Peqn', pressure%nComp)
        ksp_name = trim(Peqn%eqName) // '_'
        call Peqn%setPressureEqOptions(ksp_name)

        !------------------!
        ! Turbulence model !
        !------------------!

        call flubioMsg('FLUBIO: initialising the turbulence model...')

        ! DNS/RANS/LES
        call turbModelSelector()
        call trb%createModel()
        if(flubioTurbModel%lesModel/=-1) call les_trb%modelConstants()

        !-----------------!
        ! Energy equation !
        !-----------------!

        if(flubioControls%activateEnergyEquation==1) call createEnergyEquation()

        !----------------!
        ! Field registry !
        !----------------!

        call setMonitorsList()

        numberOfRegisteredFields = 5

        if(lowercase(flubioTurbModel%modelName)=='spalartallmaras') then
            numberOfRegisteredFields = numberOfRegisteredFields + 1
        elseif(lowercase(flubioTurbModel%modelName)=='dns') then
            ! do nothing
        else
            numberOfRegisteredFields = numberOfRegisteredFields + 2
        end if

        numberOfDerivedFields = getNumberOfFieldTasks()
        derivedFieldPos = getFieldTasksPosition()
        allocate(fieldRegistry(numberOfRegisteredFields + numberOfDerivedFields))

        fieldRegistry(1)%field => velocity
        fieldRegistry(2)%field => pressure
        fieldRegistry(3)%field => pcorr
        fieldRegistry(4)%field => rho
        fieldRegistry(5)%field => nu

        if(lowercase(flubioTurbModel%modelName)=='spalartallmaras') then
            fieldRegistry(6)%field => trb%tdrField
        elseif(lowercase(flubioTurbModel%modelName)=='dns') then
            ! do nothing
        else
            fieldRegistry(6)%field => trb%tkeField
            fieldRegistry(7)%field => trb%tdrField
        end if

        ! Additional fields
        iTask = 0
        do iElement=numberOfRegisteredFields+1,numberOfRegisteredFields+numberOfDerivedFields
            iTask = iTask + 1
            fieldRegistry(iElement)%field => runTimeObjects(derivedFieldPos(iTask))%task%taskField
        enddo

        !---------------------------!
        ! Initialise run time tasks !
        !---------------------------!

        call flubioMsg('FLUBIO: initialising monitors...')

        call initialiseTasks()

        !-------------------!
        ! Equation registry !
        !-------------------!

        allocate(eqnRegistry(3))
        eqnRegistry(1)%eqn => Ueqn
        eqnRegistry(2)%eqn => Peqn
        eqnRegistry(3)%eqn => Eeqn

    end subroutine initialiseNavierStokesSolver

! *********************************************************************************************************************

    subroutine initialiseFSSolver

    !==================================================================================================================
    ! Description:
    !! initialiseFSSolver allocates initializes the data structure for the Navier-Stokes solver.
    !==================================================================================================================

        use bcDicts
        use fieldvar
        use gradient
        use physicalConstants
        use momentum
        use pressureCorrection
        use turbulenceModels
        use runTimeTasks
        use equationRegistry

        implicit none

        character(len=:), allocatable :: ksp_name

        integer :: ierr

        integer :: iElement, iTask

        integer :: numberOfDerivedFields

        integer, dimension(:), allocatable :: derivedFieldPos

    !------------------------------------------------------------------------------------------------------------------

        call flubioMsg('FLUBIO: initialising fields...')

        !--------------------------------!
        ! Initialise boundary conditions !
        !--------------------------------!

        call setMomentumBcList()
        call setPressureBcList()
        call setScalarTransportBcList()
        call setTurbulentBcList()

        call removeFieldsList()

        !--------!
        ! Fields !
        !--------!

        ! read physical constants from the dictionary
        call readPhysicalConstants()

        ! face velocity and mass fluxes
        allocate(vf(numberOfFaces,3))
        allocate(mf(numberOfFaces,1))
        vf = 0.0
        mf = 0.0

        ! Primary variables
        call velocity%setUpField('U,V,W', 'momentum', 3)
        call pressure%setUpField('P', 'pressure', 1)
        call pcorr%copyField('P', 1, pressure)

        ! Physical variables
        call rho%setUpCoeff('rho', appendToList=.false., defaultValue=dens)
        call nu%setUpCoeff('nu', appendToList=.false., defaultValue=viscos)

        ! Compute mass fluxes
        call computeMassFluxesNoRC()

        ! Read field from file
        if(flubioControls%restFrom/=-1) then
            call loadTimeQuantities()
            call velocity%readFromFile(flubioControls%restFrom)
            call pcorr%readFromFile(flubioControls%restFrom)
            call pressure%readFromFile(flubioControls%restFrom)
            call nu%readFromFile(flubioControls%restFrom)
            call rho%readFromFile(flubioControls%restFrom)
            call readMassFluxesFromFile(flubioControls%restFrom)
        endif

        ! Compute field gradients
        call computeGradient(velocity)
        call computeGradient(pressure)
        call computeGradient(pcorr)

        call flubioMsg('FLUBIO: initialising equations...')

        !-------------------!
        ! momentum equation !
        !-------------------!

        Ueqn%eqName = 'Ueqn'
        Ueqn%nComp = velocity%nComp
        ksp_name = trim(Ueqn%eqName) // '_'

        call Ueqn%setNumericalOptions()

        ! Allocate store arrays for ARK3 (Default method) and RK3SSP
        allocate(Ueqn%K_1(numberOfElements,velocity%nComp))
        allocate(Ueqn%K_2(numberOfElements,velocity%nComp))
        Ueqn%K_1 = 0.0
        Ueqn%K_2 = 0.0

        ! Create the equation if an implicit method is used
        if(lower(Ueqn%timeOptName)=='soue' .or. lower(Ueqn%timeOptName)=='impliciteuler') then
            call Ueqn%setEqOptions(ksp_name)
            call Ueqn%createEq(ksp_name, velocity%nComp)
        endif

        ! Initialise source terms if any
        if(flubioSources%sourcesFound) call Ueqn%setEquationSources()

        !-------------------!
        ! pressure equation !
        !-------------------!

        call Peqn%createEq('Peqn', pressure%nComp)
        ksp_name = trim(Peqn%eqName) // '_'
        call Peqn%setPressureEqOptions(ksp_name)

        !------------------!
        ! Turbulence model !
        !------------------!

        call flubioMsg('FLUBIO: initialising the turbulence model...')

        ! DNS/RANS/LES
        call turbModelSelector()
        call trb%createModel()
        if(flubioTurbModel%lesModel/=-1) call les_trb%modelConstants()

        !---------------------------!
        ! initialise run time tasks !
        !---------------------------!

        call setMonitorsList()

        !----------------!
        ! Field registry !
        !----------------!

        numberOfRegisteredFields = 4

        if(lowercase(flubioTurbModel%modelName)=='spalartallmaras') then
            numberOfRegisteredFields = numberOfRegisteredFields + 1
        elseif(lowercase(flubioTurbModel%modelName)=='dns') then
            ! do nothing
        else
            numberOfRegisteredFields = numberOfRegisteredFields + 2
        end if

        numberOfDerivedFields = getNumberOfFieldTasks()
        derivedFieldPos = getFieldTasksPosition()
        allocate(fieldRegistry(numberOfRegisteredFields + numberOfDerivedFields))

        fieldRegistry(1)%field => velocity
        fieldRegistry(2)%field => pressure
        fieldRegistry(3)%field => rho
        fieldRegistry(4)%field => nu

        if(lowercase(flubioTurbModel%modelName)=='spalartallmaras') then
            fieldRegistry(5)%field => trb%tdrField
        elseif(lowercase(flubioTurbModel%modelName)=='dns') then
            ! do nothing
        else
            fieldRegistry(5)%field => trb%tkeField
            fieldRegistry(6)%field => trb%tdrField
        end if

        ! Additional fields
        iTask = 0
        do iElement=numberOfRegisteredFields+1,numberOfRegisteredFields+numberOfDerivedFields
            iTask = iTask + 1
            fieldRegistry(iElement)%field => runTimeObjects(derivedFieldPos(iTask))%task%taskField
        enddo

        ! Create all the tasks
        call initialiseTasks()

        !-------------------!
        ! Equation registry !
        !-------------------!

        allocate(eqnRegistry(2))
        eqnRegistry(1)%eqn => Ueqn
        eqnRegistry(2)%eqn => Peqn

    end subroutine initialiseFSSolver

! *********************************************************************************************************************

    subroutine saveTimeQuantities

    !==================================================================================================================
    ! Description:
    !! saveTimeQuantities dumps the current time, iteration and time step sizes to file.
    !==================================================================================================================

        use globalTimeVar
        use pressureCorrection
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: postDir
        !! post processing directory

        character(len=:), allocatable :: fieldDir
        !! field directory

        character(len=10) :: timeStamp
    !------------------------------------------------------------------------------------------------------------------

        postDir='postProc/fields/'
        write(timeStamp,'(i0)') itime
        fieldDir = postDir//trim(timeStamp)//'/time.bin'

        if(id==0) then
            ! write current time, iteration and time steps
            open(1,file=fieldDir, form='unformatted')
                write(1) itime, time
                write(1) dtime, dtime0, dtime00
                if(steadySim==1) write(1) Peqn%res5
            close(1)
        end if

    end subroutine saveTimeQuantities

! *********************************************************************************************************************

    subroutine loadTimeQuantities

    !==================================================================================================================
    ! Description:
    !! loadTimeQuantities loads the current time, iteration and time step sizes to file.
    !==================================================================================================================

        use globalTimeVar
        use pressureCorrection
    !-------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: postDir
        !! post processing directory

        character(len=:), allocatable :: fieldDir
        !! field directory

        character(len=10) :: timeStamp
        !! Current time
    !------------------------------------------------------------------------------------------------------------------

        postDir='postProc/fields/'
        write(timeStamp,'(i0)') flubioControls%restFrom
        fieldDir = postDir//trim(timeStamp)//'/time.bin'

        open(1,file=fieldDir, form='unformatted')
            read(1) itime, time
            read(1) dtime, dtime0, dtime00
            if(steadySim==1) read(1) Peqn%res5
        close(1)

    end subroutine loadTimeQuantities

! *********************************************************************************************************************

    subroutine deleteTimeFolders

        use flubioDictionaries

        implicit none

        character(len=:), allocatable :: postDir
        !! post processing directory

        character(len=:), allocatable :: fieldDir
        !! field directory

        character(len=20) :: timeStamp
        !! time stamp

        character(len=20) ::  procID
        !! processor ID
    !------------------------------------------------------------------------------------------------------------------

        logical :: dirExists
        !! boolean to check if the directory exists
    !------------------------------------------------------------------------------------------------------------------

        integer :: checkStart
        !! start time to remove time folders

        integer :: removeTime
        !! time folder to be removed
    !------------------------------------------------------------------------------------------------------------------

        if(flubioControls%nflds > 0 .or. flubioControls%iflds/=-1) then

            write(procID,'(i0)') id

            postDir='postProc/fields/'

            ! Delete folder not to keep
            checkStart = flubioControls%iflds*flubioControls%nflds
            if(itime > checkStart+1) then

                removeTime = itime - flubioControls%iflds*flubioControls%nflds
                write(timeStamp,'(i0)') removeTime

                fieldDir = postDir//trim(timeStamp)//'/'

                inquire(file=trim(fieldDir)//'/.', exist=dirExists)

                if(dirExists .and. id==0) call execute_command_line ('rm -r '//adjustl(trim(fieldDir)))

            endif

        endif

        end subroutine deleteTimeFolders

! *********************************************************************************************************************
