!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

program extractBlock

    use serialmeshvar

    implicit none

    character*15, dimension(:), allocatable :: args

    character(len=25) :: targetBoundaries
!------------------------------------------------------------------------------------------------------------------

    integer :: nid, fnumber, startField, endField, skipField, dataForm, nargs

    integer :: ni, nj, nk

    integer :: numberOfNeighbours

    integer :: iTime

!------------------------------------------------------------------------------------------------------------------

    real :: xmin, xmax

    real :: ymin, ymax

    real :: zmin, zmax

!------------------------------------------------------------------------------------------------------------------

    !------------------------!
    ! Command line arguments !
    !------------------------!

    nargs = command_argument_count()

    if(nargs>0) then
        allocate(args(nargs))
        call extractBlockParsing(args, nargs, xmin, xmax, ni, ymin, ymax, nj, zmin, zmax, nk, numberOfNeighbours, iTime, targetBoundaries)
    endif

    !------------------!
    ! read serial mesh !
    !------------------!

    call mesh%readMeshFromFiles()

    !------------------!
    ! Write data files !
    !------------------!

    call extractBlockFromMesh(xmin, xmax, ni, ymin, ymax, nj, zmin, zmax, nk, numberOfNeighbours, iTime, targetBoundaries)

contains

! *********************************************************************************************************************

    subroutine extractBlockFromMesh(xmin, xmax, ni, ymin, ymax, nj, zmin, zmax, nk, numberOfNeighbours, iTime, targetBoundaries)

        use kdtree2_module
        use serialmeshvar
        use penf
        use vtk_fortran, only : vtk_file

        implicit none

        character(len=10) :: timeStamp

        character(len=15) fieldName

        character(len=:), allocatable :: file_name

        character(*) :: targetBoundaries
    !------------------------------------------------------------------------------------------------------------------

        type(vtk_file) :: vtk_output_file

        type(kdtree2), pointer :: tree
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, j, k, l, n

        integer :: iBoundary, iBFace, is, ie, iB

        integer :: iComp, nComp

        integer :: ni, nj, nk

        integer :: totSizeU

        integer:: iTime

        integer :: numberOfNeighbours

        integer :: ierr

        integer :: isOut
    !------------------------------------------------------------------------------------------------------------------

        real :: cellCenters(3, numberOfElements+numberOfBFaces+1)

        real :: xmin, xmax

        real :: ymin, ymax

        real :: zmin, zmax

        real :: x(ni)

        real :: y(nj)

        real :: z(nk)

        real :: xn(ni,nj,nk)

        real :: yn(ni,nj,nk)

        real :: zn(ni,nj,nk)

        real :: dx, dy, dz

        real :: fieldValues(numberOfNeighbours, 3)

        real :: dist(numberOfNeighbours)

        real :: U(numberOfElements+numberOfBFaces,3), Ublock(ni,nj,nk,3), ui(3)
    !------------------------------------------------------------------------------------------------------------------

        type(kdtree2_result), allocatable :: nearestNeighbours(:)
    !------------------------------------------------------------------------------------------------------------------

        isOut=0

        write(timeStamp,'(i0)') iTime

        totSizeU = numberOfElements + numberOfBFaces
        dx=1.0
        dy=1.0
        dz=1.0

        if(ni>1) dx = (xmax-xmin)/real(ni-1)
        if(nj>1) dy = (ymax-ymin)/real(nj-1)
        if(nk>1) dz = (zmax-zmin)/real(nk-1)

        x = linspace(xmin, xmax, ni)
        y = linspace(ymin, ymax, nj)
        z = linspace(zmin, zmax, nk)

        ! Centroids
        cellCenters(1,1:numberOfElements) = mesh%centroid(1,1:numberOfElements)
        cellCenters(2,1:numberOfElements) = mesh%centroid(2,1:numberOfElements)
        cellCenters(3,1:numberOfElements) = mesh%centroid(3,1:numberOfElements)

        ! Boundaries
        iB=0
        do iBoundary=1,numberOfBoundaries

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie
                iB=iB+1
                cellCenters(1,numberOfElements+iB) = mesh%fcentroid(iBFace,1)
                cellCenters(2,numberOfElements+iB) = mesh%fcentroid(iBFace,2)
                cellCenters(3,numberOfElements+iB) = mesh%fcentroid(iBFace,3)
            end do

        end do

        ! Read velocity field
         file_name = 'postProc/fields/'//trim(timeStamp)//'/U-reconstructed.bin'
        open(1,file=file_name, form='unformatted')
            do iComp=1,3
                read(1) U(:,iComp)
            enddo
        close(1)

        write(*,*) 'Min/Max u-velocity: ', minval(U(:,1)), maxval(U(:,1))

        ! number of neighbours to look for, put this as input please
        allocate(nearestNeighbours(numberOfNeighbours))

        ! Create the tree, ask for internally rearranged data for speed,
        ! and for output sorted by increasing distance from the query vector
        tree => kdtree2_create(cellCenters, rearrange=.true., sort=.true.)

        ! Now look for the nearest neigbours
        write(*,*) 'FLUBIO: interpolating field onto the new grid points...'
        l=0
        do i=1,ni
            do j=1,nj
                do k=1,nk

                    l=l+1

                    ! Override the last cell center and look for tis nieghbours
                    cellCenters(1,totSizeU+1) = x(i)
                    cellCenters(2,totSizeU+1) = y(j)
                    cellCenters(3,totSizeU+1) = z(k)

                    ! Check if the point is outside the internal mesh
                    isOut = checkPoint(targetBoundaries, cellCenters(1:3,totSizeU+1))

                    write(*,*) 'point is:', cellCenters(1:3,totSizeU+1), isOut

                    if(isOut==0) then

                        call kdtree2_n_nearest_around_point(tree, totSizeU+1, numberOfNeighbours, numberOfNeighbours, nearestNeighbours)

                        ! Interpolate from neighbours to point
                        do n = 1,numberOfNeighbours
                            fieldValues(n,:) = U(nearestNeighbours(n)%idx,:)
                            dist(n) = sqrt(nearestNeighbours(n)%dis)
                        end do

                        ui = interpolate(fieldValues, dist, numberOfNeighbours, 3, 2.0)
                    else
                        ui = 0.0
                        isOut = 0
                    end if

                    Ublock(i,j,k,1) = ui(1)
                    Ublock(i,j,k,2) = ui(2)
                    Ublock(i,j,k,3) = ui(3)

                end do
            end do
        end do

        !-------------------------------!
        ! Write the block to vtk binary !
        !-------------------------------!

        ! create the grid
        xn = ndGrid(x, 1, ni, nj, nk, ni)
        yn = ndGrid(y, 2, ni, nj, nk, nj)
        zn = ndGrid(z, 3, ni, nj, nk, nk)

        ierr = vtk_output_file%initialize(format='binary', filename='postProc/VTKfields/block.vts', mesh_topology='StructuredGrid', &
                nx1=1, nx2=ni, ny1=1, ny2=nj, nz1=1, nz2=nk)

        ierr = vtk_output_file%xml_writer%write_piece(nx1=1, nx2=ni, ny1=1, ny2=nj, nz1=1, nz2=nk)

        n=ni*nj*nk
        ierr = vtk_output_file%xml_writer%write_geo(n=n, x=xn, y=yn, z=zn)
        ierr = vtk_output_file%xml_writer%write_dataarray(location='node', action='open')
        ierr = vtk_output_file%xml_writer%write_dataarray(data_name='Ui', x=Ublock(1:ni,1:nj,1:nk,1), &
                                                          y=Ublock(1:ni,1:nj,1:nk,2), z=Ublock(1:ni,1:nj,1:nk,3))
        ierr = vtk_output_file%xml_writer%write_dataarray(location='node', action='close')
        ierr = vtk_output_file%xml_writer%write_piece()

        ierr = vtk_output_file%finalize()

        ! Save a binary file to be used later

        open(1, file='postProc/fields/Ublock.bin', form='unformatted')
            write(1) ni, nj, nk
            write(1) xmin, ymin, zmin
            write(1) dx, dy, dz
            write(1) Ublock(1:ni,1:nj,1:nk,1)
            write(1) Ublock(1:ni,1:nj,1:nk,2)
            write(1) Ublock(1:ni,1:nj,1:nk,3)
        close(1)

        write(*,*) ' '
        write(*,*) 'FLUBIO: block file has been written!'

    end subroutine extractBlockFromMesh

! *********************************************************************************************************************

    function linspace(a, b, N)  result(x)

        implicit none

        integer :: i

        integer :: N
    !--------------------------------------------------------------------------------------------------------------------

        real :: a

        real :: b

        real :: x(N)
    !--------------------------------------------------------------------------------------------------------------------

        if(N==1) then

            x(1) = a

        else

            do i = 1, N
                x(i) = a + (b-a)*real(i-1)/real(N-1)
            end do

        end if

    end function linspace

! *********************************************************************************************************************

    function ndGrid(coords, dir, ni, nj, nk, N) result(X)

        integer :: i, j, k

        integer :: ni, nj, nk, N

        integer :: dir
    !--------------------------------------------------------------------------------------------------------------------

        real :: coords(N)

        real :: X(ni,nj,nk)

        real :: val
    !--------------------------------------------------------------------------------------------------------------------

        X=0.0

        if(dir==1) then

            do k=1,nk
                do j=1,nj
                    do i=1,ni
                        X(i,j,k) = coords(i)
                    end do
                end do
            end do

        elseif(dir==2) then

            do k=1,nk
                do j=1,nj
                    do i=1,ni
                        X(i,j,k) = coords(j)
                    end do
                end do
            end do

        elseif(dir==3) then

            do k=1,nk
                do j=1,nj
                    do i=1,ni
                        X(i,j,k) = coords(k)
                    end do
                end do
            end do


        else
            write(*,*) 'You cannot exceed 3 dimensions'
            stop
        end if

    end function ndGrid

! *********************************************************************************************************************

    subroutine extractBlockParsing(args, nargs, xmin, xmax, ni, ymin, ymax, nj, zmin, zmax, nk, numberOfNeighbours, iTime, targetBoundaries)

        implicit none

        character(len=25) :: targetBoundaries

        integer :: p, nargs, nid, stat

        integer :: ni, nj, nk

        integer :: iTime

        integer :: numberOfNeighbours

        real :: xmin, xmax

        real :: ymin, ymax

        real :: zmin, zmax

        character(*) :: args(nargs)
    !------------------------------------------------------------------------------------------------------------------

        ! get the arguments

        do p=1,nargs
            call get_command_argument(number=p, value=args(p), status=stat)
        enddo

        ! proces the options
        do p=1,nargs,4

            ! look box bounds
            if(args(p)=='-xbounds') then

                read(args(p+1),*) xmin
                read(args(p+2),*) xmax
                read(args(p+3),*) ni

            elseif(args(p)=='-ybounds') then

                read(args(p+1),*) ymin
                read(args(p+2),*) ymax
                read(args(p+3),*) nj

            elseif(args(p)=='-zbounds') then

                read(args(p+1),*) zmin
                read(args(p+2),*) zmax
                read(args(p+3),*) nk

            elseif(args(p)=='-nn') then

                read(args(p+1),*) numberOfNeighbours

            elseif(args(p)=='-help') then

                write(*,*) 'Command line options:'
                write(*,*) 'xbounds: minimum, maximum and number of points in x direction'
                write(*,*) 'ybounds: minimum, maximum and number of points in y direction'
                write(*,*) 'zbounds: minimum, maximum and number of points in z direction'
                write(*,*) 'nn: number of neighbours too seek'
                write(*,*) 'time: time to pick'
                stop

            endif

        enddo

        ! Read nn and time
        do p=1,nargs

            if(args(p)=='-nn') then
                read(args(p+1),*) numberOfNeighbours

            elseif(args(p)=='-time') then
                read(args(p+1),*) iTime

            elseif(args(p)=='-checkBounds') then
                read(args(p+1),*) targetBoundaries
            end if

        end do

    end subroutine extractBlockParsing

! *********************************************************************************************************************

    function interpolate(neigbours, dist, nn, nComp, deg) result(phi_i)

        implicit none

        integer :: n

        integer :: nn

        integer :: ncomp
    !--------------------------------------------------------------------------------------------------------------------

        real :: neigbours(nn,nComp)

        real :: dist(nn)

        real :: phi_i(nComp)

        real :: w(nn)

        real :: deg
    !--------------------------------------------------------------------------------------------------------------------

        phi_i = 0.0

        w = 1.0/(dist**deg)
        phi_i(1) = sum(neigbours(:,1)*w)/sum(w)
        phi_i(2) = sum(neigbours(:,2)*w)/sum(w)
        phi_i(3) = sum(neigbours(:,3)*w)/sum(w)

    ! equivalent form
    !    do n=1,nn
    !        w(n) = 1.0/(dist(n)**deg)
    !        phi_i(1) = phi_i(1) + neigbours(n,1)*w(n)
    !        phi_i(2) = phi_i(1) + neigbours(n,2)*w(n)
    !        phi_i(3) = phi_i(1) + neigbours(n,3)*w(n)
    !    end do
    !    phi_i = phi_i/sum(w)

    end function interpolate

! *********************************************************************************************************************

    function checkPoint(targetBoundaries, p) result(isOut)

        use m_strings

        implicit none

        character(len=25) :: targetBoundaries

        character(len=100), allocatable :: split_array(:)

        integer :: ii, jj

        integer :: isOut

        integer iBoundary, iBFace, isb, ieb

        integer, dimension(:), allocatable :: is, ie
    !------------------------------------------------------------------------------------------------------------------

        real :: p(3)

        real :: pCf(3)

        real :: n(3), nb(3)

        real :: dot
    !------------------------------------------------------------------------------------------------------------------

        isOut=0

        ! Split the arary of boundary by comma
        call split_in_array(targetBoundaries, split_array)

        allocate(is(size(split_array)))
        allocate(ie(size(split_array)))

        ii = 0
        do iBoundary=1,numberOfBoundaries
            do jj=1,size(split_array)
                if(mesh%boundaries%userName(iBoundary)==adjustl(trim(split_array(jj))))then
                    ii = ii+1
                    is(ii) = mesh%boundaries%startFace(iBoundary)
                    ie(ii) = is(ii)+mesh%boundaries%nFace(iBoundary)-1
                end if
            end do
        end do

        ! Check normal orientation. If the dot product is positive then the point is outised the domain, else keep looping
        do iBoundary=1,size(split_array)

            isb = is(iBoundary)
            ieb = ie(iBoundary)

            do iBFace=isb,ieb

                pCf = mesh%fcentroid(iBFace,:) - p

                n = pCf/sqrt(pCf(1)**2+pCf(2)**2+pCf(3)**2)
                nb = mesh%Sf(iBFace,:)/norm2(mesh%Sf(iBFace,:))

                dot = dot_product(n, nb)

                if(dot>0) then
                    isOut=0
                    exit
                else
                    isOut=1
                end if

                ! If a point is discarded for one boundary, it must be discarded for all the others
                if(isOut==1) exit

            end do

        end do

    end function checkPoint

! *********************************************************************************************************************

end program extractBlock
