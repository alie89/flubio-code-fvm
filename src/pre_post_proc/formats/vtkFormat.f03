!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____ 	        !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|	        !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |     	        !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |     	        !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____ 	        !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|	        !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

	module serialVTKFormat

		use flubioMpi, only: flubioMsg, flubioStopMsg

	contains 

	subroutine processDataVTK(nranks, is, ie, iStep, fields, fmt)

		use penf
		use vtk_fortran, only : vtk_file

		implicit none

		type(vtk_file) :: vtk_field_file
	!------------------------------------------------------------------------------------------------------------------

		character(len=10) :: file_number
		!! file number as string

		character(len=*) :: fields
		!! list of fields to process as comma separated string

		character(len=*) :: fmt
		!! output format (binary or ascii)

		character(len=:), allocatable :: fname
		!! file name
	!------------------------------------------------------------------------------------------------------------------

		integer :: nranks
		!! number of processor used in the decomposed case
		
		integer :: fnumber
		!! file number as integer
		
		integer :: ierr
		!! error flag

		integer :: is, ie, iStep
		!! loop indices
	!------------------------------------------------------------------------------------------------------------------

		do fnumber=is,ie,iStep

			write(file_number, '(i0)') fnumber
			fname='postProc/VTKfields/flubioField-'//trim(file_number)//'.vtu'

			call flubioMsg('FLUBIO: processing field number '//trim(file_number))
	
			ierr = vtk_field_file%initialize(format=fmt, filename=fname, mesh_topology='UnstructuredGrid')
			call writeCoordinatesVTK(vtk_field_file)

			call writeFieldsVTK(vtk_field_file, fnumber, nranks, fields)

			ierr = vtk_field_file%xml_writer%write_dataarray(location='cell', action='close')
			ierr = vtk_field_file%xml_writer%write_piece()
			ierr = vtk_field_file%finalize()

			call flubioMsg('FLUBIO: field number '//trim(file_number)// ' has been written!')

		end do

	end subroutine processDataVTK

! *********************************************************************************************************************

	subroutine writeCoordinatesVTK(vtk_field_file)

		use serialmeshvar
		use penf
		use vtk_fortran, only : vtk_file

		implicit none

		type(vtk_file) :: vtk_field_file

		type(flubioList), dimension(:), allocatable :: evertex
		!! list of vertices composing a cell
	!------------------------------------------------------------------------------------------------------------------

		integer :: i, n, is, ie
		!! loop indices

		integer :: ierr
		!! error flag

		integer :: numberOfElemNodes
		!! number of nodes composing each element

		integer :: connSize
		!! connectivity size

		integer(I1P) :: cell_type(numberOfElements)
		!!cell types

		integer(I4P) :: offset(numberOfElements)
		!! nodes offset

		integer(I4P), dimension(:), allocatable :: connect
		!! connectivities

		integer(I4P) :: faceOffsets(numberOfElements)
		!! face offsets

		integer(I4P), dimension(:), allocatable :: faces
		!! faces composing each

		integer(I4P), dimension(:), allocatable :: vrt
		!! nodes composing each element
	!------------------------------------------------------------------------------------------------------------------

		! Get the element connectivities
        evertex = mesh%buildNodesToElementConn()

		! Compute connectivity size
		connSize = 0
		do i=1,numberOfElements
			n = evertex(i)%getUnrolledListSize()
			connSize = connSize + n
		enddo		 

		allocate(connect(connSize))
		connect=-1

		! Cell types and offsets
		offset = 0
		ie = 0

		do i=1,numberOfElements

			! Set cell types
			cell_type(i) = 42
			numberOfElemNodes = evertex(i)%getListSize(direction=2)

			! Set offsets
			is = ie+1
			ie = is+numberOfElemNodes-1

			if(i==1) then
				offset(i) = numberOfElemNodes
			else
				offset(i) = offset(i-1) + numberOfElemNodes
			end if

			! Get connectivities
			allocate(vrt(numberOfElemNodes))
			vrt = evertex(i)%getList(is=1, ie=numberOfElemNodes, targetColumn=1, isReal=0)
			connect(is:ie) = vrt-1

			! Deallocate vrt
			deallocate(vrt)

		enddo

		! Sizes
		ierr = vtk_field_file%xml_writer%write_piece(np=numberOfPoints, nc=numberOfElements)

		! Points
		ierr = vtk_field_file%xml_writer%write_geo(np=numberOfPoints,   &
												   nc=numberOfElements, &
				     					           x=mesh%vertex(:,1),  &
											       y=mesh%vertex(:,2),  &
												   z=mesh%vertex(:,3))

		! Faces compising polyhedrons							   												
		call mesh%getFacesAndOffsets(faces, faceOffsets)	
		
		! Set connectivities
		ierr = vtk_field_file%xml_writer%write_connectivity(nc=numberOfElements,  &
				                                            connectivity=connect, &
														    offset=offset,        &
															cell_type=cell_type, &
															face=faces, &
															faceoffset=faceOffsets)
		! Open cell dataset
		ierr = vtk_field_file%xml_writer%write_dataarray(location='cell', action='open')		

	end subroutine writeCoordinatesVTK

! *********************************************************************************************************************

	subroutine writeFieldsVTK(vtk_field_file, targetTime, nranks, fields)

		use serialmeshvar
		use m_strings
		use penf
		use vtk_fortran, only : vtk_file
		use fileHandling

		implicit none

		type(vtk_file) :: vtk_field_file
	!------------------------------------------------------------------------------------------------------------------

		character(len=*) :: fields
		!! fields to process coming as a comma separated string

		character(len=10) :: procID
		!! processor ID

		character(len=10) :: timeStamp	
		!! time to process as string

		character(len=15) :: fieldName
		!! field name as string

		character(len=10), dimension(:), allocatable :: fieldList
		!! list of fields to process

		character(len=:), allocatable :: file_name
		!! file name
	!------------------------------------------------------------------------------------------------------------------

		integer :: np(nranks)
		!! number of points in each processor's domain

		integer :: nbf(nranks) 
		!! number of boundary faces in each processor's domain

		integer :: ne(nranks)
		!! number of elements in each processor's domain

		integer :: nf(nranks)
		!! number of faces in each processor's domain

		integer :: nif(nranks)
		!! number of internal faces in each processor's domain

		integer :: nbe(nranks)
		!! number of boundary elements in each processor's domain

		integer :: nb(nranks)
		!! number of boundaries

		integer :: nrb(nranks)
		!! number of non-processor boundaries (real boudnaries)

		integer, dimension(:,:), allocatable :: startFace
		!! start face of each boundary for each processor's domain
		
		integer, dimension(:,:), allocatable :: nFaces
	    !! start face of each boundary for each processor's domain

		integer :: targetTime
		!! tagert time to process

		integer :: i, ii, j, isb, ieb, iRank, iElement, iBoundary, iBFace, iComp, iField, patchFaceB, patchFaceE
		!! loop index variables
		
		integer :: nranks
		!! number of processors used in the simulation 

		integer :: ierr
		!! error flag

		integer :: nComp
		!! number of components

		integer :: nFields 
		!! number of fields

		integer, dimension(:), allocatable :: local2global
		!! mapping between local and global index for mesh elements

		integer, dimension(:), allocatable :: local2globalF
		!! mapping between local and global index for mesh faces
	!------------------------------------------------------------------------------------------------------------------

		real, dimension(:,:), allocatable :: field
		!! global field 
		
		real, dimension(:,:), allocatable :: phi
		!! local field
	!------------------------------------------------------------------------------------------------------------------

		!! Time to read
		write(timeStamp,'(i0)') targetTime

		! Read the number of boundaries from the decomposed mesh files
		call getNumberOfBoundaries(nb, nrb, nranks)

		! Allocate start and number of faces
		allocate(startFace(maxval(nb),nranks))
		allocate(nFaces(maxval(nb),nranks))
		startFace = -1
		nFaces = -1

		!------------------------------!
		! Read mesh and boundary files !
		!------------------------------!

		do ii=1,nranks
			iRank = ii-1
			write(procID,'(i0)') iRank
			call getParallelMeshSizes(procID, nb(ii), &
						              np(ii), nf(ii), nif(ii), nbf(ii), ne(ii), nbe(ii), &
									  startFace(1:nb(ii),ii), nFaces(1:nb(ii),ii))
		enddo
		
		! Find out the field to read 
		fieldList = getFieldList(fields)
		nFields = size(fieldList)

		! Get the global cell numbering
		allocate(local2global(numberOfElements))
		call globalConnPre(local2global, numberOfElements, nranks)

		! Loop over fields
		do iField=1,nFields

			call flubioMsg('FLUBIO: reconstructing '//trim(fieldList(iField))//'...')
		
			! Loop over partitions
			patchFaceE=0
			do ii=1,nranks

				! Initialize arrays
				iRank = ii-1
				write(procID,'(i0)') iRank

				call readListFromFile(fileName='grid/processor'//trim(procID)//'/faceProcAddressing', returnList=local2globalF)

				file_name = 'postProc/fields/'//trim(timeStamp)//'/'//trim(fieldList(iField))//'-d'//trim(procID)//'.bin'
				open(1, file=file_name, form='unformatted')

					 ! Read fields for the i-th partion
					 read(1) nComp
					 read(1) fieldName

					 ! Allocate field array
					 allocate(phi(ne(ii)+nbf(ii),nComp))
					 phi = 0.0

					! Allocate reconstructed field
					 if(ii==1) then
						 allocate(field(numberOfElements + numberOfBFaces,nComp))
						 field = 0.0
					 end if

					 ! Read fields components
					 do iComp=1,nComp
						 read(1) phi(:,iComp)
						 read(1)
						 read(1)
					 enddo

				close(1)

				! Reconstruct the global internal field
				do iElement=1,ne(ii)
					patchFaceE = patchFaceE+1
					field(local2global(patchFaceE),1:nComp) = phi(iElement,1:nComp)
				enddo

				! Reconstruct in the global boundary field
				patchFaceB = ne(ii)
				do iBoundary=1,nrb(ii)
					isb = startFace(iBoundary,ii)
					ieb = isb+nFaces(iBoundary,ii)-1
					do iBFace=isb,ieb
						patchFaceB = patchFaceB+1
						j = numberOfElements + local2globalF(iBFace) - numberOfIntFaces
						field(j,:) = phi(patchFaceB,:)
					enddo
				enddo

				! Deallocate and reallocate with a new size on the partition
				deallocate(phi)
				deallocate(local2globalF)
			
			! End of partition
			end do

			! Write Field
			if(nComp==1) then
				ierr = vtk_field_file%xml_writer%write_dataarray(data_name=trim(fieldName), x=field(1:numberOfElements,1))
			else
				ierr = vtk_field_file%xml_writer%write_dataarray(data_name=trim(fieldName), &
																     x=field(1:numberOfElements,1), &
						                                             y=field(1:numberOfElements,2), &
																     z=field(1:numberOfElements,3))
			end if

			! Deallocate current field
			deallocate(field)

		! End of fields
		end do

	end subroutine writeFieldsVTK

! ********************************************************************************************************************!
!							BOUNDARIES								 												  !
! ********************************************************************************************************************!

	subroutine processBoundaryDataVTK(nranks, is, ie, iStep, boundaryName, fields, fmt)

		use serialmeshvar
		use penf
		use vtk_fortran, only : vtk_file

		implicit none

		type(vtk_file) :: vtk_field_file
	!------------------------------------------------------------------------------------------------------------------

		character(*) :: boundaryName
		!! target boundary name

		character(len=*) :: fmt
		!! output format (binaru or ascii)

		character(len=10) :: file_number
		!! file number as string

		character(len=100) :: fields
		!! list of fields as comma separated strings

		character(len=:), allocatable :: fname
		!! file name
	!------------------------------------------------------------------------------------------------------------------

		integer :: nranks
		!! number of processors used
		
		integer :: fnumber
		!! field number a s integer
		
		integer :: ierr
		!! error flag

		integer :: i, iBoundary, targetBoundary, is, ie, iStep
		!! loop indices
	!------------------------------------------------------------------------------------------------------------------

		i = 0
		do iBoundary=1,numberOfBoundaries
			if(adjustl(trim(mesh%boundaries%userName(iBoundary)))==adjustl(trim(boundaryName))) then
				targetBoundary = iBoundary
				exit
			end if
			i = i+1
		end do

		if(i==numberOfBoundaries) call flubioStopMsg('FLUBIO: boundary '//trim(boundaryName)//' not found, check the spelling!')

		! Loop over times
		do fnumber=is,ie,iStep

			write(file_number, '(i0)') fnumber
			fname = 'postProc/VTKfields/'//trim(boundaryName)//'-'//trim(file_number)//'.vtu'

			! Header
			ierr = vtk_field_file%initialize(format=fmt, filename=fname, mesh_topology='UnstructuredGrid')

			! Coordinates and connectivities
			call writeBoundaryCoordinatesVTK(vtk_field_file, targetBoundary)

			! Boundary Fields
			call writeBoundaryFieldsVTK(vtk_field_file, fnumber, nranks, targetBoundary, fields)

			! Finalising
			ierr = vtk_field_file%xml_writer%write_dataarray(location='cell', action='close')
			ierr = vtk_field_file%xml_writer%write_piece()
			ierr = vtk_field_file%finalize()

			call flubioMsg('FLUBIO: boundary "'//trim(boundaryName)//'" in field number '//trim(file_number)// ' has been written!')

		end do

	end subroutine processBoundaryDataVTK

!**********************************************************************************************************************

	subroutine processBoundaryMeshVTK(boundaryName, fmt)

		use serialmeshvar
		use penf
		use vtk_fortran, only : vtk_file

		implicit none

		type(vtk_file) :: vtk_field_file
	!------------------------------------------------------------------------------------------------------------------	

		character(len=*) :: boundaryName
		!! boundary name

		character(len=*) :: fmt
		!! output file format 

		character(len=:), allocatable :: fname
		!! file name
	!------------------------------------------------------------------------------------------------------------------

		integer :: i, iBoundary
		!! loop indices
		
		integer :: targetBoundary
		!! target boundary

		integer :: ierr
		!! error flag
	!------------------------------------------------------------------------------------------------------------------

		fname='postProc/VTKfields/'//trim(boundaryName)//'.vtu'

		i = 0
		do iBoundary=1,numberOfBoundaries
			if(adjustl(trim(mesh%boundaries%userName(iBoundary)))==adjustl(trim(boundaryName))) then
				targetBoundary = iBoundary
				exit
			end if
			i = i+1
		end do

		if(i==numberOfBoundaries) call flubioStopMsg('FLUBIO: boundary '//trim(boundaryName)//' not found, check the spelling!')
		
		! Header
		ierr = vtk_field_file%initialize(format=fmt, filename=fname, mesh_topology='UnstructuredGrid')

		! Coordinates and connectivities
		call writeBoundaryCoordinatesVTK(vtk_field_file, targetBoundary)

		! Finalising
		ierr = vtk_field_file%xml_writer%write_dataarray(location='cell', action='close')
		ierr = vtk_field_file%xml_writer%write_piece()
		ierr = vtk_field_file%finalize()

		call flubioMSg('FLUBIO: boundary "'//trim(boundaryName)//'" mesh has been written!')

	end subroutine processBoundaryMeshVTK

! *********************************************************************************************************************

	subroutine writeBoundaryCoordinatesVTK(vtk_field_file, iBoundary)

		use serialmeshvar
		use penf
		use vtk_fortran, only : vtk_file

		implicit none

		type(vtk_file) :: vtk_field_file
	!------------------------------------------------------------------------------------------------------------------

		integer :: i, n, iBoundary, iBFace, is, ie, isp, iep, is_offset, ie_offset, ierr
		!! loop indices 

		integer :: np, nf,npb, nConn, fsize	
		!! work variables
	
		integer :: bPointsSize(numberOfBoundaries)
		!! size of boundary points for each boundary

		integer, dimension(:), allocatable :: nfn 
		!! number of face nodes
	!------------------------------------------------------------------------------------------------------------------

		integer, dimension(:,:), allocatable :: mapper
		!! array mapping the boundary nodes

		integer(I1P), dimension(:), allocatable :: cell_type
		!! cell type
	
		integer(I4P), dimension(:), allocatable :: offset
		!! offset for each boundary face 
		
		integer(I4P), dimension(:), allocatable :: connect
		!! connectivities

		integer, dimension(:), allocatable :: orientedConn
		!! mapper containing the oriented indices for the face nodes 
		
		integer, dimension(:), allocatable :: faceConn
		!! array hosting the face nodes
	!------------------------------------------------------------------------------------------------------------------

		real, dimension(:,:), allocatable :: points
		!! boundary points

		real, dimension(:,:,:), allocatable :: bVertex
		!! boundary vertices composing each boundary

		real :: facen(3)
		!! boundary face normal
	!------------------------------------------------------------------------------------------------------------------

		! Read data
		open(1, file='grid/bin/boundaries.bin', form='unformatted')

			read(1)  numberOfBoundaries, np, nf, fSize, n

			! Allocate
			allocate(nfn(numberOfFaces))
			allocate(mapper(n,numberOfBoundaries))
			allocate(bVertex(numberOfPoints, 3, numberOfBoundaries))

			read(1) !startFaces, nFaces
			read(1) !userName
			read(1) !allPoints
			read(1) mapper
			read(1) !bPoints
			read(1) bPointsSize
			read(1) bVertex
			read(1) nfn
		close(1)

		! Coordinates
		npb = bPointsSize(iBoundary)
		ierr = vtk_field_file%xml_writer%write_piece(np=npb, nc=mesh%boundaries%nFace(iBoundary))
		ierr = vtk_field_file%xml_writer%write_geo(np=bPointsSize(iBoundary), nc=mesh%boundaries%nFace(iBoundary), &
																			  x=bVertex(1:npb,1,iBoundary), &
																			  y=bVertex(1:npb,2,iBoundary), &
																			  z=bVertex(1:npb,3,iBoundary))
		! Connectivities
		is = mesh%boundaries%startFace(iBoundary)
		ie = is + mesh%boundaries%nFace(iBoundary) - 1
		nConn = sum(nfn(is:ie))

		allocate(cell_type(mesh%boundaries%nFace(iBoundary)))
		allocate(offset(mesh%boundaries%nFace(iBoundary)))
		allocate(connect(nConn))

		is_offset = 1
		ie_offset = is_offset + nfn(is) - 1

		i = 0
		do iBFace=is, ie

			i = i+1
			cell_type(i) = 7

			if(i==1) then
				offset(i) = nfn(iBFace)
			else
				offset(i) = offset(i-1) + nfn(iBFace)
			end if

			allocate(orientedConn(nfn(iBFace)))
			allocate(faceConn(nfn(iBFace)))

			! Orient face nodes counter-clockwise
			faceConn = mapper(is_offset:ie_offset, iBoundary)
			facen = mesh%Sf(iBFace,1:3)/norm2(mesh%Sf(iBFace,:))
			orientedConn = orientPointsInPlane(points=bVertex(faceConn,1:3, iBoundary), normal=facen, & 
															  center=mesh%fcentroid(iBFace,1:3), mode='ascending', np=nfn(iBFace))

			!connect(is_offset:ie_offset) = mapper(is_offset:ie_offset, iBoundary)-1
			connect(is_offset:ie_offset) = faceConn(orientedConn)-1

			is_offset = ie_offset + 1
			if(iBFace<ie) ie_offset = is_offset + nfn(iBFace+1) - 1

			! Deallocate for the next face
			deallocate(orientedConn)
			deallocate(faceConn)

		enddo

		ierr = vtk_field_file%xml_writer%write_connectivity(nc=mesh%boundaries%nFace(iBoundary), &
															connectivity=connect, &
															offset=offset, &
															cell_type=cell_type)

		ierr = vtk_field_file%xml_writer%write_dataarray(location='cell', action='open')

	end subroutine writeBoundaryCoordinatesVTK

! *********************************************************************************************************************

	subroutine writeBoundaryFieldsVTK(vtk_field_file, targetTime, nranks, iBoundary, fields)

		use serialmeshvar
		use m_strings
		use penf
		use vtk_fortran, only : vtk_file
		use fileHandling

		implicit none

		type(vtk_file) :: vtk_field_file
	!------------------------------------------------------------------------------------------------------------------
		
		character(len=*) :: fields
		!! fields to process coming as a comma separated string

		character(len=10) :: procID
		!! processor ID

		character(len=10) :: timeStamp	
		!! time to process as string

		character(len=15) :: fieldName
		!! field name as string

		character(len=10), dimension(:), allocatable :: fieldList
		!! list of fields to process

		character(len=:), allocatable :: file_name
		!! file name
	!------------------------------------------------------------------------------------------------------------------

		integer :: np(nranks)
		!! number of points in each processor's domain

		integer :: nbf(nranks) 
		!! number of boundary faces in each processor's domain

		integer :: ne(nranks)
		!! number of elements in each processor's domain

		integer :: nf(nranks)
		!! number of faces in each processor's domain

		integer :: nif(nranks)
		!! number of internal faces in each processor's domain

		integer :: nbe(nranks)
		!! number of boundary elements in each processor's domain

		integer :: nb(nranks)
		!! number of boundaries

		integer :: nrb(nranks)
		!! number of non-processor boundaries (real boudnaries)

		integer, dimension(:,:), allocatable :: startFace
		!! start face of each boundary for each processor's domain
		
		integer, dimension(:,:), allocatable :: nFaces
	    !! start face of each boundary for each processor's domain

		integer :: targetTime
		!! tagert time to process

		integer :: nranks
		!! number of processors used in the simulation 

		integer :: ierr
		!! error flag

		integer :: i, ii, j, isb, ieb, iRank, iElement, iBoundary, iBFace, iComp, iField, patchFaceB
		!! loop index variables
		
		integer :: nComp
		!! number of components

		integer :: nFields 
		!! number of fields

		integer :: bSize
		!! boundary points size

		integer :: boffset
		!! boundary faces offset

		integer, dimension(:), allocatable :: local2globalF
		!! mapping between local and global index for mesh faces
	!------------------------------------------------------------------------------------------------------------------

		real, dimension(:,:), allocatable :: field
		!! global field 
		
		real, dimension(:,:), allocatable :: phi
		!! local field
	!------------------------------------------------------------------------------------------------------------------

		write(timeStamp,'(i0)') targetTime

		call getNumberOfBoundaries(nb, nrb, nranks)

		allocate(startFace(maxval(nb),nranks))
		allocate(nFaces(maxval(nb),nranks))
		startFace = -1
		nFaces = -1

		!------------------------------!
		! Read mesh and boundary files !
		!------------------------------!

		do ii=1,nranks
			iRank = ii-1
			write(procID,'(i0)') iRank
			call getParallelMeshSizes(procID, nb(ii), np(ii), nf(ii), nif(ii), nbf(ii), ne(ii), nbe(ii), &
									  startFace(1:nb(ii),ii), nFaces(1:nb(ii),ii))
		enddo

		! Find out the field to read
		fieldList = getFieldList(fields)
		nFields = size(fieldList)
	
		! Loop over fields
		do iField=1,nFields

			call flubioMsg('FLUBIO: processing field: '//trim(fieldList(iField)))

			! Loop over partitions
			do ii=1,nranks

				! Initialize arrays
				iRank = ii-1
				write(procID,'(i0)') iRank

				call readListFromFile(fileName='grid/processor'//trim(procID)//'/faceProcAddressing', returnList=local2globalF)

				! Read the fields partitions
				file_name = 'postProc/fields/'//trim(timeStamp)//'/'//trim(fieldList(iField))//'-d'//trim(procID)//'.bin'
				open(1,file=file_name,form='unformatted')

					! Read fields for the i-th partion
					read(1) nComp
					read(1) fieldName

					! Allocate field matrix
					allocate(phi(ne(ii)+nbf(ii),nComp))
					phi = 0.0

					if(ii==1) allocate(field(numberOfElements + numberOfBFaces, nComp))

					do iComp=1,nComp
						read(1) phi(:,iComp)
						read(1)
						read(1)
					enddo

				close(1)

				! Reconstruct in the global array, boundary field only
				bOffset = sum(nFaces(1:iBoundary-1, ii))
				if(iBoundary==1) bOffset = 0
				patchFaceB = ne(ii) + bOffset
				isb = startFace(iBoundary,ii)
				ieb = isb+nFaces(iBoundary,ii)-1

				do iBFace=isb,ieb
					patchFaceB = patchFaceB+1
					j = numberOfElements + local2globalF(iBFace) - numberOfIntFaces
					field(j,:) = phi(patchFaceB,:)
				enddo

				! Deallocate and reallocate with a new size on the partition
				deallocate(phi)
				deallocate(local2globalF)

			! End of partion
			end do

			isb = numberOfElements + mesh%boundaries%startFace(iBoundary) - numberOfIntFaces
			ieb = isb + mesh%boundaries%nFace(iBoundary) -1

			if(nComp==1) then
				ierr = vtk_field_file%xml_writer%write_dataarray(data_name=trim(fieldName), x=field(isb:ieb,1))
			else
				ierr = vtk_field_file%xml_writer%write_dataarray(data_name=trim(fieldName), &
																 		x=field(isb:ieb,1), &
																 		y=field(isb:ieb,2), &
																 		z=field(isb:ieb,3))
			end if

			! Deallocate current field
			deallocate(field)

		! End of fields
		end do

	end subroutine writeBoundaryFieldsVTK

! *********************************************************************************************************************

	subroutine getNumberOfBoundaries(numberOfBoundaries, numberOfRealBound, nranks)

		implicit none

		character(10) :: procID
	!------------------------------------------------------------------------------------------------------------------

		integer :: ii, id, nranks, numberOfBoundaries(nranks), numberOfRealBound(nranks)
	!------------------------------------------------------------------------------------------------------------------

		do ii=1,nranks

			id=ii-1
			write(procID,'(i0)') id

			open(1,file='grid/bin/mesh-d'//trim(procID)//'.bin',form='unformatted')
				read(1)
				read(1)
				read(1)
				read(1)
				read(1)
				read(1)
				read(1) numberOfBoundaries(ii), numberOfRealBound(ii)
			close(1)

		enddo

	end subroutine getNumberOfBoundaries

! *********************************************************************************************************************

	subroutine globalFaceConn(local2globalF, nlf, len_ii, id)

		implicit none

		character(len=10) :: procID
	!------------------------------------------------------------------------------------------------------------------

		integer :: iFace, id, label, nlf, len_ii

		integer :: local2globalF(nlf)
	!------------------------------------------------------------------------------------------------------------------

		!-------------------------------------------!
		! Pass from actual labeling to original one !
		!-------------------------------------------!

		local2globalF=-1
		write(procID,'(i0)') id

		! Read local to global connectivities
		open(1,file='grid/processor'//trim(procID)//'/faceProcAddressing')
			read(1,*)
			do iFace=1,len_ii
				read(1,*) label
				local2globalF(iFace) = label
			enddo
		close(1)

	end subroutine globalFaceConn

! *********************************************************************************************************************

	subroutine globalConnPre(local2global,totSizeE,nranks)

		use fileHandling

		implicit none

		character(len=10) procID
	!------------------------------------------------------------------------------------------------------------------

		integer :: numberOfElements(nranks)

		integer :: i, ii, id, nranks, totSizeE, patchFaceE

		integer :: label(totSizeE,nranks), local2global(totSizeE)

		integer, dimension(:), allocatable :: cellProcAddr
	!------------------------------------------------------------------------------------------------------------------

		!-------------------------------------------!
		! Pass from actual labeling to original one !
		!-------------------------------------------!

		local2global = -1
		patchFaceE = 0

		do ii=1,nranks

			id = ii-1
			write(procID,'(i0)') id

			call readListFromFile(fileName='grid/processor'//trim(procID)//'/cellProcAddressing', returnList=cellProcAddr)

			do i=1,size(cellProcAddr)
				patchFaceE = patchFaceE+1
				local2global(patchFaceE) = cellProcAddr(i) + 1
			enddo

			deallocate(cellProcAddr)

		enddo

	end subroutine globalConnPre

! *********************************************************************************************************************

	subroutine getCellSize(mesh_path, numberOfElements)

		use fileHandling

		implicit none

		character(len=*) :: mesh_path

		character(len=10) :: procID
	!------------------------------------------------------------------------------------------------------------------

		integer :: n, no, nn, numberOfElements, max_owner, max_neighbour

		integer, dimension(:), allocatable :: owner, neighbour
	!------------------------------------------------------------------------------------------------------------------

		call readListFromFile(fileName=mesh_path//'/owner', returnList=owner)
		owner = owner+1

		call readListFromFile(fileName=mesh_path//'/neighbour', returnList=neighbour)
		neighbour = neighbour+1

		max_owner = maxval(owner)
		max_neighbour = maxval(neighbour)
		numberOfElements = max0(max_owner,max_neighbour)

		deallocate(owner)
		deallocate(neighbour)

	end subroutine getCellSize

! *********************************************************************************************************************

	subroutine getParallelMeshSizes(procID, bSize, np, nf, nif, nbf, ne, nbe, startFaces, nFaces)

		implicit none

		character(len=*) :: procID
	!------------------------------------------------------------------------------------------------------------------

		integer :: np, nf, nif, nbf, ne, nbe, bSize, dummy_int

		integer :: startFaces(bSize), nFaces(bSize)
	!------------------------------------------------------------------------------------------------------------------

		open(1, file='grid/bin/mesh-d'//trim(procID)//'.bin', form='unformatted')
			read(1) np, nf, nif, nbf, ne, nbe
			read(1)
			read(1)
			read(1)
			read(1)
			read(1)
			read(1) dummy_int, dummy_int, startFaces(1:bSize), nFaces(1:bSize)
		close(1)

	end subroutine getParallelMeshSizes

! *********************************************************************************************************************

	function getNumberOfFields(listName) result(nFields)

		use m_strings

		implicit none

		character(len=*) :: listName

		character(len=10) :: dummy_char

		character(len=10), dimension(:), allocatable :: charr
	!------------------------------------------------------------------------------------------------------------------

		integer :: iField, nFields, io
	!------------------------------------------------------------------------------------------------------------------

		nFields = 0
		open(1, file = 'postProc/fields/'//listName)
			do
				read(1,*,iostat=io)
				if(io/=0) exit
				nFields = nFields + 1
			end do
		close(1)

	end function getNumberOfFields

! *********************************************************************************************************************

	function getFieldList(fields) result(fieldList)

		use m_strings

		implicit none

		character(len=*) :: fields

		character(len=10) :: dummy_char

		character(len=10), dimension(:), allocatable :: fieldList
	!------------------------------------------------------------------------------------------------------------------

		integer :: iField, nFields, io
	!------------------------------------------------------------------------------------------------------------------

		if(trim(fields)=='empty') then
			nFields = getNumberOfFields('fieldList.txt')
			allocate(fieldList(nFields))
			open(1, file = 'postProc/fields/fieldList.txt')
				do iField=1,nFields
					read(1,*) dummy_char
					fieldList(iField) = trim(dummy_char)
				end do
			close(1)
		else
			call split_in_array(fields, fieldList,',')
			nFields = size(fieldList)
		end if

	end function getFieldList

! *********************************************************************************************************************

end module serialVTKFormat	