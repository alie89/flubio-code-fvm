module fileHandling

    use serialNodes, only: skipHeader, findStreamSize, checkFormat
    use strings
    use m_strings

    implicit none

    contains

    function getListSize(fileName) result(listSize)
            
        implicit none 

        character(len=*) :: fileName

        character(len=:), allocatable :: fileFormat
    !------------------------------------------------------------------------------------------------------------------	

        integer :: listSize
    !------------------------------------------------------------------------------------------------------------------

        call checkFormat(fileName, fileFormat)

        if (fileFormat=='openfoam_ascii') then
            listSize = getListSizeOFAscii(fileName)
        elseif(fileFormat=='openfoam_binary') then
            listSize = getListSizeOFBinary(fileName)
        elseif(fileFormat=='flubio_ascii') then 
            listSize = getListSizeflubioAscii(fileName)
        elseif(fileFormat=='flubio_binary') then 
            listSize = getListSizeFlubioBinary(fileName)
        else
            write(*,*) 'ERROR: cannot recognize file format for '// fileName
            stop
        endif   

    end function getListSize

    ! *********************************************************************************************************************	

    function getListSizeOFAscii(fileName) result(listSize)

    !==================================================================================================================
    ! Description:
    !! getListSizeOFAscii reads the list size from the OF mesh files.
    !==================================================================================================================

        character(len=*) :: fileName
        !! file name

        character(len=:), allocatable :: oneLine
        !! dummy argument
    !------------------------------------------------------------------------------------------------------------------

        integer :: listSize
        !! list size

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        open(1,file=fileName)
            call skipHeader(1, 'ascii')
            call findStreamSize(1, 'ascii', listSize, oneLine)
        close(1)

        end function getListSizeOFAscii 
    
!**********************************************************************************************************************

        function getListSizeOFBinary(fileName) result(listSize)

    !==================================================================================================================
    ! Description:
    !! getListSizeOFBinary reads the list size from the OF mesh files in binary format.
    !==================================================================================================================

        character(len=*) :: fileName
        !! file name

        character(len=:), allocatable :: oneLine
    !------------------------------------------------------------------------------------------------------------------

        integer :: listSize
        !! list size
    !------------------------------------------------------------------------------------------------------------------

        open(1, file=fileName, FORM = 'UNFORMATTED', access='STREAM')
            call skipHeader(1, 'binary')
            call findStreamSize(1, 'binary', listSize, oneLine)
        close(1)

        end function getListSizeOFBinary

!**********************************************************************************************************************

    function getListSizeFlubioAscii(fileName) result(listSize)

    !==================================================================================================================
    ! Description:
    !! getListSizeFlubioAscii reads the list size from the FLUBIO mesh files in ascii format.
    !==================================================================================================================

        character(len=*) :: fileName
    !------------------------------------------------------------------------------------------------------------------

        integer :: listSize
        !! list size
    !------------------------------------------------------------------------------------------------------------------

        ! Read Points
        open(1,file=fileName)
            read(1,*) listSize
        close(1)

        end function getListSizeFlubioAscii

    !**********************************************************************************************************************                
                    
    function getListSizeFlubioBinary(fileName) result(listSize)

    !==================================================================================================================
    ! Description:
    !! getListSizeFlubioBinary reads the list size from the FLUBIO mesh files in ascii format.
    !==================================================================================================================

        character(len=*) :: fileName
    !------------------------------------------------------------------------------------------------------------------

        integer :: listSize
        !! list size
    !------------------------------------------------------------------------------------------------------------------

        ! Read Points
        open(1,file=fileName, form='unformatted')
            read(1) listSize
        close(1)

    end function getListSizeFlubioBinary

!**********************************************************************************************************************

    subroutine readListFromFile(fileName, returnList)

        character(len=*) :: fileName

        character(len=:), allocatable :: fileFormat 
    !------------------------------------------------------------------------------------------------------------------    

        integer, dimension(:), allocatable :: returnList
    !------------------------------------------------------------------------------------------------------------------    

        call checkFormat(fileName, fileFormat)

        if (fileFormat=='openfoam_ascii') then
            call readListOFAscii(fileName, returnList)
        elseif(fileFormat=='openfoam_binary') then
            call readListOFBinary(fileName, returnList)
        elseif(fileFormat=='flubio_ascii') then 
            call readListFlubioAscii(fileName, returnList)
        elseif(fileFormat=='flubio_binary') then 
            call readListFlubioBinary(fileName, returnList)
        else
            write(*,*) 'ERROR: cannot recognize file format for '// fileName
            stop
        endif   

    end subroutine readListFromFile


!**********************************************************************************************************************

    subroutine readListOFAscii(fileName, returnList)

    !==================================================================================================================
    ! Description:
    !! readMeshPointsOFAscii reads the nodes composing the mesh from the OF mesh files.
    !==================================================================================================================
    
        character(len=*) :: fileName
        !! file name

        character(len=80) :: line
        !! parsed line
    
        character(len=:), allocatable :: croppedLine, oneLine
        !! cropped line
    
        character(len=16), allocatable :: carray(:)
        !! splitted line
    !------------------------------------------------------------------------------------------------------------------
    
        integer :: n
        !! loop index
    
        integer :: np
        !! number of mesh nodes
    
        integer :: ierr
        !! error flag

        integer, dimension(:), allocatable :: returnList
    !------------------------------------------------------------------------------------------------------------------
    
        open(1,file=fileName)
            
            call skipHeader(aunit=1, fileFormat='ascii')
            call findStreamSize(aunit=1, fileFormat='ascii', streamSize=np, oneLine =  oneLine)
            
            allocate(returnList(np))
            
            do n =1,np
    
                call readLine(1, line, ierr)
                croppedLine = crop(line)
        
                call substitute(croppedLine,'(','')
                call substitute(croppedLine,')','')
                call split_in_array(croppedLine, carray, ' ')
                    
                call string_to_value(carray(1), returnList(n), ierr)
        
            enddo  
    
        close(1)

    end subroutine readListOFAscii 
          
!**********************************************************************************************************************

    subroutine readListOFBinary(fileName, returnList)

    !==================================================================================================================
    ! Description:
    !! readList reads the nodes composing the mesh from the mesh files.
    !==================================================================================================================

        character(len=*) :: fileName
        !! file name

        character(len=:), allocatable :: oneLine
    !------------------------------------------------------------------------------------------------------------------

        integer :: n
        !! loop index

        integer :: np
        !! number of mesh nodes

        integer, dimension(:), allocatable :: returnList
    !------------------------------------------------------------------------------------------------------------------

        open(1, file=fileName, FORM = 'UNFORMATTED', access='STREAM')

            call skipHeader(1, 'binary')
            call findStreamSize(1, fileFormat='binary', streamSize=np,  oneLine =  oneLine)

            allocate(returnList(np))

            ! Read points
            do n =1,np
                read(1) returnList(n)
            enddo  

        close(1)

    end subroutine readListOFBinary

!**********************************************************************************************************************

    subroutine readListFlubioAscii(fileName, returnList)

    !==================================================================================================================
    ! Description:
    !! readList reads the nodes composing the mesh from the mesh files.
    !==================================================================================================================

        character(len=*) :: fileName
        !! file name
    !------------------------------------------------------------------------------------------------------------------

        integer :: n
        !! loop index

        integer :: np
        !! number of mesh nodes

        integer, dimension(:), allocatable :: returnList
    !------------------------------------------------------------------------------------------------------------------

        ! Read Points
        open(1,file=fileName)

            read(1,*) np

            allocate(returnList(np))

            ! Read points
            do n=1,np
                read(1,*) returnList(n)
            enddo

        close(1)

    end subroutine readListFlubioAscii

!**********************************************************************************************************************                
                    
    subroutine readListFlubioBinary(fileName, returnList)

    !==================================================================================================================
    ! Description:
    !! readList reads the nodes composing the mesh from the mesh files.
    !==================================================================================================================

        character(len=*) :: fileName
        !! file name
    !------------------------------------------------------------------------------------------------------------------

        integer :: n
        !! loop index

        integer :: np
        !! number of mesh nodes

        integer, dimension(:), allocatable :: returnList
    !------------------------------------------------------------------------------------------------------------------

        ! Read Points
        open(1,file=fileName, form='unformatted')

            read(1) np

            allocate(returnList(np))

            ! Read points
            do n=1,np
                read(1) returnList(n)
            enddo

        close(1)

    end subroutine readListFlubioBinary
    

!**********************************************************************************************************************

end module    
