!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!
	program meshView

		use meshvar
		use flubioMpi

		implicit none

		character(len=10) :: fmt
		!! output format

		character(len=10), dimension(:), allocatable :: args
		!! command line arguments
	!------------------------------------------------------------------------------------------------------------------

		integer :: nargs
		!! number of command line arguments
		
		integer :: ierr
		!! error flag
	!------------------------------------------------------------------------------------------------------------------

		call mpi_init(ierr)

		call setMPIvar()

		!------------------------!
		! command line arugments !
		!------------------------!

		nargs = command_argument_count()
		allocate(args(nargs))
		call parseCommandLine(args, nargs, fmt)

		!--------------------!
		! read parallel mesh !
		!--------------------!

		call mesh%readMeshFromFiles()

		!------------------!
		! Write data files !
		!------------------!

		call writeParallelMeshVTK(trim(fmt))

		call mpi_finalize(ierr)

	end program meshView

! *********************************************************************************************************************

	subroutine writeParallelMeshVTK(fmt)

		use flubioMpi
		use parallelVTKFormat, only: writeCoordinatesVTK
		use penf
		use vtk_fortran, only : vtk_file, pvtk_file

		implicit none

		type(vtk_file) :: vtk_field_file
		
		type(pvtk_file) :: pvtk_field_file

		character(len=:), allocatable :: fname, dirName

		character(len=*) :: fmt

		character(len=10) :: procID
	!------------------------------------------------------------------------------------------------------------------

		integer :: i, ierr
	!------------------------------------------------------------------------------------------------------------------

		logical :: dirExists
    !------------------------------------------------------------------------------------------------------------------

		write(procID,'(i0)') id

		! Create a directory for the field. If it does not exists create it.
		dirName = 'postProc/VTKfields/Parallel/mesh'
		inquire(file=trim(dirName)//'/.', exist=dirExists)

		if(.not. dirExists) call execute_command_line ('mkdir -p ' // adjustl(trim(dirName) ) )

		fname = 'postProc/VTKfields/Parallel/mesh/mesh-d'//trim(procID)// '.vtu'
		ierr = vtk_field_file%initialize(format=fmt, filename=fname, mesh_topology='UnstructuredGrid')

		call writeCoordinatesVTK(vtk_field_file)
		call writeCellDist(vtk_field_file)

		ierr = vtk_field_file%xml_writer%write_dataarray(location='cell', action='close')
		ierr = vtk_field_file%xml_writer%write_piece()
		ierr = vtk_field_file%finalize()

		! Master rank needs to write a vtup file
		if(id==0) then

			ierr = pvtk_field_file%initialize(filename='postProc/VTKfields/Parallel/mesh/mesh.pvtu', &
					                          mesh_topology='PUnstructuredGrid', mesh_kind="Float64")

			ierr = pvtk_field_file%xml_writer%write_dataarray(location='cell', action='open')

			ierr = pvtk_field_file%xml_writer%write_parallel_dataarray(data_name='cellDist',  &
					                                                   data_type='Float64',   &
					                                                   number_of_components=1)

			ierr = pvtk_field_file%xml_writer%write_dataarray(location='cell', action='close')

			do i=1,nid
				write(procID,'(i0)') i-1
				fname = './mesh-d'//trim(procID)// '.vtu'
				ierr = pvtk_field_file%xml_writer%write_parallel_geo(source=fname)
			end do

			ierr = pvtk_field_file%finalize()

		end if

		call flubioMsg('FLUBIO: mesh has been written!')

	end subroutine writeParallelMeshVTK

! *********************************************************************************************************************

		subroutine parseCommandLine(args, nargs, fmt)

			use flubioMpi, only: flubioStopMsg

			implicit none

			character(len=10) :: fmt

			character(len=*) :: args(nargs)
		!--------------------------------------------------------------------------------------------------------------
	
			integer :: p, nargs, stat
		!--------------------------------------------------------------------------------------------------------------

			fmt='binary'

			! Get the arguments
			do p=1,nargs
				call get_command_argument(number=p, value=args(p), status=stat)
			enddo

			! Process the options
			do p=1,nargs
				if(args(p)=='-format') then
					fmt = trim(args(p+1))
				elseif(args(p)=='-help') then
					call flubioStopMsg('example: mpirun -np 4 flubio_meshview_parallel')
				endif

			enddo

		end subroutine parseCommandLine

! *********************************************************************************************************************

	subroutine writeCellDist(vtk_field_file)

		use meshvar
		use flubioMpi
		use penf
		use vtk_fortran, only : vtk_file

		implicit none

		type(vtk_file) :: vtk_field_file
	!------------------------------------------------------------------------------------------------------------------

		integer :: ierr
	!------------------------------------------------------------------------------------------------------------------

		real, dimension(:,:), allocatable :: phi
	!------------------------------------------------------------------------------------------------------------------

		! Allocate field array
		allocate(phi(numberOfElements+numberOfBFaces, 1))
		phi = real(id)

		! Write cell distribution
		ierr = vtk_field_file%xml_writer%write_dataarray(data_name='cellDist', x=phi(1:numberOfElements, 1))

		deallocate(phi)

	end subroutine writeCellDist

! *********************************************************************************************************************

