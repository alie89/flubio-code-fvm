!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

	module parallelVTKFormat

	contains 

	subroutine processDataVTK(is, ie, iStep, fields, fmt)

		use flubioMpi
		use penf
		use m_strings
		use vtk_fortran, only : vtk_file, pvtk_file

		implicit none

		type(vtk_file) :: vtk_field_file

		type(pvtk_file) :: pvtk_field_file
	! ------------------------------------------------------------------------------------------------------------------

		character(len=10) :: file_number
		!! file number as string

		character(len=*) :: fields
		!! list of fields to process as comma separated string

		character(len=*) :: fmt
		!! output format (binary or ascii)

		character(len=:), allocatable :: fname
		!! file name

		character(len=10) :: rank_number
		!! rank number as string

		character(len=10) ::procID
		!! processor if as string

		character(len=15) :: fieldName
		!! field name

		character(len=10), dimension(:), allocatable :: fieldList
		!! field to process as list

		character(len=:), allocatable :: dirName
		!! folder to save the output
	!------------------------------------------------------------------------------------------------------------------

		integer :: nranks
		!! number of processor used in the decomposed case
		
		integer :: fnumber
		!! file number as integer
		
		integer :: ierr
		!! error flag

		integer :: is, ie, iStep
		!! loop indices

		integer :: i, iField
		!! loop indices
		
		integer, dimension(:), allocatable :: nComp
		!! number of components
		
		integer :: nFields
		!! number of fields
	!------------------------------------------------------------------------------------------------------------------

		logical :: dirExists
		!! flag to check if a directory exists
	!------------------------------------------------------------------------------------------------------------------

		do fnumber=is,ie,iStep

			write(file_number, '(i0)') fnumber
			write(rank_number, '(i0)') id

			call flubioMsg('FLUBIO: processing field number '//trim(file_number))

			! Create a directory for the field. If it does not exists create it.
			dirName = 'postProc/VTKfields/Parallel/'//trim(file_number)
			inquire(file=trim(dirName)//'/.', exist=dirExists)

			if(.not. dirExists) call execute_command_line ('mkdir -p ' // adjustl(trim(dirName)))

			fname = 'postProc/VTKfields/Parallel/'//trim(file_number)//'/flubioField-d'//trim(rank_number)//'.vtu'
			ierr = vtk_field_file%initialize(format=fmt, filename=fname, mesh_topology='UnstructuredGrid')

			call writeCoordinatesVTK(vtk_field_file)
			call writeParallelFieldsVTK(vtk_field_file, fnumber, fields, nComp)

			ierr = vtk_field_file%xml_writer%write_dataarray(location='cell', action='close')
			ierr = vtk_field_file%xml_writer%write_piece()
			ierr = vtk_field_file%finalize()

			! Master rank writes the pvtu file
			if(id==0) then

				! Find out the field to read
				fieldList = getFieldList(fields)
				nFields = size(fieldList)

				! Write the pvtu file
				fname = 'postProc/VTKfields/Parallel/'//trim(file_number)//'/flubioField-'//trim(file_number)//'.pvtu'
				ierr = pvtk_field_file%initialize(filename=fname, mesh_topology='PUnstructuredGrid', mesh_kind="Float64")

				ierr = pvtk_field_file%xml_writer%write_dataarray(location='cell', action='open')

				do iField=1,nFields

					if(fieldList(iField)=='U') then
						nComp=3
					else
						nComp = 1
					end if

					ierr = pvtk_field_file%xml_writer%write_parallel_dataarray(data_name=fieldList(iField), &
							                                                   data_type='Float64',         &
							                                                   number_of_components=nComp(iField))
				end do

				ierr = pvtk_field_file%xml_writer%write_dataarray(location='cell', action='close')

				do i=1,nid
					write(procID,'(i0)') i-1
					fname = './flubioField-d'//trim(procID)// '.vtu'
					ierr = pvtk_field_file%xml_writer%write_parallel_geo(source=fname)
				end do

				ierr = pvtk_field_file%finalize()

			end if

			call flubioMsg('FLUBIO: field number '//trim(file_number)// ' has been written!')

		end do

	end subroutine processDataVTK

! *********************************************************************************************************************

	subroutine writeCoordinatesVTK(vtk_field_file)

		use meshvar
		use penf
		use vtk_fortran, only : vtk_file

		implicit none

		type(vtk_file) :: vtk_field_file

		type(flubioList), dimension(:), allocatable :: evertex
		!! list of vertices composing a cell
	!------------------------------------------------------------------------------------------------------------------

		integer :: i, n, is, ie
		!! loop indices

		integer :: ierr
		!! error flag

		integer :: numberOfElemNodes
		!! number of nodes composing each element

		integer :: connSize
		!! connectivity size

		integer(I1P) :: cell_type(numberOfElements)
		!!cell types

		integer(I4P) :: offset(numberOfElements)
		!! nodes offset

		integer(I4P), dimension(:), allocatable :: connect
		!! connectivities

		integer(I4P) :: faceOffsets(numberOfElements)
		!! face offsets

		integer(I4P), dimension(:), allocatable :: face
		!! faces composing each

		integer(I4P), dimension(:), allocatable :: vrt
		!! nodes composing each element
	!------------------------------------------------------------------------------------------------------------------

		! Get the element connectivities
        evertex = mesh%buildNodesToElementConn()

		! Compute connectivity size
		connSize = 0
		do i=1,numberOfElements
			n = evertex(i)%getUnrolledListSize()
			connSize = connSize + n
		enddo		 

		allocate(connect(connSize))
		connect=-1

		! Cell types
		offset = 0
		ie = 0

		do i=1,numberOfElements

			cell_type(i) = 42
			numberOfElemNodes = evertex(i)%getListSize(direction=2)

			is = ie+1
			ie = is+numberOfElemNodes-1

			if(i==1) then
				offset(i) = numberOfElemNodes
			else
				offset(i) = offset(i-1) + numberOfElemNodes
			end if

			allocate(vrt(numberOfElemNodes))

			vrt = evertex(i)%getList(is=1, ie=numberOfElemNodes, targetColumn=1, isReal=0)
			
			connect(is:ie) = vrt-1

			! Deallocate vrt
			deallocate(vrt)

		enddo

		! Sizes
		ierr = vtk_field_file%xml_writer%write_piece(np=numberOfPoints, nc=numberOfElements)

		! Points
		ierr = vtk_field_file%xml_writer%write_geo(np=numberOfPoints,   &
												   nc=numberOfElements, &
				     					           x=mesh%vertex(:,1),  &
											       y=mesh%vertex(:,2),  &
												   z=mesh%vertex(:,3))

		! Connectivities and cell types 							   												
		call mesh%getFacesAndOffsets(face, faceOffsets)	
		
		ierr = vtk_field_file%xml_writer%write_connectivity(nc=numberOfElements,  &
				                                            connectivity=connect, &
														    offset=offset,        &
															cell_type=cell_type, &
															face=face, &
															faceoffset=faceOffsets)

		ierr = vtk_field_file%xml_writer%write_dataarray(location='cell', action='open')	

		deallocate(evertex)

	end subroutine writeCoordinatesVTK

! *********************************************************************************************************************

	subroutine writeParallelFieldsVTK(vtk_field_file, currentTime, fields, fieldComp)

		use meshvar
		use flubioMpi
		use m_strings
		use penf
		use vtk_fortran, only : vtk_file

		implicit none

		type(vtk_file) :: vtk_field_file
	!------------------------------------------------------------------------------------------------------------------

		character(len=*) :: fields
		!! fields to process coming as a comma separated string

		character(len=10) :: procID
		!! processor ID

		character(len=10) :: timeStamp	
		!! time to process as string

		character(len=15) :: fieldName
		!! field name as string

		character(len=10), dimension(:), allocatable :: fieldList
		!! list of fields to process

		character(len=:), allocatable :: file_name
		!! file name
	!------------------------------------------------------------------------------------------------------------------

		integer :: nComp
		!! number of components

		integer :: nFields 
		!! number of fields

		integer :: ierr
		!! error flag

		integer :: currentTime
		!! current processed time as integer
		
		integer :: iComp, iField
		!! loop indices

		integer, dimension(:), allocatable :: fieldComp
	!------------------------------------------------------------------------------------------------------------------

		real, dimension(:,:), allocatable :: phi
		!! array to store the field value
	!------------------------------------------------------------------------------------------------------------------

		! Find out the field to read 
		fieldList = getFieldList(fields)
		nFields = size(fieldList)

		allocate(fieldComp(nFields))

		! Loop over fields
		do iField=1, nFields

			 call flubioMsg('FLUBIO: reading field '//trim(fieldList(iField))//'...')

			! Initialize arrays
			write(procID,'(i0)') id
			write(timeStamp,'(i0)') currentTime

			file_name = 'postProc/fields/'//trim(timeStamp)//'/'//trim(fieldList(iField))//'-d'//trim(procID)//'.bin'
			open(1, file=file_name, form='unformatted')

				! Read fields for the i-th partion
				read(1) nComp
				read(1) fieldName

				! Allocate field array
				allocate(phi(numberOfElements+numberOfBFaces,nComp))
				phi = 0.0
				fieldComp = nComp

				! Read fields components
				do iComp=1,nComp
					read(1) phi(:,iComp)
					read(1)
					read(1)
				enddo

			close(1)

			! Write Field
			if(nComp==1) then
				ierr = vtk_field_file%xml_writer%write_dataarray(data_name=trim(fieldName), &
															     x=phi(1:numberOfElements, 1))
			else
				ierr = vtk_field_file%xml_writer%write_dataarray(data_name=trim(fieldName),    &
																 x=phi(1:numberOfElements, 1), &
						                                         y=phi(1:numberOfElements, 2), &
						                                         z=phi(1:numberOfElements, 3))
			end if

			! Deallocate and reallocate field
			deallocate(phi)

		end do

	end subroutine writeParallelFieldsVTK

! ********************************************************************************************************************!
!											BOUNDARIES		  													  	  !
! ********************************************************************************************************************!

	subroutine processBoundaryDataVTK(is, ie, iStep, boundaryName, fields, fmt)

		use flubioMpi
		use meshvar
		use penf
		use m_strings
		use vtk_fortran, only : vtk_file, pvtk_file

		implicit none

		type(vtk_file) :: vtk_field_file

		type(pvtk_file) :: pvtk_field_file
	!------------------------------------------------------------------------------------------------------------------

		character(len=*) :: boundaryName
		!! target boundary name

		character(len=*) :: fmt
		!! output format (binaru or ascii)

		character(len=10) :: file_number
		!! file number as string

		character(len=10) :: rank_number
		!! processor number as string

		character(len=10) ::procID
		!! processor if as string

		character(len=15) :: fieldName
		!! field name

		character(len=100) :: fields
		!! list of fields as comma separated strings

		character(len=10), dimension(:), allocatable :: fieldList
		!! field to process as list

		character(len=:), allocatable :: dirName
		!! folder to save the output

		character(len=:), allocatable :: fname
		!! file name
	!------------------------------------------------------------------------------------------------------------------

		integer :: fnumber
		!! field number a s integer
		
		integer :: ierr
		!! error flag

		integer :: targetBoundary
		!! target boundary

		integer, dimension(:), allocatable :: nComp
		!! number of components

		integer :: nFields
		!! number of fields to process

		integer :: i, iBoundary, is, ie, iStep, iField
	!------------------------------------------------------------------------------------------------------------------

		logical :: dirExists
		!! flag to check if a directory exists
	!------------------------------------------------------------------------------------------------------------------

		i = 0
		do iBoundary=1,numberOfBoundaries
			if(adjustl(trim(mesh%boundaries%userName(iBoundary)))==adjustl(trim(boundaryName))) then
				targetBoundary = iBoundary
				exit
			end if
			i = i+1
		end do

		if(i==numberOfBoundaries) call flubioStopMsg('FLUBIO: boundary '//trim(boundaryName)//' not found, check the spelling!')
		
		! Loop over times
		do fnumber=is,ie,iStep

			write(file_number, '(i0)') fnumber
			write(rank_number, '(i0)') id

			call flubioMsg('FLUBIO: processing field number '//trim(file_number))

			! Create a directory for the field. If it does not exists create it.
			dirName = 'postProc/VTKfields/Parallel/'//file_number
			inquire(file=trim(dirName)//'/.', exist=dirExists)

			if(.not. dirExists) call execute_command_line ('mkdir -p ' // adjustl(trim(dirName)))

			write(file_number, '(i0)') fnumber
			fname='postProc/VTKfields/Parallel/'//trim(file_number)//'/'//trim(boundaryName)//'-d'//trim(rank_number)//'-'//trim(file_number)//'.vtu'

			! Header
			ierr = vtk_field_file%initialize(format=fmt, filename=fname, mesh_topology='UnstructuredGrid')

			! Coordinates and connectivities
			call writeBoundaryCoordinatesVTK(vtk_field_file, targetBoundary)

			! Boundary Fields
			call writeParallelBoundaryFieldsVTK(vtk_field_file, fnumber, targetBoundary, fields, nComp)

			! Finalising
			ierr = vtk_field_file%xml_writer%write_dataarray(location='cell', action='close')
			ierr = vtk_field_file%xml_writer%write_piece()
			ierr = vtk_field_file%finalize()

			! Master rank writes the pvtu file
			if(id==0) then

				! Find out the field to read
				fieldList = getFieldList(fields)
				nFields = size(fieldList)

				! Write the pvtu file
				fname='postProc/VTKfields/Parallel/'//trim(file_number)//'/'//trim(boundaryName)//'-'//trim(file_number)//'.pvtu'
				ierr = pvtk_field_file%initialize(filename=fname, mesh_topology='PUnstructuredGrid', mesh_kind="Float64")

				ierr = pvtk_field_file%xml_writer%write_dataarray(location='cell', action='open')

				do iField=1,nFields

					ierr = pvtk_field_file%xml_writer%write_parallel_dataarray(data_name=fieldList(iField), &
																			   data_type='Float64', &
																			   number_of_components=nComp(iField))

				end do

				ierr = pvtk_field_file%xml_writer%write_dataarray(location='cell', action='close')

				do i=1,nid
					write(procID,'(i0)') i-1
					fname='./'//trim(boundaryName)//'-d'//trim(procID)//'-'//trim(file_number)//'.vtu'
					ierr = pvtk_field_file%xml_writer%write_parallel_geo(source=fname)
				end do

				ierr = pvtk_field_file%finalize()

			end if

			call flubioMsg('FLUBIO: boundary "'//trim(boundaryName)//'" in field number '//trim(file_number)// ' has been written!')

		end do

	end subroutine processBoundaryDataVTK

!**********************************************************************************************************************

	subroutine processBoundaryMeshVTK(boundaryName, fmt)

		use flubioMpi
		use meshvar
		use penf
		use vtk_fortran, only : vtk_file, pvtk_file

		implicit none

		type(vtk_file) :: vtk_field_file

		type(pvtk_file) :: pvtk_field_file
	!------------------------------------------------------------------------------------------------------------------

		character(len=*) :: boundaryName
		!! boundary name

		character(len=*) :: fmt
		!! output file format 

		character(len=:), allocatable :: fname
		!! file name

		character(len=10) :: file_number
		!! file number as string

		character(len=10) :: rank_number
		!! processor number as string

		character(len=10) ::procID
		!! processor if as string

		character(len=:), allocatable :: dirName
		!! output directory name
	!------------------------------------------------------------------------------------------------------------------

		integer :: i, iBoundary
		!! loop indices
		
		integer :: targetBoundary
		!! target boundary

		integer :: ierr
		!! error flag

		integer :: nComp
		!! number of componenets
	!------------------------------------------------------------------------------------------------------------------

		logical :: dirExists
		!! flag to check if a directory exists
	!------------------------------------------------------------------------------------------------------------------

		i = 0
		do iBoundary=1,numberOfBoundaries
			if(adjustl(trim(mesh%boundaries%userName(iBoundary)))==adjustl(trim(boundaryName))) then
				targetBoundary = iBoundary
				exit
			end if
			i = i+1
		end do

		if(i==numberOfBoundaries) call flubioStopMsg('FLUBIO: boundary '//trim(boundaryName)//' not found, check the spelling!')

		write(rank_number, '(i0)') id

		! Create a directory for the field. If it does not exists create it.
		dirName = 'postProc/VTKfields/Parallel/mesh'
		inquire(file=trim(dirName)//'/.', exist=dirExists)

		if(.not. dirExists) call execute_command_line ('mkdir -p ' // adjustl(trim(dirName)))

		fname='postProc/VTKfields/Parallel/mesh/'//trim(boundaryName)//'-d'//trim(rank_number)//'.vtu'

		! Header
		ierr = vtk_field_file%initialize(format=fmt, filename=fname, mesh_topology='UnstructuredGrid')

		! Coordinates and connectivities
		call writeBoundaryCoordinatesVTK(vtk_field_file, targetBoundary)

		! Finalising
		ierr = vtk_field_file%xml_writer%write_dataarray(location='cell', action='close')
		ierr = vtk_field_file%xml_writer%write_piece()
		ierr = vtk_field_file%finalize()

		! Master rank writes the pvtu file
		if(id==0) then

			! Write the pvtu file
			fname='postProc/VTKfields/Parallel/mesh/'//trim(boundaryName)//'.pvtu'
			ierr = pvtk_field_file%initialize(filename=fname, mesh_topology='PUnstructuredGrid', mesh_kind="Float64")

			ierr = pvtk_field_file%xml_writer%write_dataarray(location='cell', action='open')
			ierr = pvtk_field_file%xml_writer%write_dataarray(location='cell', action='close')

			do i=1,nid
				write(procID,'(i0)') i-1
				fname='./'//trim(boundaryName)//'-d'//trim(procID)//'.vtu'
				ierr = pvtk_field_file%xml_writer%write_parallel_geo(source=fname)
			end do

			ierr = pvtk_field_file%finalize()

		end if

		call flubioMsg('FLUBIO: boundary "'//trim(boundaryName)//'" has been written!')

	end subroutine processBoundaryMeshVTK

! *********************************************************************************************************************!

	subroutine writeBoundaryCoordinatesVTK(vtk_field_file, iBoundary)

		use flubioMpi
		use meshvar
		use penf
		use vtk_fortran, only : vtk_file

		implicit none

		type(vtk_file) :: vtk_field_file

		character(len=10) :: procID
	!------------------------------------------------------------------------------------------------------------------

		integer :: i, n, iBoundary, iBFace, is, ie, isp, iep, is_offset, ie_offset, ierr
		!! loop indices 

		integer :: np, nf,npb, nConn, fsize	
		!! work variables
	
		integer, dimension(:), allocatable :: nfn 
		!! number of face nodes

		integer, dimension(:,:), allocatable :: mapper
		!! array mapping the boundary nodes

		integer :: bPointsSize(numberOfBoundaries)
		!! size of boundary points for each boundary

		integer(I1P), dimension(:), allocatable :: cell_type
		!! cell type
	
		integer(I4P), dimension(:), allocatable :: offset
		!! offset for each boundary face 
		
		integer(I4P), dimension(:), allocatable :: connect
		!! connectivities

		integer, dimension(:), allocatable :: orientedConn
		!! mapper containing the oriented indices for the face nodes 
		
		integer, dimension(:), allocatable :: faceConn
		!! array hosting the face nodes
	!------------------------------------------------------------------------------------------------------------------

		real, dimension(:,:), allocatable :: points
		!! boundary points

		real, dimension(:,:,:), allocatable :: bVertex
		!! boundary vertices composing each boundary

		real :: facen(3)
		!! face normal
	!------------------------------------------------------------------------------------------------------------------

		write(procID,'(i0)') id

		! Read dimensions
		open(1, file='grid/bin/boundaries-d'//trim(procID)//'.bin', form='unformatted')
			read(1) i, i, i, i, n
		close(1)

		! Allocate
		allocate(nfn(numberOfFaces))
		allocate(mapper(n, numberOfBoundaries))
		allocate(bVertex(numberOfPoints, 3, numberOfBoundaries))
	
		! Read data
		open(1, file='grid/bin/boundaries-d'//trim(procID)//'.bin', form='unformatted')
			read(1)
			read(1)
			read(1)
			read(1)
			read(1) mapper
			read(1)
			read(1) bPointsSize
			read(1) bVertex
			read(1) nfn
		close(1)

		! Coordinates
		npb = bPointsSize(iBoundary)
		ierr = vtk_field_file%xml_writer%write_piece(np=npb, nc=mesh%boundaries%nFace(iBoundary))
		ierr = vtk_field_file%xml_writer%write_geo(np=bPointsSize(iBoundary),           &
												   nc=mesh%boundaries%nFace(iBoundary), &
												   x=bVertex(1:npb,1,iBoundary),        &
												   y=bVertex(1:npb,2,iBoundary),        &
												   z=bVertex(1:npb,3,iBoundary))

		! Connectivities
		is = mesh%boundaries%startFace(iBoundary)
		ie = is + mesh%boundaries%nFace(iBoundary)-1
		nConn = sum(nfn(is:ie))

		allocate(cell_type(mesh%boundaries%nFace(iBoundary)))
		allocate(offset(mesh%boundaries%nFace(iBoundary)))
		allocate(connect(nConn))

		offset = 0
		is_offset = 1
		ie_offset = is_offset + nfn(is)-1

		i = 0
		do iBFace=is, ie

			i = i+1

			! Check the cell type
			cell_type(i) = 7

			if(i==1) then
				offset(i) = nfn(iBFace)
			else
				offset(i) = offset(i-1) + nfn(iBFace)
			end if

			allocate(orientedConn(nfn(iBFace)))
			allocate(faceConn(nfn(iBFace)))
			
			! Orient face nodes counter-clockwise
			facen = mesh%Sf(iBFace,1:3)/norm2(mesh%Sf(iBFace,:))
			faceConn = mapper(is_offset:ie_offset, iBoundary)
			orientedConn = orientPointsInPlane(points=bVertex(faceConn,1:3, iBoundary), normal=facen, & 
															  center=mesh%fcentroid(iBFace,1:3), mode='ascending', np=nfn(iBFace))

			!connect(is_offset:ie_offset) = mapper(is_offset:ie_offset, iBoundary)-1
			connect(is_offset:ie_offset) = faceConn(orientedConn)-1

			is_offset = ie_offset + 1
			if(iBFace<ie) ie_offset = is_offset + nfn(iBFace+1)-1

			! Deallocate for the next face
			deallocate(orientedConn)
			deallocate(faceConn)

		enddo

		ierr = vtk_field_file%xml_writer%write_connectivity(nc=mesh%boundaries%nFace(iBoundary), &
															connectivity=connect,                &
															offset=offset,                       &
															cell_type=cell_type)

		ierr = vtk_field_file%xml_writer%write_dataarray(location='cell', action='open')


	end subroutine writeBoundaryCoordinatesVTK

! *********************************************************************************************************************

	subroutine writeParallelBoundaryFieldsVTK(vtk_field_file, currentTime, iBoundary, fields, fieldComp)

		use flubioMpi
		use meshvar
		use m_strings
		use penf
		use vtk_fortran, only : vtk_file

		implicit none

		type(vtk_file) :: vtk_field_file
	!------------------------------------------------------------------------------------------------------------------

		character(len=*) :: fields
		!! fields to process coming as a comma separated string

		character(len=10) :: procID
		!! processor ID

		character(len=10) :: timeStamp	
		!! time to process as string

		character(len=15) :: fieldName
		!! field name as string

		character(len=10), dimension(:), allocatable :: fieldList
		!! list of fields to process

		character(len=:), allocatable :: file_name
		!! file name
	!------------------------------------------------------------------------------------------------------------------

		integer :: i, ii, j, isb, ieb, iRank, iElement, iBoundary, iBFace, iComp, iField, patchFaceB
		!! loop index variables
		
		integer :: nComp
		!! number of components

		integer :: nFields 
		!! number of fields

		integer :: bSize
		!! boundary points size

		integer :: boffset
		!! boundary faces offset

		integer :: currentTime
		!! current time being processed

		integer :: ierr
		!! error flag

		integer, dimension(:), allocatable :: fieldComp
	!------------------------------------------------------------------------------------------------------------------

		real, dimension(:,:), allocatable :: phi
		!! array to store the field values
	!------------------------------------------------------------------------------------------------------------------

		!----------------------------!
		! Find out the field to read !
		!----------------------------!

		fieldList = getFieldList(fields)
		nFields = size(fieldList)

		allocate(fieldComp(nFields))

		! Loop over fields
		do iField=1,nFields

			call flubioMsg('FLUBIO: processing field: '//trim(fieldList(iField)))

			! Initialize arrays
			write(procID,'(i0)') id
			write(timeStamp,'(i0)') currentTime

			file_name = 'postProc/fields/'//trim(timeStamp)//'/'//trim(fieldList(iField))//'-d'//trim(procID)//'.bin'
			open(1,file=file_name,form='unformatted')

				! Read fields for the i-th partion
				read(1) nComp
				read(1) fieldName

				! Allocate field matrix
				allocate(phi(numberOfElements+numberOfBFaces, nComp))
				phi = 0.0
				fieldComp = nComp

				do iComp=1,nComp
					read(1) phi(:,iComp)
					read(1)
					read(1)
				enddo

			close(1)

			isb = numberOfElements + (mesh%boundaries%startFace(iBoundary) - numberOfIntFaces)
			ieb = isb + mesh%boundaries%nFace(iBoundary) -1

			if(nComp==1) then
				ierr = vtk_field_file%xml_writer%write_dataarray(data_name=trim(fieldName), x=phi(isb:ieb,1))
			else
				ierr = vtk_field_file%xml_writer%write_dataarray(data_name=trim(fieldName), &
						                                         x=phi(isb:ieb,1),          &
																 y=phi(isb:ieb,2),          &
																 z=phi(isb:ieb,3))
			end if

			! Deallocate and reallocate with a new size on the partition
			deallocate(phi)

		end do

	end subroutine writeParallelBoundaryFieldsVTK

! *********************************************************************************************************************

	function getNumberOfFields(listName) result(nFields)

		use m_strings

		implicit none

		character(len=*) :: listName

		character(len=10) :: dummy_char

		character(len=10), dimension(:), allocatable :: charr
	!------------------------------------------------------------------------------------------------------------------

		integer :: iField, nFields, io
	!------------------------------------------------------------------------------------------------------------------

		nFields = 0
		open(1, file = 'postProc/fields/'//listName)
			do
				read(1,*,iostat=io)
				if(io/=0) exit
				nFields = nFields + 1
			end do
		close(1)

	end function getNumberOfFields

! *********************************************************************************************************************

	function getFieldList(fields) result(fieldList)

		use m_strings

		implicit none

		character(len=*) :: fields

		character(len=10) :: dummy_char

		character(len=10), dimension(:), allocatable :: fieldList
	!------------------------------------------------------------------------------------------------------------------

		integer :: iField, nFields, io
	!------------------------------------------------------------------------------------------------------------------

		if(trim(fields)=='empty') then
			nFields = getNumberOfFields('fieldList.txt')
			allocate(fieldList(nFields))
			open(1, file = 'postProc/fields/fieldList.txt')
				do iField=1,nFields
					read(1,*) dummy_char
					fieldList(iField) = trim(dummy_char)
				end do
			close(1)
		else
			call split_in_array(fields, fieldList,',')
			nFields = size(fieldList)
		end if

	end function getFieldList

! *********************************************************************************************************************	

end module parallelVTKFormat