!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

	program flubioFields

		use meshvar
		use parallelVTKFormat, only: processDataVTK

		implicit none

		character(len=10) :: fmt
		!! file format

		character(len=100) :: fields
		!! fields to process as comma separated string

		character(len=20), dimension(:), allocatable :: args
		!! arguments list
	!------------------------------------------------------------------------------------------------------------------

		integer :: startField
		!! first field time to process
		
		integer :: endField
		!! last field time to process
		
		integer :: skipField
		!! number of fields to skip
		
		integer :: nargs
		!! number of arguments
		
		integer :: ierr
		!! error flag
	!------------------------------------------------------------------------------------------------------------------

		call mpi_init(ierr)

		! Initialize MPI variables
		call setMPIvar()

		!------------------------!
		! Command line arguments !
		!------------------------!

		nargs = command_argument_count()

		if(nargs>0) then
			allocate(args(nargs))
			call parseCommandLine(args, nargs, startField, endField, skipField, fields, fmt)
		else
			call flubioStopMsg("ERROR: Please specify all the command line options. Run with -help for more info.")
		endif

		!------------------!
		! read serial mesh !
		!------------------!

		call mesh%readMeshFromFiles()

		!------------------!
		! Write data files !
		!------------------!

		call processDataVTK(startField, endField, skipField, fields, trim(fmt))

		call mpi_finalize(ierr)

    end program flubioFields

! *********************************************************************************************************************

	subroutine parseCommandLine(args, nargs, startField, endField, skipField, fields, fmt)

		use flubioMpi
		use m_refsor

		implicit none

		character(len=10) :: fmt
		!! file format

		character(len=100) :: fields
		!! fields to process as comma separated string

		character(len=*) :: args(nargs)
		!! arguments list
	!------------------------------------------------------------------------------------------------------------------

		integer :: startField
		!! first field time to process
		
		integer :: endField
		!! last field time to process
		
		integer :: skipField
		!! number of fields to skip
		
		integer :: nargs
		!! number of arguments
		
		integer :: ierr
		!! error flag
        integer :: p
        !! loop index
        
        integer :: iTime
        !! loop index
        
        integer :: nTimes
        !! number of times to process
        
        integer :: stat
        !! file opener flag

        integer, dimension(:), allocatable :: timeList
        !! list of times as integer list
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
        !! flag
	!------------------------------------------------------------------------------------------------------------------

		nTimes = 0
		found = .false.
		fmt = 'binary'

		! Get the arguments
		do p=1,nargs
		   call get_command_argument(number=p, value=args(p), status=stat)
		enddo

		! proces the options
		do p=1,nargs

			! Look for the start and initial fields
		   if(args(p)=='-start') then

				read(args(p+1),*) startField

		   elseif(args(p)=='-end') then

			  	read(args(p+1),*) endField

		   elseif(args(p)=='-skip') then

			   read(args(p+1),*) skipField

		   elseif(args(p)=='-time') then

				read(args(p+1),*) startField
			  	read(args(p+1),*) endField
			  	skipField=1

		   elseif(args(p)=='-fields') then

				fields = args(p+1)
				found = .true.

			elseif(args(p)=='-format') then

				fmt = trim(args(p+1))
			
		   elseif(args(p)=='-all') then

			   ! Generate time list
			   if(id==0) call execute_command_line('generateTimeList.sh')

			   open(1,file='postProc/fields/timeList.txt')
				   do
					   read(1,*,iostat=stat)
					   if(stat/=0) exit
					   nTimes = nTimes + 1
				   end do
			   close(1)

			   allocate(timeList(nTimes))
			   open(1,file='postProc/fields/timeList.txt')
				   do iTime=1,nTimes
					   read(1,*) timeList(iTime)
				   end do
			   close(1)

			   ! Sort the list
			   call refsor(timeList)

			   if(nTimes>1) then
				   startField = timeList(2)
				   endField = timeList(nTimes-1)
				   skipField = timeList(3)-timeList(2)
			   else
				   startField = timeList(1)
				   endField = timeList(nTimes)
				   skipField = 1
			   end if

		   elseif(args(p)=='-help') then

			   write(*,*) 'Command line options:'
			   write(*,*) '-np: number of processors used'
			   write(*,*) '-start: starting field to process'
			   write(*,*) '-end: ending field to process'
			   write(*,*) '-skip: skip  n fields to process'
			   write(*,*) '-time: field (just one) to process'
			   write(*,*) '-fields: comma separated list of fields to process'
			   write(*,*) '-all: flag to process all fields at all times'
			   write(*,*)
			   write(*,*) 'example: flubio_boundaryFields -np 4 -start 0 -end 10 -skip 2'

			   stop

		   endif

		enddo

		if(.not. found) then
			fields = 'empty'
		end if

		! Check for the mandatory arguments
		call check_field(args, nargs)

	end subroutine parseCommandLine

! *********************************************************************************************************************