!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

    program flubioBoundaryField

        use meshvar
        use mpi
        use m_strings
        use parallelVTKFormat, only: processBoundaryDataVTK

        implicit none

        logical :: boundaryFlag
    !------------------------------------------------------------------------------------------------------------------

		character(len=10) :: fmt
		!! file format

		character(len=100) :: fields
		!! fields to process as comma separated string

        character(len=500) :: boundary
        !! list of boudaries to process

		character(len=20), dimension(:), allocatable :: args
		!! arguments list

        character(len=50), dimension(:), allocatable :: boundaryList
        !! list of boundaries to process
	!------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary

		integer :: startField
		!! first field time to process
		
		integer :: endField
		!! last field time to process
		
		integer :: skipField
		!! number of fields to skip
		
		integer :: nargs
		!! number of arguments
		
		integer :: ierr
		!! error flag
    !------------------------------------------------------------------------------------------------------------------

        call mpi_init(ierr)

        ! Initialize MPI variables
        call setMPIvar()

        !------------------!
        ! read serial mesh !
        !------------------!

        call mesh%readMeshFromFiles()

        !------------------------!
        ! Command line arguments !
        !------------------------!

        nargs = command_argument_count()
        if(nargs>0) then
            allocate(args(nargs))
            call parseCommandLine(args, nargs, startField, endField, skipField, boundary, fields, fmt)

			if(boundaryFlag) then
				call split_in_array(boundary, boundaryList, ',')
			else 
				boundaryList = mesh%boundaries%userName
			endif

        else
            call flubioStopMsg("ERROR: Please specify all the command line options. Run with -help for more info.")
        endif

        !------------------!
        ! Write data files !
        !------------------!

        do iBoundary=1,size(boundaryList)
            if(.not. matchw(trim(boundaryList(iBoundary)), 'procBoundary*')) then
                call processBoundaryDataVTK(startField, endField, skipField, trim(boundaryList(iBoundary)), fields, trim(fmt))
            endif
        end do

        call mpi_finalize(ierr)

    end program flubioBoundaryField

! *********************************************************************************************************************

    subroutine parseCommandLine(args, nargs, startField, endField, skipField, boundary, fields, fmt)

        use flubioMpi
        use m_refsor

        implicit none

        character(len=*) :: boundary
        !! boundary to process as comma separated string

		character(len=10) :: fmt
		!! file format

		character(len=100) :: fields
		!! fields to process as comma separated string

        character(len=*) :: args(nargs)
		!! arguments list
	!------------------------------------------------------------------------------------------------------------------

		integer :: startField
		!! first field time to process
		
		integer :: endField
		!! last field time to process
		
		integer :: skipField
		!! number of fields to skip
		
		integer :: nargs
		!! number of arguments
		
		integer :: ierr
		!! error flag
    
        integer :: p
        !! loop index
        
        integer :: iTime
        !! loop index
        
        integer :: nTimes
        !! number of times to process
        
        integer :: stat
        !! file opener flag

        integer, dimension(:), allocatable :: timeList
        !! list of times as integer list
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
        !! flag
    !------------------------------------------------------------------------------------------------------------------

        nTimes = 0
        found = .false.
        fmt = 'binary'

        ! Get the arguments
        do p=1,nargs
            call get_command_argument(number=p, value=args(p), status=stat)
        enddo

        ! Proces the options
        do p=1,nargs

            ! Look for the start and initial fields
            if(args(p)=='-start') then

                read(args(p+1),*) startField

            elseif(args(p)=='-end') then

                read(args(p+1),*) endField

            elseif(args(p)=='-skip') then

                read(args(p+1),*) skipField

            elseif(args(p)=='-boundary') then

                boundary = args(p+1)

            elseif(args(p)=='-time') then

                read(args(p+1),*) startField
                read(args(p+1),*) endField
                skipField=1

            elseif(args(p)=='-fields') then

                fields = args(p+1)
                found = .true.

            elseif(args(p)=='-format') then

                    fmt = trim(args(p+1))
                
            elseif(args(p)=='-all') then

                ! Generate time list
                if(id==0) call execute_command_line('generateTimeList.sh')

                open(1,file='postProc/fields/timeList.txt')
                    do
                        read(1,*,iostat=stat)
                        if(stat/=0) exit
                        nTimes = nTimes + 1
                    end do
                close(1)

                allocate(timeList(nTimes))
                open(1,file='postProc/fields/timeList.txt')
                    do iTime=1,nTimes
                        read(1,*) timeList(iTime)
                    end do
                close(1)

                ! Sort the list
                call refsor(timeList)

                if(nTimes>1) then
                    startField = timeList(2)
                    endField = timeList(nTimes-1)
                    skipField = timeList(3)-timeList(2)
                else
                    startField = timeList(1)
                    endField = timeList(nTimes)
                    skipField = 1
                end if

            elseif(args(p)=='-help') then

                call flubioMsg('Command line options:')
                call flubioMsg('-np: number of processors used')
                call flubioMsg('-start: starting field to process')
                call flubioMsg('-end: ending field to process')
                call flubioMsg('-skip: skip  n fields to process')
                call flubioMsg('-time: field (just one) to process')
                call flubioMsg('-boundary: target boundary to process. List of boundaries are comma separeted with no spaces.')
                call flubioMsg('-fields: comma separated list of fields to process')
                call flubioMsg('-all: flag to process all fields at all times')
                call flubioMsg('')
                call flubioMsg('example: flubio_boundaryFields -np 4 -start 0 -end 10 -skip 2 -boundary b1,b2,b3')

                call flubioStopMsg('')

            endif

        enddo

        if(.not. found) then
            fields = 'empty'
        end if

        ! Check for the mandatory arguments
        call check_field(args, nargs)

    end subroutine parseCommandLine

! *********************************************************************************************************************
