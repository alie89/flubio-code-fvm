!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module flubioSerialBoundaries

    use globalMeshVar
    use generalDictionaries
    use m_strings, only: isdigit, isalpha, string_to_value, crop, substitute, split_in_array, matchw, compact, nospace 
    use strings
    use serialNodes, only: skipHeader, findStreamSize, checkFormat

    implicit none

    type, public :: fvBoundaries

        type(generalDict) :: boundaryDict

        integer :: numberOfBoundaries

        integer :: numberOfPatches

        integer :: numberOfRealBound

        integer :: maxProcFaces

        integer :: maxWallFaces

        character(30), dimension(:), allocatable :: userName

        character(30), dimension(:), allocatable :: bcType

        integer, dimension(:), allocatable :: startFace

        integer, dimension(:), allocatable :: nFace

        integer, dimension(:), allocatable :: bind

        integer, dimension(:), allocatable :: realBound

        integer, dimension(:), allocatable :: procBound

        integer, dimension(:), allocatable :: myProc

        integer, dimension(:), allocatable :: neighProc

        integer, dimension(:), allocatable :: wallBound

        contains

            procedure:: readBoundNumber
          
            procedure :: readBoundariesFromFiles
            procedure :: readBoundariesFromFilesOFAscii
            procedure :: readBoundariesFromFilesFlubioAscii

    end type fvBoundaries

contains

    subroutine readBoundNumber(this)

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        open(1, file='grid/serial/grid/boundary')
            read(1,*) this%numberOfBoundaries
        close(1)

        numberOfBoundaries = this%numberOfBoundaries

    end subroutine readBoundNumber

! *********************************************************************************************************************

    subroutine readBoundariesFromFiles(this)

    !==================================================================================================================
    ! Description:
    !! readBoundaries reads the boundary mesh files and the boundary conditions.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: fileFormat
        !! file format
    !------------------------------------------------------------------------------------------------------------------

        call checkFormat('grid/serial/grid/boundary', fileFormat)

        if (fileFormat=='openfoam_ascii') then
            call this%readBoundariesFromFilesOFAscii()
        elseif(fileFormat=='openfoam_binary') then
            call this%readBoundariesFromFilesOFAscii()
        elseif(fileFormat=='flubio_ascii') then 
            call this%readBoundariesFromFilesFlubioAscii()
        else
           write(*,*) 'ERROR: cannot recognize file format for faces.'
           stop
        endif   

    end subroutine readBoundariesFromFiles

! *********************************************************************************************************************

    subroutine readBoundariesFromFilesOFAscii(this)

    !==================================================================================================================
    ! Description:
    !! readBoundaries reads the boundary mesh files and the boundary conditions.
    !==================================================================================================================

        class(fvBoundaries) :: this

        type(json_value), pointer :: p

        type(json_core) :: json 
    !------------------------------------------------------------------------------------------------------------------

        character(len=100) :: line
        !! parsed line

        character(len=:), allocatable :: croppedLine
        !! cropped line

        character(len=:), allocatable :: oneLine
        !! cropped line

        character(len=50), allocatable :: carray(:)
        !! splitted line
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iProc
    !------------------------------------------------------------------------------------------------------------------

        integer :: ierr
        !! error flag

        integer :: nb 
        !! number of boundaries
    !------------------------------------------------------------------------------------------------------------------
        
        logical :: isWord
    !------------------------------------------------------------------------------------------------------------------

        call json%initialize()
        call json%create_object(this%boundaryDict%jsonDict, 'boundaryDict')

        numberOfPeriodicBound = 0
        totalPerBound = 0
        
        open(1,file='grid/serial/grid/boundary')

            call skipHeader(aunit=1, fileFormat='ascii')
            call findStreamSize(aunit=1, fileFormat='ascii', streamSize=this%numberOfBoundaries, oneLine=oneLine)
            numberOfBoundaries = this%numberOfBoundaries

            allocate(this%userName(numberOfBoundaries))
            allocate(this%bcType(numberOfBoundaries))
            allocate(this%startFace(numberOfBoundaries))
            allocate(this%nFace(numberOfBoundaries))
         
            iBoundary = 0
            ! loop over the file to identify boundaries and keywords
            do
                call readLine(1, line, ierr)
                croppedLine = crop(line)
                call substitute(line,';','')   
                isWord = isalpha(crop(compact(line)))
                call split_in_array(line, carray, ' ')
                                
                ! That's the patch name!
                if(isWord .and. size(carray)==1) then

                    if(iBoundary>0) then
                        call json%add(this%boundaryDict%jsonDict, p) 
                        nullify(p)
                    endif

                    iBoundary = iBoundary+1
                    this%userName(iBoundary) = carray(1)
                    call json%create_object(p, crop(this%userName(iBoundary)))

                endif

                ! Find type
                if(size(carray)>1 .and. iBoundary>0) then

                    if(carray(1)=='type') this%bcType(iBoundary) = carray(2)
                    if(carray(1)=='nFaces') call string_to_value(carray(2), this%nFace(iBoundary), ierr)
                    if(carray(1)=='startFace') call string_to_value(carray(2), this%startFace(iBoundary), ierr)   
   
                    ! Populate boundary sub-dictionary
                    call json%add(p, crop(carray(1)), crop(carray(2)))

                endif    
                     
                if(ierr /= 0) exit

            enddo   

        close(1)

        ! Allocate original disposition of start faces to be use in calculation of the offset vector
        this%startFace = this%startFace+1

    end subroutine readBoundariesFromFilesOFAscii

! **********************************************************************************************************************

    subroutine readBoundariesFromFilesFlubioAscii(this)

    !==================================================================================================================
    ! Description:
    !! readBoundaries reads the boundary mesh files and the boundary conditions.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: json

        type(json_value), pointer :: dictPointer, boundaryPointer
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: tmp
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary
        !! target boundary 

        integer :: ival 
        !! dummy variable
    
        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        call this%boundaryDict%initJSON('grid/serial/grid/boundary_test')

        ! Get number of boundaries
        call this%boundaryDict%json%get('numberOfBoundaries', this%numberOfBoundaries, found)
        call raiseKeyErrorJSON('numberOfBoundaries', found, 'grid/serial/grid/boundary')
        numberOfBoundaries = this%numberOfBoundaries

        numberOfPeriodicBound = 0
        totalPerBound = 0
       
        allocate(this%userName(numberOfBoundaries))
        allocate(this%bcType(numberOfBoundaries))
        allocate(this%startFace(numberOfBoundaries))
        allocate(this%nFace(numberOfBoundaries))
        allocate(this%myProc(numberOfBoundaries))
        allocate(this%neighProc(numberOfBoundaries))
   
        allocate(cyclic(numberOfBoundaries))
       
        this%myProc = -1
        this%neighProc = -1
       
        call this%boundaryDict%json%get(dictPointer)
        do iBoundary=1,numberOfBoundaries

            call json%get_child(dictPointer, iBoundary+1, boundaryPointer, found)
            call json%info(boundaryPointer, name=tmp)

            this%userName(iBoundary) = tmp

            call json%get(boundaryPointer, 'type', tmp)
            call raiseKeyErrorJSON('type', found, 'grid/serial/grid/boundary')
            this%bcType(iBoundary) = tmp

            call json%get(boundaryPointer, 'startFace', ival)
            call raiseKeyErrorJSON('type', found, 'grid/serial/grid/boundary')
            this%startFace(iBoundary) = ival

            call json%get(boundaryPointer, 'nFaces', ival)
            call raiseKeyErrorJSON('type', found, 'grid/serial/grid/boundary')
            this%nFace(iBoundary) = ival

            nullify(boundaryPointer)

        enddo

        ! Allocate original disposition of start faces to be use in calculation of the offset vector
        this%startFace = this%startFace+1

        ! Clean up 
        nullify(dictPointer)   

    end subroutine readBoundariesFromFilesFlubioAscii

! **********************************************************************************************************************

    subroutine readLines(info, endLine, ninfo, nread, nskip)

        character(30) :: auxchar(nread+2), info(ninfo)
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, is, ie, j, ninfo, nread, nskip, endLine
    !------------------------------------------------------------------------------------------------------------------

        open(1,file='grid/serial/grid/boundary')

            do is=1,nskip
                read(1,*)
            enddo

            is = nskip+1
            ie = is+nread
            j = 0

            do i=1,nread
                j = j+1
                read(1,*) auxchar(j)
            enddo

            ! If the bondary is a periodic, read one more line
            if (auxchar(3)=='processorCyclic' .or. auxchar(3)=='cyclic') then
                j = j+1
                read(1,*)  auxchar(j) ! keyword name

                j = j+1
                read(1,*) auxchar(j)  ! keyword
            endif

        close(1)

        j = 1
        info(1) = auxchar(1)

        do i=3,nread,2
            j = j+1
            info(j) = auxchar(i)
        enddo

        i = nread

        ! If a boundary is periodic read neighProc or refPatch
        if (auxchar(3)=='processorCyclic' .or. auxchar(3)=='cyclic') then
            j = j+1
            info(j) = auxchar(i+2)
            endLine = ie-1+2
        else
            endLine = ie-1
        endif

    end subroutine readLines

! *********************************************************************************************************************

end module flubioSerialBoundaries

