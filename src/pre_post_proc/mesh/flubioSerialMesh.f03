!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module flubioSerialMesh

    use serialElements
    use globalMeshVar
    use flubioSerialBoundaries
    use auxilaries
    use m_unista
    use m_refsor
    use iDynamicArray_Class

    implicit none

    type, public, extends(grid_elements) :: fvMesh

        type(fvBoundaries) :: boundaries

        contains

            procedure :: readMeshFromFiles

            procedure :: compute_geom
            procedure :: computeAreas
            procedure :: computeVolumes

            procedure :: findBoundaryPoints
            procedure :: buildNodesToElementConn

            procedure :: getFacesAndOffsets

            procedure :: saveMesh

    end type fvMesh

contains

    subroutine readMeshFromFiles(this)

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        ! Starting message
        write(*,*)
        write(*,*) 'FLUBIO: initialising the serial mesh'
        write(*,*) '=============================================================='

        !---------------!
        ! Internal mesh !
        !---------------!

        ! Read Points
        call this%readMeshPoints()

        ! Read Faces, Owner, Neighbours
        call this%readMeshFaces()

        ! Build elements
        call this%buildElements()

        !---------------!
        ! Boundary mesh !
        !---------------!

        call this%boundaries%readBoundariesFromFiles()

        call this%findBoundaryPoints()

        !----------------------------------------!
        ! Compute Control Volume characteristics !
        !----------------------------------------!

        call this%compute_geom()

        !------------------!
        ! Save serial Mesh !
        !------------------!

        call this%saveMesh()

    end subroutine readMeshFromFiles

!**********************************************************************************************************************

    subroutine compute_geom(this)

        class(fvMesh) :: this
!------------------------------------------------------------------------------------------------------------------

    !------------------------------------------!
    ! Compute faces areas and elements volumes !
    !------------------------------------------!

        call this%computeAreas()

        call this%computeVolumes()

    end subroutine compute_geom

! *********************************************************************************************************************

    subroutine computeVolumes(this)

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iNode, iFace, iBFace, nFaces, numberOfiNodes

        integer, dimension(:), allocatable :: iFaces
    !------------------------------------------------------------------------------------------------------------------

        real :: centre(3), centroid(3), Sf(3), Cf(3)

        real :: localFaceSign, localVolumeSum, localVolume, localVolumeCentroidSum(3), localCentroid(3), volume
    !------------------------------------------------------------------------------------------------------------------

        ! Starting message
        write(*,*) 'FLUBIO: computing elements centroids and volumes...'

        !-------------------------------------!
        ! Compute volume and element centroid !
        !-------------------------------------!

        allocate(this%centroid(3,numberOfElements+numberOfBFaces+1))
        allocate(this%volume(numberOfElements))
        allocate(this%oldVolume(numberOfElements))

        do iElement=1,numberOfElements

            nFaces = this%iFaces(iElement)%csize
            allocate(iFaces(nFaces))
            numberOfElementFaces(iElement) = nFaces
            iFaces = this%iFaces(iElement)%col(1:nFaces)

            nFaces = numberOfElementFaces(iElement)
            centre = 0.0

            do iFace=1,nFaces
                centre = centre + this%fcentroid(iFaces(iFace),:)
            enddo

            centroid = 0.0
            Sf = 0.0

            centre = centre/nFaces
            localVolumeCentroidSum = 0.0
            localVolumeSum = 0.0

            do iFace=1,nFaces

                localFaceSign = this%elmFaceSign(iElement)%col(iFace)

                Sf = this%Sf(iFaces(iFace),:)*localFaceSign
                Cf = this%fcentroid(iFaces(iFace),:)-centre

                localVolume = Sf(1)*Cf(1)+Sf(2)*Cf(2)+Sf(3)*Cf(3)
                localVolume = localVolume/3.
                localCentroid = 0.75*this%fcentroid(iFaces(iFace),:)+0.25*centre
                localVolumeCentroidSum = localVolumeCentroidSum + localCentroid*localVolume
                localVolumeSum = localVolumeSum + localVolume

            enddo

            centroid = localVolumeCentroidSum/localVolumeSum

            volume = localVolumeSum

            this%volume(iElement) = volume
            this%OldVolume(iElement) = volume
            this%centroid(:,iElement) = centroid

            deallocate(iFaces)

        enddo

    end subroutine computeVolumes

! *********************************************************************************************************************

    subroutine computeAreas(this)

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer  iFace, iBFace, iNode, iTriangle, nFaces, numberOfiNodes

        integer, dimension(:), allocatable :: iNodes
    !------------------------------------------------------------------------------------------------------------------

        real point1(3), point2(3), point3(3), centre(3), v1(3), v2(3), cross(3)

        real centroid(3), local_centroid(3), Sf(3), area, local_area, local_Sf(3)
    !------------------------------------------------------------------------------------------------------------------

        ! Starting message
        write(*,*) 'FLUBIO: computing faces area and normals...'

        !---------------------!
        ! Compute Face center !
        !---------------------!

        nFaces = this%numberOfFaces

        allocate(this%fcentroid(nFaces,3))
        allocate(this%Sf(nFaces,3))

        this%fcentroid = 0.0
        this%Sf = 0.0

        !-----------------!
        ! Loop over faces !
        !-----------------!

        do iFace=1,nFaces

            numberOfiNodes = numberOfFaceNodes(iFace)
            allocate(iNodes(numberOfiNodes))

            iNodes = this%fvertex(iFace)%col(1:numberOfiNodes)
            centre = 0.0

            !--------------!
            ! Face Centers !
            !--------------!

            do iNode=1,numberOfiNodes
                centre = centre + this%vertex(iNodes(iNode),:)
            enddo

            centre = centre/numberOfiNodes

            !-------------------!
            ! Areas and normals !
            !-------------------!

            Sf = 0.0
            area= 0.0
            centroid = 0.0

            do iTriangle=1,numberOfiNodes

                point1=centre
                point2=this%vertex(iNodes(iTriangle),:)

                if (iTriangle<numberOfiNodes) then
                    point3 = this%vertex(iNodes(iTriangle+1),:)
                else
                    point3 = this%vertex(iNodes(1),:)
                endif

                local_centroid=(point1+point2+point3)/3.
                v1=point2-point1
                v2=point3-point1

                call cross_prod_aux(v1,v2,cross)

                local_Sf=0.5*cross
                local_area=sqrt(local_Sf(1)**2+local_Sf(2)**2+local_Sf(3)**2)

                centroid = centroid + local_area*local_centroid
                Sf = Sf + local_Sf
                area = area + local_area

            enddo

            centroid = centroid/area
            this%fcentroid(iFace,:) = centroid
            this%Sf(iFace,:) = Sf

            deallocate(iNodes)

        enddo

    end subroutine computeAreas

! *********************************************************************************************************************

    subroutine findBoundaryPoints(this)

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dirName
        !! directory name to save the data
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iBFace, is, ie, isp, iep

        integer :: psize(numberOfBoundaries), bPointsSize(numberOfBoundaries), nEntries(numberOfBoundaries)

        integer, dimension(:,:), allocatable :: mapper
    !------------------------------------------------------------------------------------------------------------------

        real, dimension(:,:,:), allocatable :: bVertex
    !------------------------------------------------------------------------------------------------------------------

        integer, dimension(:,:), allocatable :: allPoints, bPoints
    !------------------------------------------------------------------------------------------------------------------

        logical :: dirExists
    !------------------------------------------------------------------------------------------------------------------

        allocate(allPoints(maxval(this%boundaries%nFace)*maxval(numberOfFaceNodes), numberOfBoundaries))
        allocate(bPoints(maxval(this%boundaries%nFace)*maxval(numberOfFaceNodes), numberOfBoundaries))
        allocate(bVertex(numberOfPoints, 3, numberOfBoundaries))
        allocate(mapper(maxval(this%boundaries%nFace)*maxval(numberOfFaceNodes), numberOfBoundaries))

        ! Build the point List
        allPoints = 2000000000
        mapper = -1
        pSize = 0

        do iBoundary=1,numberOfBoundaries

            is = this%boundaries%startFace(iBoundary)
            ie = is+this%boundaries%nFace(iBoundary)-1

            isp = 0
            iep = 0

            nEntries(iBoundary) = 0 !this%boundaries%nFace(iBoundary)

            do iBFace=is,ie

                isp = iep+1
                iep = isp+numberOfFaceNodes(iBFace)-1
                pSize(iBoundary) = pSize(iBoundary) + numberOfFaceNodes(iBFace)
                allPoints(isp:iep,iBoundary) = this%fvertex(iBFace)%col(1:numberOfFaceNodes(iBFace))
                nEntries(iBoundary) = nEntries(iBoundary) + numberOfFaceNodes(iBFace)

            enddo

            call unista(allPoints(:,iBoundary), bPointsSize(iBoundary),  mapper(:,iBoundary))
            call refsor(allPoints(1:bPointsSize(iBoundary),iBoundary))

            if(nEntries(iBoundary) /= maxval(this%boundaries%nFace)*maxval(numberOfFaceNodes)) bPointsSize(iBoundary) = bPointsSize(iBoundary) - 1

            ! Get the needed points from the list
            do i=1,bPointsSize(iBoundary)
                bVertex(i,:,iBoundary) = this%vertex(allPoints(i,iBoundary),:)
            enddo

        enddo

        ! Create bin directory if it does not exists
        dirName = 'grid/bin'
        inquire(file=trim(dirName)//'/.', exist=dirExists)

        if(.not. dirExists) call execute_command_line ('mkdir -p ' // adjustl(trim(dirName)))

        ! Save boundary mesh
        open(1,file='grid/bin/boundaries.bin',form='unformatted')
            write(1) numberOfBoundaries, numberOfPoints, numberOfFaces, &
                    maxval(numberOfFaceNodes), maxval(this%boundaries%nFace)*maxval(numberOfFaceNodes)
            write(1) this%boundaries%startFace, this%boundaries%nFace, pSize, nEntries
            write(1) this%boundaries%userName
            write(1) allPoints
            write(1) mapper
            write(1) allPoints
            write(1) bPointsSize
            write(1) bVertex
            write(1) numberOfFaceNodes
        close(1)

        ! Ending message
        write(*,*)
        write(*,*) 'FLUBIO: Boundary points found!'
        write(*,*)

    end subroutine findBoundaryPoints

! *********************************************************************************************************************

    function buildNodesToElementConn(this) result(evertex)

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(iDynamicArray) :: ia

        type(flubioList), dimension(:), allocatable :: evertex
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, n, iNode, iFace, iElement
    !------------------------------------------------------------------------------------------------------------------

        integer, dimension(:), allocatable :: iFaces

        integer, dimension(:), allocatable :: listAsArray
    !------------------------------------------------------------------------------------------------------------------

        integer :: nFaces, targetFace
    !------------------------------------------------------------------------------------------------------------------

        call flubioMsg('FLUBIO: setting nodes-element connectivities...')

        allocate(evertex(numberOfElements))

        do iElement=1,numberOfElements

            nFaces = numberOfElementFaces(iElement)

            ia = iDynamicArray(1)
            do iFace=1,nFaces

                targetFace = this%iFaces(iElement)%col(iFace)

                do iNode=1,numberOfFaceNodes(targetFace)
                    call ia%append(this%fvertex(targetFace)%col(iNode))
                enddo

            enddo

            allocate(listAsArray(ia%size()))
            listAsArray(1:ia%size()) = ia%values(1:ia%size())
            call I_unista_org(listAsArray, n)

            call evertex(iElement)%initList(isize=n, intialValues=-1)
            call evertex(iElement)%setList(startIndex=1, targetColumn=1, direction=2, values=listAsArray(1:n), chunkSize=n)

            call ia%deallocate()
            deallocate(listAsArray)

        enddo

    end function buildNodesToElementConn

! *********************************************************************************************************************

    subroutine saveMesh(this)

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

		character(len=:), allocatable :: dirName
		!! folder to save the output
    !------------------------------------------------------------------------------------------------------------------

        logical :: dirExists
    !------------------------------------------------------------------------------------------------------------------

        ! Create bin directory if it does not exists
        dirName = 'grid/bin'
        inquire(file=trim(dirName)//'/.', exist=dirExists)

        if(.not. dirExists) call execute_command_line ('mkdir -p ' // adjustl(trim(dirName)))

        ! Save serial mesh
        open(1,file='grid/bin/mesh.bin', form='unformatted')
            write(1) numberOfPoints, numberOfElements, numberOfBFaces, numberOfFaces
            write(1) numberOfElementFaces
            write(1) this%vertex
        close(1)

        write(*,*)
        write(*,*) 'FLUBIO: serial mesh imported successfully! '
        write(*,*) '========================================================================================='
        write(*,*)

    end subroutine saveMesh

!**********************************************************************************************************************

    subroutine getFacesAndOffsets(this, faces, facesOffsets)

        class(fvMesh) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(iDynamicArray) :: listOfFaces
    !------------------------------------------------------------------------------------------------------------------

        integer :: iNode, iFace, iElement

        integer :: nFaces, nNodes, nOffsets, targetFace, targetNode

        integer :: facesOffsets(numberOfElements)

        integer, dimension(:), allocatable :: faces, faceNodes, mapper
    !------------------------------------------------------------------------------------------------------------------

        real, dimension(:,:), allocatable :: points

        real :: facen(3)
    !------------------------------------------------------------------------------------------------------------------

        call flubioMsg("FLUBIO: build face connectivities and orient face points...")

        listOfFaces = iDynamicArray(1)

        ! Loop over elements
        do iElement=1,numberOfElements

            if(iElement==1) then
                facesOffsets(iElement) = 1
            else
                facesOffsets(iElement) = facesOffsets(iElement-1) + 1
            endif

            call listOfFaces%append(numberOfElementFaces(iElement))

            ! Loop over element faces
            do iFace=1,numberOfElementFaces(iElement)

                targetFace = this%iFaces(iElement)%col(iFace)

                ! Get face nodes
                nNodes = this%fvertex(targetFace)%csize

                allocate(faceNodes(nNodes))
                faceNodes = this%fvertex(targetFace)%getIntColumn(nNodes)

                allocate(points(nNodes,3))
                do iNode=1,nNodes
                    points(iNode,1:3) = this%vertex(faceNodes(iNode),1:3)
                enddo

                call listOfFaces%append(nNodes)
                facesOffsets(iElement) = facesOffsets(iElement) + 1

                ! Sort face nodes and append them
                facen = this%Sf(targetFace,1:3)/norm2(this%Sf(targetFace,1:3))
                mapper = orientPointsInPlane(points, facen, this%fcentroid(targetFace, 1:3), 'ascending', nNodes)

                ! Add vertices in the correct order
                do iNode=1,nNodes
                    call listOfFaces%append(faceNodes(mapper(iNode))-1)
                    facesOffsets(iElement) = facesOffsets(iElement) + 1
                enddo

                deallocate(faceNodes)
                deallocate(points)

            enddo

        enddo

        ! Transform into array
        allocate(faces(listOfFaces%size()))
        faces = listOfFaces%values(1:listOfFaces%size())

        ! Clean up
        call listOfFaces%deallocate()

    end subroutine getFacesAndOffsets

!**********************************************************************************************************************

    function orientPointsInPlane(points, normal, center, mode, np) result(sortedPointsMap)

        use math

        character(len=*) :: mode
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, j, np

        integer, dimension(:), allocatable :: sortedPointsMap
    !------------------------------------------------------------------------------------------------------------------

        real :: greatest

        real :: ex(3), ey(3), ez(3)

        real :: exn(3,3), mags(3)

        real :: p(3), q(3), normal(3), center(3), r(3), v(3), t, u

        real :: points(np,3)

        real :: theta(np)
    !------------------------------------------------------------------------------------------------------------------

        ! Prepare
        ex = (/1.0, 0.0, 0.0/)
        ey = (/0.0, 1.0, 0.0/)
        ez = (/0.0, 0.0, 1.0/)

        allocate(sortedPointsMap(np))
        sortedPointsMap = -1

        ! Get the max among the dot products between e and n
        call cross_prod(ex, normal, exn(1,:))
        call cross_prod(ey, normal, exn(2,:))
        call cross_prod(ez, normal, exn(3,:))

        call mag(exn(1,:), mags(1))
        call mag(exn(2,:), mags(2))
        call mag(exn(3,:), mags(3))

        greatest = mags(1)
        j = 1
        do i = 2,3
            if (mags(i) > greatest) then
                j = i
            endif
        enddo

        p = exn(j,:)
        call cross_prod(normal,p,q)

        do i=1,np

            r = points(i,1:3) - center

            call cross_prod(r,p,v)
            t = dot_product(normal,v)

            call cross_prod(r,q,v)
            u = dot_product(normal,v)

            theta(i) = atan2(u,t)

        enddo

        ! Sort the points in ascendig order (counter-clockwise angles)
        sortedPointsMap = sortArray(theta, np, mode)

    end function orientPointsInPlane

! *********************************************************************************************************************

end module flubioSerialMesh