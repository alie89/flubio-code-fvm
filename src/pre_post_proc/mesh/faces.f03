!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module serialFaces

    use globalMeshVar
    use serialNodes
    use userDefininedTypes

    implicit none

    type, public, extends(grid_nodes) ::  grid_faces

        integer :: numberOfFaces

        integer, dimension(:), allocatable :: numberOfFaceNodes

        type(integerCellStruct), dimension(:), allocatable :: fvertex

        integer, dimension(:), allocatable :: owner

        integer, dimension(:), allocatable :: neighbour

        ! Geometric varaibles

        real, dimension(:,:), allocatable :: fcentroid

        real, dimension(:,:), allocatable :: Sf

        real, dimension(:,:), allocatable :: CN

        real, dimension(:), allocatable :: gDiff

        real, dimension(:), allocatable :: gf

        real, dimension(:,:), allocatable :: Tf

        real, dimension(:), allocatable :: wallDist

        ! pressure Ef and Tf

        real, dimension(:,:), allocatable :: Tfp

    contains
        procedure :: readMeshFaces
        procedure :: readMeshFacesOFAscii
        procedure :: readMeshFacesOFBinary
        procedure :: readMeshFacesFlubioAscii
        procedure :: readMeshFacesFlubioBinary
    end type

contains

    subroutine readMeshFaces(this)

    !==================================================================================================================
    ! Description:
    !!  readMeshFaces reads the faces composing the mesh from the mesh files.
    !==================================================================================================================

        class(grid_faces) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: fileFormat
        !! file format
    !------------------------------------------------------------------------------------------------------------------

        call checkFormat('grid/serial/grid/faces', fileFormat)

        if (fileFormat=='openfoam_ascii') then
            call this%readMeshFacesOFAscii()
        elseif(fileFormat=='openfoam_binary') then
            call this%readMeshFacesOFBinary()
        elseif(fileFormat=='flubio_ascii') then 
            call this%readMeshFacesFlubioAscii()
        elseif(fileFormat=='flubio_binary') then 
            call this%readMeshFacesFlubioBinary()
        else
            write(*,*) 'ERROR: cannot recognize file format for faces.'
            stop
        endif    

    end subroutine readMeshFaces

!**********************************************************************************************************************

    subroutine readMeshFacesOFAscii(this)

    !==================================================================================================================
    ! Description:
    !!  readMeshFaces reads the faces composing the mesh from the mesh files in OF ascii format.
    !==================================================================================================================
    
            class(grid_faces) :: this
    !------------------------------------------------------------------------------------------------------------------
    
            character(len=80) :: line
            !! parsed line
    
            character(len=:), allocatable :: croppedLine
            !! cropped line
    
            character(len=:), allocatable :: oneLine
            !! cropped line

            character(len=16), allocatable :: carray(:)
            !! splitted line
    !------------------------------------------------------------------------------------------------------------------
    
            integer :: iFace
            !! target mesh face
    
            integer :: i
            !! loop index

            integer :: n
            !! loop index
    
            integer :: nf
            !! number of faces
    
            integer :: nn
            !! number of neighbours
    
            integer:: no
            !! number of owners
    
            integer :: dummy
            !! dummy integer
    
            integer :: max_owner
            !! max owner index
    
            integer :: max_neighbour
            !! max neighbour index
    
            integer :: max_faces
            !! max number of faces

            integer :: ierr
            !! error flag
        !------------------------------------------------------------------------------------------------------------------
    
            real :: dummy2
            !! dummy real
        !------------------------------------------------------------------------------------------------------------------
    
            logical :: isblankline
            !! falg to look for a blank line

            logical :: isNum
            !! flag to look for a number
        !------------------------------------------------------------------------------------------------------------------ 

            !------------!
            ! Read Faces !
            !------------!

            open(1,file='grid/serial/grid/faces')
            
                call skipHeader(aunit=1, fileFormat='ascii')
                call findStreamSize(aunit=1, fileFormat='ascii', streamSize=nf, oneLine =  oneLine)
    
                ! Allocate arrays
                allocate(numberOfFaceNodes(nf))
                allocate(this%fvertex(nf))
    
                do n =1,nf
    
                    call readLine(1, line, ierr)
                    croppedLine = crop(line)
        
                    call substitute(croppedLine,'(',' ')
                    call substitute(croppedLine,')',' ')
                    call split_in_array(croppedLine, carray, ' ')
                    
                    call string_to_value(carray(1), numberOfFaceNodes(n), ierr)
                    call this%fvertex(n)%initIntColumn(numberOfFaceNodes(n), -1)

                    do i =1,numberOfFaceNodes(n)
                        call string_to_value(carray(i+1), this%fvertex(n)%col(i), ierr)
                        this%fvertex(n)%col(i) = this%fvertex(n)%col(i) + 1
                    enddo    

                enddo  
    
            close(1)

            ! Set sizes, very important
            max_faces = maxval(numberOfFaceNodes)
            this%NumberOfFaces = nf
            numberOfFaces = nf
            
            !--------------------------!
            ! Read Owner and Neighbour !
            !--------------------------!

            open(1,file='grid/serial/grid/owner')
                
                call skipHeader(aunit=1, fileFormat='ascii')
                call findStreamSize(aunit=1, fileFormat='ascii', streamSize=no, oneLine =  oneLine)

                ! Allocate arrays
                allocate(this%owner(no))

                n = 0
                do 
                    call readLine(1, line, ierr)
                    croppedLine = crop(line)
                    call substitute(croppedLine,'(',' ')
                    call substitute(croppedLine,')',' ')

                    isblankLine = isblank(croppedLine)
                    isNum = isdigit(croppedLine)

                    if(.not. isblankLine .and. isNum) then

                        call split_in_array(croppedLine, carray, ' ')

                        do i=1,size(carray)
                            n = n+1
                            call string_to_value(carray(i), this%owner(n), ierr)
                        enddo
                        
                    endif    

                    if(ierr/=0) exit

                enddo    

            close(1)
            
            this%owner=this%owner+1
            
            open(1,file='grid/serial/grid/neighbour')
                
                call skipHeader(aunit=1, fileFormat='ascii')
                call findStreamSize(aunit=1, fileFormat='ascii', streamSize=nn, oneLine =  oneLine)

                ! Allocate arrays
                allocate(this%neighbour(nn))

                n = 0
                do 
                    call readLine(1, line, ierr)
                    croppedLine = crop(line)
                    call substitute(croppedLine,'(',' ')
                    call substitute(croppedLine,')',' ')

                    isblankLine = isblank(croppedLine)
                    isNum = isdigit(croppedLine)

                    if(.not. isblankLine .and. isNum) then
                        call split_in_array(croppedLine, carray, ' ')

                        do i=1,size(carray)
                            n = n+1
                            call string_to_value(carray(i), this%neighbour(n), ierr)
                        enddo
                    endif    

                    if(ierr/=0) exit

                enddo   

            close(1)

            ! Set sizes, very important
            this%neighbour = this%neighbour+1
            numberOfIntFaces =  nn
            max_owner = maxval(this%owner)
            max_neighbour = maxval(this%neighbour)
            numberOfElements = max0(max_owner, max_neighbour) 

        end subroutine readMeshFacesOFAscii        

!**********************************************************************************************************************

    subroutine readMeshFacesOFBinary(this)

    !==================================================================================================================
    ! Description:
    !!  readMeshFaces reads the faces composing the mesh from the mesh files in binary format.
    !==================================================================================================================
    
            class(grid_faces) :: this
    !------------------------------------------------------------------------------------------------------------------
    
            character(len=80) :: line
            !! parsed line
    
            character(len=:), allocatable :: croppedLine
            !! cropped line
    
            character(len=:), allocatable :: oneLine
            !! cropped line

            character(len=16), allocatable :: carray(:)
            !! splitted line

    !------------------------------------------------------------------------------------------------------------------
    
            integer :: iFace
            !! target mesh face
    
            integer :: n
            !! loop index
    
            integer :: nf
            !! number of faces

            integer :: ni
            !! index size

            integer :: np
            !! number of face vertices
    
            integer :: nn
            !! number of neighbours as written in neighbour file
    
            integer :: nni
            !! number of neighbours, excluding boundary faces sometimes tagged with -1

            integer:: no
            !! number of owners
    
            integer :: max_owner
            !! max owner index
    
            integer :: max_neighbour
            !! max neighbour index
    
            integer :: max_faces
            !! max number of faces

            integer :: i, ierr 

            integer :: faceSign

            integer, dimension(:), allocatable :: faceIndex
    !------------------------------------------------------------------------------------------------------------------
     
            !------------!
            ! Read Faces !
            !------------!

            open(1,file='grid/serial/grid/faces', form = 'UNFORMATTED', access='STREAM')
            
                call skipHeader(aunit=1, fileFormat='binary')
                call findStreamSize(aunit=1, fileFormat='binary', streamSize=ni, oneLine =  oneLine)
                nf = ni -1

                ! Allocate arrays
                allocate(numberOfFaceNodes(ni-1))
                allocate(this%fvertex(ni-1))
                allocate(faceIndex(ni))
               
                ! Read the number of vertex per face
                read(1) faceIndex(1)
                do n=2,ni
                    read(1) faceIndex(n)
                    numberOfFaceNodes(n-1) = faceIndex(n) - faceIndex(n-1)
                    call this%fvertex(n-1)%initIntColumn(numberOfFaceNodes(n-1), -1)
                enddo 

                call findStreamSize(aunit=1, fileFormat='binary', streamSize=np, oneLine =  oneLine)

                do n=1,nf
                    do i =1,numberOfFaceNodes(n)
                        read(1) this%fvertex(n)%col(i)
                        this%fvertex(n)%col(i) = this%fvertex(n)%col(i) + 1
                    enddo
                enddo  

            close(1)

            ! Set sizes, very important
            max_faces = maxval(numberOfFaceNodes)
            this%NumberOfFaces = nf
            numberOfFaces = nf

            !--------------------------!
            ! Read Owner and Neighbour !
            !--------------------------!

            open(1,file='grid/serial/grid/owner', form = 'UNFORMATTED', access='STREAM')
                
                call skipHeader(aunit=1, fileFormat='binary')
                call findStreamSize(aunit=1, fileFormat='binary', streamSize=no, oneLine =  oneLine)

                ! Allocate arrays
                allocate(this%owner(no))

                do n =1,no
                    read(1) this%owner(n)
                enddo  

            close(1)

            this%owner = this%owner+1

            open(1,file='grid/serial/grid/neighbour', form = 'UNFORMATTED', access='STREAM')
                
                call skipHeader(aunit=1, fileFormat='binary')
                call findStreamSize(aunit=1, fileFormat='binary', streamSize=nn, oneLine =  oneLine)

                ! Allocate arrays
                allocate(this%neighbour(nn))
                nni = 0
                do n =1,nn
                    read(1) this%neighbour(n)
                    if(this%neighbour(n) > -1) nni = nni + 1
                enddo 

            close(1)

            ! Set sizes, very important
            this%neighbour = this%neighbour+1
            numberOfIntFaces = nni
            max_owner = maxval(this%owner)
            max_neighbour = maxval(this%neighbour)
            numberOfElements = max0(max_owner, max_neighbour)  
            
        end subroutine readMeshFacesOFBinary

!**********************************************************************************************************************

    subroutine readMeshFacesFlubioAscii(this)

    !==================================================================================================================
    ! Description:
    !!  readMeshFaces reads the faces composing the mesh from the mesh files in flubio ascii.
    !==================================================================================================================
    
            class(grid_faces) :: this
    !------------------------------------------------------------------------------------------------------------------
    
            integer :: iFace
            !! target mesh face
    
            integer :: n
            !! loop index
    
            integer :: nf
            !! number of faces
    
            integer :: nn
            !! number of neighbours
    
            integer:: no
            !! number of owners
    
            integer :: dummy
            !! dummy integer
    
            integer :: max_owner
            !! max owner index
    
            integer :: max_neighbour
            !! max neighbour index
    
            integer :: max_faces
            !! max number of faces
        !------------------------------------------------------------------------------------------------------------------
    
            real :: faceSign
            !! dummy real
        !------------------------------------------------------------------------------------------------------------------
    
            !------------!
            ! Read Faces !
            !------------!
    
            open(1,file='grid/serial/grid/faces')
    
                read(1,*) nf

                allocate(numberOfFaceNodes(nf))
                allocate(this%fvertex(nf))
    
                do n=1,nf
                    read(1,*) numberOfFaceNodes(n)
                    call this%fvertex(n)%initIntColumn(numberOfFaceNodes(n), -1)
                enddo
    
            close(1)
    
            ! Set sizes, very important
            max_faces = maxval(numberOfFaceNodes)
            this%NumberOfFaces = nf
            numberOfFaces = nf
    
            ! Read faces from mesh file, EXP wtih the struct
            open(1,file='grid/serial/grid/faces')

                ! Skip face size
                read(1,*)
    
                do n=1,nf
                    read(1,*) dummy, this%fvertex(n)%col(1:numberOfFaceNodes(n))
                    ! Add 1 since vertices numbering start from 1 and not from 0
                    this%fvertex(n)%col(1:numberOfFaceNodes(n)) = this%fvertex(n)%col(1:numberOfFaceNodes(n)) + 1
                enddo
    
            close(1)
    
            !--------------------------!
            ! Read Owner and Neighbour !
            !--------------------------!
    
            open(1,file='grid/serial/grid/owner')

                read(1,*) no

                allocate(this%owner(no))
    
                do n=1,no
                    read(1,*) this%owner(n)
                enddo
    
            close(1)
            this%owner=this%owner+1
    
            open(1,file='grid/serial/grid/neighbour')
    
                read(1,*) nn

                allocate(this%neighbour(this%NumberOfFaces))
    
                this%neighbour=0
                do n=1,nn
                    read(1,*) this%neighbour(n)
                enddo
    
            close(1)
    
            ! Set sizes, very important
            this%neighbour = this%neighbour+1
            numberOfIntFaces =  nn
            max_owner = maxval(this%owner)
            max_neighbour = maxval(this%neighbour)
            numberOfElements = max0(max_owner, max_neighbour)
    
    end subroutine readMeshFacesFlubioAscii

!**********************************************************************************************************************

    subroutine readMeshFacesFlubioBinary(this)

    !==================================================================================================================
    ! Description:
    !!  readMeshFaces reads the faces composing the mesh from the mesh files in flubio binary format.
    !==================================================================================================================
    
        class(grid_faces) :: this
    !------------------------------------------------------------------------------------------------------------------
    
        character(10) :: procID
        !! processor ID
    !------------------------------------------------------------------------------------------------------------------
    
        integer :: iFace
        !! target mesh face
    
        integer :: n
        !! loop index
    
        integer :: nf
        !! number of faces
    
        integer :: nn
        !! number of neighbours
    
        integer:: no
        !! number of owners
    
        integer :: dummy
        !! dummy integer
    
        integer :: max_owner
        !! max owner index
    
        integer :: max_neighbour
        !! max neighbour index
    
        integer :: max_faces
        !! max number of faces
    !------------------------------------------------------------------------------------------------------------------
    
        real :: faceSign
        !! dummy real
    !------------------------------------------------------------------------------------------------------------------
    
        !------------!
        ! Read Faces !
        !------------!
    
        open(1,file='grid/serial/grid/faces', form="unformatted")
    
            read(1) nf

            allocate(numberOfFaceNodes(nf))
            allocate(this%fvertex(nf))
    
            do n=1,nf
                read(1) numberOfFaceNodes(n)
                call this%fvertex(n)%initIntColumn(numberOfFaceNodes(n), -1)
            enddo
    
        close(1)
    
        ! Set sizes, very important
        max_faces = maxval(numberOfFaceNodes)
        this%NumberOfFaces = nf
        numberOfFaces = nf

        !--------------------------!
        ! Read Owner and Neighbour !
        !--------------------------!
    
        open(1,file='grid/serial/grid/owner')
    
            read(1) no

            allocate(this%owner(no))
    
            do n=1,no
                read(1) this%owner(n)
            enddo
    
        close(1)
        this%owner=this%owner+1
    
        open(1,file='grid/serial/grid/neighbour')
    
            read(1) nn

            allocate(this%neighbour(this%NumberOfFaces))
    
            this%neighbour=0
            do n=1,nn
                read(1) this%neighbour(n)
            enddo
    
        close(1)
    
        ! Set sizes, very important
        this%neighbour = this%neighbour+1
        numberOfIntFaces =  nn
        max_owner = maxval(this%owner)
        max_neighbour = maxval(this%neighbour)
        numberOfElements = max0(max_owner, max_neighbour)

    end subroutine readMeshFacesFlubioBinary

end module serialFaces
