!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

	program createMetisMsh

		use serialmeshvar

		implicit none

		character(len=10), dimension(:), allocatable :: args

		character(len=10) :: fmt
	!------------------------------------------------------------------------------------------------------------------

		integer :: etype, nargs
	!------------------------------------------------------------------------------------------------------------------

		!------------------------!
		! command line arugments !
		!------------------------!

		nargs = command_argument_count()
		allocate(args(nargs))
		call parseCommandLine(args, nargs, etype, fmt)

		!------------------!
		! read serial mesh !
		!------------------!

		call mesh%readMeshFromFiles()

		!------------------!
		! Write data files !
		!------------------!

		call writeCellConn(etype, trim(fmt))

	end program createMetisMsh

! *********************************************************************************************************************

	subroutine writeCellConn(etype, fmt)

		use serialmeshvar
	
		implicit none

		type(flubioList), dimension(:), allocatable :: evertex
		!! list of vertices composing a cell
	!------------------------------------------------------------------------------------------------------------------

		character(len=10) :: val

		character(len=*) :: fmt

		character(len=:), allocatable :: str_list
	!------------------------------------------------------------------------------------------------------------------

		integer, dimension(:), allocatable :: econn
	!------------------------------------------------------------------------------------------------------------------

		integer :: i, n, iElement, nFaces, pyramid, isize, p, etype
	!------------------------------------------------------------------------------------------------------------------

		write(*,*)
		write(*,*) 'FLUBIO: writing mesh connetivities in grid/econn.msh'
		write(*,*)

		! Get the element connectivities
        evertex = mesh%buildNodesToElementConn()

		if(fmt=='ascii') then
			open(1, file="grid/econn.msh")

				write(1,'(i0,a1,i0)') numberOfElements, ' ', etype

				do iElement=1,numberOfElements

					nFaces = numberOfElementFaces(iElement)

					n = evertex(iElement)%getListSize()
					econn = evertex(iElement)%getIntList(is=1, ie=n, isReal=0)

					! convert to string to avoid inital spaces
					do i=1,size(econn)
						write(val, '(i0)') econn(i)
						str_list = str_list//trim(val)//' '
					end do

					! write to file
					write(1,'(a)') str_list
					str_list = ''

				enddo

			close(1)
		else

			open(1, file="grid/econn.msh", form='unformatted')

				write(1) numberOfElements, etype

				do iElement=1,numberOfElements

					nFaces = numberOfElementFaces(iElement)

					n = evertex(iElement)%getListSize()
					econn = evertex(iElement)%getIntList(is=1, ie=n, isReal=0)

					! write to file
					write(1) econn
				
				enddo

			close(1)

		end if	 

		write(*,*)
		write(*,*) 'DONE!'
		write(*,*)

	end subroutine writeCellConn

! *********************************************************************************************************************

	subroutine parseCommandLine(args, nargs, etype, fmt)

		implicit none

		character(*) :: args(nargs)

		character(len=10) :: fmt
	!------------------------------------------------------------------------------------------------------------------

		integer :: p, nargs, etype, stat
	!------------------------------------------------------------------------------------------------------------------

		! Get the arguments
		do p=1,nargs
			call get_command_argument(number=p, value=args(p), status=stat)
		enddo

		fmt = 'ascii'

		! Process the options
		do p=1,nargs

			if(args(p)=='-etype') then
				read(args(p+1),*) etype
			elseif(args(p)=='-format') then
				fmt = trim(args(p+1))
			end if

			if(args(p)=='-help') then
				write(*,*) 'example: flubio_createMetisMesh -etype N -format ascii'
				stop
			endif

		enddo

		if(nargs==0) etype = 1

end subroutine parseCommandLine