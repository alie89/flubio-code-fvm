!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

	program boundaryMeshView

		use serialmeshvar
		use m_strings
		use serialVTKFormat, only: processBoundaryMeshVTK

		implicit none

		logical :: boundaryFlag
	!------------------------------------------------------------------------------------------------------------------

		character(len=500), dimension(:), allocatable :: args

		character(len=500) :: boundary

		character(len=50), allocatable :: boundaryList(:)

		character(len=6) :: fmt
	!------------------------------------------------------------------------------------------------------------------

		integer :: nranks, nargs, iBoundary
	!------------------------------------------------------------------------------------------------------------------

		!------------------!
		! read serial mesh !
		!------------------!

		call mesh%readMeshFromFiles()

		!------------------------!
		! command line arugments !
		!------------------------!

		nargs = command_argument_count()
        fmt = 'binary'

		if(nargs>0) then

			allocate(args(nargs))
			call parseCommandLine(args, nargs, boundary, boundaryFlag, fmt)
			
			if(boundaryFlag) then
				call split_in_array(boundary, boundaryList, ',')
			else 
				boundaryList = mesh%boundaries%userName
			endif
		else 
				boundaryList = mesh%boundaries%userName	
		endif

		!------------------!
		! Write data files !
		!------------------!

		do iBoundary=1,size(boundaryList)
			if(.not. matchw(trim(boundaryList(iBoundary)), 'procBoundary*')) then
				call processBoundaryMeshVTK(trim(boundaryList(iBoundary)), trim(fmt))
			endif
		end do

	end program boundaryMeshView

! *********************************************************************************************************************
	
	subroutine parseCommandLine(args, nargs, boundary, boundaryFlag, fmt)

		implicit none

		logical :: boundaryFlag

		integer :: p, nargs, dataForm, stat

		character(len=*) :: args(nargs), boundary

		character(len=6) :: fmt
	!------------------------------------------------------------------------------------------------------------------

		boundaryFlag = .false.

		! Get the arguments
		do p=1,nargs
			call get_command_argument(number=p, value=args(p), status=stat)
		enddo

		! Default value for fmt
		fmt ='binary'

		! Process the options
		do p=1,nargs
			
			if(args(p)=='-vtk') then

				dataForm = 1

			elseif(args(p)=='-boundary') then

				boundaryFlag = .true.
				boundary = args(p+1)

			elseif(args(p)=='-format') then

				fmt = trim(args(p+1))

			elseif(args(p)=='-help') then

				write(*,*) 'Command line options:'
				write(*,*) '-boundary: target boundary to process. List of boundaries are comma separeted with no spaces.'
				write(*,*) '-format: format of the vtu file (ascii or binary)'
				write(*,*)
				write(*,*) 'example: flubio_meshview -boundary b1,b2,b3'

				stop

			endif

		enddo

		if (.not. boundaryFlag) then
			boundary = ""
		endif

	end subroutine parseCommandLine

! *********************************************************************************************************************
