!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____ 	        !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|	        !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |     	        !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |     	        !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____ 	        !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|	        !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module spallartAllmaras

!==================================================================================================================
! Description:
!! This modules contains the Spalart-Allmaras turbulence model implementation.
!==================================================================================================================

      use baseRansModel
      use transportEq
      use ttbTensors

      implicit none

      type, public, extends(baseModelClass) :: SAModel

        ! Coeffs
        real :: Cb1
        real :: Cb2
        real :: Cv1
        real :: Cw2
        real :: Cw3
        real :: Ct3
        real :: Ct4
        real :: sigma
        real :: c2
        real :: c3
        real :: Cw1

        type(flubioField) :: nuTilda
        !! Trasported field

      contains
          procedure :: createModel => createSAModel
          procedure :: modelCoeffs => SAModelCoeffs
          procedure :: overrideDefaultCoeffs
          procedure :: printCoeffs => printCoeffsSA

          procedure :: tdrDiffusivity => nuTildaDiffusivity

          procedure :: tdrSourceTerm => sourceTermSA

          procedure :: updateEddyViscosity => eddyViscositySA
          procedure :: nutWallFunction => nutWallFunctionSA

          procedure :: turbulenceModel => spalartAllmarasModel

          procedure :: printResidual => nuTildaEqResidual
          procedure :: writeToFile => nuTildaWriteToFile
          procedure :: verbose => SAverbose

      end type SAModel

contains
         
    subroutine createSAModel(this)

    !==================================================================================================================
    ! Description:
    !! createSAModel is the model contructor.
    !==================================================================================================================

        class(SAModel) :: this

        integer bcFlag
        logical hasKey
    !------------------------------------------------------------------------------------------------------------------

        call this%modelCoeffs()
        call this%createCommonQuantities()

        call this%nuTilda%setUpField('NuTilda', classType='turbulence', nComp=1)

        call this%tdrField%setUpWorkField('NuTilda', classType='turbulence', nComp=1)

        if(flubioControls%restFrom/=-1) call this%nuTilda%readFromFile(flubioControls%restFrom)

        call this%nuTilda%writeToFile()

        call computeGradient(this%nuTilda)

        call this%tdrEqn%createEq('nuTildaEqn',1)
        call this%tdrEqn%setEqOptions(trim(this%tdrEqn%eqName))

        call this%nut%appendFieldToList()
        call this%updateEddyViscosity()
        call this%nut%writeToFile()

        ! Add field to the registry
        !fieldRegistry(6) => this%nuTilda

    end subroutine createSAModel

! *********************************************************************************************************************

    subroutine SAModelCoeffs(this)

    !==================================================================================================================
    ! Description:
    !! SAModelCoeffs sets the model coefficient according to NASA.
    !==================================================================================================================

        class(SAModel) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: val
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        !----------------------!
        ! Default Coefficients !
        !----------------------!

        this%Cmu = 0.09
        this%kappa = 0.41
        this%E = 9.80
        this%B = 5.25

        this%Cb1 = 0.1355
        this%Cb2 = 0.622
        this%Cv1 = 7.1
        this%Cw2 = 0.3
        this%Cw3 = 2.0

        ! Modern SA "no-ft2"
        this%Ct3 = 0.0
        !this%Ct3 = 1.2d0

        this%Ct4 = 0.5
        this%sigma = 1.5

        this%c2 = 0.7
        this%c3 = 0.9

        !----------------------------!
        ! Override from dictionaries !
        !----------------------------!

        call this%overrideDefaultCoeffs()

        this%Cw1 = this%Cb1/this%kappa**2 + (1.0+this%Cb2)*this%sigma

        call this%laminarYPlus()

        !-------------------!
        ! Print if required !
        !-------------------!

        call flubioTurbModel%json%get('PrintCoefficients', val, found)
        if(found .and. (lowercase(val)=='yes' .or. lowercase(val)=='on')) call this%printCoeffs()

    end subroutine SAModelCoeffs

! *********************************************************************************************************************

    subroutine overrideDefaultCoeffs(this)

    !==================================================================================================================
    ! Description:
    !! odoverrideDefaultCoeffs set the coefficients as you might have defined in turbulenceModel dictionary.
    !==================================================================================================================

        class(SAModel) :: this
    !------------------------------------------------------------------------------------------------------------------

        real :: coeff
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        ! C_mu
        call flubioTurbModel%json%get('Cmu', coeff, found)
        if(found) this%Cmu = coeff

        ! kappa
        call flubioTurbModel%json%get('kappa', coeff, found)
        if(found) this%kappa = coeff

        ! E
        call flubioTurbModel%json%get('E', coeff, found)
        if(found) this%E = coeff

        ! B
        call flubioTurbModel%json%get('B', coeff, found)
        if(found) this%B = coeff

        ! Cb1
        call flubioTurbModel%json%get('Cb1', coeff, found)
        if(found) this%Cb1 = coeff

        ! Cb2
        call flubioTurbModel%json%get('Cb2', coeff, found)
        if(found) this%Cb2 = coeff

        ! Cv1
        call flubioTurbModel%json%get('Cv1', coeff, found)
        if(found) this%Cv1 = coeff

        ! Cw2
        call flubioTurbModel%json%get('Cw2', coeff, found)
        if(found) this%Cw2 = coeff

        ! Cw3
        call flubioTurbModel%json%get('Cw3', coeff, found)
        if(found) this%Cw3 = coeff

        ! Ct3
        call flubioTurbModel%json%get('Ct3', coeff, found)
        if(found) this%Ct3 = coeff

        ! Ct4
        call flubioTurbModel%json%get('Ct4', coeff, found)
        if(found) this%Ct4 = coeff

        ! sigma
        call flubioTurbModel%json%get('sigma', coeff, found)
        if(found) this%sigma = coeff

        ! c2
        call flubioTurbModel%json%get('c2', coeff, found)
        if(found) this%c2 = coeff

        ! c3
        call flubioTurbModel%json%get('c3', coeff, found)
        if(found) this%c3 = coeff

    end subroutine overrideDefaultCoeffs

! *********************************************************************************************************************

    subroutine spalartAllmarasModel(this)

     !=================================================================================================================
     ! Description:
     !! spalartAllmarasModel is the driver for the SA model.
     !=================================================================================================================

        class(SAModel) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: ierr, iElement
    !------------------------------------------------------------------------------------------------------------------

        !---------------------------!
        ! Compute friction velocity !
        !---------------------------!

        call this%computeFrictionVelocity()

        !------------------------!
        ! Assemble and solve tke !
        !------------------------!

        call this%tdrEqn%assembleEq(this%nuTilda, this%tdrDiffusivity(), boundaryNonOrthCorr=.true.)

        call this%tdrSourceTerm()

        ! Relax transport equation
        if(steadySim==1) call this%tdrEqn%relaxEq(this%nuTilda)

        call this%tdrEqn%solve(this%nuTilda, 1, 0, 0)

        ! Clean up memory
        call this%tdrEqn%clearMatAndRhs()
        call this%tdrEqn%destroyEqCoeffs()

        call this%nuTilda%updateBoundaryTurbulenceField()

        if(flubioTurbModel%bounded==1) call bound(this%nuTilda, 0.0, flubioTurbModel%boundType)

        call computeGradient(this%nuTilda)

        !----------------------------!
        ! Update turbulent viscosity !
        !----------------------------!

        call this%updateEddyViscosity()

        ! Store the field into a dummy variable to be use in run time sampling
        this%tdrField%phi(:,1) = this%nuTilda%phi(:,1)

    end subroutine spalartAllmarasModel

! *********************************************************************************************************************

    subroutine eddyViscositySA(this)

    !==================================================================================================================
    ! Description:
    !! eddyViscositySA computes the eddy viscosity according to the SA turbulece model.
    !==================================================================================================================

        class(SAmodel) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: X, fv1, nuTilda
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements

            nuTilda = this%nuTilda%phi(iElement,1)
            X = nuTilda/viscos
            fv1 = X**3/(X**3+this%cv1**3)

            this%nut%phi(iElement,1) = rho%phi(iElement,1)*this%nuTilda%phi(iElement,1)*fv1

        enddo

        ! Apply wall function if any
        call this%nutWallFunction()

        ! Update processor boundaries
        call this%nut%updateGhosts()

        ! Compute effective viscosity
        nu%phi(1:numberOfElements,1) = viscos + this%nut%phi(1:numberOfElements,1)

        ! Update processor boundaries
        call nu%updateGhosts()

    end subroutine eddyViscositySA
                 
! *********************************************************************************************************************
                 
    function nuTildaDiffusivity(this) result(muEff)

    !==================================================================================================================
    ! Description:
    !! nuTildaDiffusivity computes the diffusion coefficient for the SA equation.
    !==================================================================================================================

        class(SAModel) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(flubioField) :: muEff
    !------------------------------------------------------------------------------------------------------------------

        ! Create muEff
        call muEff%createWorkField(fieldName='muEff', classType='scalar', nComp=1)

        ! Compute effective diffusivities 
        muEff%phi(:,1) = rho%phi(:,1)*(this%nuTilda%phi(:,1) + viscos/dens)*this%sigma

        ! Update ghosts
        call muEff%updateGhosts()

    end function nuTildaDiffusivity

! *********************************************************************************************************************

    subroutine nutWallFunctionSA(this)

    !==================================================================================================================
    ! Description:
    !! nutWallFunctionSA computes the value of the eddy viscosity at solid walls for the SA turbulence models.
    !==================================================================================================================

        class(SAModel) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iBFace, iElement, is, ie, iOwner, iWall, patchFace
    !------------------------------------------------------------------------------------------------------------------

        real :: SMALL
    !------------------------------------------------------------------------------------------------------------------

        SMALL = 1.0e-12

        !--------------------------------------!
        ! Correct viscosity at wall boundaries !
        !--------------------------------------!

        patchFace=numberOfElements
        i=0

        do iBoundary=1,numberOfBoundaries

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
            iWall = 0

            if(trim(mesh%boundaries%bcType(iBoundary))=='wall') i = i+1

            do iBFace=is,ie

                patchFace = patchFace+1
                iOwner = mesh%owner(iBFace)

                ! Get the wall boundaries
                if(trim(mesh%boundaries%bcType(iBoundary))=='wall') then

                  iWall=  iWall+1
                  nu%phi(patchFace,1) = viscos
                  this%nut%phi(patchFace,1) = 0.0

                elseif(trim(mesh%boundaries%bcType(iBoundary))/='wall') then

                  ! Zero gradient
                  this%nut%phi(patchFace,1) = this%nut%phi(iOwner,1)
                  nu%phi(patchFace,1) = viscos + this%nut%phi(iOwner,1)

                endif

            enddo

        enddo

    end subroutine nutWallFunctionSA

! *********************************************************************************************************************

    subroutine sourceTermSA(this)

    !==================================================================================================================
    ! Description:
    !! sourceTermSA computes the source term for the SA equation.
    !==================================================================================================================
                   
        class(SAmodel) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(ttbTensor2) :: gradU         

        type(flubioField) :: dummy, dummy2    
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------
                   
        real :: W(numberOfElements), S(numberOfElements), Shat, Sbar

        !real :: R(numberOfElements,3), Rsum(numberOfElements)

        real :: aC, bC, nuTilda, gradnuTilda(3), Omega, dot, d, density, SMALL
                 
        real :: fv1, fv2, fw, ft2, X, kappa, g, rsmall, coef, a, b, vol
                 
        real :: c2, c3, cb1, cb2, cv1, cw1, cw2, cw3, ct3, ct4, sigma
    !------------------------------------------------------------------------------------------------------------------

        !---------!
        ! Prepare !
        !---------!

        SMALL = 1e-10

        ! Coefficients, for clarity
        kappa = this%kappa
        c2 = this%c2
        c3 = this%c3
        cb1 = this%Cb1
        cb2 = this%Cb2
        cv1 = this%Cv1
        cw1 = this%Cw1
        cw2 = this%Cw2
        cw3 = this%Cw3
        ct4 = this%Ct4
        sigma = this%sigma

        !call rotationTensor(R, Rsum)
        call gradU%create(dim=numberOfElements, phi=velocity%phiGrad(1:numberOfElements, 1:3, 1:3))
        W = sqrt(2.0)*norm(skw(gradU))
        
        if (this%productionType==3) then
            S = sqrt(2.0)*norm(symm(gradU))
        endif 
      
        !----------------------!
        ! Compute coefficients !
        !----------------------!

        do iElement=1,numberOfElements

            nuTilda = this%nuTilda%phi(iElement,1)
            gradnuTilda = this%nuTilda%phiGrad(iElement,:,1)

            vol = mesh%volume(iElement)
            d = this%wd%dperp(iElement)
            density = rho%phi(iElement,1)
            
            X = nuTilda/viscos
            fv1 = X**3/(X**3+cv1**3)
            fv2 = 1.0-X/(1+X*fv1)

            !Omega = sqrt(2.0*Rsum(iElement))
            Omega = W(iElement)

            ! Kati-Launder correction
            if (this%productionType==3) then
                Omega = sqrt(W(iElement)*S(iElement))
            endif 
      
            ! Limit Shat to be always positive
            Sbar = nuTilda*fv2/((kappa*d)**2)

            !if(Sbar>=-c2*Omega) then
            !    Shat = Omega + Sbar
            !else
            !    a = Omega*(Omega*c2**2 + c3*Sbar)
            !    b = (c3-2*c2)*Omega - Sbar+ SMALL
            !    Shat = Omega + a/b
            !endif

            ! This is an alternative way to limit Shat (see https://turbmodels.larc.nasa.gov/spalart.html)
            Shat = max(Omega + Sbar, 0.3*Omega)

            ! ft2 is considered to be zero is modern SA model (ct3=0.)
            rsmall = min(nuTilda/(Shat*(kappa*d)**2 + SMALL), 10.0)
            g = rsmall + cw2*(rsmall**6 - rsmall)
            fw = g*((1+cw3**6)/(g**6+cw3**6))**(1./6.)
            ft2 = ct3*exp(-ct4*X**2)
            coef = (cw1*fw - cb1*ft2/kappa**2)/d**2

            !-------------!
            ! Source term !
            !-------------!

            dot = dot_product(gradnuTilda, gradnuTilda)

            aC = coef*nuTilda*density*vol
            bC = cb2*sigma*density*dot*vol + cb1*(1-ft2)*Shat*nuTilda*density*vol

            this%tdrEqn%aC(iElement,1) = this%tdrEqn%aC(iElement,1) + aC
            this%tdrEqn%bC(iElement,1) = this%tdrEqn%bC(iElement,1) + bC

        enddo

    end subroutine sourceTermSA

! *********************************************************************************************************************

    subroutine nuTildaWriteToFile(this)

    !==================================================================================================================
    ! Description:
    !! nuTildaWriteToFile writes the SA fields to output.
    !==================================================================================================================

        class(SAmodel) :: this
    !------------------------------------------------------------------------------------------------------------------

        call this%nuTilda%writeToFile()
        call this%nut%writeToFile()
        if(this%outputYPlus==1) call this%writeWallYPlus()

    end subroutine nuTildaWriteToFile

! *********************************************************************************************************************

    subroutine nuTildaEqresidual(this)

    !==================================================================================================================
    ! Description:
    !! nuTildaEqresidual writes the residual to file and to standard output.
    !==================================================================================================================

        class(SAmodel) :: this
    !------------------------------------------------------------------------------------------------------------------

        call this%tdrEqn%printresidual()
        call this%tdrEqn%writeresidual()

    end subroutine nuTildaEqresidual

! *********************************************************************************************************************

    subroutine SAverbose(this)

    !==================================================================================================================
    ! Description:
    !! SAverbose writes info to the standard output.
    !==================================================================================================================

        class(SAmodel) :: this
    !------------------------------------------------------------------------------------------------------------------

        call this%nuTilda%fieldVerbose()

    end subroutine SAverbose

! *********************************************************************************************************************

    subroutine printCoeffsSA(this)

    !==================================================================================================================
    ! Description:
    !! printCoeffsSA prints coefficients to screen.
    !==================================================================================================================

        class(SAmodel) :: this
    !------------------------------------------------------------------------------------------------------------------

        if(id==0) then
            write(*,'(A)') '!---------------------------------!'
            write(*,'(A)') '! Spalart-Allmaras coefficients:  !'
            write(*,'(A)') '!---------------------------------!'
            write(*,*)
            write(*,'(A, f6.4)') ' Cmu = ', this%Cmu
            write(*,'(A, f6.4)') ' kappa = ', this%kappa
            write(*,'(A, f6.4)') ' Cv1 = ', this%Cv1
            write(*,'(A, f6.4)') ' Cb1 = ', this%Cb1
            write(*,'(A, f6.4)') ' Cb2 = ', this%Cb2
            write(*,'(A, f6.4)') ' Cw2 = ', this%Cw2
            write(*,'(A, f6.4)') ' Cw3 = ', this%Cw3
            write(*,'(A, f6.4)') ' Ct3 = ', this%Ct3
            write(*,'(A, f6.4)') ' Ct4 = ', this%Ct4
            write(*,'(A, f6.4)') ' sigma = ', this%sigma
            write(*,'(A, f6.4)') ' c2 = ', this%c2
            write(*,'(A, f6.4)') ' c3 = ', this%c3
            write(*,'(A)') '!---------------------------------!'
            write(*,*)
        end if

    end subroutine printCoeffsSA

end module spallartAllmaras