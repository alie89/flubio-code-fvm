!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____ 	        !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|	        !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |     	        !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |     	        !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____ 	        !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|	        !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module kappaOmegaWilcox

!======================================================================================================================
! Description:
!! this module contains the implementation of the k-omega model by Wilcox (1988).
!======================================================================================================================

    use baseRansModel

    implicit none

    type, public, extends(baseModelClass) :: kw1988

        ! Coefficients
        real :: C_a1
        real :: C_a2
        real :: C_b1
        real :: C_b2
        real :: sigma_k1
        real :: sigma_o1
        real :: sigma_k2
        real :: sigma_o2
        real :: bstar
        real :: Clim
        real :: crc
        real :: divCoeff

        ! Fields
        type(flubioField) :: tke
        type(flubioField) :: tdr

    contains

        procedure :: turbulenceModel => kappaOmega
        procedure :: kappaOmegaTkeFirst

        procedure :: modelCoeffs => modelCoeffsWilcox
        procedure :: printCoeffs => printCoeffsKappaOmega
        procedure :: overrideDefaultCoeffs

        procedure :: createModel => createKappaOmega

        procedure :: updateEddyViscosity => updateEddyViscosityWilcox
        procedure :: nutWallFunction => nutWallFunctionWilcox

        ! TDR
        procedure :: omegaHat

        procedure :: tdrWallFunction => omegaWallFunction
        procedure :: tdrLowReWallFunction => omegaLowReWallFunction
        procedure :: setNearWallTdr => setNearWallOmega

        procedure :: tdrDiffusivity => omegaDiffusivityWilcox

        procedure :: applyOmegaWallFunction

        procedure :: tdrSourceTerm => omegaSourceTermWilcox
        procedure :: tdrSustTerm

        procedure :: curvatureCorrection

        ! TKE 
        procedure :: tkeWallFunction
        procedure :: setWallTke
        
        procedure :: tkeDiffusivity => kappaDiffusivityWilcox

        procedure :: tkeSourceTerm => kappaSourceTermWilcox
        procedure :: tkeSustTerm 

        ! Production
        procedure :: turbulentProduction => turbulentProductionWrapper
        procedure :: turbulentProductionKappaOmega
        procedure :: turbulentProductionIncompressible
        procedure :: vorticityProduction
        procedure :: katoLaunderProduction

        procedure :: Plim
        procedure :: wallProduction

        procedure :: F1
        procedure :: F2

        procedure :: printresidual => kappaOmegaresidual
        procedure :: writeToFile => kappaOmegaWriteToFile
        procedure :: verbose => kappaOmegaVerbose

    end type kw1988

contains

subroutine kappaOmega(this)

    !==================================================================================================================
    ! Description:
    !! kappaOmega is the model runner.
    !==================================================================================================================

        class(kw1988) :: this

        integer :: iElement, is, ie, patchFace, iBoundary, iBFace, ierr
    !------------------------------------------------------------------------------------------------------------------

        !---------------------------!
        ! Compute friction velocity !
        !---------------------------!

        call this%computeFrictionVelocity()

        !-----------------------!
        ! Assmble and solve tdr !
        !-----------------------!

        ! Wall function 
        call this%setNearWallTdr()

        ! Tdr equation
        call this%tdrEqn%assembleEq(this%tdr, this%tdrDiffusivity(), boundaryNonOrthCorr=.true.)

        call this%turbulentProduction()

        call this%tdrSourceTerm()

        ! Relax tdr equation
        if(steadySim==1) call this%tdrEqn%relaxEq(this%tdr)

        ! Force tdr to assume the prescribed value
        call this%applyOmegaWallFunction()

        ! Solve for tdr
        call this%tdrEqn%solve(this%tdr, setGuess=1, reusePC=0, reuseMat=0)
        
        ! Clean up memory
        call this%tdrEqn%clearMatAndRhs()
        call this%tdrEqn%destroyEqCoeffs()

        ! Update fields
        call this%tdr%updateBoundaryTurbulenceField(this%C_b1)

        call computeGradient(this%tdr)

        ! Bound tdr
        if(flubioTurbModel%bounded==1) call bound(this%tdr, 0.0, flubioTurbModel%boundType)

        !------------------------!
        ! Assemble and solve tke !
        !------------------------!

        ! wall function
        call this%setWallTke()

        call this%tkeEqn%assembleEq(this%tke, this%tkeDiffusivity(), boundaryNonOrthCorr=.true.)

        call this%tkeSourceTerm()
        
        ! Relax tke equation
        if(steadySim==1) call this%tkeEqn%relaxEq(this%tke)

        ! solve for tke
        call this%tkeEqn%solve(this%tke, setGuess=1, reusePC=0, reuseMat=0)

        ! Clean up memory
        call this%tkeEqn%clearMatAndRhs()
        call this%tkeEqn%destroyEqCoeffs()

        ! Update boundary field
        call this%tke%updateBoundaryTurbulenceField()

        call computeGradient(this%tke)

        ! Bound tke
        if(flubioTurbModel%bounded==1) call bound(this%tke, 0.0, flubioTurbModel%boundType)

        !----------------------------!
        ! Update turbulent viscosity !
        !----------------------------!

        call this%updateEddyViscosity()

        ! Store the field into a dummy variable to be use in run time sampling
        this%tkeField%phi(:,1) = this%tke%phi(:,1)
        this%tdrField%phi(:,1) = this%tdr%phi(:,1)

    end subroutine kappaOmega

!**********************************************************************************************************************

    subroutine kappaOmegaTkeFirst(this)

    !==================================================================================================================
    ! Description:
    !! kappaOmega is the model runner.
    !==================================================================================================================

        class(kw1988) :: this

        integer :: iElement, is, ie, patchFace, iBoundary, iBFace, ierr
    !------------------------------------------------------------------------------------------------------------------

        !---------------------------!
        ! Compute friction velocity !
        !---------------------------!

        call this%computeFrictionVelocity()

        call this%turbulentProduction()

        !------------------------!
        ! Assemble and solve tke !
        !------------------------!

        ! wall function
        call this%setWallTke()

        call this%tkeEqn%assembleEq(this%tke, this%tkeDiffusivity(), boundaryNonOrthCorr=.true.)

        call this%tkeSourceTerm()
        
        ! Relax tke equation
        if(steadySim==1) call this%tkeEqn%relaxEq(this%tke)

        ! solve for tke
        call this%tkeEqn%solve(this%tke, setGuess=1, reusePC=0, reuseMat=0)

        ! Clean up memory
        call this%tkeEqn%clearMatAndRhs()
        call this%tkeEqn%destroyEqCoeffs()

        ! Update boundary field
        call this%tke%updateBoundaryTurbulenceField()

        call computeGradient(this%tke)

        ! Bound tke
        if(flubioTurbModel%bounded==1) call bound(this%tke, 0.0, flubioTurbModel%boundType)

        !-----------------------!
        ! Assemble and solve tdr !
        !-----------------------!

        ! Wall function 
        call this%setNearWallTdr()

        ! Tdr equation
        call this%tdrEqn%assembleEq(this%tdr, this%tdrDiffusivity(), boundaryNonOrthCorr=.true.)

        call this%tdrSourceTerm()

        ! Relax tdr equation
        if(steadySim==1) call this%tdrEqn%relaxEq(this%tdr)

        ! Force tdr to assume the prescribed value
        call this%applyOmegaWallFunction()

        ! Solve for tdr
        call this%tdrEqn%solve(this%tdr, setGuess=1, reusePC=0, reuseMat=0)
        
        ! Clean up memory
        call this%tdrEqn%clearMatAndRhs()
        call this%tdrEqn%destroyEqCoeffs()

        ! Update fields
        call this%tdr%updateBoundaryTurbulenceField(this%C_b1)

        call computeGradient(this%tdr)

        ! Bound tdr
        if(flubioTurbModel%bounded==1) call bound(this%tdr, 0.0, flubioTurbModel%boundType)

        !----------------------------!
        ! Update turbulent viscosity !
        !----------------------------!

        call this%updateEddyViscosity()

        ! Store the field into a dummy variable to be use in run time sampling
        this%tkeField%phi(:,1) = this%tke%phi(:,1)
        this%tdrField%phi(:,1) = this%tdr%phi(:,1)

    end subroutine kappaOmegaTkeFirst

!**********************************************************************************************************************

    subroutine modelCoeffsWilcox(this)

    !==================================================================================================================
    ! Description:
    !! modelCoeffsWilcox sets the turbulence model coefficients according to NASA.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: val
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        this%Cmu = 0.09
        this%kappa = 0.41
        this%E = 9.80
        this%B = 5.25

        this%C_a1 = 0.52 !5.0/9.0
        this%C_a2 = 0.4404

        this%C_b1 = 0.072 !0.075
        this%C_b2 = 0.0828

        this%bstar = 0.09

        this%sigma_k1 = 0.5
        this%sigma_o1 = 0.5

        this%sigma_k2 = 1.0
        this%sigma_o2 = 0.856

        this%Clim = 7.0/8.0

        this%crc = 1.40

        this%divCoeff = 2.0/3.0

        call this%overrideDefaultCoeffs()

        call this%laminarYPlus()

        call flubioTurbModel%json%get('PrintCoefficients', val, found)
        if(found .and. (lowercase(val)=='yes' .or. lowercase(val)=='on')) call this%printCoeffs()

    end subroutine modelCoeffsWilcox

!**********************************************************************************************************************

    subroutine overrideDefaultCoeffs(this)

    !==================================================================================================================
    ! Description:
    !! overrideDefaultCoeffs sets the coefficients as you might have defined in turbulenceModel dictionary.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        real :: coeff
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        ! C_mu
        call flubioTurbModel%json%get('Cmu', coeff, found)
        if(found) this%Cmu = coeff

        ! kappa
        call flubioTurbModel%json%get('kappa', coeff, found)
        if(found) this%kappa = coeff

        ! E
        call flubioTurbModel%json%get('E', coeff, found)
        if(found) this%E = coeff

        ! B
        call flubioTurbModel%json%get('B', coeff, found)
        if(found) this%B = coeff

        ! Ca1
        call flubioTurbModel%json%get('Ca1', coeff, found)
        if(found) this%C_a1 = coeff

        ! Ca2
        call flubioTurbModel%json%get('Ca2', coeff, found)
        if(found) this%C_a2 = coeff

        ! Cb1
        call flubioTurbModel%json%get('Cb1', coeff, found)
        if(found) this%C_b1 = coeff

        ! Cb2
        call flubioTurbModel%json%get('Cb2', coeff, found)
        if(found) this%C_b2 = coeff

        ! bstar
        call flubioTurbModel%json%get('bstar', coeff, found)
        if(found) this%bstar = coeff

        ! sigma_k1
        call flubioTurbModel%json%get('sigma_k1', coeff, found)
        if(found) this%sigma_k1 = coeff

        ! sigma_o1
        call flubioTurbModel%json%get('sigma_o1', coeff, found)
        if(found) this%sigma_o1 = coeff

        ! sigma_k2
        call flubioTurbModel%json%get('sigma_k2', coeff, found)
        if(found) this%sigma_k2 = coeff

        ! sigma_o2
        call flubioTurbModel%json%get('sigma_o2', coeff, found)
        if(found) this%sigma_o2 = coeff

        ! Clim
        call flubioTurbModel%json%get('Clim', coeff, found)
        if(found) this%Clim = coeff

        ! CRC
        call flubioTurbModel%json%get('crc', coeff, found)
        if(found) this%crc = coeff

        ! divCoeff
        call flubioTurbModel%json%get('divCoeff', coeff, found)
        if(found) this%divCoeff = coeff

    end subroutine overrideDefaultCoeffs

! *********************************************************************************************************************

    subroutine printCoeffsKappaOmega(this)

    !==================================================================================================================
    ! Description:
    !! printCoeffsKappaOmega prints coefficients to screen.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        if(id==0) then
            write(*,'(A)') '!----------------------------------!'
            write(*,'(A)') '! Kappa-Omega Wilcox coefficients: !'
            write(*,'(A)') '!----------------------------------!'
            write(*,*)
            write(*,'(A, f6.4)') ' Cmu = ', this%Cmu
            write(*,'(A, f6.4)') ' kappa = ', this%kappa
            write(*,'(A, f6.4)') ' Ca1 = ', this%C_a1
            write(*,'(A, f6.4)') ' Ca2 = ', this%C_a2
            write(*,'(A, f6.4)') ' Cb1 = ', this%C_b1
            write(*,'(A, f6.4)') ' Cb2 = ', this%C_b2
            write(*,'(A, f6.4)') ' bstar = ', this%bstar
            write(*,'(A, f6.4)') ' sigma_k1 = ', this%sigma_k1
            write(*,'(A, f6.4)') ' sigma_k2 = ', this%sigma_k2
            write(*,'(A, f6.4)') ' sigma_o1 = ', this%sigma_o1
            write(*,'(A, f6.4)') ' sigma_o2 = ', this%sigma_o2
            write(*,'(A, f6.4)') ' Clim = ', this%Clim
            write(*,'(A, f6.4)') ' divCoeff = ', this%divCoeff
            write(*,'(A)') '!---------------------------------!'
            write(*,*)
        end if

    end subroutine printCoeffsKappaOmega

! *********************************************************************************************************************

    subroutine createKappaOmega(this)

    !==================================================================================================================
    ! Description:
    !! createKappaOmega is the model constructor.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: ksp_name

        ! DELETE
        integer is, ie, iBoundary, iBFace, iElement, patchFace
    !------------------------------------------------------------------------------------------------------------------

        ! Model coeffs
        call this%modelCoeffs()
        call this%createCommonQuantities()

        ! TKE
        call this%tke%setUpField('Tke', 'turbulence', 1)
        if(flubioControls%restFrom/=-1) call this%tke%readFromFile(flubioControls%restFrom)
        call computeGradient(this%tke)

        call this%tkeField%setUpWorkField('Tke', classType='turbulence', nComp=1)

        ! TDR
        call this%tdr%setUpField('Tdr', 'turbulence', 1, this%C_b1)
        if(flubioControls%restFrom/=-1) call this%tdr%readFromFile(flubioControls%restFrom)
        call computeGradient(this%tdr)

        call this%tdrField%setUpWorkField('Tdr', classType='turbulence', nComp=1)

        ! Update nut
        call this%updateEddyViscosity()
        call this%nut%appendFieldToList()

        call this%nut%writeToFile()
        call nu%writeToFile()

        ! Update Tdr with wall function value
        call this%setNearWallTdr()

        ! Equations
        call this%tkeEqn%createEq('tkeEqn', this%tke%nComp)
        ksp_name = trim(this%tkeEqn%eqName)
        call this%tkeEqn%setEqOptions(ksp_name)

        call this%tdrEqn%createEq('tdrEqn', this%tdr%nComp)
        ksp_name = trim(this%tdrEqn%eqName)
        call this%tdrEqn%setEqOptions(ksp_name)

        call this%tke%writeToFile()
        call this%tdr%writeToFile()

    end subroutine createKappaOmega

!**********************************************************************************************************************

    subroutine updateEddyViscosityWilcox(this)

    !==================================================================================================================
    ! Description:
    !! updateEddyViscosityWilcox computes the eddy viscosity according to the turbulence model.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: density, tke, tdr, m, F1kw, F2kw, SMALL

        real :: wHat(numberOfElements)
    !------------------------------------------------------------------------------------------------------------------

        ! Prepare
        SMALL = 1.e-15

        wHat = this%omegaHat()

        !------------------------!
        ! Compute eddy viscosity !
        !------------------------!

        do iElement=1,numberOfElements

            density = rho%phi(iElement,1)
            tke = max(this%tke%phi(iElement,1),0.0)

            tdr = wHat(iElement)
            this%nut%phi(iElement,1) = density*tke/max(tdr,SMALL)

        enddo

        ! Correct eddy viscosity at wall boundary cells
        call this%nutWallFunction()

        ! Update processor boundaries
        call this%nut%updateGhosts()

        ! Bound eddy viscosity
        call bound(this%nut, 0.0, flubioTurbModel%boundType)

        ! Compute effective viscosity
        nu%phi(1:numberOfElements,1) = viscos + this%nut%phi(1:numberOfElements,1)

        ! Update effective viscosity at processor boundaries
        call nu%updateGhosts()

    end subroutine updateEddyViscosityWilcox

!**********************************************************************************************************************

    subroutine nutWallFunctionWilcox(this)

    !==================================================================================================================
    ! Description:
    !! nutWallFunctionWilcox computes the eddy viscosity at wall boundaries.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iBFace, iElement, is, ie, iOwner, iWall, patchFace
    !------------------------------------------------------------------------------------------------------------------

        real :: density, tke, tdr, SMALL
    !------------------------------------------------------------------------------------------------------------------

        SMALL = 1.e-15

        !--------------------------------------!
        ! Correct viscosity at wall boundaries !
        !--------------------------------------!

        i = 0
        do iBoundary=1,numberOfRealBound

            iWall = 0
            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
            patchFace = numberOfElements + (is-numberOfIntFaces-1)

            if(trim(mesh%boundaries%bcType(iBoundary))=='wall') i=i+1

            ! Wall function
            if(trim(mesh%boundaries%bcType(iBoundary))=='wall' .and. trim(this%tdr%bCond(iBoundary))=='omegaWallFunction') then

                iWall = iWall+1
                do iBFace=is,ie
                    patchFace = patchFace+1
                    nu%phi(patchFace,1) = this%nuWall(iWall,i)
                    this%nut%phi(patchFace,1) = this%nuWall(iWall,i) - viscos
                end do

            ! Resolved wall
            elseif((trim(mesh%boundaries%bcType(iBoundary))=='wall' .and. &
                   trim(this%tdr%bCond(iBoundary))/='omegaWallFunction')) then

                iWall = iWall+1
                do iBFace=is,ie
                    patchFace = patchFace+1
                    nu%phi(patchFace,1) = viscos
                    this%nut%phi(patchFace,1) = 0.0
                end do

            ! Calculated from k and omega
            elseif(trim(mesh%boundaries%bcType(iBoundary))/='wall') then

                do iBFace=is,ie

                    patchFace = patchFace+1

                    density = rho%phi(patchFace,1)
                    tke = max(this%tke%phi(patchFace,1),0.0)
                    tdr = this%tdr%phi(patchFace,1)

                    this%nut%phi(patchFace,1) = density*tke/tdr
                    nu%phi(patchFace,1) = viscos + density*tke/max(tdr, SMALL)

                end do

            end if

        end do

    end subroutine nutWallFunctionWilcox

! *********************************************************************************************************************

    function omegaHat(this) result(wHat)
    
        class(kw1988) :: this

        type(ttbTensor2) :: G
    !------------------------------------------------------------------------------------------------------------------    

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------
        
        real :: tdr

        real :: modS(numberOfElements), wHat(numberofElements)
    !------------------------------------------------------------------------------------------------------------------
        
        call G%create(dim=numberOfElements, phi=velocity%phiGrad(1:numberOfElements, 1:3, 1:3))
        modS = sqrt(2.0)*norm(symm(G))

        do iElement=1,numberOfElements
            tdr = this%tdr%phi(iElement,1)
            wHat(iElement) = max(tdr,this%Clim*modS(iElement)/sqrt(this%bstar))
        enddo    
        
    end function omegaHat

! *********************************************************************************************************************

    subroutine setNearWallOmega(this) 

    !==================================================================================================================
    ! Description:
    !! setNearWallOmega sets the value of omega at the cell centers of boundary cells (type wall only)
    !! using the automatic wall treatment.
    !==================================================================================================================
    
        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------
    
        integer :: iBoundary, iBFace, iOwner, is, ie, patchFace
    !------------------------------------------------------------------------------------------------------------------
    
        real :: omega
        !! omega at the near cell
    !------------------------------------------------------------------------------------------------------------------
    
        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            patchFace = numberOfElements + (is-numberOfIntFaces-1)

            ! High Re wall Function
            if(trim(this%tdr%bCond(iBoundary))=='omegaWallFunction') then

                ! Set omega at boundary cells and faces
                do iBFace=is, ie
                    patchFace = patchFace+1
                    iOwner = mesh%owner(iBFace)
                    omega = this%tdrWallFunction(iBFace)
                    this%tdr%phi(iOwner,1) = omega
                    this%tdr%phi(patchFace, 1) = omega
                enddo

            elseif(trim(this%tdr%bCond(iBoundary))=='omegaLowReWallFunction') then

                ! Do not modify omega at cell centers in this case
                do iBFace=is, ie
                    patchFace = patchFace+1
                    iOwner = mesh%owner(iBFace)
                    omega = this%tdrLowReWallFunction(iBFace)
                    this%tdr%phi(patchFace, 1) = omega
                enddo

            endif

        enddo
    
        ! Update omega gradient
        call computeGradient(this%tdr)

    end subroutine setNearWallOmega

! *********************************************************************************************************************

    function omegaLowReWallFunction(this, iBFace) result(omegaWall)

    !==================================================================================================================
    ! Description:
    !! omegaLowReWallFunction returns the low Re omega wall value.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iBFace
    !------------------------------------------------------------------------------------------------------------------

        real :: aCb, bCb, corr

        real :: omegaWall, d, mu, c_b1
    !------------------------------------------------------------------------------------------------------------------

        C_b1 = this%C_b1

        ! Compute omega at boundary face
        d = dot_product(mesh%CN(iBFace,:), mesh%Sf(iBFace,:)/norm2(mesh%Sf(iBFace,:)))

        omegaWall = (6.0*viscos)/(C_b1*d**2)

        ! 60.0 gives me an odd result on flat plate
        !if(flubioTurbModel%ransModel==2) then
        !    omegaWall = (60.0*viscos)/(C_b1*d**2)
        !else
        !    omegaWall = (6.0*viscos)/(C_b1*d**2)
        !endif

    end function omegaLowReWallFunction

! *********************************************************************************************************************

    function omegaWallFunction(this, iBFace) result(bCb)

    !==================================================================================================================
    ! Description:
    !! omegaWallFunction sets the value of omega at the cell centers of boundary cells (only of type wall)
    !! using the automatic wall treatment.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iBFace, iOwner
    !------------------------------------------------------------------------------------------------------------------

        real :: aCb, bCb

        real :: C_b1, C_mu, Cmu025, kappa, yPlus

        real :: omegaVis, omegaLog, omegaWall, d, tke
    !------------------------------------------------------------------------------------------------------------------

        ! Coefficients
        kappa = this%kappa
        C_b1 = 0.075 !this%C_b1
        C_mu = this%Cmu
        Cmu025 = C_mu**0.25

        ! Find omega value at cell centers 
        iOwner = mesh%owner(iBFace)

        yPlus = this%yPlus(iOwner)
        d = dot_product(mesh%CN(iBFace,:), mesh%Sf(iBFace,:)/norm2(mesh%Sf(iBFace,:)))
        tke = this%tke%phi(iOwner,1)

        omegaVis = (6.0*viscos)/(C_b1*d**2)
        omegaLog = sqrt(max(tke,0.0))/(Cmu025*kappa*d)

        ! Omega at walls, using advanced blending methods
        omegaWall = blend(omegaVis, omegaLog, this%wallFunctionBlending, yPlus, threshold=this%yPlusLaminar, n=2.0)

        bCb = omegaWall

    end function omegaWallFunction

! *********************************************************************************************************************

    function omegaDiffusivityWilcox(this) result(muEff)

    !==================================================================================================================
    ! Description:
    !! omegaDiffusivityWilcox computes the diffusion coefficient for the tdr equation.
    !==================================================================================================================
    
        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------      

        type(flubioField) :: muEff
        !! effective diffusivity
    !------------------------------------------------------------------------------------------------------------------
    
        real :: sigma
    !------------------------------------------------------------------------------------------------------------------
    
        sigma = this%sigma_o1
    
        ! Create field
        call muEff%createWorkField(fieldName='muEff', classType='scalar', nComp=1)

        !Compute effective diffusivities
        muEff%phi(:,1) = viscos + this%nut%phi(:,1)*sigma
        call muEff%updateGhosts()
    
    end function omegaDiffusivityWilcox

!**********************************************************************************************************************

    subroutine applyOmegaWallFunction(this)

    !==================================================================================================================
    ! Description:
    !! applyOmegaWallFunction applies the wall function if they are required.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iOwner, iBFace, iComp, is ,ie, bFlag
    !------------------------------------------------------------------------------------------------------------------

        real :: aCb, bCb
    !------------------------------------------------------------------------------------------------------------------

        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            ! High Re wall Function
            if(trim(this%tdr%bCond(iBoundary))=='omegaWallFunction') then

                do iBFace=is, ie

                    iOwner = mesh%owner(iBFace)
                    bCb = this%tdrWallFunction(iBFace)

                    ! Set wall function value in the first cell
                    this%tdrEqn%aC(iOwner,1) = 1.0
                    this%tdrEqn%anb(iOwner)%coeffs = 0.0
                    this%tdrEqn%bC(iOwner,1) = bCb

                enddo

            endif

        enddo

    end subroutine applyOmegaWallFunction

!**********************************************************************************************************************

    subroutine omegaSourceTermWilcox(this)

    !==================================================================================================================
    ! Description:
    !! omegaSourceTermWilcox computes the source term for the tdr equation.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: divU(numberOfElements) 
        !! divergence of U.

        real :: F4(numberOfElements)  
        !! curvature correction function.

        real :: density, tdr, nut, Pk, vol, gamm, beta, ccr, small

        real :: aC, bC, positiveDiv, negativeDiv
    !------------------------------------------------------------------------------------------------------------------

        small = 1.e-12

        gamm = this%C_a1
        beta = this%C_b1

        divU = -this%divCoeff*gamm*div(mf)

        if(this%curvatureCorr==1) then 
            F4 = this%curvatureCorrection()
        else 
            F4 = 1.0
        endif
        
        ! Compute bstar*rho*omega and gamma*P/nut 
        do iElement=1,numberOfElements

            tdr = this%tdr%phi(iElement,1)

            density = rho%phi(iElement,1)
            nut = this%nut%phi(iElement,1)
            Pk = this%Pk(iElement)
            ccr = F4(iElement)

            vol = mesh%volume(iElement)

            ! Divergence term (hope it stabilises)
            positiveDiv = doubleBarFunction(-density*divU(iElement), 0.0)
            negativeDiv = doubleBarFunction(density*divU(iElement)*tdr, 0.0)

            aC = ccr*beta*density*tdr*vol + positiveDiv
            bC = (gamm*Pk/max(nut,small))*vol + negativeDiv

            this%tdrEqn%aC(iElement,1) = this%tdrEqn%aC(iElement,1) + aC
            this%tdrEqn%bC(iElement,1) = this%tdrEqn%bC(iElement,1) + bC

        enddo

        if(flubioTurbModel%sustTerm) call this%tdrSustTerm()

    end subroutine omegaSourceTermWilcox

!**********************************************************************************************************************

    function curvatureCorrection(this) result(F4)

    !==================================================================================================================
    ! Description:
    !! curvatureCorrection computes the simplified curvature correction by Hellsten.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(ttbTensor2) :: G
    !------------------------------------------------------------------------------------------------------------------
   
        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: S(numberofElements)

        real :: W(numberofElements)

        real :: R(numberofElements)

        real :: F4(numberofElements)
    !------------------------------------------------------------------------------------------------------------------

        call G%create(dim=numberOfElements, phi=velocity%phiGrad(1:numberOfElements, 1:3, 1:3))

        S = sqrt(2.0)*norm(symm(G)) + 1.0e-15
        W = sqrt(2.0)*norm(skw(G))

        R = (W/S)*(W/S - 1.0)

        F4 = 1.0/(1.0 + this%crc*R)

    end function curvatureCorrection

!**********************************************************************************************************************

    subroutine tdrSustTerm(this)

    !==================================================================================================================
    ! Description:
    !! tdrSustTerm computes the ambient turbulence source term for the tdr equation.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: density, vol, beta

        real :: F1kw, F2kw, bC
    !------------------------------------------------------------------------------------------------------------------

        if(this%ransModel==0) beta = this%C_b1

        do iElement=1,numberOfElements

            if(this%ransModel /= 0) then
                F1kw = this%F1(iElement)
                beta = F1kw*this%C_b1 + (1-F1kw)*this%C_b2
            end if

            density = rho%phi(iElement,1)
            vol = mesh%volume(iElement)

            bC = beta*density*vol*this%tdr_amb**2
            this%tdrEqn%bC(iElement,1) = this%tdrEqn%bC(iElement,1) + bC

        enddo

    end subroutine tdrSustTerm

!**********************************************************************************************************************

    subroutine TkeSustTerm(this)

    !==================================================================================================================
    ! Description:
    !! TkeSustTerm computes the ambient turbulence source term for the tke equation.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real ::  density, vol, beta

        real ::  bC
    !------------------------------------------------------------------------------------------------------------------

        beta = this%bstar

        do iElement=1,numberOfElements

            density = rho%phi(iElement,1)
            vol = mesh%volume(iElement)

            bC = beta*this%tke_amb*this%tdr_amb
            this%tdrEqn%bC(iElement,1) = this%tdrEqn%bC(iElement,1) + bC

        enddo

    end subroutine tkeSustTerm

! *********************************************************************************************************************

    subroutine setWallTke(this)

    !==================================================================================================================
    ! Description:
    !! setWallTke set the wall value for k.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iOwner, iBFace, iComp, is ,ie, patchFace, bFlag
    !------------------------------------------------------------------------------------------------------------------

        real :: aCb, bCb, tkeWall
    !------------------------------------------------------------------------------------------------------------------

        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
            patchFace = numberOfElements + (is-numberOfIntFaces-1)

            if(trim(this%tke%bCond(iBoundary))=='tkeLowReWallFunction') then

                do iBFace=is, ie

                    patchFace = patchFace+1
                    iOwner = mesh%owner(iBFace)

                    ! Low Re wall Function
                    tkeWall = this%tkeWallFunction(iBFace)

                    ! Update wall value
                    this%tke%phi(patchFace,1) = tkeWall

                enddo

            endif

        enddo

    ! Update Tke gradient
    call computeGradient(this%tke)

    end subroutine setWallTke

! *********************************************************************************************************************

    function tkeWallFunction(this, iBFace) result(tkeWall)

    !==================================================================================================================
    ! Description:
    !! tkeWallFunction implements the tke wall function (the wall treatment is automatic)
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBFace, iOwner
    !------------------------------------------------------------------------------------------------------------------

        real :: C, Ck, Bk, Cf, Ceps2, Cmu25

        real :: tkeWall, mu, d, utau, yPlus
    !------------------------------------------------------------------------------------------------------------------

        iOwner = mesh%owner(iBFace)

        Cmu25 = this%Cmu**0.25

        d = dot_product(mesh%CN(iBFace,:), mesh%Sf(iBFace,:)/norm2(mesh%Sf(iBFace,:)))
        utau =  Cmu25*sqrt(this%tke%phi(iOwner,1))
        yPlus = utau*d/viscos

        if(yPlus>this%yPlusLaminar) then
            Ck = -0.416;
            Bk = 8.366;
            tkeWall= (Ck/this%kappa)*log(yPlus) + Bk;
        else
             C = 11.0;
             Ceps2 = 1.9;
             Cf = (1.0/(yPlus + C)**2 + 2.0*yPlus/C**3 - 1.0/C**2);
             tkeWall = (2400.0/Ceps2**2)*Cf;
        end if

        tkeWall = tkeWall*utau**2

    end function tkeWallFunction

!**********************************************************************************************************************

    function kappaDiffusivityWilcox(this) result(muEff)

    !==================================================================================================================
    ! Description:
    !! kappaDiffusivityWilcox computes the diffusion coefficient for the tke equation.
    !==================================================================================================================
    
        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(flubioField) :: muEff
        !! effective diffusivity
    !------------------------------------------------------------------------------------------------------------------
    
        real :: sigma
    !------------------------------------------------------------------------------------------------------------------
    
        sigma = this%sigma_k1
        
        ! Create field
        call muEff%createWorkField(fieldName='muEff', classType='scalar', nComp=1)

        ! Compute effective diffusivity 
        muEff%phi(:,1) = viscos + this%nut%phi(:,1)*sigma
        call muEff%updateGhosts()

    end function kappaDiffusivityWilcox

!**********************************************************************************************************************

    subroutine kappaSourceTermWilcox(this)

    !==================================================================================================================
    ! Description:
    !! kappaSourceTermWilcox computes the source term for the tke equation.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: divU(numberOfElements) 
        !! divergence of U

        real :: bstar, tke, tdr, Pk, density, vol

        real :: aC, bC, positiveDiv, negativeDiv
    !------------------------------------------------------------------------------------------------------------------

        bstar = this%bstar

        divU = -this%divCoeff*div(mf)

        !-------------------------!
        ! Compute tke source term !
        !-------------------------!

        do iElement=1,numberOfElements

            density = rho%phi(iElement,1)
            Pk = this%Pk(iElement)
            tke = this%tke%phi(iElement,1)
            tdr = this%tdr%phi(iElement,1)

            vol = mesh%volume(iElement)

            ! Divergence term (hope it stabilises)
            positiveDiv = doubleBarFunction(-density*divU(iElement), 0.0)
            negativeDiv = doubleBarFunction(density*divU(iElement)*tke, 0.0)

            aC = bstar*density*tdr*vol + positiveDiv
            bC = Pk*vol + negativeDiv

            this%tkeEqn%aC(iElement,1) = this%tkeEqn%aC(iElement,1) + aC
            this%tkeEqn%bC(iElement,1) = this%tkeEqn%bC(iElement,1) + bC

        enddo

         if(flubioTurbModel%sustTerm) call this%tkeSustTerm()

    end subroutine kappaSourceTermWilcox

!**********************************************************************************************************************

    subroutine turbulentProductionWrapper(this)

    !==================================================================================================================
    ! Description:
    !! turbulentProduction computes the turbulent production Pk.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------
        
        if(this%productionType==0) then
            call this%turbulentProductionKappaOmega()
        elseif(this%productionType==1) then
            call this%turbulentProductionIncompressible()
        elseif(this%productionType==2) then
            call this%vorticityProduction()
        elseif(this%productionType==3) then
            call this%katoLaunderProduction()
        else
            call flubioStopMsg('ERROR: please specify a method to compute the turbulent production')
        end if

    end subroutine turbulentProductionWrapper

! *********************************************************************************************************************

    subroutine turbulentProductionKappaOmega(this)

    !==================================================================================================================
    ! Description:
    !! turbulentProduction computes the turbulent production Pk.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(ttbTensor2) :: G
        !! Gradient as a tensor        
    !------------------------------------------------------------------------------------------------------------------

        real :: P(numberOfElements)
        !! Production term

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call G%create(dim=numberOfElements, phi=velocity%phiGrad(1:numberOfElements, 1:3, 1:3))

        P = this%nut%phi(1:numberOfElements,1)*(G ** dev(G + transpose(G)))
        
        ! Wall function correction
        P = this%wallProduction(P)
        
        ! Production limiter. Limiter apply to BSL and SST only
        if(this%ransModel==1 .or. this%ransModel==2) then
            this%Pk = this%Plim(P) 
        else
            this%Pk = P
        endif

    end subroutine turbulentProductionKappaOmega

! *********************************************************************************************************************

    subroutine turbulentProductionIncompressible(this)

    !==================================================================================================================
    ! Description:
    !! turbulentProductionIncompressible computes the turbulent production Pk inculding the deviatoric term.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(ttbTensor2) :: G
        !! Gradient as a tensor
    !------------------------------------------------------------------------------------------------------------------

        real :: P(numberOfElements)
        !! Production term
    !------------------------------------------------------------------------------------------------------------------

        call G%create(dim=numberOfElements, phi=velocity%phiGrad(1:numberOfElements, 1:3, 1:3))

        P(1:numberOfElements) = 2.0*this%nut%phi(1:numberOfElements,1)*norm(symm(G))**2

        ! Wall function correction
        P = this%wallProduction(P)

        ! Production limiter. Limiter apply to BSL and SST only
        if(this%ransModel==1 .or. this%ransModel==2) then
            this%Pk = this%Plim(P) 
        else 
            this%Pk = P
        endif

    end subroutine turbulentProductionIncompressible

! *********************************************************************************************************************

    subroutine vorticityProduction(this)

    !==================================================================================================================
    ! Description:
    !! vorticityProduction computes the turbulent production Pk from the vorticity field as used in "-V" versions of k.omega.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(ttbTensor2) :: G
        !! Gradient as a tensor
    !------------------------------------------------------------------------------------------------------------------

        real :: P(numberOfElements)
        !! Production term
    !------------------------------------------------------------------------------------------------------------------

        call G%create(dim=numberOfElements, phi=velocity%phiGrad(1:numberOfElements, 1:3, 1:3))

        P(1:numberOfElements) = 2.0*this%nut%phi(1:numberOfElements,1)*norm(omega(G))**2

        ! Wall function correction
        P = this%wallProduction(P)

        ! Production limiter. Limiter apply to BSL and SST only
        if(this%ransModel==1 .or. this%ransModel==2) then
            this%Pk = this%Plim(P) 
        else 
            this%Pk = P
        endif

    end subroutine vorticityProduction

! *********************************************************************************************************************

    subroutine katoLaunderProduction(this)

    !==================================================================================================================
    ! Description:
    !! katoLaunderProduction computes the turbulent production Pk using the Kato-Launder correction.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(ttbTensor2) :: G
        !! Gradient as a tensor
    !------------------------------------------------------------------------------------------------------------------

        real :: P(numberOfElements)
        !! Production term
    !------------------------------------------------------------------------------------------------------------------

        call G%create(dim=numberOfElements, phi=velocity%phiGrad(1:numberOfElements, 1:3, 1:3))

        P(1:numberOfElements) = 2.0*this%nut%phi(1:numberOfElements,1)*norm(omega(G))*norm(symm(G))

        ! Wall function correction
        P = this%wallProduction(P)

        ! Production limiter. Limiter apply to BSL and SST only
        if(this%ransModel==1 .or. this%ransModel==2) then
            this%Pk = this%Plim(P) 
        else 
            this%Pk = P
        endif

    end subroutine katoLaunderProduction

! *********************************************************************************************************************

    function Plim(this, Pin) result(Pout)

    !==================================================================================================================
    ! Description:
    !! Plim limits the production as prescibed by BSL and SST models
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: tke, tdr
    
        real :: limiter 
        !! production limiter

        real :: Pin(numberOfElements)
        !! Input production

        real :: Pout(numberOfElements)
        !! Limited production
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements

            tke = this%tke%phi(iElement,1)
            tdr = this%tdr%phi(iElement,1)

            if(this%ransModel==1) limiter = 20.0*this%bstar*tdr*tke

            if(this%ransModel==2) limiter = 10.0*this%bstar*tdr*tke
            
            if(Pin(iElement) > limiter ) then 
                Pout(iElement) = limiter
            else 
                Pout(iElement) = Pin(iElement)
            endif

        enddo

    end function Plim 

! *********************************************************************************************************************

    function wallProduction(this, Pin) result(Pout)

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iBFace, iOwner, patchFace, wallPatchFace, iWall, is, ie, iFace
    !------------------------------------------------------------------------------------------------------------------

        real :: tke
        !! Tke 

        real :: d 
        !! wall distance 

        real :: utk 
        !! Friction velocity from k

        real :: tw 
        !! wall shear stress

        real :: magUp 
        !! wall velocity magnitude

        real :: Up(3)
        !! velocity parallel to the wall 

        real :: deltaU(3)
        !! U-Uwall

        real :: nf(3)
        !! wall normal

        real :: Pin(numberOfElements)
        !! Input production

        real :: Pout(numberOfElements)
        !! Limited production
    !------------------------------------------------------------------------------------------------------------------

        Pout = Pin

        ! Correct boundary cells
        do iBoundary=1,numberOfWallBound

            iWall = mesh%boundaries%wallBound(iBoundary)

            if(this%tdr%bCond(iWall)==trim('omegaWallFunction')) then

                is = mesh%boundaries%startFace(iWall)
                ie = is + mesh%boundaries%nFace(iWall)-1
                patchFace = numberOfElements + (is - numberOfIntFaces)-1
                !wallPatchFace = 0

                do iBFace=is,ie

                    !wallPatchFace = wallPatchFace + 1

                    patchFace = patchFace+1
                    iOwner = mesh%owner(iBFace)

                    ! Disable wall production fi yPlus < 11.46
                    if(this%yplus(iOwner) > this%yPlusLaminar) then

                        tke = max(this%tke%phi(iOwner,1),0.0)
                        d = dot_product(mesh%CN(iBFace,:), mesh%Sf(iBFace,:)/norm2(mesh%Sf(iBFace,:)))

                        ! Velocity parallel to the wall
                        nf = mesh%Sf(iBFace,:)/norm2(mesh%Sf(iBFace,:))
                        deltaU = velocity%phi(iOwner,:) - velocity%phi(patchFace,:)
    
                        ! Centroid velocity - wall velocity parallel to the wall
                        Up = deltaU - dot_product(deltaU, nf)*nf
                        magUp = norm2(Up)

                        utk = sqrt(tke)*(this%Cmu)**0.25
                        tw = (viscos + this%nut%phi(patchFace,1))*magUp/d
                        !tw = abs(this%tauWall(patchFace,iBoundary))

                        Pout(iOwner) = tw*utk/(this%kappa*d)
                    endif

                enddo

            endif

        enddo

    end function wallProduction 

! *********************************************************************************************************************

    function F1(this, iElement) result(F1kw)

    !==================================================================================================================
    ! Description:
    !! F1 computes the blending function F1 used in many turbulence models of the k-omega family.
    !==================================================================================================================
    
        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------
    
        integer :: iElement
        !! target cell
    !------------------------------------------------------------------------------------------------------------------
    
        real :: F1kw
        !! F1 function
    
        real :: CD
        !! CD as reported at https://turbmodels.larc.nasa.gov/sst.html
        
        real :: arg1
        !!arg1 as reported at https://turbmodels.larc.nasa.gov/sst.html

        real :: arg2
        !! arg2 as reported at https://turbmodels.larc.nasa.gov/sst.html
    
        real :: tke
        !! turbulent kinetic energy at target cell

        real :: tdr
        !! turbulent dissipation rate at target cell

        real :: d 
        !! wall distance at target cell

        real :: density
        !! fluid density at taget cell

        real :: gk(3)
        !! turbulent kinetic energy gradient at target cell

        real :: gw(3)
        !! turbulent dissipation rate gradient at target cell

        real :: gradk_gradw
        !! dot product between gk and gw
    !------------------------------------------------------------------------------------------------------------------
    
        ! Prepare
        tke = max(this%tke%phi(iElement,1), 0.0)
        tdr = max(this%tdr%phi(iElement,1), 1e-10)
        d = this%wd%dperp(iElement)
        density = rho%phi(iElement,1)
        gk = this%tke%phiGrad(iElement,:,1)
        gw = this%tdr%phiGrad(iElement,:,1)
        gradk_gradw = dot_product(gk, gw)
        
        ! Compute F1 
        arg2 = max(sqrt(tke)/(this%bstar*tdr*d), 500.0*viscos/(tdr*d**2))
        CD = max(2.0*density*this%sigma_o2*gradk_gradw/tdr, 1.0e-10)

        arg1 = min(arg2, 4.0*density*this%sigma_o2*tke/(CD*d**2))
        F1kw = tanh(arg1**4)

    end function F1

! *********************************************************************************************************************

    function F2(this, iElement) result(F2kw)

    !==================================================================================================================
    ! Description:
    !! F2 computes the blending function F1 used in many turbulence models of the k-oemga family.
    !==================================================================================================================
        
        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------
        
        integer :: iElement
        !! target cell
    !------------------------------------------------------------------------------------------------------------------

        real :: F2kw
        !! F2 function
    
        real :: arg2
        !! arg2 as reported at https://turbmodels.larc.nasa.gov/sst.html
    
        real :: tke
        !! turbulent kinetic energy at target cell

        real :: tdr
        !! turbulent dissipation rate at target cell

        real :: d 
        !! wall distance at target cell

        real :: density
        !! fluid density at taget cell
    !------------------------------------------------------------------------------------------------------------------
        
        ! Prepare
        tke = max(this%tke%phi(iElement,1),0.0)
        tdr = max(this%tdr%phi(iElement,1), 1e-10)
        d = this%wd%dperp(iElement)
        density = rho%phi(iElement,1)

        ! Compute F2
        arg2 = max(2.0*sqrt(tke)/(this%bstar*tdr*d), 500.0*viscos/(tdr*d**2))
        F2kw = tanh(arg2**2)
        
    end function F2
    
! *********************************************************************************************************************

    subroutine kappaOmegaWriteToFile(this)

    !==================================================================================================================
    ! Description:
    !! kappaOmegaWriteToFile writes the k-omega fields to output.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        call this%tke%writeToFile()
        call this%tdr%writeToFile()
        call this%nut%writeToFile()
        if(this%outputYPlus==1) call this%writeWallYPlus()

    end subroutine kappaOmegaWriteToFile

! *********************************************************************************************************************

    subroutine kappaOmegaResidual(this)

    !==================================================================================================================
    ! Description:
    !! kappaOmegaResidual writes the residuals to file and to standard output.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        call this%tkeEqn%printresidual()
        call this%tdrEqn%printresidual()

        call this%tkeEqn%writeresidual()
        call this%tdrEqn%writeresidual()

    end subroutine kappaOmegaResidual

! *********************************************************************************************************************

    subroutine kappaOmegaVerbose(this)

    !==================================================================================================================
    ! Description:
    !! kappaOmegaVerbose writes info to the standard output.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        call this%tke%fieldVerbose()
        call this%tdr%fieldVerbose()
        call this%nut%fieldVerbose()

    end subroutine kappaOmegaVerbose

! *********************************************************************************************************************

end module kappaOmegaWilcox
