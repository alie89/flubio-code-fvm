!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____ 	        !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|	        !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |     	        !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |     	        !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____ 	        !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|	        !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module kappaOmegaBSL

!==================================================================================================================
! Description:
!! this module contains the implementation of the k-omega BSL model.
!==================================================================================================================

    use kappaOmegaWilcox

    implicit none

    type, public, extends(kw1988) :: kwBSL

        real :: gamma_1
        real :: gamma_2

    contains

        procedure :: modelCoeffs => modelCoeffsBSL

        procedure :: tkeDiffusivity => kappaDiffusivityBSL
        procedure :: tdrDiffusivity => omegaDiffusivityBSL

        procedure :: gradTkeGradTdr
        procedure :: tdrSourceTerm => omegaSourceTermBSL

    end type kwBSL

contains

    subroutine modelCoeffsBSL(this)

    !==================================================================================================================
    ! Description:
    !! modelCoeffsBSL sets the turbulence model coefficients according to NASA.
    !==================================================================================================================

        class(kwBSL) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: val
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        !----------------------!
        ! Default Coefficients !
        !----------------------!

        this%Cmu = 0.09
        this%kappa = 0.41
        this%E = 9.80
        this%B = 5.25

        this%C_a1 = 0.52d0 !5.0d0/9.0d0
        this%C_a2 = 0.4404d0

        this%C_b1 = 0.072d0 !0.075d0
        this%C_b2 = 0.0828d0

        this%bstar = 0.09d0

        this%sigma_k1 = 0.5d0
        this%sigma_o1 = 0.5d0

        this%sigma_k2 = 1.0
        this%sigma_o2 = 0.856

        this%Clim = 7.0/8.0

        call this%laminarYPlus()

        !----------------------------!
        ! Override from dictionaries !
        !----------------------------!

        call this%overrideDefaultCoeffs()

        this%gamma_1 = this%C_b1/this%bstar - this%sigma_o1*this%kappa**2/sqrt(this%bstar)
        this%gamma_2 = this%C_b2/this%bstar - this%sigma_o2*this%kappa**2/sqrt(this%bstar)

        !-------------------!
        ! Print if required !
        !-------------------!

        call flubioTurbModel%json%get('PrintCoefficients', val, found)
        if(found .and. (lowercase(val)=='yes' .or. lowercase(val)=='on')) call this%printCoeffs()

    end subroutine modelCoeffsBSL

!**********************************************************************************************************************

    function kappaDiffusivityBSL(this) result(muEff)

    !==================================================================================================================
    ! Description:
    !! kappaDiffusivityBSL computes the diffusion coefficient for the tke equation in the BSL model.
    !==================================================================================================================

        class(kwBSL) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(flubioField) :: muEff
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iOwner, patchFace, iBFace, is, ie, iBoundary
    !------------------------------------------------------------------------------------------------------------------

        real :: sigma, F1kw, F2kw, F1test
    !------------------------------------------------------------------------------------------------------------------

        ! Create field
        call muEff%createWorkField(fieldName='muEff', classType='scalar', nComp=1)

        ! Compute effective diffusivities 
        do iElement=1,numberOfElements
            F1kw = this%F1(iElement)
            sigma = F1kw*this%sigma_k1 + (1.0-F1kw)*this%sigma_k2
            muEff%phi(iElement,1) = viscos + this%nut%phi(iElement,1)*sigma
        enddo

        ! Boundary Faces
        patchFace = numberOfElements
        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie

                patchFace = patchFace+1
                iOwner = mesh%owner(iBFace)

                !F1kw = this%F1(iOwner)
                !sigma = F1kw*this%sigma_k1 + (1-F1kw)*this%sigma_k2

                ! Do not blend at boundaries
                sigma = this%sigma_k1

                muEff%phi(patchFace,1) = viscos + this%nut%phi(patchFace,1)*sigma

            enddo

        enddo

        ! Update ghosts
        call muEff%updateGhosts()

    end function kappaDiffusivityBSL

!**********************************************************************************************************************

    function omegaDiffusivityBSL(this) result(muEff)

    !==================================================================================================================
    ! Description:
    !! omegaDiffusivityBSL computes the diffusion coefficient for the tdr equation in the BSL model.
    !==================================================================================================================

        class(kwBSL) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(flubioField) :: muEff
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iOwner, patchFace, iBFace, is, ie, iBoundary
    !------------------------------------------------------------------------------------------------------------------

        real :: sigma, F1kw
    !------------------------------------------------------------------------------------------------------------------

        ! Create field
        call muEff%createWorkField(fieldName='muEff', classType='scalar', nComp=1)    

        ! Compute effective diffusivities 
        do iElement=1,numberOfElements
            F1kw = this%F1(iElement)
            sigma = F1kw*this%sigma_o1 + (1.0-F1kw)*this%sigma_o2
            muEff%phi(iElement,1) = viscos + this%nut%phi(iElement,1)*sigma
        enddo

        ! Boundary Faces
        patchFace = numberOfElements
        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie

                patchFace = patchFace+1
                iOwner = mesh%owner(iBFace)

                !F1kw = this%F1(iOwner)
                !sigma = F1kw*this%sigma_o1 + (1-F1kw)*this%sigma_o2

                ! Do not blend at boundaries
                sigma = this%sigma_o1
                 
                muEff%phi(patchFace,1) = viscos + this%nut%phi(patchFace,1)*sigma

            enddo

        enddo

        ! Update ghosts
        call muEff%updateGhosts()

    end function omegaDiffusivityBSL

!**********************************************************************************************************************

    function gradTkeGradTdr(this) result(CDkw)

    !==================================================================================================================
    ! Description:
    !! gradTkeGradTdr computes the scalar product between the gradient of tke and the gradient of tdr.
    !==================================================================================================================

        class(kwBSL) :: this
    !------------------------------------------------------------------------------------------------------------------
    
        integer :: iElement
        !! loop index
    !------------------------------------------------------------------------------------------------------------------

        real :: density
        !! fluid density 

        real :: tdr
        !! tdr

        real :: gradK(3)
        !! Tke gradient 

        real ::  gradW(3)
        !! Tdr gradient
        
        real :: dot
        !! dot product 
        
        real :: CDkw(numberOfElements)
        !! CDkw as defined by NASA
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements

            density = rho%phi(iElement,1)
            tdr = max(this%tdr%phi(iElement,1), 1e-10)

            gradK = this%tke%phiGrad(iElement,:,1)
            gradW = this%tdr%phiGrad(iElement,:,1)
            dot = dot_product(gradK, gradW)

            CDkw(iElement) = 2.0*density*this%sigma_o2*dot/tdr

        enddo

    end function gradTkeGradTdr

!**********************************************************************************************************************

    subroutine omegaSourceTermBSL(this)

    !==================================================================================================================
    ! Description:
    !! omegaSourceTermBSL computes source term for the tdr equation for the BSL model.
    !==================================================================================================================

        class(kwBSL) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: density, tdr, nut, Pk, vol, gamm, beta, F1kw, F2kw

        real :: alphaTilde, betaTilde

        real :: aC, bC, corrbC, corraC, positiveDiv, negativeDiv, ccr

        real :: CDkw(numberOfElements)

        real :: divU(numberOfElements) 
        !! divergence of U

        real :: F4(numberOfElements)
        !! curvature correction function
    !------------------------------------------------------------------------------------------------------------------

        CDkw = this%gradTkeGradTdr()

        divU = this%divCoeff*div(mf)

        if(this%curvatureCorr==1) then 
            F4 = this%curvatureCorrection()
        else 
            F4 = 1.0
        endif
        
        ! Compute omega equation rhs
        do iElement=1,numberOfElements

            tdr = max(this%tdr%phi(iElement,1), 1e-10)
            density = rho%phi(iElement,1)
            nut = this%nut%phi(iElement,1)
            Pk = this%Pk(iElement)

            ccr = F4(iElement)
            vol = mesh%volume(iElement)

            ! Blend coeffs
            F1kw = this%F1(iElement)
            alphaTilde = F1kw*this%C_a1 + (1.0-F1kw)*this%C_a2
            betaTilde = F1kw*this%C_b1 + (1.0-F1kw)*this%C_b2

            ! Cross production 
            call doubleBar(-CDkw(iElement)/max(tdr, 1e-12), 0.0, corraC)
            call doubleBar(CDkw(iElement), 0.0, corrbC)

            ! Divergence term (hope it stabilises)
            positiveDiv = doubleBarFunction(-density*alphaTilde*divU(iElement), 0.0)
            negativeDiv = doubleBarFunction(density*alphaTilde*divU(iElement)*tdr, 0.0)

            ! Source term 
            aC = ccr*betaTilde*density*tdr*vol + (1-F1kw)*corraC*vol + positiveDiv*vol
            bC = (alphaTilde*Pk/max(nut, 1.0e-12))*vol + (1.0-F1kw)*corrbC*vol + negativeDiv*vol

            this%tdrEqn%aC(iElement,1) = this%tdrEqn%aC(iElement,1) + aC
            this%tdrEqn%bC(iElement,1) = this%tdrEqn%bC(iElement,1) + bC 

        enddo

        if(flubioTurbModel%sustTerm) call this%tdrSustTerm()

    end subroutine omegaSourceTermBSL

end module kappaOmegaBSL