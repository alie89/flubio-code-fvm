!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____ 	        !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|	        !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |     	        !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |     	        !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____ 	        !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|	        !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module qZetaModel

!======================================================================================================================
! Description:
!! this module contains the implementation of the q-zeta model by Gibson and Dafa'Alla.
!======================================================================================================================

    use baseRansModel

    implicit none

    type, public, extends(baseModelClass) :: qZeta

        ! Coefficients
        real :: C1
        real :: C2
        real :: sigmaz

        logical :: anisotropic

        ! Fields
        type(flubioField) :: q
        type(flubioField) :: zeta

    contains

        procedure :: turbulenceModel => qz

        procedure :: modelCoeffs => modelCoeffsQZeta
        procedure :: printCoeffs => printCoeffsQZeta
        procedure :: overrideDefaultCoeffs

        procedure :: createModel => createQZeta
        procedure :: updateEddyViscosity => updateEddyViscosityQZeta
        procedure :: nutWallFunction => nutWallFunctionQZeta

        ! Zeta
        procedure :: tdrDiffusivity => zetaDiffusivity
        procedure :: tdrSourceTerm => zetaSourceTerm

        ! Q 
        procedure :: tkeDiffusivity => qDiffusivity
        procedure :: tkeSourceTerm => qSourceTerm
       
        ! Production
        procedure :: turbulentProductionqZeta

        ! Functons
        procedure :: f2 
        procedure :: fmu
        procedure :: Eqz
        procedure :: computeTke 
        procedure :: computeTdr

        ! Print and writes
        procedure :: printresidual => qZetaResidual
        procedure :: writeToFile => qZetaWriteToFile
        procedure :: verbose => qZetaVerbose

    end type qZeta

contains

    subroutine qz(this)

    !==================================================================================================================
    ! Description:
    !! qz is the model runner.
    !==================================================================================================================

        class(qZeta) :: this
    !------------------------------------------------------------------------------------------------------------------

        !---------------------------!
        ! Compute friction velocity !
        !---------------------------!

        call this%computeFrictionVelocity()

        !-----------------------!
        ! Assmble and solve tdr !
        !-----------------------!

        ! Zeta equation
        call this%tdrEqn%assembleEq(this%zeta, this%tdrDiffusivity(), boundaryNonOrthCorr=.true.)

        call this%turbulentProductionqZeta()

        call this%tdrSourceTerm()

        ! Relax tdr equation
        if(steadySim==1) call this%tdrEqn%relaxEq(this%zeta)

        ! Solve for tdr
        call this%tdrEqn%solve(this%zeta, setGuess=1, reusePC=0, reuseMat=0)

        ! Clean up memory
        call this%tdrEqn%clearMatAndRhs()
        call this%tdrEqn%destroyEqCoeffs()

        ! Update fields
        call this%zeta%updateBoundaryTurbulenceField()

        call computeGradient(this%zeta)

        ! Bound tdr
        if(flubioTurbModel%bounded==1) call bound(this%zeta, 1e-12, flubioTurbModel%boundType)

        !------------------------!
        ! Assemble and solve tke !
        !------------------------!

        call this%tkeEqn%assembleEq(this%q, this%tkeDiffusivity(), boundaryNonOrthCorr=.true.)

        call this%tkeSourceTerm()
        
        ! Relax tke equation
        if(steadySim==1) call this%tkeEqn%relaxEq(this%q)

        ! solve for tke
        call this%tkeEqn%solve(this%q, setGuess=1, reusePC=0, reuseMat=0)

        ! Clean up memory
        call this%tkeEqn%clearMatAndRhs()
        call this%tkeEqn%destroyEqCoeffs()

        ! Update boundary field
        call this%q%updateBoundaryTurbulenceField()

        call computeGradient(this%q)

        ! Bound tke
        if(flubioTurbModel%bounded==1) call bound(this%q, 1e-12, flubioTurbModel%boundType)

        !----------------------------!
        ! Update turbulent viscosity !
        !----------------------------!

        call this%updateEddyViscosity()

        ! Store the field into a dummy variable to be use in run time sampling
        this%tkeField%phi(:,1) = this%q%phi(:,1)
        this%tdrField%phi(:,1) = this%zeta%phi(:,1)

    end subroutine qz

!**********************************************************************************************************************

    subroutine modelCoeffsQZeta(this)

    !==================================================================================================================
    ! Description:
    !! modelCoeffsQZeta sets the turbulence model coefficients according to NASA.
    !==================================================================================================================

        class(qZeta) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: val
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        this%Cmu = 0.09
        this%kappa = 0.41
        this%E = 9.80
        this%B = 5.25

        this%C1 = 1.44
        this%C2 = 1.92
        this%sigmaz = 1.0/1.3
        this%anisotropic = .true.

        call this%overrideDefaultCoeffs()

        call this%laminarYPlus()

        call flubioTurbModel%json%get('PrintCoefficients', val, found)
        if(found .and. (lowercase(val)=='yes' .or. lowercase(val)=='on')) call this%printCoeffs()

    end subroutine modelCoeffsQZeta

!**********************************************************************************************************************

    subroutine overrideDefaultCoeffs(this)

    !==================================================================================================================
    ! Description:
    !! overrideDefaultCoeffs sets the coefficients as you might have defined in turbulenceModel dictionary.
    !==================================================================================================================

        class(qZeta) :: this
    !------------------------------------------------------------------------------------------------------------------

        real :: coeff
    !------------------------------------------------------------------------------------------------------------------

        logical :: anisotropic

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        ! C_mu
        call flubioTurbModel%json%get('Cmu', coeff, found)
        if(found) this%Cmu = coeff

        ! kappa
        call flubioTurbModel%json%get('kappa', coeff, found)
        if(found) this%kappa = coeff

        ! E
        call flubioTurbModel%json%get('E', coeff, found)
        if(found) this%E = coeff

        ! B
        call flubioTurbModel%json%get('B', coeff, found)
        if(found) this%B = coeff

        ! C1
        call flubioTurbModel%json%get('C1', coeff, found)
        if(found) this%C1 = coeff

        ! C2
        call flubioTurbModel%json%get('C2', coeff, found)
        if(found) this%C2 = coeff

        ! sigmaz
        call flubioTurbModel%json%get('sigmaz', coeff, found)
        if(found) this%sigmaz = coeff

        ! anisotropic
        call flubioTurbModel%json%get('anisotropic', anisotropic, found)
        if(found) this%anisotropic = anisotropic

    end subroutine overrideDefaultCoeffs

! *********************************************************************************************************************

    subroutine printCoeffsQZeta(this)

    !==================================================================================================================
    ! Description:
    !! printCoeffsQZeta prints coefficients to screen.
    !==================================================================================================================

        class(qZeta) :: this
    !------------------------------------------------------------------------------------------------------------------

        if(id==0) then
            write(*,'(A)') '!----------------------!'
            write(*,'(A)') '! q-zeta coefficients: !'
            write(*,'(A)') '!----------------------!'
            write(*,*)
            write(*,'(A, f6.4)') ' Cmu = ', this%Cmu
            write(*,'(A, f6.4)') ' kappa = ', this%kappa
            write(*,'(A, f6.4)') ' C1 = ', this%C1
            write(*,'(A, f6.4)') ' C2 = ', this%C2
            write(*,'(A, f6.4)') ' sigmaz = ', this%sigmaz
            write(*,'(A)') '!---------------------------------!'
            write(*,*)
        end if

    end subroutine printCoeffsQZeta

! *********************************************************************************************************************

    subroutine createQZeta(this)

    !==================================================================================================================
    ! Description:
    !! createQZeta is the model constructor.
    !==================================================================================================================

        class(qZeta) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: ksp_name
    !------------------------------------------------------------------------------------------------------------------

        ! Model coeffs
        call this%modelCoeffs()
        call this%createCommonQuantities()

        ! q
        call this%q%setUpField('Q', 'turbulence', 1)
        if(flubioControls%restFrom/=-1) call this%q%readFromFile(flubioControls%restFrom)
        call computeGradient(this%q)

        call this%tkeField%setUpWorkField('Q', classType='turbulence', nComp=1)

        ! Zeta
        call this%zeta%setUpField('Zeta', 'turbulence', 1)
        if(flubioControls%restFrom/=-1) call this%zeta%readFromFile(flubioControls%restFrom)
        call computeGradient(this%zeta)

        call this%tdrField%setUpWorkField('Zeta', classType='turbulence', nComp=1)

        ! Update nut
        call this%updateEddyViscosity()
        call this%nut%appendFieldToList()
        call this%nut%writeToFile()
        call nu%writeToFile()

        ! Equations
        call this%tkeEqn%createEq('tkeEqn', this%q%nComp)
        ksp_name = trim(this%tkeEqn%eqName)
        call this%tkeEqn%setEqOptions(ksp_name)

        call this%tdrEqn%createEq('tdrEqn', this%zeta%nComp)
        ksp_name = trim(this%tdrEqn%eqName)
        call this%tdrEqn%setEqOptions(ksp_name)

        call this%q%writeToFile()
        call this%zeta%writeToFile()

    end subroutine createQZeta

!**********************************************************************************************************************

    subroutine updateEddyViscosityQZeta(this)

    !==================================================================================================================
    ! Description:
    !! updateEddyViscosityQZeta computes the eddy viscosity according to the turbulence model.
    !==================================================================================================================

        class(qZeta) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: fmu(numberOfElements + numberOfBFaces)

        real :: density, q, zeta, tke, tdr, SMALL
    !------------------------------------------------------------------------------------------------------------------

        ! Prepare
        SMALL = 1.e-15

        fmu = this%fmu()

        !------------------------!
        ! Compute eddy viscosity !
        !------------------------!

        do iElement=1,numberOfElements

            density = rho%phi(iElement,1)
            q = max(this%q%phi(iElement,1),1e-12)
            zeta = this%zeta%phi(iElement,1)

            tke = this%computeTke(q, zeta)
            tdr = this%computeTdr(q, zeta)

            this%nut%phi(iElement,1) = this%Cmu*fmu(iElement)*(tke**2/tdr)

        enddo

        ! Correct eddy viscosity at wall boundary cells
        call this%nutWallFunction()

        ! Update processor boundaries
        call this%nut%updateGhosts()

        ! Bound eddy viscosity
        call bound(this%nut, 0.0, flubioTurbModel%boundType)

        ! Compute effective viscosity
        nu%phi(1:numberOfElements,1) = viscos + this%nut%phi(1:numberOfElements,1)

        ! Update effective viscosity at processor boundaries
        call nu%updateGhosts()

    end subroutine updateEddyViscosityQZeta

!**********************************************************************************************************************

    subroutine nutWallFunctionQZeta(this)

    !==================================================================================================================
    ! Description:
    !! nutWallFunctionQZeta computes the eddy viscosity at wall boundaries.
    !==================================================================================================================

        class(qZeta) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iBFace, iElement, is, ie, iOwner, iWall, patchFace
    !------------------------------------------------------------------------------------------------------------------

        real :: fmu(numberOfElements + numberOfBFaces)

        real :: density, q, zeta, tke, tdr
    !------------------------------------------------------------------------------------------------------------------

        fmu = this%fmu()

        !--------------------------------------!
        ! Correct viscosity at wall boundaries !
        !--------------------------------------!

        i = 0
        do iBoundary=1,numberOfRealBound

            iWall = 0
            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
            patchFace = numberOfElements + (is-numberOfIntFaces-1)

            if(trim(mesh%boundaries%bcType(iBoundary))=='wall') i = i+1

            if(trim(mesh%boundaries%bcType(iBoundary))=='wall') then

                iWall = iWall+1
                do iBFace=is,ie
                    patchFace = patchFace+1
                    nu%phi(patchFace,1) = viscos
                    this%nut%phi(patchFace,1) = 0.0
                end do

            ! Calculated from k and omega
            elseif(trim(mesh%boundaries%bcType(iBoundary))/='wall') then

                do iBFace=is,ie

                    patchFace = patchFace+1

                    density = rho%phi(patchFace,1)
                    q = max(this%q%phi(patchFace,1),0.0)
                    zeta = this%zeta%phi(patchFace,1)

                    tke = this%computeTke(q, zeta)
                    tdr = this%computeTdr(q, zeta)

                    this%nut%phi(patchFace,1) = this%Cmu*fmu(patchFace)*(tke**2/tdr)
                    nu%phi(patchFace,1) = viscos + this%Cmu*fmu(patchFace)*(tke**2/tdr)

                end do

            end if

        end do

    end subroutine nutWallFunctionQZeta

! *********************************************************************************************************************

    function zetaDiffusivity(this) result(muEff)

    !==================================================================================================================
    ! Description:
    !! zetaDiffusivity computes the diffusion coefficient for the tdr equation.
    !==================================================================================================================
    
        class(qZeta) :: this
    !------------------------------------------------------------------------------------------------------------------      

        type(flubioField) :: muEff
        !! effective diffusivity
    !------------------------------------------------------------------------------------------------------------------
    
        real :: sigma
    !------------------------------------------------------------------------------------------------------------------
    
        sigma = this%sigmaz
    
        ! Create field
        call muEff%createWorkField(fieldName='muEff', classType='scalar', nComp=1)

        !Compute effective diffusivities
        muEff%phi(:,1) = viscos + this%nut%phi(:,1)*sigma
        call muEff%updateGhosts()
    
    end function zetaDiffusivity

!**********************************************************************************************************************

    subroutine zetaSourceTerm(this)

    !==================================================================================================================
    ! Description:
    !! zetaSourceTerm computes the source term for the tdr equation.
    !==================================================================================================================

        class(qZeta) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: E(numberOfElements+numberOfBFaces), f2(numberOfElements + numberOfBFaces)

        real :: q, zeta, Pk, vol, coeff, small

        real :: pos, neg

        real :: aC, bC
    !------------------------------------------------------------------------------------------------------------------

        small = 1.e-12

        E = this%Eqz()
        f2 = this%f2()

        ! Zeta source term
        do iElement=1,numberOfElements

            zeta = this%zeta%phi(iElement,1)
            q = this%q%phi(iElement,1)
            Pk = this%Pk(iElement)
            vol = mesh%volume(iElement)

            coeff = 1.0 - 2.0*this%C2*f2(iElement)

            pos = doubleBarFunction(-coeff*(zeta/q), 0.0)
            neg = doubleBarFunction(coeff*(zeta/q)*zeta, 0.0)
            aC = pos*vol
            bC = (2.0*this%C1 - 1.0)*Pk*(zeta/q)*vol + E(iElement)*vol + neg*vol

            !coeff = 1.0 - 2.0*this%C2*f2(iElement)
            !aC = 0.0
            !bC = (2.0*this%C1 - 1.0)*Pk*(zeta/q)*vol + E(iElement)*vol + coeff*(zeta/q)*zeta*vol

            this%tdrEqn%aC(iElement,1) = this%tdrEqn%aC(iElement,1) + aC
            this%tdrEqn%bC(iElement,1) = this%tdrEqn%bC(iElement,1) + bC

        enddo

    end subroutine zetaSourceTerm

!**********************************************************************************************************************

    function qDiffusivity(this) result(muEff)

    !==================================================================================================================
    ! Description:
    !! qDiffusivity computes the diffusion coefficient for the tke equation.
    !==================================================================================================================
    
        class(qZeta) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(flubioField) :: muEff
        !! effective diffusivity
    !------------------------------------------------------------------------------------------------------------------
    
        real :: sigma
    !------------------------------------------------------------------------------------------------------------------
    
        sigma = 1.0
        
        ! Create field
        call muEff%createWorkField(fieldName='muEff', classType='scalar', nComp=1)

        ! Compute effective diffusivity 
        muEff%phi(:,1) = viscos + this%nut%phi(:,1)*sigma
        call muEff%updateGhosts()

    end function qDiffusivity

!**********************************************************************************************************************

    subroutine qSourceTerm(this)

    !==================================================================================================================
    ! Description:
    !! qSourceTerm computes the source term for the q equation.
    !==================================================================================================================

        class(qZeta) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: q, zeta, Pk, vol

        real :: aC, bC
    !------------------------------------------------------------------------------------------------------------------

        !-------------------------!
        ! Compute tke source term !
        !-------------------------!

        do iElement=1,numberOfElements

            Pk = this%Pk(iElement)
            q = this%q%phi(iElement,1)
            zeta = this%zeta%phi(iElement,1)

            vol = mesh%volume(iElement)

            aC = (zeta/q)*vol
            bC = Pk*vol 

            this%tkeEqn%aC(iElement,1) = this%tkeEqn%aC(iElement,1) + aC
            this%tkeEqn%bC(iElement,1) = this%tkeEqn%bC(iElement,1) + bC

        enddo

    end subroutine qSourceTerm

!**********************************************************************************************************************

    subroutine turbulentProductionQZeta(this)

    !==================================================================================================================
    ! Description:
    !! turbulentProductionQZeta computes the turbulent production Pk.
    !==================================================================================================================

        class(qZeta) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(ttbTensor2) :: G
        !! Gradient as a tensor        
    !------------------------------------------------------------------------------------------------------------------
  
        integer :: iElement

        real :: P(numberOfElements)
        !! Production term
    !------------------------------------------------------------------------------------------------------------------

        call G%create(dim=numberOfElements, phi=velocity%phiGrad(1:numberOfElements, 1:3, 1:3))

        this%Pk = 2.0*this%nut%phi(1:numberOfElements,1)*norm(symm(G))**2/(2.0*this%q%phi(1:numberOfElements,1))

    end subroutine turbulentProductionQZeta

! *********************************************************************************************************************

    function computeTke(this, q, zeta) result(tke)

        class(qZeta) :: this
    !------------------------------------------------------------------------------------------------------------------

        real :: q, zeta, tke
    !------------------------------------------------------------------------------------------------------------------

        tke = q**2

    end function computeTke

! *********************************************************************************************************************

    function computeTdr(this, q, zeta) result(tdr)

        class(qZeta) :: this
    !------------------------------------------------------------------------------------------------------------------

        real :: q, zeta, tdr 
    !------------------------------------------------------------------------------------------------------------------

        tdr = 2.0*q*zeta

    end function computeTdr

! *********************************************************************************************************************

    function f2(this) result(tmp)

        class(qZeta) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: Rt, tke, q,  zeta

        real :: tmp(numberOfElements+numberOfBFaces)
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements+numberOfBFaces
            q = this%q%phi(iElement,1)
            zeta = this%zeta%phi(iElement,1)
            tke = this%computeTke(q, zeta) 
            Rt = q*tke/(2.0*viscos*zeta)
            tmp(iElement) = 1.0-0.3*exp(-Rt**2)
        enddo 

    end function f2 

! *********************************************************************************************************************

    function fmu(this) result(tmp)

        class(qZeta) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: Rt, tke, q,  zeta

        real :: tmp(numberOfElements+numberOfBFaces)
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements+numberOfBFaces

            q = this%q%phi(iElement,1)
            zeta = this%zeta%phi(iElement,1)
            tke = this%computeTke(q, zeta) 
            Rt = q*tke/(2.0*viscos*zeta)

            if(this%anisotropic) then
                tmp(iElement) = exp((-2.5 + Rt/20.0)/(1.0 + Rt/130.0)**3)
            else
                tmp(iElement) = exp(-6.0/(1.0+Rt/50.0)**2)*(1.0 + 3.0*exp(-Rt/10.0))
            endif 

        enddo 

    end function fmu

! *********************************************************************************************************************

    function Eqz(this) result(E)

        class(qZeta) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: q, nut

        real :: tmp(numberOfElements+numberOfBFaces), E(numberOfElements)
    !------------------------------------------------------------------------------------------------------------------

        ! Compute magSqrGradGrad
        tmp = gradGradMag(velocity) 

        ! Compute E
        do iElement=1,numberOfElements
            q = this%q%phi(iElement,1)
            nut = this%nut%phi(iElement,1) 
            E(iElement) = (viscos*nut/q)*tmp(iElement)
        enddo 

    end function Eqz

! *********************************************************************************************************************

    subroutine qZetaWriteToFile(this)

    !==================================================================================================================
    ! Description:
    !! qZetaWriteToFile writes the k-omega fields to output.
    !==================================================================================================================

        class(qZeta) :: this
    !------------------------------------------------------------------------------------------------------------------

        call this%q%writeToFile()
        call this%zeta%writeToFile()
        call this%nut%writeToFile()
        if(this%outputYPlus==1) call this%writeWallYPlus()

    end subroutine qZetaWriteToFile

! *********************************************************************************************************************

    subroutine qZetaResidual(this)

    !==================================================================================================================
    ! Description:
    !! qZetaResidual writes the residuals to file and to standard output.
    !==================================================================================================================

        class(qZeta) :: this
    !------------------------------------------------------------------------------------------------------------------

        call this%tkeEqn%printresidual()
        call this%tdrEqn%printresidual()

        call this%tkeEqn%writeresidual()
        call this%tdrEqn%writeresidual()

    end subroutine qZetaResidual

! *********************************************************************************************************************

    subroutine qZetaVerbose(this)

    !==================================================================================================================
    ! Description:
    !! qZetaVerbose writes info to the standard output.
    !==================================================================================================================

        class(qZeta) :: this
    !------------------------------------------------------------------------------------------------------------------

        call this%q%fieldVerbose()
        call this%zeta%fieldVerbose()
        call this%nut%fieldVerbose()

    end subroutine qZetaVerbose

! *********************************************************************************************************************

end module qZetaModel
