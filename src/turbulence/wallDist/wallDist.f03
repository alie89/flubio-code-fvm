!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module wallDistance

!==================================================================================================================
! Description:
!! this module implements the wall distance equation ($\nabla^2\phi=-1$).
!==================================================================================================================

    use flubioFields
    use gradient
    use meshvar
    use transportEq

    implicit none

    type, public :: flubioWallDist

        type(flubioField) :: wallDist

        type(transportEqn) :: wallDistEq

        real, dimension(:), allocatable :: dperp

    contains
        procedure :: applyWallDistBoundaryConditions
        procedure :: computeWallDistance
        procedure :: correctWallDistance

    end type flubioWallDist

contains

    subroutine computeWallDistance(this)

    !==================================================================================================================
    ! Description:
    !! computeWallDistance computes the distance field from the wall boundaries.
    !==================================================================================================================

        class(flubioWallDist) :: this

        type(flubioField) :: ones
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iCorr, iElement, pcFlag, flag(numberOfBoundaries)
    !------------------------------------------------------------------------------------------------------------------

        real :: b, c, SMALL

        real :: bCDdtQ(numberOfElements,1)
    !------------------------------------------------------------------------------------------------------------------

        SMALL = 1e-8

        ! Dummy value for the diffusivity
        call ones%setUpCoeff('ones', appendToList=.false.)
        ones%phi = 1.0

        ! Create the field
        call this%wallDist%createField(fieldName='wallDist', classType='scalar', nComp=1)
        this%wallDist%bcFlag = 2
        call this%wallDist%updateGhosts()
    !    call this%wallDist%appendFieldToList()

        call mesh%boundaries%findWallBoundaries()

        ! Create the equation
        call this%wallDistEq%createEq('wallDistEqn', 1)
        call setWallDistEqOptions(this%wallDistEq, 'wallDistEqn_')

        ! Add rhs
        this%wallDistEq%bC(:,1) = this%wallDistEq%bC(:,1) + mesh%volume(:)

        ! Save the RHS with no diffusion term, reuse coeffs in non-orthogonal corrections!
        bCDdtQ = this%wallDistEq%bC

        call this%wallDistEq%addDiffusionTerm(this%wallDist, ones)

        call this%applyWallDistBoundaryConditions()

        ! Solve for wall distance
        pcFlag = 0
        do iCorr=1, flubioTurbModel%wallDistCorr+1

            call this%wallDistEq%solve(this%wallDist, setGuess=1, reusePC=pcFlag, reuseMat=iCorr)

            call this%wallDist%updateBoundaryField()

            call computeGradient(this%WallDist)

            ! Reset RHS
            this%wallDistEq%bC = bCDdtQ

            ! Update with the new cross diffusion
            call this%wallDistEq%updateRhsWithCrossDiffusion(this%wallDist, ones, 1)

            pcFlag = 1

        enddo

        !-------------------------------------------!
        ! Compute normal wall distance distribution !
        !-------------------------------------------!

        allocate(this%dperp(numberOfElements+numberOfBFaces))
        this%dperp = 0.0

        do iElement=1,numberOfElements
            c = this%wallDist%phiGrad(iElement,1,1)**2 + this%wallDist%phiGrad(iElement,2,1)**2 + this%wallDist%phiGrad(iElement,3,1)**2
            b = 2.0*this%wallDist%phi(iElement,1)
            this%dperp(iElement) = -sqrt(c + SMALL) + sqrt(c + b + SMALL)
        enddo

        !-----------------------!
        ! Correct wall distance !
        !-----------------------!

        call this%correctWallDistance()
        this%wallDist%phi(1:numberOfElements,1) = this%dperp(1:numberOfElements)
        call this%wallDist%writeToFile()

        ! Destroy all, only dperp must survive
        call ones%destroyField()
        call this%wallDist%destroyField()
        call this%wallDistEq%destroyEq()

    end subroutine computeWallDistance

! *********************************************************************************************************************

    subroutine applyWallDistBoundaryConditions(this)

    !==================================================================================================================
    ! Description:
    !! applyBoundaryConditionPhiWall applies the boundary condition for the wall distance equation.
    !==================================================================================================================

        class(flubioWallDist) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: bType
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iOwner, iBFace, is ,ie, bFlag
    !------------------------------------------------------------------------------------------------------------------

        real :: corr
    !------------------------------------------------------------------------------------------------------------------

        ! Boundary loop
        do iBoundary=1,mesh%boundaries%numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            bFlag = velocity%bcFlag(iBoundary)
            bType = trim(mesh%boundaries%bcType(iBoundary))

            do iBFace=is, ie

                ! FIXED VALUE BC EQUAL TO ZERO AT WALLS
                if (bFlag==1 .or. bFlag==8 .or. bFlag==9 .or. bFlag==10 .or. bType=='wall') then
                !if (bType=='wall') then
                    iOwner = mesh%owner(iBFace)
                    corr = mesh%gDiff(iBFace)
                    this%wallDistEq%aC(iOwner,1) = this%wallDistEq%aC(iOwner,1) + corr
                endif

            enddo
        enddo

    end subroutine applyWallDistBoundaryConditions

! *********************************************************************************************************************

    subroutine correctWallDistance(this)

    !==================================================================================================================
    ! Description:
    !! correctWallDistance assigns the wall distance at the boundary cells (known from the mesh geometry).
    !==================================================================================================================

        class(flubioWallDist) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iBFace, iOwner, is, ie
    !------------------------------------------------------------------------------------------------------------------

        ! Set a very large value to owner cell
        do iBoundary=1, numberOfWallBound

            i = mesh%boundaries%wallBound(iBoundary)
            is = mesh%boundaries%startFace(i)
            ie = is+mesh%boundaries%nFace(i)-1

            do iBFace=is,ie
                iOwner = mesh%owner(iBFace)
                this%dperp(iOwner) = dot_product(mesh%CN(iBFace,:), mesh%Sf(iBFace,:)/norm2(mesh%Sf(iBFace,:)))
            enddo

        enddo

    end subroutine correctWallDistance

! *********************************************************************************************************************

    subroutine setWallDistEqOptions(this, kspName)

    !==================================================================================================================
    ! Description:
    !! setWallDistEqOptions sets the option for the wall distance equation.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(*) :: kspName
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName, key, solverType, pcType, convOpt
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        !----------------!
        ! Solver options !
        !----------------!

        call flubioSolvers%json%get(this%eqName//'%solverType', solverType, found)
        if(found) then
            this%solverType = solverType
        else
            this%solverType = 'petsc'
        endif

        call flubioSolvers%json%get(this%eqName//'%solver', solverType, found)
        call raiseKeyErrorJSON(this%eqName//'%solver', found, 'settings/solvers')
        this%solverName = solverType

        call flubioSolvers%json%get(this%eqName//'%preconditioner', pcType, found)
        call raiseKeyErrorJSON(this%eqName//'%preconditioner', found, 'settings/solvers')
        this%pcName = pcType

        call flubioSolvers%setPreconditionerOption(lowercase(this%pcName), this%pc, this%solverType)

        !--------------------!
        ! Numerical settings !
        !--------------------!

        ! Absolute solver tolerance
        call flubioSolvers%json%get(this%eqName//'%absTol', this%absTol, found)
        call raiseKeyErrorJSON(this%eqName//'%absTol', found, 'settings/solvers')

        ! Relative solver tolerance
        call flubioSolvers%json%get(this%eqName//'%relTol', this%relTol, found)
        call raiseKeyErrorJSON(this%eqName//'%relTol', found, 'settings/solvers')

        ! Minimum and maximum number of iterations
        call flubioSolvers%json%get(this%eqName//'%minIter', this%minIter, found)
        if(.not. found) this%minIter = 0

        call flubioSolvers%json%get(this%eqName//'%maxIter', this%maxIter, found)
        if(.not. found) this%maxIter = 1000

    end subroutine setWallDistEqOptions

! *********************************************************************************************************************

end module wallDistance