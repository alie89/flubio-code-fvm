!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module flubioFields

!==================================================================================================================
! Description:
!! flubioFields implements the data structures and the methods operating on a "field".
!==================================================================================================================

    use bcDicts
    use flubioDictionaries
    use flubioMpi
    use meshvar
    use flubioContainters

    use json_module

    implicit none

    integer :: numberOfRegisteredFields

    type, public :: flubioField

        type(json_file) :: bcDict
        !! boundary condtion dictionary in json format

        character(len=30), allocatable :: fieldName(:)
        !! Name of the field

        character(10) :: bcClassType
        !! Set this paramenter to specifay which set of boundary condition the fields should point to

        character(len=30), dimension(:), allocatable :: bCond
        !! Applied boundary conditions

        character(len = 275), dimension(:,:), allocatable  :: boundaryExpression
        !! Expression passed to the interpreter

        integer nComp
        !! Number of components

        integer :: isActive
        !! Flag for IO

        integer :: gradientType
        !! Flag for gradient type

        integer :: gradientLimiterType
        !! Flag for gradient limiter type

        integer :: outputGradient
        !! Flag to save the gradient of the field

        real :: kLim
        !! Flag for gradient limiter type

        real, dimension(:,:), allocatable :: phi
        !! Vector field variable

        real, dimension(:,:), allocatable :: phif
        !! Vector field variable

        real, dimension(:,:), allocatable :: phio
        !! Vector field variable at previous time-step

        real, dimension(:,:), allocatable :: phioo
        !! Two times old vector field variable

        real, dimension(:,:,:), allocatable :: phiGrad
        !!  Vector field gradient

        integer, dimension(:), allocatable :: bcIndex
        !! Boundary address

        integer, dimension(:), allocatable :: bcFlag
        !! Boundary flags

        real, dimension(:,:), allocatable :: boundaryValue
        !! Boundary value assigned in the boundary condition

    contains

        procedure :: createField
        procedure :: createWorkField
        procedure :: arrayToField
        procedure :: copyField
        procedure :: copyMembers
        procedure :: destroyField
        procedure :: destroyWorkField
        procedure :: destroyArrayToField

        procedure :: getInternalField
        procedure :: getBoundaryField

        procedure :: setInternalField
        procedure :: setBcType

        procedure :: setBcFlags
        procedure :: setBcFlagsNeumann0
        procedure :: setBoundaryValue
        procedure :: setBoundaryConditionJSON
        procedure :: updateBoundaryField => updateBoundaryScalarField
        procedure :: updateBoundaryVelocityField
        procedure :: updateBoundaryPressureField
        procedure :: updateBoundaryTurbulenceField
        procedure :: boundaryExtrapolation
        procedure :: extrapolateFromInterior
        procedure :: setUpField
        procedure :: setUpWorkField
        procedure :: setUpCoeff
        procedure :: setGradientType

        procedure :: updateGhosts
        procedure :: updateGhostsGradient

        procedure :: updateValues
        procedure :: updateOldFieldValues
        procedure :: componentToZero
        procedure :: bound0
        procedure :: boundwithNeighbours
        procedure :: getFieldNeighbours
        procedure :: relax
        procedure :: linearInterpolation
        procedure :: normal

        procedure :: writeToFile
        procedure :: writeToFileW
        procedure :: readFromFile
        procedure :: appendFieldToList

        procedure :: fieldVerbose

    end type flubioField

    ! Field registry
    type, public :: flubioFieldRegistry
        class(flubioField), pointer :: field
    end type flubioFieldRegistry

contains

    subroutine createField(field, fieldName, classType, nComp)

    !===================================================================================================================
    ! Description:
    !! createField is the class constructor.
    !===================================================================================================================

        class(flubioField) :: field
    !-------------------------------------------------------------------------------------------------------------------

        character(len=*) :: fieldName
        !! field name

        character(len=*) ::  classType
        !! type of field, need to get the correct update() function

        character(len=:), allocatable :: dummy_str
        !! dummy string

        character(len=30) :: fname(nComp)
        !! field name
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
        !! target element

        integer :: iComp
        !!  target component

        integer :: nComp
        !! number of field components
    !------------------------------------------------------------------------------------------------------------------

        field%nComp = nComp
        field%bcClassType = classType

        allocate(field%fieldName(nComp))

        ! Split name by comma
        dummy_str = fieldName
        do iComp=1,nComp
            call split(dummy_str, ',', fname(iComp))
            field%fieldName(iComp) = trim(fname(iComp))
        end do

        allocate(field%phi(numberOfElements+numberOfBfaces,nComp))
        allocate(field%phif(numberOfFaces,nComp))
        allocate(field%phio(numberOfElements+numberOfBfaces,nComp))
        allocate(field%phioo(numberOfElements+numberOfBfaces,nComp))
        allocate(field%phiGrad(numberOfElements+numberOfBfaces,3,nComp))

        allocate(field%bcIndex(numberOfBoundaries))
        allocate(field%bcFlag(numberOfBoundaries))
        allocate(field%bCond(numberOfBoundaries))
        allocate(field%boundaryValue(numberOfRealBound,nComp))

        field%phi = 0.0
        field%phif = 0.0
        field%phio = 0.0
        field%phioo = 0.0
        field%phiGrad = 0.0

        field%bcIndex = -1
        field%bcFlag = -1
        field%boundaryValue = 0.0

    end subroutine createField

! *********************************************************************************************************************

    subroutine copyField(field, fieldName, nComp, sourceField)

    !==================================================================================================================
    ! Description:
    !! copyField is the class copy-constructor and it copies from an existing field.
    !==================================================================================================================

        class(flubioField) :: field

        type(flubioField) :: sourceField
    !------------------------------------------------------------------------------------------------------------------

        character(*) :: fieldName
        !! field name

        character(len=:), allocatable :: dummy_str
        !! dummy string

        character(len=30) :: fname(nComp)
        !! field name
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !!  target component

        integer :: nComp
        !! number of field components
    !------------------------------------------------------------------------------------------------------------------

        field%nComp = nComp
        field%bcClassType =  sourceField%bcClassType

        allocate(field%fieldName(nComp))

        ! Split name by comma
        dummy_str = fieldName
        do iComp=1,nComp
            call split(dummy_str, ',', fname(iComp))
            field%fieldName(iComp) = trim(fname(iComp))
        end do

        allocate(field%phi(numberOfElements+numberOfBfaces,nComp))
        allocate(field%phif(numberOfFaces,nComp))
        allocate(field%phio(numberOfElements+numberOfBfaces,nComp))
        allocate(field%phioo(numberOfElements+numberOfBfaces,nComp))
        allocate(field%phiGrad(numberOfElements+numberOfBfaces,3,nComp))

        allocate(field%bcIndex(numberOfBoundaries))
        allocate(field%bcFlag(numberOfBoundaries))
        allocate(field%bCond(numberOfBoundaries))
        allocate(field%boundaryValue(numberOfRealBound,nComp))

        field%phi = sourceField%phi
        field%phif = sourceField%phif
        field%phio = sourceField%phio
        field%phioo = sourceField%phioo
        field%phiGrad = sourceField%phiGrad

        field%bcIndex = sourceField%bcIndex
        field%bcFlag = sourceField%bcFlag
        field%boundaryValue = sourceField%boundaryValue
        field%bCond = sourceField%bCond

    end subroutine copyField

! *********************************************************************************************************************

    subroutine copyMembers(field, sourceField)

    !==================================================================================================================
    ! Description:
    !! copyMembers copies the field values to another existing field.
    !==================================================================================================================

        class(flubioField) :: field

        type(flubioField) :: sourceField
    !------------------------------------------------------------------------------------------------------------------

        field%phi = sourceField%phi
        field%phif = sourceField%phif
        field%phio = sourceField%phio
        field%phioo = sourceField%phioo
        field%phiGrad = sourceField%phiGrad

        field%bcIndex = sourceField%bcIndex
        field%bcFlag = sourceField%bcFlag
        field%boundaryValue = sourceField%boundaryValue
        field%bCond = sourceField%bCond

    end subroutine copyMembers

! *********************************************************************************************************************

    subroutine createWorkField(field, fieldName, classType, nComp)

    !===================================================================================================================
    ! Description:
    !! createWorkField creates a field which has less members allocated and its usually used for temporary operations.
    !===================================================================================================================

        class(flubioField) :: field
    !-------------------------------------------------------------------------------------------------------------------

        character(len=*) :: fieldName
        !! field name

        character(len=*) ::  classType
        !! type of field, need to get the correct update() function

        character(len=:), allocatable :: dummy_str
        !! dummy string

        character(len=30) :: fname(nComp)
        !! field name
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
        !! target element

        integer :: iComp
        !!  target component

        integer :: nComp
        !! number of field components
    !------------------------------------------------------------------------------------------------------------------

        field%nComp = nComp
        field%bcClassType = classType

        allocate(field%fieldName(nComp))

        ! Split name by comma
        dummy_str = fieldName
        do iComp=1,nComp
            call split(dummy_str, ',', fname(iComp))
            field%fieldName(iComp) = trim(fname(iComp))
        end do

        allocate(field%phi(numberOfElements+numberOfBfaces,nComp))
        allocate(field%phif(numberOfFaces,nComp))
        allocate(field%phiGrad(numberOfElements+numberOfBfaces,3,nComp))

        allocate(field%bcIndex(numberOfBoundaries))
        allocate(field%bcFlag(numberOfBoundaries))
        allocate(field%bCond(numberOfBoundaries))

        field%phi = 0.0
        field%phif = 0.0
        field%phiGrad = 0.0

        field%bcIndex = -1
        field%bcFlag = -1

    end subroutine createWorkField

! *********************************************************************************************************************

    subroutine arrayToField(field, array, classType, nComp)

    !===================================================================================================================
    ! Description:
    !! arrayToField creates a field which starting with an array. All boundaries will be zero gradient.
    !===================================================================================================================

        class(flubioField) :: field
    !-------------------------------------------------------------------------------------------------------------------

        character(*) ::  classType
        !! type of field, need to get the correct update() function

        character(len=:), allocatable :: dummy_str
        !! dummy string
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !!  target component

        integer :: nComp
        !! number of field components
    !------------------------------------------------------------------------------------------------------------------

        real :: array(numberOfElements+numberOfBFaces, nComp)
    !------------------------------------------------------------------------------------------------------------------

        field%nComp = nComp
        field%bcClassType = classType

        allocate(field%phi(numberOfElements+numberOfBfaces,nComp))
        allocate(field%phif(numberOfFaces,nComp))

        allocate(field%phiGrad(numberOfElements+numberOfBfaces,3,nComp))

        allocate(field%bcIndex(numberOfBoundaries))
        allocate(field%bcFlag(numberOfBoundaries))
        allocate(field%bCond(numberOfBoundaries))

        field%phi = 0.0
        do iComp=1,nComp
            field%phi(:,iComp) = array(:,iComp)
        end do

        field%phif = 0.0
        field%phiGrad = 0.0

        field%bcIndex = -1
        field%bcFlag = -1

        ! Zero gradient everywhere
        call field%setBcFlagsNeumann0()
        call field%updateBoundaryField()

    end subroutine arrayToField

! *********************************************************************************************************************

    subroutine destroyField(field)

    !==================================================================================================================
    ! Description:
    !! destroyField is the field deconstructor. It can be used to delete a field from the momory.
    !==================================================================================================================

        class(flubioField) :: field
    !-------------------------------------------------------------------------------------------------------------------

        deallocate(field%fieldName)

        deallocate(field%phi)
        deallocate(field%phif)
        deallocate(field%phio)
        deallocate(field%phioo)

        deallocate(field%phiGrad)

        deallocate(field%bcIndex)
        deallocate(field%bcFlag)
        deallocate(field%bCond)
        deallocate(field%boundaryValue)

    end subroutine destroyField

! *********************************************************************************************************************

    subroutine destroyWorkField(field)

    !==================================================================================================================
    ! Description:
    !! destroyWorkField destroy a temporary work field.
    !==================================================================================================================

        class(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        deallocate(field%phi)
        deallocate(field%phiGrad)

        deallocate(field%bcIndex)
        deallocate(field%bCond)

    end subroutine destroyWorkField

! *********************************************************************************************************************

    subroutine destroyArrayToField(field)

    !==================================================================================================================
    ! Description:
    !! destroyArrayToField destroy a temporary work field.
    !==================================================================================================================

        class(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        deallocate(field%phi)
        deallocate(field%phif)
        deallocate(field%phiGrad)

        deallocate(field%bcIndex)
        deallocate(field%bCond)

    end subroutine destroyArrayToField

! *********************************************************************************************************************

    function getInternalField(field) result(internalField)

    !==================================================================================================================
    ! Description:
    !! getInternalField return the internal field only.
    !==================================================================================================================

        class(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        real :: internalField(numberOfElements, field%nComp)
    !------------------------------------------------------------------------------------------------------------------

        internalField = field%phi(1:numberOfElements, 1:field%nComp)

    end function getInternalField

! *********************************************************************************************************************

    function getBoundaryField(field, boundaryName) result(boundaryField)

    !==================================================================================================================
    ! Description:
    !! getBoundaryField returns a field at a target boundary
    !==================================================================================================================

        class(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) boundaryName
    !------------------------------------------------------------------------------------------------------------------

        integer :: is, ie, nFaces, startFace, endFace

        integer :: targetBoundary(3)
    !------------------------------------------------------------------------------------------------------------------

        real, dimension(:,:), allocatable :: boundaryField
    !------------------------------------------------------------------------------------------------------------------

        targetBoundary = mesh%boundaries%findBoundaryByName(boundaryName)
        is = targetBoundary(1)
        ie = targetBoundary(2)
        nFaces = targetBoundary(3)

        allocate(boundaryField(nFaces, field%nComp))
        boundaryField = 0.0

        startFace = numberOfElements + (is-numberOfIntFaces)
        endFace = numberOfElements + (ie-numberOfIntFaces)
        boundaryField(1:nFaces,:) = field%phi(startFace:endFace, :)

    end function getBoundaryField

! *********************************************************************************************************************

    subroutine updateGhosts(this)

    !==================================================================================================================
    ! Description:
    !! updateGhosts updates the ghost values at a each processor boundary.
    !! If you have updated a field, this subroutine MUST be gradientcalled, otherwise ghosts points will not be updated.
    !! Some important operations such as interpolation and  compuation call this subroutine automatically.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: ierr
        !! error flag

        integer :: stat(MPI_STATUS_SIZE)
        !! mpi status

        integer :: ireq(2*numberOfProcBound1)
        !! requests array
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, j, iBoundary, iElement, iFace, iOwner, is, ie, patchFace

        integer :: nbuf

        integer :: nFace
        !! number of faces

        integer :: fComp
        !! number of field components
    !------------------------------------------------------------------------------------------------------------------

        integer :: iSend
        !! target sent message

        integer :: iRecv
        !! target received message

        integer :: istat(MPI_STATUS_SIZE)
        !! target request status

        integer :: nSend
        !! number of sent messages

        integer :: nRecv
        !! inumber of received messages

        integer :: idr
        !! receiving process

        integer :: buffSize
        !! buffer size
    !------------------------------------------------------------------------------------------------------------------

        real, dimension(:,:,:), allocatable :: sbuf
        !! sent buffer

        real, dimension(:,:,:), allocatable :: rbuf
        !! received buffer
    !------------------------------------------------------------------------------------------------------------------

        ! ---------------------------------------------------------------------------!
        ! Send and Receive the cell center field value from the neighbour processors !
        ! ---------------------------------------------------------------------------!

        fComp = this%nComp

        ! Prepare the send buffers
        buffSize = maxProcFaces

        allocate(sbuf(buffSize,fComp,numberOfProcBound1))
        allocate(rbuf(buffSize,fComp,numberOfProcBound1))

        sbuf = 0.0
        rbuf = 0.0

        do iBoundary=1,numberOfProcBound

            i = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(i)
            ie = is+mesh%boundaries%nFace(i)-1

            ! Fill the send buffer
            nbuf = 0
            do iFace=is,ie
                nbuf = nbuf+1
                iOwner = mesh%owner(iFace)
                sbuf(nbuf,:,iBoundary) = this%phi(iOwner,:)
            enddo

        enddo

        !-------------------------------------------------!
        ! Perform the comunication between the processors !
        !-------------------------------------------------!

        nSend = numberOfProcBound
        nRecv = numberOfProcBound

        ! Open senders
        do iSend=1,nSend
            i = mesh%boundaries%procBound(iSend)
            idr = mesh%boundaries%neighProc(i)
            buffSize = maxProcFaces*fComp
            call mpi_isend(sbuf(1:maxProcFaces,:,iSend),buffSize,MPI_REAL8,idr,0,MPI_COMM_WORLD,ireq(iSend),ierr)
        enddo

        ! Open receivers
        do iRecv=1,nRecv
            i = mesh%boundaries%procBound(iRecv)
            idr = mesh%boundaries%neighProc(i)
            buffSize = maxProcFaces*fComp
            call mpi_irecv(rbuf(1:maxProcFaces,:,iRecv),buffSize,MPI_REAL8,idr,0,MPI_COMM_WORLD,ireq(nSend+iRecv),ierr)
        enddo

        ! Wait the end of the communications
        call mpi_waitAll(nSend+nRecv,ireq,MPI_STATUSES_IGNORE,ierr)

        ! Fill the space at the end of the centroid vector
        do iBoundary=1,numberOfProcBound

            i = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(i)
            ie = is+mesh%boundaries%nFace(i)-1
            patchFace = numberOfElements + (is-numberOfIntFaces-1)

            ! Fill the send buffer
            nbuf = 0
            do iFace=is,ie
                nbuf = nbuf+1
                this%phi(patchFace+nbuf,:) = rbuf(nbuf,:,iBoundary)
            enddo

        enddo

        deallocate(sbuf)
        deallocate(rbuf)

    end subroutine updateGhosts

! *********************************************************************************************************************

    subroutine updateGhostsGradient(this)

    !==================================================================================================================
    ! Description:
    !! updateGhostsGradient updates the gradient at ghost cells.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !! target field component

        integer :: fComp
        !! number of field component

        integer :: sComp
        !! target gradient component
    !------------------------------------------------------------------------------------------------------------------

        fComp = this%nComp

        ! Loop over variable components

        do iComp=1,fComp

        ! Update iComp of the gradient
            do sComp=1,3
                call mpi_update_ghosts_I(this%phiGrad(:,sComp,iComp),1)
            enddo

        end do

    end subroutine updateGhostsGradient

!**********************************************************************************************************************

    subroutine mpi_update_ghosts_I(phi, fComp)

    !==================================================================================================================
    ! Description:
    !! mpi_update_ghosts_I updates the ghost cells. this function is retained since sometime it can be handy, if you are not using
    !! a field class.
    !==================================================================================================================

        integer :: ierr
        !! error flag

        integer :: stat(MPI_STATUS_SIZE)
        !! mpi status

        integer :: ireq(2*numberOfProcBound1)
        !! requests array
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, j, iBoundary, iElement, iFace, iOwner, is, ie, patchFace

        integer :: nbuf

        integer :: nFace
        !! number of faces

        integer :: fComp
        !! number of field components
    !------------------------------------------------------------------------------------------------------------------

        integer :: iSend
        !! target sent message

        integer :: iRecv
        !! target received message

        integer :: istat(MPI_STATUS_SIZE)
        !! target request status

        integer :: nSend
        !! number of sent messages

        integer :: nRecv
        !! inumber of received messages

        integer :: idr
        !! receiving process

        integer :: buffSize
        !! buffer size
    !------------------------------------------------------------------------------------------------------------------

        real, dimension(:,:,:), allocatable :: sbuf
        !! sent buffer

        real, dimension(:,:,:), allocatable :: rbuf
        !! received buffer

        real :: phi(numberOfElements+numberOfBFaces,fComp)
        !! cell centred values to exchange
    !------------------------------------------------------------------------------------------------------------------

        !--------------------------!
        ! Prepare the send buffers !
        !--------------------------!

        buffSize = maxProcFaces
        allocate(sbuf(buffSize,fComp,numberOfProcBound1))
        allocate(rbuf(buffSize,fComp,numberOfProcBound1))

        sbuf = 0.0
        rbuf = 0.0

        do iBoundary=1, numberOfProcBound

            i = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(i)
            ie = is+mesh%boundaries%nFace(i)-1

            ! Fill the send buffer

            nbuf = 0

            do iFace=is,ie
                nbuf = nbuf+1
                iOwner = mesh%owner(iFace)
                sbuf(nbuf,:,iBoundary) = phi(iOwner,:)
            enddo
        enddo

        !-------------------------------------------------!
        ! Perform the comunication between the processors !
        !-------------------------------------------------!

        nSend = numberOfProcBound
        nRecv = numberOfProcBound

        ! Open senders
        do iSend=1,nSend
            i = mesh%boundaries%procBound(iSend)
            idr = mesh%boundaries%neighProc(i)
            buffSize = maxProcFaces*fComp
            call mpi_isend(sbuf(1:maxProcFaces,:,iSend), buffSize, MPI_REAL8, idr, 0, MPI_COMM_WORLD, ireq(iSend), ierr)
        enddo

        ! Open receivers
        do iRecv=1,nRecv
            i = mesh%boundaries%procBound(iRecv)
            idr = mesh%boundaries%neighProc(i)
            buffSize = maxProcFaces*fComp
            call mpi_irecv(rbuf(1:maxProcFaces,:,iRecv),buffSize,MPI_REAL8,idr,0,MPI_COMM_WORLD,ireq(nSend+iRecv),ierr)
        enddo

        ! Wait the end of the communications
        call mpi_waitAll(nSend+nRecv,ireq,MPI_STATUSES_IGNORE,ierr)

        ! Fill the space at the end of the centroid vector
        do iBoundary=1,numberOfProcBound

            i = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(i)
            ie = is+mesh%boundaries%nFace(i)-1
            patchFace = numberOfElements + (is-numberOfIntFaces-1)

            ! Fill the send buffer
            nbuf = 0
            do iFace=is,ie
                nbuf = nbuf+1
                phi(patchFace+nbuf,:) = rbuf(nbuf,:,iBoundary)
            enddo

        enddo

        ! Wait all the processor before advancing. May be it useless, just to be sure :)
        !call mpi_barrier(MPI_COMM_WORLD, ierr)

        deallocate(sbuf)
        deallocate(rbuf)

    end subroutine mpi_update_ghosts_I

!**********************************************************************************************************************

    subroutine setInternalField(this)

    !==================================================================================================================
    ! Description:
    !! setInternalField sets the initial internal values of a target field.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len = 10), dimension(3) :: variables
        !! variables in string

        character(len = 275), dimension(:), allocatable  :: expr
        !! expression passed to the interpreter

        character(len=:), allocatable :: func
        !! expression parsed from the boundary condition file

    !------------------------------------------------------------------------------------------------------------------

        integer :: i
        !! loop index

        integer:: iComp
        !! taget oomponents

        integer :: iElement
        !! target cell

        integer :: fComp
        !! number of field components
    !------------------------------------------------------------------------------------------------------------------

        real :: variablesValues(3)
        !! name of the variables to be interpreted

        real :: val
        !! auxiliary number

        real :: getFieldVal
        !! function to get the interpreted fiedl value
    !------------------------------------------------------------------------------------------------------------------

        fComp = this%nComp

        variables(1) = 'x'
        variables(2) = 'y'
        variables(3) = 'z'

       call this%bcDict%get('internalField', func)
       call split_in_array(func, expr, ' ')

    ! Fill the domain
        do iComp=1,fComp

            do iElement=1,numberOfElements

                variablesValues(1) = mesh%centroid(1,iElement) ! x
                variablesValues(2) = mesh%centroid(2,iElement) ! y
                variablesValues(3) = mesh%centroid(3,iElement) ! z

                val = getFieldValue(expr(iComp), variables, variablesValues)
                this%phi(iElement,iComp) = val

            end do

        enddo

    end subroutine setInternalField

!**********************************************************************************************************************

    subroutine setBcType(this)

    !==================================================================================================================
    ! Description:
    !! setBcType sets the boundary condition type.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: bcName, bcType
        !! name of the boundaries
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary
        !! boundary loop index

        integer :: targetBoundary
        !! target boundary

        integer :: dummy
        !! dummy integer

        integer :: bcIndex(numberOfRealBound)
        !! boundary index
    !------------------------------------------------------------------------------------------------------------------

        ! Set boundary type vector
        do iBoundary=1,numberOfRealBound
            bcName=trim(mesh%boundaries%userName(iBoundary))
            call this%bcDict%get(bcName//'%type', bcType )
            this%bCond(iBoundary) = bcType
        enddo

    end subroutine setBcType

! *********************************************************************************************************************

    subroutine setBcFlags(this)

    !==================================================================================================================
    ! Description:
    !! setBcFlags sets the boundary condition flags as defined in BcDicts.f03
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: message
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBFace
        !! target boundary face

        integer :: iBoundary
        !! boundary loop index

        integer :: iB
        !! target boundary address

        integer :: bCflag
        !! boundary condition flag
    !------------------------------------------------------------------------------------------------------------------

        logical :: hasKey
        !! if the key is not in the dictionary, it is false
    !------------------------------------------------------------------------------------------------------------------

        this%bcFlag = -1

        do iBoundary=1,numberOfRealBound

            ! Using json file, there is no need to map the bounadry index between the mesh one and the boundary condition file
            iB = iBoundary

            if(trim(this%bcClassType)=='momentum') then
                call momentumBC%dict%get(trim(this%bCond(iB)), bCflag, hasKey)

                if(.not. hasKey) then
                    message = 'ERROR: field "' // trim(this%fieldName(1)) // '" - unknown boundary condition: "' // trim(this%bCond(iB)) // '" at boundary "' // trim(mesh%boundaries%userName(iBoundary)) //'"'
                    call flubioStopMsg(message)
                end if

            elseif(trim(this%bcClassType)=='pressure') then
                call pressureBC%dict%get(trim(this%bCond(iB)), bCflag, hasKey)

                if(.not. hasKey) then
                    message = 'ERROR: field "' // trim(this%fieldName(1)) // '" - unknown boundary condition: "' // trim(this%bCond(iB)) // '" at boundary "' // trim(mesh%boundaries%userName(iBoundary)) //'"'
                    call flubioStopMsg(message)
                end if

            elseif(trim(this%bcClassType)=='turbulence') then
                call turbulentBC%dict%get(trim(this%bCond(iB)), bCflag, hasKey)

                if(.not. hasKey) then
                    message = 'ERROR: field "' // trim(this%fieldName(1)) // '" - unknown boundary condition: "' // trim(this%bCond(iB)) // '" at boundary "' // trim(mesh%boundaries%userName(iBoundary)) //'"'
                    call flubioStopMsg(message)
                end if

            else
                call scalarBC%dict%get(trim(this%bCond(iB)), bCflag, hasKey)

                if(.not. hasKey) then
                    message = 'ERROR: field "' // trim(this%fieldName(1)) // '" - unknown boundary condition: "' // trim(this%bCond(iB)) // '" at boundary "' // trim(mesh%boundaries%userName(iBoundary)) //'"'
                    call flubioStopMsg(message)
                end if

            end if

            this%bCFlag(iBoundary) = bCflag

        end do

    end subroutine setBcFlags

! *********************************************************************************************************************

    subroutine setBcFlagsNeumann0(this)

    !==================================================================================================================
    ! Description:
    !! setBcFlagsNeumann0 sets all the flags equal to 2, which corresponds to zero gradient.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary
        !! boundary loop index
    !------------------------------------------------------------------------------------------------------------------

        this%bcFlag = -1

        do iBoundary=1,mesh%boundaries%numberOfRealBound
            this%bCFlag(iBoundary) = 2
        end do

    end subroutine setBcFlagsNeumann0

! *********************************************************************************************************************

    subroutine updateBoundaryScalarField(this)

    !==================================================================================================================
    ! Description:
    !! updateBoundaryScalarField updates the target field at all the real boundaries.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=10) :: bCFlagStr
        !! boundary flag as string

        character(len=:), allocatable :: message
        !! output message
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iBFace, iOwner, patchFace, iComp, is, ie, bFlag
    !------------------------------------------------------------------------------------------------------------------

        real :: SMALL
        !! small number

        real :: phi0(this%nComp)
        !!  boundary value to assign
    !------------------------------------------------------------------------------------------------------------------

        SMALL = 1.0e-10

        patchFace = numberOfElements

        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
            bFlag = this%bCFlag(iBoundary)

            ! Void
            if(this%bCFlag(iBoundary)==0) then

                call updatevoidBoundaryField(this%phi, iBoundary, this%nComp)

            ! Fixed value
            elseif(this%bCFlag(iBoundary)==1) then

                phi0 = this%boundaryValue(iBoundary, 1:this%nComp)
                call updateDirichletBoundaryField(this%phi, phi0, iBoundary, this%nComp)

            ! Zero gradient
            elseif(this%bCFlag(iBoundary)==2) then

                call updateneumann0BoundaryField(this%phi, iBoundary, this%nComp)

            ! Fixed Gradient
            elseif(this%bCFlag(iBoundary)==3) then

                call updateneumannBoundaryField(this%phi, this%boundaryValue(iBoundary,:), iBoundary, this%nComp)

            ! Fixed Flux
            elseif(this%bCFlag(iBoundary)==4) then

                call updateprescribedFluxBoundaryField(this%phi, this%boundaryValue(iBoundary,:), iBoundary, this%nComp)

            ! Robin, mixed BC
            elseif(this%bCFlag(iBoundary)==5) then

                ! call updateRobinBoundaryField(this%phi, this%phiGrad, K, iBoundary, this%nComp)

            ! farField
            elseif(this%bCFlag(iBoundary)==6) then

                phi0 = this%boundaryValue(iBoundary,:)
                call updatefarFieldBoundaryField(this%phi, phi0, iBoundary, this%nComp)

            ! User defined dirichlet
            elseif(this%bCFlag(iBoundary)==1000) then

                call updateUserDefinedBoundaryField(this%phi, iBoundary, this%boundaryExpression(iBoundary,:), 1)

            ! Periodic, override check
            elseif(mesh%boundaries%bcType(iBoundary)== 'cyclic') then

                ! do nothing, it will be treated as a processor boundary

            else

                write(bCFlagStr,'(i0)') this%bCFlag(iBoundary)
                message = 'FLUBIO - updateBoundaryScalarField: Boundary condition number ' //trim(bCFlagStr)// ' not found'
                call flubioStopMsg(message)

            end if

        enddo

    end subroutine updateBoundaryScalarField

! *********************************************************************************************************************

    subroutine setBoundaryValue(this)

    !==================================================================================================================
    ! Description:
    !! setBoundaryValue sets the field's value as prescribed from the boundary conditions.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len = 275), dimension(:), allocatable  :: expr

        character(len=:), allocatable :: func
        !! expression parsed from the boundary condition file
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary
        !! loop boudanry index

        integer :: iB
        !! target boundary address

        integer :: iComp
        !! target component
    !------------------------------------------------------------------------------------------------------------------

        real, dimension(:), allocatable :: phi0
        !! boundary values to be assigned
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        ! Do the same with a possible boundary inline expression
        allocate(this%boundaryExpression(numberOfRealBound,this%nComp))

        do iBoundary=1,numberOfRealBound

            if(this%bcFlag(iBoundary)==1000 .or. this%bcFlag(iBoundary)==1001) then

                this%boundaryValue(iBoundary,1:this%nComp) = 0.0

                call this%bcDict%get(trim(mesh%boundaries%userName(iBoundary)) //'%value', func, found)

                call split_in_array(func, expr, ' ')

                ! Put here the logic to parse the boundary expression
                this%boundaryExpression(iBoundary, :) = expr

            else
                call this%bcDict%get(trim(mesh%boundaries%userName(iBoundary)) //'%value', phi0)
                this%boundaryValue(iBoundary,1:this%nComp) = phi0
            end if

        end do

    end subroutine setBoundaryValue

! *********************************************************************************************************************

    subroutine setBoundaryConditionJSON(this)

    !==================================================================================================================
    ! Description:
    !! setBoundaryConditionJSON reads the boundary condtion json file.
    !==================================================================================================================

        use, intrinsic :: iso_fortran_env , only: error_unit

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: error_cnt
    !------------------------------------------------------------------------------------------------------------------

        ! Initialise the JSON dictionary
        error_cnt = 0
        call this%bcDict%initialize()
        if (this%bcDict%failed()) then
            call this%bcDict%print_error_message(error_unit)
            error_cnt = error_cnt + 1
        end if

        ! Load JSON bc dictionary
        call this%bcDict%load(filename = 'BCs/'//trim(this%fieldName(1))//'bc')

        !if there was an error reading the file
        if (this%bcDict%failed()) then
            call this%bcDict%print_error_message(error_unit)
            error_cnt = error_cnt + 1
        endif

        ! use fortran-style paths
        call this%bcDict%initialize(path_mode=1,path_separator=json_CK_'%')

    end subroutine setBoundaryConditionJSON

! *********************************************************************************************************************

    subroutine extrapolateFromInterior(this)

    !==================================================================================================================
    ! Description:
    !! boundaryExtrapolation extrapolates the value at boundaries from interior using first or second order interpolation.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iBFace, iOwner

        integer :: is ,ie, iComp, patchFace
    !------------------------------------------------------------------------------------------------------------------

        real :: dot
        !! auxiliary value to store  a scalar product

        real:: order
        !! extrapolation order
    !------------------------------------------------------------------------------------------------------------------

        is=0
        ie=0
        patchFace = numberOfElements

        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie

                patchFace = patchFace+1
                iOwner = mesh%owner(iBFace)

                do iComp=1,this%nComp
                    dot=dot_product(this%phiGrad(iOwner,:,iComp),mesh%CN(iBFace,:))
                    this%phi(patchFace, iComp) = this%phi(iOwner, iComp) + 0.0*dot
                end do

            enddo

        enddo

    end subroutine extrapolateFromInterior

! *********************************************************************************************************************

    subroutine boundaryExtrapolation(this)

    !==================================================================================================================
    ! Description:
    !! boundaryExtrapolation extrapolates the value at boundaries from interior using first or second order interpolation.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iBFace, iOwner

        integer :: is ,ie, iComp, patchFace
    !------------------------------------------------------------------------------------------------------------------

        real :: dot
        !! auxiliary value to store  a scalar product

        real:: order
        !! extrapolation order

    !------------------------------------------------------------------------------------------------------------------

        is=0
        ie=0
        patchFace = numberOfElements

        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie

                patchFace = patchFace+1
                iOwner = mesh%owner(iBFace)

                if(this%bcFlag(iBoundary)/=1) then
                    do iComp=1,this%nComp
                        dot=dot_product(this%phiGrad(iOwner,:,iComp),mesh%CN(iBFace,:))
                        this%phi(patchFace, iComp) = this%phi(iOwner, iComp) + 0.0*dot
                    end do
                endif

            enddo

        enddo

    end subroutine boundaryExtrapolation

! *********************************************************************************************************************

    subroutine setUpField(this, fieldName, classType, nComp, optArg)

    !==================================================================================================================
    ! Description:
    !! setUpField sets up a field, calling some mandatory produres defined in this class.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: fieldName
        !! field name

        character(len=*) ::  classType
        !! type of field, need to get the correct update() function
    !------------------------------------------------------------------------------------------------------------------

        integer :: nComp
        !! number of components
    !------------------------------------------------------------------------------------------------------------------

        real, optional :: optArg
    !------------------------------------------------------------------------------------------------------------------

        call this%createField(fieldName, classType, nComp)
        call this%setBoundaryConditionJSON()
        call this%setInternalField()
        call this%setBcType()
        call this%setBcFlags()
        call this%setBoundaryValue()
        call this%setGradientType()

        if(classType=='momentum') then
            call this%updateBoundaryVelocityField()
        elseif(classType=='pressure') then
            call this%updateBoundaryPressureField()
        elseif(classType=='turbulence') then
            call this%updateBoundaryTurbulenceField(optArg)
        else
            call this%updateBoundaryField()
        end if

        call this%updateGhosts()
        call this%appendFieldToList()

    end subroutine setUpField

! *********************************************************************************************************************

    subroutine setUpWorkField(this, fieldName, classType, nComp, optArg)

    !==================================================================================================================
    ! Description:
    !! setUpWorkField sets up a work field. The field is set up with zero gradient boundary conditions by defaults.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: fieldName
        !! field name

        character(len=*) ::  classType
        !! type of field, need to get the correct update() function
    !------------------------------------------------------------------------------------------------------------------

        integer :: nComp
        !! number of components
    !------------------------------------------------------------------------------------------------------------------

        real, optional :: optArg
    !------------------------------------------------------------------------------------------------------------------

        call this%createWorkField(fieldName, classType, nComp)
        call this%setBcFlagsNeumann0()
        call this%updateBoundaryField()
        call this%updateGhosts()

        if(classType=='momentum') then
            call this%updateBoundaryVelocityField()
        elseif(classType=='pressure') then
            call this%updateBoundaryPressureField()
        elseif(classType=='turbulence') then
            call this%updateBoundaryTurbulenceField(optArg)
        else
            call this%updateBoundaryField()
        end if

        call this%updateGhosts()

    end subroutine setUpWorkField

! *********************************************************************************************************************

    subroutine setUpCoeff(this, fieldName, appendToList, defaultValue)

    !==================================================================================================================
    ! Description:
    !! setUpCoeff sets up a coefficient field
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: fieldName
        !! field name
    !------------------------------------------------------------------------------------------------------------------

        real, optional :: defaultValue
        !! If present, assign this value to the coefficient
    !------------------------------------------------------------------------------------------------------------------

        logical :: appendToList
        !! append to list flag
    !------------------------------------------------------------------------------------------------------------------

        call this%createField(fieldName, classType='scalar', nComp=1)
        call this%setBcFlagsNeumann0()
        call this%updateBoundaryField()
        call this%updateGhosts()
        call this%setGradientType()

        if(appendToList) call this%appendFieldToList()

        if(present(defaultValue)) then
            this%phi(:,1) = defaultValue
            this%phio(:,1) = defaultValue
            this%phioo(:,1) = defaultValue
            call this%updateGhosts()
        endif

    end subroutine setUpCoeff

! *********************************************************************************************************************

    subroutine updateValues(this, values)

    !==================================================================================================================
    ! Description:
    !! updateOldFieldValues stores the old values of the field.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
    !------------------------------------------------------------------------------------------------------------------

        real :: values(numberOfElements+numberOfBFaces, this%nComp)
    !------------------------------------------------------------------------------------------------------------------

        do iComp=1,this%nComp
            this%phi(:,iComp) = values(:, iComp)
        enddo

        call this%updateGhosts()

    end subroutine updateValues

! *********************************************************************************************************************

    subroutine updateOldFieldValues(this)

    !==================================================================================================================
    ! Description:
    !! updateOldFieldValues stores the old values of the field.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        ! Set uoo needed by SOUE and CN time scheme
        this%phioo = this%phio
        this%phio = this%phi

    end subroutine updateOldFieldValues

! *********************************************************************************************************************

    subroutine minMaxField(field, minField, maxField, minCellCenter, maxCellCenter)

    !==================================================================================================================
    ! Description:
    !! minMaxField computes the maxium and the minimum value of a field and scatter it to the master only.
    !==================================================================================================================

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !! target component

        integer :: ierr
        !! error flag

        integer :: tmp
        !! working integer

        integer :: isbuf(nid,1)
        !! sent buffer

        integer :: irbuf(nid,1)
        !! receive buffer

        integer, dimension(:,:), allocatable :: minFieldLoc
        !! min field value location

        integer, dimension(:,:), allocatable :: maxFieldLoc
        !! max field value location
    !------------------------------------------------------------------------------------------------------------------

        real :: sbuf(nid,1)
        !! sent buffer

        real :: rbuf(nid,1)
        !! receive buffer

        real :: csbuf(nid,3)
        !! sent buffer for cell center

        real :: crbuf(nid,3)
        !! receive buffer for cell center

        real, dimension(:), allocatable :: minField
        !! min field value

        real, dimension(:), allocatable :: maxField
        !! max field value

        real, dimension(:,:), allocatable :: minCellCenter
        !! cell center of the min cell center

        real, dimension(:,:), allocatable  :: maxCellCenter
        !! cell center of the max cell center
    !------------------------------------------------------------------------------------------------------------------

        ! max value
        allocate(minField(field%nComp))
        allocate(maxField(field%nComp))

        sbuf = 0.0
        rbuf = 0.0
        minField = 0.0
        maxField = 0.0

        allocate(minFieldLoc(field%nComp,2))
        allocate(maxFieldLoc(field%nComp,2))

        isbuf = 0
        irbuf = 0
        minFieldLoc = -1
        maxFieldLoc = -1

        allocate(minCellCenter(field%nComp,3))
        allocate(maxCellCenter(field%nComp,3))
        csbuf = 0.0
        crbuf = 0.0
        minCellCenter = 0.0
        maxCellCenter = 0.0

        ! Fill the buffer array
        do iComp=1,field%nComp

            ! Send and Receive the buffer
            sbuf(id+1,1) = maxval(field%phi(1:numberOfElements,icomp))
            call mpi_Reduce(sbuf, rbuf, nid, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
            maxField(iComp) = maxval(rbuf(:,1))

            ! Find out the max location (processor and cell)
            isbuf(id+1,1) = maxloc(field%phi(1:numberOfElements,icomp), dim=1)
            call mpi_Reduce(isbuf, irbuf, nid, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD, ierr)

            tmp = maxloc(rbuf(:,1), dim=1)
            maxFieldLoc(iComp,1) = irbuf(tmp,1)
            maxFieldLoc(iComp,2) = tmp

            ! Find out cell centers
            csbuf(id+1,1:3) = mesh%centroid(:,isbuf(id+1,1))
            call mpi_Reduce(csbuf, crbuf, nid*3, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
            maxCellCenter(iComp,1:3) = crbuf(tmp,1:3)
        end do


        ! min value
        sbuf = 0.0
        rbuf = 0.0
        isbuf = 0
        irbuf = 0
        csbuf = 0.0
        crbuf = 0.0

        ! Fill the buffer array
        do iComp=1,field%nComp

            ! Send and Receive the buffer
            sbuf(id+1,1) = minval(field%phi(1:numberOfElements,iComp))
            call mpi_Reduce(sbuf, rbuf, nid, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
            minField(iComp) = minval(rbuf(:,1))

            ! Send and Receive the buffer
            isbuf(id+1,1) = minloc(field%phi(1:numberOfElements,iComp), dim=1)
            call mpi_Reduce(isbuf, irbuf, nid, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD, ierr)

            tmp = minloc(rbuf(:,1), dim=1)
            minFieldLoc(iComp,1) = irbuf(tmp,1)
            minFieldLoc(iComp,2) = tmp

            ! Find out cell centers
            csbuf = 0.0
            csbuf(id+1,1:3) = mesh%centroid(:,isbuf(id+1,1))
            call mpi_Reduce(csbuf, crbuf, nid*3, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
            minCellCenter(iComp,1:3) = crbuf(tmp, 1:3)

        end do

    end subroutine minMaxField

! *********************************************************************************************************************

    subroutine minMaxFieldAll(field, minField, maxField)

    !==================================================================================================================
    ! Description:
    !! minMaxFieldAll computes the maxium and the minimum value of a field and scatter it to all procs.
    !==================================================================================================================

        class(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !! target component

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        real :: sbuf(nid,1)
        !! sent buffer

        real ::  rbuf(nid,1)
        !! receive buffer

        real, dimension(:), allocatable :: minField
        !! min field value

        real, dimension(:), allocatable :: maxField
        !! max field value
    !------------------------------------------------------------------------------------------------------------------

        ! max value
        allocate(minField(field%nComp))
        allocate(maxField(field%nComp))

        sbuf = 0.0
        rbuf = 0.0
        minField = 0.0
        maxField = 0.0

        ! Fill the buffer array
        do iComp=1,field%nComp

            ! Send and Receive the buffer
            sbuf(id+1,1) = maxval(field%phi(1:numberOfElements,icomp))
            call mpi_Allreduce(sbuf, rbuf, nid, MPI_REAL8, MPI_SUM, MPI_COMM_WORLD, ierr)
            maxField(iComp)=maxval(rbuf(:,1))

        end do

        ! min value
        sbuf = 0.0
        rbuf = 0.0


        ! Fill the buffer array
        do iComp=1,field%nComp

            ! Send and Receive the buffer
            sbuf(id+1,1)=minval(field%phi(1:numberOfElements,iComp))
            call mpi_Allreduce(sbuf, rbuf, nid, MPI_REAL8, MPI_SUM, MPI_COMM_WORLD, ierr)
            minField(iComp)=minval(rbuf(:,1))

        end do

    end subroutine minMaxFieldAll

! *********************************************************************************************************************

    subroutine minMaxArr(array, minField, maxField, asize, nComp)

    !==================================================================================================================
    ! Description:
    !! minMaxField computes the maxium and the minimum value of an array and scatter it to the master only.
    !==================================================================================================================

        integer :: asize
        !! size of the array

        integer :: iComp
        !! target component

        integer :: nComp
        !! number of components

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        real :: sbuf(nid,1)
        !! sent buffer

        real ::  rbuf(nid,1)
        !! receive buffer

        real, dimension(:), allocatable :: minField
        !! min field value

        real, dimension(:), allocatable :: maxField
        !! max field value

        real :: array(asize, nComp)
        !! input array
    !------------------------------------------------------------------------------------------------------------------

        ! max value
        allocate(minField(nComp))
        allocate(maxField(nComp))

        sbuf = 0.0
        rbuf = 0.0
        minField = 0.0
        maxField = 0.0

        ! Fill the buffer array
        do iComp=1,nComp

            sbuf(id+1,1) = maxval(array(1:asize, icomp))

        ! Send and Receive the buffer
            call mpi_Reduce(sbuf, rbuf, nid, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
            maxField(iComp) = maxval(rbuf(:,1))

        end do

        ! Min value
        sbuf = 0.0
        rbuf = 0.0

        do iComp=1,nComp

            ! Fill the buffer array
            sbuf(id+1,1) = minval(array(1:asize,iComp))

            ! Send and Receive the buffer
            call mpi_Reduce(sbuf, rbuf, nid, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
            minField(iComp) = minval(rbuf(:,1))

        end do

    end subroutine minMaxArr

! *********************************************************************************************************************

    subroutine meanField(field, fieldMean)

    !==================================================================================================================
    ! Description:
    !! meanField computes the mean value of a field.
    !==================================================================================================================

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
        !! target element

        integer :: iComp
        !! target component

        integer :: ierr
        !! error flag
   !------------------------------------------------------------------------------------------------------------------

        real :: sbuf
        !! sent buffer

        real ::  rbuf
        !! receive buffer

        real :: localMeanField(field%nComp), fieldMean(field%nComp)
        !! mean field value

        real :: localVol, vol
        !! domain volume
    !------------------------------------------------------------------------------------------------------------------

        localVol = 0.0
        vol = 0.0

        ! Get the total volume
        do iElement=1,numberOfElements
            localVol = localVol + mesh%volume(iElement)
        end do

        call mpi_allReduce(localVol, vol, 1, MPI_REAL8, MPI_SUM, MPI_COMM_WORLD, ierr)

        ! Compute the local means

        localMeanField = 0.0
        fieldMean = 0.0

        do iComp=1,field%nComp

            sbuf = 0.0
            rbuf = 0.0

            do iElement=1,numberOfElements
                localMeanField(iComp) =  localMeanField(iComp) + field%phi(iElement,iComp)*mesh%volume(iElement)
            end do

            sbuf = localMeanField(iComp)
            call mpi_allReduce(sbuf, rbuf, 1, MPI_REAL8, MPI_SUM, MPI_COMM_WORLD, ierr)
            fieldMean(iComp) = rbuf/vol

        end do

    end subroutine meanField

! *********************************************************************************************************************

    subroutine bound0(this, lowerBound)

    !==================================================================================================================
    ! Description:
    !! bound0 bounds a field and force it to be grater then zero.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: message
        !! message to display

        character(len=25) :: string
        !! dummy string
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !! trget component

        integer :: iElement
        !! target cell

        integer :: ilen
        !! string length

        integer :: countNegs
        !! number of bounded cells

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        real :: lowerBound
        !! lower bound
    !------------------------------------------------------------------------------------------------------------------

        do iComp=1,this%nComp
            countNegs = 0
            do iElement=1,numberOfElements+numberOfBFaces
                if(this%phi(iElement,iComp) < 0.0 .and. this%phi(iElement,iComp) < lowerBound) then
                    countNegs = countNegs + 1
                    this%phi(iElement,iComp) = 0.0
                elseif(this%phi(iElement,iComp) > 0.0 .and. this%phi(iElement,iComp) < lowerBound) then
                    this%phi(iElement,iComp) = lowerBound
                endif
            end do

            ! Sum up negs
            call mpi_AllReduce(countNegs, countNegs, 1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, ierr)

            if(countNegs > 0) then
                call value_to_string(countNegs, string, ilen)
                message = trim(this%fieldName(iComp))// ' bounded in '//trim(string)//' cells'
                call flubioMsg(message)
            endif

        end do

    end subroutine bound0

! *********************************************************************************************************************

    subroutine boundWithNeighbours(this, lowerBound)

    !==================================================================================================================
    ! Description: boundWithNeighbours bounds a field and force it to be grater then a speicifed threshold.
    !! It tries to bound the field doing the average among the cell neighbours before clipping brutally.
    !==================================================================================================================

        class(flubioField) :: this

        type(flubioList) ::  neighbourVolumes(numberOfElements)
        !! list of neighbour volumes

        type(flubioList) ::  neighbourValues(numberOfElements)
        !! list of neighbour values
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: message
        !! message to display

        character(len=25) :: string
        !! dummy string
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !! target component

        integer :: ilen
        !! string length

        integer :: iElement
        !! target cell

        integer :: n
        !! target neighbour

        integer :: nn
        !! number of neighbours

        integer :: countNegs
        !! number of negative neighbour cells

        integer :: countBounded
        !! number of bounded cells

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        real :: phi(numberOfElements+numberOfBFaces,1)

        real :: neighMean
        !! weighted average among the neighbours

        real :: neighbourValue
        !! cell value at the target neighbour cell

        real :: neighbourVol
        !! cell volume at the target neighbour cell

        real :: vol
        !! sum of the cells volume

        real :: lowerBound
        !! lower boundary to clip
    !------------------------------------------------------------------------------------------------------------------

        phi(1:numberOfElements,1) = mesh%volume(1:numberOfElements)
        neighbourVolumes = getNeighbourQuantities(phi=phi, nComp=1)
        neighbourValues = this%getFieldNeighbours()

        do iComp=1,this%nComp

            countBounded = 0
            do iElement=1,numberOfElements

                ! reset for each element
                vol = 0.0
                neighMean = 0.0

                if(this%phi(iElement,iComp) < 0.0 .and. this%phi(iElement,iComp) < lowerBound) then

                    countBounded = countBounded + 1
                    ! loop over neighbours
                    nn = mesh%numberOfNeighbours(iElement)
                    countNegs = 0
                    do n=1,nn
                        neighbourVol = neighbourVolumes(iElement)%getAt(targetRow=n, targetColumn=1, isReal=1.0)
                        vol = vol + neighbourVol
                        neighbourValue = neighbourValues(iElement)%getAt(targetRow=n, targetColumn=iComp, isReal=1.0)
                        neighMean = neighMean + max(neighbourValue, lowerBound)*neighbourVol
                        if(neighbourValue<lowerBound) countNegs = countNegs +1
                    end do

                    neighMean = (neighMean + max(this%phi(iElement,icomp), lowerBound)*mesh%volume(iElement))/(vol + mesh%volume(iElement))

                    ! If it is still negative, clip it
                    this%phi(iElement,iComp) = max(neighMean, lowerBound)

                elseif(this%phi(iElement,iComp) > 0.0 .and. this%phi(iElement,iComp) < lowerBound) then

                    this%phi(iElement,iComp) = lowerBound

                end if

            end do

            ! Sum up negs
            call mpi_AllReduce(countBounded, countBounded, 1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, ierr)

            if(countBounded > 0) then
                call value_to_string(countBounded, string, ilen)
                message = ''// trim(this%fieldName(iComp))// ' bounded in '//trim(string)//' cells'
                call flubioMsg(message)
            endif

        end do

    end subroutine boundWithNeighbours

!**********************************************************************************************************************

    subroutine writeToFile(this)

    !==================================================================================================================
    ! Description:
    !! writeToFile writes the field to a file to be post processed.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: postDir
        !! post processing directory

        character(len=:), allocatable :: fieldDir
        !! field directory

        character(len=20) :: timeStamp
        !! time stamp

        character(len=20) ::  procID
        !! processor ID
    !------------------------------------------------------------------------------------------------------------------

        logical :: dirExists
        !! boolean to check if the directory exists
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !! target components

        integer nComp
        !! number of components

        integer gComp
        !! number of gradient components

        integer :: checkStart
        !! start time to remove time folders

        integer :: removeTime
        !! time folder to be removed
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        postDir='postProc/fields/'
        write(timeStamp,'(i0)') itime
        fieldDir = postDir//trim(timeStamp)//'/'

    ! Create a directory for the field. If it does not exists create it.

        inquire(file=trim(fieldDir)//'/.', exist=dirExists)

        if(dirExists) then

            ! write Field
            open(1,file=fieldDir//trim(this%fieldName(1))//'-d'//trim(procID)//'.bin', form='unformatted')

                write(1) this%nComp
                write(1) this%fieldName(1)

                do iComp = 1,this%nComp
                    write(1) this%phi(:,iComp)
                    write(1) this%phio(:,iComp)
                    write(1) this%phioo(:,iComp)
                end do

            close(1)

            ! Save the gradient if required
            if(this%outputGradient==1) then
                nComp = 3
                do gComp=1,this%nComp

                    open(1,file=fieldDir//'grad'//trim(this%fieldName(gComp))//'-d'//trim(procID)//'.bin', form='unformatted')

                        write(1) nComp
                        write(1) 'grad'//this%fieldName(gComp)

                        do iComp = 1,3
                            write(1) this%phiGrad(:,iComp, gComp)
                            write(1) nComp
                            write(1) nComp
                        end do

                    close(1)
                end do
            end if

        elseif(.not. dirExists) then

            ! create the folder
            call execute_command_line ('mkdir -p ' // adjustl(trim(fieldDir) ) )

            ! write Field
            open(1,file=fieldDir//trim(this%fieldName(1))//'-d'//trim(procID)//'.bin', form='unformatted')

                write(1) this%nComp
                write(1) this%fieldName(1)

                do iComp=1,this%nComp
                    write(1) this%phi(:,iComp)
                    write(1) this%phio(:,iComp)
                    write(1) this%phioo(:,iComp)
                end do

            close(1)

            ! Save the gradient if required
            if(this%outputGradient==1) then
                nComp = 3
                do gComp=1,this%nComp

                    open(1,file=fieldDir//'grad'//trim(this%fieldName(gComp))//'-d'//trim(procID)//'.bin', form='unformatted')

                        write(1) nComp
                        write(1) 'grad'//this%fieldName(gComp)

                        do iComp = 1,3
                            write(1) this%phiGrad(:,iComp, gComp)
                            write(1) nComp
                            write(1) nComp
                        end do

                    close(1)
                end do
            end if

        end if

     end subroutine writeToFile

!**********************************************************************************************************************

    subroutine writeToFileW(this)

    !==================================================================================================================
    ! Description:
    !! writeToFile writes the field to a file to be post processed.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: postDir
        !! post processing directory

        character(len=:), allocatable :: fieldDir
        !! field directory

        character(len=20) :: timeStamp
        !! time stamp

        character(len=20) ::  procID
        !! processor ID
    !------------------------------------------------------------------------------------------------------------------

        logical :: dirExists
        !! boolean to check if the directory exists
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !! target components

        integer nComp
        !! number of components

        integer gComp
        !! number of gradient components

        integer :: checkStart
        !! start time to remove time folders

        integer :: removeTime
        !! time folder to be removed
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        postDir='postProc/fields/'
        write(timeStamp,'(i0)') itime
        fieldDir = postDir//trim(timeStamp)//'/'

        ! Create a directory for the field. If it does not exists create it.

        inquire(file=trim(fieldDir)//'/.', exist=dirExists)

        if(dirExists) then

            ! write Field
            open(1,file=fieldDir//trim(this%fieldName(1))//'-d'//trim(procID)//'.bin', form='unformatted')

                write(1) this%nComp
                write(1) this%fieldName(1)

                do iComp = 1,this%nComp
                    write(1) this%phi(:,iComp)
                    write(1) this%phi(:,iComp)
                    write(1) this%phi(:,iComp)
                end do

            close(1)

        elseif(.not. dirExists) then

            ! create the folder
            call execute_command_line ('mkdir -p ' // adjustl(trim(fieldDir) ) )

            ! write Field
            open(1,file=fieldDir//trim(this%fieldName(1))//'-d'//trim(procID)//'.bin', form='unformatted')

                write(1) this%nComp
                write(1) this%fieldName(1)

                do iComp=1,this%nComp
                    write(1) this%phi(:,iComp)
                    write(1) this%phi(:,iComp)
                    write(1) this%phi(:,iComp)
                end do

            close(1)

        end if

    end subroutine writeToFileW

!**********************************************************************************************************************

    subroutine readFromFile(this, timeToRead, stopIfNotFound)

    !==================================================================================================================
    ! Description:
    !! readFromFile reads the field from a file.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: postDir
        !! post processing directory

        character(len=:), allocatable :: fieldDir
        !! field directory

        character(len=:), allocatable :: fileName
        !! file name

        character(len=20) :: timeStamp
        !! time stamp

        character(len=20) ::  procID
        !! processor ID
    !------------------------------------------------------------------------------------------------------------------

        logical, optional :: stopIfNotFound
        !! If true, the code will stop has the reading of the file is mandatory

        logical :: stopSimulation
        !! If true, simulation will be aborted

        logical :: fileExists
        !! boolean to check if the file exists
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !! target components

        integer :: timeToRead
        !! time to read
    !------------------------------------------------------------------------------------------------------------------

        if(present(stopIfNotFound)) then
            stopSimulation = stopIfNotFound
        else
            stopSimulation = .false.
        endif

        write(procID,'(i0)') id
        write(timeStamp,'(i0)') timeToRead

        postDir = 'postProc/fields/'
        fieldDir = postDir//trim(timeStamp)//'/'
        fileName = fieldDir//trim(this%fieldName(1))//'-d'//trim(procID)//'.bin'

        ! Check if file exists
        inquire(file=fileName, exist=fileExists)

        if(fileExists) then
            call flubioMsg('FLUBIO: recovering ' // trim(this%fieldName(1)) // ' from file...')

            ! Read field
            open(1,file=fieldDir//trim(this%fieldName(1))//'-d'//trim(procID)//'.bin', form='unformatted')

                read(1) this%nComp
                read(1) this%fieldName(1)

                do iComp=1,this%nComp
                    read(1) this%phi(:,iComp)
                    read(1) this%phio(:,iComp)
                    read(1) this%phioo(:,iComp)
                end do

            close(1)

            ! Update ghosts after read
            call this%updateGhosts()

        else

            if(stopSimulation) then
                call flubioStopMsg('ERROR: requested file '//fileName//' cannot be found.')
            else
                call flubioMsg('FLUBIO: requested file '//fileName//' cannot be found, using default initialisation.')
            endif

        endif

    end subroutine readFromFile

! *********************************************************************************************************************

    subroutine appendFieldToList(this)

    !==================================================================================================================
    ! Description:
    !! appendFieldToList appends the field to the list of field to post-process.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: postDir
        !! post processing directory
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp

        logical :: fileExists
        !! boolean, checks if the directory exists
    !------------------------------------------------------------------------------------------------------------------

        postDir='postProc/fields/'

        inquire(file = postDir // "fieldList.txt", exist=fileExists)

        ! Update the file lists to use during post-processing
        if(id==0) then
            if(fileExists .and. itime==0) then
                open(1,file = postDIr // 'fieldList.txt', access='append')
                    write(1,*) trim(this%fieldName(1))
                    if(this%outputGradient==1) then
                        do iComp=1,this%nComp
                            write(1,*) trim('grad'//this%fieldName(iComp))
                        end do
                    end if
                close(1)
            else
                open(1,file=postDir // 'fieldList.txt')
                    write(1,*) trim(this%fieldName(1))
                    if(this%outputGradient==1) then
                        do iComp=1,this%nComp
                            write(1,*) trim('grad'//this%fieldName(iComp))
                        end do
                    end if
                close(1)
            end if
        end if

    end subroutine appendFieldToList

! *********************************************************************************************************************

    subroutine removeFieldsList()

    !==================================================================================================================
    ! Description:
    !! removeFieldsList deletes the any field list at the beginning of a simulation.
    !==================================================================================================================

        character(len=:), allocatable :: postDir
        !! post processing directory
    !------------------------------------------------------------------------------------------------------------------

        logical :: fileExists
        !! boolean, checks if the directory exists
    !------------------------------------------------------------------------------------------------------------------

        postDir='postProc/fields/'

        inquire(file = postDir // "fieldList.txt", exist=fileExists)

        if(id==0) then
            if(fileExists) then
                open(1,file=postDir // "fieldList.txt", STATUS="OLD")
                close(1,STATUS="DELETE")
            end if
        end if

    end subroutine removeFieldsList

! *********************************************************************************************************************

    subroutine updateBoundaryVelocityField(this)

    !==================================================================================================================
    ! Description:
    !! updateBoundaryVelocityFields updates the velocity at boundaries.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iBFace, iOwner, patchFace, iComp, bFlag
    !------------------------------------------------------------------------------------------------------------------

        real :: phi0(3)
        !! boundary value to be assigned

        real:: SMALL
        !! small number

        real, dimension(:), allocatable :: slipLength
        !! slip length for robin bC

        real, dimension(:), allocatable :: phiInf
        !! coefficient in robin boundary condition
    !------------------------------------------------------------------------------------------------------------------

        SMALL = 1e-10

        patchFace = numberOfElements

        do iBoundary=1,numberOfRealBound

            bFlag=this%bcFlag(iBoundary)

            ! Void
            if(bFlag==0) then

                call updateVoidBoundaryField(this%phi, iBoundary, 3)

            ! FixedValue
            elseif(bFlag==1 .or. bFlag==4 .or. bFlag==9) then

                phi0 = this%boundaryValue(iBoundary,:)
                call updateDirichletBoundaryField(this%phi, phi0, iBoundary, 3)

            ! Symmetry and Slip
            elseif (this%bcFlag(iBoundary)==2 .or. this%bcFlag(iBoundary)==3 ) then

                call updateSlipBoundaryField(this%phi, iBoundary)

            ! Outlet
            elseif(this%bcFlag(iBoundary)==5) then

                ! Extrapolate velocity at outlet
                call updateOuletVelocityBoundaryField(this%phi, this%phiGrad, iBoundary)

            ! Fixed gradient
            elseif(this%bcFlag(iBoundary)==6) then

                phi0 = this%boundaryValue(iBoundary,:)
                call updateNeumannBoundaryField(this%phi, phi0, iBoundary, 3)

            ! Robin, mixed boundary condition (l*dudn=u)
            elseif(this%bcFlag(iBoundary)==10) then

                call this%bcDict%get(trim(mesh%boundaries%userName(iBoundary))//'%slipLength', slipLength)
                call this%bcDict%get(trim(mesh%boundaries%userName(iBoundary))//'%phiInf', phiInf)
            !   call updateRobinBoundaryField(this%phi, this%phiGrad, iBoundary, slipLength, phiInf, 3)
                call flubioStopMsg("FLUBIO ERROR: this bc has been not tested. Sorry.")

            elseif(this%bcFlag(iBoundary)==11) then

                phi0 = this%boundaryValue(iBoundary,:)
                call updateFarFieldBoundaryField(this%phi, phi0, iBoundary, 3)

            elseif(this%bcFlag(iBoundary)==12) then
                call updateRotatingBoundaryField(this%phi, iBoundary, this%bcDict)

            ! User defined dirichlet
            elseif(this%bCFlag(iBoundary)==1000) then

                call updateUserDefinedBoundaryField(this%phi, iBoundary, this%boundaryExpression(iBoundary,:), 3)

            ! User defined velocity inlet
            elseif(this%bCFlag(iBoundary)==1001) then

                call updateUserDefinedBoundaryField(this%phi, iBoundary, this%boundaryExpression(iBoundary,:), 3)

            ! Periodic, override check
            elseif(mesh%boundaries%bcType(iBoundary)== 'cyclic') then

            endif

        enddo

    end subroutine updateBoundaryVelocityField

! *********************************************************************************************************************

    subroutine updateBoundaryPressureField(this)

    !==================================================================================================================
    ! Description:
    !! updateBoundaryPressureField updates the pressure at boundaries.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iBFace, iOwner, patchFace, iComp, bFlag
    !------------------------------------------------------------------------------------------------------------------

        real :: phi0(1)
        !! boundary value to be assigned
    !------------------------------------------------------------------------------------------------------------------

        patchFace = numberOfElements

        do iBoundary=1,numberOfRealBound

            bFlag=this%bcFlag(iBoundary)

            ! void
            if(bFlag==0) then

                call updatevoidBoundaryField(this%phi, iBoundary, 1)

            ! fixedPressure
            elseif(bFlag==1) then

                phi0 = this%boundaryValue(iBoundary,1)
                call updateDirichletBoundaryField(this%phi, phi0, iBoundary, 1)

            ! Zero gradient
            elseif (this%bcFlag(iBoundary)==2) then

                call updateneumann0BoundaryField(this%phi, iBoundary, 1)

            ! Inlet-Outlet
            elseif (this%bcFlag(iBoundary)==3) then

                phi0 = this%boundaryValue(iBoundary,1)
                call updatefarFieldPressureBoundaryField(this%phi, phi0, iBoundary, 1)

            ! Dirichlet
            elseif(bFlag==1) then

                phi0 = this%boundaryValue(iBoundary,1)
                call updateDirichletBoundaryField(this%phi, phi0, iBoundary, 1)

            ! Periodic, override check
            elseif(mesh%boundaries%bcType(iBoundary)== 'cyclic') then

            endif

        enddo

    end subroutine updateBoundaryPressureField

! *********************************************************************************************************************

    subroutine updateBoundaryTurbulenceField(this, Cb1)

    !==================================================================================================================
    ! Description:
    !! updateBoundaryTurbulenceField updates a turbulence field (e.g. tke or tdr) at boundaries.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=10) :: bCFlagStr
        !! boundary flag as string

        character(len=:), allocatable :: message
        !! output message
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iBFace, iOwner, patchFace, iComp, is, ie, bFlag
    !------------------------------------------------------------------------------------------------------------------

        real :: SMALL
        !! small number

        real :: phi0(this%nComp)
        !!  boundary value to assign

        real, optional :: Cb1
        !! Constant Cb1, it is optional in this routine

        real, dimension(:), allocatable :: slipLength
        !! slip length for robin bC

        real, dimension(:), allocatable :: phiInf
        !! coefficient in robin boundary condition
    !------------------------------------------------------------------------------------------------------------------

        SMALL = 1.0e-10

        patchFace = numberOfElements
        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
            bFlag = this%bCFlag(iBoundary)

            ! void
            if(this%bCFlag(iBoundary)==0) then

                call updatevoidBoundaryField(this%phi, iBoundary, this%nComp)

            ! Fixed value
            elseif(this%bCFlag(iBoundary)==1) then

                phi0 = this%boundaryValue(iBoundary,1)
                call updateDirichletBoundaryField(this%phi, phi0, iBoundary, this%nComp)

            ! Zero gradient
            elseif(this%bCFlag(iBoundary)==2) then

                call updateneumann0BoundaryField(this%phi, iBoundary, this%nComp)

            ! Fixed Gradient
            elseif(this%bCFlag(iBoundary)==3) then

                call updateneumannBoundaryField(this%phi, this%boundaryValue(iBoundary,:), iBoundary, this%nComp)

            ! Fixed Flux
            elseif(this%bCFlag(iBoundary)==4) then

                call updateprescribedFluxBoundaryField(this%phi, this%boundaryValue(iBoundary,:), iBoundary, this%nComp)

            ! Robin, mixed BC
            elseif(this%bCFlag(iBoundary)==5) then

                call this%bcDict%get(trim(mesh%boundaries%userName(iBoundary))//'%slipLength', slipLength)
                call this%bcDict%get(trim(mesh%boundaries%userName(iBoundary))//'%phiInf', phiInf)
            !   call updateRobinBoundaryField(this%phi, this%phiGrad, iBoundary, slipLength, phiInf, this%nComp)
                call flubioStopMsg("FLUBIO ERROR: this bc has been not tested. Sorry.")

            ! farField
            elseif(this%bCFlag(iBoundary)==6) then

                phi0 = this%boundaryValue(iBoundary,:)
                call updatefarFieldBoundaryField(this%phi, phi0, iBoundary, this%nComp)

            ! User defined dirichlet
            elseif(this%bCFlag(iBoundary)==1000) then

                call updateUserDefinedBoundaryField(this%phi, iBoundary, this%boundaryExpression(iBoundary,:), this%nComp)

            ! Turbulent Intensity
            elseif(this%bCFlag(iBoundary)==7) then

                phi0 = 1.5*this%boundaryValue(iBoundary,1)**2
                call updateDirichletBoundaryField(this%phi, phi0, iBoundary, this%nComp)

            ! Viscosity ratio
            elseif(this%bCFlag(iBoundary)==8) then

                call flubioStopMsg('This condition is not ready yet, please use "fixed value" instead')

                phi0 = this%boundaryValue(iBoundary,1)
                call updateDirichletBoundaryField(this%phi, phi0, iBoundary, this%nComp)

            ! omega low Re wall Function
            elseif(this%bCFlag(iBoundary)==9) then

                ! Do nothing, call setNearWallOmega() if you want to set omega at walls

            ! Omega wall function
            elseif(this%bCFlag(iBoundary)==10) then

                call updateNeumann0BoundaryField(this%phi, iBoundary, this%nComp)

            ! Periodic, override check
            elseif(mesh%boundaries%bcType(iBoundary)== 'cyclic') then

            else

                write(bCFlagStr,'(i0)') this%bCFlag(iBoundary)
                message = 'FLUBIO - updateBoundaryTurbulenceField: Boundary condition number ' //trim(bCFlagStr)// ' not found'
                call flubioStopMsg(message)

            end if

        enddo

    end subroutine updateBoundaryTurbulenceField

!**********************************************************************************************************************

    function getFieldNeighbours(this) result(neighbourValues)

    !==================================================================================================================
    ! Description:
    !! For a taget cell, getCellNeighbours returns a vector with the field values of its nieghbours.
    !==================================================================================================================

        class(flubioField) :: this

        type(flubioList) :: neighbourValues(numberOfElements)
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iElement, iFace, iBFace, iOwner, iNeigh, iProc, pNeigh, patchFace
    !------------------------------------------------------------------------------------------------------------------

        integer :: iOwnerNeighbourCoef, localNeigh, is ,ie
    !------------------------------------------------------------------------------------------------------------------

        !-----------------------------------------------!
        ! Get neighbour value, ghost values are exluded !
        !-----------------------------------------------!

        do iElement=1,numberOfElements

            call neighbourValues(iElement)%initList(isize=mesh%numberOfNeighbours(iElement), jsize=this%nComp, intialValues=0.0)

            do iNeigh=1,mesh%numberOfNeighbours(iElement)
                if(mesh%localNeighbours(iElement)%col(iNeigh)>0) then
                    localNeigh = mesh%localNeighbours(iElement)%col(iNeigh)
                    call neighbourValues(iElement)%setList(startIndex=1, targetRow=iNeigh, &
                                             direction=1, values=this%phi(localNeigh, :), chunkSize=this%nComp)
                endif
            enddo

        enddo

        !------------------------------!
        ! Extend to Processor boundary !
        !------------------------------!

        i = 0
        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1
            patchFace = numberOfElements + (is-numberOfIntFaces-1)
            pNeigh = 0

            i = i+1
            do iBFace=is,ie

                pNeigh = pNeigh+1
                iOwner = mesh%owner(iBFace)
                iOwnerNeighbourCoef = mesh%iOwnerNeighbourCoef(iBFace)

                call neighbourValues(iOwner)%setList(startIndex=1, targetRow=iOwnerNeighbourCoef, &
                                                           direction=1, values=this%phi(patchFace+pNeigh,:), chunkSize=this%nComp)
            enddo

        enddo

    end function getFieldNeighbours

!**********************************************************************************************************************

    subroutine componentToZero(this, cmpt)

    !==================================================================================================================
    ! Description:
    !! componentToZero sets to zero the thirdfield componets.
    !==================================================================================================================

        class(flubioField) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: cmpt
    !------------------------------------------------------------------------------------------------------------------

        this%phi(:,cmpt) = 0.0d0

    end subroutine componentToZero

! *********************************************************************************************************************

    real function getFieldValue(expr, var, varVal)

        use interpreter
    !------------------------------------------------------------------------------------------------------------------

        character(len = 10),  dimension(3) :: var

        character(len = 275) :: expr

        character (len = 5)  :: statusFlag
    !------------------------------------------------------------------------------------------------------------------

        real :: varVal(3), val
    !------------------------------------------------------------------------------------------------------------------

        !Initializes function
        call init(expr, var, statusflag)

        ! evaluate expression
        if(statusFlag=='ok') then
            getFieldValue = evaluate(varVal)
        else
            call flubioStopMsg('ERROR: something went wrong during the expression parsing, please check it!')
        end if

        ! Destroy actual function evaluator
        call destroyfunc()

        return

    end function getFieldValue

!**********************************************************************************************************************

    function getNeighbourQuantities(phi, nComp) result(neighbourPhi)

    !==================================================================================================================
    ! Description:
    !! getNeighbourQuantities returns a vector containing a target quantites from cell neighbours.
    !==================================================================================================================

        type(flubioList) :: neighbourPhi(numberOfElements)
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iElement, iFace, iBFace, iOwner, iNeigh, iProc, pNeigh, patchFace

        integer :: iOwnerNeighbourCoef, localNeigh, is ,ie, nComp
    !------------------------------------------------------------------------------------------------------------------

        real :: phi(numberOfElements+numberOfBFaces, nComp)
    !------------------------------------------------------------------------------------------------------------------

        ! update ghost cells
        call mpi_update_ghosts_I(phi, nComp)

        ! Loop over elements
        do iElement=1,numberOfElements

            call neighbourPhi(iElement)%initList(isize=mesh%numberOfNeighbours(iElement), jsize=nComp, intialValues=0.0)

            do iNeigh=1,mesh%numberOfNeighbours(iElement)
                if(mesh%localNeighbours(iElement)%col(iNeigh)>0) then
                    localNeigh = mesh%localNeighbours(iElement)%col(iNeigh)
                    call neighbourPhi(iElement)%setList(startIndex=1, targetRow=iNeigh, &
                                      direction=1, values=phi(localNeigh,:), chunkSize=nComp)
                endif
            enddo

        enddo

        ! Extend to Processor boundary
        i = 0
        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1
            patchFace = numberOfElements + (is-numberOfIntFaces-1)
            pNeigh = 0

            i = i+1
            do iBFace=is,ie

                pNeigh = pNeigh+1

                iOwner = mesh%owner(iBFace)
                iOwnerNeighbourCoef = mesh%iOwnerNeighbourCoef(iBFace)

                call neighbourPhi(iOwner)%setList(startIndex=1, targetRow=iOwnerNeighbourCoef, &
                                                    direction=1, values=phi(patchFace+pNeigh,:), chunkSize=nComp)
            enddo

        enddo

    end function getNeighbourQuantities

!**********************************************************************************************************************

    subroutine fieldVerbose(field)

    !==================================================================================================================
    ! Description:
    !! fieldVerbose prints at screen some information related to the simulation.
    !! You can customize the output as you retain more appropriate.
    !==================================================================================================================

        class(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: str
        !! auxiliary string

        character(len=:), allocatable :: strMinLoc
        !! auxiliary string

        character(len=:), allocatable :: strMaxLoc
        !! auxiliary string

        character(len=:), allocatable ::  final
        !! output string

        character(len=30) :: max
        !! maxium value in string

        character(len=30) :: min
        !! minimum value in string

        character(len=16) :: minPosition(3)
        !! minimum location as string

        character(len=16) :: maxPosition(3)
        !! maximum location as string

        integer :: iComp
        !! target componenents

        integer :: fcomp
        !! number of field components
    !------------------------------------------------------------------------------------------------------------------

        real, dimension(:), allocatable :: minField
        !! minimum field value

        real, dimension(:), allocatable :: maxField
        !! maximum field value

        real, dimension(:,:), allocatable :: minCellCenter
        !! cell center of the min cell center

        real, dimension(:,:), allocatable  :: maxCellCenter
        !! cell center of the max cell center
    !------------------------------------------------------------------------------------------------------------------

        if(flubioControls%verbosityLevel==1) call minMaxField(field, minField, maxField, minCellCenter, maxCellCenter)

        if(id==0 .and. flubioControls%verbosityLevel==1) then

            fcomp =  field%nComp
            if(fcomp>1) fcomp = fcomp-bdim

            do iComp=1,fcomp
                write(min,'(ES12.5)') minField(iComp)
                write(max,'(ES12.5)') maxField(iComp)
                str = 'min('// trim(field%fieldName(iComp))//'), max('// trim(field%fieldName(iComp))//'): '

                write(minPosition(1),'(ES12.5)') minCellCenter(iComp,1)
                write(minPosition(2),'(ES12.5)') minCellCenter(iComp,2)
                write(minPosition(3),'(ES12.5)') minCellCenter(iComp,3)

                write(maxPosition(1),'(ES12.5)') maxCellCenter(iComp,1)
                write(maxPosition(2),'(ES12.5)') maxCellCenter(iComp,2)
                write(maxPosition(3),'(ES12.5)') maxCellCenter(iComp,3)
                strMinLoc = ', min at: (' // trim(minPosition(1)) // ', ' // trim(minPosition(2)) // ', ' // trim(minPosition(3)) // ')'
                strMaxLoc = ', max at: (' // trim(maxPosition(1)) // ', ' // trim(maxPosition(2)) // ', ' // trim(maxPosition(3)) // ')'

                final = ' ' // str //'('// adjustl(trim(min)) // ', ' // trim(adjustl(trim(max)))//') ' // adjustl(trim(strMinLoc)) // adjustl(trim(strMaxLoc))
                write(*,*) final
            end do

        endif

    end subroutine fieldVerbose

!**********************************************************************************************************************

    subroutine setGradientType(field)

    !==================================================================================================================
    ! Description:
    !! setGradientType sets the gradient option to be used with this field. It overrrides the default value.
    !==================================================================================================================

        class(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: optName
        !! option name

        character(len=:), allocatable :: opt
        !! option value

        character(len=:), dimension(:), allocatable :: acvec
        !! first array element lenght
    !------------------------------------------------------------------------------------------------------------------

        integer :: iField

        integer, dimension(:),allocatable :: ilen
        !! first array element lenght
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        optName = 'gradient-'//trim(field%fieldName(1))
        call flubioOptions%json%get(optName, opt, found)
        if(.not. found) opt='none'

        if(lowercase(opt)=='greengauss') then
            field%gradientType=0
        elseif(lowercase(opt)=='leastsquares') then
            field%gradientType=1
        elseif(lowercase(opt)=='greengausstvd') then
            field%gradientType=2
        else
            field%gradientType = flubioOptions%gradOpt
        end if

        optName = 'gradientLimiter-'//trim(field%fieldName(1))
        call flubioOptions%json%get(optName, opt, found)
        if(.not. found) opt='none'

        if(lowercase(opt)=='celllimitedmd') then
            field%gradientLimiterType=1
        elseif(lowercase(opt)=='faceLimitedmd') then
            field%gradientLimiterType=2
        else
            field%gradientLimiterType=flubioOptions%gradLim
            field%kLim = flubioOptions%kLim
        end if

        if(opt/='none') then
            optName = 'clipping-'//trim(field%fieldName(1))
            call flubioOptions%json%get(optName, field%kLim, found)
        end if

        ! Set gradient output flag
        call flubioControls%json%get('SaveGradients', acvec, ilen, found)
        if(found) then
            do iField=1,size(acvec)
                if(acvec(iField)==field%fieldName(1)) then
                    field%outputGradient=1
                else
                    field%outputGradient=0
                end if
            end do
        end if

    end subroutine setGradientType

! *********************************************************************************************************************

    subroutine relax(field, urf)

    !==================================================================================================================
    ! Description:
    !! relax explicit relax a filed as:
    !! \begin(equation)
    !! \phi^{new} = \phi^{old} + urf*(\phi^{new}-\phi^{old})
    !! \end{equation}
    !==================================================================================================================

        class(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        real :: urf
        !! under-relaxation factor
    !------------------------------------------------------------------------------------------------------------------

        field%phi = field%phio + urf*(field%phi - field%phio)

    end subroutine relax

! *********************************************************************************************************************

    subroutine linearInterpolation(field)

    !==================================================================================================================
    ! Description:
    !! linearInterpolation performs a standard linear interpolation to find the face values of a variable.
    !==================================================================================================================

        class(flubioField) :: field
        !! field to interpolate
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iBFace,  iFace, Owner, Neighbour

        integer :: i, pNeigh, is, ie, opt, patchFace, ghostFace
    !------------------------------------------------------------------------------------------------------------------

        real :: corr
        !! skewness correction

        real :: gC
        !! linear interpolation weight

        real :: magSf
        !! face area
    !------------------------------------------------------------------------------------------------------------------

        field%phif = 0.0

        call field%updateGhosts()

        do iFace=1,numberOfIntFaces

            Owner = mesh%owner(iFace)
            Neighbour = mesh%neighbour(iFace)

            gC = mesh%gf(iFace)

            field%phif(iFace,:) = gC*field%phi(Neighbour,:) + (1-gC)*field%phi(Owner,:)

        enddo

        !----------------!
        ! Boundary Faces !
        !----------------!

        patchFace = 0
        i = 0
        do iBoundary=1,numberOfBoundaries

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
            ghostFace = numberOfElements + (is-numberOfIntFaces-1)
            pNeigh = 0

            if(mesh%boundaries%realBound(iBoundary)==-1) i=i+1

            do iBFace=is, ie

                patchFace = patchFace+1
                Owner = mesh%owner(iBFace)

                if(mesh%boundaries%realBound(iBoundary)/=-1) then
                    ! Real boundaries
                    field%phif(iBFace,:) = field%phi(numberOfElements+patchFace,:)
                else
                    ! Processor Boundaries
                    pNeigh = pNeigh+1
                    gC = mesh%gf(iBFace)
                    field%phif(iBFace,:) = gC*field%phi(ghostFace+pNeigh,:) + (1.0-gC)*field%phi(Owner,:)
                endif

            enddo

        enddo

    end subroutine linearInterpolation

!**********************************************************************************************************************

    function normal(field) result(n)

    !==================================================================================================================
    ! Description:
    !! normal computes the field normal using is face gradient.
    !==================================================================================================================

        class(flubioField) :: field
        !! field to interpolate
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp, iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: small
        ! small number ot avoid division by 0

        real :: gmag
        !! gradient magnitude

        real :: n(numberOfElements+numberOfBFaces,3,field%nComp)
        !! normal to the field
    !------------------------------------------------------------------------------------------------------------------

        small = 1e-10

        do iComp=1,field%ncomp

            do iElement=1,numberOfElements+numberOfBFaces
                gmag=sqrt(field%phiGrad(iElement,1,iComp)**2 + &
                          field%phiGrad(iElement,2,iComp)**2 + &
                          field%phiGrad(iElement,3,iComp)**2)

                n(iElement,1:3,iComp) = field%phiGrad(iElement,1:3,iComp)/(gmag + small)

            end do

        end do

    end function normal

! *********************************************************************************************************************

    function getFieldSize(fieldName, timeToRead) result(nComp)

    !==================================================================================================================
    ! Description:
    !! readFromFile reads the field from a file.
    !==================================================================================================================

        character(len=*) :: fieldName
        !! Name of the field

        character(len=:), allocatable :: postDir
        !! post processing directory

        character(len=:), allocatable :: fieldDir
        !! field directory

        character(len=20) :: timeStamp
        !! time stamp

        character(len=20) :: procID
        !! processor ID
    !------------------------------------------------------------------------------------------------------------------

        logical :: dirExists
        !! boolean to check if the directory exists
    !------------------------------------------------------------------------------------------------------------------

        integer :: timeToRead
        !! time to read

        integer :: nComp
        !! number of components
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        postDir='postProc/fields/'
        write(timeStamp,'(i0)') timeToRead
        fieldDir = postDir//trim(timeStamp)//'/'

        call flubioMsg('FLUBIO: recovering ' // trim(fieldName) // ' from file...')

        ! Check if the directorty exists.
        inquire(file=trim(fieldDir)//'/.', exist=dirExists)

        if(dirExists) then
            ! Write Field
            open(1,file=fieldDir//trim(fieldName)//'-d'//trim(procID)//'.bin', form='unformatted')
            read(1) nComp
            close(1)
        else
            call flubioMsg('FLUBIO: field folder does not exists... something went wrong!')
        end if

    end function getFieldSize

!**********************************************************************************************************************

end module flubioFields