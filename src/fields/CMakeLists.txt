# Library name
set(TARGET flubiofields)

# Collect files
file(GLOB src "*.f03")
set_source_files_properties(${src} PROPERTIES LANGUAGE Fortran)

# Add the library

add_library(${TARGET} ${src})
set_target_properties(${TARGET} PROPERTIES COMPILE_FLAGS ${CMAKE_Fortran_FLAGS})
target_include_directories(${TARGET} PRIVATE "${JSON_INC_PATH};${TTB_INC_PATH};${MPI_Fortran_INCLUDE_PATH};${CMAKE_Fortran_MODULE_DIRECTORY}") 
target_link_libraries(${TARGET} PRIVATE ${JSON_LIBRARY} ${MPI_Fortran_LIBRARIES} ${TTB_LIBRARY}
                                        globalvar meshvar fortranexpressionevaluator flubiodictionaries bcs userdefinedtypes)
