!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module gradientBasedInterpolations

!==================================================================================================================
! Description:
!! gradientBasedInterpolations implements the methods to compute the face value according with a gradient based scheme.
!==================================================================================================================

	use flubioFields
	use flubioDictionaries
	use standardInterpolations
	use meshvar
	use fieldvar

	implicit none

contains

	subroutine gradientBasedInterpolation(field, convOpt, fComp)

	!==================================================================================================================
	! Description:
	!! gradientBasedInterpolation interpolates a field at mesh faces using a gradient based scheme.
	!==================================================================================================================

		type(flubioField) :: field
	!------------------------------------------------------------------------------------------------------------------

		integer :: iComp, iFace, iBFace, iOwner, iNeighbour, iBoundary, fComp, is, ie, convOpt

		integer :: iUpwind, iDownwind, iProc, pNeigh

		integer :: pos(numberOfFaces,1), sw
		!! upwind cell indicator

		integer :: patchFace
		!! auxiliary index
	!------------------------------------------------------------------------------------------------------------------

		real :: phiGrad_f(numberOfFaces,3,fComp)
		!! field gradient interpolated at mesh faces
	!------------------------------------------------------------------------------------------------------------------

		real :: upwindGrad(3,fComp)
		!! upwind cell gradient

		real :: rCf(3)
		!! cell center to cell face distance

		real :: CN(3)
		!! distance beween owner and neighbour

		real :: grad_f(3)
		!! field gradient interpolated at a cell face

		real :: aux(3)
		!! auxiliary vector

		real :: dot
		!! auxiliary value to store a scalar product

		real :: corr
		!! deffered correction term

		real :: d1
		!! auxiliary value

		real :: d2
		!! auxiliary value

		real :: phiC(fComp)
		!! upwind cell value

		real :: phiD(fComp)
		!! downind cell value

		real :: phi_f
		!! face value

		real :: L
		!! inverse distance weighting factor

		real :: switch
		!! see pos

		real :: psi
		!! limiter
	!------------------------------------------------------------------------------------------------------------------

		! Prepare
		pos = 0
		field%phif = 0.0

		call interpolateGradientCorrected(phiGrad_f, field, opt=1, fComp=fComp)

		!--------------------------!
		! Update convective fluxes !
		!--------------------------!

		do iFace=1,numberOfIntFaces

			iOwner = mesh%owner(iFace)
			iNeighbour = mesh%neighbour(iFace)

			! Find index of the upwind cell
			if (mf(iFace,1)>=0) pos(iFace,1)=1

			iUpwind   =  pos(iFace,1)*iOwner + (1-pos(iFace,1))*iNeighbour
			iDownwind =  pos(iFace,1)*iNeighbour + (1-pos(iFace,1))*iOwner
			upwindGrad = field%phiGrad(iUpwind,:,:)

			! Compute correction for HO schemes
			rCf = mesh%fcentroid(iFace,:) - mesh%centroid(:,iUpwind)

			! Compute phiC, phiD, phiU (upwind, downwind and far upwind values)
			phiC = field%phi(iUpwind, :)
			phiD = field%phi(iDownwind, :)

			upwindGrad = field%phiGrad(iUpwind,:,:)

			! Compute face value
			do iComp=1, fComp
				grad_f = phiGrad_f(iFace,:,iComp)
				call  gradientBasedSchemes(upwindGrad(:,iComp), grad_f, rCf, corr, convOpt)
				field%phif(iFace,iComp) = phiC(iComp) + corr
			enddo

		enddo

		!----------------------!
		! Processor Boundaries !
		!----------------------!

		do iBoundary=1,numberOfProcBound

			iProc = mesh%boundaries%procBound(iBoundary)
			is = mesh%boundaries%startFace(iProc)
			ie = is+mesh%boundaries%nFace(iProc)-1
			patchFace = numberOfElements + (is-numberOfIntFaces-1)
			pNeigh = 0

			do iBFace=is,ie

				pNeigh = pNeigh+1
				iOwner = mesh%owner(iBFace)

				! Compute correction for HO schemes
				if(mf(iBFace,1)>=0) pos(iBFace,1)=1

				switch = pos(iBFace,1)

				rCf = mesh%fcentroid(iBFace,:) - mesh%centroid(:,iOwner)*switch - mesh%centroid(:,patchFace+pNeigh)*(1-switch)

				upwindGrad = field%phiGrad(iOwner,:,:)*switch + field%phiGrad(patchFace+pNeigh,:,:)*(1-switch)

				! Compute phiC, phiD, phiU (upwind, downwind and far upwind values)
				phiC = field%phi(iOwner,:)*switch + field%phi(patchFace+pNeigh,:)*(1-switch)
				phiD = field%phi(patchFace+pNeigh,:)*switch + field%phi(iOwner,:)*(1-switch)

				! Compute face value
				do iComp=1, fComp

					grad_f = phiGrad_f(iBFace,:,iComp)
					call gradientBasedSchemes(upwindGrad(:,iComp), grad_f, rCf, corr, convOpt)

					field%phif(iBFace,iComp) = phiC(iComp) + corr

				enddo

			enddo

		enddo

		!-----------------!
		! Real Boundaries !
		!-----------------!

		patchFace = 0
		do iBoundary=1,numberOfRealBound

			is = mesh%boundaries%startFace(iBoundary)
			ie = is+mesh%boundaries%nFace(iBoundary)-1

			do iBFace=is, ie
				patchFace = patchFace+1
				field%phif(iBFace,:) = field%phi(numberOfElements+patchFace,:)
			enddo

		enddo

	end subroutine gradientBasedInterpolation

! **********************************************************************************************************************

	subroutine gradientBasedSchemes(grad, grad_f, rCf, corr, opt)

	!===================================================================================================================!
	! Description:
	!! gradientBasedSchemes computes the deferred correction for gradient based schemes.
	!! In essence, it compues the difference between the upwind face value and the face value computed with an HR scheme.
	!===================================================================================================================!

		integer :: opt
		!! scheme option
	!------------------------------------------------------------------------------------------------------------------

		real :: grad(3)
		!! upwind cell gradient

		real :: rCf(3)
		!! cell center to cell face distance

		real :: CN(3)
		!! distance beween owner and neighbour

		real :: grad_f(3)
		!! field gradient interpolated at a cell face

		real :: aux(3)
		!! auxiliary vector

		real :: dot
		!! auxiliary value to store a scalar product

		real :: corr
		!! deffered correction term

		real :: phi_f
		!! face value
	!------------------------------------------------------------------------------------------------------------------

		if(opt==1) then

			! CD
			aux = grad_f
			dot = dot_product(aux, rCf)
			corr = dot

		elseif(opt==2) then

			! SOU
			aux = 2.0*grad - grad_f
			dot = dot_product(aux, rCf)
			corr = dot

		elseif(opt==3) then

			! QUICK
			aux = grad + grad_f
			dot = dot_product(aux, rcf)
			corr = 0.5*dot

		elseif(opt==4) then

			! Fromm
			aux = grad
			dot = dot_product(aux, rcf)
			corr = dot

		else
			call flubioMsg('Error: unknown gradient based scheme')
		endif

	end subroutine gradientBasedSchemes

! *********************************************************************************************************************

	subroutine interpolateGradientCorrected(phiGrad_f, field, opt, fComp)

	!==================================================================================================================!
	! Description:
	!! interpolateGradientCorrected interpolates the gradient of a variable from cell centers to faces in two ways:
	!! - standard linear interpolation
	!! - corrected interpolation to be often preferred
	!==================================================================================================================!

		type(flubioField) :: field
		!! field use to compute the gradient
	!-------------------------------------------------------------------------------------------------------------------

		integer iBoundary, iFace, iOwner, iNeighbour

		integer i, is, ie, opt, fComp, iComp, iProc, pNeigh, patchFace
	!------------------------------------------------------------------------------------------------------------------

		real phiGrad_f(numberOfFaces,3,fComp)
		!! field gradient interpolated at mesh faces

		real :: dphi(fComp)
		!! difference between owner and neighbour cell center values

		real :: dmag
		!! distance vector magnitude

		real :: dot
		!! auxiliary value to store a doct product
	!------------------------------------------------------------------------------------------------------------------

		phiGrad_f = 0.0

		!------------------------!
		! Standard interpolation !
		!------------------------!

		do iComp=1,fComp

			do	i=1,3
				call interpolateElementToFace(phiGrad_f(:,i,iComp),field%phiGrad(:,i,iComp), 1, 0)
			enddo

			!------------------------------!
			! Correction of internal faces !
			!------------------------------!

			if (opt/=0) then

				do iFace=1,numberOfIntFaces

					iOwner = mesh%owner(iFace)
					iNeighbour = mesh%neighbour(iFace)

					dphi(iComp) = field%phi(iNeighbour,iComp) - field%phi(iOwner,iComp)
					dmag = sqrt(dot_product(mesh%CN(iFace,:), mesh%CN(iFace,:)))

					dot = dot_product(phiGrad_f(iFace,:,iComp), mesh%CN(iFace,:)/norm2(mesh%CN(iFace,:)))
					phiGrad_f(iFace,:,iComp) = phiGrad_f(iFace,:,iComp) + (dphi(iComp)/dmag-dot)*mesh%CN(iFace,:)/norm2(mesh%CN(iFace,:))

				enddo

				!----------------------!
				! Processor Boundaries !
				!----------------------!

				do iBoundary=1,numberOfProcBound

					iProc = mesh%boundaries%procBound(iBoundary)

					is = mesh%boundaries%startFace(iProc)
					ie = is+mesh%boundaries%nFace(iProc)-1
					patchFace = numberOfElements + (is-numberOfIntFaces-1)
					pNeigh = 0

					do iFace=is,ie

						pNeigh = pNeigh+1

						iOwner = mesh%owner(iFace)

						dphi(iComp) = (field%phi(patchFace+pNeigh,iComp) - field%phi(iOwner,iComp))
						dmag = sqrt(dot_product(mesh%CN(iFace,:), mesh%CN(iFace,:)))

						dot = dot_product(phiGrad_f(iFace,:,iComp),mesh%CN(iFace,:)/norm2(mesh%CN(iFace,:)))
						phiGrad_f(iFace,:,iComp) = phiGrad_f(iFace,:,iComp) + (dphi(iComp)/dmag-dot)*mesh%CN(iFace,:)/norm2(mesh%CN(iFace,:))

					enddo

				enddo

			endif

		enddo

	end subroutine interpolateGradientCorrected

!**********************************************************************************************************************

end module gradientBasedInterpolations