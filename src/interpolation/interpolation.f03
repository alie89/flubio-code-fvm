!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module interpolation

!==================================================================================================================
! Description:
!! interpolation cointains the method to produce a field interpolation. It is a wrapper for interpolation methods.
!==================================================================================================================

    use flubioDictionaries
    use flubioFields
    use meshvar
    use gradientBasedInterpolations
    use standardInterpolations
    use tvdInterpolations

    implicit none

contains

    subroutine interpolate(field, interpType, opt)

    !==================================================================================================================
    ! Description:
    !! interpolate performs the field interpolation at mesh faces according to the user's selected method.
    !==================================================================================================================

        type(flubioField) :: field
        !! field to interpolate
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: interpType
        !! interpolation type
    !------------------------------------------------------------------------------------------------------------------

        integer :: opt
        !! interpolation option

        integer :: fComp
        !! number of field components
    !------------------------------------------------------------------------------------------------------------------

        fComp = field%nComp

        ! Update ghosts
        call field%updateGhosts()

        ! Linear
        if(trim(interpType)=='linear' .or. trim(interpType)=='skewcorrected') then

            call interpolateElementToFaceStd(field, flubioOptions%interpOpt)

        ! Hamonic
        elseif(trim(interpType)=='harmonic') then

             call harmonicInterpolation(field)

        ! Gradient based
        elseif(trim(interpType)=='gradientbased') then

              call gradientBasedInterpolation(field, opt, fComp)

        ! TVD
        elseif(trim(interpType)=='tvd') then

              call TVDInterpolation(field, opt, fComp)

        else
               call printInterpolationMethodsList(opt)

        endif

     end subroutine interpolate

!**********************************************************************************************************************

    function faceFlux(field, opt, coeff) result(f)

    !==================================================================================================================
    ! Description:
    !! faceFlux returns the face flux of a field.
    !==================================================================================================================

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace

        integer :: opt
    !------------------------------------------------------------------------------------------------------------------

        real :: f(numberOfFaces, 1)
        !! cell center averaged field

        real :: dot, c

        real, optional :: coeff
    !------------------------------------------------------------------------------------------------------------------

        f = 0.0

        if(present(coeff))then
            c = coeff
        else
            c = 1.0
        endif

        ! Interpolate the field at cell faces
        if(opt==1) then
            call interpolate(field, interpType='linear', opt=-1)
        elseif(opt >= 2 .and. opt <= 4) then
            call interpolate(field, interpType='gradientbased', opt=opt)
        else
            call interpolate(field, interpType='tvd', opt=opt)
        end if

        do iFace=1,numberOfFaces

            if(field%nComp > 1) then
                dot = c*dot_product(field%phif(iFace,1:field%nComp), mesh%Sf(iFace, 1:field%nComp))
            else
                dot = c*field%phif(iFace,1)*norm2(mesh%Sf(iFace,:))
            end if

            f(iFace,1) = dot

        end do

    end function faceFlux

!**********************************************************************************************************************

    function boundaryFlux(field, targetBoundary, coeff, opt) result(f)

    !==================================================================================================================
    ! Description:
    !! boundaryFlux returns the flux of a field at a target boundary.
    !==================================================================================================================
    
        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------
    
        character(len=*) :: targetBoundary
    !------------------------------------------------------------------------------------------------------------------

        integer, optional :: opt

        integer :: i, iBFace, is, ie, nFaces, interOption, boundaryIndex(3)
    !------------------------------------------------------------------------------------------------------------------
    
        real, dimension(:), allocatable :: f
        !! cell center averaged field
        
        real :: dot, c
    
        real, optional :: coeff
    !------------------------------------------------------------------------------------------------------------------
    
        if(present(coeff))then
            c = coeff
        else
            c = 1.0
        endif

        if(present(opt))then
            interOption = opt
        else
            interOption = 1
        endif
    
        ! Interpolate the field at cell faces
        if(interOption==1) then
            call interpolate(field, interpType='linear', opt=-1)
        elseif(interOption >= 2 .and. interOption <= 4) then
            call interpolate(field, interpType='gradientbased', opt=interOption)
        else
            call interpolate(field, interpType='tvd', opt=interOption)
        end if

        boundaryIndex = mesh%boundaries%findBoundaryByName(targetBoundary)
    
        is = boundaryIndex(1)
        ie = boundaryIndex(2)
        nFaces = boundaryIndex(3)

        allocate(f(nFaces))

        i = 0
        do iBFace=is,ie
    
            i = i+1

            if(field%nComp > 1) then
                dot = c*dot_product(field%phif(iBFace,1:field%nComp), mesh%Sf(iBFace, 1:field%nComp))
            else
                dot = c*field%phif(iBFace,1)*norm2(mesh%Sf(iBFace,:))
            end if

            f(i) = dot
    
        end do
    
    end function boundaryFlux

!**********************************************************************************************************************

    function boundaryMassFlow(field, targetBoundary) result(f)

    !==================================================================================================================
    ! Description:
    !! boundaryMassFlow returns the mass flow rate through target boundary.
    !==================================================================================================================
        
        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------
        
        character(len=*) :: targetBoundary
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBFace, is, ie, nFaces, interOption, boundaryIndex(3)
    !------------------------------------------------------------------------------------------------------------------
        
        real, dimension(:), allocatable :: f
        !! cell center averaged field
    !------------------------------------------------------------------------------------------------------------------
                    
        boundaryIndex = mesh%boundaries%findBoundaryByName(targetBoundary)
        
        is = boundaryIndex(1)
        ie = boundaryIndex(2)
        nFaces = boundaryIndex(3)
    
        allocate(f(nFaces))
    
        i = 0
        do iBFace=is,ie
            i = i+1
            f(i) = mf(iBFace,1)
        end do
        
    end function boundaryMassFlow

!**********************************************************************************************************************

    subroutine printInterpolationMethodsList(opt)

    !==================================================================================================================
    ! Description:
    !! printInterpolationMethods prints to screen the available interpolation methods.
    !==================================================================================================================

        character(len=15), dimension(4) :: list
    !------------------------------------------------------------------------------------------------------------------

        integer :: opt, scheme
    !------------------------------------------------------------------------------------------------------------------

        list(1) = 'linear'
        list(2) = 'gradientBased'
        list(3) = 'harmonic'
        list(4) = 'tvd'

        if(id==0) then
            write(*,*) 'FLUBIO: unknown interpolation method: ', opt
            write(*,*) 'Available interpolation methods are:'
            do scheme=1,size(list)
                write(*,*) trim(list(scheme))
            end do
        end if

        call flubioStop()

    end subroutine printInterpolationMethodsList

!**********************************************************************************************************************

    function faceAveraging(field) result(avg)

    !==================================================================================================================
    ! Description:
    !! faceAveraging returns the cell face averaged
    !==================================================================================================================

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iFace, iBFace, iOwner, iNeighbour
    !------------------------------------------------------------------------------------------------------------------

        real :: avg(numberOfElements, field%nComp)
        !! cell center averaged field

        real :: areaSum(numberOfElements)
    !------------------------------------------------------------------------------------------------------------------

        avg = 0.0
        areaSum = 0.0

        ! Interpolate the field at cell faces
        call interpolate(field, interpType='linear', opt=-1)

        ! Internal Faces
        do iFace=1,numberOfIntFaces
            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            areaSum(iOwner) = areaSum(iOwner) + norm2(mesh%Sf(iFace,:))
            areaSum(iNeighbour) = areaSum(iNeighbour) + norm2(mesh%Sf(iFace,:))

            avg(iOwner,:) = avg(iOwner,:) + field%phif(iFace,:)*norm2(mesh%SF(iFace,:))
            avg(iNeighbour,:) = avg(iNeighbour,:) + field%phif(iFace,:)*norm2(mesh%Sf(iFace,:))
        end do

        ! Boundary Faces
        do iFace=numberOfIntFaces+1,numberOfFaces
            iOwner = mesh%owner(iFace)
            areaSum(iOwner) = areaSum(iOwner) + norm2(mesh%Sf(iFace,:))
            avg(iOwner,:) = avg(iOwner,:) + field%phif(iFace,:)*norm2(mesh%Sf(iFace,:))
        end do

        do iElement=1,numberOfElements
            avg(iElement,:) = avg(iElement,:)/areaSum(iElement)
        end do

    end function faceAveraging

!**********************************************************************************************************************

    subroutine boundWithFaceValues(field, lowerBound)

    !==================================================================================================================
    ! Description:
    !! boundWithFaceValues bound a field using face values.
    !==================================================================================================================

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iElement, iFace, iBFace, iComp, iOwner, iNeighbour, is, ie
    !------------------------------------------------------------------------------------------------------------------

        real :: avg(numberOfElements, field%nComp)

        real :: lowerBound
    !------------------------------------------------------------------------------------------------------------------

        avg = faceAveraging(field)

        do iElement=1,numberOfElements

            do iComp=1,field%nComp

                if(field%phi(iElement, iComp) < lowerBound .and. field%phi(iElement, iComp) > 0.0) then
                    field%phi(iElement, iComp) = lowerBound
                elseif(field%phi(iElement, iComp) < lowerBound .and. field%phi(iElement, iComp) < 0.0) then
                    field%phi(iElement, iComp) = max(avg(iElement,iComp), lowerBound)
                end if

            end do

        end do

    end subroutine boundWithFaceValues

!**********************************************************************************************************************

    subroutine bound(field, lowerBound, boundType)

    !==================================================================================================================
    ! Description:
    !! bound counds a field using the selected methods
    !==================================================================================================================

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: boundType
    !------------------------------------------------------------------------------------------------------------------

        real :: lowerBound
    !------------------------------------------------------------------------------------------------------------------

        if(lowercase(boundType)=='clipped') then
            call field%bound0(lowerBound)
        elseif(lowercase(boundType)=='cellweighted') then
            call field%boundWithNeighbours(lowerBound)
        elseif(lowercase(boundType)=='faceweighted') then
            call boundWithFaceValues(field, lowerBound)
        else
            call boundWithFaceValues(field, lowerBound)
        end if

        ! Update ghosts
        call field%updateGhosts()

    end subroutine bound

!**********************************************************************************************************************

end module interpolation