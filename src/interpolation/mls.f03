!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module mlsInterpolation

    use m_strings
    use flubioMpi
    use meshvar

    implicit none

contains 

    function weightingFunction(r, d, functionType) result(W)

    !==================================================================================================================
    ! Description:
    !! weightingFunction returns the value of the selected weighting function.
    !==================================================================================================================        

        character(len=*) :: functionType
        !! function type
    !------------------------------------------------------------------------------------------------------------------   
        
        real :: r
        !! distance 

        real :: d 
        !! support size

        real :: W 
        !! weighting  function value
    !------------------------------------------------------------------------------------------------------------------

        if(lowercase(trim(functionType))=='exponential') then
            W = exponentialWeighting(r, d)
        elseif(lowercase(trim(functionType))=='rbf') then
            W = rbfWeighting(r, d)  
        elseif(lowercase(trim(functionType))=='cubic') then
            W = cubicWeighting(r, d)       
        elseif(lowercase(trim(functionType))=='roma') then
            W = romaWeighting(r, d)
        elseif(lowercase(trim(functionType))=='peskin') then
            W = peskinWeighting(r, d)              
        else 
            call flubioMsg('FLUBIO ERRROR: unknown weighthing function '// trim(functionType) // '. Available choices are:')
            call flubioMsg('- exponential')
            call flubioMsg('- rbf')
            call flubioMsg('- cubic')
            call flubioMsg('- roma')
            call flubioMsg('- peskin')
            call flubioStop()                
        endif    

    end function weightingFunction

! *********************************************************************************************************************

    function exponentialWeighting(r, d) result(W)

    !==================================================================================================================
    ! Description:
    !! exponentialWeighting returns the value of the exponential weighting function.
    !==================================================================================================================          
        
        real :: r
        !! distance 
        
        real :: rtilde
        !! scaled distance 

        real :: d 
        !! support size

        real :: W 
        !! weighting  function value
    !------------------------------------------------------------------------------------------------------------------

        rtilde = abs(r/d)

        if(rtilde < 1.0) then 
            W = exp(-(rtilde/0.3)**2)
        else 
            W = 0.0
        endif         

    end function exponentialWeighting

! *********************************************************************************************************************

    function rbfWeighting(r, d) result(W)

    !==================================================================================================================
    ! Description:
    !! rbfWeighting returns the value of the exponential rbf weighting function.
    !==================================================================================================================          
            
        real :: r
        !! distance 
            
        real :: rtilde
        !! scaled distance 
    
        real :: d 
        !! support size
    
        real :: W 
        !! weighting  function value
    !------------------------------------------------------------------------------------------------------------------
    
        rtilde = abs(r/d)
    
        if(rtilde < 2.0) then 
            W = exp(-2.0*rtilde**2)
        else 
            W = 0.0
        endif         
    
    end function rbfWeighting
    
! *********************************************************************************************************************

    function cubicWeighting(r, d) result(W)

    !==================================================================================================================
    ! Description:
    !! cubicWeighting returns the value of the cubic weighting function.
    !==================================================================================================================          
            
        real :: r
        !! distance 
    
        real :: d 
        !! support size
    
        real :: W 
        !! weighting  function value

        real :: rtilde
        !! scaled distance 
    !------------------------------------------------------------------------------------------------------------------
    
        rtilde = abs(r/d)

        if(rtilde >= 0.0 .and. rtilde <= 0.5) then 
            W = 3.0/4.0 - rtilde**2
        elseif(rtilde >= 0.5 .and. rtilde < 1.5) then 
            W = 0.5*( (9.0/4.0 - 3.0*rtilde + rtilde**2 ) )
        else 
            W = 0.0
        endif              

    end function cubicWeighting

! *********************************************************************************************************************

    function romaWeighting(r, d) result(W)

    !==================================================================================================================
    ! Description:
    !! romaWeighting returns the value of the Roma et al. weighting function.
    !==================================================================================================================          
                    
        real :: r
        !! distance 

        real :: rtilde
        !! scaled distance 
            
        real :: d 
        !! support size
            
        real :: W 
        !! weighting  function value
    !------------------------------------------------------------------------------------------------------------------

        rtilde = abs(r/d)

        if(rtilde >= 0.5 .and. rtilde <= 1.5) then 
            W = (1.0/6.0)*( 5.0 - 3.0*rtilde - sqrt(-3.0*(1.0 - rtilde)**2 + 1.0) )
        elseif(rtilde < 0.5 ) then  
            W = (1.0/3.0)*( 1.0 + sqrt(-3.0*rtilde**2 + 1.0) )
        else
            W = 0.0
        endif             
            
    end function romaWeighting

! *********************************************************************************************************************

    function peskinWeighting(r, d) result(W)

    !==================================================================================================================
    ! Description:
    !! peskinWeighting returns the value of the Peskin weighting function.
    !==================================================================================================================          
                        
        real :: r
        !! distance 
    
        real :: rtilde
        !! scaled distance 
                
        real :: d 
        !! support size
                
        real :: W 
        !! weighting  function value
    !------------------------------------------------------------------------------------------------------------------
    
        rtilde = abs(r/d)
    
        if(rtilde >= 0.0 .and. rtilde < 1.0) then 
            W = (1.0/8.0)*( 3.0 - 2.0*rtilde - sqrt(1.0 + 4.0*rtilde - 4.0*rtilde**2))
        elseif(rtilde >= 1.0 .and. rtilde <= 2.0) then  
            W = (1.0/8.0)*( 5.0 - 2.0*rtilde - sqrt(-7.0 + 12.0*rtilde - 4.0*rtilde**2))
        else
            W = 0.0
        endif             
                
    end function peskinWeighting

! *********************************************************************************************************************

    function Wmat(x, xi, nSupport, d, functionType) result(What)

    !==================================================================================================================
    ! Description:
    !! Wmat returns the diagonal matrix W.
    !==================================================================================================================   

        character(len=*) :: functionType
    !------------------------------------------------------------------------------------------------------------------

        integer :: i 
        !! loop index

        integer :: nSupport 
        !! number of support points
    !------------------------------------------------------------------------------------------------------------------        

        real :: x(3)
        !! point where to evaluate the matrix

        real :: xi(nSupport,3)
        !! interpolation points
    
        real :: W 
        !! weighthing function 

        real :: d 
        !! support size

        real :: r 
        !! distance beween points

        real :: What(nSupport, nSupport)
        !! weighting matrix
    !------------------------------------------------------------------------------------------------------------------

        What = 0.0

        do i=1,nSupport
            r = sqrt( (x(1) - xi(i,1))**2 + (x(2) - xi(i,2))**2 + (x(3) - xi(i,3))**2 )
            W = weightingFunction(r, d, functionType)
            What(i,i) = W
        enddo     

    end function Wmat
! *********************************************************************************************************************

    function Wmat2D(x, xi, nSupport, d, functionType) result(What)

    !==================================================================================================================
    ! Description:
    !! Wmat returns the diagonal matrix W.
    !==================================================================================================================   
    
        character(len=*) :: functionType
    !------------------------------------------------------------------------------------------------------------------
    
        integer :: i 
        !! loop index
    
        integer :: nSupport 
        !! number of support points
    !------------------------------------------------------------------------------------------------------------------        
    
        real :: x(2)
        !! point where to evaluate the matrix
    
        real :: xi(nSupport,2)
        !! interpolation points
        
        real :: W 
        !! weighthing function 
    
        real :: d 
        !! support size
    
        real :: r 
        !! distance beween points
    
        real :: What(nSupport, nSupport)
        !! weighting matrix
    !------------------------------------------------------------------------------------------------------------------
    
        What = 0.0
    
        do i=1,nSupport
            r = sqrt( (x(1) - xi(i,1))**2 + (x(2) - xi(i,2))**2)
            W = weightingFunction(r, d, functionType)
            What(i,i) = W
        enddo     
    
    end function Wmat2D

! *********************************************************************************************************************

    function polynomialBasis(x) result(polyBasis)

    !==================================================================================================================
    ! Description:
    !! polynomialBasis returns the polynomial basis evaluated at x.
    !==================================================================================================================   
  
        real :: x(3)
        !! point where to evaluate the matrix

        real :: polyBasis(1,4)
        !! polynomial basis
    !------------------------------------------------------------------------------------------------------------------
    
        polyBasis(1,1) = 1.0
        polyBasis(1,2) = x(1)
        polyBasis(1,3) = x(2)
        polyBasis(1,4) = x(3)
    
    end function polynomialBasis

! *********************************************************************************************************************

    function polynomialBasis2D(x) result(polyBasis)

    !==================================================================================================================
    ! Description:
    !! polynomialBasis2D returns the polynomial basis evaluated at x.
    !==================================================================================================================   
      
        real :: x(2)
        !! point where to evaluate the matrix
    
        real :: polyBasis(1,3)
        !! polynomial basis
    !------------------------------------------------------------------------------------------------------------------
        
        polyBasis(1,1) = 1.0
        polyBasis(1,2) = x(1)
        polyBasis(1,3) = x(2)
      
    end function polynomialBasis2D    

! *********************************************************************************************************************

    function Pmat(x, xi, nSupport) result(P)

    !==================================================================================================================
    ! Description:
    !! Pmat returns the polynomial basis matrix evaluated at x.
    !==================================================================================================================   
      
        integer :: i 
        !! loop index

        integer :: nSupport 
        !! number of support points
    !------------------------------------------------------------------------------------------------------------------        

        real :: x(3)
        !! point where to evaluate the matrix

        real :: xi(nSupport,3)
        !! interpolation points

        real :: basis(1,4)
        !! polynomial basis at the point x

        real :: P(nSupport, 4)
        !! P matrix
    !------------------------------------------------------------------------------------------------------------------

        do i=1,nSupport
            basis = polynomialBasis(xi(i,:))
            P(i,:) = basis(1,:)
        enddo      

    end function Pmat
! *********************************************************************************************************************

    function Pmat2D(x, xi, nSupport) result(P)
 
    !==================================================================================================================
    ! Description:
    !! Pmat2D returns the polynomial basis matrix evaluated at x.
    !==================================================================================================================   
          
        integer :: i 
        !! loop index
    
        integer :: nSupport 
        !! number of support points
    !------------------------------------------------------------------------------------------------------------------        
    
        real :: x(2)
        !! point where to evaluate the matrix
    
        real :: xi(nSupport,2)
        !! interpolation points
    
        real :: basis(1,3)
        !! polynomial basis at the point x
    
        real :: P(nSupport, 3)
        !! P matrix
    !------------------------------------------------------------------------------------------------------------------
    
        do i=1,nSupport
            basis = polynomialBasis2D(xi(i,:))
            P(i,:) = basis(1,:)
        enddo      
    
    end function Pmat2D

! *********************************************************************************************************************

    function Bmat(x, xi, nSupport, d, functionType) result(B)

    !==================================================================================================================
    ! Description:
    !! Bmat returns the MLS RHS (P^T * What).
    !==================================================================================================================   
          
        character(len=*) :: functionType
    !------------------------------------------------------------------------------------------------------------------

        integer :: i 
        !! loop index

        integer :: nSupport 
        !! number of support points
    !------------------------------------------------------------------------------------------------------------------        

        real :: x(3)
        !! point where to evaluate the matrix

        real :: xi(nSupport,3)
        !! interpolation points

        real :: d 
        !! support size

        real :: What(nSupport, nSupport)
        !! weighting matrix 

        real :: P(nSupport, 4) 
        !! polynomial basis matrix

        real :: B(4,nSupport)
    !------------------------------------------------------------------------------------------------------------------
    
        P = Pmat(x, xi, nSupport)
        What = Wmat(x, xi, nSupport, d, functionType)
        B = MatMul(transpose(P), What)
    
    end function Bmat

! *********************************************************************************************************************

    function Bmat2D(x, xi, nSupport, d, functionType) result(B)

    !==================================================================================================================
    ! Description:
    !! Bmat returns the MLS RHS (P^T * What).
    !==================================================================================================================   
              
        Character(len=*) :: functionType
    !------------------------------------------------------------------------------------------------------------------
    
        integer :: i 
        !! loop index
    
        integer :: nSupport 
        !! number of support points
    !------------------------------------------------------------------------------------------------------------------        
    
        real :: x(2)
        !! point where to evaluate the matrix
    
        real :: xi(nSupport,2)
        !! interpolation points
    
        real :: d 
        !! support size
    
        real :: What(nSupport, nSupport)
        !! weighting matrix 
    
        real :: P(nSupport, 3) 
        !! polynomial basis matrix
    
        real :: B(4,nSupport)
    !------------------------------------------------------------------------------------------------------------------
        
        P = Pmat2D(x, xi, nSupport)
        What = Wmat2D(x, xi, nSupport, d, functionType)
        B = MatMul(transpose(P), What)
        
    end function Bmat2D
        
! *********************************************************************************************************************

    function Amat(x, xi, nSupport, d, functionType) result(A)

    !==================================================================================================================
    ! Description:
    !! Amat returns the MLS matrix A (P^T * What * P).
    !==================================================================================================================   
              
        character(len=*) :: functionType
    !------------------------------------------------------------------------------------------------------------------

        integer :: nSupport
    !------------------------------------------------------------------------------------------------------------------

        real :: x(3)
        !! point where to evaluate the matrix
    
        real :: xi(nSupport,3)
        !! interpolation points
    
        real :: d 
        !! support size
    
        real :: What(nSupport, nSupport)
        !! weighting matrix 
    
        real :: P(nSupport, 4) 
        !! polynomial basis matrix
    
        real :: B(4,nSupport)

        real :: A(4,4)
        !! MLS matrix
    !------------------------------------------------------------------------------------------------------------------
        
        P = Pmat(x, xi, nSupport)
        What = Wmat(x, xi, nSupport, d, functionType)
        B = MatMul(transpose(P), What)
        A = MatMul(B, P)

    end function Amat
! *********************************************************************************************************************

    function Amat2D(x, xi, nSupport, d, functionType) result(A)

    !==================================================================================================================
    ! Description:
    !! Amat returns the MLS matrix A (P^T * What * P).
    !==================================================================================================================   
                  
        character(len=*) :: functionType
    !------------------------------------------------------------------------------------------------------------------
    
        integer :: nSupport
    !------------------------------------------------------------------------------------------------------------------
    
        real :: x(2)
        !! point where to evaluate the matrix
        
        real :: xi(nSupport,2)
        !! interpolation points
        
        real :: d 
        !! support size
        
        real :: What(nSupport, nSupport)
        !! weighting matrix 
        
        real :: P(nSupport, 3) 
        !! polynomial basis matrix
        
        real :: B(3,nSupport)
    
        real :: A(3,3)
        !! MLS matrix
    !------------------------------------------------------------------------------------------------------------------
            
        P = Pmat2D(x, xi, nSupport)
        What = Wmat2D(x, xi, nSupport, d, functionType)
        B = MatMul(transpose(P), What)
        A = MatMul(B, P)
    
    end function Amat2D
                    
! *********************************************************************************************************************

    function transferFunction(x, xi, nSupport, d, functionType) result(Phi)

    !==================================================================================================================
    ! Description:
    !! transferFunction returns the MLS transfer function Phi^T (p^T * inv(A) * B).
    !==================================================================================================================   
                  
        character(len=*) :: functionType
    !------------------------------------------------------------------------------------------------------------------
       
        integer :: nSupport, i, j
    !------------------------------------------------------------------------------------------------------------------

        real :: x(3)
        !! point where to evaluate the matrix
        
         real :: xi(nSupport,3)
        !! interpolation points
        
        real :: d 
        !! support size
        
        real :: polyBasis(1,4)
        !! polynomial bases at the point x

        real :: What(nSupport, nSupport)
        !! weighting matrix 
        
        real :: P(nSupport, 4) 
        !! polynomial basis matrix
        
        real :: B(4,nSupport)
        !! MLS RHS
    
        real :: A(4,4)
        !! MLS matrix

        real :: invA(4,4)
        !! MLS matrix inverse

        real :: pinvA(1,4)
        !! polynomial bases at the point x

        real :: Phi(1, nSupport)
        !! transfer function
    !------------------------------------------------------------------------------------------------------------------
        
        !Compute P
        polyBasis = polynomialBasis(x)
        P = Pmat(x, xi, nSupport)

        ! Compute What, likely better to simply multiply the diagonal
        What = Wmat(x, xi, nSupport, d, functionType)
        B = MatMul(transpose(P), What)

        ! Compute mls A matrix
        A = MatMul(B, P)

        ! Invert A, if the points are not enough, the system can be singular
        invA = matinv4(A)

        ! Compute the transfter funtion
        pinvA = MatMul(polyBasis, invA)
        Phi = MatMul(pinvA, B)

        if( abs(sum(Phi(1,:)) -1.0) > 5.0e-2 ) then 

            write(*,*) 'ERROR: The transfer function sum must satisfy the partition of unity, MLS matrix might be ill-conditioned! Percentual error  = ', 100.*abs(sum(Phi(1,:)) -1.0)
            write(*,*) 'Try to change the window function or check if the lagrangian point has enough support points (9 in 2D, 27 in 3D). Few support points means a mesh too strecthed and not enough support points can be found with the current dilation factor.'
            write(*,*) 'xlag = ', x, 'grid points within the support: ', nSupport
            write(*,*) 'dilatation factor = ', d
            write(*,*) 'functionType = ', functionType
   
            do i=1,nSupport
                write(*,*) 'xi = ', xi(i,1:3)
            enddo
            
            do i=1,nSupport
                write(*,*) 'What = ', What(i,i)
            enddo

            do i=1,4
                write(*,*) 'A = ', A(i,:)
            enddo
  
            stop

        endif 
        
    end function transferFunction
                    
! *********************************************************************************************************************

    function transferFunction2D(x, xi, nSupport, d, functionType) result(Phi)

    !==================================================================================================================
    ! Description:
    !! transferFunction returns the MLS transfer function Phi^T (p^T * inv(A) * B).
    !==================================================================================================================   
                      
        character(len=*) :: functionType
    !------------------------------------------------------------------------------------------------------------------
           
        integer :: nSupport, i, j
    !------------------------------------------------------------------------------------------------------------------
    
        real :: x(2)
        !! point where to evaluate the matrix
            
        real :: xi(nSupport,2)
        !! interpolation points
            
        real :: d 
        !! support size
            
        real :: polyBasis(1,3)
        !! polynomial bases at the point x
    
        real :: What(nSupport, nSupport)
        !! weighting matrix 
            
        real :: P(nSupport, 3) 
        !! polynomial basis matrix
            
        real :: B(3,nSupport)
        !! MLS RHS
        
        real :: A(3,3)
        !! MLS matrix
    
        real :: invA(3,3)
        !! MLS matrix inverse
    
        real :: pinvA(1,3)
        !! polynomial bases at the point x
    
        real :: Phi(1, nSupport)
        !! transfer function
    !------------------------------------------------------------------------------------------------------------------
        
        !Compute P
        polyBasis = polynomialBasis2D(x)
        P = Pmat2D(x, xi, nSupport)
    
        ! Compute What, likely better to simply multiply the diagonal
        What = Wmat2D(x, xi, nSupport, d, functionType)
        B = MatMul(transpose(P), What)
    
        ! Compute mls A matrix
        A = MatMul(B, P)

        ! Invert A, if the points are not enough, the system can be singular
        invA = matinv3(A)
          
        ! Compute the transfter funtion
        pinvA = MatMul(polyBasis, invA)
        Phi = MatMul(pinvA, B)

        if( abs(sum(Phi(1,:)) -1.0) > 5.0e-2 ) then 

            write(*,*) 'ERROR: The transfer function sum must satisfy the partition of unity, MLS matrix might be ill-conditioned! Percentual error  = ', 100.*abs(sum(Phi(1,:)) -1.0)
            write(*,*) 'Try to change the window function or check if the lagrangian point has enough support points (9 in 2D, 27 in 3D). Few support points means a mesh too strecthed and not enough support points can be found with the current dilation factor.'
            write(*,*) 'xlag = ', x, 'grid points within the support: ', nSupport
            write(*,*) 'dilatation factor = ', d
            write(*,*) 'functionType = ', functionType
   
            do i=1,nSupport
                write(*,*) 'xi = ', xi(i,1:2)
            enddo
            
            do i=1,nSupport
                write(*,*) 'What = ', What(i,i)
            enddo

            do i=1,3
                write(*,*) 'A = ', A(i,:)
            enddo
  
            stop

        endif 

    end function transferFunction2D

! *********************************************************************************************************************

    function mlsInterp(f, x, xi, nSupport, d, functionType) result(fi)

    !==================================================================================================================
    ! Description:
    !! mlsInterpolation returns the interpolated value at the point xi.
    !==================================================================================================================   
                      
        character(len=*) :: functionType
    !------------------------------------------------------------------------------------------------------------------

        integer :: i
        !! loop index

        integer :: nSupport
        !! number of support points
    !------------------------------------------------------------------------------------------------------------------

        real :: x(3)
        !! point where to evaluate the matrix
                
        real :: xi(nSupport,3)
        !! interpolation points
            
        real :: d 
        !! support size
    
        real :: Phi(1, nSupport)
        !! transfer function

        real :: f(nSupport,1)
        !! function at the support point

        real :: fi(1,1)
        !! interpolated value at x 
    !------------------------------------------------------------------------------------------------------------------

        ! Compute the transfer function
        Phi = transferFunction(x, xi, nSupport, d, functionType)

        ! Interpolatethe point
        fi = MatMul(Phi, f) 
        
    end function mlsInterp

! *********************************************************************************************************************

end module mlsInterpolation    