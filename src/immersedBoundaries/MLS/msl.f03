!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module immersedBoudaryMLS

    use flubioContainters
    use generalDictionaries
    use bodies
    use fieldvar, only: velocity
    use momentum

    implicit none

    type, extends(generalDict) :: ibmDict

        logical :: found
        !! True if ibm dictionary exists in settings

        character(len=:), allocatable :: functionType
        !! function to use for as window function

        integer :: writeForcesEvery
        !! write ibm body forces every N steps

        real :: lambdas
        !! dilatation factor

    contains
        procedure :: set => setIBM

    end type ibmDict


    type, public :: ibm_mls

        type(flubioField) :: fibm
        !! Immersed bounary force field

        type(flubioField) :: fibmGhost
        !! Immersed bounary force field at ghost cells only

        type(ibmDict) :: ibmDictionary

        character(len=:), dimension(:), allocatable :: bodyName
        !! list collecting the file names

        integer :: numberOfBodies2D
        !! number of bodies, if any

        integer :: numberOfBodies3D
        !! number of bodies, if any

        type(body2D), dimension(:), allocatable :: ibmBody2D
        !! two dimensional objects defined by a collection of points

        type(body3D), dimension(:), allocatable :: ibmBody3D
        !! Three dimensional objects defined by stl surfaces

        logical :: found2D
        !! True if 2D bodies exist

        logical :: found3D
        !! True if 3D bodies exist

    contains
         procedure :: setupImmersedBoundaryMethod
         procedure :: sumGhostContribution
         procedure :: immersedBoundaryForcing
         procedure :: applyForces
         procedure :: resetForces
         procedure :: writeBodyForces
         procedure :: writeBodyStates
         procedure :: moveBodies
         procedure :: writeBodyPosition
    end type ibm_mls

contains

    subroutine setIBM(this)

    !===================================================================================================================
    ! Description:
    !! setIBM parses settings/ibm dictionary.
    !==================================================================================================================

        class(ibmDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName
        !! Dictionary name
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        dictName = 'settings/ibm'

        ! Initialise the JSON dictionary
        inquire(file=trim(dictName), exist=found)

        if (.not. found) then
            this%found = .false.
        else
            this%found = .true.
            call this%initJSON(dictName)
        end if

        call this%json%get('dilatationFactor', this%lambdas, found)
        if(.not. found) then

            if(bdim==0) then
                ! 3D
                this%lambdas = 1.75
            else
                !2D
                this%lambdas = 1.5
            endif
        endif

        call this%json%get('weigthingFuntion', this%functionType, found)
        if(.not. found) this%functionType = 'exponential'

        call this%json%get('writeForcesEvery', this%writeForcesEvery, found)
        if(.not. found) this%writeForcesEvery = 3

    end subroutine setIBM

!**********************************************************************************************************************

    subroutine setupImmersedBoundaryMethod(this)

    !===================================================================================================================
    ! Description:
    !! setupImmersedBoundaryMethod sets up the data structure for the immersed boundary method.
    !==================================================================================================================

        class(ibm_mls) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable  :: path
        !! file path

        character(len=50), dimension(:), allocatable  :: splitPath
        !! dictionary path split in at %

        character(len=:), dimension(:), allocatable  :: tmpcvec
        !! tmp vector to parse the dictionary
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBody, b
        !! loop index

        integer, dimension(:),allocatable :: ilen
        !! first array element length
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, dictPointer2D, dictPointer3D, Pointer2D, Pointer3D, bodyPointer
    !------------------------------------------------------------------------------------------------------------------

        real, dimension(:), allocatable :: tmpvec
    !------------------------------------------------------------------------------------------------------------------

        logical :: found2D, found3D, found, nameFound
    !------------------------------------------------------------------------------------------------------------------

        ! Read IBM dictionary
        call this%ibmDictionary%set()

        !------------------------------------!
        ! Get the number of 2D and 3D bodies !
        !------------------------------------!

        ! Get the pointer to the dictionary
        call this%ibmDictionary%json%get(dictPointer)

        ! Initialise force field
        call this%fibm%createWorkField(fieldName='fibm', classType='scalar', nComp=3)
        call this%fibmGhost%createWorkField(fieldName='fibmGhost', classType='scalar', nComp=3)

        ! Initialise json factory
        call jcore%initialize()

        call jcore%get(dictPointer, '2D', Pointer2D, this%found2D)
        call jcore%get(dictPointer, '3D', Pointer3D, this%found3D)

        ! Create the field with volume cells
        call volumeField%createWorkField(fieldName='cellVolume', classType='scalar', nComp=1)
        volumeField%phi(1:numberOfElements,1) = mesh%volume(1:numberOfElements)
        call volumeField%updateGhosts()

        ! Setup IBM data
        if(bdim==0) then

            call this%ibmDictionary%json%info('3D', n_children=this%numberOfBodies3D)
            if(.not. this%found3D .or. this%numberOfBodies3D < 1) call flubioStopMsg('ERROR: No 3D bodies found, please set them in settings/ibm')

            allocate(this%ibmBody3D(this%numberOfBodies3D))

            do b=1,this%numberOfBodies3D


                call jcore%get_child(Pointer3D, b, bodyPointer, found)
                call jcore%get_path(bodyPointer, path, found)
                if(found) then
                    call split_in_array(path, splitPath, '.')
                    this%ibmBody3D(b)%name = splitPath(2)
                endif

                ! File name
                call jcore%get(bodyPointer, 'file', this%ibmBody3D(b)%fileName, nameFound)

                ! Motion options
                call jcore%get(bodyPointer, 'motionType', this%ibmBody3D(b)%motionType, found)
                if(.not. found) this%ibmBody3D(b)%motionType = 'fixed'

                ! Parse the state vector
                if(lowercase(this%ibmBody3D(b)%motionType)=='prescribed') then

                    call this%ibmBody3D(b)%motionExpression%initToZero()

                    call jcore%get(bodyPointer, 'motion.x', tmpcvec, ilen, found)
                    this%ibmBody3D(b)%motionExpression%x(1) = tmpcvec(1)
                    this%ibmBody3D(b)%motionExpression%x(2) = tmpcvec(2)
                    this%ibmBody3D(b)%motionExpression%x(3) = tmpcvec(3)
                    if(.not. found) call flubioStopMsg('ERROR: please specify the position expression x(t)')

                    call jcore%get(bodyPointer, 'motion.xDot', tmpcvec, ilen, found)
                    this%ibmBody3D(b)%motionExpression%xDot(1) = tmpcvec(1)
                    this%ibmBody3D(b)%motionExpression%xDot(2) = tmpcvec(2)
                    this%ibmBody3D(b)%motionExpression%xDot(3) = tmpcvec(3)
                    if(.not. found) call flubioStopMsg('ERROR: please specify the velocity expression xDot(t)')

                    call jcore%get(bodyPointer, 'motion.omega', tmpcvec, ilen, found)
                    this%ibmBody3D(b)%motionExpression%omega(1) = tmpcvec(1)
                    this%ibmBody3D(b)%motionExpression%omega(2) = tmpcvec(2)
                    this%ibmBody3D(b)%motionExpression%omega(3) = tmpcvec(3)
                    if(.not. found) call flubioStopMsg('ERROR: please specify the angular velocity expression omega(t)')

                    call jcore%get(bodyPointer, 'motion.quaternions', tmpcvec, ilen, found)
                    this%ibmBody3D(b)%motionExpression%quaternions(1) = tmpcvec(1)
                    this%ibmBody3D(b)%motionExpression%quaternions(2) = tmpcvec(2)
                    this%ibmBody3D(b)%motionExpression%quaternions(3) = tmpcvec(3)
                    this%ibmBody3D(b)%motionExpression%quaternions(4) = tmpcvec(4)
                    if(.not. found) call flubioStopMsg('ERROR: please specify the quaternion expression q(t), put ["0.0", "0.0", "0.0", "0.0"] if using euler angles.')

                    call jcore%get(bodyPointer, 'motion.eulerAngles', tmpcvec, ilen, found)
                    this%ibmBody3D(b)%motionExpression%eulerAngles(1) = tmpcvec(1)
                    this%ibmBody3D(b)%motionExpression%eulerAngles(2) = tmpcvec(2)
                    this%ibmBody3D(b)%motionExpression%eulerAngles(3) = tmpcvec(3)
                    if(.not. found) call flubioStopMsg('ERROR: please specify the euler angles expression eu(t).')

                elseif(lowercase(this%ibmBody3D(b)%motionType)=='prescribedposition' .or. lowercase(this%ibmBody3D(b)%motionType)=='rotationaroundaxis') then

                    call this%ibmBody3D(b)%motionExpression%initToZero()

                    call jcore%get(bodyPointer, 'motion.x', tmpcvec, ilen, found)
                    this%ibmBody3D(b)%motionExpression%x(1) = tmpcvec(1)
                    this%ibmBody3D(b)%motionExpression%x(2) = tmpcvec(2)
                    this%ibmBody3D(b)%motionExpression%x(3) = tmpcvec(3)
                    if(.not. found) call flubioStopMsg('ERROR: please specify the position expression x(t)')

                    call jcore%get(bodyPointer, 'motion.eulerAngles', tmpcvec, ilen, found)
                    this%ibmBody3D(b)%motionExpression%eulerAngles(1) = tmpcvec(1)
                    this%ibmBody3D(b)%motionExpression%eulerAngles(2) = tmpcvec(2)
                    this%ibmBody3D(b)%motionExpression%eulerAngles(3) = tmpcvec(3)
                    if(.not. found) call flubioStopMsg('ERROR: please specify the euler angles expression eu(t).')

                endif

                call jcore%get(bodyPointer, 'CoR', tmpvec, found)
                if(.not. found) then
                    ! Load STL
                    call this%ibmBody3D(b)%initialiseBody3D()
                    this%ibmBody3D(b)%xcor = this%ibmBody3D(b)%computeSTLCentroid()
                else
                    this%ibmBody3D(b)%xcor = tmpvec
                    ! Load STL
                    call this%ibmBody3D(b)%initialiseBody3D()
                endif

            enddo

        else

            call this%ibmDictionary%json%info('2D', n_children=this%numberOfBodies2D)

            if(.not. this%found2D .and. this%numberOfBodies2D < 1) call flubioStopMsg('ERROR: No 2D bodies found, please set them in settings/ibm')

            ! Parse info from dictionary
            allocate(this%ibmBody2D(this%numberOfBodies2D))

            do b=1, this%numberOfBodies2D

                call jcore%get_child(Pointer2D, b, bodyPointer, found)
                call jcore%get_path(bodyPointer, path, found)
                if(found) then
                    call split_in_array(path, splitPath, '.')
                    this%ibmBody2D(b)%name = splitPath(2)
                endif

                call jcore%get(bodyPointer, 'file', this%ibmBody2D(b)%fileName, nameFound)

                ! Motion options
                call jcore%get(bodyPointer, 'motionType', this%ibmBody2D(b)%motionType, found)
                if(.not. found) this%ibmBody2D(b)%motionType = 'fixed'

                ! Parse the state vector
                if(this%ibmBody2D(b)%motionType=='prescribed') then

                    call this%ibmBody2D(b)%motionExpression%initToZero()

                    call jcore%get(bodyPointer, 'motion.x', tmpcvec, ilen, found)
                    this%ibmBody2D(b)%motionExpression%x(1) = tmpcvec(1)
                    this%ibmBody2D(b)%motionExpression%x(2) = tmpcvec(2)
                    this%ibmBody2D(b)%motionExpression%x(3) = tmpcvec(3)
                    if(.not. found) call flubioStopMsg('ERROR: please specify the position expression x(t)')

                    call jcore%get(bodyPointer, 'motion.xDot', tmpcvec, ilen, found)
                    this%ibmBody2D(b)%motionExpression%xDot(1) = tmpcvec(1)
                    this%ibmBody2D(b)%motionExpression%xDot(2) = tmpcvec(2)
                    this%ibmBody2D(b)%motionExpression%xDot(3) = tmpcvec(3)
                    if(.not. found) call flubioStopMsg('ERROR: please specify the velocity expression xDot(t)')

                    call jcore%get(bodyPointer, 'motion.omega', tmpcvec, ilen, found)
                    this%ibmBody2D(b)%motionExpression%omega(1) = tmpcvec(1)
                    this%ibmBody2D(b)%motionExpression%omega(2) = tmpcvec(2)
                    this%ibmBody2D(b)%motionExpression%omega(3) = tmpcvec(3)
                    if(.not. found) call flubioStopMsg('ERROR: please specify the angular velocity expression omega(t)')

                    call jcore%get(bodyPointer, 'motion.quaternions', tmpcvec, ilen, found)
                    this%ibmBody2D(b)%motionExpression%quaternions(1) = tmpcvec(1)
                    this%ibmBody2D(b)%motionExpression%quaternions(2) = tmpcvec(2)
                    this%ibmBody2D(b)%motionExpression%quaternions(3) = tmpcvec(3)
                    this%ibmBody2D(b)%motionExpression%quaternions(4) = tmpcvec(4)
                    if(.not. found) call flubioStopMsg('ERROR: please specify the quaternion expression q(t), put ["0.0", "0.0", "0.0", "0.0"] if using euler angles.')

                    call jcore%get(bodyPointer, 'motion.eulerAngles', tmpcvec, ilen, found)
                    this%ibmBody2D(b)%motionExpression%eulerAngles(1) = tmpcvec(1)
                    this%ibmBody2D(b)%motionExpression%eulerAngles(2) = tmpcvec(2)
                    this%ibmBody2D(b)%motionExpression%eulerAngles(3) = tmpcvec(3)
                    if(.not. found) call flubioStopMsg('ERROR: please specify the euler angles expression eu(t).')

                elseif(lowercase(this%ibmBody2D(b)%motionType)=='prescribedposition' .or. lowercase(this%ibmBody2D(b)%motionType)=='rotationaroundaxis') then

                    call this%ibmBody2D(b)%motionExpression%initToZero()

                    call jcore%get(bodyPointer, 'motion.x', tmpcvec, ilen, found)
                    this%ibmBody2D(b)%motionExpression%x(1) = tmpcvec(1)
                    this%ibmBody2D(b)%motionExpression%x(2) = tmpcvec(2)
                    this%ibmBody2D(b)%motionExpression%x(3) = tmpcvec(3)
                    if(.not. found) call flubioStopMsg('ERROR: please specify the position expression x(t)')

                    call jcore%get(bodyPointer, 'motion.eulerAngles', tmpcvec, ilen, found)
                    this%ibmBody2D(b)%motionExpression%eulerAngles(1) = tmpcvec(1)
                    this%ibmBody2D(b)%motionExpression%eulerAngles(2) = tmpcvec(2)
                    this%ibmBody2D(b)%motionExpression%eulerAngles(3) = tmpcvec(3)
                    if(.not. found) call flubioStopMsg('ERROR: please specify the euler angles expression eu(t).')

                endif

                ! Load lagrangian points from file
                call this%ibmBody2D(b)%loadPoints()

                call jcore%get(bodyPointer, 'CoR', tmpvec, found)
                if(.not. found) then
                    ! Load lagrangian points from file
                    call this%ibmBody2D(b)%loadPoints()
                    this%ibmBody2D(b)%xcor = this%ibmBody2D(b)%computePointsCentroid()
                else
                    this%ibmBody2D(b)%xcor = tmpvec
                    ! Load lagrangian points from file
                    call this%ibmBody2D(b)%loadPoints()
                endif

            enddo

        endif

        ! Clean
        nullify(dictPointer)
        if(this%found2D) nullify(Pointer2D)
        if(this%found3D) nullify(Pointer3D)
        if(nameFound) nullify(bodyPointer)
        call jcore%destroy()

    end subroutine setupImmersedBoundaryMethod

!**********************************************************************************************************************

    subroutine sumGhostContribution(this)

    !==================================================================================================================
    ! Description:
    !! sumGhostContribution adds the eulerian forces contribution oming from the neihgbour partition.
    !==================================================================================================================

        class(ibm_mls) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBFace, iOwner, iBoundary, iProc, is, ie, pNeigh, patchFace
    !------------------------------------------------------------------------------------------------------------------

        ! Update Ghosts
        call this%fibmGhost%updateGhosts()

        ! Sum up ghost contribution to the owner processor face cell
        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1
            patchFace = numberOfElements + (is-numberOfIntFaces-1)
            pNeigh = 0

            do iBFace=is,ie
                pNeigh = pNeigh + 1
                iOwner = mesh%owner(iBFace)
                this%fibm%phi(iOwner,:) = this%fibm%phi(iOwner,:) + this%fibmGhost%phi(patchFace+pNeigh,:)
            enddo

        enddo

    end subroutine sumGhostContribution

!**********************************************************************************************************************

    subroutine immersedBoundaryForcing(this)

    !==================================================================================================================
    ! Description:
    !! immersedBoundaryForcing wraps the immersed boundary forces calculation
    !! (support search, lagrangian points ownership, eulerian forces).
    !==================================================================================================================

        class(ibm_mls) :: this
    !--------------------------------------------------------------------------------------------------------------

        integer :: iBody, iElement
    !--------------------------------------------------------------------------------------------------------------

        ! Reset IBM forces to zero
        call this%resetForces()

        ! 3D bodies
        do iBody=1,this%numberOfBodies3D

            ! Find facet owership
            call this%ibmBody3D(iBody)%findOwnership()

            ! Set lagrangian points coordinates
            call this%ibmBody3D(iBody)%setLagrangianPointCoordinates(this%ibmDictionary%lambdas, this%ibmDictionary%functionType)

            ! Find lagrangian point support cells
            call this%ibmBody3D(iBody)%findSupportCells()

            ! Update body velocity
            !call this%ibmBody3D(iBody)%updateBodyVelocity()

            ! Compute ibm forces
            !call this%ibmBody3D(iBody)%eulerianForces(u=velocity, ulag=this%ibmBody3D(iBody)%ulag, &
            !                                        f=this%fibm, fGhost=this%fibmGhost, functionType=this%ibmDictionary%functionType)

        enddo

        ! 2D bodies
        do iBody=1,this%numberOfBodies2D

            ! Find point ownership
            call this%ibmBody2D(iBody)%findOwnership()

            ! Set lagrangian points coordinates
            call this%ibmBody2D(iBody)%setLagrangianPointCoordinates2D(this%ibmDictionary%lambdas, this%ibmDictionary%functionType)

            ! Find lagrangian point support cells
            call this%ibmBody2D(iBody)%findSupportCells()

            call this%ibmBody2D(iBody)%updateBodyVelocity()

            ! Compute ibm forces
            call this%ibmBody2D(iBody)%eulerianForces(velocity, this%ibmBody2D(iBody)%ulag, &
                                                      this%fibm, this%fibmGhost, this%ibmDictionary%functionType)
        enddo

        ! Sum up force contributions coming from neighbour boundaries
        call this%sumGhostContribution()

    end subroutine immersedBoundaryForcing

!**********************************************************************************************************************

    subroutine applyForces(this)

    !==================================================================================================================
    ! Description:
    !! applyForces adds the eulerian forces to the momentum rhs.
    !==================================================================================================================

        class(ibm_mls) :: this
    !--------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !--------------------------------------------------------------------------------------------------------------

        if(lower(Ueqn%timeOptName)=='soue' .or. lower(Ueqn%timeOptName)=='impliciteuler') then

            ! Solve UEqn again with forces
            call momentumEquationIBM(this%fibm%phi(:,1:3))

        else

            ! Update velocity Field with body forces
            do iElement=1,numberOfElements
                velocity%phi(iElement,1:3-bdim) = velocity%phi(iElement,1:3-bdim) + dtime*this%fibm%phi(iElement,1:3-bdim)
            enddo

            call velocity%updateGhosts()
            call velocity%updateBoundaryVelocityField()

        endif

    end subroutine applyForces

!**********************************************************************************************************************

    subroutine resetForces(this)

    !==================================================================================================================
    ! Description:
    !! resetForces resets immersed boundary forces.
    !==================================================================================================================

        class(ibm_mls) :: this
    !--------------------------------------------------------------------------------------------------------------

        this%fibm%phi = 0.0
        this%fibmGhost%phi = 0.0

    end subroutine resetForces

!*****************************************************************************************************************writeForcesEvery*****

    subroutine writeBodyForces(this)

    !==================================================================================================================
    ! Description:
    !! writeBodyForces writes forces to file.
    !==================================================================================================================

        class(ibm_mls) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBody
    !------------------------------------------------------------------------------------------------------------------

        if(mod(itime, this%ibmDictionary%writeForcesEvery)==0 .or. itime==1) then

            do iBody=1,this%numberOfBodies2D
                call this%ibmBody2D(iBody)%writeForceToFile()
            enddo

            do iBody=1,this%numberOfBodies3D
                call this%ibmBody3D(iBody)%writeForceToFile()
            enddo

        endif

    end subroutine writeBodyForces

!**********************************************************************************************************************

    subroutine writeBodyStates(this)

    !==================================================================================================================
    ! Description:
    !! writeBodyStates writes states to file.
    !==================================================================================================================

        class(ibm_mls) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBody
    !------------------------------------------------------------------------------------------------------------------

        if(mod(itime, this%ibmDictionary%writeForcesEvery)==0 .or. itime==1) then

            do iBody=1,this%numberOfBodies2D
                call this%ibmBody2D(iBody)%writeStateToFile()
            enddo

            do iBody=1,this%numberOfBodies3D
                call this%ibmBody3D(iBody)%writeStateToFile()
            enddo

        endif

    end subroutine writeBodyStates

!**********************************************************************************************************************

    subroutine moveBodies(this)

    !==================================================================================================================
    ! Description:
    !! writeBodyForces writes forces to file.
    !==================================================================================================================

        class(ibm_mls) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBody
    !------------------------------------------------------------------------------------------------------------------

        do iBody=1,this%numberOfBodies2D
            call this%ibmBody2D(iBody)%moveBody()
        enddo

        do iBody=1,this%numberOfBodies3D
            call this%ibmBody3D(iBody)%moveBody()
        enddo

    end subroutine moveBodies

!**********************************************************************************************************************

    subroutine writeBodyPosition(this)

    !==================================================================================================================
    ! Description:
    !! writeBodyPosition writes body position to file.
    !==================================================================================================================

        class(ibm_mls) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=20) :: fieldName(1)
    !------------------------------------------------------------------------------------------------------------------

            integer :: iBody
    !------------------------------------------------------------------------------------------------------------------

            real, dimension(:,:), allocatable :: surfaceField
    !------------------------------------------------------------------------------------------------------------------

        do iBody=1,this%numberOfBodies2D
            call this%ibmBody2D(iBody)%writeBodyPoints()
        enddo

         do iBody=1,this%numberOfBodies3D

            ! Write the decomposition
            fieldName = "facet_ownership"
            allocate(surfaceField(this%ibmBody3D(iBody)%numberOfLagrangianPoints,1))
            surfaceField(:,1) = real(id)
            call this%ibmBody3D(iBody)%writeFieldsOnBody(surfaceFields=surfaceField, fieldNames=fieldName, nFields=1)

            deallocate(surfaceField)

        enddo

    end subroutine writeBodyPosition

!**********************************************************************************************************************

end module immersedBoudaryMLS