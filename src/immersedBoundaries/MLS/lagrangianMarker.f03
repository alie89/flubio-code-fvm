module lagrangianMarker

    !use m_strings, only : value_to_string
    use flubioMpi
    use meshvar
    use flubioFields

    implicit none

    type, public :: lagrangianPoint

        character(len=:), allocatable :: functionType
        !! weigthing function type

        integer :: numberOfSupportCells
        !! number of support cells

        integer :: nearestCell
        !! nearest cell to the lagrangian point

        integer, dimension(:), allocatable :: support
        !! list of support cells

        real, dimension(:,:), allocatable :: Phi
        !! transfer function for the mls interpolation.

        real :: x(3)
        !! lagrangian point coordinates

        real :: area
        !! triangular area

        real :: lambdas
        !! dilatation coefficient

    contains
        procedure :: setLagrangianPoint
        procedure :: findNearestCell
        procedure :: findSupport
        procedure :: checkCellIndex
        procedure :: getVolumeFromIndex
        procedure :: getCentroidFromIndex
        procedure :: getFieldValueFromIndex
        procedure :: getFaceFromIndex
        procedure :: getInfluenceRadius
        procedure :: maxDistance
        procedure :: removeDuplicatedCells
        procedure :: computeTransferFunction
        procedure :: interpolateFieldOnPoint
        procedure :: interpolateFieldOnPoint2D
        procedure :: hl
        procedure :: computeCl
        procedure :: eulerianForce
    end type lagrangianPoint

    type(flubioField) :: volumeField
    !! field known with the IBM context storing the cell volumes

contains

    subroutine setLagrangianPoint(this, x, area)

    !==================================================================================================================
    ! Description:
    !! setLagrangianPoint set the position of the lagrangian point
    !==================================================================================================================

        class(lagrangianPoint) :: this
    !------------------------------------------------------------------------------------------------------------------

        real :: area

        real :: x(3)
    !------------------------------------------------------------------------------------------------------------------

        this%x = x
        this%area = area

    end subroutine setLagrangianPoint

!**********************************************************************************************************************

    subroutine findNearestCell(this, ncells, radius)

    !==================================================================================================================
    ! Description:
    !! findNearestCell finds out the cell hosting the lagrangian point.
    !==================================================================================================================

        class(lagrangianPoint) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(dArgDynamicArray) :: nearestNeighbour

        type(KdTreeSearch) :: search

        type(kdtree2_result), dimension(:), allocatable :: nearestNeighbourMK
    !------------------------------------------------------------------------------------------------------------------

        character(len=25) :: s(3)
    !------------------------------------------------------------------------------------------------------------------

        integer :: n, ncells, nFound, nSearch, nn(ncells)
    !------------------------------------------------------------------------------------------------------------------

        real, optional :: radius
    !------------------------------------------------------------------------------------------------------------------

        logical :: is_valid, nearest_found
    !------------------------------------------------------------------------------------------------------------------

        if(flubioOptions%KDtreeType==0) then

            if(present(radius)) then
            ! Look only nearby the point
                nearestNeighbour = search%kNearest(mesh%tree, &
                x=mesh%centroid(1,:), y=mesh%centroid(2,:), z=mesh%centroid(3,:), &
                xquery=this%x(1), yquery=this%x(2), zquery=this%x(3), k=ncells, radius=radius)

                ! Override ncells
                ncells = size(nearestNeighbour%i%values)

            else

                ! Get the 6 nearest cell, exclude boundary points (numberOfElements< idx < numberOfElements+numberOfRealBFaces)
                nearestNeighbour = search%kNearest(mesh%tree, &
                                                x=mesh%centroid(1,:), y=mesh%centroid(2,:), z=mesh%centroid(3,:), &
                                                xquery=this%x(1), yquery=this%x(2), zquery=this%x(3), k=ncells)
            endif

            nn = nearestNeighbour%i%values(1:ncells)

        elseif(flubioOptions%KDtreeType==2) then

            mesh%centroid(1:3, numberOfElements+numberOfBFaces+1) = this%x(1:3)

            if(present(radius)) then

                ! Be conservative, kdtree2 cannot quite paly with dynamic arrays
                if(bdim==1) then
                    nSearch = 4*15
                else
                    nSearch = 4*35
                endif

                allocate(nearestNeighbourMK(nSearch))

                ! Look inly nearby the point
                call kdtree2_r_nearest_around_point(mesh%tree_mk2, numberOfElements+numberOfBFaces+1, nSearch, radius**2, nFound, nSearch, nearestNeighbourMK)

            else

                allocate(nearestNeighbourMK(ncells))

                ! Get the 6 nearest cell, exclude boundary points (numberOfElements< idx < numberOfElements+numberOfRealBFaces)
                call kdtree2_n_nearest_around_point(mesh%tree_mk2, numberOfElements+numberOfBFaces+1, ncells, ncells, nearestNeighbourMK)

            endif

            ! Get first nCells only
            do n=1,ncells
                nn(n) = nearestNeighbourMK(n)%idx
            enddo

            deallocate(nearestNeighbourMK)

        endif

        ! Check the index is valid
        nearest_found = .false.
        do n = 1, ncells

            is_valid = this%checkCellIndex(nn(n))

            if(is_valid) then
                this%nearestCell = nn(n)
                nearest_found = .true.
                exit
            endif

        enddo

        call value_to_string(this%x(1), s(1))
        call value_to_string(this%x(2), s(2))
        call value_to_string(this%x(3), s(3))

        if(.not. nearest_found) call flubioStopMSg('FLUBIO ERROR: none of the nearest points to the lagrangian marker ('//  &
           trim(s(1))// ' ' // trim(s(2)) // ' ' // trim(s(3)) //') is an interior cell. Please check your input data.')

    end subroutine findNearestCell

!**********************************************************************************************************************

    subroutine findSupport(this, l, r)

    !==================================================================================================================
    ! Description:
    !! findSupport finds out the support around the lagrangian point.
    !==================================================================================================================

        class(lagrangianPoint) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(KdTreeSearch) :: search

        type(dArgDynamicArray) :: results

        type(kdtree2_result), dimension(:), allocatable :: resultsMK
    !------------------------------------------------------------------------------------------------------------------

        integer :: n, nfound, nalloc, nvalid, nSearch, l

        integer, dimension(:), allocatable :: nn

        integer, dimension(:), allocatable :: tmp
    !------------------------------------------------------------------------------------------------------------------

        real :: r

        real :: c(3), cn(3)
    !------------------------------------------------------------------------------------------------------------------

        logical :: is_valid

        logical, dimension(:), allocatable :: duplicated
    !------------------------------------------------------------------------------------------------------------------

        ! Radius 5 times the reference length
        call this%findNearestCell(ncells=6, radius=r)

        ! Radius just above the cell reference length
        r = this%getInfluenceRadius()

        ! Find nearest point inside a ball of radius r (expect a max of 35 points in 3D, 15 in 2D)
        if(flubioOptions%KDtreeType==0) then
            results = search%kNearest(mesh%tree, &
                                    x=mesh%centroid(1,:), y=mesh%centroid(2,:), z=mesh%centroid(3,:), &
                                    xquery=mesh%centroid(1,this%nearestCell), yquery=mesh%centroid(2,this%nearestCell), &
                                    zquery=mesh%centroid(3,this%nearestCell), radius=r)

            nfound = size(results%i%values)
            allocate(nn(nFound))
            nn = results%i%values(1:nfound)

        elseif(flubioOptions%KDtreeType==2) then

            if(bdim==1) then
                nSearch = 4*15
            else
                nSearch = 10*35
            endif

            allocate(resultsMK(nSearch))
            call kdtree2_r_nearest(mesh%tree_mk2, mesh%centroid(:,this%nearestCell), r**2, nfound, nSearch, resultsMK)

            allocate(nn(nFound))
            do n=1,nfound
                nn(n) = resultsMK(n)%idx
            enddo

        endif

        if(nfound<4) call flubioStopMsg('FLUBIO ERROR: cannot find any support cell for the lagrangian point.')

        ! Clear any existing support
        this%numberOfSupportCells = 0
        if(allocated(this%support)) deallocate(this%support)
        allocate(this%support(nfound))
        this%support = -1
        allocate(duplicated(nfound))
        allocate(tmp(nfound))

        ! Get rid of duplications
        do n=1,nfound
            tmp(n) = nn(n)
        enddo
        duplicated = this%removeDuplicatedCells(tmp, nfound)

        ! Fill the support cells vector, exclude non valid cells (duplications or boundary faces)
        do n=1,nfound
            is_valid = this%checkCellIndex(nn(n))
            if(is_valid .and. .not. duplicated(n)) then
                this%numberOfSupportCells = this%numberOfSupportCells+1
                this%support(this%numberOfSupportCells) = nn(n)
            endif
        enddo

        deallocate(duplicated)
        deallocate(tmp)
        deallocate(nn)

    end subroutine findSupport

!**********************************************************************************************************************

    function checkCellIndex(this, index) result(is_valid)

    !==================================================================================================================
    ! Description:
    !! checkCellIndex checks if an index falls inside the cell range or it is a boundary face.
    !==================================================================================================================

        class(lagrangianPoint) :: this
    !-----------------------------------------------------------------------------------------------------------------

        integer :: index
    !-----------------------------------------------------------------------------------------------------------------

        logical :: is_valid
    !-----------------------------------------------------------------------------------------------------------------

        is_valid = .true.

        if(index > numberOfElements .and. index <= numberOfElements + numberOfRealBFaces) is_valid = .false.

    end function checkCellIndex

!**********************************************************************************************************************

    function removeDuplicatedCells(this, index, n) result(duplicated)

    !==================================================================================================================
    ! Description:
    !! checkCellIndex checks if an index falls inside the cell range or it is a boundary face.
    !==================================================================================================================

        class(lagrangianPoint) :: this
    !-----------------------------------------------------------------------------------------------------------------

        integer :: i, j, k, n, ii, index(n), ghostIndex(n)
    !-----------------------------------------------------------------------------------------------------------------

        real :: c1(3), c2(3), r(n), delta(3), tol
    !-----------------------------------------------------------------------------------------------------------------

        logical :: mask(n)

        logical :: duplicated(n)
    !-----------------------------------------------------------------------------------------------------------------

        tol = 1.0e-12

        mask = .false.
        duplicated = .false.

        do i=1,n

            c1 = this%getCentroidFromIndex(index(i))

            do j=1,n

                if(i==j) then
                    mask(j) = .true.
                    cycle
                elseif(i/=j .and. .not. mask(j)) then

                    c2 = this%getCentroidFromIndex(index(j))

                    delta = c1-c2

                    if(abs(delta(1))<tol .and. abs(delta(2))<tol .and. abs(delta(3))<tol) then
                        mask(j) = .true.
                        duplicated(j) = .true.
                    endif

                else
                    cycle
                endif

            enddo

        enddo

    end function removeDuplicatedCells

!**********************************************************************************************************************

    function getVolumeFromIndex(this, index) result(vol)

        class(lagrangianPoint) :: this
    !-----------------------------------------------------------------------------------------------------------------

        integer :: index

        integer :: pNeigh

        integer :: offset
    !-----------------------------------------------------------------------------------------------------------------

        real :: vol
    !-----------------------------------------------------------------------------------------------------------------

        offset = numberOfElements + numberOfRealBFaces

        if(index <= numberofElements) then
            vol = volumeField%phi(index,1)
        else
            pNeigh = mesh%boundaries%ghostBoundaryMap(index-offset,1)
            vol = volumeField%phi(pNeigh,1)
        endif

    end function getVolumeFromIndex

!**********************************************************************************************************************

    function getCentroidFromIndex(this, index) result(c)

        class(lagrangianPoint) :: this
    !-----------------------------------------------------------------------------------------------------------------

        integer :: index

        integer :: pNeigh

        integer :: offset
    !-----------------------------------------------------------------------------------------------------------------

        real :: c(3)
    !-----------------------------------------------------------------------------------------------------------------

        offset = numberOfElements + numberOfRealBFaces

        if(index <= numberofElements) then
            c = mesh%centroid(1:3,index)
        else
            pNeigh = mesh%boundaries%ghostBoundaryMap(index-offset,1)
            c = mesh%centroid(1:3,pNeigh)
        endif

    end function getCentroidFromIndex

!**********************************************************************************************************************

    function getFieldValueFromIndex(this, field, index) result(c)

        class(lagrangianPoint) :: this

        type(flubioField) :: field
    !-----------------------------------------------------------------------------------------------------------------

        integer :: index

        integer :: pNeigh

        integer :: offset
    !-----------------------------------------------------------------------------------------------------------------

        real :: c(field%nComp)
    !-----------------------------------------------------------------------------------------------------------------

        offset = numberOfElements + numberOfRealBFaces

        if(index <= numberofElements) then
            c = field%phi(index, 1:field%nComp)
        else
            pNeigh = mesh%boundaries%ghostBoundaryMap(index-offset,1)
            c = field%phi(pNeigh,1:field%nComp)
        endif

    end function getFieldValueFromIndex

!**********************************************************************************************************************

    function getFaceFromIndex(this, index) result(iBFace)

        class(lagrangianPoint) :: this
    !-----------------------------------------------------------------------------------------------------------------

        integer :: index

        integer :: iBFace

        integer :: offset
    !-----------------------------------------------------------------------------------------------------------------

        offset = numberOfElements + numberOfRealBFaces

        if(index <= numberofElements) then
            iBFace = -1
        else
            iBFace = mesh%boundaries%ghostBoundaryMap(index-offset,2)
        endif

    end function getFaceFromIndex

!**********************************************************************************************************************

    subroutine computeTransferFunction(this, functionType)

        use mlsInterpolation, only: transferFunction, transferFunction2D

        class(lagrangianPoint) :: this
    !-----------------------------------------------------------------------------------------------------------------

        character(len=*) :: functionType
    !------------------------------------------------------------------------------------------------------------------

        integer :: n
    !------------------------------------------------------------------------------------------------------------------

        real :: d
        !! dilatation factor

        real :: rmax
        !! max distance to include

        real :: xsup(this%numberOfSupportCells,3)
        !! cell centers of the support cells
    !------------------------------------------------------------------------------------------------------------------

        ! Prepare
        if(allocated(this%Phi)) deallocate(this%Phi)
        allocate(this%Phi(1, this%numberOfSupportCells))

        ! Fill support cell centers and field value in support cells
        do n=1,this%numberOfSupportCells
            xsup(n,1:3) = mesh%centroid(1:3,this%support(n))
        enddo

        ! Dilatation factor
        !d = this%getInfluenceRadius()
        d = this%maxDistance()

        ! Compute the transfer function
        if(bdim==0) then
            this%Phi = transferFunction(x=this%x, xi=xsup , nSupport=this%numberOfSupportCells, d=d, functionType=functionType)
        else
            this%Phi = transferFunction2D(x=this%x(1:2), xi=xsup(:,1:2), nSupport=this%numberOfSupportCells, d=d, functionType=functionType)
        endif

    end subroutine computeTransferFunction

!**********************************************************************************************************************

    function interpolateFieldOnPoint(this, field, functionType) result(fi)

        use mlsInterpolation, only: transferFunction

        class(lagrangianPoint) :: this

        type(flubioField) :: field
    !-----------------------------------------------------------------------------------------------------------------

        character(len=*) :: functionType
    !------------------------------------------------------------------------------------------------------------------

        integer :: n
    !------------------------------------------------------------------------------------------------------------------

        real :: val(1,1)

        real :: xsup(this%numberOfSupportCells,3)
        !! cell centers of the support cells

        real :: f(this%numberOfSupportCells,field%nComp)
        !! field value of the support cells

        real :: ftmp(this%numberOfSupportCells,1)

        real :: fi(field%nComp)
        !! interpolated field value
    !------------------------------------------------------------------------------------------------------------------

        ! Fill field values within the support cells
        do n=1,this%numberOfSupportCells
            f(n,1:field%nComp) = this%getFieldValueFromIndex(field, this%support(n))
        enddo

        ! compute the transfte function
        call this%computeTransferFunction(functionType)

        ! Interpolate the field at the point
        fi = 0.0
        do n=1,field%nComp
            ftmp(1:this%numberOfSupportCells,1) = f(1:this%numberOfSupportCells,n)
            val = MatMul(this%Phi, ftmp)
            fi(n) = val(1,1)
        enddo

    end function interpolateFieldOnPoint

!**********************************************************************************************************************

    function interpolateFieldOnPoint2D(this, field, functionType) result(fi)

        use mlsInterpolation, only: transferFunction

        class(lagrangianPoint) :: this

        type(flubioField) :: field
    !-----------------------------------------------------------------------------------------------------------------

        character(len=*) :: functionType
    !------------------------------------------------------------------------------------------------------------------

        integer :: n
    !------------------------------------------------------------------------------------------------------------------

        real :: val(1,1)

        real :: xsup(this%numberOfSupportCells,3)
        !! cell centers of the support cells

        real :: f(this%numberOfSupportCells,field%nComp)
        !! field value of the support cells

        real :: ftmp(this%numberOfSupportCells,1)

        real :: fi(field%nComp)
        !! interpolated field value
    !------------------------------------------------------------------------------------------------------------------

        ! Fill field values within the support cells
        do n=1,this%numberOfSupportCells
            f(n,1:field%nComp) = this%getFieldValueFromIndex(field, this%support(n))
        enddo

        ! compute the transfte function
        call this%computeTransferFunction(functionType)

        ! Interpolate the field at the point
        fi = 0.0
        do n=1,field%nComp-bdim
            ftmp(1:this%numberOfSupportCells,1) = f(1:this%numberOfSupportCells,n)
            val = MatMul(this%Phi, ftmp)
            fi(n) = val(1,1)
        enddo

    end function interpolateFieldOnPoint2D

!**********************************************************************************************************************

    function getInfluenceRadius(this) result(d)

        class(lagrangianPoint) :: this
    !-----------------------------------------------------------------------------------------------------------------

        real :: vol

        real :: d
    !-----------------------------------------------------------------------------------------------------------------

        if(bdim==0) then
            vol = this%getVolumeFromIndex(index=this%nearestCell)
            d = this%lambdas*vol**(1.0/3.0)
        else
            ! Be sure the lenght in z is 1, otherwise the calculation will be wrong
            vol = this%getVolumeFromIndex(index=this%nearestCell)
            d = this%lambdas*sqrt(vol)
        endif

    end function getInfluenceRadius

!**********************************************************************************************************************

    function maxDistance(this) result(rmax)

        class(lagrangianPoint) :: this
    !-----------------------------------------------------------------------------------------------------------------

        integer :: n
    !-----------------------------------------------------------------------------------------------------------------

        real :: c(3)

        real :: r, rmax, dx, dy, dz, small
    !-----------------------------------------------------------------------------------------------------------------

        small = 1.0e-10

        rmax = -100.0
        do n=1,this%numberOfSupportCells

            c = this%getCentroidFromIndex(this%support(n))

            dx = this%x(1) - c(1)
            dy = this%x(2) - c(2)
            dz = this%x(3) - c(3)

            r = sqrt(dx**2 + dy**2 + dz**2)

            if(r>rmax) rmax = r

        enddo

        ! Make it a bit larger
        rmax = rmax + small

    end function maxDistance

!**********************************************************************************************************************

    function hl(this) result(h)

        class(lagrangianPoint) :: this
    !-----------------------------------------------------------------------------------------------------------------

        integer :: n
    !-----------------------------------------------------------------------------------------------------------------

        real :: vol

        real :: h
    !-----------------------------------------------------------------------------------------------------------------

        h = 0.0
        do n=1,this%numberOfSupportCells
            vol = this%getVolumeFromIndex(this%support(n))
            if(bdim==0) then
                h = h + this%Phi(1,n)*vol**(1.0/3.0)
            else
                ! Be sure the extension in z direction is 1, or this will be wrong
                h = h + this%Phi(1,n)*vol**(1.0/2.0)
            endif
        enddo

    end function hl

!**********************************************************************************************************************

    function computeCl(this) result(Cl)

        class(lagrangianPoint) :: this
    !-----------------------------------------------------------------------------------------------------------------

        integer :: n
    !-----------------------------------------------------------------------------------------------------------------

        real :: small

        real :: den

        real :: vol

        real :: hl

        real :: Cl
    !-----------------------------------------------------------------------------------------------------------------

        small = 1.0e-12

        ! Compute denominator
        den = 0.0
        do n=1,this%numberOfSupportCells
            vol = this%getVolumeFromIndex(this%support(n))
            den = den + this%Phi(1,n)*vol
        enddo

        if(den<small) call flubioStopMsg('ERROR: denominator is close to zero and Cl cannot be computed.')

        ! Compute hl
        hl = this%hl()

        ! Compute Cl
        Cl = hl*this%area/den

    end function computeCl

!**********************************************************************************************************************

    subroutine eulerianForce(this, f, fghost, Flag)

        class(lagrangianPoint) :: this

        type(flubioField) :: f

        type(flubioField) :: fghost
    !-----------------------------------------------------------------------------------------------------------------

        integer :: n

        integer :: iOwner

        integer :: iBFace

        integer :: cellIndex

        integer :: faceIndex
    !-----------------------------------------------------------------------------------------------------------------

        real :: Cl

        real :: Flag(3-bdim)
    !-----------------------------------------------------------------------------------------------------------------

        ! Compute Cl
        Cl = this%computeCl()

        ! Eulerian force
        do n=1,this%numberOfSupportCells

            cellIndex = this%support(n)
            faceIndex = this%getFaceFromIndex(cellIndex)

            ! Ghost cells in support
            if(faceIndex > -1 ) then
                iOwner = mesh%owner(faceIndex)
                fghost%phi(iOwner, 1:3-bdim) = fghost%phi(iOwner, 1:3-bdim) + Cl*this%Phi(1,n)*Flag(1:3-bdim)
            else
                f%phi(cellIndex,1:3-bdim) = f%phi(cellIndex,1:3-bdim) + Cl*this%Phi(1,n)*Flag(1:3-bdim)
            endif

        enddo

    end subroutine eulerianForce

!**********************************************************************************************************************

end module lagrangianMarker