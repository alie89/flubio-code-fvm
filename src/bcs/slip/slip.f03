!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

	subroutine slipBoundary(aC, acd, acc, bC)

	!==================================================================================================================
	! Description:
	!! slipBoundary computes the diagonal coefficient and rhs correction when a slip boundary condition is applied.
	!! The correction reads:
	!! \begin{eqnarray*}
	!!    &\textbf{a}_C^b = 0  \\
	!!    &\textbf{b}_C^b = 0
	!! \end{eqnarray*}
	!==================================================================================================================

	implicit none

		real :: aC(3)
		!! diagonal coefficient corretion due to the velocity inlet boundary condition at the target boundary face owner

		real :: bC(3)
		!! right hand side corretion due to the velocity inlet boundary condition

		real :: acd(3)
		!! diffusion contribution

		real :: acc(3)
		!! convcetion contribution
!------------------------------------------------------------------------------------------------------------------

	! No flow through the wall and no velocity gradient
		aC = 0.0
		bC = 0.0

		acd = 0.0
		acc = 0.0

	end subroutine slipBoundary

! *********************************************************************************************************************

	subroutine updateSlipBoundaryField(phi, iBoundary)

	!==================================================================================================================
	! Description:
	!! updateSlipBoundaryField loops over all the boundary faces of a target patch
	!! and updates the field face values according to the following formula:
	!! \begin{eqnarray*}
	!!    &a = \textbf{u}_b \cdot \textbf{n}_b
	!!    &u_b = u_C-a n_b^x \\
	!!    &v_b = v_C-a n_b^y \\
	!!    &w_b = w_C-a n_b^z \\
	!! \end{eqnarray*}
	!==================================================================================================================

		! modules
		use meshvar
		use massFlowVar
	
		! arguments
		implicit none

		integer :: iBoundary
		!! target boundary

		integer :: iBFace
		!! target boundary face

		integer :: is
		!! start face of the target boundary

		integer :: ie
		!! end face of the target boundary

		integer :: iOwner
		!! owner of the target boundary face

		integer ::fComp
		!! number of field components

		integer :: patchFace
		!! auxiliary loop index

!------------------------------------------------------------------------------------------------------------------
	
		real :: nf(3)
		!! boundary face normal

		real :: a

		real :: phi(numberOfElements+numberOfBFaces, 3)
		!! field values

!------------------------------------------------------------------------------------------------------------------

		is = mesh%boundaries%startFace(iBoundary)

		ie = is+mesh%boundaries%nFace(iBoundary)-1

		patchFace=numberOfElements + (is-numberOfIntFaces-1)

		do iBFace=is,ie

		   patchFace = patchFace+1

		   iOwner = mesh%owner(iBFace)

		   nf = mesh%Sf(iBFace,:)/norm2(mesh%Sf(iBFace,:))

		   a = phi(iOwner,1)*nf(1) + &
			   phi(iOwner,2)*nf(2) + &
			   phi(iOwner,3)*nf(3)

			phi(patchFace,1) = phi(iOwner,1) - a*nf(1)

			phi(patchFace,2) = phi(iOwner,2) - a*nf(2)

			phi(patchFace,3) = phi(iOwner,3) - a*nf(3)

		enddo

	end subroutine updateSlipBoundaryField

! *********************************************************************************************************************

