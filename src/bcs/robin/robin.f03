!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

	subroutine robinBoundary(iBoundary, iBFace, aC, bC, acd, acc, phiGradb, phib, mu, slipLength, phiInf, fComp)

		use meshvar
		use massFlowVar
	
		implicit none

		integer :: iBoundary, iBFace, iComp, fComp
	!------------------------------------------------------------------------------------------------------------------

		real :: aC(fComp), bC(fComp), corr(fComp), phiGradb(3, fComp), phib(fComp)
	!------------------------------------------------------------------------------------------------------------------

		real :: slipLength(fComp), phiInf(fComp), mu

		real :: a, b, dot

		real :: c
		!! switch for convection

		real :: acd(fComp)
		!! diffusion contribution

		real :: acc(fComp)
		!! convcetion contribution

	!------------------------------------------------------------------------------------------------------------------

		do iComp=1,fComp

			dot = dot_product(phiGradb(:,iComp),mesh%Tf(iBFace,:))
			
			a = slipLength(iComp)*mu*mesh%gDiff(iBFace)*norm2(mesh%Sf(iBFace,:))
			b = slipLength(iComp)*norm2(mesh%Sf(iBFace,:))+mu*mesh%gDiff(iBFace)
			c = mu*dot/b

			aC(iComp) = mf(iBFace,1) + a/b
			bC(iComp) = (a/b)*phiInf(iComp) + c

			acd = a/b
			acc = mf(iBFace,1)

		enddo

	end subroutine robinBoundary

! *********************************************************************************************************************

	subroutine updateRobinBoundaryField(phi, phiGrad, iBoundary, mu, slipLength, phiInf, fComp)

		use meshvar
		
		implicit none

		integer :: iBFace, iBoundary, is, ie, iOwner, iComp, fComp, patchFace
	!------------------------------------------------------------------------------------------------------------------

		real :: phi(numberOfElements+numberOfBFaces, fComp), phiGrad(numberOfElements+numberOfBFaces, 3, fComp)

		real :: a, b, dot, mu, slipLength(fComp), phiInf(fComp)
	!------------------------------------------------------------------------------------------------------------------

		is = mesh%boundaries%startFace(iBoundary)
		ie = is+mesh%boundaries%nFace(iBoundary)-1
		patchFace = numberOfElements +(is-numberOfIntFaces-1)

		do iBFace=is,ie

		   patchFace = patchFace+1
		   iOwner = mesh%owner(iBFace)

		   dot = dot_product(phiGrad(patchFace,:,iComp), mesh%Tf(iBFace,:))
		 
		   do iComp=1,3

			   a = slipLength(iComp)*norm2(mesh%Sf(iBFace,:))*phiInf(iComp) + mu*mesh%gDiff(iBFace)*phi(iOwner,iComp)-dot
			   b = slipLength(iComp)*norm2(mesh%Sf(iBFace,:))+mu*mesh%gDiff(iBFace)

			   phi(patchFace,:) = a/b

			enddo

		enddo

	end subroutine updateRobinBoundaryField

! *********************************************************************************************************************

