!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

	subroutine prescribedFluxBoundary(iBFace, aC, bC, acd, acc, fixedGrad, mu, c, fComp)

	!==================================================================================================================
	! Description:
	!! prescribedFluxBoundary computes the diagonal coefficient and rhs correction when a Neumann boundary condition is applied
	!!	(i.e. the boundary flux is prescribed).
	!! The correction reads:
	!! \begin{eqnarray}
	!!    &q_b = (\mu_b\nabla\phi_b)\cdot\textbf{S}_b \\
	!!    &a_C^b = m_f \\
	!!    &b_C^b = q_b
	!! \end{eqnarray}
	!==================================================================================================================

		use meshvar
		use massFlowVar

		implicit none

		integer :: iBFace, fComp
	!------------------------------------------------------------------------------------------------------------------

		real :: aC(fComp)
		!! diagonal coefficent correction

		real :: bC(fComp)
		!! rhs correction

		real :: mu
		!! diffusion coefficient

		real :: c
		!! switch for convection

		real :: acd(fComp)
		!! diffusion contribution

		real :: acc(fComp)
		!! convcetion contribution

		real :: flux(fComp)
		!! flux to be applied

		real :: fixedGrad(fComp)
		!! fixed gradient at the boundary

	!------------------------------------------------------------------------------------------------------------------

        aC = c*mf(iBFace,1)
		flux = fixedGrad*norm2(mesh%Sf(iBFace,:))
		bC = flux

		acd = 0.0
		acc = c*mf(iBFace,1)

	end subroutine prescribedFluxBoundary

! *********************************************************************************************************************

	subroutine updatePrescribedFluxBoundaryField(phi, mu, prescribedFlux, iBoundary, fComp)

	!==================================================================================================================
	! Description:
	!! updateprescribedFluxBoundaryField loops over all the boundary faces of a target patch
	!! and updates the face values according to the prescribed flux:
	!! \begin{equation*}
	!!    \phi_b = \phi_C -\dfrac{q_b}{\mu g_Diff} \\
	!! \end{equation*}
	!==================================================================================================================

		use meshvar
		
		implicit none

		integer :: iBoundary, iBFace, is, ie, iOwner, fComp, patchFace
!------------------------------------------------------------------------------------------------------------------
	
		real :: phi(numberOfElements+numberOfBFaces, fComp)
		!! field values

		real :: prescribedFlux(fComp)
		!! fixed gradient at boundary face

		real :: delta
		!! cell center to boundary face center distance

		real :: nf(3)
		!! boundary face normal

		real :: mu
		!! diffusion coefficient
!------------------------------------------------------------------------------------------------------------------

		is = mesh%boundaries%startFace(iBoundary)
		ie = is+mesh%boundaries%nFace(iBoundary)-1
		patchFace = numberOfElements+(is-numberOfIntFaces-1)

		do iBFace=is,ie
		   patchFace = patchFace+1
		   iOwner = mesh%owner(iBFace)
		   nf = mesh%Sf(iBFace,:)/norm2(mesh%Sf(iBFace,:))
		   delta = dot_product(mesh%CN(iBFace,:), nf)

		!   phi(patchFace,:) = phi(iOwner,:) + delta*fixedGrad
			phi(patchFace,:) = phi(iOwner,:) - prescribedFlux/(mu*mesh%gDiff(iBFace))
		enddo

	end subroutine updateprescribedFluxBoundaryField

! *********************************************************************************************************************

