!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

	subroutine symmetryBoundary(phi, iBFace, aC, bC, acd, acc, mu, fComp)

	!==================================================================================================================
	! Description:
	!! symmetryBoundary computes the diagonal coefficient and rhs correction when a symmetry boundary condition is applied.
	!! The correction reads:
	!! \begin{eqnarray}
	!!    &\textbf{a}_C^b = 2\mu_b\frac{\textbf{S}_b^2}{d_\perp}  \\
	!!    &b_C^b|_u = 2\mu_b\frac{\textbf{S}_b}{d_\perp}[v_C n_b^y+w_C n_b^z]n_b^x  \\
	!!    &b_C^b|_v = 2\mu_b\frac{\textbf{S}_b}{d_\perp}[u_C n_b^x+w_C n_b^z]n_b^y \\
	!!    &b_C^b|_w = 2\mu_b\frac{\textbf{S}_b}{d_\perp}[u_C n_b^x+v_C n_b^y]n_b^z \\
	!! \end{eqnarray}
	!==================================================================================================================

		! modules
		use meshvar
		use massFlowVar

		! arguments
		implicit none

		integer iBFace
		!! target boundary face

		integer :: fComp
		!! number of field components

		integer :: iOwner
		!! owner of the target boundary face
	!------------------------------------------------------------------------------------------------------------------

		real :: aC(fComp)
		!! diagonal coefficient corretion due to the velocity inlet boundary condition at the target boundary face owner

		real :: bC(fComp)
		!! right hand side corretion due to the velocity inlet boundary condition

		real:: nf(3)
		!! surface normal at a target boundary face

		real :: mu
		!! dinamic viscosity at a target boundary face

		real :: a
		!! auxiliary variable

		real :: b
		!! auxiliary variable

		real :: phi(numberOfElements+numberOfBFaces, 3)
		!! field values

		real :: acd(fComp)
		!! diffusion contribution

		real :: acc(fComp)
		!! convcetion contribution

		real :: walldist
		!! corrected distance		
	!------------------------------------------------------------------------------------------------------------------

		iOwner = mesh%owner(iBFace)

		! Get the surface normal
		nf = mesh%Sf(iBFace,:)/norm2(mesh%Sf(iBFace,:))
		walldist = norm2(mesh%Sf(iBFace,:))**2/dot_product(mesh%CN(iBFace,:), mesh%Sf(iBFace,:))
		aC = 2.0*mu*mesh%gDiff(iBFace)*nf**2

		!u-Component
		a = phi(iOwner,2)*nf(2)*nf(1)
		b = phi(iOwner,3)*nf(3)*nf(1)

		bC(1) = -2.0*mu*walldist*(a+b)

		!v-Component
		a = phi(iOwner,1)*nf(1)*nf(2)
		b = phi(iOwner,3)*nf(3)*nf(2)

		bC(2) = -2.0*mu*walldist*(a+b)

		!w-Component
		a = phi(iOwner,1)*nf(1)*nf(3)
		b = phi(iOwner,2)*nf(2)*nf(3)

		bC(3) = -2.0*mu*walldist*(a+b)

		acd = 0.0
		acc = 0.0

	end subroutine symmetryBoundary

! *********************************************************************************************************************

	subroutine updateSymmetryBoundaryField(phi, iBoundary)

	!==================================================================================================================
	! Description:
	!! updateSymmetryBoundaryField loops over all the boundary faces of a target patch
	!! and updates the field face values according to the following formula:
	!! \begin{eqnarray}
	!!    &a = \textbf{u}_b \cdot textbf{n}_b
	!!    &u_b = u_C-a n_b^x \\
	!!    &v_b = v_C-a n_b^y \\
	!!    &w_b = w_C-a n_b^z \\
	!! \end{eqnarray}
	!==================================================================================================================

		use meshvar
		use massFlowVar
		
		implicit none

		integer :: iBoundary
		!! target boundary

		integer :: iBFace
		!! target boundary face

		integer :: is
		!! start face of the target boundary

		integer :: ie
		!! end face of the target boundary

		integer :: iOwner
		!! owner of the target boundary face

		integer ::fComp
		!! number of field components

		integer :: patchFace
		!! auxiliary loop index
	!------------------------------------------------------------------------------------------------------------------

		real nf(3)
		!! surface normal at a target boundary face

		real :: a
		!! auxiliary variable

		real :: phi(numberOfElements+numberOfBFaces, 3)
		!! field values
	!------------------------------------------------------------------------------------------------------------------

		is = mesh%boundaries%startFace(iBoundary)
		ie = is+mesh%boundaries%nFace(iBoundary)-1
		patchFace = is-numberOfIntFaces

		do iBFace=is,ie

		   patchFace = patchFace+1
		   iOwner = mesh%owner(iBFace)
		   nf = mesh%Sf(iBFace,:)/norm2(mesh%Sf(iBFace,:))

		   a = phi(iOwner,1)*nf(1) + &
			   phi(iOwner,2)*nf(2) + &
			   phi(iOwner,3)*nf(3)

			phi(patchFace,1) = phi(iOwner,1) - a*nf(1)
			phi(patchFace,2) = phi(iOwner,2) - a*nf(2)
			phi(patchFace,3) = phi(iOwner,3) - a*nf(3)

		enddo

	end subroutine updateSymmetryBoundaryField

! *********************************************************************************************************************

