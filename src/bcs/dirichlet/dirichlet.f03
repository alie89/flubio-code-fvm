!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

	subroutine dirichletBoundary(iBFace, aC, bC, acd, acc, phib, mu, c, fComp)

	!==================================================================================================================
	! Description:
	!! dirichletBoundary computes the diagonal coefficient and rhs correction when a dirichlet (fixed value)
	!!	boundary condition is applied. The correction reads:
	!! \begin{eqnarray}
	!!    &a_C^b = \mu_b\frac{S_b}{d_{Cb}} \\
	!!    &b_C^b = -m_f\phi_b + \mu_b\frac{S_b}{d_{Cb}}\phi_b
	!! \end{eqnarray}
	!==================================================================================================================

		use meshvar
		use massFlowVar

		implicit none

		integer :: iBFace, fComp
	!------------------------------------------------------------------------------------------------------------------

		real :: aC(fComp)
		!! diagonal coefficent correction

		real :: bC(fComp)
		!! boundary condition rhs

		real :: phib(fComp)
		!! rhs correction

		real :: mu
		!! diffusion coefficient

		real :: c
		!! switch for convection

		real :: acd(fComp)
		!! diffusion contribution

		real :: acc(fComp)
		!! convcetion contribution
	!------------------------------------------------------------------------------------------------------------------

		aC = mu*mesh%gDiff(iBFace)
		bC = -c*mf(iBFace,1)*phib + mu*mesh%gDiff(iBFace)*phib

		acd = mu*mesh%gDiff(iBFace)
		acc = 0.0

	end subroutine dirichletBoundary

! *********************************************************************************************************************

	subroutine dirichletBoundaryFf(iBFace, aC, bC, acd, acc, phib, rhof, mu, c, fComp)
	!==================================================================================================================
	! Description:
	!! dirichletBoundaryFf is the same as dirichletBoundary,
	!! but it uses the velocity face flux instead of the mass flow rate $mf$.
	!==================================================================================================================

		use meshvar
		use massFlowVar
		
		implicit none

		integer :: iBFace, fComp
	!------------------------------------------------------------------------------------------------------------------

		real :: aC(fComp)

		real :: bC(fComp)
		!! diagonal coefficent correction

		real :: phib(fComp)
		!! rhs correction

		real :: mu
		!! diffusion coefficient

		real :: rhof
		!! density at face

		real :: c
		!! Convection switch

		real :: acd(fComp)
		!! diffusion contribution

		real :: acc(fComp)
		!! convcetion contribution
	!------------------------------------------------------------------------------------------------------------------

		aC = mu*mesh%gDiff(iBFace)
		bC = -c*mf(iBFace,1)/rhof*phib + mu*mesh%gDiff(iBFace)*phib

		acd = mu*mesh%gDiff(iBFace)
		acc = 0.0

	end subroutine dirichletBoundaryFf

! *********************************************************************************************************************

	subroutine updateDirichletBoundaryField(phi, phi0, iBoundary, fComp)

	!==================================================================================================================
	! Description:
	!! updateDirichletBoundaryField loops over all the boundary faces of a target patch
	!! and updates the field face values with the prescribed one:
	!! \begin{equation*}
	!!    \phi_b = \phi_{specified} \\
	!! \end{equation*}
	!==================================================================================================================

		use meshvar

		implicit none

		integer :: iBoundary, iBFace, is, ie, iOwner, fComp, patchFace
	!------------------------------------------------------------------------------------------------------------------
	
		real :: phi(numberOfElements+numberOfBFaces, fComp)
		!! field values

		real :: phi0(fComp)
		!! value to be assigned at the boundary
	!------------------------------------------------------------------------------------------------------------------

		is = mesh%boundaries%startFace(iBoundary)
		ie = is+mesh%boundaries%nFace(iBoundary)-1

		patchFace = numberOfElements + (is-numberOfIntFaces-1)

		do iBFace=is,ie
			patchFace = patchFace+1
			phi(patchFace,:) = phi0
		enddo

	end subroutine updateDirichletBoundaryField

! *********************************************************************************************************************

	subroutine updateUserDefinedBoundaryField(phi, iBoundary, expr, fComp)

	!==================================================================================================================
	! Description:
	!! updateDirichletBoundaryField is the same as updateDiricheletBoundaryField,
	!! but the boudnary expression can be parsed from the boundary condition file, allowing for custom expressions.
	!==================================================================================================================

		use meshvar
		use globalTimeVar

		implicit none

		character(len=10), dimension(4) :: variables
		!! variables in string

		character(len=*), dimension(fComp) :: expr
		!! expression passed to the interpreter

		character(len=500) :: func
		!! expression parsed from the boundary condition file
	!------------------------------------------------------------------------------------------------------------------

		integer :: iBoundary, is, ie, iOwner, patchFace

		integer :: i
		!! loop index

		integer :: iBFace
		!! boundary face loop index

		integer:: iComp
		!! taget components

		integer :: fComp
		!! number of field components

	!------------------------------------------------------------------------------------------------------------------
	
		real :: phi(numberOfElements+numberOfBFaces, fComp), pi
	!------------------------------------------------------------------------------------------------------------------

		real :: variablesValues(4)
		!! name of the variables to be interpreted

		real :: val
		!! auxiliary number

		real :: getBoundaryFieldValue
		!! function to get the interpreted fiedl value
	!------------------------------------------------------------------------------------------------------------------

		is = mesh%boundaries%startFace(iBoundary)
		ie = is+mesh%boundaries%nFace(iBoundary)-1
		patchFace=numberOfElements + (is-numberOfIntFaces-1)

		variables(1) = 'x'
		variables(2) = 'y'
		variables(3) = 'z'
		variables(4) = 't'

		do iComp=1,fComp

			do iBFace=is,ie

				patchFace = patchFace+1

				! variables
				variablesValues(1) = mesh%fcentroid(iBFace,1) ! x
				variablesValues(2) = mesh%fcentroid(iBFace,2) ! y
				variablesValues(3) = mesh%fcentroid(iBFace,3) ! z
				variablesValues(4) = time ! t

				! Set the your boundary field
				val = getBoundaryFieldValue(expr(iComp), variables, variablesValues)
				phi(patchFace, iComp) = val

			enddo

		enddo

	end subroutine updateUserDefinedBoundaryField

! *********************************************************************************************************************

	real function getBoundaryFieldValue(expr, var, varVal)

	!------------------------------------------------------------------------------------------------------------------
	! Description:
	!! getBoundaryFieldValue is an auxiliary function that convert a mathematical string expression into a real value.
	!------------------------------------------------------------------------------------------------------------------

		use interpreter
		use flubioMpi
	!------------------------------------------------------------------------------------------------------------------

		implicit none

		character(len = 10),  dimension(3) :: var

		character(len = 275) :: expr

		character (len = 5)  :: statusFlag
	!------------------------------------------------------------------------------------------------------------------

		real :: varVal(3), val
	!------------------------------------------------------------------------------------------------------------------

		!Initializes function
		call init(expr, var, statusflag)

		! evaluate expression
		if(statusFlag=='ok') then
			getBoundaryFieldValue = evaluate(varVal)
		else
			call flubioStopMsg('FLUBIO: getBoundaryFieldValue - something went wrong during the expression parsing, please check it!')
		end if

		! Destroy actual function evaluator
		call destroyfunc()

		return

	end function getBoundaryFieldValue

! *********************************************************************************************************************

	subroutine dirichletBoundaryDiffusionOnly(iBFace, aC, bC, phib, mu, fComp)

	!==================================================================================================================
	! Description:
	!! dirichletBoundary computes the diagonal coefficient and rhs correction when a dirichlet (fixed value)
	!!	boundary condition is applied. The correction reads:
	!! \begin{eqnarray}
	!!    &a_C^b = \mu_b\frac{S_b}{d_{Cb}} \\
	!!    &b_C^b =  \mu_b\frac{S_b}{d_{Cb}}\phi_b
	!! \end{eqnarray}
	!==================================================================================================================

		use meshvar
		use massFlowVar

		implicit none

		integer :: iBFace, fComp
	!------------------------------------------------------------------------------------------------------------------

		real :: aC(fComp)
		!! diagonal coefficent correction

		real :: bC(fComp)
		!! rhs correction

		real :: phib(fComp)
		!! rhs correction

		real :: mu
		!! diffusion coefficient
	!------------------------------------------------------------------------------------------------------------------

		aC = mu*mesh%gDiff(iBFace)
		bC = mu*mesh%gDiff(iBFace)*phib

	end subroutine dirichletBoundaryDiffusionOnly

! *********************************************************************************************************************
