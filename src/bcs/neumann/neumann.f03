!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

	subroutine neumannBoundary(iBFace, aC, bC, acd, acc, fixedGrad, mu, c, fComp)

	!==================================================================================================================
	! Description:
	!! prescribedFluxBoundary computes the diagonal coefficient and rhs correction when a Neumann boundary condition is applied
	!!	(i.e. the boundary flux is prescribed).
	!! The correction reads:
	!! \begin{eqnarray}
	!!    &q_b = (\mu_b\nabla\phi_b)\cdot\textbf{S}_b \\
	!!    &a_C^b = m_f \\
	!!    &b_C^b = q_b = \mu\nabla\phi\cdot\textbf{n}
	!! \end{eqnarray}
	!==================================================================================================================

		use meshvar
		use massFlowVar

		implicit none

		integer :: iBoundary, iBFace, fComp
	!------------------------------------------------------------------------------------------------------------------

		real :: aC(fComp), bC(fComp), fixedGrad(fComp), flux(fComp), mu

		real :: c
		!! switch for convection

		real :: acd(fComp)
		!! diffusion contribution

		real :: acc(fComp)
		!! convcetion contribution
	!------------------------------------------------------------------------------------------------------------------

		aC = c*mf(iBFace,1)
		flux = fixedGrad*norm2(mesh%Sf(iBFace,:))
		bC = mu*flux

		acd = 0.0
		acc = c*mf(iBFace,1)

	end subroutine neumannBoundary

! *********************************************************************************************************************

	subroutine neumannBoundaryFf(iBFace, aC, bC, fixedGrad, rhof, mu, c, fComp)

	!==================================================================================================================
	! Description:
	!! neumannBoundaryFf is the same as neumannBoundary,
	!! but uses velocity face fluxes and instead of the mass flow rate $mf$.
	!==================================================================================================================

		use meshvar
		use massFlowVar

		implicit none

		integer iBoundary, iBFace, fComp
	!------------------------------------------------------------------------------------------------------------------

		real :: aC(fComp), bC(fComp), fixedGrad(fComp), flux(fComp), rhof, mu, c
	!------------------------------------------------------------------------------------------------------------------

		aC = c*mf(iBFace,1)/rhof
		flux = fixedGrad*norm2(mesh%Sf(iBFace,:))
		bC = mu*flux

	end subroutine neumannBoundaryFf

! *********************************************************************************************************************

	subroutine updateneumannBoundaryField(phi, fixedGrad, iBoundary, fComp)

	!==================================================================================================================
	! Description:
	!! updateneumannBoundaryField loops over all the boundary cells of a target patch
	!! and updates the face values according to the prescribed gradient:
	!! \begin{equation*}
	!!    \phi_b = \phi_C + d_{Cb}\nabla\phi_b \\
	!! \end{equation*}
	!==================================================================================================================

		use meshvar

		implicit none

		integer iBoundary, iBFace, is, ie, iOwner, fComp, patchFace
!------------------------------------------------------------------------------------------------------------------
	
		real phi(numberOfElements+numberOfBFaces, fComp), fixedGrad(fComp), delta, nf(3)
!------------------------------------------------------------------------------------------------------------------

		is = mesh%boundaries%startFace(iBoundary)
		ie = is+mesh%boundaries%nFace(iBoundary)-1
		patchFace=numberOfElements + (is-numberOfIntFaces-1)

		do iBFace=is,ie

		   patchFace = patchFace+1
		   iOwner = mesh%owner(iBFace)

		   nf = mesh%Sf(iBFace,:)/norm2(mesh%Sf(iBFace,:))
		   delta = dot_product(mesh%CN(iBFace,:), nf)
		   phi(patchFace,:) = phi(iOwner,:) + delta*fixedGrad

		enddo

	end subroutine updateneumannBoundaryField

!**********************************************************************************************************************

	subroutine neumannBoundaryDiffusionOnly(iBFace, aC, bC, fixedGrad, mu, fComp)

	!==================================================================================================================
	! Description:
	!! prescribedFluxBoundary computes the diagonal coefficient and rhs correction when a Neumann boundary condition is applied
	!!	(i.e. the boundary flux is prescribed).
	!! The correction reads:
	!! \begin{eqnarray}
	!!    &q_b = (\mu_b\nabla\phi_b)\cdot\textbf{S}_b \\
	!!    &a_C^b = m_f \\
	!!    &b_C^b = q_b = \mu\nabla\phi\cdot\textbf{n}
	!! \end{eqnarray}
	!==================================================================================================================

		use meshvar
		use massFlowVar

		implicit none

		integer :: iBFace, fComp
	!------------------------------------------------------------------------------------------------------------------

		real :: aC(fComp), bC(fComp), fixedGrad(fComp), flux(fComp), mu
	!------------------------------------------------------------------------------------------------------------------

		aC = 0.0
		flux = fixedGrad*norm2(mesh%Sf(iBFace,:))
		bC = mu*flux

	end subroutine neumannBoundaryDiffusionOnly

! *********************************************************************************************************************
