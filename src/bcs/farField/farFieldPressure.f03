!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

    subroutine farFieldPressureBoundary(iBFace, rhoDf, fluxCb)

!==================================================================================================================
! Description:
!! farFieldPressureBoundary switches between neumann0 and dirichelt boundary conditions as function of
!! the mass flow rate $m_f$ value. If $m_f>0$, then the flow is going outside the domain and dirichelt is applied, else
!! the pressure is extrapolated form interior as neumann0 appiles.
!==================================================================================================================

    use massFlowVar

    implicit none

    integer :: iBFace
!------------------------------------------------------------------------------------------------------------------

    real :: rhoDf, fluxCb
!------------------------------------------------------------------------------------------------------------------

    if(mf(iBFace,1)>0.0) then
        fluxCb = rhoDf
    else
        fluxCb = 0.0
    end if

end subroutine farFieldPressureBoundary

!**********************************************************************************************************************

subroutine updatefarFieldPressureBoundaryField(phi, phi0, iBoundary, fComp)

!==================================================================================================================
! Description:
!==================================================================================================================

    use meshvar
    use massFlowVar

    implicit none

    integer :: iBoundary, iBFace, is, ie, iOwner, fComp, patchFace
!------------------------------------------------------------------------------------------------------------------

    real :: phi(numberOfElements+numberOfBFaces, fComp)
    !! field values

    real :: phi0(fComp)
    !! value to be assigned at the boundary
!------------------------------------------------------------------------------------------------------------------

    is = mesh%boundaries%startFace(iBoundary)
    ie = is+mesh%boundaries%nFace(iBoundary)-1

    patchFace=numberOfElements + (is-numberOfIntFaces-1)

    do iBFace=is,ie
        patchFace = patchFace+1

        if(mf(iBFace,1)>0.0) then
            phi(patchFace,:) = phi0
        else
            iOwner = mesh%owner(iBFace)
            phi(patchFace,:)=phi(iOwner,:)
        end if

    enddo

end subroutine updatefarFieldPressureBoundaryField
