!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

    subroutine farFieldBoundary(iBFace, aC, bC, acd, acc, phib, mu, c, fComp)

    !==================================================================================================================
    ! Description:
    !! farFieldBoundary switches between neumann0 and dirichelt boundary conditions as function of
    !! the mass flow rate $m_f$ value. If $m_f>0$, then the flow is going outside the domain and neumann0 is applied, else
    !! the velocity is fixed as prescribed in the boundary condition file.
    !==================================================================================================================

    use meshvar
    use massFlowVar

    implicit none

    integer :: iBFace, fComp
!------------------------------------------------------------------------------------------------------------------

    real :: aC(fComp)

    real :: bC(fComp)
    !! diagonal coefficent correction

    real :: phib(fComp)
    !! rhs correction

    real :: mu
    !! diffusion coefficient

    real :: c
    !! switch for convection

    real :: acd(fComp)
    !! diffusion contribution

    real :: acc(fComp)
    !! convcetion contribution
!------------------------------------------------------------------------------------------------------------------

    if(mf(iBFace,1)>0.0) then
    
        aC = c*mf(iBFace,1)
        bC = 0.0

        acd = 0.0
        acc = c*mf(iBFace,1)

    else

        aC = mu*mesh%gDiff(iBFace)
        bC = -c*mf(iBFace,1)*phib + mu*mesh%gDiff(iBFace)*phib

        acd = mu*mesh%gDiff(iBFace)
        acc = 0.0

    end if

end subroutine farFieldBoundary

! *********************************************************************************************************************

subroutine updatefarFieldBoundaryField(phi, phi0, iBoundary, fComp)

!==================================================================================================================
! Description:
!! updatefarFieldBoundaryField loops over all the boundary faces of a target patch
!! and updates the field face values as function of the mass flow rate sign, as highlighted in the farFieldBoundary
!! description.
!==================================================================================================================

    use meshvar
    use massFlowVar

    implicit none

    integer :: iBoundary, iBFace, is, ie, iOwner, fComp, patchFace
!------------------------------------------------------------------------------------------------------------------

    real :: phi(numberOfElements+numberOfBFaces, fComp)
    !! field values

    real :: phi0(fComp)
    !! value to be assigned at the boundary
!------------------------------------------------------------------------------------------------------------------

    is = mesh%boundaries%startFace(iBoundary)
    ie = is+mesh%boundaries%nFace(iBoundary)-1

    patchFace = numberOfElements + (is-numberOfIntFaces-1)

    do iBFace=is,ie
        patchFace = patchFace+1

        if(mf(iBFace,1)>0.0) then
            iOwner = mesh%owner(iBFace)
            phi(patchFace,:) = phi(iOwner,:)
        else
            phi(patchFace,:) = phi0
        end if

    enddo

end subroutine updatefarFieldBoundaryField
