module themo

!==================================================================================================================
! Description:
!! themo contains thermophysical properties for temperature dependent fluids.
!==================================================================================================================

    use fieldvar, only : pressure, temperature
    use generalDictionaries
    use meshvar, only : numberOfElements, numberOfBFaces
    use physicalConstants

    implicit none

    type, public :: myThermo

        character(:), allocatable ::  fluidModel
        !! fluid model to use

        character(:), allocatable :: suntherland
        !! flag for suntherland law

        real :: beta
        !! Dilatation coefficient

        real :: T0
        !! Reference temperature

        real :: p0
        !! Reference pressure

        real :: rho0
        !! Reference density

        real :: As
        !! suntherland law constant As

        real :: TS
        !! suntherland law constant Ts

        real :: B
        !! adaibatic fluid constant

        real :: R
        !! Gas constant

        real :: gamma
        !! cv/cp

        contains

            procedure :: adiabaticFluid
            procedure :: boussinesq
            procedure :: compressibility
            procedure :: incompressible
            procedure :: incompressiblePerfectGas
            procedure :: linear
            procedure :: perfectGas
            procedure :: readThermoConstants
            procedure :: suntherlandLaw
            procedure :: updateThermoProperties

    end type myThermo

    ! create a themo object
    type(myThermo) :: thermo

contains

    subroutine readThermoConstants(this)

    !==================================================================================================================
    ! Description:
    !! Read density, viscosity and other parameters from a file.
    !==================================================================================================================

        class(myThermo) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        dictName = 'physical/themalConstants'
        call physical%initJSON(dictName)

        call physical%json%get('thermo%fluidModel', this%fluidModel, found)
        call raiseKeyErrorJSON('fluidModel', found, dictName)

        call physical%json%get('thermo%suntherlandLaw', this%suntherland, found)
        call raiseKeyErrorJSON('fluidModel', found, dictName)

        ! Suntherlad law
        if(this%suntherland=='yes') then

            call physical%json%get('thermo%sutherland%As', this%As, found)
            call raiseKeyErrorJSON('AS', found, dictName)

            call physical%json%get('thermo%sutherland%Ts', this%Ts, found)
            call raiseKeyErrorJSON('Ts', found, dictName)

        end if

        ! Density model
        if(this%fluidModel=='incompressible') then

            call physical%json%get('thermo%incompressible%rho0', this%rho0, found)
            call raiseKeyErrorJSON('rho0', found, dictName)

        elseif(this%fluidModel=='incompressiblePerfectGas') then

            call physical%json%get('thermo%incompressiblePerfectGas%p0', this%p0, found)
            call raiseKeyErrorJSON('p0', found, dictName)

            call physical%json%get('thermo%incompressiblePerfectGas%R', this%R, found)
            call raiseKeyErrorJSON('R', found, dictName)

        elseif(this%fluidModel=='adiabatic') then

            call physical%json%get('thermo%adiabatic%B', this%B, found)
            call raiseKeyErrorJSON('B', found, dictName)

            call physical%json%get('thermo%adiabatic%p0', this%p0, found)
            call raiseKeyErrorJSON('p0', found, dictName)

            call physical%json%get('thermo%adiabatic%gamma', this%gamma, found)
            call raiseKeyErrorJSON('gamma', found, dictName)

        elseif(this%fluidModel=='boussinesq') then

            call physical%json%get('thermo%boussinesq%beta', this%beta, found)
            call raiseKeyErrorJSON('beta', found, dictName)

            call physical%json%get('thermo%boussinesq%rho0', this%rho0, found)
            call raiseKeyErrorJSON('rho0', found, dictName)

            call physical%json%get('thermo%boussinesq%T0', this%T0, found)
            call raiseKeyErrorJSON('T0', found, dictName)

        elseif(this%fluidModel=='linear') then

            call physical%json%get('thermo%linear%p0', this%p0, found)
            call raiseKeyErrorJSON('p0', found, dictName)

        elseif(this%fluidModel=='perfectGas') then

            call physical%json%get('thermo%perfectGas%R', this%R, found)
            call raiseKeyErrorJSON('R', found, dictName)

        end if

    end subroutine readThermoConstants

! ********************************************************************************************************************!

    subroutine updateThermoProperties(this)

    !==================================================================================================================
    ! Description:
    !!  updateThermoProperties updates desnity and viscosity accoding to the thermal model.
    !==================================================================================================================

        class(myThermo) :: this
    !------------------------------------------------------------------------------------------------------------------

        if(this%fluidModel=='incompressible') then
            call this%incompressible()
        elseif(this%fluidModel=='incompressiblePerfectGas') then
            call this%incompressiblePerfectGas()
        elseif(this%fluidModel=='adiabatic') then
            call this%adiabaticFluid()
        elseif(this%fluidModel=='boussinesq') then
            call this%boussinesq()
        elseif(this%fluidModel=='linear') then
            call this%linear()
        elseif(this%fluidModel=='perfectGas') then
            call this%perfectGas()
        else
            call this%incompressible()
        end if

        ! suntherland if active
        if(this%suntherland=='yes') call this%suntherlandLaw()

    end subroutine updateThermoProperties

!**********************************************************************************************************************

    subroutine suntherlandLaw(this)

    !==================================================================================================================
    ! Description:
    !! sutherlandLaw computes the viscosity according to the suntherland law.
    !==================================================================================================================

        class(myThermo) :: this
    !------------------------------------------------------------------------------------------------------------------

        nu%phi(:,1) = this%As*sqrt(temperature%phi(:,1))/(1.0 + this%Ts/temperature%phi(:,1))

    end subroutine suntherlandLaw

!**********************************************************************************************************************

    subroutine incompressible(this)

    !==================================================================================================================
    ! Description:
    !! incompressible fixes the density to a constant value
    !==================================================================================================================

        class(myThermo) :: this
    !------------------------------------------------------------------------------------------------------------------

        rho%phi(:,1) = dens

    end subroutine incompressible

!**********************************************************************************************************************

    subroutine perfectGas(this)

    !==================================================================================================================
    ! Description:
    !! perfectGas computes the density for a perfect gas.
    !==================================================================================================================

        class(myThermo) :: this
    !------------------------------------------------------------------------------------------------------------------

        real :: psi(numberOfElements + numberOfBFaces)
    ! -----------------------------------------------------------------------------------------------------------------

        psi = this%compressibility()

        rho%phi(:,1) = psi*pressure%phi(:,1)

    end subroutine perfectGas

!**********************************************************************************************************************

    subroutine incompressiblePerfectGas(this)

    !==================================================================================================================
    ! Description:
    !! incompressiblePerfectGas computes the density for an incmpressible perfect gas.
    !==================================================================================================================

        class(myThermo) :: this
    !------------------------------------------------------------------------------------------------------------------

        real :: psi(numberOfElements + numberOfBFaces)
    ! -----------------------------------------------------------------------------------------------------------------

        psi = this%compressibility()

        rho%phi(:,1) = psi*this%p0

    end subroutine incompressiblePerfectGas

!**********************************************************************************************************************

    subroutine adiabaticFluid(this)

    !==================================================================================================================
    ! Description:
    !! adiabaticFluid computes the density for an adiabatic perfect fluid.
    !==================================================================================================================

        class(myThermo) :: this
    !------------------------------------------------------------------------------------------------------------------

        real :: gamma
    !------------------------------------------------------------------------------------------------------------------

        gamma = 1.0/this%gamma

        rho%phi(:,1) = this%rho0*((pressure%phi(:,1)+this%B))/(this%p0+this%B)**gamma

    end subroutine adiabaticFluid

!**********************************************************************************************************************

    subroutine boussinesq(this)

    !==================================================================================================================
    ! Description:
    !!  boussinesq computes the density according to the boussinesq appoximation.
    !==================================================================================================================

        class(myThermo) :: this
    !------------------------------------------------------------------------------------------------------------------

        rho%phi(:,1) = dens*(1.0-this%beta*(temperature%phi(:,1) - this%T0))

    end subroutine boussinesq

!**********************************************************************************************************************

    subroutine linear(this)

    !==================================================================================================================
    ! Description:
    !! linear computes the density according to a linear relation.
    !==================================================================================================================

        class(myThermo) :: this
    !------------------------------------------------------------------------------------------------------------------

        real :: psi(numberOfElements + numberOfBFaces)
    ! -----------------------------------------------------------------------------------------------------------------

        psi = this%compressibility()

        rho%phi(:,1) = psi*pressure%phi(:,1) + this%rho0

    end subroutine linear

!**********************************************************************************************************************

    function compressibility(this) result(psi)

    !==================================================================================================================
    ! Description:
    !! psi computes and returns the cmpressibility.
    !==================================================================================================================

        class(myThermo) :: this
    !------------------------------------------------------------------------------------------------------------------

        real :: psi(numberOfElements + numberOfBFaces)
    ! -----------------------------------------------------------------------------------------------------------------

        psi = 1.0/(this%R*temperature%phi(:,1))

    end function compressibility

!**********************************************************************************************************************

end module themo
