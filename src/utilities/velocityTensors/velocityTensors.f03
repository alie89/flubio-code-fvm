!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!
module velocityTensors

    use globalMeshVar
    use fieldvar
    use physicalConstants
    use ttbTensors

    implicit none

contains

    function strainTensor(phi) result(S)

    !==================================================================================================================
    ! Description:
    !! strainTensor computes the stranin tensor and its inner product.
    !==================================================================================================================

        type(flubioField) :: phi

        type(ttbTensor2) :: G

        type(ttbTensor2S) :: S
    !------------------------------------------------------------------------------------------------------------------

        call G%create(dim=numberOfElements+numberOfBFaces, phi=velocity%phiGrad(1:numberOfElements+numberOfBFaces, 1:3, 1:3))
        S = symm(G)

    end function strainTensor

! *********************************************************************************************************************

    function rotationTensor(phi) result(R)

    !==================================================================================================================
    ! Description:
    !! strainTensor computes the stranin tensor and its inner product.
    !==================================================================================================================

        type(flubioField) :: phi

        type(ttbTensor2) :: G

        type(ttbTensor2) :: R
    !------------------------------------------------------------------------------------------------------------------

        call G%create(dim=numberOfElements+numberOfBFaces, phi=velocity%phiGrad(1:numberOfElements+numberOfBFaces, 1:3, 1:3))
        R = omega(G)

    end function rotationTensor

! *********************************************************************************************************************

    function lighthillTensor(rho0, viscous) result(L)

    !==================================================================================================================
    ! Description:
    !! strainTensor computes the stranin tensor and its inner product.
    !==================================================================================================================

        type(flubioField) :: phi

        type(ttbTensor2) :: G

        type(ttbTensor2) :: tau

        type(ttbTensor2) :: L
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iComp
    !------------------------------------------------------------------------------------------------------------------

        logical, optional :: viscous

        logical :: includeViscousTensor
    !------------------------------------------------------------------------------------------------------------------

        real :: rho0

        real :: tmp(numberOfElements+numberOfBFaces, 3, 3)
    !------------------------------------------------------------------------------------------------------------------

        if(present(viscous)) then 
            includeViscousTensor = .True.
        else 
            includeViscousTensor = viscous
        endif 

        do iElement=1,numberOfElements+numberOfBFaces
            do iComp=1,velocity%nComp 

                tmp(iElement,1,1) = rho0*velocity%phi(iElement,1)**2
                tmp(iElement,1,2) = rho0*velocity%phi(iElement,1)*velocity%phi(iElement,2)
                tmp(iElement,1,3) = rho0*velocity%phi(iElement,1)*velocity%phi(iElement,3)

                tmp(iElement,2,1) = rho0*velocity%phi(iElement,2)*velocity%phi(iElement,1)
                tmp(iElement,2,2) = rho0*velocity%phi(iElement,2)**2
                tmp(iElement,2,3) = rho0*velocity%phi(iElement,2)*velocity%phi(iElement,3) 

                tmp(iElement,3,1) = rho0*velocity%phi(iElement,3)*velocity%phi(iElement,1) 
                tmp(iElement,3,2) = rho0*velocity%phi(iElement,3)*velocity%phi(iElement,2)
                tmp(iElement,3,3) = rho0*velocity%phi(iElement,3)**2 

            enddo
        enddo 

        call L%create(dim=numberOfElements + numberOfBFaces, phi=tmp)

        if(includeViscousTensor) then

            do iElement=1,numberOfElements+numberOfBFaces
                tmp(iElement,:,:) = nu%phi(iElement,1)*velocity%phiGrad(iElement,:,:)
            enddo 

            call G%create(dim=numberOfElements + numberOfBFaces, phi=tmp)
            tau = G + transpose(G)
            L = L - tau
            
        endif 

    end function lighthillTensor

! *********************************************************************************************************************

    function strainTensorModule() result(modS)

    !==================================================================================================================
    ! Description:
    !! strainTensorModule computes the stranin tensor module.
    !==================================================================================================================

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real ::  modS(numberOfElements)
        !! Strain rate module

        real :: S(6), Ssum
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements

            ! Diagonal
            S(1) = velocity%phiGrad(iElement,1,1) ! dudx
            S(2) = velocity%phiGrad(iElement,2,2) ! dvdy
            S(3) = velocity%phiGrad(iElement,3,3) ! dwdz

            ! Off-diagonal
            S(4) = 0.5*(velocity%phiGrad(iElement,1,2) + velocity%phiGrad(iElement,2,1)) ! 0.5*(dvdx+dudy)
            S(5) = 0.5*(velocity%phiGrad(iElement,1,3) + velocity%phiGrad(iElement,3,1)) ! 0.5*(dwdx+dudz)
            S(6) = 0.5*(velocity%phiGrad(iElement,2,3) + velocity%phiGrad(iElement,3,2)) ! 0.5*(dwdy+dvdz)

            ! SijSij (inner product)
            Ssum = (S(1)**2+S(2)**2+S(3)**2) + 2*(S(4)**2+S(5)**2+S(6)**2)

            ! Module: modS=sqrt(2*Ssum)
            modS(iElement) = sqrt(2*Ssum)

        enddo

    end function strainTensorModule

!**********************************************************************************************************************

end module velocityTensors