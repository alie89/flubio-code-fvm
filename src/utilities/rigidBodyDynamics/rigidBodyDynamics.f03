module rigidBodyDynamics

    use math, only: matInv3, cross_product

    implicit none

contains 

function Rq(quat) result(R)

    !==================================================================================================================
    ! Description:
    !! Rq returns the rotation matrix to rotate the coordinate system from body coordinates to fixed earth
    !==================================================================================================================

        real :: quat(4)
        !! quaternion

        real :: R(3,3)
        !! rotation matrix

        real :: e0, ex, ey, ez
        !! quaternion components
    !------------------------------------------------------------------------------------------------------------------ 

        e0 = quat(1)
        ex = quat(2) 
        ey = quat(3)  
        ez = quat(4)

        ! First row
        R(1,1) = ex**2 + e0**2 - ey**2 - ez**2
        R(1,2) = 2.0*(ex*ey + ez*e0)
        R(1,3) = 2.0*(ex*ez - ey*e0)

        ! Second row
        R(2,1) = 2.0*(ex*ey - ez*e0)
        R(2,2) = ey**2 + e0**2 - ex**2 - ez**2 
        R(2,3) = 2.0*(ey*ez + ex*e0)

        ! Third row
        R(3,1) = 2.0*(ex*ez + ey*e0)
        R(3,2) = 2.0*(ey*ez - ex*e0)
        R(3,3) = ez**2 + e0**2 - ex**2 - ey**2

    end function Rq

!**********************************************************************************************************************

    function invRq(quat) result(R)

    !==================================================================================================================
    ! Description:
    !! invRq returns the rotation matrix to rotate the coordinate system from fixed earth coordinates to body coordinates
    !==================================================================================================================

        real :: quat(4)
        !! quaternion

        real :: R(3,3)
        !! rotation matrix

        real :: e0, ex, ey, ez
        !! quaternion components
    !------------------------------------------------------------------------------------------------------------------ 

        e0 = quat(1)
        ex = quat(2) 
        ey = quat(3)  
        ez = quat(4)

        ! First row
        R(1,1) = ex**2 + e0**2 - ey**2 - ez**2
        R(1,2) = 2.0*(ex*ey - ez*e0)
        R(1,3) = 2.0*(ex*ez + ey*e0)

        ! Second row
        R(2,1) = 2.0*(ex*ey + ez*e0)
        R(2,2) = ey**2 + e0**2 - ex**2 - ez**2 
        R(2,3) = 2.0*(ey*ez - ex*e0)

        ! Third row
        R(3,1) = 2.0*(ex*ez - ey*e0)
        R(3,2) = 2.0*(ey*ez + ex*e0)
        R(3,3) = ez**2 + e0**2 - ex**2 - ey**2

    end function invRq

!**********************************************************************************************************************

    function Reu(angles) result(R)

    !==================================================================================================================
    ! Description:
    !! Reu returns the rotation matrix to rotate the coordinate system using euler angles.
    !! phi -> x, theta -> y, psi -> z
    !==================================================================================================================

        real :: angles(3)
        !! quaternion

        real :: R(3,3)
        !! rotation matrix

        real :: phi, theta, psi
        !! angles components
    !------------------------------------------------------------------------------------------------------------------ 

        phi = angles(1)
        theta = angles(2) 
        psi = angles(3)  

        ! First row
        R(1,1) = cos(theta)*cos(psi)
        R(1,2) = cos(theta)*sin(psi)
        R(1,3) = -sin(theta)

        ! Second row
        R(2,1) = sin(phi)*sin(theta)*cos(psi) - cos(phi)*sin(psi)
        R(2,2) = sin(phi)*sin(theta)*sin(psi) + cos(phi)*cos(psi)
        R(2,3) = sin(phi)*cos(theta)

        ! Third row
        R(3,1) = cos(phi)*sin(theta)*cos(psi) + sin(phi)*sin(psi)
        R(3,2) = cos(phi)*sin(theta)*sin(psi) - sin(phi)*cos(psi)
        R(3,3) = cos(phi)*cos(theta)

    end function Reu

!**********************************************************************************************************************

    function invReu(angles) result(R)

    !==================================================================================================================
    ! Description:
    !! invReu returns the rotation matrix to rotate the coordinate system from fixed earth coordinates to body coordinates.
    !! phi -> x, theta -> y, psi -> z
    !==================================================================================================================

        real :: angles(3)
        !! quaternion

        real :: R(3,3)
        !! rotation matrix

        real :: phi, theta, psi
        !! angles components
    !------------------------------------------------------------------------------------------------------------------ 
        
        phi = angles(1)
        theta = angles(2) 
        psi = angles(3)  

        ! First row
        R(1,1) = cos(theta)*cos(psi)
        R(1,2) = sin(phi)*sin(theta)*cos(psi) - cos(phi)*sin(psi)
        R(1,3) = cos(phi)*sin(theta)*cos(psi) + sin(phi)*sin(psi)

        ! Second row
        R(2,1) = cos(theta)*sin(psi)
        R(2,2) = sin(phi)*sin(theta)*sin(psi) + cos(phi)*cos(psi)
        R(2,3) = cos(phi)*sin(theta)*sin(psi) - sin(phi)*cos(psi)
    
        ! Third row
        R(3,1) = -sin(theta)
        R(3,2) = sin(phi)*cos(theta)
        R(3,3) = cos(phi)*cos(theta)

    end function invReu

!**********************************************************************************************************************

    function invReu2D(theta) result(R)

    !==================================================================================================================
    ! Description:
    !! invReu2D returns the rotation matrix for 2D rotation around z-axis.
    !! phi -> x, theta -> y, psi -> z
    !==================================================================================================================
    
        real :: R(3,3)
        !! rotation matrix
    
        real :: theta
    !------------------------------------------------------------------------------------------------------------------ 
            
        ! First row
        R(1,1) = cos(theta)
        R(1,2) = -sin(theta)
        R(1,3) = 0.0
    
        ! Second row
        R(2,1) = sin(theta)
        R(2,2) = cos(theta)
        R(2,3) = 0.0
        
        ! Third row
        R(3,1) = 0.0
        R(3,2) = 0.0
        R(3,3) = 1.0
    
    end function invReu2D

!**********************************************************************************************************************

    function bodyToFixed(quat, vb) result(vf)

    !==================================================================================================================
    ! Description:
    !! bodyToFixed rotates a vector to fixed earth coordinates.
    !==================================================================================================================

        real :: vb(3)
        !! vector in body coordinates

        real :: vf(3)
        !! vector in fixed earth coordinates

        real :: tmpvf(3,1)
        !! vector in body coordinates

        real :: tmpvb(3,1)
        !! vector in fixed earth coordinates

        real :: quat(4)
        !! quaternion

        real :: R(3,3)
        !! rotation matrix
    !------------------------------------------------------------------------------------------------------------------

        tmpvb(:,1) = vb

        R = invRq(quat) 

        tmpvf = matmul(R, tmpvb)

        vf = tmpvf(:,1)

    end function bodyToFixed

!**********************************************************************************************************************
    
    function fixedToBody(quat, vf) result(vb)

    !==================================================================================================================
    ! Description:
    !! fixedToBody rotates a vector to body coordinates.
    !==================================================================================================================

        real :: vb(3)
        !! vector in body coordinates

        real :: vf(3)
        !! vector in fixed earth coordinates

        real :: tmpvf(3,1)
        !! vector in body coordinates

        real :: tmpvb(3,1)
        !! vector in fixed earth coordinates

        real :: quat(4)
        !! quaternion

        real :: R(3,3)
        !! rotation matrix
    !------------------------------------------------------------------------------------------------------------------

        tmpvf(:,1) = vf

        R = Rq(quat)

        tmpvb = matmul(R, tmpvf)

        vb = tmpvb(:,1)

    end function fixedToBody
    
!**********************************************************************************************************************

    function quaterionToEuler(quat, psi0) result(eu)

    !==================================================================================================================
    ! Description:
    !! quaterionToEuler computes the Euler's angles from quaternion.
    !==================================================================================================================    

        real :: eu(3)
        !! euler angle vectors 

        real :: quat(4)
        !! quaternion

        real :: e0, ex, ey, ez
        !! quaternion component

        real :: pi
        !! greek pi

        real, optional :: psi0
        !! arbitrary psi

        real :: psi
        !! arbitray psi
    !------------------------------------------------------------------------------------------------------------------

        if(present(psi0)) then 
            psi = psi0
        else 
            psi = 0.0
        endif
        pi = acos(-1.0)

        e0 = quat(1)
        ex = quat(2)
        ey = quat(3)
        ez = quat(4)

        if(e0*ey-ex*ez==0.5) then

            ! theta
            eu(1) = 2.0*asin(ex/cos(pi/4) + psi0)

            ! phi
            eu(2) = pi/2.0

            ! psi
            eu(3) = 0.0

        elseif(e0*ey-ex*ez==-0.5) then 

            ! theta
            eu(1) = 2.0*asin(ex/cos(pi/4) - psi0)

            ! phi
            eu(2) = -pi/2.0
        
            ! psi
            eu(3) = 0.0

        else 

            ! theta
            eu(1) = atan2( 2.0*(e0*ex + ey*ez), e0**2 + ez**2 - ex**2 - ey**2 )

            ! phi
            eu(2) = asin(2.0*(e0*ey - ex*ez))
        
            ! psi
            eu(3) = atan2( 2.0*(e0*ez + ex*ey), e0**2 + ex**2 - ey**2 - ez**2 )

        endif 

    end function quaterionToEuler

!**********************************************************************************************************************

    function eulerToQuaternion(eu) result(quat)

    !==================================================================================================================
    ! Description:
    !! quaterionToEuler computes the Euler's angles from quaternion.
    !==================================================================================================================    
    
        real :: eu(3)
        !! euler angle vectors 
    
        real :: quat(4)
        !! quaternion
    
        real :: phi, theta, psi
        !! Euler's angles
    !------------------------------------------------------------------------------------------------------------------

        phi = 0.5*eu(1) 
        theta = 0.5*eu(2)
        psi = 0.5*eu(3)

        quat(1) = cos(phi)*cos(theta)*cos(psi) + sin(phi)*sin(theta)*sin(psi) 
        quat(2) = sin(phi)*cos(theta)*cos(psi) - cos(phi)*sin(theta)*sin(psi)
        quat(3) = cos(phi)*sin(theta)*cos(psi) + sin(phi)*cos(theta)*sin(psi)
        quat(4) = cos(phi)*cos(theta)*sin(psi) - sin(phi)*sin(theta)*cos(psi)

    end function eulerToQuaternion  

!**********************************************************************************************************************

    function eulerToAngular(eu) result(R)

        real :: eu(3)
        !! Euler angles

        real :: R(3,3)
        !! rotation matrix

        real :: phi, theta, psi
    !-----------------------------------------------------------------------------------------------------------------

        phi = eu(1) 
        theta = eu(2) 
        psi = eu(3)

        R(1,1) = 1.0
        R(1,2) = 0.0
        R(1,3) = -sin(theta)

        R(2,1) = 0.0
        R(2,2) = cos(phi)
        R(2,3) = sin(phi)*cos(theta)

        R(3,1) = 0.0
        R(3,2) = -sin(phi)
        R(3,3) = cos(phi)*cos(theta)

    end function eulerToAngular

!**********************************************************************************************************************

    function newtonEquation(mass, u, omega, F) result(udot) 

    !==================================================================================================================
    ! Description:
    !! newtonEquation implements the newton equation for rigid body translation.
    !==================================================================================================================   
        
        real :: mass
        !! body mass

        real :: F(3)
        !! external forces in body oordinates

        real :: omega(3)
        !! angular velocity in body coordinates

        real :: u(3)
        !! velocity in body coordinates

        real :: udot(3)
        !! acceleration in body coordinates
    !------------------------------------------------------------------------------------------------------------------

        udot = F/mass - cross_product(omega, u)

    end function newtonEquation

!**********************************************************************************************************************  

    function eulerEquation(I, omega, M) result(omegadot) 

    !==================================================================================================================
    ! Description:
    !! eulerEquation implements the euler equation for rigid body rotation.
    !==================================================================================================================   
            
        real :: I(3,3)
        !! tensor of inertia

        real :: invI(3,3)
        !! tensor of inertia inverse

        real :: M(3)
        !! external moments in body oordinates
    
        real :: omega(3)
        !! angular velocity in body coordinates

        real :: omegadot(3)
        !! rotation rate in body coordinates

        real :: Iomega(3,1)
        !! I*omega

        real :: IM(3,1)
        !! I^-1*M

        real :: mtmp(3,1)
        !! moments as matrix

        real :: omegatmp(3,1)
        !! moments as matrix
    !------------------------------------------------------------------------------------------------------------------
    
        mtmp(:,1) = M
        omegatmp(:,1) = omega

        invI = matInv3(I)

        Iomega = matmul(I, omegatmp)

        IM = matmul(invI, mtmp)

        omegadot = IM(:,1) - cross_product(omega,Iomega(:,1)) 
    
    end function eulerEquation

!**********************************************************************************************************************

    function kinematics(u) result(xdot) 

    !==================================================================================================================
    ! Description:
    !! kinematics implements the simple kinematics relation between velocity and poistion in body coordiantes.
    !==================================================================================================================   
                
        real :: u(3)
        !! velocity in body coordinates

        real :: xdot(3)
        !! velocity in body coordinates
    !------------------------------------------------------------------------------------------------------------------
    
        xdot = u
        
    end function kinematics

!**********************************************************************************************************************

    function kinematicsFixedEarth(quat,u) result(xdot) 

    !==================================================================================================================
    ! Description:
    !! kinematics implements the simple kinematics relation between velocity and poistion in body coordiantes.
    !==================================================================================================================   
        
        real :: quat(4)

        real :: u(3)
        !! velocity in body coordinates
    
        real :: xdot(3)
        !! velocity in body coordinates

        real :: R(3,3)

        real :: tmpu(3,1), tmpv(3,1)
    !------------------------------------------------------------------------------------------------------------------
        
        tmpu(:,1) = u

        R = invRq(quat) 

        tmpv = matmul(R, tmpu)

        xdot = tmpv(:,1)
            
    end function kinematicsFixedEarth

!**********************************************************************************************************************

    function quatKinematics(omega,e) result(edot) 

    !==================================================================================================================
    ! Description:
    !! quatKinematics implements the quaternion kinematics equations.
    !==================================================================================================================   
                    
        real :: omega(3)
        !! angular velocity in body coordinates
    
        real :: e(4)
        !! quaternion

        real :: tmp(4,1)
        !! quaternion as matrix

        real :: tmpdot(4,1)
        !! quaternion as matrix

        real :: edot(4)
        !! quaternion rate of change

        real :: M(4,4)
        !! matrix for quaternion kinematics equation

        real :: p, q, r 
        !! omegadot componets
    !------------------------------------------------------------------------------------------------------------------
        
        p = omega(1) 
        q = omega(2)
        r = omega(3)

        tmp(:,1) = e

        M(1,1) = 0.0
        M(1,2) = -p
        M(1,3) = -q
        M(1,4) = -r
        
        M(2,1) = p
        M(2,2) = 0.0
        M(2,3) = r 
        M(2,4) = -q

        M(3,1) = q 
        M(3,2) = -r 
        M(3,3) = 0.0
        M(3,4) = p

        M(4,1) = r 
        M(4,2) = q
        M(4,3) = -p
        M(4,4) = 0.0

        tmpdot = 0.5*matmul(M,tmp)
        edot = tmpdot(:,1)

    end function quatKinematics

!**********************************************************************************************************************

    function adamsBashforth4(h, state0, res, Fb, Mb, m, I, itime) result(state)

    !==================================================================================================================
    ! Description:
    !! adamBashforth4 implemens the 4th order Adams-Bashforth explicit time scheme
    !==================================================================================================================   

        integer :: itime
        !! time step number
    !------------------------------------------------------------------------------------------------------------------

        real :: AB(4)
        !! Adams-Bashforth coefficients

        real :: state(13)
        !! Rigid body state vector. 
        !! state(1:3) = velocity, state(4:6) = rotation rate, state(7:9) = position, state(10:13) = quaternion

        real :: state0(13)
        !! Old state
    
        real :: res(13,4)
        !! equations residuals for the last 4 timesteps

        real:: h
        !! time step size

        real :: m
        !! body mass
        
        real :: I(3,3)
        !! tensor of inertia
        
        real :: Fb(3)
        !! Net force on the body
        
        real :: Mb(3)
        !! Net moment on the body
    !------------------------------------------------------------------------------------------------------------------

        ! Update the residual vector
        res(:,4) = res(:,3)
        res(:,3) = res(:,2)
        res(:,2) = res(:,1)

        state = state0

        ! Euler
        if(itime==1) then 
        
            AB(1) = 1.0
            AB(2) = 0.0
            AB(3) = 0.0
            AB(4) = 0.0

        ! Second order
        elseif(itime ==2) then 

            AB(1) = 3.0/2.0
            AB(2) = -1.0/2.0
            AB(3) = 0.0
            AB(4) = 0.0

        ! Third order
        elseif(itime==3) then
        
            AB(1) =  23.0/12.0
            AB(2) = -16.0/12.0
            AB(3) =  5.0/12.0
            AB(4) =  0.0

        ! Forth order
        else

            AB(1) =  55.0/24.0
            AB(2) = -59.0/24.0
            AB(3) =  37.0/24.0
            AB(4) = -9.0/24.0

        endif

        ! Compute new residuals
        res(1:3,1) = newtonEquation(mass=m, u=state(1:3), omega=state(4:6), F=Fb)

        res(4:6,1) = eulerEquation(I=I, omega=state(4:6), M=Mb)

        res(7:9,1) = kinematicsFixedEarth(quat=state(10:13), u=state(1:3))

        res(10:13,1) = quatKinematics(omega=state(4:6), e=state(10:13))

        ! Update the state using Adams-Bashforth
        state = state0 + h*(AB(1)*res(:,1) + AB(2)*res(:,2) + AB(3)*res(:,3) + AB(4)*res(:,4))

        ! normalize q
        !state(10:13) = state(10:13)/sqrt(state(10)**2+state(11)**2+state(12)**2+state(13)**2)

    end function adamsBashforth4

!**********************************************************************************************************************

end module rigidBodyDynamics
