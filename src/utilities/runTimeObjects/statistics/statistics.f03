!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module statistics

!==================================================================================================================
! Description:
!! This module implements the data structures and the methods to collect flow statistics.
!==================================================================================================================

    use flubioDictionaries
    use fieldvar
    use globalMeshVar
    use physicalConstants
    use runTimeObject

    implicit none

    type, public, extends(runTimeObj) :: flowStats

        character(len=:), dimension(:), allocatable :: avgFields
        !! field to average

        integer :: statsFlag
        !! flag to collect statistics

        integer :: fstats
        !! samplig frequency

        integer, dimension(:), allocatable :: fieldIndex
        !! field index in the registry

        real :: startStat
        !! start collecting sample after startStat

        real :: endStat
        !! stop collecting sample after startStat

        real :: tsample
        !! number of time/iteration samples

        integer :: numberOfAvgFields
        !! number of fields to average

        type(flubioField), dimension(:), allocatable :: meanFields
        !! field to mean

        type(flubioField), dimension(:), allocatable :: meanRMS
        !! field to mean

        logical, dimension(:), allocatable :: fieldsFound
        !! fields found in the registry

        logical :: rms
        !! flag to compute rms

    contains

        procedure :: initialise => createStatistics
        procedure :: run => updateStatistics

    end type flowStats

contains

! *********************************************************************************************************************

    subroutine createStatistics(this)

    !==================================================================================================================
    ! Description:
    !! createStatistics is the class constructor.
    !==================================================================================================================

        class(flowStats) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: optName

        character(len=:), allocatable :: classType

        character(len=:), allocatable :: rms
    !------------------------------------------------------------------------------------------------------------------

        integer :: avg, fieldIndex, nComp

        integer, dimension(:), allocatable :: ilen
    !------------------------------------------------------------------------------------------------------------------    

        logical :: found, monitorsFound, monitorFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------

        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)

        ! Get monitors pointer
        call jcore%get_child(monitorsPointer, this%objName, monitorPointer, monitorFound)

        call jcore%get(monitorPointer, 'startAveraging', this%startStat, found)
        call raiseKeyErrorJSON('startAveraging', found, 'settings/monitors/'//this%objName)

        call jcore%get(monitorPointer, 'endAveraging', this%endStat, found)
        call raiseKeyErrorJSON('endAveraging', found, 'settings/monitors/'//this%objName)

        call jcore%get(monitorPointer, 'saveEvery', this%saveEvery, found)
        call raiseKeyErrorJSON('saveEvery', found, 'settings/monitors/'//this%objName)

        call jcore%get(monitorPointer, 'collectEvery',this%fstats, found)
        call raiseKeyErrorJSON('collectEvery', found, 'settings/monitors/'//this%objName)

        call jcore%get(monitorPointer, 'rms',rms, found, 'settings/monitors/'//this%objName)

        this%rms = .false.
        if(lowercase(rms)=="yes" .or. lowercase(rms)=="on" .or. lowercase(rms)=="true") this%rms = .true.

        call jcore%get(monitorPointer, 'fields', this%avgFields, ilen, found)
        call raiseKeyErrorJSON('fields', found, 'settings/monitors/'//this%objName)

        this%numberOfAvgFields = size(this%avgFields)

        ! Allocate
        this%tsample = 0.0
        allocate(this%fieldsFound(this%numberOfAvgFields))
        allocate(this%fieldIndex(this%numberOfAvgFields))
        allocate(this%meanFields(this%numberOfAvgFields))
        allocate(this%meanRMS(this%numberOfAvgFields))

        do avg=1,this%numberOfAvgFields
            found = checkRegistry(trim(this%avgFields(avg)))
            this%fieldsFound(avg) = found
        enddo

        ! Create averages
        do avg=1,this%numberOfAvgFields

             fieldIndex = getFieldLocationInRegistry(trim(this%avgFields(avg)))
             this%fieldIndex(avg) = fieldIndex
             nComp = fieldRegistry(fieldIndex)%field%nComp
             classType = fieldRegistry(fieldIndex)%field%bcClassType

             call this%meanFields(avg)%createField(fieldName=trim(this%avgFields(avg))//"Mean", classType=classType, nComp=nComp)
             if(this%rms) call this%meanRMS(avg)%createField(fieldName=trim(this%avgFields(avg))//"RMS", classType=classType, nComp=nComp)
    
        enddo

        ! Clean up
        nullify(dictPointer)
        if(monitorsFound) nullify(monitorsPointer)
        if(monitorFound) nullify(monitorPointer)
        call jcore%destroy()

    end subroutine createStatistics

! *********************************************************************************************************************

    subroutine resetStatistics(this)

    !==================================================================================================================
    ! Description:
    !! resetStatistics resets the statistics.
    !==================================================================================================================

        class(flowStats) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: avg
    !------------------------------------------------------------------------------------------------------------------

        this%tsample = 0.0

        ! Create averages
        do avg=1,this%numberOfAvgFields
            this%meanFields(avg)%phi = 0.0
            if(this%rms) this%meanRMS(avg)%phi = 0.0
       enddo

    end subroutine resetStatistics

! *********************************************************************************************************************

    subroutine destroyStatistics(this)

    !==================================================================================================================!
    ! Description:
    !! destroyStatistics deletes statistics from the memory.
    !==================================================================================================================!

        class(flowStats) :: this
    !------------------------------------------------------------------------------------------------------------------

        deallocate(this%avgFields)
        deallocate(this%meanFields)
        if(this%rms) deallocate(this%meanRMS)

    end subroutine destroyStatistics

! *********************************************************************************************************************

    subroutine updateStatistics(this)

    !==================================================================================================================
    ! Description:
    !! updateStatistics updates the flow statistics.
    !==================================================================================================================

        class(flowStats) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer:: i, avg, nComp
    !------------------------------------------------------------------------------------------------------------------

        real, dimension(:), allocatable :: rms(:,:)
    !------------------------------------------------------------------------------------------------------------------
        
        !--------------------!
        ! Collect Statistics !
        !--------------------!

        if(time>this%startStat .and. time<this%endStat .and. mod(itime,this%fstats)==0) then

            call flubioMsg("  FLUBIO: calculating averaging")

            this%tsample = this%tsample+1.0
          
            do avg=1,this%numberOfAvgFields

                i = this%fieldIndex(avg)
                nComp = fieldRegistry(i)%field%nComp
                this%meanFields(avg)%phi = (this%meanFields(avg)%phi + fieldRegistry(i)%field%phi)/this%tsample

                ! Write statistics to file
                if(mod(itime,this%saveEvery)==0 .or. abs(this%saveEvery)==1) call this%meanFields(avg)%writeToFileW()

                if(this%rms) then 

                    allocate(rms(numberOfElements+numberOfBFaces, nComp))
                    rms = 0.0

                    rms = fieldRegistry(i)%field%phi - this%meanFields(avg)%phi
                    this%meanRMS(avg)%phi = (this%meanRMS(avg)%phi + rms)/this%tsample

                    deallocate(rms)

                    ! Write statistics to file
                    if(mod(itime,this%saveEvery)==0 .or. abs(this%saveEvery)==1) call this%meanRMS(avg)%writeToFileW()

                endif    

            enddo    
            
        endif

    end subroutine updateStatistics

! *********************************************************************************************************************

end module statistics