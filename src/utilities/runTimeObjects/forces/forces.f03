!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module forceMonitors

!==================================================================================================================
! Description:
!! This module implements the data structure and the mehods to perform force monitors over patches.
!==================================================================================================================

    use flubioDictionaries
    use fieldvar
    use meshvar
    use physicalConstants
    use runTimeObject
    use m_strings
    use interpolation

    implicit none

    type, public, extends(runTimeObj) :: forceCoeffs

        logical :: isActive
        !! Switch it on and off

        character(len=:), allocatable :: dictName
        !! Name of the dictionary to hook up

        character(len=50), dimension(:), allocatable :: patchNames
        !! where to calculate forces, but split by comma

        integer :: frequency
        !! frequency required to compute forces

        real :: rhoRef
        !! reference density

        real :: Uref
        !! reference velocity

        real :: Pref
        !! reference pressure

        real :: Aref
        !! reference area

        integer, dimension(:), allocatable :: is
        !! start index

        integer, dimension(:), allocatable :: ie
        !! end index

        integer, dimension(:), allocatable :: nFaces
        !! patch faces

        integer, dimension(:), allocatable :: pIndex
        !! patch index

    contains

        procedure :: initialise => createForceCoeffs
        procedure :: run => aeroCoeffs

    end type forceCoeffs

    type, public, extends(runTimeObj) :: Cp

        logical :: isActive
        !! Switch it on and off

        character(len=:), allocatable :: dictName
        !! Name of the dictionary to hook up

        character(len=50), dimension(:), allocatable :: patchNames
        !! where to calculate forces, but split by comma

        integer :: frequency
        !! frequency required to compute forces

        real :: rhoRef
        !! reference density

        real :: Uref
        !! reference velocity

        real :: Pref
        !! reference pressure

        real :: Aref
        !! reference area

        integer, dimension(:), allocatable :: is
        !! start index

        integer, dimension(:), allocatable :: ie
        !! end index

        integer, dimension(:), allocatable :: nFaces
        !! patch faces

        integer, dimension(:), allocatable :: pIndex
        !! patch index

    contains

        procedure :: initialise => createPressureCoeff
        procedure :: run => pressureCoeff

    end type Cp

    type, public, extends(runTimeObj) :: Cf

        logical :: isActive
        !! Switch it on and off

        character(len=:), allocatable :: dictName
        !! Name of the dictionary to hook up

        character(len=50), dimension(:), allocatable :: patchNames
        !! where to calculate forces, but split by comma

        integer :: frequency
        !! frequency required to compute forces

        real :: rhoRef
        !! reference density

        real :: Uref
        !! reference velocity

        real :: Aref
        !! reference area

        integer, dimension(:), allocatable :: is
        !! start index

        integer, dimension(:), allocatable :: ie
        !! end index

        integer, dimension(:), allocatable :: nFaces
        !! patch faces

        integer, dimension(:), allocatable :: pIndex
        !! patch index

    contains

        procedure :: initialise => createFrictionCoeff
        procedure :: run => skinFrictionCoeff2
        procedure :: skinFrictionCoeff2

    end type Cf

contains

    subroutine createForceCoeffs(this)

    !==================================================================================================================
    ! Description:
    !! createForcesMonitos is the class constructor.
    !==================================================================================================================

        class(forceCoeffs) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: optName
        !! option name

        character(len=:), allocatable :: val
        !! value

        character(len=:), dimension(:), allocatable :: acvec
        !! first array element lenght
    !------------------------------------------------------------------------------------------------------------------

        integer, dimension(:),allocatable :: ilen
        !! first array element lenght

        integer :: i, nPatches
    !------------------------------------------------------------------------------------------------------------------

        logical :: found, monitorsFound, monitorFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------

        this%dictName = this%objName

        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)

        ! Get monitors pointer
        call jcore%get_child(monitorsPointer, this%dictName, monitorPointer, monitorFound)

        !------------------------------!
        ! Get data from the dictionary !
        !------------------------------!

        ! Active
        call jcore%get(monitorPointer, 'active', val, found)
        if(.not. found) this%isActive = .true.

        if(found .and. (val=='yes' .or. val=='true') )  then 
            this%isActive = .true.
        else 
            this%isActive = .false.
        endif

        ! Patches
        call jcore%get(monitorPointer, 'patches', acvec, ilen, found)
        call raiseKeyErrorJSON(this%dictName//'%patches', found, 'settings/monitors/'//this%objName)

        nPatches = size(acvec)
        allocate(this%patchNames(nPatches))
        allocate(this%is(nPatches))
        allocate(this%ie(nPatches))
        allocate(this%nFaces(nPatches))
        allocate(this%pIndex(nPatches))

        do i=1,nPatches
            this%patchNames(i) = adjustl(trim(acvec(i)))
        end do

        ! Start and end pathc index
        call mesh%boundaries%findBoundaryIndices(this%patchNames, this%is, this%ie, this%nFaces, this%pIndex, nPatches)

        ! Execution frequency
        call jcore%get(monitorPointer, 'outputFrequency', this%frequency, found)
        call raiseKeyErrorJSON(this%dictName//'%outputFrequency', found, 'settings/monitors/'//this%objName)

        ! Reference values
        call jcore%get(monitorPointer, 'refDensity',  this%rhoRef, found)
        call raiseKeyErrorJSON(this%dictName//'%refDensity', found, 'settings/monitors/'//this%objName)

        call jcore%get(monitorPointer, 'refVelocity', this%Uref, found)
        call raiseKeyErrorJSON(this%dictName//'%refVelocity', found, 'settings/monitors/'//this%objName)

        call jcore%get(monitorPointer, 'refPressure', this%pRef, found)
        if(.not. found) this%pRef = 0.0

        call jcore%get(monitorPointer, 'refArea', this%Aref, found)
        call raiseKeyErrorJSON(this%dictName//'%refArea', found, 'settings/monitors/'//this%objName)

        ! Clean up
        nullify(dictPointer)
        if(monitorsFound) nullify(monitorsPointer)
        if(monitorFound) nullify(monitorPointer)
        call jcore%destroy()

    end subroutine createForceCoeffs

! *********************************************************************************************************************

    subroutine createPressureCoeff(this)

    !==================================================================================================================
    ! Description:
    !! createForcesMonitos is the class constructor.
    !==================================================================================================================

        class(Cp) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: optName
        !! option name

        character(len=:), allocatable :: val
        !! value

        character(len=:), dimension(:), allocatable :: acvec
        !! first array element length
    !------------------------------------------------------------------------------------------------------------------

        integer, dimension(:), allocatable :: ilen
        !! first array element length

        integer :: i, nPatches
    !------------------------------------------------------------------------------------------------------------------

        logical :: found, monitorsFound, monitorFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------

        this%dictName = this%objName

        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)

        ! Get monitors pointer
        call jcore%get_child(monitorsPointer, this%dictName, monitorPointer, monitorFound)

        !------------------------------!
        ! Get data from the dictionary !
        !------------------------------!

        ! Active
        call jcore%get(monitorPointer, 'active', val, found)
        if(.not. found) this%isActive = .true.

        if(found .and. (val=='yes' .or. val=='true') )  then 
            this%isActive = .true.
        else 
            this%isActive = .false.
        endif

        ! Patches
        call jcore%get(monitorPointer, 'patches', acvec, ilen, found)
        call raiseKeyErrorJSON(this%dictName//'%patches', found, 'settings/monitors/'//this%objName)

        nPatches = size(acvec)
        allocate(this%patchNames(nPatches))
        allocate(this%is(nPatches))
        allocate(this%ie(nPatches))
        allocate(this%nFaces(nPatches))
        allocate(this%pIndex(nPatches))

        do i=1,nPatches
            this%patchNames(i) = adjustl(trim(acvec(i)))
        end do

        ! Start and end pathc index
        call mesh%boundaries%findBoundaryIndices(this%patchNames, this%is, this%ie, this%nFaces, this%pIndex, nPatches)

        ! Execution frequency
        call jcore%get(monitorPointer, 'outputFrequency', this%frequency, found)
        call raiseKeyErrorJSON(this%dictName//'%outputFrequency', found, 'settings/monitors/'//this%objName)

        ! Reference values
        call jcore%get(monitorPointer, 'refDensity',  this%rhoRef, found)
        call raiseKeyErrorJSON(this%dictName//'%refDensity', found, 'settings/monitors/'//this%objName)

        call jcore%get(monitorPointer, 'refVelocity', this%Uref, found)
        call raiseKeyErrorJSON(this%dictName//'%refVelocity', found, 'settings/monitors/'//this%objName)

        call jcore%get(monitorPointer, 'refPressure', this%pRef, found)
        if(.not. found) this%pRef = 0.0

        ! Clean up
        nullify(dictPointer)
        if(monitorsFound) nullify(monitorsPointer)
        if(monitorFound) nullify(monitorPointer)
        call jcore%destroy()

    end subroutine createPressureCoeff

! *********************************************************************************************************************

    subroutine createFrictionCoeff(this)

    !==================================================================================================================
    ! Description:
    !! createForcesMonitos is the class constructor.
    !==================================================================================================================

        class(Cf) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: optName
        !! option name

        character(len=:), allocatable :: val
        !! value

        character(len=:), dimension(:), allocatable :: acvec
        !! first array element lenght
    !------------------------------------------------------------------------------------------------------------------

        integer, dimension(:),allocatable :: ilen
        !! first array element lenght

        integer :: i, nPatches
    !------------------------------------------------------------------------------------------------------------------

        logical :: found, monitorsFound, monitorFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------

        this%dictName = this%objName

        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)

        ! Get monitors pointer
        call jcore%get_child(monitorsPointer, this%dictName, monitorPointer, monitorFound)

        !------------------------------!
        ! Get data from the dictionary !
        !------------------------------!

        ! Active
        call jcore%get(monitorPointer, 'active', val, found)
        if(.not. found) this%isActive = .true.

        if(found .and. (val=='yes' .or. val=='true') )  then 
            this%isActive = .true.
        else 
            this%isActive = .false.
        endif

        ! Patches
        call jcore%get(monitorPointer, 'patches', acvec, ilen, found)
        call raiseKeyErrorJSON(this%dictName//'%patches', found, 'settings/monitors/'//this%objName)

        nPatches = size(acvec)
        allocate(this%patchNames(nPatches))
        allocate(this%is(nPatches))
        allocate(this%ie(nPatches))
        allocate(this%nFaces(nPatches))
        allocate(this%pIndex(nPatches))

        do i=1,nPatches
            this%patchNames(i) = adjustl(trim(acvec(i)))
        end do

        ! Start and end patch index
        call mesh%boundaries%findBoundaryIndices(this%patchNames, this%is, this%ie, this%nFaces, this%pIndex, nPatches)

        ! Execution frequency
        call jcore%get(monitorPointer, 'outputFrequency', this%frequency, found)
        call raiseKeyErrorJSON(this%dictName//'%outputFrequency', found, 'settings/monitors/'//this%objName)

        ! Reference values
        call jcore%get(monitorPointer, 'refDensity',  this%rhoRef, found)
        call raiseKeyErrorJSON(this%dictName//'%refDensity', found, 'settings/monitors/'//this%objName)

        call jcore%get(monitorPointer, 'refVelocity', this%Uref, found)
        call raiseKeyErrorJSON(this%dictName//'%refVelocity', found, 'settings/monitors/'//this%objName)

        ! Clean up
        nullify(dictPointer)
        if(monitorsFound) nullify(monitorsPointer)
        if(monitorFound) nullify(monitorPointer)
        call jcore%destroy()

    end subroutine createFrictionCoeff

! *********************************************************************************************************************

    subroutine aeroCoeffs(this)

        use ttbTensors

    !==================================================================================================================
    ! Description:
    !! aeroCoeffs computes the forces (pressure and viscous) on a target boundary.
    !==================================================================================================================

        class(forceCoeffs) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(ttbTensor2) :: G
        !! boudary gradient 

        type(ttbTensor2S) :: R
        !! deviatoric part of the boundary gardients

        type(ttbTensor1) :: normals
        !! boundary normals

        type(ttbTensor1) :: Rn
        !! R*normals
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iOwner, iBFace, pFace, patchFace, is ,ie, nFaces, ierr, bFaces, targetBound
    !------------------------------------------------------------------------------------------------------------------

        real :: pn(3)
        !! pressure normal force

        real :: tn(3)
        !! tangential force

        real :: gradUn(3)
        !! norma velocity gradient

        real :: Force(3,3)
        !! pforce in the processor domain, decomposed in contributions (viscous, pressure, total)

        real :: Ftot(3,3)
        !! total force

        real :: coeffs(3)
        !! aero coeffs

        real :: pdyn
        !! dynamic pressure
    !------------------------------------------------------------------------------------------------------------------

        ! Create the file
        if (itime==1) then 
            open(1,file='postProc/monitors/forces-'//adjustl(trim(this%patchNames(iBoundary)))//'.dat', status='replace')
                write(1,*) "# Time    Cd_pressure    Cd_viscous    Cd_total    Cs_pressure    Cs_viscous    Cs_total    Cl_pressure    Cl_viscous    Cl_total"
            close(1)
        endif 

        if(mod(itime, this%frequency)==0 .and. this%isActive) then

            pdyn = 0.5*this%rhoRef*this%Aref*this%Uref**2

            ! Gradient and normals, boundaries only
            is = numberOfElements + 1
            ie = numberOfElements + numberOfBFaces
            call G%create(dim=numberOfBFaces, phi=velocity%phiGrad(is:ie,1:3,1:3))

            ! Normals
            is = numberOfIntFaces+1 
            ie = numberOfFaces
            call normals%create(dim=numberOfBFaces, phi=mesh%Sf(is:ie,1:3))

            ! Stress tensor by nu (newtonian fluid only!)
            R = dev(2.0*symm(G))

            ! Project onto boundaries
            Rn = R*normals

            do iBoundary=1,size(this%pIndex)

                is = this%is(iBoundary)
                ie = this%ie(iBoundary)
                nFaces = this%nFaces(iBoundary)
                pFace = this%pIndex(iBoundary) - numberOfElements
                patchFace = this%pIndex(iBoundary)

                ! Loop over boundary faces
                i = 0
                Force = 0.0
           
                do iBFace=is,ie

                    i = i+1
                    pFace = pFace + 1
                    patchFace = patchFace+1
                    iOwner = mesh%owner(iBFace)

                    !------------------------------!
                    ! Pressure force, i.e. p*n*dA  !
                    !------------------------------!

                    pn = pressure%phi(patchFace,1)*mesh%Sf(iBFace,:)

                    !------------------------------------!
                    ! Shear stress, i.e. mu*grad(u)*n*dA !
                    !------------------------------------!

                    !gradUn(1) = dot_product(velocity%phiGrad(patchFace,:,1),mesh%Sf(iBFace,:))
                    !gradUn(2) = dot_product(velocity%phiGrad(patchFace,:,2),mesh%Sf(iBFace,:))
                    !gradUn(3) = dot_product(velocity%phiGrad(patchFace,:,3),mesh%Sf(iBFace,:))

                    !tn = nu%phif(iBFace,1)*gradUn

                    tn = nu%phif(iBFace,1)*Rn%T(pFace)%a

                    ! Sum forces up
                    Force(1,:) = Force(1,:)-pn
                    Force(2,:) = Force(2,:)+tn
                    Force(3,:) = Force(3,:)-pn+tn

                enddo

                !-------------!
                ! Total Force !
                !-------------!

                Ftot = 0.0
                do i=1,3
                    call mpi_Reduce(Force(i,:), Ftot(i,:), 3, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
                enddo

                !--------------------------!
                ! Aerodynamic coefficients !
                !--------------------------!

                ! Normals point inside the domain, thus put a minus
                Ftot = Ftot/pDyn
                coeffs(1) = -Ftot(3,1)
                coeffs(2) = -Ftot(3,2)
                coeffs(3) = -Ftot(3,3)

                !------------------------!
                ! Write the coefficients !
                !------------------------!

                if(id==0) then                    
                    open(1,file='postProc/monitors/forces-' //adjustl(trim(this%patchNames(iBoundary)))//'.dat', access='append')
                        write(1,*) time, -Ftot(1,1), -Ftot(2,1), -Ftot(3,1), -Ftot(1,2), -Ftot(2,2), -Ftot(3,2), -Ftot(1,3), -Ftot(2,3), -Ftot(3,3)
                    close(1)
                endif

            enddo

        end if

    end subroutine aeroCoeffs

!**********************************************************************************************************************

    subroutine pressureCoeff(this)

    !==================================================================================================================
    ! Description:
    !! pressureCoeff computes the Cp on a target boundary surface.
    !==================================================================================================================

        class(Cp) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=20) :: timeStamp
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iOwner, iBFace, patchFace, is ,ie, PFace, nFaces, ierr, totBFaces, targetBound

        integer :: displs(nid), sendcount(nid), procBFaces(nid)
    !------------------------------------------------------------------------------------------------------------------

        real :: pw
        !! wall pressure

        real :: pdyn
        !! dynamic pressure

        real, dimension(:), allocatable :: x_local
        !! x position component in a processor domain

        real, dimension(:), allocatable :: y_local
        !! y position component in a processor domain

        real, dimension(:), allocatable ::z_local
        !! z position component in a processor domain

        real, dimension(:), allocatable :: cp_local
        !! pressure coefficient in a processor domain

        real, dimension(:), allocatable :: x
        !! x position component

        real, dimension(:), allocatable :: y
        !! y position component

        real, dimension(:), allocatable :: z
        !! z position component

        real, dimension(:), allocatable :: cp
        !! pressure ceofficient
    !------------------------------------------------------------------------------------------------------------------

        if(mod(itime, this%frequency)==0 .and. this%isActive) then
            
            pdyn = 0.5*this%rhoRef*this%Uref**2

            do iBoundary=1,size(this%pIndex)

                is = this%is(iBoundary)
                ie = this%ie(iBoundary)
                nFaces = this%nFaces(iBoundary)

                call mpi_Allgather(nFaces, 1, MPI_INTEGER, procBFaces, 1, MPI_INTEGER, MPI_COMM_WORLD, ierr)

                totBFaces = SUM(procBFaces)

                allocate(x_local(nFaces))
                allocate(y_local(nFaces))
                allocate(z_local(nFaces))
                allocate(cp_local(nFaces))

                allocate(x(totBFaces))
                allocate(y(totBFaces))
                allocate(z(totBFaces))
                allocate(cp(totBFaces))

                ! Prepare to gather arrays from all procs
                sendcount = procBFaces

                displs(1) = 0
                do i=2,nid
                    displs(i) = SUM(sendcount(1:i-1))
                enddo

                patchFace = 0
                do iBFace=is,ie

                    patchFace = patchFace +1
                    iOwner = mesh%owner(iBFace)

                    x_local(patchFace) = mesh%fcentroid(iBFace,1)
                    y_local(patchFace) = mesh%fcentroid(iBFace,2)
                    z_local(patchFace) = mesh%fcentroid(iBFace,3)

                    pw = pressure%phi(iOwner,1) - this%pRef

                    cp_local(patchFace) = pw/pdyn

                enddo

                !---------------------------!
                ! Gather coordinates and tw !
                !---------------------------!

                call mpi_gatherv(x_local, nFaces, MPI_REAL8, x, &
                        sendcount, displs, MPI_REAL8, 0, MPI_COMM_WORLD, ierr)

                call mpi_gatherv(y_local, nFaces, MPI_REAL8, y, &
                        sendcount, displs, MPI_REAL8, 0, MPI_COMM_WORLD, ierr)

                call mpi_gatherv(z_local, nFaces, MPI_REAL8, z, &
                        sendcount, displs, MPI_REAL8, 0, MPI_COMM_WORLD, ierr)

                call mpi_gatherv(cp_local, nFaces, MPI_REAL8, cp, &
                        sendcount, displs, MPI_REAL8, 0, MPI_COMM_WORLD, ierr)

                !-----------------------------!
                ! Write cp along the boundary !
                !-----------------------------!

                if(id==0) then
                    write(timeStamp,'(i0)') itime
                    open(1,file='postProc/monitors/Cp-'//adjustl(trim(this%patchNames(iBoundary)))//'-'//trim(timeStamp)//'.dat', status='replace', access='append')
                        do i=1,totBFaces
                            write(1,*) x(i), y(i), z(i), cp(i)
                        enddo
                    close(1)

                endif

                ! Deallocate once written
                deallocate(x_local)
                deallocate(y_local)
                deallocate(z_local)
                deallocate(cp_local)

                deallocate(x)
                deallocate(y)
                deallocate(z)
                deallocate(cp)

            enddo

        endif

    end subroutine pressureCoeff

! *********************************************************************************************************************

    subroutine skinFrictionCoeff(this)

      use ttbTensors

    !==================================================================================================================
    ! Description:
    !! skinFriction computes the shear stress mu*du/dn along a target boundary.
    !==================================================================================================================

        class(Cf) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(ttbTensor2) :: G
        !! boudary gradient 

        type(ttbTensor2S) :: R
        !! deviatoric part of the boundary gardients

        type(ttbTensor1) :: normals
        !! boundary normals

        type(ttbTensor1) :: Rn
        !! R*normals
    !------------------------------------------------------------------------------------------------------------------

        character(len=20) :: timeStamp
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iOwner, iBFace, patchFace, is ,ie, pFace, bFace, nFaces, ierr, totBFaces, targetBound

        integer :: displs(nid), sendcount(nid), procBFaces(nid)
    !------------------------------------------------------------------------------------------------------------------

        real :: pdyn
        !! dynamic pressure

        real :: nf(numberOfFaces, 3)
        !! face normals

        real, dimension(:), allocatable :: x_local
        !! x position component in a processor domain

        real, dimension(:), allocatable :: y_local
        !! y position component in a processor domain

        real, dimension(:), allocatable ::z_local
        !! z position component in a processor domain

        real, dimension(:,:), allocatable :: tw_local
        !! wall shear stress  in a processor domain

        real, dimension(:), allocatable :: x
        !! x position component

        real, dimension(:), allocatable :: y
        !! y position component

        real, dimension(:), allocatable :: z
        !! z position component

        real, dimension(:,:), allocatable :: tw
        !! wall shear stress
    !------------------------------------------------------------------------------------------------------------------

        if(mod(itime, this%frequency)==0 .and. this%isActive) then

            ! Gradient and normals, boundaries only
            is = numberOfElements + 1
            ie = numberOfElements + numberOfBFaces
            call G%create(dim=numberOfBFaces, phi=velocity%phiGrad(is:ie,1:3,1:3))

            ! Normals
            nf = mesh%faceNormals() 
            is = numberOfIntFaces+1 
            ie = numberOfFaces
            call normals%create(dim=numberOfBFaces, phi=nf(is:ie,1:3))

            ! Stress tensor by nu (newtonian fluid only!)
            R = dev(2.0*symm(G))

            ! Project onto boundaries
            Rn = R*normals

            ! Scaling factor
            pdyn = 0.5*this%rhoRef*this%Uref**2

            do iBoundary=1,size(this%pIndex)

                is = this%is(iBoundary)
                ie = this%ie(iBoundary)
                nFaces = this%nFaces(iBoundary)

                call mpi_gather(nFaces, 1, MPI_INTEGER, procBFaces, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)

                totBFaces = SUM(procBFaces)

                allocate(x_local(nFaces))
                allocate(y_local(nFaces))
                allocate(z_local(nFaces))
                allocate(tw_local(nFaces,3))

                allocate(x(totBFaces))
                allocate(y(totBFaces))
                allocate(z(totBFaces))
                allocate(tw(totBFaces,3))

                tw = 0.0
                tw_local = 0.0

                ! Prepare to gather arrays from all procs
                sendcount = procBFaces
                displs(1) = 0
                do i=2,nid
                    displs(i) = SUM(sendcount(1:i-1))
                enddo

                ! Loop over boundary faces
                bFace = this%pIndex(iBoundary)
                pFace = this%pIndex(iBoundary) - numberOfElements
                patchFace = 0
                do iBFace=is,ie

                    patchFace = patchFace + 1
                    pFace = pFace + 1
                    bFace = bFace + 1
                    iOwner = mesh%owner(iBFace)

                    x_local(patchFace) = mesh%fcentroid(iBFace,1)
                    y_local(patchFace) = mesh%fcentroid(iBFace,2)
                    z_local(patchFace) = mesh%fcentroid(iBFace,3)

                    tw_local(patchFace,1:3) = nu%phi(bFace,1)*Rn%T(pFace)%a/pdyn

                enddo

                !---------------------------!
                ! Gather coordinates and tw !
                !---------------------------!

                call mpi_gatherv(x_local, nFaces, MPI_REAL8, x, &
                        sendcount, displs, MPI_REAL8, 0, MPI_COMM_WORLD, ierr)

                call mpi_gatherv(y_local, nFaces, MPI_REAL8, y, &
                        sendcount, displs, MPI_REAL8, 0, MPI_COMM_WORLD, ierr)

                call mpi_gatherv(z_local, nFaces, MPI_REAL8, z, &
                        sendcount, displs, MPI_REAL8, 0, MPI_COMM_WORLD, ierr)

                call mpi_gatherv(tw_local(:,1), nFaces, MPI_REAL8, tw(:,1), &
                        sendcount, displs, MPI_REAL8, 0, MPI_COMM_WORLD, ierr)

                call mpi_gatherv(tw_local(:,2), nFaces, MPI_REAL8, tw(:,2), &
                        sendcount, displs, MPI_REAL8, 0, MPI_COMM_WORLD, ierr)

                call mpi_gatherv(tw_local(:,3), nFaces, MPI_REAL8, tw(:,3), &
                        sendcount, displs, MPI_REAL8, 0, MPI_COMM_WORLD, ierr)

                !-----------------------------!
                ! Write tw along the boundary !
                !-----------------------------!

                if(id==0) then
                    write(timeStamp,'(i0)') itime
                    open(1,file='postProc/monitors/skinFriction-'//adjustl(trim(this%patchNames(iBoundary)))//'-'//trim(timeStamp)//'.dat', status='replace', access='append')
                        do i=1,totBFaces
                            write(1,*) x(i), y(i), z(i), tw(i,1), tw(i,2), tw(i,3)
                        enddo
                    close(1)

                endif

                ! Deallocate once written
                deallocate(x_local)
                deallocate(y_local)
                deallocate(z_local)
                deallocate(tw_local)

                deallocate(x)
                deallocate(y)
                deallocate(z)
                deallocate(tw)

            enddo

        endif

    end subroutine skinFrictionCoeff

! *********************************************************************************************************************

    subroutine skinFrictionCoeff2(this)

    !==================================================================================================================
    ! Description:
    !! skinFriction computes the shear stress mu*du/dn along a target boundary.
    !==================================================================================================================

        class(Cf) :: this
    !------------------------------------------------------------------------------------------------------------------
        
        character(len=20) :: timeStamp
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iOwner, iBFace, patchFace, is ,ie, PFace, nFaces, ierr, totBFaces, targetBound

        integer :: displs(nid), sendcount(nid), procBFaces(nid)
    !------------------------------------------------------------------------------------------------------------------

        real :: Up(3)
        !! cell center velocity value

        real :: deltaU(3)
        !! velocity

        real :: nf(3)
        !! boundary face normal

        real :: magUp
        !! magnitude of he cell center velocity

        real :: pdyn
        !! dynamic pressure

        real :: dot
        !! auxiliary value to store a dot prodcut

        real :: dudn
        !! velocity normal gradient

        real :: d
        !! wall distance

        real, dimension(:), allocatable :: x_local
        !! x position component in a processor domain

        real, dimension(:), allocatable :: y_local
        !! y position component in a processor domain

        real, dimension(:), allocatable ::z_local
        !! z position component in a processor domain

        real, dimension(:), allocatable :: tw_local
        !! wall shear stress  in a processor domain

        real, dimension(:), allocatable :: x
        !! x position component

        real, dimension(:), allocatable :: y
        !! y position component

        real, dimension(:), allocatable :: z
        !! z position component

        real, dimension(:), allocatable :: tw
        !! wall shear stress
    !------------------------------------------------------------------------------------------------------------------

        if(mod(itime, this%frequency)==0 .and. this%isActive) then

            pdyn = 0.5*this%rhoRef*this%Uref**2

            do iBoundary=1,size(this%pIndex)

                is = this%is(iBoundary)
                ie = this%ie(iBoundary)
                nFaces = this%nFaces(iBoundary)
                patchFace = this%pIndex(iBoundary)

                call mpi_Allgather(nFaces, 1, MPI_INTEGER, procBFaces, 1, MPI_INTEGER, MPI_COMM_WORLD, ierr)

                totBFaces = SUM(procBFaces)

                allocate(x_local(nFaces))
                allocate(y_local(nFaces))
                allocate(z_local(nFaces))
                allocate(tw_local(nFaces))

                allocate(x(totBFaces))
                allocate(y(totBFaces))
                allocate(z(totBFaces))
                allocate(tw(totBFaces))

                ! Prepare to gather arrays from all procs
                sendcount = procBFaces

                displs(1) = 0
                do i=2,nid
                    displs(i) = SUM(sendcount(1:i-1))
                enddo

                ! Loop over boundary faces
                pFace = this%pIndex(iBoundary)
                patchFace = 0
                do iBFace=is,ie

                    patchFace = patchFace +1
                    pFace = pFace + 1
                    iOwner = mesh%owner(iBFace)

                    x_local(patchFace) = mesh%fcentroid(iBFace,1)
                    y_local(patchFace) = mesh%fcentroid(iBFace,2)
                    z_local(patchFace) = mesh%fcentroid(iBFace,3)

                    ! Velocity parallel to the wall
                    nf = mesh%Sf(iBFace,:)/norm2(mesh%Sf(iBFace,:))

                    deltaU = velocity%phi(iOwner,:) - velocity%phi(pFace,:)

                    dot = dot_product(deltaU, nf)

                    Up = deltaU-dot*nf

                    magUp = norm2(Up)

                    d = dot_product(mesh%CN(iBFace,:), mesh%Sf(iBFace,:)/norm2(mesh%Sf(iBFace,:)))

                    dudn = magUp/d

                    tw_local(patchFace) = nu%phi(pFace,1)*dudn/pdyn

                enddo

                !---------------------------!
                ! Gather coordinates and tw !
                !---------------------------!

                call mpi_gatherv(x_local, nFaces, MPI_REAL8, x, &
                        sendcount, displs, MPI_REAL8, 0, MPI_COMM_WORLD, ierr)

                call mpi_gatherv(y_local, nFaces, MPI_REAL8, y, &
                        sendcount, displs, MPI_REAL8, 0, MPI_COMM_WORLD, ierr)

                call mpi_gatherv(z_local, nFaces, MPI_REAL8, z, &
                        sendcount, displs, MPI_REAL8, 0, MPI_COMM_WORLD, ierr)

                call mpi_gatherv(tw_local, nFaces, MPI_REAL8, tw, &
                        sendcount, displs, MPI_REAL8, 0, MPI_COMM_WORLD, ierr)

                !-----------------------------!
                ! Write tw along the boundary !
                !-----------------------------!

                if(id==0) then
                    write(timeStamp,'(i0)') itime
                    open(1,file='postProc/monitors/skinFriction-'//adjustl(trim(this%patchNames(iBoundary)))//'-'//trim(timeStamp)//'.dat', status='replace', access='append')
                        do i=1,totBFaces
                            write(1,*) x(i), y(i), z(i), tw(i)
                        enddo
                    close(1)

                endif

                ! Deallocate once written
                deallocate(x_local)
                deallocate(y_local)
                deallocate(z_local)
                deallocate(tw_local)

                deallocate(x)
                deallocate(y)
                deallocate(z)
                deallocate(tw)

            enddo

        endif

    end subroutine skinFrictionCoeff2

! *********************************************************************************************************************

end module forceMonitors
