module sumFields

    use flubioDictionaries, only: flubioMonitors, raiseKeyErrorJSON
    use fieldvar, only: fieldRegistry,getFieldLocationInRegistry
    use json_module
    use runTimeObject

    implicit none

    type, public, extends(fieldObj) :: addObj
        character(len=:), dimension(:), allocatable :: fieldsToAdd
        character(len=:), allocatable :: operation
    contains
        procedure :: initialise => createAddObj
        procedure :: run => addFields
    end type addObj

contains
        
subroutine createAddObj(this)

    !==================================================================================================================
    ! Description:
    !! createAdddObj is the field constructor.
    !==================================================================================================================
    
        class(addObj) :: this
    !-------------------------------------------------------------------------------------------------------------------
    
        logical :: found, monitorsFound, monitorFound
    !------------------------------------------------------------------------------------------------------------------
    
        type(json_core) :: jcore
    
        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------
    
        integer, dimension(:), allocatable :: ilen
    !------------------------------------------------------------------------------------------------------------------

        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)
    
        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)
    
        ! Get monitor pointer
        call jcore%get_child(monitorsPointer, this%objName, monitorPointer, monitorFound)
    
        call jcore%get(monitorPointer, 'runEvery', this%runAt, found)
        if(.not. found) this%runAt = 1

        call jcore%get(monitorPointer, 'saveEvery', this%saveEvery, found)
        call raiseKeyErrorJSON('saveEvery', found, 'settings/monitors/'//this%objName)

        call jcore%get(monitorPointer, 'components', this%nComp, found)
        call raiseKeyErrorJSON('components', found, 'settings/monitors/'//this%objName)

        call jcore%get(monitorPointer, 'operation', this%operation, found)
        call raiseKeyErrorJSON('operation', found, 'settings/monitors/'//this%objName)
        
        call jcore%get(monitorPointer, 'fieldsToAdd', this%fieldsToAdd, ilen, found)
        call raiseKeyErrorJSON('fieldsToAdd', found, 'settings/monitors/'//this%objName)

        call jcore%get(monitorPointer, 'name', this%fieldName, found)
        call raiseKeyErrorJSON('name', found, 'settings/monitors/'//this%objName)
    
        call this%taskField%createWorkField(fieldName=this%fieldName, classType='scalar', nComp=this%nComp)
    
        ! Clean up
        nullify(dictPointer)
        if(monitorsFound) nullify(monitorsPointer)
        if(monitorFound) nullify(monitorPointer)
        call jcore%destroy()
    
    end subroutine createAddObj
    
! *********************************************************************************************************************

    subroutine addFields(this)

    !==================================================================================================================
    ! Description:
    !! addFields adds the fields specifiedin the list.
    !==================================================================================================================

        class(addObj):: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, fieldIndex
    !------------------------------------------------------------------------------------------------------------------
      
        if(mod(itime, this%runAt)==0) then

            do i=1,size(this%fieldsToAdd)

                fieldIndex = getFieldLocationInRegistry(trim(this%fieldsToAdd(i)))

                ! Check the dimensions are consistent
                if(fieldRegistry(fieldIndex)%field%nComp /= this%nComp) call flubioStopMsg("ERROR: dimensio of fields to add are not consistent.")

                if(lowercase(this%operation)=='subtract') then
                    this%taskField%phi = this%taskField%phi - fieldRegistry(fieldIndex)%field%phi
                else    
                    this%taskField%phi = this%taskField%phi + fieldRegistry(fieldIndex)%field%phi
                endif 

                call this%write()

            enddo

        endif    
        
    end subroutine addFields

!**********************************************************************************************************************

end module sumFields