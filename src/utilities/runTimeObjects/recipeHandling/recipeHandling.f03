module recipeHandling

    use flubioDictionaries
    use flubioMpi
    use json_module
    use runTimeObject
    use equationRegistry
    use turbulenceModels, only: trb 
    !use m_strings, only: value_to_string

    implicit none

    type, extends(runTimeObj), public :: recipeHandlingObj
        type(generalDict) :: recipeHandlingDict
        integer, dimension(:), allocatable :: stages
    contains
        procedure :: initialise => readRecipeHandlingDict
        procedure :: run => changeSettings
    end type recipeHandlingObj

contains
        
    subroutine readRecipeHandlingDict(this)

    !==============================================================================================================
    ! Description:
    !! readDict reads the runTimeObject dictionary.
    !==============================================================================================================

        class(recipeHandlingObj) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName
        !! Dictionary name
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore
    
        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------

        logical :: monitorFound, monitorsFound, found
    !------------------------------------------------------------------------------------------------------------------

        dictName = 'settings/recipeHandling'

       ! Initialise the JSON dictionary
        inquire(file=trim(dictName), exist=found)

        if (.not. found) then
            this%recipeHandlingDict%dictFound = .false.
        else
            this%recipeHandlingDict%dictFound = .true.
            call this%recipeHandlingDict%initJSON(dictName)
        end if

        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)
    
        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)
    
        ! Get monitor pointer
        call jcore%get_child(monitorsPointer, this%objName, monitorPointer, monitorFound)
    
        call jcore%get(monitorPointer, 'stages', this%stages, found)

        ! Clean up
        nullify(dictPointer)
        if(monitorsFound) nullify(monitorsPointer)
        if(monitorFound) nullify(monitorPointer)
        call jcore%destroy()        

    end subroutine readRecipeHandlingDict

!**********************************************************************************************************************    

    subroutine changeSettings(this)

    !==============================================================================================================
    ! Description:
    !! changeSettings changes the equation setting at a target time step.
    !==============================================================================================================

        class(recipeHandlingObj) :: this
    !--------------------------------------------------------------------------------------------------------------

        character(len=25) :: timeStamp

        character(len=:), allocatable :: eqName, path
        !! First array element lenght

        character(len=50), dimension(:), allocatable  :: splitPath

        character(len=:), allocatable :: opt
    !--------------------------------------------------------------------------------------------------------------

        integer :: nEqns

        integer :: eqnIndex

        integer :: iEqn, ilen

        integer :: n
    !--------------------------------------------------------------------------------------------------------------   

        type(json_core) :: jcore
    
        type(json_value), pointer :: dictPointer, recipePointer, timePointer, eqnPointer
    !--------------------------------------------------------------------------------------------------------------

        logical :: recipeFound, timeFound, eqnFound, nameFound, foundInRegistry, found
    !--------------------------------------------------------------------------------------------------------------

        real :: urf
    !--------------------------------------------------------------------------------------------------------------

        if(ANY(this%stages==itime)) then
            
            call value_to_string(itime, timeStamp, ilen)

            ! Get the pointer to the dictionary
            call this%recipeHandlingDict%json%get(dictPointer)
        
            ! Initialize json factory
            call jcore%initialize()
            call jcore%get(dictPointer, 'Recipe', recipePointer, recipeFound)

            if(recipeFound) then 

                ! Get monitor pointer
                call jcore%get_child(recipePointer, timeStamp, timePointer, timeFound)

                if(timeFound) then 

                    ! Loop over equations
                    call jcore%info(timePointer, n_children=nEqns)
                  
                    do iEqn=1,nEqns

                        ! Get eqn pointer
                        call jcore%get_child(timePointer, iEqn, eqnPointer, eqnFound)

                        ! Get eqn name
                        call jcore%get_path(eqnPointer, path, nameFound)
                        if(nameFound) then
                            call split_in_array(path, splitPath, '.')
                            eqName = splitPath(3)
                        else
                            call flubioStopMsg('ERROR: cannot find the equation name, please check if the dictionary is correct.')
                        end if

                        if(eqnFound) then 

                            foundInRegistry = checkEqnRegistry(eqName, warn=.false.)

                            if(foundInRegistry) then

                                eqnIndex = getEqnLocationInRegistry(eqName)

                                ! Under-relaxation
                                call jcore%get(eqnPointer, 'urf', urf, found)
                                if(found) eqnRegistry(eqnIndex)%eqn%urf = urf

                                ! Convective scheme
                                call jcore%get(eqnPointer, 'convectiveScheme', opt, found)
                                if(found) then
                                    eqnRegistry(eqnIndex)%eqn%convOptName = lowercase(opt)
                                    call flubioOptions%setConvectionOption(eqnRegistry(eqnIndex)%eqn%convOptName, eqnRegistry(eqnIndex)%eqn%convOpt)
                                endif

                                ! Time scheme
                                call jcore%get(eqnPointer, 'timeScheme', opt, found)
                                if(found) then
                                    eqnRegistry(eqnIndex)%eqn%timeOptName = lowercase(opt)
                                    call flubioOptions%setTimeSchemeOption(eqnRegistry(eqnIndex)%eqn%eqName, eqnRegistry(eqnIndex)%eqn%timeOpt, eqnRegistry(eqnIndex)%eqn%timeOptName)
                                endif

                                ! bounded correction
                                call jcore%get(eqnPointer, 'bounded', opt, found)
                                if(found .and. (lowercase(opt)=='yes' .or. lowercase(opt)=='on' .or. lowercase(opt)=='true')) then
                                    call flubioOptions%setBoundedCorrection(eqnRegistry(eqnIndex)%eqn%eqName, eqnRegistry(eqnIndex)%eqn%boundedCorr)
                                endif
                               
                                ! Change non-orthogonal corrections
                                call jcore%get(eqnPointer, 'nonOrthogonalCorr', n, found)
                                if(found) flubioOptions%nCorr = n

                            ! Trubulence models. Tke and Tdr equations are wrapped insiede the model and not targettable
                            elseif( lowercase(trb%modelName) /= 'dns' .and. (lowercase(eqName)=='tkeeqn' .or. lowercase(eqName)=='tdreqn')) then             

                                ! Under-relaxation
                                call jcore%get(eqnPointer, 'urf', urf, found)
                                if(lowercase(eqName)=='tkeeqn') then
                                    if(found) trb%tkeEqn%urf = urf
                                elseif(lowercase(eqName)=='tdreqn') then 
                                    if(found) trb%tdrEqn%urf = urf
                                endif    

                                ! Convective scheme
                                call jcore%get(eqnPointer, 'convectiveScheme', opt, found)
                                if(lowercase(eqName)=='tkeeqn') then
                                    if(found) call flubioOptions%setConvectionOption(trb%tkeEqn%convOptName, trb%tkeEqn%convOpt)
                                elseif(lowercase(eqName)=='tdreqn') then 
                                    if(found) call flubioOptions%setConvectionOption(trb%tdrEqn%convOptName, trb%tdrEqn%convOpt)
                                endif    

                                ! Time scheme
                                call jcore%get(eqnPointer, 'timeScheme', opt, found)
                                if(lowercase(eqName)=='tkeeqn') then
                                    if(found) call flubioOptions%setTimeSchemeOption(trb%tkeEqn%eqName, trb%tkeEqn%timeOpt, trb%tkeEqn%timeOptName)
                                elseif(lowercase(eqName)=='tdreqn') then 
                                    if(found) call flubioOptions%setTimeSchemeOption(trb%tdrEqn%eqName, trb%tdrEqn%timeOpt, trb%tdrEqn%timeOptName)
                                endif    

                                ! bounded correction
                                call jcore%get(eqnPointer, 'bounded', opt, found)
                                if(lowercase(eqName)=='tkeeqn') then
                                    if(found) call flubioOptions%setBoundedCorrection(trb%tkeEqn%eqName, trb%tkeEqn%boundedCorr)
                                elseif(lowercase(eqName)=='tdreqn') then 
                                    if(found) call flubioOptions%setBoundedCorrection(trb%tdrEqn%eqName, trb%tdrEqn%boundedCorr)
                                endif    

                            else    
                                call flubioMsg("Warning: cannot find equation "//trim(eqName)//" in the equation registry")
                            endif 

                        else 
                            call flubioMsg("Warning: no equations found, the recipe will not change.")    
                        endif

                    enddo    

                else 
                    call flubioStopMsg("FLUBIO ERROR: target time '"//timeStamp//"' not found in the dictionary.")    
                endif        

            else 
                call flubioStopMsg("FLUBIO ERROR: recipe section not found, check the dictionary.")    
            endif 
        
        endif    
        
    end subroutine changeSettings

!**********************************************************************************************************************  

    !subroutine 

        !==============================================================================================================
        ! Description:
        !! .
        !==============================================================================================================

        !--------------------------------------------------------------------------------------------------------------

    !end subroutine

!**********************************************************************************************************************  

end module recipeHandling