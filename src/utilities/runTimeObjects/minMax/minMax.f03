module minMaxFields

    use flubioDictionaries, only: flubioMonitors, raiseKeyErrorJSON
    use fieldvar, only: fieldRegistry,getFieldLocationInRegistry
    use json_module
    use runTimeObject
    use qvector_m
    use dDynamicArray_Class
    !use m_strings, only: value_to_string
    
    implicit none

    type, public :: minMaxArray
        character(len=:), allocatable :: fieldname
        type(dDynamicArray), dimension(:), allocatable :: minVals
        type(dDynamicArray), dimension(:), allocatable :: maxVals
    end type minMaxArray

    type, public, extends(fieldObj) :: minMaxObj
        integer :: numberOfFields
        character(len=:), dimension (:), allocatable :: fields
        character(len=:), allocatable :: operation
        type(minMaxArray), dimension(:), allocatable :: minMaxData
    contains
        procedure :: initialise => createMinMaxObj
        procedure :: run => computeMinMax
    end type minMaxObj

contains
         
    subroutine createMinMaxObj(this)

    !==================================================================================================================
    ! Description:
    !! createAdddObj is the field constructor.
    !==================================================================================================================
    
        class(minMaxObj) :: this
    !------------------------------------------------------------------------------------------------------------------
    
        logical :: found, monitorsFound, monitorFound
    !------------------------------------------------------------------------------------------------------------------
    
        type(json_core) :: jcore
    
        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------
    
        integer :: i, j, isize, iComp, nComp

        integer, dimension(:), allocatable :: ilen
    !------------------------------------------------------------------------------------------------------------------

        real :: dummy
    !------------------------------------------------------------------------------------------------------------------

        isize = storage_size(dummy)/8

        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)
    
        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)
    
        ! Get monitor pointer
        call jcore%get_child(monitorsPointer, this%objName, monitorPointer, monitorFound)
    
        call jcore%get(monitorPointer, 'runEvery', this%runAt, found)
        if(.not. found) this%runAt = 1

        call jcore%get(monitorPointer, 'saveEvery', this%saveEvery, found)
        call raiseKeyErrorJSON('saveEvery', found, 'settings/monitors/'//this%objName)
        
        call jcore%get(monitorPointer, 'fields', this%fields, ilen, found)
        call raiseKeyErrorJSON('fields', found, 'settings/monitors/'//this%objName)

        call jcore%get(monitorPointer, 'operation', this%operation, found)
        if(.not. found) this%operation = "both"
    
        this%numberOfFields = size(this%fields)

        ! Allocate the data containter
        allocate(this%minMaxData(this%numberOfFields))

        do i=1,this%numberOfFields

            j = getFieldLocationInRegistry(trim(this%fields(i)))
            nComp = fieldRegistry(j)%field%nComp 

            allocate(this%minMaxData(i)%minVals(nComp))
            allocate(this%minMaxData(i)%maxVals(nComp))

            do iComp=1,nComp
                this%minMaxData(i)%minVals(iComp) = dDynamicArray(1)
                this%minMaxData(i)%maxVals(iComp) = dDynamicArray(1)
            enddo    
             
        enddo    

        ! Open the files in postProc/monitors
        if(id==0) then
            if(lowercase(this%operation)=='min') then
                open(1, file='postProc/monitors/'//this%objName//'_min.dat')
                    write(1,*) '# Time', this%fields
                close(1)
            elseif(lowercase(this%operation)=='max') then
                open(1, file='postProc/monitors/'//this%objName//'_max.dat')
                    write(1,*) '#   Time   ', this%fields
                close(1)
            else
                open(1, file='postProc/monitors/'//this%objName//'_min.dat')
    
                close(1)
                open(1, file='postProc/monitors/'//this%objName//'_max.dat')
                    write(1,*) '# Time', this%fields
                close(1)
            endif                 
        endif

        ! Clean up
        nullify(dictPointer)
        if(monitorsFound) nullify(monitorsPointer)
        if(monitorFound) nullify(monitorPointer)
        call jcore%destroy()
    
    end subroutine createMinMaxObj
    
! *********************************************************************************************************************

    subroutine computeMinMax(this)

    !==================================================================================================================
    ! Description:
    !! computeMinMax prints the max or the min of the fields in the list in a file.
    !==================================================================================================================

        class(minMaxObj):: this
    !------------------------------------------------------------------------------------------------------------------

      character(len=:), allocatable :: stringMin
      character(len=:), allocatable :: stringMax
      character(len=50) :: stringValueMin
      character(len=50) :: stringValueMax
      character(len=50) :: stringTime
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, fieldIndex, iComp, nComp, ilen
    !------------------------------------------------------------------------------------------------------------------

        real, dimension(:), allocatable :: fieldMin, fieldMax

        real, dimension(:,:), allocatable :: minCellCenter
        !! cell center of the min cell center

        real, dimension(:,:), allocatable  :: maxCellCenter
        !! cell center of the max cell center
    !------------------------------------------------------------------------------------------------------------------

        stringMin = ""
        stringMax = ""

        if(mod(itime, this%runAt)==0) then

            call value_to_string(time,stringTime,ilen)
            stringMin = stringMin//trim(stringTime)
            stringMax = stringMax//trim(stringTime)

            do i=1,this%numberOfFields

                fieldIndex = getFieldLocationInRegistry(trim(this%fields(i)))
                nComp = fieldRegistry(fieldIndex)%field%nComp

                call minMaxField(fieldRegistry(fieldIndex)%field, fieldMin, fieldMax, minCellCenter, maxCellCenter)

                if(lowercase(this%operation)=='min') then

                    do iComp=1,nComp
                        call this%minMaxData(i)%minVals(iComp)%append(fieldMin(iComp))
                        call value_to_string(fieldMin(iComp),stringValueMin,ilen)
                        stringMin = stringMin//' '//trim(stringValueMin)
                    enddo
 
                elseif(lowercase(this%operation)=='max') then

                    do iComp=1,nComp
                        call this%minMaxData(i)%maxVals(iComp)%append(fieldMax(iComp))
                        call value_to_string(fieldMax(iComp),stringValueMax,ilen)
                        stringMax = stringMax//' '//trim(stringValueMax)
                    enddo

                else

                    do iComp=1,nComp
                        call this%minMaxData(i)%minVals(iComp)%append(fieldMin(iComp))
                        call this%minMaxData(i)%maxVals(iComp)%append(fieldMax(iComp))

                        call value_to_string(fieldMin(iComp),stringValueMin,ilen)
                        stringMin = stringMin//' '//trim(stringValueMin)

                        call value_to_string(fieldMax(iComp),stringValueMax,ilen)
                        stringMax = stringMax//' '//trim(stringValueMax)

                    enddo
                    
                endif           
                
                deallocate(fieldMin)
                deallocate(fieldMax)

            enddo

            if(id==0 .and. (mod(itime,this%saveEvery)==0 .or. abs(this%saveEvery)==1)) then

                if(this%operation=='min') then
                    open(1,file='postProc/monitors/'//this%objName//'_min.dat',access='append')
                        write(1,'(A)') stringMin
                    close(1)
                elseif(this%operation=='max') then    
                    open(1,file='postProc/monitors/'//this%objName//'_max.dat',access='append')
                    write(1,'(A)') stringMax
                    close(1)
                else
                    open(1,file='postProc/monitors/'//this%objName//'_min.dat',access='append')
                        write(1,'(A)') stringMin
                    close(1)
                    open(1,file='postProc/monitors/'//this%objName//'_max.dat',access='append')
                        write(1,'(A)') stringMax
                    close(1)
                endif        

            endif    

        endif
        
    end subroutine computeMinMax

!**********************************************************************************************************************    

end module minMaxFields