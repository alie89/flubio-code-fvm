module sampledSurfaces

    use flubioMpi
    use flubioDictionaries, only: flubioMonitors
    use probes, only: sampleObj, faceToProbe, nearestNeighbourCell, checkProbe
    use flubioFields, only: numberOfRegisteredFields
    use fieldvar, only: fieldRegistry
    use meshvar
    use fossil
    use json_module
    use runTimeObject
    use qvector_m
    use iDynamicArray_Class

    implicit none

    type, public :: container
        integer :: nProbes
        real, dimension(:), allocatable :: x
        real, dimension(:), allocatable :: y
        real, dimension(:), allocatable :: z
        real, dimension(:,:), allocatable :: probeValues
    end type
    
    ! Extend for line probes
    type, extends(sampleObj), public :: sampledSurfaceObj
        
        integer :: nSurfaces
        
        character(len=:), dimension(:), allocatable :: stlSurfaceNames

        type(file_stl_object), dimension(:), allocatable :: STLFiles
        !! STL file object

        type(surface_stl_object), dimension(:), allocatable :: STLSurfaces
        !! STL surface object

        type(iDynamicArray), dimension(:), allocatable  :: ownedProbes

        type(container), dimension(:), allocatable :: rawSurfaces
        
        type(iDynamicArray) :: fieldToSampleIndex
        
    contains
        procedure :: initialise => createSurfaceProbes
        procedure :: run => sampleSurfaces
        procedure :: findOwnedProbes
        procedure :: writeProbesToFile
    end type sampledSurfaceObj

contains

    subroutine createSurfaceProbes(this)

    !==================================================================================================================
    ! Description:
    !! createSurfaceProbes generates the cartisian ponts along the segment joining the inout points.
    !==================================================================================================================

        class(sampledSurfaceObj) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: optName
        !! option name

        character(len=:), allocatable :: val
        !! value

        character(len=:), dimension(:), allocatable :: acvec
        !! first array element lenght
    !------------------------------------------------------------------------------------------------------------------

        integer, dimension(:),allocatable :: ilen
        !! first array element lenght

        integer :: i, n, iField, iTime, isize
    !------------------------------------------------------------------------------------------------------------------

        logical :: found, monitorsFound, monitorFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------

        this%dictName = this%objName

        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)

        ! Get monitors pointer
        call jcore%get_child(monitorsPointer, this%dictName, monitorPointer, monitorFound)

        !------------------------------!
        ! Get data from the dictionary !
        !------------------------------!

        ! Active
        call jcore%get(monitorPointer, 'active', this%isActive, found)
        if(.not. found) this%isActive = .true.

        ! Get interpolation type
        call jcore%get(monitorPointer, 'interpolation', this%interpType, found)
        if(.not. found) call flubioStopMsg('ERROR: cannot find the task interpolation type.')

        ! Get the number of neighbours
        call jcore%get(monitorPointer, 'numberOfNeighbours', this%nn, found)
        if(.not. found) this%runAt = 4

        ! Get the sampling frequency
        call jcore%get(monitorPointer, 'sampleEvery', this%runAt, found)
        if(.not. found) call flubioStopMsg('ERROR: cannot find the sampling frequency.')

        ! Get fields to post-process
        call jcore%get(monitorPointer, 'fields', acvec, ilen, found)
        if(found) then
            allocate(this%fields(size(acvec)))
            do i=1,size(acvec)
                this%fields(i) = trim(acvec(i))
            end do
        else
            call flubioStopMsg('ERROR: cannot find the fields to process.')
        end if

        ! Get task type
        call jcore%get(monitorPointer, 'type', this%type, found)
        if(.not. found) call flubioStopMsg('ERROR: cannot find the task type.')
        
        ! Get stl file name (it will become a list)
        call jcore%get(monitorPointer, 'surfaces', this%stlSurfaceNames, ilen, found)
        if(.not. found) call flubioStopMsg('ERROR: cannot find the stl file.')

        this%nSurfaces = size(this%stlSurfaceNames)
        allocate(this%stlSurfaces(this%nSurfaces))
        allocate(this%stlFiles(this%nSurfaces))
        allocate(this%ownedProbes(this%nSurfaces))
        allocate(this%rawSurfaces(this%nSurfaces))

        ! Load stl and set up sample points
        do i=1,this%nSurfaces
            call this%STLFiles(i)%load_from_file(facet=this%STLSurfaces(i)%facet, file_name=this%stlSurfaceNames(i), is_ascii=.true.)
            this%rawSurfaces(i)%nProbes = size(this%STLSurfaces(i)%facet)

            allocate(this%rawSurfaces(i)%x(this%rawSurfaces(i)%nProbes))
            allocate(this%rawSurfaces(i)%y(this%rawSurfaces(i)%nProbes))
            allocate(this%rawSurfaces(i)%z(this%rawSurfaces(i)%nProbes))

        end do

        ! Find out the probes hosted by the processor
        isize = storage_size(i)/8
        do i=1,size(this%stlSurfaceNames)
            this%ownedProbes(i) = iDynamicArray(1)
        end do
        call this%findOwnedProbes()
        
        ! Find out fields in the registry
        isize = storage_size(i)/8
        this%fieldToSampleIndex = iDynamicArray(1)

        do iField=1,numberOfRegisteredFields
            do i=1,size(this%fields)
                if(trim(this%fields(i))==trim(fieldRegistry(iField)%field%fieldName(1))) then
                    call this%fieldToSampleIndex%append(iField)
                end if
            end do
        end do

    end subroutine createSurfaceProbes

! *********************************************************************************************************************
    
    subroutine findOwnedProbes(this)

        use vecfor

    !==================================================================================================================
    ! Description:
    !! findOwnedProbes finds the probes hosted by a processor
    !==================================================================================================================

        class(sampledSurfaceObj) :: this

        type(vector_R8P) :: v
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, n, p, index
    !------------------------------------------------------------------------------------------------------------------

        real :: point(3)
    !------------------------------------------------------------------------------------------------------------------

        do i=1,this%nSurfaces

            do p=1,this%rawSurfaces(i)%nProbes

                ! Take the facet center, avoid nodes duplications
                point = 0.0
                do n=1,3
                    point(1) = point(1) + this%stlSurfaces(i)%facet(p)%vertex(n)%x/3.0
                    point(2) = point(2) + this%stlSurfaces(i)%facet(p)%vertex(n)%y/3.0
                    point(3) = point(3) + this%stlSurfaces(i)%facet(p)%vertex(n)%z/3.0
                enddo

                this%rawSurfaces(i)%x(p) = point(1)
                this%rawSurfaces(i)%y(p) = point(2)
                this%rawSurfaces(i)%z(p) = point(3)

                index = mesh%findPointOwnership(point)
                if(index>-1) then
                    call this%ownedProbes(i)%append(p)
                end if
    
            end do

        end do

    end subroutine findOwnedProbes

! *********************************************************************************************************************
    
    subroutine sampleSurfaces(this)

    !==================================================================================================================
    ! Description:
    !! sampleSurfaces runs the post processing task for the target probing object.
    !==================================================================================================================

        class(sampledSurfaceObj) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iProbe, iSurface, targetProbe, targetField, iField, iComp, nComp, ierr
    !------------------------------------------------------------------------------------------------------------------

        real :: probe(3)

        real, dimension(:), allocatable :: probeValue

        real, dimension(:), allocatable :: sbuf, rbuf
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        if(mod(this%runAt, itime)==0 .and. this%isActive) then
            
            do iSurface=1,this%nSurfaces
                
                ! Loop over fields to sample
                do iField=1,this%fieldToSampleIndex%size()
    
                    nComp = fieldRegistry(iField)%field%nComp
    
                    allocate(sbuf(this%rawSurfaces(iSurface)%nProbes))
                    allocate(rbuf(this%rawSurfaces(iSurface)%nProbes))
                    allocate(probeValue(nComp))
    
                    allocate(this%rawSurfaces(iSurface)%probeValues(this%rawSurfaces(iSurface)%nProbes, nComp))
                    this%rawSurfaces(iSurface)%probeValues = 0.0
    
                    targetField = this%fieldToSampleIndex%values(iField)
   
                    ! Start probing
                    do iProbe=1,this%ownedProbes(iSurface)%size()
    
                        ! Get the target probe
                        targetProbe = this%ownedProbes(iSurface)%values(iProbe)
    
                        probe(1) = this%rawSurfaces(iSurface)%x(targetProbe)
                        probe(2) = this%rawSurfaces(iSurface)%y(targetProbe)
                        probe(3) = this%rawSurfaces(iSurface)%z(targetProbe)
    
                        ! Interpolate
                        if(this%interpType=='nearestNeighbour') then
                            probeValue = nearestNeighbourCell(fieldRegistry(targetField)%field, probe)
                        elseif(this%interpType=='neighToProbe') then
                            !!probeValue = neighToProbe(fieldRegistry(targetField)%field, probe)
                        elseif(this%interpType=='faceToProbe') then
                            probeValue = faceToProbe(fieldRegistry(targetField)%field, probe)
                        else
                            call flubioStopMsg('ERROR: unknown interpolation type '//this%interpType)
                        end if
    
                        this%rawSurfaces(iSurface)%probeValues(targetProbe,1:nComp) = probeValue
    
                    end do
    
                    ! Gather the probe values from different processor
                    do iComp=1,nComp
                        sbuf(1:this%rawSurfaces(iSurface)%nProbes) = this%rawSurfaces(iSurface)%probeValues(1:this%rawSurfaces(iSurface)%nProbes,iComp)
                        call mpi_Reduce(sbuf, rbuf, this%rawSurfaces(iSurface)%nProbes, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
                        this%rawSurfaces(iSurface)%probeValues(1:this%rawSurfaces(iSurface)%nProbes, iComp) = rbuf
                    end do
    
                    ! WriteToFile, master node ONLY!
                    if(id==0) call this%writeProbesToFile(this%fields(iField), iSurface)
    
                    ! Deallocate arrays and get ready for the next probe object
                    deallocate(sbuf)
                    deallocate(rbuf)
                    deallocate(probeValue)
                    deallocate(this%rawSurfaces(iSurface)%probeValues)
    
                end do
            
            end do
            
        end if

    end subroutine sampleSurfaces

! *********************************************************************************************************************

    subroutine writeProbesToFile(this, fieldName, iSurface)

    !==================================================================================================================
    ! Description:
    !! writeProbesToFile write the probe values to a file.
    !==================================================================================================================

        class(sampledSurfaceObj) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: postDir

        character(len=20) :: timeStamp

        character(len=*) fieldName
    !------------------------------------------------------------------------------------------------------------------

        integer :: n, iSurface
    !------------------------------------------------------------------------------------------------------------------

        logical :: dirExists
    !------------------------------------------------------------------------------------------------------------------

        write(timeStamp,'(i0)') itime
        postDir = 'postProc/monitors/'//trim(this%dictName)//'/'//trim(timeStamp)//'/'

        ! Create a directory for the field. If it does not exists create it.
        inquire(file=trim(postDir)//'/.', exist=dirExists)

        ! create the folder
        if(.not. dirExists) call execute_command_line ('mkdir -p ' // adjustl(trim(postDir)))

        open(1, file=postDir//trim(this%stlSurfaceNames(iSurface))//'-'//trim(fieldName)//'.dat')
            write(1,*) '#   ', 'x   y   z   ', trim(fieldName)
            do n=1,this%rawSurfaces(iSurface)%nProbes
                write(1,*) this%rawSurfaces(iSurface)%x(n), this%rawSurfaces(iSurface)%y(n), this%rawSurfaces(iSurface)%z(n), this%rawSurfaces(iSurface)%probeValues(n,:)
            end do
        close(1)

    end subroutine writeProbesToFile

! *********************************************************************************************************************


end module sampledSurfaces