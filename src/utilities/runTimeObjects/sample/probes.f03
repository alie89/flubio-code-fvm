module probes

    use fieldvar, only: fieldRegistry
    use flubioFields
    use meshvar
    use m_strings
    use runTimeObject
    use qvector_m
    use iDynamicArray_Class

    implicit none

    ! Base class
    type, public, extends(runTimeObj) :: sampleObj
        character(len=:), allocatable :: dictName
        character(len=:), allocatable :: type
        character(len=:), allocatable :: interpType
        character(len=15), dimension(:), allocatable :: fields
        integer :: nn
        logical :: isActive
    end type sampleObj

    ! Extend for probes
    type, extends(sampleObj), public :: probeObj
        integer :: nProbes
        type(iDynamicArray) :: ownedProbes
        type(iDynamicArray) :: fieldToSampleIndex
        real, dimension(:), allocatable :: x
        real, dimension(:), allocatable :: y
        real, dimension(:), allocatable :: z
        real, dimension(:,:), allocatable :: probeValues

    contains
        procedure :: initialise => createProbes
        procedure :: run => executeProbing
        procedure :: findOwnedProbes
        procedure :: writeProbesToFile
        procedure :: clear
    end type probeObj

contains

    subroutine createProbes(this)

    !==================================================================================================================
    ! Description:
    !! createProbes is the class constructor.
    !==================================================================================================================

        class(probeObj) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: optName
        !! option name

        character(len=:), allocatable :: val
        !! value

        character(len=:), dimension(:), allocatable :: acvec
        !! first array element lenght
    !------------------------------------------------------------------------------------------------------------------

        integer, dimension(:),allocatable :: ilen
        !! first array element lenght

        integer :: i, iField, isize
    !------------------------------------------------------------------------------------------------------------------

        logical :: found, monitorsFound, monitorFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------

        this%dictName = this%objName

        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)

        ! Get monitors pointer
        call jcore%get_child(monitorsPointer, this%dictName, monitorPointer, monitorFound)

        !------------------------------!
        ! Get data from the dictionary !
        !------------------------------!

        ! Active
        call jcore%get(monitorPointer, 'active', this%isActive, found)
        if(.not. found) this%isActive = .true.

        ! Get interpolation type
        call jcore%get(monitorPointer, 'interpolation', this%interpType, found)
        if(.not. found) call flubioStopMsg('ERROR: cannot find the task interpolation type.')

        ! Get the number of neighbours
        call jcore%get(monitorPointer, 'numberOfNeighbours', this%nn, found)
        if(.not. found) this%nn = 4

        ! Get the sampling frequency
        call jcore%get(monitorPointer, 'sampleEvery', this%runAt, found)
        if(.not. found) call flubioStopMsg('ERROR: cannot find the sampling frequency.')

        ! Get fields to post-process
        call jcore%get(monitorPointer, 'fields', acvec, ilen, found)
        if(found) then
            allocate(this%fields(size(acvec)))
            do i=1,size(acvec)
                this%fields(i) = trim(acvec(i))
            end do
        else
            call flubioStopMsg('ERROR: cannot find the fields to process.')
        end if

        ! Get task type
        call jcore%get(monitorPointer, 'type', this%type, found)
        if(.not. found) call flubioStopMsg('ERROR: cannot find the task type.')

        ! Probe coordinates (not sure I can leave them as allocatable)
        call jcore%get(monitorPointer, 'nProbes', this%nProbes, found)
        if(.not. found) call flubioStopMsg('ERROR: cannot find the number of probes for task "'//this%dictName//'"')

        allocate(this%x(this%nProbes))
        allocate(this%y(this%nProbes))
        allocate(this%z(this%nProbes))

        call jcore%get(monitorPointer, 'x', this%x, found)
        if(.not. found) call flubioStopMsg('ERROR: cannot find the x coordiantes for task "'//this%dictName//'"')

        call jcore%get(monitorPointer, 'y', this%y, found)
        if(.not. found) call flubioStopMsg('ERROR: cannot find the y coordiantes for task "'//this%dictName//'"')

        call jcore%get(monitorPointer, 'z', this%z, found)
        if(.not. found) call flubioStopMsg('ERROR: cannot find the z coordiantes for task "'//this%dictName//'"')

        ! Find out the probes hosted by the processor
        isize = storage_size(i)/8
        this%ownedProbes = iDynamicArray(1)
        call this%findOwnedProbes()

        ! Find out fields in the registry
        isize = storage_size(i)/8
        this%fieldToSampleIndex = iDynamicArray(1)

        do iField=1,numberOfRegisteredFields
            do i=1,size(this%fields)
                if(trim(this%fields(i))==trim(fieldRegistry(iField)%field%fieldName(1))) then
                    call this%fieldToSampleIndex%append(iField)
                end if
            end do
        end do

        ! Clean up
        nullify(dictPointer)
        if(monitorsFound) nullify(monitorsPointer)
        if(monitorFound) nullify(monitorPointer)
        call jcore%destroy()

    end subroutine createProbes

! *********************************************************************************************************************

    subroutine executeProbing(this)

    !==================================================================================================================
    ! Description:
    !! executeProbing runs the post processing task for the target probing object.
    !==================================================================================================================

        class(probeObj) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iProbe, targetProbe, targetField, iField, iComp, nComp, ierr
    !------------------------------------------------------------------------------------------------------------------

        real :: probe(3)

        real, dimension(:), allocatable :: probeValue

        real, dimension(:), allocatable :: sbuf, rbuf
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        if(mod(itime, this%runAt)==0 .and. this%isActive) then

            ! Loop over fields to sample
            do iField=1,this%fieldToSampleIndex%size()

                nComp = fieldRegistry(iField)%field%nComp

                allocate(sbuf(this%nProbes))
                allocate(rbuf(this%nProbes))
                allocate(probeValue(nComp))

                allocate(this%probeValues(this%nProbes, nComp))
                this%probeValues = 0.0

                targetField = this%fieldToSampleIndex%values(iField)

                ! Start probing
                do iProbe=1,this%ownedProbes%size()

                    ! Get the target probe
                    targetProbe = this%ownedProbes%values(iProbe)

                    probe(1) = this%x(targetProbe)
                    probe(2) = this%y(targetProbe)
                    probe(3) = this%z(targetProbe)

                    ! Interpolate
                    if(this%interpType=='nearestNeighbour') then
                        probeValue = nearestNeighbourCell(fieldRegistry(targetField)%field, probe)
                    elseif(this%interpType=='neighToProbe') then
                        !!probeValue = neighToProbe(fieldRegistry(targetField)%field, probe)
                    elseif(this%interpType=='faceToProbe') then
                        probeValue = faceToProbe(fieldRegistry(targetField)%field, probe)
                    else
                        call flubioStopMsg('ERROR: unknown interpolation type '//this%interpType)
                    end if

                    this%probeValues(targetProbe,1:nComp) = probeValue

                end do

                ! Gather the probe values from different processor
                do iComp=1,nComp
                    sbuf(1:this%nProbes) = this%probeValues(1:this%nProbes,iComp)
                    call mpi_Reduce(sbuf, rbuf, this%nProbes, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
                    this%probeValues(1:this%nProbes, iComp) = rbuf
                end do

                ! writeToFile, master node ONLY!
                if(id==0) call this%writeProbesToFile(this%fields(iField))

                ! Deallocate arrays and get ready for the next probe object
                deallocate(sbuf)
                deallocate(rbuf)
                deallocate(probeValue)
                deallocate(this%probeValues)

            end do

        endif

    end subroutine executeProbing

! *********************************************************************************************************************

    subroutine findOwnedProbes(this)

    !==================================================================================================================
    ! Description:
    !! findOwnedProbes finds the probes hosted by a processor
    !==================================================================================================================

        class(probeObj) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: p, index
    !------------------------------------------------------------------------------------------------------------------

        real :: point(3)
    !------------------------------------------------------------------------------------------------------------------

        do p=1,this%nProbes

            point(1) = this%x(p)
            point(2) = this%y(p)
            point(3) = this%z(p)

            index = mesh%findPointOwnership(point)
            if(index>-1) then
                call this%ownedProbes%append(p)
            end if

        end do

    end subroutine findOwnedProbes

! *********************************************************************************************************************

    subroutine writeProbesToFile(this, fieldName)

    !==================================================================================================================
    ! Description:
    !! writeProbesToFile write the probe values to a file.
    !==================================================================================================================

        class(probeObj) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: postDir

        character(len=20) :: timeStamp

        character(len=*) fieldName
    !------------------------------------------------------------------------------------------------------------------

        integer :: n
    !------------------------------------------------------------------------------------------------------------------

        logical :: dirExists
    !------------------------------------------------------------------------------------------------------------------

        write(timeStamp,'(i0)') itime
        postDir = 'postProc/monitors/'//trim(this%dictName)//'/'//trim(timeStamp)//'/'

        ! Create a directory for the field. If it does not exists create it.
        inquire(file=trim(postDir)//'/.', exist=dirExists)

        ! create the folder
        if(.not. dirExists) call execute_command_line ('mkdir -p ' // adjustl(trim(postDir)))

        open(1, file=postDir//trim(fieldName)//'.dat')
            write(1,*) '#   ', 'x   y   z   ', trim(fieldName)
            do n=1,this%nProbes
                write(1,*) this%x(n), this%y(n), this%z(n), this%probeValues(n,:)
            end do
        close(1)

    end subroutine writeProbesToFile

! *********************************************************************************************************************

    subroutine clear(this)

    !==================================================================================================================
    ! Description:
    !! clear deallocates some member data for the pbe class.
    !==================================================================================================================

        class(probeObj) :: this
    !------------------------------------------------------------------------------------------------------------------

        ! Deallocate for the next probe if any
        deallocate(this%z)
        deallocate(this%x)
        deallocate(this%y)
        deallocate(this%fields)
        call this%ownedProbes%deallocate()

    end subroutine clear

! *********************************************************************************************************************

    subroutine checkProbe(found)

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        if(.not. found) call flubioStopMsg('ERROR: probe not found!')

    end subroutine checkProbe

!**********************************************************************************************************************
!                                                Interpolation methods
!**********************************************************************************************************************

    function nearestNeighbourCell(field, probe) result(probeValue)

        type(flubioField) :: field

        type(KdTreeSearch) :: search

        type(dArgDynamicArray) :: nearestNeighbour
    !------------------------------------------------------------------------------------------------------------------

        integer :: nn, indexes(1)

        real :: probe(3), probeValue(field%nComp), distances(1)
    !------------------------------------------------------------------------------------------------------------------

        if(flubioOptions%kdtreeType==0) then
            nearestNeighbour = search%kNearest(mesh%tree, &
                                            x=mesh%centroid(1,:), y=mesh%centroid(2,:), z=mesh%centroid(3,:), &
                                            xquery=probe(1), yquery=probe(2), zquery=probe(3), k=1)
            nn = nearestNeighbour%i%values(1)
            probeValue = field%phi(nn,:)
        else

            mesh%centroid(:,numberOfBElements+numberOfBFaces+1) = probe
            call n_nearest_to_around_point(mesh%tree_mk, numberOfBElements+numberOfBFaces+1, 1, 1, indexes, distances)
            nn = indexes(1)
            probeValue = field%phi(nn,:)

        endif

    end function nearestNeighbourCell

! *********************************************************************************************************************

    function faceToProbe(field, probe) result(probeValue)

        type(flubioField) :: field

        type(KdTreeSearch) :: search

        type(dArgDynamicArray) :: nearestNeighbour
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, nFaces, targetFace, iOwner, hostCell, boundaryProbe, indexes(1)
    !------------------------------------------------------------------------------------------------------------------

        real :: probe(3), r(3), faceDistance, probeValue(field%nComp), w, wsum, small, distances(1)
    !------------------------------------------------------------------------------------------------------------------

        small = 1e-10

        boundaryProbe = 0

        if(flubioOptions%KDtreeType==0) then
            nearestNeighbour = search%kNearest(mesh%tree, &
                                            x=mesh%centroid(1,:), y=mesh%centroid(2,:), z=mesh%centroid(3,:), &
                                            xquery=probe(1), yquery=probe(2), zquery=probe(3), k=1)

            hostCell = nearestNeighbour%i%values(1)
        else
            mesh%centroid(:,numberOfBElements+numberOfBFaces+1) = probe
            call n_nearest_to_around_point(mesh%tree_mk, numberOfBElements+numberOfBFaces+1, 1, 1, indexes, distances)
            hostCell = indexes(1)
        endif

        ! If the host cell is a boundary face, get its owner instead
        if(hostCell > numberOfElements) then
            iFace = numberOfIntFaces + (hostCell-numberOfElements)
            iOwner = mesh%owner(iFace)
            hostCell = iOwner
            boundaryProbe = 1
        end if

        ! Get list of face
        probeValue = 0.0
        wsum = 0.0
        nFaces = mesh%iFaces(hostCell)%csize

        do iFace=1,nFaces

            targetFace = mesh%iFaces(hostCell)%col(iFace)
            r = mesh%fcentroid(targetFace,:) - probe
            faceDistance = sqrt(r(1)**2 + r(2)**2 + r(3)**2)

            w = 1.0/(faceDistance**2 + small)
            wsum = wsum + w
            probeValue = probeValue + w*field%phif(targetFace,:)

            ! If the weigh is bigger than 1e+4, return the face value
            if(boundaryProbe==1 .and. w>10000.0) then
                probeValue = field%phif(targetFace,:)
                return
            end if

        end do

        ! Add cell center value as well
        w = 1.0/(nearestNeighbour%v%values(1) + small)
        probeValue = probeValue + w*field%phi(hostCell,:)
        wsum = wsum + w

        ! Divide with weights
        probeValue = probeValue/wsum

    end function faceToProbe

! *********************************************************************************************************************

end module probes