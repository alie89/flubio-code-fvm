module lineProbes

    use flubioMpi
    use probes

    implicit none

    ! Extend for line probes
    type, extends(probeObj), public :: lineProbeObj
        real :: startPoint(3)
        real :: endPoint(3)
    contains
        procedure :: initialise => createProbePoints
    end type lineProbeObj

contains

    subroutine createProbePoints(this)

    !==================================================================================================================
    ! Description:
    !! createProbePoints generates the cartisian ponts along the segment joining the inout points.
    !==================================================================================================================

        class(lineProbeObj) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: optName
        !! option name

        character(len=:), allocatable :: val
        !! value

        character(len=:), dimension(:), allocatable :: acvec
        !! first array element lenght
    !------------------------------------------------------------------------------------------------------------------

        integer, dimension(:),allocatable :: ilen
        !! first array element lenght

        integer :: i, n, iField, iTime, isize
    !------------------------------------------------------------------------------------------------------------------

        logical :: found, monitorsFound, monitorFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------

        real, dimension(:), allocatable :: t

        real, dimension(:), allocatable :: rvec
    !------------------------------------------------------------------------------------------------------------------

        this%dictName = this%objName

        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)

        ! Get monitors pointer
        call jcore%get_child(monitorsPointer, this%dictName, monitorPointer, monitorFound)

        !------------------------------!
        ! Get data from the dictionary !
        !------------------------------!

        ! Active
        call jcore%get(monitorPointer, 'active', this%isActive, found)
        if(.not. found) this%isActive = .true.

        ! Get interpolation type
        call jcore%get(monitorPointer, 'interpolation', this%interpType, found)
        if(.not. found) call flubioStopMsg('ERROR: cannot find the task interpolation type.')

        ! Get the number of neighbours
        call jcore%get(monitorPointer, 'numberOfNeighbours', this%nn, found)
        if(.not. found) this%nn = 4

        ! Get the sampling frequency
        call jcore%get(monitorPointer, 'sampleEvery', this%runAt, found)
        if(.not. found) call flubioStopMsg('ERROR: cannot find the sampling frequency.')

        ! Get fields to post-process
        call jcore%get(monitorPointer, 'fields', acvec, ilen, found)
        if(found) then
            allocate(this%fields(size(acvec)))
            do i=1,size(acvec)
                this%fields(i) = trim(acvec(i))
            end do
        else
            call flubioStopMsg('ERROR: cannot find the fields to process.')
        end if

        ! Get task type
        call jcore%get(monitorPointer, 'type', this%type, found)
        if(.not. found) call flubioStopMsg('ERROR: cannot find the task type.')

        ! Probe coordinates (not sure I can leave them as allocatable)
        call jcore%get(monitorPointer, 'nProbes', this%nProbes, found)
        if(.not. found) call flubioStopMsg('ERROR: cannot find the number of probes for task "'//trim(this%dictName)//'"')

        ! Parse start and end points
        call jcore%get(monitorPointer, 'startPoint', rvec, found)
        if(.not. found) call flubioStopMsg('ERROR: cannot find the start point for task "'//trim(this%dictName)//'"')
        this%startPoint = rvec

        call jcore%get(monitorPointer, 'endPoint', rvec, found)
        if(.not. found) call flubioStopMsg('ERROR: cannot find the end point for task "'//trim(this%dictName)//'"')
        this%endPoint = rvec

        allocate(this%x(this%nProbes))
        allocate(this%y(this%nProbes))
        allocate(this%z(this%nProbes))
        allocate(t(this%nProbes))

        t = linspace(0.0, 1.0, this%nProbes)

        this%x(1) = this%startPoint(1)
        this%y(1) = this%startPoint(2)
        this%z(1) = this%startPoint(3)

        this%x(this%nProbes) = this%endPoint(1)
        this%y(this%nProbes) = this%endPoint(2)
        this%z(this%nProbes) = this%endPoint(3)

        do n=2,this%nProbes-1
            this%x(n) = this%x(1) + t(n)*(this%x(this%nProbes)-this%x(1))
            this%y(n) = this%y(1) + t(n)*(this%y(this%nProbes)-this%y(1))
            this%z(n) = this%z(1) + t(n)*(this%z(this%nProbes)-this%z(1))
        end do

        ! Find out the probes hosted by the processor
        isize = storage_size(i)/8
        this%ownedProbes = iDynamicArray(1)
        call this%findOwnedProbes()

        ! Find out fields in the registry
        isize = storage_size(i)/8
        this%fieldToSampleIndex = iDynamicArray(1)

        do iField=1,numberOfRegisteredFields
            do i=1,size(this%fields)
                if(trim(this%fields(i))==trim(fieldRegistry(iField)%field%fieldName(1))) then
                    call this%fieldToSampleIndex%append(iField)
                end if
            end do
        end do

    end subroutine createProbePoints

! *********************************************************************************************************************

    function linspace(a, b, N)  result(x)

        implicit none

        integer :: i, N
   !--------------------------------------------------------------------------------------------------------------------

        real :: a, b, x(N)
    !--------------------------------------------------------------------------------------------------------------------

        if(N==1) then

            x(1) = a

        else

            do i = 1, N
                x(i) = a + (b-a)*real(i-1)/real(N-1)
            end do

        end if

    end function linspace

! *********************************************************************************************************************


end module lineProbes