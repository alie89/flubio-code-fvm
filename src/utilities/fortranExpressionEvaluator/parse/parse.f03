!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module parseFromFile

    use interpreter
    use meshvar
    use globalTimeVar
    use strings
    use m_strings

    implicit none

contains

! *********************************************************************************************************************

    subroutine fileParsing(fname, Q, nComp)

    !==================================================================================================================
    ! Description:
    !! fileParsing parses an expression from a file and transforms it into an array.
    !==================================================================================================================

        character(*) fname

        character(len=275), dimension(:), allocatable  :: expr
        !! boundary condition as inline expression

        character(len=500) :: func
        !! expression parsed from the boundary condition file

        character(len = 10), dimension(4) :: variables
        !! variables in string
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
        !! target element

        integer :: iComp
        !! target component

        integer :: nComp
        !! number of componets

        integer :: fieldSize
        !! size of the parsed expression
    !------------------------------------------------------------------------------------------------------------------

        real :: variablesValues(4)
        !! name of the variables to be interpreted

        real :: val
        !! auxiliary number

        real :: Q(numberOfElements, nComp)
        !! parsed expression turned into an array
    !------------------------------------------------------------------------------------------------------------------

        ! Read boundary Field to be asigned
        open(1,file=trim(fname))
            read(1,'(a)') func
            call split_in_array(func, expr, ' ')
        close(1)

        !---------------------!
        ! Evaluate expression !
        !---------------------!

        variables(1) = 'x'
        variables(2) = 'y'
        variables(3) = 'z'
        variables(4) = 'time'

        do iComp=1,nComp

            do iElement=1,numberOfElements

                ! variables
                variablesValues(1) = mesh%centroid(1,iElement) ! x
                variablesValues(2) = mesh%centroid(2,iElement) ! y
                variablesValues(3) = mesh%centroid(3,iElement) ! z
                variablesValues(4) = time ! t

                ! Set the your boundary field
                val = getExprValue(expr(iComp+1), variables, variablesValues, 4)
                Q(iElement, iComp) = val

            enddo

        enddo

    end subroutine fileParsing

! *********************************************************************************************************************

    subroutine expressionParsing(func, Q, nComp)

    !==================================================================================================================
    ! Description:
    !! expressionParsing converts a string into a real array of values.
    !==================================================================================================================

        character(len = 275), dimension(:), allocatable  :: expr
        !! boundary condition as inline expression

        character(len= * ) :: func
        !! expression parsed from the boundary condition file

        character(len = 10), dimension(4) :: variables
        !! variables in string
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
        !! target element

        integer :: iComp
        !! target component

        integer :: nComp
        !! number of componets

        integer :: fieldSize
        !! size of the parsed expression
    !------------------------------------------------------------------------------------------------------------------

        real :: variablesValues(4)
        !! name of the variables to be interpreted

        real :: val
        !! auxiliary number

        real :: Q(numberOfElements, nComp)
        !! parsed expression turned into an array
    !------------------------------------------------------------------------------------------------------------------

        ! Read boundary Field to be asigned
        call split_in_array(func, expr, ',')

        !---------------------!
        ! Evaluate expression !
        !---------------------!

        variables(1) = 'x'
        variables(2) = 'y'
        variables(3) = 'z'
        variables(4) = 'time'

        do iComp=1,nComp

            do iElement=1,numberOfElements

                ! variables
                variablesValues(1) = mesh%centroid(1,iElement) ! x
                variablesValues(2) = mesh%centroid(2,iElement) ! y
                variablesValues(3) = mesh%centroid(3,iElement) ! z
                variablesValues(4) = time ! t

                ! Set the your boundary field
                val = getExprValue(adjustl(expr(iComp)), variables, variablesValues, 4)
                Q(iElement, iComp) = val

            enddo

        enddo

    end subroutine expressionParsing

! *********************************************************************************************************************

    subroutine expressionParsingWithField(func, Q, field)

    !==================================================================================================================
    ! Description:
    !! expressionParsing converts a string into a real array of values.
    !==================================================================================================================

        use flubioFields

        type(flubioField) :: field

        character(len=275), dimension(:), allocatable  :: expr
        !! boundary condition as inline expression

        character(len = *) :: func
        !! expression parsed from the boundary condition file

        character(len = 10), dimension(field%nComp+4) :: variables
        !! variables in string
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
        !! target element

        integer :: iComp
        !! target component

        integer :: nComp
        !! number of componets

        integer :: iField
        !! target field component
    !------------------------------------------------------------------------------------------------------------------

        real :: variablesValues(4+field%nComp)
        !! name of the variables to be interpreted

        real :: val
        !! auxiliary number

        real :: Q(numberOfElements, field%nComp)
        !! parsed expression turned into an array
    !------------------------------------------------------------------------------------------------------------------

        nComp = field%nComp

        ! Read boundary Field to be asigned
        call split_in_array(func, expr, ',')

        !---------------------!
        ! Evaluate expression !
        !---------------------!

        variables(1) = 'x'
        variables(2) = 'y'
        variables(3) = 'z'
        variables(4) = 'time'

        do iComp=1,nComp
            variables(4 + iComp) = lowercase(field%fieldName(iComp))
        end do

        do iComp=1,nComp

            do iElement=1,numberOfElements

                ! variables
                variablesValues(1) = mesh%centroid(1,iElement) ! x
                variablesValues(2) = mesh%centroid(2,iElement) ! y
                variablesValues(3) = mesh%centroid(3,iElement) ! z
                variablesValues(4) = time ! t

                do iField=1,nComp
                    variablesValues(4 + iComp) = field%phi(iElement, iComp)
                end do

                ! Set the your boundary field
                val = getExprValue(adjustl(expr(iComp)), variables, variablesValues, 4+nComp)
                Q(iElement, iComp) = val

            enddo

        enddo

    end subroutine expressionParsingWithField

! *********************************************************************************************************************

    real function getExprValue(expr, var, varVal, nval)

        use interpreter
        use flubioMpi
    !------------------------------------------------------------------------------------------------------------------

        integer :: nval
    !------------------------------------------------------------------------------------------------------------------

        character(len = 10), dimension(nval) :: var

        character(len = 275) :: expr

        character (len = 5) :: statusFlag
    !------------------------------------------------------------------------------------------------------------------

        real :: varVal(nval)
    !------------------------------------------------------------------------------------------------------------------

        !Initializes function
        call init(expr, var, statusFlag)

        ! Evaluate expression
        if(statusFlag=='ok') then
            getExprValue = evaluate(varVal)
        else
            call flubioStopMsg('ERROR: cannot parse expression: '//trim(expr))
        end if

        ! Destroy actual function evaluator
        call destroyfunc()

    end function getExprValue

end module parseFromFile
