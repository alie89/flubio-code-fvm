!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module gradient

!==================================================================================================================
! Description:
!! gradient is the module that allows to compute a field gradient. It contains the main driver which wraps
!! all the gradient methods.
!==================================================================================================================

	use flubioDictionaries
	use flubioFields
	use meshvar

	use greenGauss
	use leastSquares
	use faceLimited
	use cellLimited

	implicit none

contains

	subroutine computeGradient(field)

	!==================================================================================================================
	! Description:
	!! computeGradient computes the gradient of a given field.
	!==================================================================================================================

		type(flubioField) :: field		
		!! field used to compute the gradient
	!------------------------------------------------------------------------------------------------------------------

		integer :: iComp
		!! target component 
		
		integer :: nComp
		!! number of field components
	!------------------------------------------------------------------------------------------------------------------

		nComp = field%nComp

		if(field%gradientType==1) then

		!-----------------------!
		! Least square gradient !
		!-----------------------!

			call leastSquareGrad(field)

		elseif(field%gradientType==2) then

			call greenGaussGradTVD(field, flubioOptions%gradientFaceLimiter)

		else

		!----------------------!
		! Green-Gauss gradient !
		!----------------------!

			call greenGaussGrad(field)

		endif

		!-------------------!
		! Gradient limiter  !
		!-------------------!

		if(field%gradientLimiterType==1) then

			call cellLimitedMDGradient(field)

		elseif(field%gradientLimiterType==2) then

			call faceLimitedMDGradient(field)

		elseif(field%gradientLimiterType==3) then

			call cellLimitedGradient(field)

		endif

		!--------------------!
		! Boundary Gradients !
		!--------------------!

		call boundaryGradient(field)

		!---------------!
		! Update ghosts !
		!---------------!

		call field%updateGhostsGradient()
		
	end subroutine computeGradient

! *********************************************************************************************************************

	subroutine boundaryGradient(field)

	!==================================================================================================================
	! Description:
	!! boundaryGradient computes the wall gradient vector.
	!==================================================================================================================

		type(flubioField) :: field
		!! field used to compute the gradient
	!------------------------------------------------------------------------------------------------------------------

		integer iComp, nComp, iBoundary, iOwner, iBFace, patchFace, is, ie
	!------------------------------------------------------------------------------------------------------------------
		
		real dPhi
		!!  difference between cell center and face center values

		real :: gradPhi
		!! gradient of phi at cell center

		real :: nf(3)
		!! boundary face normal

		real :: gradPhif(3)
		!! gradient of phi at cell face

		real :: CN(3)
		!! distance between cell center nad face center

		real :: nonOrthVec(3)
		!! nonr orthogonality vector

		real :: alpha
		!! non-orthogonality angle

		real :: dot
		!! auxiliary variable to store a doct product

		real :: dperp
		!! perpendicular distance between face center and cell center

		real :: nonOrthCorr
		!! number of non-orthogonal corrections
	!------------------------------------------------------------------------------------------------------------------

		nComp = field%nComp
		patchFace = numberOfElements

		!----------------------!
		! loop over boundaries !
		!----------------------!

		do iBoundary=1,numberOfRealBound

			is = mesh%boundaries%startFace(iBoundary)
			ie = is+mesh%boundaries%nFace(iBoundary)-1

			do iBFace=is, ie

				patchFace = patchFace+1
				iOwner = mesh%owner(iBFace)

				!---------------------!
				! Compute quantitites !
				!---------------------!

				do iComp=1,nComp

					dPhi = field%phi(patchFace,iComp) - field%phi(iOwner,iComp)

					nf = mesh%Sf(iBFace,:)/norm2(mesh%Sf(iBFace,:))
					CN = mesh%CN(iBFace,:)
					gradPhif = field%phiGrad(patchFace,:,iComp)

					!---------------------------!
					! Non orthogonal correction !
					!---------------------------!

					nonOrthVec = mesh%Tf(iBFace,:)/norm2(mesh%Sf(iBFace,:))
					nonOrthCorr = dot_product(gradPhif,nonOrthVec)

					dperp = dot_product(CN,nf)
					dot = dot_product(nf, mesh%CN(iBFace,:)/norm2(mesh%CN(iBFace,:)))

					alpha = 1.0/dot

					!--------------------!
					! Corrected gradient !
					!--------------------!

					gradPhi = (alpha*dPhi/dperp + nonOrthCorr)

					!-------------------------!
					! Surface Normal Gradient !
					!-------------------------!

					field%phiGrad(patchFace,1,iComp) = gradPhif(1) + nf(1)*(gradPhi - gradPhif(1)*nf(1))
					field%phiGrad(patchFace,2,iComp) = gradPhif(2) + nf(2)*(gradPhi - gradPhif(2)*nf(2))
					field%phiGrad(patchFace,3,iComp) = gradPhif(3) + nf(3)*(gradPhi - gradPhif(3)*nf(3))

				end do

			enddo

		enddo
		
	end subroutine boundaryGradient
	
! *********************************************************************************************************************

	function surfaceNormalGradient(field, scaled) result(snGrad)

	!==================================================================================================================
	! Description:
	!! surfaceNormalGradient computes the gradient times the surface normal.
	!==================================================================================================================

		type(flubioField) :: field
		!! field used to compute the gradient
	!------------------------------------------------------------------------------------------------------------------

		character(len=*) :: scaled
	!------------------------------------------------------------------------------------------------------------------

		integer iComp, iFace, patchFace
	!------------------------------------------------------------------------------------------------------------------

		real :: nf(3)
		!! boundary face normal

		real :: phiGrad_f(numberOfFaces,3,field%nComp)
		!! gradient of phi at cell face

		real :: snGrad(numberOfFaces, field%nComp)
		!! cell face normal gradient

		real :: dot
		!! auxiliary variable to store a doct product
	!------------------------------------------------------------------------------------------------------------------

		phiGrad_f = 0.0
		call interpolateGradientCorrected(phiGrad_f, field, opt=1, fComp=field%nComp)

		do iFace=1,numberOfFaces

			if (trim(scaled)=='unscaled') then
				nf = mesh%Sf(iFace, :)
			else
				nf = mesh%Sf(iFace, :)/norm2(mesh%Sf(iFace, :))
			end if

			do iComp=1, field%nComp
				dot = dot_product(phiGrad_f(iFace,:,iComp), nf)
				snGrad(iFace, iComp) = dot
			enddo

		end do

	end function surfaceNormalGradient

! *********************************************************************************************************************

	subroutine correctBoundaryNormalGradient(field, snGrads)

	!==================================================================================================================
	! Description:
	!! boundaryGradient computes the wall gradient vector.
	!==================================================================================================================

		type(flubioField) :: field
		!! field used to compute the gradient
	!------------------------------------------------------------------------------------------------------------------

		integer iComp, nComp, iBoundary, iOwner, iBFace, patchFace, is, ie
	!------------------------------------------------------------------------------------------------------------------
		
		real :: snGrads(numberOfFaces, field%nComp) 
		!! surface normal gradients

		real dPhi
		!!  difference between cell center and face center values

		real :: gradPhi
		!! gradient of phi at cell center

		real :: nf(3)
		!! boundary face normal

		real :: gradPhif(3)
		!! gradient of phi at cell face

		real :: CN(3)
		!! distance between cell center nad face center

		real :: nonOrthVec(3)
		!! nonr orthogonality vector

		real :: alpha
		!! non-orthogonality angle

		real :: dot
		!! auxiliary variable to store a doct product

		real :: dperp
		!! perpendicular distance between face center and cell center

		real :: nonOrthCorr
		!! number of non-orthogonal corrections
	!------------------------------------------------------------------------------------------------------------------

		nComp = field%nComp
		patchFace = numberOfElements

		!----------------------!
		! loop over boundaries !
		!----------------------!

		do iBoundary=1,numberOfRealBound

			is = mesh%boundaries%startFace(iBoundary)
			ie = is+mesh%boundaries%nFace(iBoundary)-1

			do iBFace=is, ie

				patchFace = patchFace+1
				iOwner = mesh%owner(iBFace)

				!---------------------!
				! Compute quantitites !
				!---------------------!

				do iComp=1,nComp

					dPhi = field%phi(patchFace,iComp) - field%phi(iOwner,iComp)

					nf = mesh%Sf(iBFace,:)/norm2(mesh%Sf(iBFace,:))
					CN = mesh%CN(iBFace,:)
					gradPhif = field%phiGrad(patchFace,:,iComp)

					!---------------------------!
					! Non orthogonal correction !
					!---------------------------!

					nonOrthVec = mesh%Tf(iBFace,:)/norm2(mesh%Sf(iBFace,:))
					nonOrthCorr = dot_product(gradPhif,nonOrthVec)

					dperp = dot_product(CN,nf)
					dot = dot_product(nf, mesh%CN(iBFace,:)/norm2(mesh%CN(iBFace,:)))

					alpha = 1.0/dot

					!--------------------!
					! Corrected gradient !
					!--------------------!

					gradPhi = (alpha*dPhi/dperp + nonOrthCorr)

					!-------------------------!
					! Surface Normal Gradient !
					!-------------------------!

					snGrads(iBFace, iComp) = gradPhi
					
				end do

			enddo

		enddo
		
	end subroutine correctBoundaryNormalGradient

! *********************************************************************************************************************

end module gradient