!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module leastSquares

!==================================================================================================================
! Description:
!! leastSquares implementts the least square method for the computation of the gradients.
!==================================================================================================================

	use flubioDictionaries
	use flubioFields
	use meshvar

	implicit none

contains

	subroutine leastSquareGrad(field)

	!==================================================================================================================
	! Description:
	!! leastSquareGrad computes the gradient using the least square method.
	!! For a cartesian and equispaced mesh, the standard Green-Gauss formula is recovered.
	!==================================================================================================================

		type(flubioField) :: field
	!------------------------------------------------------------------------------------------------------------------

		integer i, is, ie, pNeigh, patchFace, iBoundary
	!------------------------------------------------------------------------------------------------------------------

		integer iElement, iBElements, iFace, iBFace, iComp, fComp, iOwner, iNeighbour, iProc, ghostFace
	!------------------------------------------------------------------------------------------------------------------

		real ::  w
		!! weight

		real :: dPhi
		!! difference between onwer and neighbour values

		real :: lsq_mat(numberOfElements,6)
		!! least square matrix

		real :: lsq_rhs(numberOfElements,3)
		!!  least squares righ hand side

		real :: A(3,3)
		!! cell least squares matrix

		real :: x(3)
		!! solution

		real :: y(3)
		!! cell  least squares matrix
	!------------------------------------------------------------------------------------------------------------------

		fComp = field%nComp

		! update field at ghosts
		call field%updateGhosts()

		! compute least squares matrix
		lsq_mat = mesh%leastSquareMatrix()


		do iComp=1,fComp

		   lsq_rhs=0.0

		   patchFace=numberOfElements

		   do iFace=1,numberOfIntFaces

			  iOwner = mesh%owner(iFace)
			  iNeighbour = mesh%neighbour(iFace)

			  !-----------------!
			  ! Compute weights !
			  !-----------------!

			  w = 1.0/sqrt(mesh%CN(iFace,1)**2 + mesh%CN(iFace,2)**2 + mesh%CN(iFace,3)**2)
			  dPhi = field%phi(iNeighbour,iComp) - field%phi(iOwner,iComp)

			  !--------------------------!
			  ! Compute least square rhs !
			  !--------------------------!

		  	  ! Owner Cell
			  lsq_rhs(iOwner,1) = lsq_rhs(iOwner,1) + w*mesh%CN(iFace,1)*dPhi
			  lsq_rhs(iOwner,2) = lsq_rhs(iOwner,2) + w*mesh%CN(iFace,2)*dPhi
			  lsq_rhs(iOwner,3) = lsq_rhs(iOwner,3) + w*mesh%CN(iFace,3)*dPhi

			  ! Neighbour Cell, note that: dPhi = -dPhi and CN = -CN, thus again a plus at rhs
			  lsq_rhs(iNeighbour,1) = lsq_rhs(iNeighbour,1) + w*mesh%CN(iFace,1)*dPhi
			  lsq_rhs(iNeighbour,2) = lsq_rhs(iNeighbour,2) + w*mesh%CN(iFace,2)*dPhi
			  lsq_rhs(iNeighbour,3) = lsq_rhs(iNeighbour,3) + w*mesh%CN(iFace,3)*dPhi

		   enddo

		   !----------------------!
		   ! Processor Boundaries !
		   !----------------------!

		   do iBoundary=1,numberOfProcBound

			   iProc = mesh%boundaries%procBound(iBoundary)
			   is = mesh%boundaries%startFace(iProc)
			   ie = is+mesh%boundaries%nFace(iProc)-1
			   ghostFace = numberOfElements + (is-numberOfIntFaces-1)
			   pNeigh=0

			   do iBFace=is,ie

				   pNeigh=pNeigh+1
				   iOwner = mesh%owner(iBFace)

				   ! Compute weights
				   w = 1.0/sqrt(mesh%CN(iBFace,1)**2 + mesh%CN(iBFace,2)**2 + mesh%CN(iBFace,3)**2)
				   dPhi = field%phi(ghostFace+pNeigh,iComp) - field%phi(iOwner,iComp)

			   	   ! Compute least square rhs
				   lsq_rhs(iOwner,1) = lsq_rhs(iOwner,1) +  w*mesh%CN(iBFace,1)*dPhi
				   lsq_rhs(iOwner,2) = lsq_rhs(iOwner,2) +  w*mesh%CN(iBFace,2)*dPhi
				   lsq_rhs(iOwner,3) = lsq_rhs(iOwner,3) +  w*mesh%CN(iBFace,3)*dPhi

			   enddo

		   enddo
	   	
		   !-----------------!
		   ! Real Boundaries !
		   !-----------------!

		   do iBoundary=1,numberOfRealBound

			   is = mesh%boundaries%startFace(iBoundary)
			   ie = is+mesh%boundaries%nFace(iBoundary)-1

			   do iBFace=is,ie

				   patchFace = patchFace+1
				   iOwner = mesh%owner(iBFace)

				   ! Compute weights
				   w = 1.0/sqrt(mesh%CN(iBFace,1)**2 + mesh%CN(iBFace,2)**2 + mesh%CN(iBFace,3)**2)
				   dPhi = field%phi(patchFace,iComp) - field%phi(iOwner,iComp)

			 	   ! Compute least square rhs
				   lsq_rhs(iOwner,1) = lsq_rhs(iOwner,1) +  w*mesh%CN(iBFace,1)*dPhi
				   lsq_rhs(iOwner,2) = lsq_rhs(iOwner,2) +  w*mesh%CN(iBFace,2)*dPhi
				   lsq_rhs(iOwner,3) = lsq_rhs(iOwner,3) +  w*mesh%CN(iBFace,3)*dPhi

			  enddo

		   enddo

		   !--------------------------------------------------------------!
		   ! Solve LSQ linear system by gaussian elimination in each cell !
		   !--------------------------------------------------------------!

		   do iElement=1,numberOfElements

			  A(1,1) = lsq_mat(iElement,1)
			  A(1,2) = lsq_mat(iElement,4)
			  A(1,3) = lsq_mat(iElement,5)
			  A(2,1) = lsq_mat(iElement,4)
			  A(2,2) = lsq_mat(iElement,2)
			  A(2,3) = lsq_mat(iElement,6)
			  A(3,1) = lsq_mat(iElement,5)
			  A(3,2) = lsq_mat(iElement,6)
			  A(3,3) = lsq_mat(iElement,3)

			  y = lsq_rhs(iElement,1:3)

			  ! Solve LSQ system using a local gaussian elimination
			  call gauss_2(A,y,x,3)

			  field%phiGrad(iElement,1:3,iComp) = x(1:3)

		   enddo

		enddo ! End of component

		! Set boundary gradient
		patchFace=numberOfElements

		do iBFace=numberOfIntFaces+1,numberOfFaces

		   patchFace = patchFace+1
		   iOwner = mesh%owner(iBFace)

		   ! equal to the owner element and then overridden
		   field%phiGrad(patchFace,:,:) = field%phiGrad(iOwner,:,:)

		enddo

	end subroutine leastSquareGrad

! *********************************************************************************************************************

	subroutine gauss_2(a,b,x,n)

	!==================================================================================================================
	!! Solutions to a system of linear equations A*x=b
	!! Method: Gauss elimination (with scaling and pivoting)
	!! N.B: this solver does NOT work in parallel and it must be used locally!
	! Alex G. (November 2009)
	!-----------------------------------------------------------
	! input ...
	! a(n,n) - array of coefficients for matrix A
	! b(n)   - array of the right hand coefficients b
	! n      - number of equations (size of matrix A)
	! output ...
	! x(n)   - solutions
	! coments ...
	! the original arrays a(n,n) and b(n) will be destroyed
	! during the calculation
	!==================================================================================================================

		implicit none

		integer n
		real a(n,n), b(n), x(n)
		real s(n)
		real c, pivot, store
		integer i, j, k, l

		! step 1: begin forward elimination
		do k=1, n-1

			! step 2: "scaling"
			! s(i) will have the largest element from row i
			do i=k,n                       ! loop over rows
				s(i) = 0.0
				do j=k,n                    ! loop over elements of row i
					s(i) = max(s(i),abs(a(i,j)))
				end do
			end do

			! step 3: "pivoting 1"
			! find a row with the largest pivoting element
			pivot = abs(a(k,k)/s(k))
			l = k
			do j=k+1,n
				if(abs(a(j,k)/s(j)) > pivot) then
					pivot = abs(a(j,k)/s(j))
					l = j
				end if
			end do

			! Check if the system has a sigular matrix
			if(pivot==0.0) then
				write(*,*) ' The matrix is sigular '
				return
			end if

			! step 4: "pivoting 2" interchange rows k and l (if needed)
			if (l /= k) then
				do j=k,n
					store = a(k,j)
					a(k,j) = a(l,j)
					a(l,j) = store
				end do
				store = b(k)
				b(k) = b(l)
				b(l) = store
			end if

			! step 5: the elimination (after scaling and pivoting)
			do i=k+1,n
				c=a(i,k)/a(k,k)
				a(i,k) = 0.0
				b(i)=b(i)- c*b(k)
				do j=k+1,n
					a(i,j) = a(i,j)-c*a(k,j)
				end do
			end do
		end do

		! step 6: back substiturion
		x(n) = b(n)/a(n,n)
		do i=n-1,1,-1
			c=0.0
			do j=i+1,n
				c= c + a(i,j)*x(j)
			end do
			x(i) = (b(i)- c)/a(i,i)
		end do

	end subroutine gauss_2

! *********************************************************************************************************************

end module leastSquares