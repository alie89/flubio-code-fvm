!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module transportEq

!==================================================================================================================
! Description:
!! This modules implements the general transport equation and its data structure. It is particularly important
!! since all other equation types are derived from this class.
!==================================================================================================================

#include "petsc/finclude/petscksp.h"
	use petscksp

    use flubioDictionaries
    use math
    use meshvar
    use diffusionFluxes
    use convectiveFluxes
    use timeSchemes
    use sourceTerms
    use operators, only: div

    implicit none

    type, public :: amgclParams

        integer :: solverType
        integer :: coarseningType
        integer :: relaxationType
        integer :: npre
        integer :: npost
        integer :: ncycle
        integer :: maxIter
        integer :: max_levels
        integer :: pre_cycles
        integer :: coarse_enough
        integer :: fillIn
        integer :: mrestart

        integer :: allow_rebuild
        integer :: direct_coarse
        integer :: do_trunc

        real :: damping
        real :: eps_strong
        real :: eps_trunc

    end type amgclParams

    type, public :: transportEqn

        Mat A
        !!  PETSc matrix.

        Vec rhs
        !!  PETSc right hand side

        KSP ksp
        !! PETSc ksp to use

        type(amgclParams) :: amgclSolverOptions
        !! amgcl solver options if any

        type(sourceTermsArray), dimension(:), allocatable :: fvSources
        !! Vector containing the source files if any

        logical :: isAllocated
        !! flag to check if A and rhs are allocated or not

        logical :: coeffsAllocated
        !! flag to check if aC, anb and bC are allocated

        character(len=:), allocatable :: eqName
        !! Name of the choosen convective scheme

        character(len=:), allocatable :: convOptName
        !! Name of the choosen convective scheme

        character(len=:), allocatable :: timeOptName
        !! Name of the choosen time scheme

        character(len=:), allocatable :: solverName
        !! Name of the choosen linear solver

        character(len=:), allocatable :: pcName
        !! Name of the choosen preconditioner

        character(len=:), allocatable :: solverType
        !! SolverType, PETSc or AMGCL

        integer :: convImp
        !! Flag for the implcit convection

        integer :: convOpt
        !! Flag for the selected convective scheme

        integer :: timeOpt
        !! Flag for the selected time scheme

        integer :: solver
        !! Flag for the solver

        integer :: pc
        !! Flag for preconditioner

        integer :: boundedCorr
        !! Flag for the bounded correction -div(U)*phi

        integer :: adaptiveRelaxation
        !! flag for adaptive relaxation

        integer :: nComp
        !! number of componets of the equation

        integer :: maxIter
        !! maximum number of iterations

        integer :: minIter
        !! minimum number of iterations

        real, dimension(:,:), allocatable :: aC
        !! Diagonal coefficients for a transport equation matrix

        type(realCellStructMat), dimension(:), allocatable :: anb
        !! Off-diagonal coefficients for a transport equation matrix

        real, dimension(:,:), allocatable :: bC
        !! Right hand side for a transport equation

        real, dimension(:,:), allocatable :: aCD
        !! aC with only contribution from diffusion

        real, dimension(:,:), allocatable :: aCC
        !! aC with only contribution from convection

        real, dimension(:,:), allocatable :: aCC0
        !! old value for aCC

        real :: urf
        !! under relaxation factor for a transport equation

        real, dimension(:), allocatable :: res
        !! Actual equation residual

        real, dimension(:), allocatable :: scalingRef
        !! Actual equation scaled residual

        real, dimension(:), allocatable :: unscaledRes
        !! Actual equation unscaled residual

        real, dimension(:), allocatable :: convergenceResidual
        !! Actual equation unscaled residual

        real, dimension(:), allocatable :: resmax
        !! Maximum residual for the equation

        real :: absTol
        !! Linear solver maxium absolute tolerace for the equation

        real :: relTol
        !! Linear solver maxium relative tolerace for the equation

        contains
            procedure :: createEq => createTransportEq
            procedure :: destroyEq => destroyTransportEq
            procedure :: createEqCoeffs => createTransportEqCoeffs
            procedure :: destroyEqCoeffs => destroyTransportEqCoeffs
            procedure :: clearMatAndRhs
            procedure :: resetCoeffs

            procedure :: setNumericalOptions
            procedure :: setUnderRelaxationFactor
            procedure :: setEqOptions
            procedure :: setEqOptionsWithoutConvection
            procedure :: setSolverOptions
            procedure :: setEquationSources
            procedure :: getAMGCLOptions
            procedure :: AMGCLDefaults

            procedure :: checkSteadyState

            generic ::   addTransientTerm => addTransientTermStd, addTransientTermWithCoeff
            procedure :: addTransientTermStd
            procedure :: addTransientTermWithCoeff

            generic :: addDiffusionTerm => addDiffusionTermWithFieldDiffusivity, addDiffusionTermWithConstantDiffusivity, &
                                           addDiffusionTermWithoutCrossDiffusionWithConstantDiffusivity, &
                                           addDiffusionTermWithoutCrossDiffusionWithFieldDiffusivity
            procedure :: addDiffusionTermWithFieldDiffusivity
            procedure :: addDiffusionTermWithConstantDiffusivity
            procedure :: addDiffusionTermWithoutCrossDiffusionWithFieldDiffusivity
            procedure :: addDiffusionTermWithoutCrossDiffusionWithConstantDiffusivity

            generic :: addConvectiveTerm => addConvectiveTermDC, addConvectiveTermWithCoeff
            generic :: addConvectiveTermImplicit => addConvectiveTermIC, addConvectiveTermImplicitWithCoeff
            procedure :: addConvectiveTermDC
            procedure :: addConvectiveTermWithCoeff
            procedure :: addConvectiveTermIC
            procedure :: addConvectiveTermImplicitWithCoeff

            procedure :: addBoundedTerm
            procedure :: relaxEq
            procedure :: updateURF
            procedure :: implicitPressureGradCentralCoeff
            procedure :: addBodyForce
            procedure :: addBalancedBodyForce
            procedure :: addDevTerm
            procedure :: addSubstantialDerivative
            procedure :: addExplicitSourceTerm
            procedure :: addLinearizedSourceTerm
            procedure :: addCustomSourceTerm
            procedure :: addVolumeSourceTerms
            procedure :: updateRhsWithCrossDiffusion
            procedure :: updateRhsWithCrossDiffusionWithConstantDiffusivity
            procedure :: applyBoundaryConditions => applyBoundaryConditionsTransport
            procedure :: assembleEq
            procedure :: assembleEqWithCoeff
            procedure :: assembleMat
            procedure :: assembleRhs
            procedure :: setReferenceValue
            procedure :: setMatReferenceValue
            procedure :: convertToCSR
            procedure :: eliminateEquations

            procedure :: setUpDiffusionTerm
            procedure :: setUpConvectiveTerm

            procedure :: allocateMatrices
            procedure :: setLinearSolver
            procedure :: solve
            procedure :: explicitSolve
            procedure :: scaledResidual2
            procedure :: getNorm
            procedure :: checkConvergence
            procedure :: getResidualForConvergenceCheck
            procedure :: writeResidual
            procedure :: printResidual
            procedure :: viewKspSettings

            procedure :: initialResidualPETSc
            procedure :: initialResidualFlubio
            procedure :: scaledResidualPETSc
            procedure :: scaledResidualFlubio

            procedure :: H
            procedure :: H1
            procedure :: sumMagAnb

            procedure :: verbose
            procedure :: kspVerbose
            procedure :: amgclVerbose
            procedure :: exportMatrixToFile
            procedure :: printCurrentIteration
            procedure :: printDelimiter

            ! TEST
            procedure :: solveAMGCL

    end type transportEqn

contains

! *********************************************************************************************************************

    subroutine createTransportEq(this, eqName, nComp)

    !==================================================================================================================
    ! Description:
    !! createTransportEq is the class constructor.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: eqName
        !! equation name
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement

        integer :: nComp
        !! number of components
    !------------------------------------------------------------------------------------------------------------------

        this%nComp = nComp

        call this%createEqCoeffs()

        allocate(this%res(nComp))
        allocate(this%resmax(nComp))
        allocate(this%scalingRef(nComp))
        allocate(this%unscaledRes(nComp))
        allocate(this%convergenceResidual(nComp))

        this%scalingRef = 0.0
        this%unscaledRes = 0.0
        this%convergenceResidual = 0.0
        this%resmax = 0.0

        this%eqName = eqName

        this%isAllocated = .false.

    end subroutine createTransportEq

! *********************************************************************************************************************

    subroutine destroyTransportEq(this)

    !==================================================================================================================
    ! Description:
    !! createTransportEq is the class de-constructor.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        call this%destroyEqCoeffs()

        deallocate(this%res)
        deallocate(this%scalingRef)
        deallocate(this%unscaledRes)
        deallocate(this%convergenceResidual)
        deallocate(this%resmax)

        call KSPDestroy(this%ksp,ierr)
        call MatDestroy(this%A, ierr)
        call VecDestroy(this%rhs, ierr)

    end subroutine destroyTransportEq

! *********************************************************************************************************************

    subroutine createTransportEqCoeffs(this)

    !==================================================================================================================
    ! Description:
    !! createTransportEqCoeffs allocates equation coefficients and rhs.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        if(.not. this%coeffsAllocated) then
            allocate(this%aC(numberOfElements, this%nComp))
            allocate(this%anb(numberOfElements))
            allocate(this%bC(numberOfElements, this%nComp))

            this%aC = 0.0
            this%bC = 0.0

            do iElement=1,numberOfElements
                this%anb(iElement)%csize_i = mesh%numberOfNeighbours(iElement)
                this%anb(iElement)%csize_j = this%nComp
                call this%anb(iElement)%initRealMat(mesh%numberOfNeighbours(iElement), this%nComp)
            end do

            this%coeffsAllocated = .true.
        endif

    end subroutine createTransportEqCoeffs

! *********************************************************************************************************************

    subroutine destroyTransportEqCoeffs(this)

    !==================================================================================================================
    ! Description:
    !! destroyTransportEqCoeffs deallocates equation coefficients and rhs.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        if(this%coeffsAllocated) then

            deallocate(this%aC)
            deallocate(this%anb)
            deallocate(this%bC)

            this%coeffsAllocated = .false.

        endif

    end subroutine destroyTransportEqCoeffs

! *********************************************************************************************************************

    subroutine clearMatAndRhs(this)

    !==================================================================================================================
    ! Description:
    !! clearMatAndRhs clears the matrix A and the rhs.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        call KSPDestroy(this%ksp,ierr)
        call MatDestroy(this%A, ierr)
        call VecDestroy(this%rhs, ierr)

        this%isAllocated = .false.

    end subroutine clearMatAndRhs

! *********************************************************************************************************************

    subroutine setEqOptions(this, kspName)

    !==================================================================================================================
    ! Description:
    !! setEqOptions sets the solver and discretization options for the target transport equation by parsing the settings from
    !! the relevant dictionaries.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: kspName
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName, key, solverType, pcType, convOpt, relax
    !------------------------------------------------------------------------------------------------------------------

        integer :: intValue
    !------------------------------------------------------------------------------------------------------------------

        real :: anumber

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        !----------------!
        ! Solver options !
        !----------------!

        call flubioSolvers%json%get(this%eqName//'%solverType', solverType, found)
        if(found) then
            this%solverType = lowercase(solverType)
        else
            this%solverType = 'petsc'
        endif

        ! Add additional options as command line, if any
        call this%setSolverOptions()
        if(this%solverType=='amgcl') call this%getAMGCLOptions()

        call flubioSolvers%json%get(this%eqName//'%solver', solverType, found)
        call raiseKeyErrorJSON(this%eqName//'%solver', found, 'settings/solvers')
        this%solverName = solverType

        call flubioSolvers%json%get(this%eqName//'%preconditioner', pcType, found)
        call raiseKeyErrorJSON(this%eqName//'%preconditioner', found, 'settings/solvers')
        this%pcName = pcType

        call flubioSolvers%setPreconditionerOption(lowercase(this%pcName), this%pc, this%solverType)

        !--------------------!
        ! Numerical settings !
        !--------------------!

        call flubioOptions%setTimeSchemeOption(this%eqName, this%timeOpt, this%timeOptName)

        call flubioOptions%json%get(this%eqName//'%convectiveScheme', convOpt, found)
        if(.not. found) convOpt = 'none'
        this%convOptName = lowercase(convOpt)
        call flubioOptions%setConvectionOption(this%convOptName, this%convOpt)

        ! Check or assign a default
        if(this%convOpt==-1 .and. flubioOptions%convOpt==-1) then
            call printConvectiveSchemes(this%eqname)
        elseif(this%convOpt==-1 .and. flubioOptions%convOpt/=-1) then
            call flubioOptions%json%get('Default%convectiveScheme', convOpt, found)
            this%convOptName = convOpt
            this%convOpt = flubioOptions%convOpt
        end if

        call flubioOptions%setConvectionImplicit(this%eqName, this%convImp)
        call flubioOptions%setBoundedCorrection(this%eqName, this%boundedCorr)

        ! Absolute solver tolerance
        call flubioSolvers%json%get(this%eqName//'%absTol', this%absTol, found)
        call raiseKeyErrorJSON(this%eqName//'%absTol', found, 'settings/solvers')

        ! Relative solver tolerance
        call flubioSolvers%json%get(this%eqName//'%relTol', this%relTol, found)
        call raiseKeyErrorJSON(this%eqName//'%relTol', found, 'settings/solvers')

        ! Minimum and maximum number of iterations
        call flubioSolvers%json%get(this%eqName//'%minIter', this%minIter, found)
        if(.not. found) this%minIter = 0

        call flubioSolvers%json%get(this%eqName//'%maxIter', this%maxIter, found)
        if(.not. found) this%maxIter = 1000

        !-----------------------!
        ! Steady state controls !
        !-----------------------!

        if(steadySim==1) then

            key = this%eqName//'%urf-'
            call flubioSteadyStateControls%json%get(this%eqName//'%urf', this%urf, found)
            call raiseKeyErrorJSON(this%eqName//'%urf', found, 'settings/steadyStateControls')

            call flubioSteadyStateControls%json%get(this%eqName//'%residual', anumber, found)
            call raiseKeyErrorJSON(this%eqName//'%residual', found, 'settings/steadyStateControls')
            this%resmax(:) = anumber

           ! Adaptive urf
            call flubioSteadyStateControls%json%get(this%eqName//'%adaptiveRelaxation', relax, found)
            if(.not. found) relax='no'
            if(lowercase(relax)=='yes' .or. lowercase(relax)=='on') then
                this%adaptiveRelaxation = 1

                if(this%convImp==1) call flubioStopMsg('ERROR: Adaptive relaxation cannot be use with fully implicit convection. Plase switch to defferred correction mode.')

                ! Be carefull, avoid multiple allocation
                if(.not. allocated(this%aCD)) allocate(this%aCD(numberOfElements, this%nComp))
                if(.not. allocated(this%aCC)) allocate(this%aCC(numberOfElements, this%nComp))
                if(.not. allocated(this%aCC0)) allocate(this%aCC0(numberOfElements, this%nComp))

                this%aCD = 0.0
                this%aCC = 0.0
                this%aCC0 = 0.0

            else
                this%adaptiveRelaxation = 0
            endif

        end if

    end subroutine setEqOptions

! *********************************************************************************************************************

    subroutine setEqOptionsWithoutConvection(this, kspName)

    !==================================================================================================================
    ! Description:
    !! setEqOptions sets the solver and discretization options for the target transport equation by parsing the settings from
    !! the relevant dictionaries.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: kspName
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName, key, solverType, pcType, relax
    !------------------------------------------------------------------------------------------------------------------

        integer :: intValue
    !------------------------------------------------------------------------------------------------------------------

        real :: anumber
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        !----------------!
        ! Solver options !
        !----------------!

        call flubioSolvers%json%get(this%eqName//'%solverType', solverType, found)
        if(found) then
            this%solverType = solverType
        else
            this%solverType = 'petsc'
        endif

        ! Add additional options as command line, if any
        call this%setSolverOptions()
        if(this%solverType=='amgcl') call this%getAMGCLOptions()

        call flubioSolvers%json%get(this%eqName//'%solver', solverType, found)
        call raiseKeyErrorJSON(this%eqName//'%solver', found, 'settings/solvers')
        this%solverName = solverType

        call flubioSolvers%json%get(this%eqName//'%preconditioner', pcType, found)
        call raiseKeyErrorJSON(this%eqName//'%preconditioner', found, 'settings/solvers')
        this%pcName = pcType

        call flubioSolvers%setPreconditionerOption(lowercase(this%pcName), this%pc, this%solverType)

        !--------------------!
        ! Numerical settings !
        !--------------------!

        call flubioOptions%setTimeSchemeOption(this%eqName, this%timeOpt, this%timeOptName)

        ! Absolute solver tolerance
        call flubioSolvers%json%get(this%eqName//'%absTol', this%absTol, found)
        call raiseKeyErrorJSON(this%eqName//'%absTol', found, 'settings/solvers')

        ! Relative solver tolerance
        call flubioSolvers%json%get(this%eqName//'%relTol', this%relTol, found)
        call raiseKeyErrorJSON(this%eqName//'%relTol', found, 'settings/solvers')

        ! Minimum and maximum number of iterations
        call flubioSolvers%json%get(this%eqName//'%minIter', this%minIter, found)
        if(.not. found) this%minIter = 0

        call flubioSolvers%json%get(this%eqName//'%maxIter', this%maxIter, found)
        if(.not. found) this%maxIter = 1000

        !-----------------------!
        ! Steady state controls !
        !-----------------------!

        if(steadySim==1) then
            key = this%eqName//'%urf-'
            call flubioSteadyStateControls%json%get(this%eqName//'%urf', this%urf, found)
            call raiseKeyErrorJSON(this%eqName//'%urf', found, 'settings/steadyStateControls')

            call flubioSteadyStateControls%json%get(this%eqName//'%residual', anumber, found)
            call raiseKeyErrorJSON(this%eqName//'%residual', found, 'settings/steadyStateControls')
            this%resmax(:) = anumber
        end if

    end subroutine setEqOptionsWithoutConvection

! *********************************************************************************************************************

    subroutine setNumericalOptions(this)

    !==================================================================================================================
    ! Description:
    !! setNumericalOptions sets the numrical option only for the transport equation.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: key, convOpt
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        call flubioOptions%setTimeSchemeOption(this%eqName, this%timeOpt, this%timeOptName)

        call flubioOptions%json%get(this%eqName//'%convectiveScheme', convOpt, found)
        if(.not. found) convOpt = 'none'
        this%convOptName = lowercase(convOpt)

        call flubioOptions%setConvectionOption(this%convOptName, this%convOpt)

        ! Check or assign a default
        if(this%convOpt==-1 .and. flubioOptions%convOpt==-1) then
            call printConvectiveSchemes(this%eqname)
        elseif(this%convOpt==-1 .and. flubioOptions%convOpt/=-1) then
            this%convOpt = flubioOptions%convOpt
        end if

        call flubioOptions%setConvectionImplicit(this%eqName, this%convImp)
        call flubioOptions%setBoundedCorrection(this%eqName, this%boundedCorr)

    end subroutine setNumericalOptions

! *********************************************************************************************************************

    subroutine setUnderRelaxationFactor(this)

    !==================================================================================================================
    ! Description:
    !! setUnderRelaxationFactor stes the implicit under-relaxation factor for the transport equation.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: key, opt
    !------------------------------------------------------------------------------------------------------------------

        real :: anumber
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        if(steadySim==1) then
            key = this%eqName//'%urf'
            call flubioSteadyStateControls%json%get(key, this%urf, found)
            call raiseKeyErrorJSON(key, found, 'settings/steadyStateControls')
        end if

    end subroutine setUnderRelaxationFactor

!------------------------------------------------------------------------------------------------------------------
!                                           linear system assembly methods
!------------------------------------------------------------------------------------------------------------------

    subroutine addConvectiveTermDC(this, field)

    !==================================================================================================================
    ! Description:
    !! addConvectiveTerm adds the convective term to the equation's coeffiecients and rhs.
    !! The assembling procedure is carried out looping on the mesh faces and taking advantage of the owner-neighbour definition.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer  :: iElement, iBoundary, iFace, iBFace, iProc, iComp, is, ie

        integer :: iOwner, iNeighbour,  iOwnerNeighbourCoef, iNeighbourOwnerCoef
    !------------------------------------------------------------------------------------------------------------------

        real :: fluxC1f(numberOfFaces, this%nComp), fluxC2f(numberOfFaces, this%nComp), fluxVf(numberOfFaces, this%nComp)

        real :: ones(this%nComp)

        real :: corr
    !------------------------------------------------------------------------------------------------------------------

        ones = 1.0

        ! Compute convective fluxes
        call assembleConvectiveTerm(field, this%convOpt, fluxC1f, fluxC2f, fluxVf)
        if(this%adaptiveRelaxation==1) then 
            this%aCC0 = this%aCC
            this%aCC = 0.0
        endif 

        !------------------------------------!
        !  Assemble fluxes of interior faces !
        !------------------------------------!

        do iFace = 1,numberOfIntFaces
            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            iOwnerNeighbourCoef = mesh%iOwnerNeighbourCoef(iFace)
            iNeighbourOwnerCoef = mesh%iNeighbourOwnerCoef(iFace)

            ! Assemble fluxes for owner cell
            this%aC(iOwner,:) = this%aC(iOwner,:) + fluxC1f(iFace,:)
            this%bC(iOwner,:) = this%bC(iOwner,:) - fluxVf(iFace,:)
            call this%anb(iOwner)%add(iOwnerNeighbourCoef, fluxC2f(iFace,:))

            ! Assemble fluxes for neighbour cell
            this%aC(iNeighbour,:) = this%aC(iNeighbour,:) - fluxC2f(iFace,:)
            this%bC(iNeighbour,:) = this%bC(iNeighbour,:) + fluxVf(iFace,:)
            call this%anb(iNeighbour)%subtract(iNeighbourOwnerCoef, fluxC1f(iFace,:))

            if(this%adaptiveRelaxation==1) then
                this%aCC(iOwner, :) = this%aCC(iOwner, :) + fluxC1f(iFace, :)
                this%aCC(iNeighbour, :) = this%aCC(iNeighbour, :) - fluxC2f(iFace, :)
            endif

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace=is,ie

                iOwner = mesh%owner(iBFace)
                iOwnerNeighbourCoef = mesh%iOwnerNeighbourCoef(iBFace)

                ! Assemble fluxes for owner cell
                this%aC(iOwner,:) = this%aC(iOwner,:) + fluxC1f(iBFace,:)
                this%bC(iOwner,:) = this%bC(iOwner,:) - fluxVf(iBFace,:)

                call this%anb(iOwner)%subtract(iOwnerNeighbourCoef, fluxC2f(iBFace, :))

                if(this%adaptiveRelaxation==1) this%aCC(iOwner, :) = this%aCC(iOwner, :) + fluxC1f(iBFace, :)

            enddo

        enddo

        ! Bounded correction and deviatoric term
        if(this%boundedCorr==1) call this%addBoundedTerm()

        ! if(this%devTerm==1) call addDevTerm()

    end subroutine addConvectiveTermDC

! *********************************************************************************************************************

    subroutine addConvectiveTermIC(this, field)

    !==================================================================================================================
    ! Description:
    !! addConvectiveTermImplicit adds the convective part to the equation's coefficients.
    !! The assembling procedure is carried out looping on the mesh faces and taking advantage of the owner-neighbour definition.
    !! This routine is the same as assembleConvectionMomentumEq, but it does not include the rhs,
    !! since the deferred correction is not used (i.e. the whole HR term is treated implicitly).
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iBoundary, iFace, iBFace, iProc, iComp

        integer :: iOwner, iNeighbour,  iOwnerNeighbourCoef, iNeighbourOwnerCoef

        integer :: is, ie
    !------------------------------------------------------------------------------------------------------------------

        real :: fluxC1f(numberOfFaces, this%nComp), fluxC2f(numberOfFaces, this%nComp), fluxVf(numberOfFaces, this%nComp)

        real :: corr
        !! correction
    !------------------------------------------------------------------------------------------------------------------

        ! Compute convective fluxes
        call assembleConvectiveTermImplicit(field, this%convOpt, fluxC1f, fluxC2f, fluxVf)

        !  Assemble fluxes of interior faces
        do iFace = 1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            iOwnerNeighbourCoef=mesh%iOwnerNeighbourCoef(iFace)
            iNeighbourOwnerCoef=mesh%iNeighbourOwnerCoef(iFace)

            ! Assemble fluxes for owner cell
            this%aC(iOwner,:) = this%aC(iOwner,:) + fluxC1f(iFace,:)
            call this%anb(iOwner)%add(iOwnerNeighbourCoef, fluxC2f(iFace,:))

            ! Assemble fluxes for neighbour cell
            this%aC(iNeighbour,:) = this%aC(iNeighbour,:) - fluxC2f(iFace,:)
            call this%anb(iNeighbour)%subtract(iNeighbourOwnerCoef, fluxC1f(iFace,:))

        enddo

        ! Processor Boundaries
        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace=is,ie

                iOwner = mesh%owner(iBFace)
                iOwnerNeighbourCoef=mesh%iOwnerNeighbourCoef(iBFace)

                !   Assemble fluxes for owner cell
                this%aC(iOwner,:) = this%aC(iOwner,:) + fluxC1f(iBFace,:)
                call this%anb(iOwner)%add(iOwnerNeighbourCoef, fluxC2f(iBFace,:))
                !this%anb(iOwner,iOwnerNeighbourCoef,:) = this%anb(iOwner,iOwnerNeighbourCoef,:) + fluxC2f(iBFace,:)

            enddo

        enddo

        ! Bounded correction
        if(this%boundedCorr==1) call this%addBoundedTerm()

!        if(this%devTerm==1) call addDevTerm

    end subroutine addConvectiveTermIC

!*********************************************************************************************************************

    subroutine addConvectiveTermWithCoeff(this, field, coeff)

    !==================================================================================================================
    ! Description:
    !! addConvectiveTermWithCoeff adds the convective term to the equation's coeffiecients and rhs.
    !! The assembling procedure is carried out looping on the mesh faces and taking advantage of the owner-neighbour definition.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field

        type(flubioField) :: coeff
    !------------------------------------------------------------------------------------------------------------------

        integer  :: iElement, iBoundary, iFace, iBFace, iProc, iComp, is, ie

        integer :: iOwner, iNeighbour,  iOwnerNeighbourCoef, iNeighbourOwnerCoef
    !------------------------------------------------------------------------------------------------------------------

        real :: fluxC1f(numberOfFaces, this%nComp), fluxC2f(numberOfFaces, this%nComp), fluxVf(numberOfFaces, this%nComp)

        real :: ones(this%nComp)

        real :: corr
    !------------------------------------------------------------------------------------------------------------------

        ones = 1.0
        ! Compute convective fluxes
        call assembleConvectiveTerm(field, coeff, this%convOpt, fluxC1f, fluxC2f, fluxVf)
        if(this%adaptiveRelaxation==1) then 
            this%aCC0 = this%aCC
            this%aCC = 0.0
        endif

        !------------------------------------!
        !  Assemble fluxes of interior faces !
        !------------------------------------!

        do iFace = 1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            iOwnerNeighbourCoef = mesh%iOwnerNeighbourCoef(iFace)
            iNeighbourOwnerCoef = mesh%iNeighbourOwnerCoef(iFace)

            ! Assemble fluxes for owner cell
            this%aC(iOwner,:) = this%aC(iOwner,:) + fluxC1f(iFace,:)
            this%bC(iOwner,:) = this%bC(iOwner,:) - fluxVf(iFace,:)
            call this%anb(iOwner)%add(iOwnerNeighbourCoef, fluxC2f(iFace,:))

            ! Assemble fluxes for neighbour cell
            this%aC(iNeighbour,:) = this%aC(iNeighbour,:) - fluxC2f(iFace,:)
            this%bC(iNeighbour,:) = this%bC(iNeighbour,:) + fluxVf(iFace,:)
            call this%anb(iNeighbour)%subtract(iNeighbourOwnerCoef, fluxC1f(iFace,:))

            if(this%adaptiveRelaxation==1) then
                this%aCC(iOwner, :) = this%aCC(iOwner, :) + fluxC1f(iFace, :)
                this%aCC(iNeighbour, :) = this%aCC(iNeighbour, :) - fluxC2f(iFace, :)
            endif

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace=is,ie

                iOwner = mesh%owner(iBFace)
                iOwnerNeighbourCoef = mesh%iOwnerNeighbourCoef(iBFace)

                ! Assemble fluxes for owner cell
                this%aC(iOwner,:) = this%aC(iOwner,:) + fluxC1f(iBFace,:)
                this%bC(iOwner,:) = this%bC(iOwner,:) - fluxVf(iBFace,:)

                call this%anb(iOwner)%subtract(iOwnerNeighbourCoef, fluxC2f(iBFace,:))

                if(this%adaptiveRelaxation==1) this%aCC(iOwner, :) = this%aCC(iOwner, :) + fluxC1f(iBFace, :)

            enddo

        enddo

        ! Bounded correction and deviatoric term
        if(this%boundedCorr==1) call this%addBoundedTerm()

        ! if(this%devTerm==1) call addDevTerm()

    end subroutine addConvectiveTermWithCoeff

! *********************************************************************************************************************

    subroutine addConvectiveTermImplicitWithCoeff(this, field, coeff)

    !==================================================================================================================
    ! Description:
    !! addConvectiveTermImplicitWithCoeff adds the convective part to the equation's coefficients.
    !! The assembling procedure is carried out looping on the mesh faces and taking advantage of the owner-neighbour definition.
    !! This routine is the same as assembleConvectionMomentumEq, but it does not include the rhs,
    !! since the deferred correction is not used (i.e. the whole HR term is treated implicitly).
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field

        type(flubioField) :: coeff
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iBoundary, iFace, iBFace, iProc, iComp

        integer :: iOwner, iNeighbour,  iOwnerNeighbourCoef, iNeighbourOwnerCoef

        integer :: is, ie
    !------------------------------------------------------------------------------------------------------------------

        real :: fluxC1f(numberOfFaces, this%nComp), fluxC2f(numberOfFaces, this%nComp), fluxVf(numberOfFaces, this%nComp)

        real :: corr
        !! correction
    !------------------------------------------------------------------------------------------------------------------

        ! Assemble convective fluxes
        call assembleConvectiveTermImplicit(field, coeff, this%convOpt, fluxC1f, fluxC2f, fluxVf)

        !  Assemble fluxes of interior faces
        do iFace = 1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            iOwnerNeighbourCoef=mesh%iOwnerNeighbourCoef(iFace)
            iNeighbourOwnerCoef=mesh%iNeighbourOwnerCoef(iFace)

            ! Assemble fluxes for owner cell
            this%aC(iOwner,:) = this%aC(iOwner,:) + fluxC1f(iFace,:)
            call this%anb(iOwner)%add(iOwnerNeighbourCoef, fluxC2f(iFace,:))

            ! Assemble fluxes for neighbour cell
            this%aC(iNeighbour,:) = this%aC(iNeighbour,:) - fluxC2f(iFace,:)
            call this%anb(iNeighbour)%subtract(iNeighbourOwnerCoef, fluxC1f(iFace,:))

        enddo

        ! Processor Boundaries
        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace=is,ie

                iOwner = mesh%owner(iBFace)
                iOwnerNeighbourCoef=mesh%iOwnerNeighbourCoef(iBFace)

                !  Assemble fluxes for owner cell
                this%aC(iOwner,:) = this%aC(iOwner,:) + fluxC1f(iBFace,:)
                call this%anb(iOwner)%add(iOwnerNeighbourCoef, fluxC2f(iBFace,:))

            enddo

        enddo

        ! Bounded correction
        if(this%boundedCorr==1) call this%addBoundedTerm()

        !if(this%devTerm==1) call addDevTerm

    end subroutine addConvectiveTermImplicitWithCoeff

! *********************************************************************************************************************

    subroutine addDiffusionTermWithFieldDiffusivity(this, field, diffCoeff, boundaryNonOrthCorr)

    !==================================================================================================================
    ! Description:
    !! addDiffusionTermWithFieldDiffusivity adds the diffusion term in the equation's coefficients and right hand side.
    !! The assembling procedure is carried out looping on mesh faces and taking advantage of the owner-neighbour definition.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field

        type(flubioField) :: diffCoeff
    !------------------------------------------------------------------------------------------------------------------

        logical :: applyBoundaryNonOrthCorr

        logical, optional :: boundaryNonOrthCorr
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iFace, iBFace, iBoundary, iProc, iComp, nComp

        integer :: iOwner, iNeighbour,  iOwnerNeighbourCoef, iNeighbourOwnerCoef

        integer :: is, ie
    !------------------------------------------------------------------------------------------------------------------

        real :: fluxC1f(numberOfFaces, field%nComp), fluxC2f(numberOfFaces, field%nComp)

        real :: fluxVf(numberOfFaces, field%nComp)
    !------------------------------------------------------------------------------------------------------------------

        call assembleDiffusionFluxes(field, diffCoeff, fluxC1f, fluxC2f, fluxVf)
        nComp = size(this%aC(1,:))

        if(this%adaptiveRelaxation==1) this%aCD = 0.0

        if(present(boundaryNonOrthCorr)) then
            applyBoundaryNonOrthCorr = boundaryNonOrthCorr
        else
            applyBoundaryNonOrthCorr = .false.
        endif

        ! Assemble fluxes of interior faces
        do iFace = 1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            iOwnerNeighbourCoef = mesh%iOwnerNeighbourCoef(iFace)
            iNeighbourOwnerCoef = mesh%iNeighbourOwnerCoef(iFace)

            ! Assemble fluxes for owner cell
            this%aC(iOwner, :) = this%aC(iOwner, :) + fluxC1f(iFace, :)
            call this%anb(iOwner)%add(iOwnerNeighbourCoef, fluxC2f(iFace, :))

            this%bC(iOwner,:) = this%bC(iOwner,:) - fluxVf(iFace,:)

            ! Assemble fluxes for neighbour cell
            this%aC(iNeighbour, :) = this%aC(iNeighbour, :) - fluxC2f(iFace, :)
            call this%anb(iNeighbour)%subtract(iNeighbourOwnerCoef, fluxC1f(iFace, :))

            this%bC(iNeighbour,:) = this%bC(iNeighbour,:) + fluxVf(iFace,:)

            if(this%adaptiveRelaxation==1) then
                this%aCD(iOwner, :) = this%aCD(iOwner, :) + fluxC1f(iFace, :)
                this%aCD(iNeighbour, :) = this%aCD(iNeighbour, :) - fluxC2f(iFace, :)
            endif

        enddo

        ! Processor Boundaries
        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)

            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace=is,ie

                iOwner = mesh%owner(iBFace)
                iOwnerNeighbourCoef = mesh%iOwnerNeighbourCoef(iBFace)

                ! Assemble fluxes for owner cell
                this%aC(iOwner, :) = this%aC(iOwner, :) + fluxC1f(iBFace, :)
                call this%anb(iOwner)%add(iOwnerNeighbourCoef, fluxC2f(iBFace, :))

                this%bC(iOwner,:) = this%bC(iOwner,:) - fluxVf(iBFace,:)

                if(this%adaptiveRelaxation==1) this%aCD(iOwner, :) = this%aCD(iOwner, :) + fluxC1f(iBFace, :)

            enddo

        enddo

        ! Add non orthogonal correction at boundaries
        if(applyBoundaryNonOrthCorr) then

            ! Real boundaries
            do iBoundary=1,numberOfRealBound

                is = mesh%boundaries%startFace(iBoundary)
                ie = is+mesh%boundaries%nFace(iBoundary)-1

                ! Add cross term for dirichlet-like bc only
                if(trim(field%bCond(iBoundary)) /= "neumann0" .and. trim(field%bCond(iBoundary)) /= "neumann" &
                                                              .and. trim(field%bCond(iBoundary)) /= "void") then
                    do iBFace=is,ie
                        iOwner = mesh%owner(iBFace)
                        this%bC(iOwner,:) = this%bC(iOwner,:) - fluxVf(iBFace,:)
                    enddo
                endif

            enddo

        endif

    end subroutine addDiffusionTermWithFieldDiffusivity

! *********************************************************************************************************************

    subroutine addDiffusionTermWithConstantDiffusivity(this, field, diffCoeff, boundaryNonOrthCorr)

    !==================================================================================================================
    ! Description:
    !! addDiffusionTermWithConstantDiffusivity adds the diffusion term in the equation's coefficients and right hand side.
    !! The assembling procedure is carried out looping on mesh faces and taking advantage of the owner-neighbour definition.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        logical :: applyBoundaryNonOrthCorr

        logical, optional :: boundaryNonOrthCorr
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iFace, iBFace, iBoundary, iProc, iComp, nComp

        integer :: iOwner, iNeighbour,  iOwnerNeighbourCoef, iNeighbourOwnerCoef

        integer :: is, ie
    !------------------------------------------------------------------------------------------------------------------

        real :: diffCoeff

        real :: fluxC1f(numberOfFaces, field%nComp), fluxC2f(numberOfFaces, field%nComp)

        real :: fluxVf(numberOfFaces, field%nComp)
    !------------------------------------------------------------------------------------------------------------------

        call assembleDiffusionFluxes(field, diffCoeff, fluxC1f, fluxC2f, fluxVf)

        nComp = size(this%aC(1,:))
        if(this%adaptiveRelaxation==1) this%aCD = 0.0

        if(present(boundaryNonOrthCorr)) then
            applyBoundaryNonOrthCorr = boundaryNonOrthCorr
        else
            applyBoundaryNonOrthCorr = .false.
        endif

        ! Assemble fluxes of interior faces
        do iFace = 1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            iOwnerNeighbourCoef = mesh%iOwnerNeighbourCoef(iFace)
            iNeighbourOwnerCoef = mesh%iNeighbourOwnerCoef(iFace)

            ! Assemble fluxes for owner cell
            this%aC(iOwner, :) = this%aC(iOwner, :) + fluxC1f(iFace, :)
            call this%anb(iOwner)%add(iOwnerNeighbourCoef, fluxC2f(iFace, :))

            this%bC(iOwner,:) = this%bC(iOwner,:) - fluxVf(iFace,:)

            ! Assemble fluxes for neighbour cell
            this%aC(iNeighbour, :) = this%aC(iNeighbour, :) - fluxC2f(iFace, :)
            call this%anb(iNeighbour)%subtract(iNeighbourOwnerCoef, fluxC1f(iFace, :))

            this%bC(iNeighbour,:) = this%bC(iNeighbour,:) + fluxVf(iFace,:)

            if(this%adaptiveRelaxation==1) then
                this%aCD(iOwner, :) = this%aCD(iOwner, :) + fluxC1f(iFace, :)
                this%aCD(iNeighbour, :) = this%aCD(iNeighbour, :) - fluxC2f(iFace, :)
            endif

        enddo

        ! Processor Boundaries
        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)

            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace=is,ie

                iOwner = mesh%owner(iBFace)
                iOwnerNeighbourCoef = mesh%iOwnerNeighbourCoef(iBFace)

                ! Assemble fluxes for owner cell
                this%aC(iOwner, :) = this%aC(iOwner, :) + fluxC1f(iBFace, :)
                call this%anb(iOwner)%add(iOwnerNeighbourCoef, fluxC2f(iBFace, :))

                this%bC(iOwner,:) = this%bC(iOwner,:) - fluxVf(iBFace,:)

                if(this%adaptiveRelaxation==1) this%aCD(iOwner, :) = this%aCD(iOwner, :) + fluxC1f(iBFace, :)

            enddo

        enddo

        ! Add non orthogonal correction at boundaries
        if(applyBoundaryNonOrthCorr) then

            ! Real boundaries
            do iBoundary=1,numberOfRealBound

                is = mesh%boundaries%startFace(iBoundary)
                ie = is+mesh%boundaries%nFace(iBoundary)-1

                ! Add cross term for dirichlet-like bc only
                if(trim(field%bCond(iBoundary)) /= "neumann0" .and. trim(field%bCond(iBoundary)) /= "neumann" &
                                                              .and. trim(field%bCond(iBoundary)) /= "void") then
                    do iBFace=is,ie
                        iOwner = mesh%owner(iBFace)
                        this%bC(iOwner,:) = this%bC(iOwner,:) - fluxVf(iBFace,:)
                    enddo
                endif

            enddo

        endif

    end subroutine addDiffusionTermWithConstantDiffusivity

! *********************************************************************************************************************

    subroutine addDiffusionTermWithoutCrossDiffusionWithConstantDiffusivity(this, diffCoeff)

    !==================================================================================================================
    ! Description:
    !! addDiffusionTermWithoutCrossDiffusion adds the diffusion term in the equation's coefficients and right hand side.
    !! The assembling procedure is carried out looping on mesh faces and taking advantage of the owner-neighbour definition.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iFace, iBFace, iBoundary, iProc, iComp, nComp

        integer :: iOwner, iNeighbour,  iOwnerNeighbourCoef, iNeighbourOwnerCoef

        integer :: is, ie
    !------------------------------------------------------------------------------------------------------------------

        real  :: diffCoeff

        real :: fluxC1f(numberOfFaces, this%nComp), fluxC2f(numberOfFaces, this%nComp)
    !------------------------------------------------------------------------------------------------------------------

        call assembleDiffusionFluxes(diffCoeff, this%nComp, fluxC1f, fluxC2f)

        nComp = size(this%aC(1,:))

        if(this%adaptiveRelaxation==1) this%aCD = 0.0

        ! Assemble fluxes of interior faces
        do iFace = 1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            iOwnerNeighbourCoef = mesh%iOwnerNeighbourCoef(iFace)
            iNeighbourOwnerCoef = mesh%iNeighbourOwnerCoef(iFace)

            ! Assemble fluxes for owner cell
            this%aC(iOwner, :) = this%aC(iOwner, :) + fluxC1f(iFace, :)
            call this%anb(iOwner)%add(iOwnerNeighbourCoef, fluxC2f(iFace, :))

            ! Assemble fluxes for neighbour cell
            this%aC(iNeighbour, :) = this%aC(iNeighbour, :) - fluxC2f(iFace, :)
            call this%anb(iNeighbour)%subtract(iNeighbourOwnerCoef, fluxC1f(iFace, :))

            if(this%adaptiveRelaxation==1) then
                this%aCD(iOwner, :) = this%aCD(iOwner, :) + fluxC1f(iFace, :)
                this%aCD(iNeighbour, :) = this%aCD(iNeighbour, :) - fluxC2f(iFace, :)
            endif

        enddo

        ! Processor Boundaries
        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)

            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace=is,ie

                iOwner = mesh%owner(iBFace)
                iOwnerNeighbourCoef = mesh%iOwnerNeighbourCoef(iBFace)

                ! Assemble fluxes for owner cell
                this%aC(iOwner, :) = this%aC(iOwner, :) + fluxC1f(iBFace, :)
                call this%anb(iOwner)%add(iOwnerNeighbourCoef, fluxC2f(iBFace, :))

                if(this%adaptiveRelaxation==1) this%aCD(iOwner, :) = this%aCD(iOwner, :) + fluxC1f(iBFace, :)

            enddo

        enddo

    end subroutine addDiffusionTermWithoutCrossDiffusionWithConstantDiffusivity

! *********************************************************************************************************************

    subroutine addDiffusionTermWithoutCrossDiffusionWithFieldDiffusivity(this, diffCoeff)

    !==================================================================================================================
    ! Description:
    !! addDiffusionTermWithoutCrossDiffusion adds the diffusion term in the equation's coefficients and right hand side.
    !! The assembling procedure is carried out looping on mesh faces and taking advantage of the owner-neighbour definition.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField)  :: diffCoeff
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iFace, iBFace, iBoundary, iProc, iComp, nComp

        integer :: iOwner, iNeighbour,  iOwnerNeighbourCoef, iNeighbourOwnerCoef

        integer :: is, ie
    !------------------------------------------------------------------------------------------------------------------

        real :: fluxC1f(numberOfFaces, this%nComp), fluxC2f(numberOfFaces, this%nComp)
    !------------------------------------------------------------------------------------------------------------------

        call assembleDiffusionFluxes(diffCoeff, this%nComp, fluxC1f, fluxC2f)

        nComp = size(this%aC(1,:))

        if(this%adaptiveRelaxation==1) this%aCD = 0.0

        ! Assemble fluxes of interior faces
        do iFace = 1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            iOwnerNeighbourCoef = mesh%iOwnerNeighbourCoef(iFace)
            iNeighbourOwnerCoef = mesh%iNeighbourOwnerCoef(iFace)

            ! Assemble fluxes for owner cell
            this%aC(iOwner, :) = this%aC(iOwner, :) + fluxC1f(iFace, :)
            call this%anb(iOwner)%add(iOwnerNeighbourCoef, fluxC2f(iFace, :))

            ! Assemble fluxes for neighbour cell
            this%aC(iNeighbour, :) = this%aC(iNeighbour, :) - fluxC2f(iFace, :)
            call this%anb(iNeighbour)%subtract(iNeighbourOwnerCoef, fluxC1f(iFace, :))

            if(this%adaptiveRelaxation==1) then
                this%aCD(iOwner, :) = this%aCD(iOwner, :) + fluxC1f(iFace, :)
                this%aCD(iNeighbour, :) = this%aCD(iNeighbour, :) - fluxC2f(iFace, :)
            endif

        enddo

        ! Processor Boundaries
        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)

            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace=is,ie

                iOwner = mesh%owner(iBFace)
                iOwnerNeighbourCoef = mesh%iOwnerNeighbourCoef(iBFace)

                ! Assemble fluxes for owner cell
                this%aC(iOwner, :) = this%aC(iOwner, :) + fluxC1f(iBFace, :)
                call this%anb(iOwner)%add(iOwnerNeighbourCoef, fluxC2f(iBFace, :))

                if(this%adaptiveRelaxation==1) this%aCD(iOwner, :) = this%aCD(iOwner, :) + fluxC1f(iBFace, :)

            enddo

        enddo

    end subroutine addDiffusionTermWithoutCrossDiffusionWithFieldDiffusivity

! *********************************************************************************************************************

    subroutine addTransientTermStd(this, field, ddtCoef)

    !==================================================================================================================
    ! Description:
    !! addTransientTerm adds the transient term to the equantion's diagonal coefficient and rhs.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field, ddtCoef
    !------------------------------------------------------------------------------------------------------------------

        integer :: fComp, timeOpt
    !------------------------------------------------------------------------------------------------------------------

        timeOpt = this%timeOpt
        fComp = this%nComp

        if(timeOpt==0) then

            call Euler(field%phio, ddtCoef%phio(1:numberOfElements,1), fComp, this%aC, this%bC)

        elseif(timeOpt==1) then

            if(itime==1) then
                call Euler(field%phio, ddtCoef%phio, fComp, this%aC, this%bC)
            else
                call AdamsMoulton(field%phio, field%phioo, ddtCoef%phi(1:numberOfElements,1), &
                                  ddtCoef%phio(1:numberOfElements,1), ddtCoef%phioo(1:numberOfElements,1), &
                                  fComp, this%aC, this%bC)
            endif

        elseif(timeOpt==2) then

            if(itime==1) then
                call Euler(field%phio, ddtCoef%phi(1:numberOfElements,1), fComp, this%aC, this%bC)
            else
                call crankNicholson(field%phio, field%phioo, ddtCoef%phi(1:numberOfElements,1), &
                                    ddtCoef%phio(1:numberOfElements,1), ddtCoef%phioo(1:numberOfElements,1), &
                                    fComp, this%aC, this%bC)
            endif

        elseif(timeOpt==-1) then

            ! Steady state, do nothing

        else

            call flubioStopMsg('ERROR: Unknown time scheme ')

        endif

    end subroutine addTransientTermStd

! *********************************************************************************************************************

    subroutine addTransientTermWithCoeff(this, field, ddtCoef, coef)

    !==================================================================================================================
    ! Description:
    !! addTransientTermWithCoeff adds the transient term to the equantion's diagonal coefficient and rhs.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field, ddtCoef, coef
    !------------------------------------------------------------------------------------------------------------------

        integer :: fComp, timeOpt
    !------------------------------------------------------------------------------------------------------------------

        real :: cEff(numberOfElements), cEff0(numberOfElements), cEff00(numberOfElements)
    !------------------------------------------------------------------------------------------------------------------

        cEff = ddtCoef%phi(:,1)*coef%phi(:,1)
        cEff0 = ddtCoef%phio(:,1)*coef%phio(:,1)
        cEff00 = ddtCoef%phioo(:,1)*coef%phioo(:,1)

        timeOpt = this%timeOpt
        fComp = this%nComp

        if(timeOpt==0) then

            call Euler(field%phio, cEff0, fComp, this%aC, this%bC)

        elseif(timeOpt==1) then

            if(itime==1) then
                call Euler(field%phio, cEff0, fComp, this%aC, this%bC)
            else
                call AdamsMoulton(field%phio, field%phioo,&
                                  cEff, cEff0, cEff00, &
                                  fComp, this%aC, this%bC)
            endif

        elseif(timeOpt==2) then

            if(itime==1) then
                call Euler(field%phio, cEff0, fComp, this%aC, this%bC)
            else
                call crankNicholson(field%phio, field%phioo, &
                        cEff, cEff0, cEff00, &
                        fComp, this%aC, this%bC)
            endif

        elseif(timeOpt==-1) then
            ! Steady state, do nothing
        else
            call flubioStopMsg('ERROR: Unknown time scheme ')
        endif

    end subroutine addTransientTermWithCoeff

! *********************************************************************************************************************

    subroutine relaxEq(this, field)

    !==================================================================================================================
    ! Description:
    !! relaxEq relaxes implixitly the target equation:
    !! \begin{eqnarray}
    !!  a_C = \dfrac{a_C}{urf} \\
    !!  b_C = b_C + \dfrac{1-urf}{urf}a_C\phi
    !! \end{eqnarray}
    !! A diagonal dominace check is done in order to improve the stability of the solution as well.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement

        integer :: iComp
    !------------------------------------------------------------------------------------------------------------------

        real :: relaxFactor
        !! cell relaxation factor

        real :: urf(numberOfElements)
        !! adaptive urf

        real :: aC0(numberOfElements, field%nComp)
        !! Diagonal coefficeints before performing the diagonal dominance check

        real :: sumAnb(numberOfElements,field%nComp)
        ! Sum of the magnitude of off-diagonal coefficients
    !------------------------------------------------------------------------------------------------------------------

        aC0 = this%aC
        sumAnb = this%sumMagAnb()

        ! Compute new urf
        if(this%adaptiveRelaxation==1) urf = this%updateURF(aC0, field%nComp)

        ! Modify the central coefficients and rhs according to the implicit under-relaxation method
        do iComp=1,field%nComp

            ! Ensure diagonal dominance
            do iElement=1,numberOfElements

                this%aC(iElement,iComp) = max(sumAnb(iElement, iComp), abs(this%aC(iElement,iComp)))

                if(this%adaptiveRelaxation==1) then
                    relaxFactor = urf(iElement)
                else
                    relaxFactor = this%urf
                endif

                ! Relax aC
                this%aC(iElement,iComp) = this%aC(iElement,iComp)/relaxFactor
                this%bC(iElement,iComp) = this%bC(iElement,iComp) + (this%aC(iElement,iComp) - aC0(iElement,iComp))*field%phi(iElement,iComp)

            enddo

        enddo

    end subroutine relaxEq

! *********************************************************************************************************************

    function updateURF(this, aC0, nComp) result(urf)

    !==================================================================================================================
    ! Description:
    !! updateURF computes the urf to use in the current iteration if the adaptive relaxation is turned on (see Li and Tao 2020)
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: urfField
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: str
        !! auxiliary string

        character(len=:), allocatable ::  final
        !! output string

        character(len=30) :: maxs
        !! maxium value in string

        character(len=30) :: mins
        !! minimum value in string
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iComp, nComp, nrelax, ierr

        integer :: sbuf(nid), rbuf(nid)
    !------------------------------------------------------------------------------------------------------------------

        real :: delta
        !! delta between old and new ACC coeffs

        real :: acc, acc0, acd, ac, acp, tmp, conservativeURF
        !! workig variables

        real :: aC0(numberOfElements, nComp)
        !! old aC

        real :: urf(numberOfElements)
        !! urf

        real, dimension(:), allocatable :: minURF
        !! min urf

        real, dimension(:), allocatable :: maxURF
        !! max urf

    !------------------------------------------------------------------------------------------------------------------

        urf = 0.0
        
        nrelax = 0

        do iElement=1,numberOfElements
            conservativeURF = 1000.0
            do iComp=1,nComp-bdim

                acc = this%aCC(iElement,iComp)
                acc0 = this%aCC0(iElement,iComp)
                acd = this%aCD(iElement,iComp)
                ac = acc+acd

                delta = abs(acc-acc0)/acc

                tmp = (acc + acd)/((1.0+delta)*acc + acd + ac*((1.0-this%urf)/this%urf))

                conservativeURF = max(min(conservativeURF, tmp), 0.5)
            
            enddo
            
            if(conservativeURF<0.0 .and. conservativeURF> 1.0) then
                call flubioStopMsg('Error: relaxation factor cannot be negative or bigger than one.')
            elseif(itime==1) then
                urf(iElement) = this%urf
            else
                urf(iElement) = min(conservativeURF, 0.95)
            endif

            if(conservativeURF==0.5) then
                nrelax = nrelax + 1
            endif 

        enddo
        
        ! Info
        sbuf = 0
        sbuf(id+1) = nrelax
        call mpi_reduce(sbuf, rbuf, nid, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
        nrelax = sum(rbuf)
        write(mins,'(i0)') nrelax
        str = 'Low relaxation cells: ' // trim(adjustl(mins))
        call flubioMsg(str)

        ! Min and max URF
        call minMaxArr(urf, minURF, maxURF, numberOfElements, 1)

        if(id==0 .and. flubioControls%verbosityLevel==1) then

            write(mins,'(ES12.5)') minURF
            write(maxs,'(ES12.5)') maxURF
            str = 'min(urf-'// trim(this%eqName)//'), max(urf-'// trim(this%eqName)//'): '
            final = ' ' // str //'('// adjustl(trim(mins)) // ', ' // trim(adjustl(trim(maxs)))//')'

           call flubioMsg(final)

        endif

    end function updateURF

! *********************************************************************************************************************

    subroutine eliminateEquations(this, listOfCells, prescibedValue, n)

    !==================================================================================================================
    ! Description:
    !! eliminateEquations eliminates a target set of equation by putting aC=1.0, anb=0.0 and bC=prescibedValue.
    !! Target cell must be selected by each processor, this is NOT a global subroutine.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: n
        !! Number of cells to eliminate

        integer :: icell
        !! loop index

        integer :: listOfCells(n)
        !! index of the cells to eliminate
    !------------------------------------------------------------------------------------------------------------------

        real :: prescibedValue(n)
        !! prescribed value to assign
    !------------------------------------------------------------------------------------------------------------------

        do icell=1,n
            this%aC(listOfCells(icell), 1) = 1.0
            this%anb(listOfCells(icell))%coeffs = 0.0
            this%bC(listOfCells(icell),1) = prescibedValue(icell)
        enddo

    end subroutine eliminateEquations

! *********************************************************************************************************************

    subroutine updateRhsWithCrossDiffusion(this, field, diffCoeff, nComp)

    !==================================================================================================================
    ! Description:
    !! updateRhsWithCrossDiffusion computes the cross-diffusion term for the non-orthogonal
    !! correction term and adds it to the equation's rhs.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field, diffCoeff
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBoundary, iBFace, iOwner, iNeighbour, iProc, is, ie, iComp, nComp
    !------------------------------------------------------------------------------------------------------------------

        real :: phiGrad_f(numberOfFaces,3,nComp)
        !! gradient interpolated at mesh faces

        real :: dot
        !! dot product

        real :: dot2
        !! dot product

        real :: limiter
        !! cross diffusion limiter

        real :: nu_f

        real :: fluxVf(numberOfFaces, field%nComp)
    !------------------------------------------------------------------------------------------------------------------

        !---------------------!
        ! Interpolate viscosty !
        !----------------------!

        call interpolate(diffCoeff, phys%interpType, -1)

        !-----------------------!
        ! Interpolate gradients !
        !-----------------------!

        call interpolateGradientCorrected(phiGrad_f, field, opt=0, fComp=nComp)

        !------------------------------------!
        ! Account for cell non-orthogonality !
        !------------------------------------!

        do iComp=1,nComp

            do iFace=1,numberOfFaces

                nu_f = diffCoeff%phif(iFace,1)

                dot = dot_product(phiGrad_f(iFace,:,iComp), mesh%Tf(iFace,:))
                dot2 = dot_product(phiGrad_f(iFace,:,iComp), mesh%Sf(iFace,:))

                if(flubioOptions%lambda==1.0) then
                    limiter = 1.0
                else
                    limiter = limitCrossDiffusion(dot2, dot, flubioOptions%lambda)
                end if

                fluxVf(iFace,iComp) = -nu_f*dot*limiter

            enddo

        enddo

        !------------------------------!
        ! Assemble in the equation rhs !
        !------------------------------!

        do iFace = 1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            ! Assemble fluxes for owner and neighbour cell
            this%bC(iOwner,:) = this%bC(iOwner,:) - fluxVf(iFace,:)
            this%bC(iNeighbour,:) = this%bC(iNeighbour,:) + fluxVf(iFace,:)

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace=is,ie

                iOwner = mesh%owner(iBFace)

                ! Assemble fluxes for owner and neighbour cell
                this%bC(iOwner,:) = this%bC(iOwner,:) - fluxVf(iBFace,:)

            enddo

        enddo

        !-----------------!
        ! Real Boundaries !
        !-----------------!

        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie
                iOwner = mesh%owner(iBFace)
                this%bC(iOwner,:) = this%bC(iOwner,:) - fluxVf(iBFace,:)
            enddo

        enddo

    end subroutine updateRhsWithCrossDiffusion

! *********************************************************************************************************************

    subroutine updateRhsWithCrossDiffusionWithConstantDiffusivity(this, field, nu_f, nComp)

    !==================================================================================================================
    ! Description:
    !! updateRhsWithCrossDiffusion computes the cross-diffusion term for the non-orthogonal
    !! correction term and adds it to the equation's rhs.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field, diffCoeff
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBoundary, iBFace, iOwner, iNeighbour, iProc, is, ie, iComp, nComp
    !------------------------------------------------------------------------------------------------------------------

        real :: phiGrad_f(numberOfFaces,3,nComp)
        !! gradient interpolated at mesh faces

        real :: nu_f
        !! diffusion coefficient

        real :: dot
        !! dot product

        real :: dot2
        !! dot product

        real :: limiter
        !! cross diffusion limiter

        real :: fluxVf(numberOfFaces, field%nComp)
    !------------------------------------------------------------------------------------------------------------------

        !-----------------------!
        ! Interpolate gradients !
        !-----------------------!

        call interpolateGradientCorrected(phiGrad_f, field, opt=1, fComp=nComp)

        !------------------------------------!
        ! Account for cell non-orthogonality !
        !------------------------------------!

        do iComp=1,nComp

            do iFace=1,numberOfFaces

                dot = dot_product(phiGrad_f(iFace,:,iComp), mesh%Tf(iFace,:))
                dot2 = dot_product(phiGrad_f(iFace,:,iComp), mesh%Sf(iFace,:))

                if(flubioOptions%lambda==1.0) then
                    limiter = 1.0
                else
                    limiter = limitCrossDiffusion(dot2, dot, flubioOptions%lambda)
                end if

                fluxVf(iFace,iComp) = -nu_f*dot*limiter

            enddo

        enddo

        !------------------------------!
        ! Assemble in the equation rhs !
        !------------------------------!

        do iFace = 1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            ! Assemble fluxes for owner and neighbour cell
            this%bC(iOwner,:) = this%bC(iOwner,:) - fluxVf(iFace,:)
            this%bC(iNeighbour,:) = this%bC(iNeighbour,:) + fluxVf(iFace,:)

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace=is,ie

                iOwner = mesh%owner(iBFace)

                ! Assemble fluxes for owner and neighbour cell
                this%bC(iOwner,:) = this%bC(iOwner,:) - fluxVf(iBFace,:)

            enddo

        enddo

        !-----------------!
        ! Real Boundaries !
        !-----------------!

        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie
                iOwner = mesh%owner(iBFace)
                this%bC(iOwner,:) = this%bC(iOwner,:) - fluxVf(iBFace,:)
            enddo

        enddo

    end subroutine updateRhsWithCrossDiffusionWithConstantDiffusivity

!------------------------------------------------------------------------------------------------------------------
!                                           PETSc Linear system settings
!------------------------------------------------------------------------------------------------------------------

    subroutine allocateMatrices(this)

    !==================================================================================================================
    ! Description:
    !! allocateMatrices generates the PETSc matrix and rhs.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: ierr
    !------------------------------------------------------------------------------------------------------------------

        MatType myType

        VecType myVec
    !------------------------------------------------------------------------------------------------------------------

        mytype = MATMPIAIJ
        myVec = VECMPI

        !-----------------------------------!
        ! Create Matrix and right hand side !
        !-----------------------------------!

        ! Matrix
        call MatCreate(PETSC_COMM_WORLD, this%A, ierr)

        call MatSetSizes(this%A, flubioSolvers%lm, flubioSolvers%lm, flubioSolvers%M, flubioSolvers%M, ierr)

        call MatSetType(this%A, myType, ierr)

        call MatMPIAIJSetPreallocation(this%A, flubioSolvers%d_nz, mesh%d_nnz, flubioSolvers%o_nz, mesh%o_nnz, ierr)

        call MatSetUp(this%A, ierr)

        ! Rhs
        call VecCreate(PETSC_COMM_WORLD, this%rhs, ierr)

        call VecSetSizes(this%rhs, flubioSolvers%lm, flubioSolvers%M, ierr)

        call VecSetType(this%rhs, myVec, ierr)

        this%isAllocated = .true.

        end subroutine allocateMatrices

!**********************************************************************************************************************

    subroutine solve(this, field, setGuess, reusePC, reuseMat)

    !==================================================================================================================
    ! Description:
    !! solve assembles the matrix and rhs in PETSc format and  solves the linear system Ax=rhs.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------
        type(flubioField) :: field
        !! field to use to compute residuals
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
        !! target element

        integer :: iComp
        !! target compoent

        integer :: setGuess
        !! flag to set the inital guess

        integer :: reusePC
        !! falg to reuse the preconditioner

        integer ::reuseMat
        !! flag to reuse matrix

        integer :: nn
        !! number of neighbours

        integer :: it
        !! number of iterations done in ksp

        integer :: itAMGCL(1)
        !! number of iterations done in amgcl

        integer :: fComp
        !! fied components

        integer*8 :: defaultctx
        !! default context

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        real :: res
        !! final residual

        real :: resAMGCL(5)
        !! final residual in AMGCL

        real :: initResidual
        !! initial scaled residual

        real :: unscaledResidual
        !! initial unscaled residual

        real :: scalingFactor
        !! scaling factor

        real :: fieldMean(this%nComp)
        !! average of the solution vector

        real :: small
        !! small value
    !------------------------------------------------------------------------------------------------------------------

        Vec  x
        !! auxiliary vectors

        Vec vres
        !! residual vector

        PC  pc
        !! Preconditioner

        KSP ksp
        !! KSP

        KSPConvergedReason reason
        !! convergence reason
    !------------------------------------------------------------------------------------------------------------------

        small = 1e-10

        !-------------------------------------------!
        ! Fill the matrix with the equation coeffs  !
        ! Do this many times as the number of comp  !
        !-------------------------------------------!

        fComp = this%nComp
        if(this%nComp>1) fComp = fComp - bdim

        if(this%solverType=='petsc') then

            do iComp=1,fComp

                if(reuseMat<=1) then

                    !Allocate memory for A and rhs
                    if(.not. this%isAllocated) then
                        call this%allocateMatrices()
                        call this%setLinearSolver(this%eqName//'_')
                    endif

                    call MatZeroEntries(this%A, ierr)

                    do iElement=1,numberOfElements

                        ! Diagonal value
                        call MatSetValue(this%A, mesh%cellGlobalAddr(iElement)-1, mesh%cellGlobalAddr(iElement)-1, &
                                        this%aC(iElement,iComp), INSERT_VALUES, ierr)

                        ! Neighbours values
                        nn = mesh%numberOfNeighbours(iElement)
                        call MatSetValues(this%A, 1, mesh%cellGlobalAddr(iElement)-1, nn, mesh%conn(iElement)%col(1:nn)-1, &
                                        this%anb(iElement)%coeffs(1:nn,iComp), INSERT_VALUES, ierr)

                    enddo

                    call MatAssemblyBegin(this%A, MAT_FINAL_ASSEMBLY, ierr)
                    call MatAssemblyEnd(this%A, MAT_FINAL_ASSEMBLY, ierr)

                endif

                !-------------------------------------!
                ! Fill a vector with the equation rhs !
                !-------------------------------------!

                call VecZeroEntries(this%rhs,ierr)

                call VecSetValues(this%rhs, numberOfElements, mesh%cellGlobalAddr-1, this%bC(:,iComp), INSERT_VALUES, ierr)

                call VecAssemblyBegin(this%rhs, ierr)
                call VecAssemblyEnd(this%rhs, ierr)

                !-------------------------!
                ! Solve the linear system !
                !-------------------------!

                ! Duplicate vector
                call VecDuplicate(this%rhs, x, ierr)
                call VecDuplicate(this%rhs, vres, ierr)
                call VecZeroEntries(x, ierr)

                ! Use the same preconditioner according to user's setting. This might or might not improve the performace.
                if(reusePC==1) then
                    call KSPSetReusePreconditioner(this%ksp, PETSC_TRUE, ierr)
                else
                    call KSPSetReusePreconditioner(this%ksp, PETSC_FALSE, ierr)
                endif

                ! Update the matrix and recompute the preconditioner if needed
                call KSPSetOperators(this%ksp, this%A, this%A, ierr)

                ! Set a non-zero guess for the iterative solver. The guess is the latest available solution obtained for phi
                if(setGuess==1) then

                    call VecSetValues(x, numberOfElements, mesh%cellGlobalAddr-1, field%phi(1:numberOfElements,iComp),INSERT_VALUES,ierr)

                    call VecAssemblyBegin(x, ierr)
                    call VecAssemblyEnd(x, ierr)

                    call KSPSetInitialGuessNonZero(this%ksp,PETSC_TRUE,ierr)

                endif

                ! Initial residual ad scaling factor
                initResidual = this%initialResidualPETSc(field, iComp)
                scalingFactor = this%scaledResidualPETSc(field, iComp, 'standard')
                this%res(iComp)  = initResidual

                ! Set convergece test, Default + minIter
                flubioSolvers%itmin = this%minIter
                call KSPConvergedDefaultCreate(defaultctx, ierr)
                call KSPSetConvergenceTest(this%ksp, KSPConvergenceTestFlubio, defaultctx, PETSC_NULL_FUNCTION, ierr)

                ! Solve the system
                call KSPSolve(this%ksp, this%rhs, x, ierr)

                ! Check convergence
                call KSPGetConvergedReason(this%ksp, reason, ierr)

                if (reason < 0) then
                    if(id==0) write(*,*) reason
                    call flubioStopMsg('ERROR: KSP has not converged. Simulations stopped.')
                    return
                endif

                ! Extract values
                call VecGetValues(x, flubioSolvers%lm, mesh%cellGlobalAddr-1, field%phi(1:numberOfElements,iComp), ierr)

                ! Get final residual norm and number of iterations
                call MatMult(this%A, x, vres, ierr)
                call VecAXPY(vres, -1.0, this%rhs, ierr)
                call VecNorm(vres, NORM_1, res, ierr)
                call KSPGetIterationNumber(this%ksp, it, ierr)

                ! Store the residuals
                this%unscaledRes(iComp) = initResidual
                if(reuseMat<=1) then
                    this%convergenceResidual(iComp) = initResidual
                    this%scalingRef(iComp) = scalingFactor
                end if

                ! KSP verbosity
                call this%kspVerbose(field%fieldName(iComp), it, reuseMat, initResidual, res, scalingFactor)

                ! ALWAYS deallocate memory for petsc objects created in any subroutine!
                call VecDestroy(x,ierr)
                call VecDestroy(vres,ierr)

            end do

        ! Solve with AMGCL instead
        else

            call meanField(field, fieldMean)

            do iComp=1,fComp

                ! Compute initial residual
                initResidual = this%initialResidualFlubio(field, iComp)
                scalingFactor = this%scaledResidualFlubio(field, iComp, 'standard')
                this%res(iComp) = initResidual

                ! Solve with AMCL wrapper
                call this%solveAMGCL(field%phi(1:numberOfElements, iComp), iComp, itAMGCL, fieldMean(iComp))

                ! Store the residuals
                this%unscaledRes(iComp) = initResidual
                if(reuseMat<=1) then
                    this%convergenceResidual(iComp) = initResidual
                    this%scalingRef(iComp) = scalingFactor
                end if

                ! Compute final residual
                call field%updateGhosts()
                unscaledResidual = this%initialResidualFlubio(field, iComp)

                ! Print to screen
                call this%amgclVerbose(field%fieldName(iComp), itAMGCL(1), initResidual, unscaledResidual, scalingFactor)

            enddo

        endif

    end subroutine solve

! *********************************************************************************************************************

    subroutine explicitSolve(this, field)

    !==================================================================================================================
    ! Description:
    !! explicitMomentumSolve solves explicitely the target equation. It assembles the matrix, but does not call the linear solver.
    !! Instead, It perform just a matrix-vector multiplication with the last known values of the cell centered field.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field
        !! field at the old time step
    !------------------------------------------------------------------------------------------------------------------

        Vec y
        !! auxiliary vecotr to store a rhs
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
        !! target element

        integer :: iComp
        !! target component

        integer :: fComp
        !! number of field component

        integer :: ierr
        !! error flag

        integer :: nn
        !! number of neighbours

        real :: sumAnb(numberOfElements,1)
    !------------------------------------------------------------------------------------------------------------------

        fComp = this%nComp
        if(fComp>1) fComp = fComp - 1

        do iComp=1,fComp

            if(.not. this%isAllocated) call this%allocateMatrices()

            !---------------------!
            ! Assemble the matrix !
            !---------------------!

            call MatZeroEntries(this%A, ierr)
            call VecZeroEntries(this%rhs, ierr)
            sumAnb = 0.0

            do iElement=1,numberOfelements

                ! Diagonal value
                call MatSetValue(this%A, mesh%cellGlobalAddr(iElement)-1, mesh%cellGlobalAddr(iElement)-1, &
                                 this%aC(iElement,iComp), INSERT_VALUES,ierr)

                ! Neighbours values
                nn = mesh%numberOfNeighbours(iElement)
                call MatSetValues(this%A, 1, mesh%cellGlobalAddr(iElement)-1, nn, mesh%conn(iElement)%col(1:nn)-1, &
                                  this%anb(iElement)%coeffs(1:nn,iComp), INSERT_VALUES, ierr)

            enddo

            call MatAssemblyBegin(this%A, MAT_FINAL_ASSEMBLY, ierr)
            call MatAssemblyEnd(this%A ,MAT_FINAL_ASSEMBLY, ierr)

            !-------------------------------------!
            ! Fill a vector with the equation rhs !
            !-------------------------------------!

            ! duplicate the vector
            call VecDuplicate(this%rhs, y, ierr)

            call VecSetValues(this%rhs, numberOfElements, mesh%cellGlobalAddr-1 ,field%phi(1:numberOfElements,iComp), INSERT_VALUES, ierr)

            call VecAssemblyBegin(this%rhs, ierr)
            call VecAssemblyEnd(this%rhs, ierr)

            !------------------------------------------------------------------------!
            ! Matrix multiplication, remember to subtract aC(iElement)*phi(iElement) !
            !------------------------------------------------------------------------!

            call MatMult(this%A, this%rhs, y, ierr)
            call VecGetValues(y, flubioSolvers%lm, mesh%cellGlobalAddr-1, sumAnb(:,1), ierr)

            ! Subtract off the diagonal term
            do iElement=1,numberOfElements

                this%bC(iElement,iComp) = this%bC(iElement,iComp) - (sumAnb(iElement,1) - &
                        this%aC(iElement,iComp)*field%phi(iElement,iComp))

                field%phi(iElement,iComp) = this%bC(iElement,iComp)/this%aC(iElement,icomp)

            enddo

            ! Destroy PETSc object
            call VecDestroy(y, ierr)

        enddo

    end subroutine explicitSolve

!**********************************************************************************************************************

    subroutine setLinearSolver(this, kspName)

    !==================================================================================================================
    ! Description:
    !! setLinearSolver sets the linear solvers options according to the user's choices.
    !! Please note that all the solvers use the default settings. To custumize your application you should use
    !! the command line database as specified in PETSc.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        PetscReal div

        KSPType  mySolver

        PC  mypc

        character(len=*) kspName
        !! KSP name used to target this KSP object
    !------------------------------------------------------------------------------------------------------------------

        integer :: myPrecond
        !! preconditioner flag

        integer :: ufields(3-bdim), vfields(1), wfields(1), pfields(1), bs

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        mySolver = this%solverName
        myPrecond = this%pc
        kspName = kspName

        div = 10000.0 ! or PETSC_DEFAULT_REAL

        !  linear solver options for velocity
        call KSPCreate(PETSC_COMM_WORLD, this%ksp, ierr)
        call KSPSetOptionsPrefix(this%ksp, kspName ,ierr)
        call KSPSetType(this%ksp, mySolver, ierr)
        call KSPSetTolerances(this%ksp, this%relTol, this%absTol, div, this%maxIter, ierr)
        !call KSPSetMinimumIterations(this%ksp, this%minIter, ierr)
        call KSPSetErrorIfNotConverged(this%ksp, PETSC_TRUE, ierr)

        ! Set norm type
        call KSPSetNormType(this%ksp, KSP_NORM_UNPRECONDITIONED, ierr)

        if(this%solverName=='ibcgs') call KSPSetLagNorm(this%ksp, PETSC_TRUE, ierr)

        ! Set this preconditioner
        call KSPGetPC(this%ksp, mypc, ierr)

        if(myPrecond==0) then

            ! Block Jacobi
            call PCSetType(mypc,PCBJACOBI,ierr)

        elseif(myPrecond==1) then

            ! Additive  Schwarz
            call PCSetType(mypc,PCASM,ierr)

        elseif(myPrecond==2) then

            ! GAMG
            call PCSetType(mypc,PCGAMG,ierr)

        elseif(myPrecond==3) then

            ! Hypre
            call PCSetType(mypc,PCHYPRE,ierr)

        elseif(myPrecond==4) then

            ! Point Jacobi
            call PCSetType(mypc,PCPBJACOBI,ierr)

        elseif(myPrecond==5) then

            ! SOR
            call PCSetType(mypc,PCSOR,ierr)

        elseif(myPrecond==6) then

            ! Sparse Approximate Inverse method (SPAI)
            call PCSetType(mypc,PCSPAI,ierr)

        elseif(myPrecond==7) then

            ! Kaczmarz
            ! call PCSetType(mypc,PCKACZMARZ,ierr)

            call flubioMsg('FLUBIO: this preconditioner is in the PETSc list, but appears to be not working')

        elseif(myPrecond==8) then

            ! ML
            call PCSetType(mypc, PCML, ierr)

        elseif(myPrecond==9) then

            ! EISENSTAT
            call PCSetType(mypc, PCEISENSTAT, ierr)

         elseif(myPrecond==10) then

            ! ICC
            call PCSetType(mypc, PCICC, ierr)

         elseif(myPrecond==11) then

            ! LU
            call PCSetType(mypc, PCLU, ierr)

         elseif(myPrecond==12) then

            ! CHOLESKY
            call PCSetType(mypc, PCCHOLESKY, ierr)

        elseif(myPrecond==-1) then

            ! No preconditioning
            call PCSetType(mypc,PCNONE,ierr)

        else

            ! Default
            call PCSetType(mypc,PCBJACOBI,ierr)

        endif

        ! Set preconditioner reordering type
        call PCFactorSetMatOrderingType(mypc, flubioSolvers%matOrderingType, ierr)

        ! Enable command line option
        call KSPSetFromOptions(this%ksp, ierr)

    end subroutine setLinearSolver

! *********************************************************************************************************************

    subroutine addBoundedTerm(this)

    !==================================================================================================================
    ! Description:
    !! addBoundedTerm adds an extra stabilzation term to the matrix diagonal as function of the velocity divergence:
    !! \begin{equation*}
    !!   a_C = a_C + \nabla\cdot u
    !! \end{equation*}
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp, iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: divTmp(numberOfElements)
        !! divergence of \texbf{u}
    !------------------------------------------------------------------------------------------------------------------

        divTmp = div(mf)

        do iComp=1,this%nComp
            this%aC(:,iComp) = this%aC(:,iComp) - divTmp
        enddo

    end subroutine addBoundedTerm

! *********************************************************************************************************************

    subroutine resetCoeffs(this)

    !==================================================================================================================
    ! Description:
    !! resetCoeffs makes zero the coeffs and rhs of the target equation.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        ! Reset coefficients
        this%aC = 0.0
        call resetNonUniformMatrix(this%anb, numberOfElements)
        this%bC = 0.0

    end subroutine resetCoeffs

!**********************************************************************************************************************

    function implicitPressureGradCentralCoeff(this) result(aCUP)

        !==================================================================================================================
        ! Description:
        !! implicitPressureGradCentralCoeff computes the pressure-velocity coupling central coeffs
        !! due to the discretisation of the pressure gradient/velocity face flux.
        !==================================================================================================================
    
            class(transportEqn) :: this
        !------------------------------------------------------------------------------------------------------------------
    
            integer :: iFace, iBFace, iBoundary, iOwner, iNeighbour
    
            integer :: pNeigh, iProc, is, ie, patchFace
        !------------------------------------------------------------------------------------------------------------------
    
            real :: gf
            !! weighthing factor
    
            real :: Sf(3)
            !! Surface area vector
    
            real :: fF(3)
            !! face to neighbour distance vector
    
            real :: Cf(3)
            !! owner to face distance vector
    
            real :: nf(3)
            !! face normal

            real :: aCUP(numberofElements, 3)
        !------------------------------------------------------------------------------------------------------------------
    
            ! Internal Faces
            do iFace=1,numberOfIntFaces
    
                iOwner = mesh%owner(iFace)
                iNeighbour = mesh%neighbour(iFace)
        
                ! gf for the owner is 1-gf for the way it is computed, legacy code!
    
                Cf = mesh%fcentroid(iFace,:) - mesh%centroid(:,iOwner)
                fF = mesh%centroid(:,iNeighbour) - mesh%fcentroid(iFace,:)
                nf = mesh%Sf(iFace,:)/norm2(mesh%Sf(iFace,:))
    
                gf = 1.0 - dot_product(Cf,nf)/(dot_product(Cf,nf) + dot_product(fF,nf) )
                Sf = mesh%Sf(iFace,:)
    
                ! Owner
                aCUP(iOwner,:) = aCUP(iOwner,:) + gf*Sf
  
                ! Neighbour, Check the sign
                aCUP(iNeighbour,:) = aCUP(iNeighbour,:) - (1-gf)*Sf
  
            enddo
    
            ! Processor boundaries
            do iBoundary=1,numberOfProcBound
    
               iProc = mesh%boundaries%procBound(iBoundary)
               is = mesh%boundaries%startFace(iProc)
               ie = is+mesh%boundaries%nFace(iProc)-1
               patchFace = numberOfElements + (is-numberOfIntFaces-1)
               pNeigh = 0
    
               do iBFace=is,ie
    
                    pNeigh = pNeigh+1
                    iOwner = mesh%owner(iBFace)
          
                    Cf = mesh%fcentroid(iBFace,:) - mesh%centroid(:,iOwner)
                    fF = mesh%centroid(:,patchFace+pNeigh) - mesh%fcentroid(iBFace,:)
                    nf = mesh%Sf(iBFace,:)/norm2(mesh%Sf(iBFace,:))
    
                    gf = 1.0 - dot_product(Cf,nf)/(dot_product(Cf,nf) + dot_product(fF,nf))
                    Sf = mesh%Sf(iBFace,:)
    
                    ! Owner
                    aCUP(iOwner,:) = aCUP(iOwner,:) + gf*Sf

               enddo
    
            enddo
    
        end function implicitPressureGradCentralCoeff

! *********************************************************************************************************************

    subroutine addBodyForce(this, f, nComp)

    !==================================================================================================================
    ! Description:
    !! addBodyForce adds a body force to the target equation. Please note that the body force is added with a negative sign!
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iComp, nComp
    !------------------------------------------------------------------------------------------------------------------

        real :: f(numberOfElements+numberOfBFaces, nComp)
        !! body force to add
    !------------------------------------------------------------------------------------------------------------------

        do iComp=1,this%nComp
           this%bC(:, iComp) = this%bC(:,iComp) - f(1:numberOfElements,iComp)*mesh%volume(:)
        enddo

    end subroutine addBodyForce

! *********************************************************************************************************************

    subroutine balancedBodyForce(fb, f, nComp)

    !==================================================================================================================
    ! Description:
    !! addBalancedBodyForce takes a body force, perform a stabilization procedure and then it adds it to the equation.
    !! This subroutine is experimental and its definition is found is eq. (15.211) Moukalleld et al., 1st edition.
    !==================================================================================================================

        type(flubioField) :: bodyForce
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iBFace, iFace, iElement, iComp, nComp

        integer :: iProc, pNeigh, is, ie
    !------------------------------------------------------------------------------------------------------------------

        integer :: iOwner, iNeighbour
    !------------------------------------------------------------------------------------------------------------------

        real :: Svol(3)
        !! Surface normal vector scaled by the cell volume

        real :: dot
        !! auxiliary variable for a dot product

        real :: f(numberOfElements+numberOfBFaces, nComp)
        !! body force to balance

        real :: fb(numberOfElements+numberOfBFaces, nComp)
        !! balanced body force
    !------------------------------------------------------------------------------------------------------------------

        fb = 0.0

        if(nComp==1) call flubioStopMsg('FLUBIO: this routine works only with vector fields')

        ! Create a field containing a body force
        call bodyForce%createWorkField(fieldName='bodyForce', classType='scalar', nComp=nComp)

        ! Interpolate the body force at mesh faces
        call interpolate(bodyForce, 'linear', -1)

        ! Internal Faces
        do iFace=1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            ! Assemble for owner cell
            Svol = mesh%Sf(iFace,:)/mesh%volume(iOwner)
            dot = dot_product(bodyForce%phif(iFace,:), mesh%CN(iFace,:))
            fb(iOwner,:) = fb(iOwner,:) + (1.0-mesh%gf(iFace))*Svol*dot

            ! Assemble for neighbour cell
            Svol = -mesh%Sf(iFace,:)/mesh%volume(iNeighbour)
            dot = dot_product(bodyForce%phif(iFace,:), -mesh%CN(iFace,:))
            fb(iNeighbour,:) = fb(iNeighbour,:) + mesh%gf(iFace)*Svol*dot

        end do

        ! Processor Boundaries
        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1
            pNeigh = 0

            do iBFace=is,ie

                pNeigh=pNeigh+1
                iOwner = mesh%owner(iBFace)

                ! Assemble for owner cell
                Svol = mesh%Sf(iBFace,:)/mesh%volume(iOwner)
                dot = dot_product(bodyForce%phif(iBFace,:), mesh%CN(iBFace,:))
                fb(iOwner,:) = fb(iOwner,:) + (1.0-mesh%gf(iBFace))*Svol*dot

            enddo

        enddo

        ! Real Boundaries
        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie

                iOwner = mesh%owner(iBFace)

                ! Assemble for owner cell
                Svol = mesh%Sf(iBFace,:)/mesh%volume(iOwner)
                dot = dot_product(bodyForce%phif(iBFace,:), mesh%CN(iBFace,:))
                fb(iOwner,:) = fb(iOwner,:) + (1.0-mesh%gf(iBFace))*Svol*dot

            enddo

        enddo

        ! Destroy the work field
        call bodyForce%destroyField()

    end subroutine balancedBodyForce

! *********************************************************************************************************************

    subroutine addbalancedBodyForce(this, f, nComp)

    !==================================================================================================================
    ! Description:
    !! addBalancedBodyForce takes a body force, perform a stabilization procedure and then it adds it back to the equation.
    !! This function is experimental and its definition is found is eq. (15.211) Moukalleld et al., 1st edition.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp, iElement, nComp
    !------------------------------------------------------------------------------------------------------------------

        integer :: iOwner, iNeighbour
    !------------------------------------------------------------------------------------------------------------------

        real :: f(numberOfElements+numberOfBFaces, nComp)
        !! body force to balance

        real :: fb(numberOfElements+numberOfBFaces, nComp)
        !! balanced body force to add
    !------------------------------------------------------------------------------------------------------------------

        call balancedBodyForce(fb, f, nComp)

        do iComp=1,this%nComp
            this%bC(:,iComp) = this%bC(:,iComp) - fb(1:numberOfElements,iComp)*mesh%volume
        enddo

    end subroutine addBalancedBodyForce

! *********************************************************************************************************************

    subroutine addDevTerm(this, mu, nComp)

    !==================================================================================================================
    ! Description:
    !! addDevTerm adds the deviatoric term $2/3\mu div(U) \phi$ to the transport equation
    !! implicitly if the sign is positive, explictly if negative.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, nComp
    !------------------------------------------------------------------------------------------------------------------

        real :: div(numberOfElements + numberOfBFaces)
        !! auxiliary variable

        real :: aC
        !! matrix contribution

        real :: bC
        !! rhs contribution

        real :: vol
        !! cell volume

        real :: mu
        !! cell diffusivity

        real :: dev
    !------------------------------------------------------------------------------------------------------------------

        ! divergence of U
        call divU(div)

        do iElement=1,numberOfElements

            vol = mesh%volume(iElement)

            dev = (2.0/3.0)*mu*div(iElement)*vol

            call doubleBar(dev, 0.0, aC)
            call doubleBar(-dev, 0.0, bC)

            this%aC(iElement,:) = this%aC(iElement,:) + aC
            this%bC(iElement,:) = this%bC(iElement,:) + bC

        enddo

    end subroutine addDevTerm

! *********************************************************************************************************************

    subroutine addSubstantialDerivative(this, field)

    !==================================================================================================================
    ! Description:
    !! addSubstantialDerivative adds the substantial derivative of a field as source term to the equation rhs:
    !! \begin{equation*}
    !!  S = \dfrac{D\phi}{Dt}V_C=(\dfrac{\partial \phi -\phi^\circ}{\partial t}+\textbf{u}\cdot\nabla\phi)V_C
    !! \end{equation*}
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement

        integer :: iComp

    !------------------------------------------------------------------------------------------------------------------

        real :: vol
        !! cell volume

        real :: timeTerm(field%nComp)
        !! time detivative

        real :: advTerm(field%nComp)
        !! convective term

        real :: DphiDt(field%nComp)
        !! substantial derivative
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements

            vol = mesh%volume(iElement)

            do iComp=1,field%nComp
                advTerm(iComp) = dot_product(velocity%phi(iElement,:),field%phiGrad(iElement,:,iComp))
            end do

            if(steadySim==0) then
                timeTerm = (field%phi(iElement,:)-field%phio(iElement,:))/dtime
                DphiDt = timeTerm + advTerm
            else
                DphiDt = advTerm
            end if

            this%bC(iElement,:) = this%bC(iElement,:) + DphiDt*vol

        enddo

    end subroutine addSubstantialDerivative

! *********************************************************************************************************************

    subroutine addCustomSourceTerm(this, field)

    !==================================================================================================================
    ! Description:
    !! computeCustomSourceTerm adds a source term for the target equation as specified in settings/volumeSource.
    !==================================================================================================================

        use parseFromFile

        class(transportEqn) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: sourceName
        !! expression value

        character(len=:), allocatable :: sourceType
        !! source type

        character(len=:), allocatable :: linearPart
        !! expression value for the linear part

        character(len=:), allocatable :: nonLinearPart
        !! expression value for the non linear part

        character(len=:), allocatable :: expr
        !! expression value
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, iRegion, iSource, nSources, fComp
    !------------------------------------------------------------------------------------------------------------------

        real ::  aC(field%nComp), bC(field%nComp), SMALL

        real :: linearSourceTerm(numberOfElements, field%nComp)
        !! source term to add

        real :: nonLinearsourceTerm(numberOfElements, field%nComp)
        !! source term to add

        real :: sourceTerm(numberOfElements, field%nComp)
        !! source term to add
    !------------------------------------------------------------------------------------------------------------------

        logical :: found, customSourcesFound, eqnFound, sourceFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, customSourcesPointer, eqnPointer, sourcePointer
    !------------------------------------------------------------------------------------------------------------------

        SMALL = 1.0e-10

        fComp = field%nComp

        sourceTerm = 0.0
        linearSourceTerm = 0.0
        nonLinearSourceTerm = 0.0

        call flubioSources%json%info('customVolumeSources%'//this%eqName, n_children=nSources)

        ! Get the pointer to the dictionary
        call flubiosources%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'customVolumeSources', customSourcesPointer, customSourcesFound)
        call raiseKeyErrorJSON('customVolumeSources%', customSourcesFound, 'settings/volumeSources')

        call jcore%get(customSourcesPointer, this%eqName, eqnPointer, eqnFound)
        call raiseKeyErrorJSON(this%eqName, eqnFound, 'settings/volumeSources')

        do iSource=1,nSources

            ! Get monitors pointer
            call jcore%get_child(eqnPointer, iSource, sourcePointer, sourceFound)

            ! Get monitor type
            call jcore%get(sourcePointer, 'type', sourceType, found)
            call raiseKeyErrorJSON('type', found, 'settings/volumeSources')

            if(lowercase(sourceType)=='explicitsource') then

                call jcore%get(sourcePointer, 'expression', expr, sourceFound)
                if(.not. found) call flubioStopMsg('ERROR: cannot find the expression for the volume source "'//sourceName//'"')

                call expressionParsingWithField(expr, sourceTerm, field)

                !! add the volume source to the rhs
                do iElement=1,numberOfElements
                    aC = 0.0
                    bC = sourceTerm(iElement,:)*mesh%volume(iElement)
                    call applyToSelectedRegions(aC, bC, iElement, sourceName, this%nComp)
                    this%bC(iElement,:) = this%bC(iElement,:) + bC(:)
                enddo

            elseif(lowercase(sourceType)=='nonlinearsource') then

                call flubioOptions%json%get('VolumeSources%'//sourceName//'%implictPart', linearPart, found)
                if(.not. found) call flubioStopMsg('ERROR: cannot find the linear part for the volume source "'//sourceName//'"')

                call expressionParsingWithField(linearPart, linearSourceTerm, field)

                call flubioOptions%json%get('VolumeSources%'//sourceName//'%explicitPart', nonLinearPart, found)
                if(.not. found) call flubioStopMsg('ERROR: cannot find the non-linear part for the volume source "'//sourceName//'"')

                call expressionParsingWithField(nonLinearPart, nonLinearSourceTerm, field)

                !! Add the volume source to diagonal and rhs
                do iElement=1,numberOfElements

                    aC = linearSourceTerm(iElement,:)*mesh%volume(iElement)
                    bC = nonLinearSourceTerm(iElement,:)*mesh%volume(iElement)
                    call applyToSelectedRegions(aC, bC, iElement, sourceName, this%nComp)

                    this%aC(iElement,:) = this%aC(iElement,:) - aC(:)
                    this%bC(iElement,:) = this%bC(iElement,:) + bC(:)

                enddo

            else

                call flubioStopMsg('ERROR: unknown source type. Either use explicitSource &
                        to an explicit source term or nonLinearSource to provide a linear + non linear &
                        source decomposition.')
            end if

        end do

        ! Clean up
        nullify(dictPointer)
        if(customSourcesFound) nullify(customSourcesPointer)
        if(eqnFound) nullify(eqnPointer)
        if(sourceFound) nullify(sourcePointer)
        call jcore%destroy()

    end subroutine addCustomSourceTerm

! *********************************************************************************************************************

    subroutine addVolumeSourceTerms(this)

    !==================================================================================================================
    ! Description:
    !! computeCustomSourceTerm adds a source term for the target equation as specified in settings/volumeSource.
    !==================================================================================================================

        use parseFromFile

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iSource
    !------------------------------------------------------------------------------------------------------------------

        real :: aC(numberOfElements,3)
        !! Diagonal coefficient

        real :: bC(numberOfElements,3)
        !! right hand side
    !------------------------------------------------------------------------------------------------------------------

        do iSource=1,size(this%fvSources)
            call this%fvSources(iSource)%source%computeSource(aC, bC)
            this%aC(:,1:this%nComp) = this%aC(:,1:this%nComp) + aC(:,1:this%nComp)
            this%bC(:,1:this%nComp) = this%bC(:,1:this%nComp) + bC(:,1:this%nComp)
        enddo

    end subroutine addVolumeSourceTerms

! *********************************************************************************************************************

    subroutine addExplicitSourceTerm(this, source, fComp)

    !==================================================================================================================
    ! Description:
    !! addExplicitSourceTerm adds a source term wrapped in field form.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, fComp
    !------------------------------------------------------------------------------------------------------------------

        real :: source(numberOfElements, fComp)

        real :: bC(fComp)
    !------------------------------------------------------------------------------------------------------------------

        !! add the rhs with the volume source
        do iElement=1,numberOfElements
            bC = source(iElement,:)*mesh%volume(iElement)
            this%bC(iElement,:) = this%bC(iElement,:) + bC(:)
        enddo

    end subroutine addExplicitSourceTerm

! *********************************************************************************************************************

    subroutine addLinearizedSourceTerm(this, linearPart, nonLinearPart, fComp)

    !==================================================================================================================
    ! Description:
    !! addLinearizedSourceTerm adds a source term to the equation. The linear part contributes to the diagonal coefficient.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, fComp
    !------------------------------------------------------------------------------------------------------------------

        real :: linearPart(numberOfElements, fComp)
        !! linear part

        real :: nonLinearPart(numberOfElements, fComp)
        !! non-linear part

        real :: aC(fComp), bC(fComp)
    !------------------------------------------------------------------------------------------------------------------

        !! add the rhs with the volume source
        do iElement=1,numberOfElements

            aC = linearPart(iElement,:)*mesh%volume(iElement)
            bC = nonLinearPart(iElement,:)*mesh%volume(iElement)

            this%aC(iElement,:) = this%aC(iElement,:) - aC(:)
            this%bC(iElement,:) = this%bC(iElement,:) + bC(:)

        enddo

    end subroutine addLinearizedSourceTerm

! *********************************************************************************************************************

    subroutine applyBoundaryConditionsTransport(this, field, diffCoeff, convCoeff)

    !==================================================================================================================
    ! Description:
    !! applyBoundaryConditionsTransport applies the boundary conditions for the target transport equation.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field
        !! field

        type(flubioField) :: diffCoeff
        !! diffusion coefficient

        type(flubioField), optional :: convCoeff
        !! convection coefficient
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iOwner, iBFace, iComp, is ,ie, patchFace, bFlag
    !------------------------------------------------------------------------------------------------------------------

        real :: c
        !! convection coeffcient

        real :: mu
        !! diffusion coeffcient

        real phi0(this%nComp)
        !! boundary calue to be eventully assigned

        real, dimension(:), allocatable :: slipLength
        !! slip length for robin bC

        real, dimension(:), allocatable :: phiInf
        !! coefficient in robin boundary condition
    !------------------------------------------------------------------------------------------------------------------

        real :: aCb(field%nComp)
        !! diagonal coefficient boundary correction

        real :: bCb(field%nComp)
        !! rhs boundary correction

        real :: acd(field%nComp)
        !! diffusion term contribution

        real :: acc(field%nComp)
        !!convective term contribution
    !------------------------------------------------------------------------------------------------------------------

        patchFace = numberOfElements

        ! Interpolate physical quantities
        if(present(convCoeff)) then
            call interpolate(diffCoeff, phys%interpType, -1)
            call interpolate(convCoeff, phys%interpType, -1)
        else
            call interpolate(diffCoeff, phys%interpType, -1)
        end if

        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
            bFlag = field%bcFlag(iBoundary)

            do iBFace=is, ie

                patchFace = patchFace+1
                iOwner = mesh%owner(iBFace)

                c = 1.0
                if(present(convCoeff)) c = convCoeff%phif(iBFace,1)
                mu = diffCoeff%phif(iBFace,1)

                ! void bc
                if(bFlag==0) then

                    call voidBoundary(aCb, bCb, acd, acc, this%nComp)

                ! Dirichlet bc
                elseif (bFlag==1) then

                    call dirichletBoundary(iBFace, aCb, bCb, acd, acc, field%phi(patchFace,:), mu, c, this%nComp)

                ! Zero Gradient bc
                elseif (bFlag==2) then

                    call neumann0Boundary(iBFace, aCb, bCb, acd, acc, c, this%nComp)

                ! Fixed Gradient bc
                elseif (bFlag==3) then

                    call neumannBoundary(iBFace, aCb, bCb, acd, acc, field%boundaryValue(iBoundary,:), mu, c, this%nComp)

                ! Fixed Flux bc
                elseif (bFlag==4) then

                    call prescribedFluxBoundary(iBFace, aCb, bCb, acd, acc, field%boundaryValue(iBoundary,:), mu, c, this%nComp)

                ! Robin bc
                elseif (bFlag==5) then

                    call field%bcDict%get(trim(mesh%boundaries%userName(iBoundary))//'%slipLength', slipLength)
                    call field%bcDict%get(trim(mesh%boundaries%userName(iBoundary))//'%phiInf', phiInf)
                    call robinBoundary(iBoundary, iBFace, aCb, bCb, acd, acc, field%phiGrad(patchFace,:,:), field%phi(iBFace,:), mu, slipLength, phiInf, this%nComp)

                ! FarField
                elseif(bFlag==6) then

                    phi0 = field%boundaryValue(iBoundary,1)
                    call farFieldBoundary(iBFace, aCb, bCb, acd, acc, phi0, mu, c,this%nComp)

                ! User defined dirichlet
                elseif (bFlag==1000) then

                    call dirichletBoundary(iBFace, aCb, bCb, acd, acc, field%phi(patchFace,:), mu, c, this%nComp)

                ! Turbulent Intensity
                elseif (bFlag==7) then

                    call dirichletBoundary(iBFace, aCb, bCb, acd, acc, field%phi(patchFace,:), mu, c, this%nComp)

                ! Viscosity ratio
                elseif (bFlag==8) then

                    call dirichletBoundary(iBFace, aCb, bCb, acd, acc, field%phi(patchFace,:), mu, c, this%nComp)

                ! Low Re omega wall function
                elseif (bFlag==9) then

                    ! wall function, set as dirichlet condition, but be AWARE to set the cell center and the wall value
                    ! before calling the boundary conditions.
                    call dirichletBoundary(iBFace, aCb, bCb, acd, acc, field%phi(patchFace,:), mu, c, this%nComp)

                ! High Re omega wall function
                elseif (bFlag==10) then

                    ! wall function, set as dirichlet condition, but be AWARE to set the cell center and the wall value
                    ! before calling the boundary conditions.
                    call dirichletBoundary(iBFace, aCb, bCb, acd, acc, field%phi(patchFace,:), mu, c, this%nComp)

                ! Low Re tke wall function
                elseif (bFlag==11) then

                    ! wall function, set as dirichlet condition, but be AWARE to set the cell center and the wall value
                    ! before calling the boundary conditions.
                    call dirichletBoundary(iBFace, aCb, bCb, acd, acc, field%phi(patchFace,:), mu, c, this%nComp)

                else

                    call flubioStopMsg('FLUBIO: Boundary Condition not implemented at '//mesh%boundaries%userName(iBoundary))

                endif

                ! Correct coefficients
                this%aC(iOwner,:) = this%aC(iOwner,:) + aCb
                this%bC(iOwner,:) = this%bC(iOwner,:) + bCb

                if(this%adaptiveRelaxation==1) then
                    this%aCD(iOwner,:) = this%aCD(iOwner,:) + acd
                    this%aCC(iOwner,:) = this%aCC(iOwner,:) + acc
                endif

            enddo

        enddo

    end subroutine applyBoundaryConditionsTransport

! *********************************************************************************************************************

    subroutine assembleEq(this, field, muEff, boundaryNonOrthCorr)

    !==================================================================================================================
    ! Description:
    !! assembleEq wraps all the necessary operations needed to assemble an equation. It is suefull to quicly build an equation
    !! in one shot.
    !==================================================================================================================

        use physicalConstants
    !------------------------------------------------------------------------------------------------------------------

        class(transportEqn) :: this

        type(flubioField) :: field, muEff
    !------------------------------------------------------------------------------------------------------------------

        logical :: applyBoundaryNonOrthCorr

        logical, optional :: boundaryNonOrthCorr
    !------------------------------------------------------------------------------------------------------------------

        if(present(boundaryNonOrthCorr)) then
            applyBoundaryNonOrthCorr = boundaryNonOrthCorr
        else
            applyBoundaryNonOrthCorr = .false.
        endif

        !------------------------------------!
        ! Reset coeffs and update old fields !
        !------------------------------------!

        call this%createEqCoeffs()
        call this%resetCoeffs()

        call field%updateOldFieldValues()

        !--------------------------!
        ! Assemble linear system   !
        !--------------------------!

        ! Transient
        if(steadySim==0) call this%addTransientTerm(field, rho)

        ! Diffusion
        call this%addDiffusionTerm(field, muEff, applyBoundaryNonOrthCorr)

        ! Convection
        if(this%convImp==0) then
            call this%addConvectiveTerm(field)
        else
            call this%addConvectiveTermImplicit(field)
        end if

        ! Boundary conditions
         call this%applyBoundaryConditions(field, muEff)

    end subroutine assembleEq

! *********************************************************************************************************************

    subroutine assembleEqWithCoeff(this, field, muEff, cEff, boundaryNonOrthCorr)

    !==================================================================================================================
    ! Description:
    !! assembleEqWithCoeff wraps all the necessary operations needed to assemble an equation. It is suefull to quicly build an equation
     !! in one shot.
    !==================================================================================================================

        use physicalConstants
    !------------------------------------------------------------------------------------------------------------------

        class(transportEqn) :: this

        type(flubioField) :: field, muEff, cEff
    !------------------------------------------------------------------------------------------------------------------

        logical :: applyBoundaryNonOrthCorr

        logical, optional :: boundaryNonOrthCorr
    !------------------------------------------------------------------------------------------------------------------

        if(present(boundaryNonOrthCorr)) then
            applyBoundaryNonOrthCorr = boundaryNonOrthCorr
        else
            applyBoundaryNonOrthCorr = .false.
        endif

        !------------------------------------!
        ! Reset coeffs and update old fields !
        !------------------------------------!

        call this%resetCoeffs()

        call field%updateOldFieldValues()

        !--------------------------!
        ! Assemble linear system   !
        !--------------------------!

        ! Transient
        if(steadySim==0) call this%addTransientTerm(field, rho, cEff)

        ! Diffusion
        call this%addDiffusionTerm(field, muEff, applyBoundaryNonOrthCorr)

        ! Convection
        if(this%convImp==0) then
            call this%addConvectiveTerm(field, cEff)
        else
            call this%addConvectiveTermImplicit(field, cEff)
        end if

        ! Boundary conditions
        call this%applyBoundaryConditions(field, muEff, cEff)

    end subroutine assembleEqWithCoeff

! *********************************************************************************************************************

    subroutine setUpDiffusionTerm(this, field, muEff, crossDiffusionTerm)

    !==================================================================================================================
    ! Description:
    !! setUpDiffusionTerm wraps all the necessary operations needed to assemble diffusionTerm.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field, muEff

        character(len=*) :: crossDiffusionTerm
    !------------------------------------------------------------------------------------------------------------------

        ! Add to matrix
        call this%addDiffusionTerm(field, muEff)

    end subroutine setUpDiffusionTerm

! *********************************************************************************************************************

    subroutine setUpConvectiveTerm(this, field)

    !==================================================================================================================
    ! Description:
    !! setUpConvectiveTerm wraps all the necessary operations needed to assemble the convective term.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        ! Add to matrix
        if(this%convImp==0) then
            call this%addConvectiveTerm(field)
        else
            call this%addConvectiveTermImplicit(field)
        end if

    end subroutine setUpConvectiveTerm

! *********************************************************************************************************************

    subroutine assembleMat(this, iComp)

    !==================================================================================================================
    ! Description:
    !! assembleMat assembles the matrix for the ith component of the equation in PETSc format.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
        !! target element

        integer :: iComp
        !! target compoent

        integer :: setGuess
        !! flag to set the inital guess

        integer :: reusePC
        !! falg to reuse the preconditioner

        integer ::reuseMat
        !! flag to reuse matrix

        integer :: nn
        !! number of neighbours

        integer :: it
        !! number of iterations done in ksp

        integer :: fComp
        !! fied components

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        !Allocate memory for A and rhs
        if(.not. this%isAllocated) call this%allocateMatrices()

        call MatZeroEntries(this%A, ierr)

        do iElement=1,numberOfElements

            ! Diagonal value, PETSC counts from 0
            call MatSetValue(this%A, mesh%cellGlobalAddr(iElement)-1, mesh%cellGlobalAddr(iElement)-1, &
                             this%aC(iElement,iComp), INSERT_VALUES, ierr)

            ! Neighbours values
            nn = mesh%numberOfNeighbours(iElement)
            call MatSetValues(this%A, 1, mesh%cellGlobalAddr(iElement)-1, nn, mesh%conn(iElement)%col(1:nn)-1, &
                              this%anb(iElement)%coeffs(1:nn,iComp), INSERT_VALUES, ierr)

        enddo

        call MatAssemblyBegin(this%A, MAT_FINAL_ASSEMBLY, ierr)
        call MatAssemblyEnd(this%A, MAT_FINAL_ASSEMBLY, ierr)

    end subroutine assembleMat

!**********************************************************************************************************************

    subroutine assembleRhs(this, iComp)

    !==================================================================================================================
    ! Description:
    !! assembleRhs assembles the ith-component of equation rhs.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, iComp, ierr

        integer :: ii(numberOfElements)
    !------------------------------------------------------------------------------------------------------------------

        call VecZeroEntries(this%rhs,ierr)

        ! PETSC counts from 0
        do i=1,numberOfElements
            ii(i) = mesh%cellGlobalAddr(i)-1
        enddo

        call VecSetValues(this%rhs, numberOfElements, ii, this%bC(:, iComp), INSERT_VALUES, ierr)

        call VecAssemblyBegin(this%rhs, ierr)
        call VecAssemblyEnd(this%rhs, ierr)

    end subroutine assembleRhs

!**********************************************************************************************************************

    subroutine setReferenceValue(this, iElement, value, iComp)

    !==================================================================================================================
    ! Description:
    !! setReferenceValue sets the value of field at target local cell number.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iComp
    !------------------------------------------------------------------------------------------------------------------

        real :: aC, bC, aC0, value
    !------------------------------------------------------------------------------------------------------------------

        aC0 = this%aC(iElement, iComp)
        this%bC(iElement, iComp) = this%bC(iElement, iComp) + aC0*value
        this%aC(iElement, iComp) = this%aC(iElement, iComp) + aC0

    end subroutine setReferenceValue

! *********************************************************************************************************************

    subroutine setMatReferenceValue(this, iG, val, iComp)

    !==================================================================================================================
    ! Description:
    !! setMatReferenceValue sets the value of field at target GLOBAL cell number.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iG
        !!target element

        integer :: iComp
        !! target component

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        real :: aC
        !! cell diagonal coefficient

        real :: bC
        !! cell rhs

        real :: val
        !! value to assign
    !------------------------------------------------------------------------------------------------------------------

        bC = this%bC(iG, iComp) + this%aC(iG, iComp)*val
        aC = 2.0*this%aC(iG, iComp)

        call MatSetValue(this%A, iG, iG, aC, INSERT_VALUES, ierr)
        call MatAssemblyBegin(this%A, MAT_FINAL_ASSEMBLY, ierr)
        call MatAssemblyEnd(this%A, MAT_FINAL_ASSEMBLY, ierr)

        call VecSetValue(this%rhs, iG, bC, INSERT_VALUES, ierr)
        call VecAssemblyBegin(this%rhs, ierr)
        call VecAssemblyEnd(this%rhs, ierr)

    end subroutine setMatReferenceValue

! *********************************************************************************************************************

    subroutine scaledResidual2(this, field, iComp, ref)

    !==================================================================================================================
    ! Description:
    !! scaledResidual scales the equation residual according to the following formula:
    !! \begin{equation*}
    !!    res= \frac{|\sum_{cells} A\phi - rhs|}{|\sum_{cells} a_C\phi_C|}
    !! \end{equation*}
    !! This normalization does not lead at a unitary scaled residual, but follows the literature.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, iComp, ierr

        integer :: ii(numberOfElements)

    !------------------------------------------------------------------------------------------------------------------

        real :: ref, small, fieldMean(field%nComp)

        real :: n1, n2

        real :: dummy(numberOfElements)
    !------------------------------------------------------------------------------------------------------------------

        Vec ax, axBar, resBar, x, xBar, ax_axBar, b
    !------------------------------------------------------------------------------------------------------------------

        small = 1e-10

        call meanField(field, fieldMean)

        call VecDuplicate(this%rhs, x, ierr)
        call VecDuplicate(this%rhs, xBar, ierr)
        call VecDuplicate(this%rhs, ax, ierr)
        call VecDuplicate(this%rhs, axBar, ierr)
        call VecDuplicate(this%rhs, resBar, ierr)
        call VecDuplicate(this%rhs, ax_axBar, ierr)
        call VecDuplicate(this%rhs, b, ierr)

        ! PETSC counts from 0
        do i=1,numberOfElements
            ii(i) = mesh%cellGlobalAddr(i)-1
        enddo

        ! b
        call VecZeroEntries(b,ierr)
        call VecSetValues(b, numberOfElements, ii, this%bC(1:numberOfElements,iComp), INSERT_VALUES, ierr)
        call VecAssemblyBegin(b, ierr)
        call VecAssemblyEnd(b, ierr)

        ! x
        call VecZeroEntries(x,ierr)
        call VecSetValues(x, numberOfElements, ii, field%phi(1:numberOfElements,iComp),INSERT_VALUES,ierr)
        call VecAssemblyBegin(x, ierr)
        call VecAssemblyEnd(x, ierr)

        ! xBar
        dummy = fieldMean(iComp)

        call VecZeroEntries(xbar,ierr)
        call VecSetValues(xBar, numberOfElements, ii, dummy, INSERT_VALUES, ierr)
        call VecAssemblyBegin(xbar, ierr)
        call VecAssemblyEnd(xbar, ierr)

        ! Ax
        call MatMult(this%A, x, ax, ierr)

        !AxBar
        call MatMult(this%A, xBar, axBar, ierr)

        !resBar
        call VecAXPY(b, -1.0, axBar, ierr)
        call VecAXPY(ax, -1.0, axBar, ierr)

        ! norms
        call VecNorm(b, NORM_1, n1, ierr)
        call VecNorm(ax, NORM_1, n2, ierr)

        ! scale the residual
        ref = n1 + n2
        this%res(iComp) = this%res(iComp)/(ref + small)

        ! destroy all the working vectors
        call VecDestroy(x, ierr)
        call VecDestroy(xBar, ierr)
        call VecDestroy(ax, ierr)
        call VecDestroy(axBar, ierr)
        call VecDestroy(resBar, ierr)
        call VecDestroy(ax_axBar, ierr)
        call VecDestroy(b, ierr)

    end subroutine scaledResidual2

! *********************************************************************************************************************

    function initialResidualPETSc(this, field, iComp) result(residual)

    !==================================================================================================================
    ! Description:
    !! initialResidualPETSc computes the initial unscaled residual |Ax-b|, using Mat A, Vec x, Vec rhs.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field
        !! field to solve for
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !! target component

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        real :: residual
        !! residual
    !------------------------------------------------------------------------------------------------------------------

        Vec x
        !! vector storing the actaul solution

        Vec vres
        !! vector storing the residual for each cell
    !------------------------------------------------------------------------------------------------------------------

        ! Duplicate vector
        call VecDuplicate(this%rhs, x, ierr)
        call VecZeroEntries(x, ierr)

        call VecDuplicate(this%rhs, vres, ierr)

        ! Set the current solution
        call VecSetValues(x, numberOfElements, mesh%cellGlobalAddr-1, field%phi(1:numberOfElements,iComp), INSERT_VALUES, ierr)
        call VecAssemblyBegin(x, ierr)
        call VecAssemblyEnd(x, ierr)

        ! Get initial residual norm
        call MatMult(this%A, x, vres, ierr)
        call VecAXPY(vres, -1.0, this%rhs, ierr)
        call VecNorm(vres, NORM_1, residual, ierr)

        ! Clean
        call VecDestroy(x, ierr)
        call VecDestroy(vres,ierr)

    end function initialResidualPETSc

!**********************************************************************************************************************

    function initialResidualFLUBIO(this, field, iComp) result(residual)

    !==================================================================================================================
    ! Description:
    !! initialResidualFLUBIO computes the the initial unscaled residual |Ax-b|, using  aC, anb, field%phi, bC.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field
        !! field to solve for

        type(flubioList) :: neighbourValues(numberOfElements)
        !! neighbour values
    !------------------------------------------------------------------------------------------------------------------

        integer :: n
        !! loop index

        integer :: iElement
        !! target cell

        integer :: iComp
        !! target componet

        integer :: nn
        !! number of neighbours

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        real :: localResidual
        !! local residual

        real residual
        !! residual

        real :: neighbourValue
        !! neighbour value

        real :: Ax(numberOfElements)
        !! A*x
    !------------------------------------------------------------------------------------------------------------------

        ! Get neighbour values
        neighbourValues = field%getFieldNeighbours()

        Ax = 0.0
        do iElement=1,numberOfElements

            nn = mesh%numberOfNeighbours(iElement)

            ! Off-diagonal contribution
            do n=1,nn
                neighbourValue = neighbourValues(iElement)%getAt(targetRow=n, targetColumn=iComp, isReal=1.0)
                Ax(iElement) = Ax(iElement) + this%anb(iElement)%coeffs(n,iComp)*neighbourValue
            enddo

            ! Add the diagonal contribution
            Ax(iElement) = Ax(iElement) + this%aC(iElement, iComp)*field%phi(iElement,iComp)

       enddo

       ! Compute residual and scaling factor
       localResidual = norm1(Ax-this%bC(1:numberOfElements, iComp), numberOfElements)

       ! Sum up contributions
       call mpi_Allreduce(localResidual, residual, 1, MPI_REAL8, MPI_SUM, MPI_COMM_WORLD, ierr)

    end function initialResidualFLUBIO

!**********************************************************************************************************************

    function scaledResidualPETSc(this, field, iComp, scalingType) result(scalingFactor)

    !==================================================================================================================
    ! Description:
    !! scaledResidualPETSc computes the residual scaling factor.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        character(len = *) :: scalingType
    !------------------------------------------------------------------------------------------------------------------

        integer :: ierr
        !! error flag

        integer :: iComp
        !! target component
    !------------------------------------------------------------------------------------------------------------------

        real :: localScalingFactor
        !! local scaling factor

        real :: scalingFactor
        !! global scaling factor

        real :: small, fieldMean(field%nComp)

        real :: n1, n2

        real :: dummy(numberOfElements)
    !------------------------------------------------------------------------------------------------------------------

        Vec ax, axBar, resBar, x, xBar, ax_axBar, b
    !------------------------------------------------------------------------------------------------------------------

        small = 1e-10

        ! Max among the first 5 iterations
        if(scalingType=='rmax') then

            !scalingFactor = maxval(this%res5) ! add this member data

        ! Scale using diagonal values
        elseif(scalingType=='diagonal') then

            ! |aC*phi|
            localScalingFactor = sum(abs(this%aC(1:numberOfElements,iComp)*field%phi(1:numberOfElements,iComp)))

            ! Sum up contributions
            call mpi_Allreduce(localScalingFactor, scalingFactor, 1, MPI_REAL8, MPI_SUM, MPI_COMM_WORLD, ierr)

            scalingFactor = scalingFactor + small

        ! OpenFOAM way
        else

            call meanField(field, fieldMean)

            call VecDuplicate(this%rhs, x, ierr)
            call VecDuplicate(this%rhs, xBar, ierr)
            call VecDuplicate(this%rhs, ax, ierr)
            call VecDuplicate(this%rhs, axBar, ierr)
            call VecDuplicate(this%rhs, resBar, ierr)
            call VecDuplicate(this%rhs, ax_axBar, ierr)
            call VecDuplicate(this%rhs, b, ierr)

            ! b
            call VecZeroEntries(b,ierr)
            call VecSetValues(b, numberOfElements, mesh%cellGlobalAddr-1, this%bC(1:numberOfElements,iComp), INSERT_VALUES, ierr)
            call VecAssemblyBegin(b, ierr)
            call VecAssemblyEnd(b, ierr)

            ! x
            call VecZeroEntries(x,ierr)
            call VecSetValues(x, numberOfElements, mesh%cellGlobalAddr-1, field%phi(1:numberOfElements,iComp),INSERT_VALUES,ierr)
            call VecAssemblyBegin(x, ierr)
            call VecAssemblyEnd(x, ierr)

            ! xBar
            dummy = fieldMean(iComp)

            call VecZeroEntries(xbar,ierr)
            call VecSetValues(xBar, numberOfElements, mesh%cellGlobalAddr-1, dummy, INSERT_VALUES, ierr)
            call VecAssemblyBegin(xbar, ierr)
            call VecAssemblyEnd(xbar, ierr)

            ! Ax
            call MatMult(this%A, x, ax, ierr)

            !AxBar
            call MatMult(this%A, xBar, axBar, ierr)

            !resBar
            call VecAXPY(b, -1.0, axBar, ierr)
            call VecAXPY(ax, -1.0, axBar, ierr)

            ! norms
            call VecNorm(b, NORM_1, n1, ierr)
            call VecNorm(ax, NORM_1, n2, ierr)

            ! scale the residual
            scalingFactor = n1 + n2 + small

            ! destroy all the working vectors
            call VecDestroy(x, ierr)
            call VecDestroy(xBar, ierr)
            call VecDestroy(ax, ierr)
            call VecDestroy(axBar, ierr)
            call VecDestroy(resBar, ierr)
            call VecDestroy(ax_axBar, ierr)
            call VecDestroy(b, ierr)

        endif

    end function scaledResidualPETSc

!**********************************************************************************************************************

    function scaledResidualFLUBIO(this, field, iComp, scalingType) result(scalingFactor)

    !==================================================================================================================
    ! Description:
    !! scaledResidualFLUBIO computes the residual scaling factor.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field
        !! field to solve for

        type(flubioList) ::  neighbourValues(numberOfElements)
        !! neighbour values
    !------------------------------------------------------------------------------------------------------------------

        character(len = *) :: scalingType
    !------------------------------------------------------------------------------------------------------------------

        integer :: ierr
        !! error flag

        integer :: iElement
        !! target cell

        integer :: iComp
        !! target component

        integer :: n
        !! loop index

        integer :: nn
        !! number of neighbours
    !------------------------------------------------------------------------------------------------------------------

        real :: small
        !! small value

        real :: localScalingFactor
        !! local scaling factor

        real :: scalingFactor
        !! global scaling factor

        real :: n1, n2
        !! woring varaibles

        real :: Ax(numberOfElements)
        !! A*x

        real :: Axbar(numberOfElements)
        !! a*fieldMean

        real :: fieldMean(field%nComp)
        !! Mean field

        real :: neighbourValue
        !! neighbour value
    !------------------------------------------------------------------------------------------------------------------

        if(scalingType=='rmax') then

            !scalingFactor = maxval(this%res5) ! add this member data

        elseif(scalingType=='diagonal') then

            ! |aC*phi|
            localScalingFactor = sum(abs(this%aC(1:numberOfElements,iComp)*field%phi(1:numberOfElements,iComp)))

            ! Sum up contributions
            call mpi_Allreduce(localScalingFactor, scalingFactor, 1, MPI_REAL8, MPI_SUM, MPI_COMM_WORLD, ierr)

        else

            small = 1e-12

            call meanField(field, fieldMean)
            neighbourValues = field%getFieldNeighbours()

            Ax = 0.0
            Axbar = 0

            do iElement=1,numberOfElements

                nn = mesh%numberOfNeighbours(iElement)

                ! Off-diagonal contribution
                do n=1,nn
                    neighbourValue = neighbourValues(iElement)%getAt(targetRow=n, targetColumn=iComp, isReal=1.0)
                    Ax(iElement) = Ax(iElement) + this%anb(iElement)%coeffs(n,iComp)*neighbourValue
                    Axbar(iElement) = Axbar(iElement) + this%anb(iElement)%coeffs(n, iComp)*fieldMean(iComp)
                enddo

                ! Add the diagoan contribution
                Ax(iElement) = Ax(iElement) + this%aC(iElement, iComp)*field%phi(iElement,iComp)
                Axbar(iElement) = Axbar(iElement) + this%aC(iElement, iComp)*fieldMean(iComp)

           enddo

           ! Compute residual and scaling factor
           n1 = norm1(Ax-Axbar, numberOfElements)
           n2 = norm1(Axbar-this%bC(1:numberOfElements, iComp), numberOfElements)
           localScalingFactor = n1 + n2 + small

           ! Sum up contributions
           call mpi_Allreduce(localScalingFactor, scalingFactor, 1, MPI_REAL8, MPI_SUM, MPI_COMM_WORLD, ierr)

        endif

    end function scaledResidualFLUBIO

!**********************************************************************************************************************

    function getNorm(this, x) result(initResidual)

    !==================================================================================================================
    ! Description:
    !! getNorm scomputes the residual norm of the linear system.
    !==================================================================================================================

        class(transportEqn) :: this

        integer :: ierr
    !------------------------------------------------------------------------------------------------------------------

        real :: initResidual
        !! Residual Norm !Ax-b|

        Vec x
        !! Field values vector

        Vec vres
        !! Working vecotr
    !------------------------------------------------------------------------------------------------------------------

        call VecDuplicate(this%rhs, vres, ierr)

        ! Get initial residual norm
        call MatMult(this%A, x, vres, ierr)
        call VecAXPY(vres, -1.0, this%rhs, ierr)
        call VecNorm(vres, NORM_1, initResidual, ierr)

        ! Clean up
        call VecDestroy(vres, ierr)

    end function getNorm

!**********************************************************************************************************************

    subroutine writeResidual(this)

    !==================================================================================================================
    ! Description:
    !! writeresiduals writes the residual in a data file ready to plot.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: header
    !------------------------------------------------------------------------------------------------------------------

        real :: timeElapsed
        !! time Elapsed per iteration
    !------------------------------------------------------------------------------------------------------------------

        ! File Header
        timeElapsed = telap(2)-telap(1)

        if(this%nComp==1) then
            header = '# Iteration      ScaledResidual      UnscaledResidual'
        elseif(this%nComp==3) then
            header = '# Iteration      ScaledResidual-x    ScaledResidual-y      ScaledResidual-z      &
                      UnscaledResidual-x    UnscaledResidual-y      UnscaledResidual-z'
        elseif(this%nComp==3 .and. bdim==1) then
            header = '# Iteration      ScaledResidual-x    ScaledResidual-y      UnscaledResidual-x    UnscaledResidual-y'
        end if

        !-----------------!
        ! Write residuals !
        !-----------------!

        if(id==0 .and. flubioControls%saveResiduals==1 .and. mod(itime,flubioControls%resOutputFrequency)==0) then

            if(itime<=1) then

                open(1,file='postProc/monitors/res-'//trim(this%eqName)//'.dat',status='replace')
                    write(1,*) header
                    write(1,*) itime, this%convergenceResidual(:)/(this%scalingRef(:) + 1e-10), this%convergenceResidual(:)
                close(1)
            else
                open(1,file='postProc/monitors/res-'//trim(this%eqName)//'.dat',access='append')
                    write(1,*) itime, this%convergenceResidual(:)/(this%scalingRef(:) + 1e-10), this%convergenceResidual(:)
                close(1)

            endif

        endif

    end subroutine writeResidual

! *********************************************************************************************************************

    subroutine verbose(this, field)

    !==================================================================================================================
    ! Description:
    !! verbose prints at screen some information related to the simulation. You can custumize the output as you retain
    ! more appropriate.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: str, final

        character(len=30) :: min
        !! minum field value as string

        character(len=30) :: max
        !! maximum field value as string

        integer :: iComp
        !! target component

        integer :: fcomp
        !! field components
    !------------------------------------------------------------------------------------------------------------------

        real, dimension(:), allocatable :: minField
        !! minimum value of the field in each processor domain

        real, dimension(:), allocatable :: maxField
        !! maxium value of the field in each processor domain

        real, dimension(:,:), allocatable :: minCellCenter
        !! cell center of the min cell center

        real, dimension(:,:), allocatable  :: maxCellCenter
        !! cell center of the max cell center
    !------------------------------------------------------------------------------------------------------------------

        if(flubioControls%verbosityLevel==1) call minMaxField(field, minField, maxField, minCellCenter, maxCellCenter)

        if(id==0 .and. flubioControls%verbosityLevel==1) then

            fcomp = field%nComp
            if(fcomp>1) fcomp = fcomp-bdim

            do iComp=1,fcomp
                write(min,'(ES12.5)') minField(iComp)
                write(max,'(ES12.5)') maxField(iComp)
                str = 'min('//trim(field%fieldName(iComp))//'), max('//trim(field%fieldName(iComp))//'): '
                final = ' ' // str //' ('//trim(min) // ', ' //trim(adjustl(trim(max)))//')'
                write(*,*) final
            end do

        endif

    end subroutine verbose

!**********************************************************************************************************************

    subroutine kspVerbose(this, fieldName, it, iCorr, initResidual, res, scalingFactor)

    !==================================================================================================================
    ! Description:
    !! kspVerbose prints at screen some information related to the ksp solve.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len = *) fieldName

        character(len=:), allocatable :: str1, str2, final, optName

        character(len=20) :: string1, string2, string3

        character(len=30) :: itStr, initResidualStr, finalResStr
    !------------------------------------------------------------------------------------------------------------------

        integer :: it, iCorr, ierr, outputMatrixEvery, outputCorrEvery
    !------------------------------------------------------------------------------------------------------------------

        real :: initResidual, res, scalingFactor, small
    !------------------------------------------------------------------------------------------------------------------

        PetscBool set, setOutputFreq, setCorrOutputFreq
    !------------------------------------------------------------------------------------------------------------------

        small = 1e-10

        if(id==0) then

            write(itStr,'(i0)') it
            if(lowercase(flubioSolvers%scalingType)=='yes' .or. lowercase(flubioSolvers%scalingType)=='on') then
                write(initResidualStr,'(ES12.5)') initResidual/(scalingFactor + small)
                write(finalResStr,'(ES12.5)') res/(scalingFactor + small)
            else
                write(initResidualStr,'(ES12.5)') initResidual
                write(finalResStr,'(ES12.5)') res
            end if

            str1 = ' ' //  trim(fieldName) // ' converged in '// adjustl(trim(itStr))
            str2 = ' iterations, initial norm: ' // trim(adjustl(trim(initResidualStr))) // ', final norm: ' // adjustl(trim(finalResStr))
            final = str1 // str2
            write(*,*) final

        endif

        outputMatrixEvery = 1
        optName = '-'//this%eqName//'_'//'save_linear_system_every'
        call PetscOptionsGetString(PETSC_NULL_OPTIONS,PETSC_NULL_CHARACTER, optName, string1, setOutputFreq, ierr)
        if(setOutputFreq) read(string1,*) outputMatrixEvery

        if((mod(itime, outputMatrixEvery)==0 .or. outputMatrixEvery==1) .and. setOutputFreq) then
            outputCorrEvery = 1
            optName = '-'//this%eqName//'_'//'save_linear_system_ncorr_every'
            call PetscOptionsGetString(PETSC_NULL_OPTIONS,PETSC_NULL_CHARACTER, optName, string3, setCorrOutputFreq, ierr)
            if(setCorrOutputFreq) read(string3,*) outputCorrEvery

            if(mod(iCorr, outputCorrEvery)==1 .or. outputCorrEvery==1) then
                ! Print matrix if required
                optName = '-'//this%eqName//'_'//'save_linear_system_ncorr_every'
                call PetscOptionsGetString(PETSC_NULL_OPTIONS,PETSC_NULL_CHARACTER, optName, string2, set, ierr)
                call this%exportMatrixToFile(iCorr)
            endif

        end if

    end subroutine kspVerbose

!**********************************************************************************************************************

    subroutine amgclVerbose(this, fieldName, it, initResidual, res, scalingFactor)

    !==================================================================================================================
    ! Description:
    !! amgclVerbose prints at screen some information related to the amgcl solve.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len = *) fieldName

        character(len=:), allocatable :: str1, str2, final, optName

        character(len=20) :: string1, string2, string3

        character(len=30) :: itStr, initResidualStr, finalResStr
    !------------------------------------------------------------------------------------------------------------------

        integer :: it, ierr
    !------------------------------------------------------------------------------------------------------------------

        real :: initResidual, res, scalingFactor, small
    !------------------------------------------------------------------------------------------------------------------

        small = 1e-10

        if(id==0) then

            write(itStr,'(i0)') it
            if(lowercase(flubioSolvers%scalingType)=='yes' .or. lowercase(flubioSolvers%scalingType)=='on') then
                    write(initResidualStr,'(ES12.5)') initResidual/(scalingFactor + small)
                    write(finalResStr,'(ES12.5)') res/(scalingFactor + small)
            else
                    write(initResidualStr,'(ES12.5)') initResidual
                    write(finalResStr,'(ES12.5)') res
            end if

            str1 = ' ' //  trim(fieldName) // ' converged in '// adjustl(trim(itStr))
            str2 = ' iterations, initial norm: ' // trim(adjustl(trim(initResidualStr))) // ', final norm: ' // adjustl(trim(finalResStr))
            final = str1 // str2
            write(*,*) final

        endif

    end subroutine amgclVerbose

!**********************************************************************************************************************

    subroutine exportMatrixToFile(this, iCorr)

    !==================================================================================================================
    ! Description:
    !! exportMatrixToFile save the matrix to file.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=20) :: iter
        !! current iteration as string

        character(len=20) ::nonOrthCorr
        !!  non orthogonal correction number as string

        character(len=20) :: viewerFormat
        !! viewer format

        character(len=:), allocatable :: fileName
        !! file name

        character(len=:), allocatable ::  viewerType
        !! viewer type
    !------------------------------------------------------------------------------------------------------------------

        integer :: iCorr
        !! target correction

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        PetscBool set
        !! flag to chek if an option is set

        PetscViewer matrixViewer
        !! PETSc viewr for the matrix

        PetscViewer rhsViewer
         !! PETSc viewr for the rhs
    !------------------------------------------------------------------------------------------------------------------

        ! Find out the viewer to use
        viewerType = '-'//this%eqName//'_'//'viewer_type'
        call PetscOptionsGetString(PETSC_NULL_OPTIONS,PETSC_NULL_CHARACTER, viewerType, viewerFormat, set, ierr)

        write(iter,'(i0)') itime
        write(nonOrthCorr,'(i0)') iCorr

        if(lowercase(trim(viewerFormat))=='ascii') then
            fileName = this%eqName//'_Matrix_it_'//trim(iter)//'_nOrthCorr_'//trim(nonOrthCorr)//'.dat'
            call PetscViewerASCIIOpen(PETSC_COMM_WORLD, fileName, matrixViewer, ierr)
            call MatView(this%A, matrixViewer,ierr)

            fileName = this%eqName//'_rhs_it_'//trim(iter)//'_nOrthCorr_'//trim(nonOrthCorr)//'.dat'
            call PetscViewerASCIIOpen(PETSC_COMM_WORLD, fileName, rhsViewer, ierr)
            call VecView(this%rhs, rhsViewer,ierr)
        else
            fileName = this%eqName//'_Matrix_it_'//trim(iter)//'.bin'
            call PetscViewerBinaryOpen(PETSC_COMM_WORLD, fileName, FILE_MODE_WRITE, matrixViewer, ierr)
            call MatView(this%A, matrixViewer,ierr)

            fileName = this%eqName//'_rhs_it_'//trim(iter)//'_nOrthCorr_'//trim(nonOrthCorr)//'.bin'
            call PetscViewerBinaryOpen(PETSC_COMM_WORLD, fileName, FILE_MODE_WRITE , rhsViewer, ierr)
            call VecView(this%rhs, rhsViewer,ierr)
        end if

        ! clean up
        call  PetscViewerDestroy(matrixViewer, ierr)
        call  PetscViewerDestroy(rhsViewer, ierr)

    end subroutine exportMatrixToFile

!**********************************************************************************************************************

    subroutine printResidual(this)

    !==================================================================================================================
    ! Description:
    !! printresidual prints the equation residual.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(:), allocatable :: str
        !! string to set a sentence

        character(:), allocatable :: res_str
        !! final residual as string

        character(:), allocatable :: final
        !! fianl string to print

        character(len=30) :: res(3)
        !! residual as string
    !------------------------------------------------------------------------------------------------------------------

        integer :: fComp
        !! field components
    !------------------------------------------------------------------------------------------------------------------

        fComp = this%nComp
        if(fComp>1) fComp = fComp - bdim

        if(fComp==1) then
            write(res(1),'(ES13.6)') this%convergenceResidual(1)/(this%scalingRef(1) + 1e-10)
            res_str = trim(res(1))
        elseif(fComp==2) then
            write(res(1),'(ES13.6)') this%convergenceResidual(1)/(this%scalingRef(1) + 1e-10)
            write(res(2),'(ES13.6)') this%convergenceResidual(2)/(this%scalingRef(2) + 1e-10)
            res_str = trim(res(1)) //' '// res(2)
        else
            write(res(1),'(ES13.6)') this%convergenceResidual(1)/(this%scalingRef(1) + 1e-10)
            write(res(2),'(ES13.6)') this%convergenceResidual(2)/(this%scalingRef(2) + 1e-10)
            write(res(3),'(ES13.6)') this%convergenceResidual(3)/(this%scalingRef(3) + 1e-10)
            res_str = trim(res(1)) //' '// trim(res(2)) //' '// trim(res(3))
        end if

        if(id==0) then
            str = ' '//trim(this%eqName)//' initial residual: '
            final = str // res_str
            write(*,*) final
        endif

    end subroutine printResidual

!**********************************************************************************************************************

    subroutine printDelimiter(this)

    !==================================================================================================================
    ! Description:
    !! printDelimiter prints a delimiter with time information.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=80) :: FMT
        !! format
    !------------------------------------------------------------------------------------------------------------------

        if(id==0) then

            write(*,*)
            FMT = "(A16, ES12.5, A8)"
            write(*,FMT) ' Time elapsed: ', telap(2)-telap(1), ' seconds'
            write(*,*) '***************************************************************************************************'
            write(*,*)

            FMT = "(A14, ES13.6, A1, A19, i0)"

            if(steadySim==0 .and. time<tmax) write(*,FMT) ' Time = ', time, ',' ,' Time-step number:   ', itime

            write(*,*)

        end if

    end subroutine printDelimiter

!**********************************************************************************************************************

    subroutine printCurrentIteration(this)

    !==================================================================================================================
    ! Description:
    !! printCurrentIteration prints the current iteration.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=10) :: itimeStr
        !! current iteration as string

        character(len=:), allocatable :: final
        !! fianl string to print
    !------------------------------------------------------------------------------------------------------------------

        write(itimeStr,'(i0)') itime

        if(id==0) then
            final = ' Iteration number: ' // adjustl(trim(itimeStr))
            if(steadySim==1) write(*,*) final
            write(*,*)
        end if

    end subroutine printCurrentIteration

!**********************************************************************************************************************

    subroutine checkSteadyState(this)

    !==================================================================================================================
    ! Description:
    !! checkSteadyState checks if the simulation is steady.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: optName
        !! option name

        character(len=:), allocatable :: opt
        !! parsed option from the dictionary
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
        !! flag
    !------------------------------------------------------------------------------------------------------------------

        ! check default option
        call flubioOptions%json%get('Default%TimeScheme', opt, found)
        if(.not. found) opt = 'none'

        if(lowercase(opt)=='steady') then
            steadySim=1
        else
            steadySim=0
        end if

        ! Check equation option, if default is not present
        if(.not. found) then

            call flubioOptions%json%get(this%eqName//'%TimeScheme', opt, found)
            if(.not. found) opt = 'none'

            if(lowercase(opt)=='steady') then
                steadySim=1
            else
                steadySim=0
            end if

        end if

    end subroutine checkSteadyState

! *********************************************************************************************************************

    subroutine checkConvergence(this)

    !==================================================================================================================
    ! Description:
    !! checkConvergence checks if the convergence criteria are satified or if the simulation has exceeded
    !! the maximum number of iterations.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        !-------------------!
        ! Check Convergence !
        !-------------------!

        if(itime>=nint(tmax)) then

            flubioControls%convergence=1

            call flubioMsg('Equation has reached the maxium number of Iterations, simulation will be stopped at the end of the current iteration...')

        elseif(steadySim==0) then

            flubioControls%convergence=1

        elseif(this%convergenceResidual(1)/this%scalingRef(1)<=this%resmax(1)) then

            flubioControls%convergence=1

            if(id==0) then
                call flubioMsg('Equation has CONVERGED!')
                write(*,*) 'Residual for '// trim(this%eqName) // ' is: ', this%convergenceResidual/this%scalingRef
                write(*,*)
            endif

        endif

    end subroutine checkConvergence

! *********************************************************************************************************************

    subroutine getResidualForConvergenceCheck(this)

    !==================================================================================================================
    ! Description:
    !! getResidualForConvergenceCheck store the residual after the first non-orthogonal correction
    !! and uses it to check the equation convergence.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        this%convergenceResidual = this%res

    end subroutine getResidualForConvergenceCheck

! *********************************************************************************************************************

    subroutine viewKspSettings(this)

    !==================================================================================================================
    ! Description:
    !! viewKspSettings print the ksp settings to the screen.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        call KSPview(this%ksp, PETSC_VIEWER_STDOUT_WORLD, ierr)

    end subroutine viewKspSettings

! *********************************************************************************************************************

    subroutine setSolverOptions(this)

    !==================================================================================================================
    ! Description:
    !! setSolverOptions parse options from the lsolver dictionary and set the as petsc options.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        TYPE(JSON_CORE) :: jcore

        type(JSON_VALUE), pointer :: objPointer, dictPointer, subdict_pointer, keyPointer
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictValue, path, optionPath, petscOpt

        character(len=50), dimension(:), allocatable  :: splitPath
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, opt_size, ierr
    !------------------------------------------------------------------------------------------------------------------

        logical :: found, keyFound, found_subdict
    !------------------------------------------------------------------------------------------------------------------

        call jcore%initialize()
        call  flubioSolvers%json%get(dictPointer)
        call jcore%get_child(dictPointer, this%eqName, objPointer, found)

        if(found) call jcore%get_child(objPointer, 'options', subdict_pointer, found_subdict)

        if(found_subdict) then

            optionPath = trim(this%eqName)//'%options'
            call flubioSolvers%json%info(optionPath, n_children=opt_size)

            do i = 1, opt_size

                call jcore%get_child(subdict_pointer, i, keyPointer, keyFound)

                if(keyFound)then

                    call jcore%get_path(keyPointer, path, keyFound)
                    call jCore%get(keyPointer, dictValue)

                    call split_in_array(path, splitPath, '.')
                    petscOpt = "-"//trim(this%eqName)//'_'//trim(splitPath(size(splitPath)))
                    call PetscOptionsSetValue(PETSC_NULL_OPTIONS, petscOpt, dictValue, ierr)

                else
                    call flubioStopMsg("ERROR: cannot parse option correctly. Please check your json dictionary.")
                endif

            end do

        end if

        ! Clean up
        nullify(dictPointer)
        if(found_subdict) nullify(objPointer)
        if(keyFound) nullify(keyPointer)
        call jcore%destroy()

    end subroutine setSolverOptions

! *********************************************************************************************************************

    function H(this, field) result(H_)

    !==================================================================================================================
    ! Description:
    !! H returns the H operator, commonly in SIMPLE algorithm description, i.e. :
    !! \begin{equation}
    !!    H = [b_C -\sum a_{nb}*\phi_{anb}]/a_C
    !!  \end{equation}
    !==================================================================================================================

#include "petsc/finclude/petscksp.h"
        use petscksp

        class(transportEqn) :: this

        type(flubioField) :: field
        !! field to use to compute H
    !------------------------------------------------------------------------------------------------------------------

        Vec y
        !! auxiliary vector
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
        !! target element

        integer :: iComp
        !! target compoent

        integer :: setGuess
        !! flag to set the inital guess

        integer :: reusePC
        !! falg to reuse the preconditioner

        integer ::reuseMat
        !! flag to reuse matrix

        integer :: nn
        !! number of neighbours

        integer :: it
        !! number of iterations done in ksp

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        real :: H_(numberOfElements, this%nComp), sumAnb(numberOfElements,1)

        real :: val
        !! auxiliary varialbe to stoe the cell center rhs

        real :: gradP
        !! auxiliary varialbe to stoe the cell center pressure gradient
    !------------------------------------------------------------------------------------------------------------------

        H_ = 0.0

        do iComp=1,this%nComp

            !---------------------!
            ! Assemble the matrix !
            !---------------------!

            call MatZeroEntries(this%A, ierr)
            call VecZeroEntries(this%rhs, ierr)
            sumAnb = 0.0

            do iElement=1,numberOfelements

                ! Diagonal value
                call MatSetValue(this%A, mesh%cellGlobalAddr(iElement)-1, mesh%cellGlobalAddr(iElement)-1, &
                                 this%aC(iElement,iComp), INSERT_VALUES, ierr)

                ! Neighbours values
                nn = mesh%numberOfNeighbours(iElement)
                call MatSetValues(this%A, 1, mesh%cellGlobalAddr(iElement)-1, nn, mesh%conn(iElement)%col(1:nn)-1, &
                                  this%anb(iElement)%coeffs(1:nn,iComp),INSERT_VALUES,ierr)

            enddo

            call MatAssemblyBegin(this%A, MAT_FINAL_ASSEMBLY, ierr)
            call MatAssemblyEnd(this%A, MAT_FINAL_ASSEMBLY, ierr)

            !-------------------------------------!
            ! Fill a vector with the equation rhs !
            !-------------------------------------!

            ! duplicate the vector
            call VecDuplicate(this%rhs, y, ierr)

            call VecSetValues(this%rhs,numberOfElements, mesh%cellGlobalAddr-1, field%phi(1:numberOfElements,iComp), INSERT_VALUES, ierr)

            call VecAssemblyBegin(this%rhs, ierr)
            call VecAssemblyEnd(this%rhs, ierr)

            !------------------------------------------------------------------------!
            ! Matrix multiplication, remember to subtract aC(iElement)*phi(iElement) !
            !------------------------------------------------------------------------!

            call MatMult(this%A, this%rhs, y, ierr)
            call VecGetValues(y, flubioSolvers%lm, mesh%cellGlobalAddr-1, sumAnb(:,1), ierr)

            ! Subtract off the diagonal term and add the pressure gradient again
            do iElement=1,numberOfElements

                val = this%bC(iElement,iComp) -(sumAnb(iElement,1) &
                        -this%aC(iElement,iComp)*field%phi(iElement,iComp))

                H_(iElement,iComp) = val/this%aC(iElement,iComp)

            enddo

            ! Destroy PETSc object
            call VecDestroy(y, ierr)

        enddo

    end function H

! *********************************************************************************************************************

    function H1(this) result(H1_)

    !==================================================================================================================
    ! Description:
    !! H1 returns the H1 operator, commonly in SIMPLE algorithm description, i.e. :
    !! \begin{equation}
    !!    H_1 = \sum a_{nb}
    !!  \end{equation}
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
        !! target element

        integer :: iComp
        !! target component
    !------------------------------------------------------------------------------------------------------------------

        real :: H1_(numberOfElements, this%nComp)
        !! H1 to return
    !------------------------------------------------------------------------------------------------------------------

        H1_ = 0.0

        do iComp=1,this%nComp

            do iElement=1,numberOfelements
                H1_(iElement, iComp) = sum(this%anb(iElement)%coeffs(1:mesh%numberOfNeighbours(iElement), iComp))
            enddo

        enddo

    end function H1

! *********************************************************************************************************************

    function sumMagAnb(this) result(sumAnb)

    !==================================================================================================================
    ! Description:
    !! sumMagAnb returns the sum of the mag of off-diagona coefficients
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
        !! target element

        integer :: iComp
        !! target component
    !------------------------------------------------------------------------------------------------------------------

        real :: sumAnb(numberOfElements, this%nComp)
        !! sum of the magnitude of off-diagonal coefficients
    !------------------------------------------------------------------------------------------------------------------

        sumAnb = 0.0

        do iComp=1,this%nComp

            do iElement=1,numberOfelements
                sumAnb(iElement, iComp) = sum(abs(this%anb(iElement)%coeffs(1:mesh%numberOfNeighbours(iElement), iComp)))
            enddo

        enddo

    end function sumMagAnb

! *********************************************************************************************************************

    subroutine applyToSelectedRegions(aC, bC, iElement, sourceName, nComp)

    !==================================================================================================================
    ! Description:
    !! applyToSelectedRegions applies the source only to the selected regions, if they are defined
    !==================================================================================================================

        character(len=*) sourceName
        !! Name of the source

        character(len=:), dimension(:), allocatable :: regionNames
        !! region names
    !------------------------------------------------------------------------------------------------------------------

        integer iElement
        !! target element

        integer :: iRegion
        !! target region

        integer :: regionIndex
        !! region index

        integer :: nComp
        !! number of components

        integer, dimension(:),allocatable :: ilen
        !! first array element lenght
    !------------------------------------------------------------------------------------------------------------------

        real :: aC(nComp)
        !! diagonal coefficient

        real ::  bC(nComp)
        !! equation rhs

        real :: applySource
        !! value to understand if the source has to be applied in a cell or not
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
        !! flag
    !------------------------------------------------------------------------------------------------------------------

        ! Check if where the source is to be applied
        call flubioOptions%json%get('VolumeSources%'//sourceName//'%regions', regionNames, ilen, found)

        ! Filter only if you find the region where to do so, otherwise apply the source in the whole domain
        if(found) then

            ! everywhere will apply the source everything by skipping the check
            if(regionNames(1)=='everywhere') return

            ! sum up the sources position flag. If the sum is 0, then do not apply the source,
            ! while if it is bigger than 1.0 apply the source
            applySource = 0.0
            do iRegion=1,size(regionNames)
                regionIndex = mesh%getRegionIndexByName(adjustl(trim(regionNames(iRegion))))
                applySource = applySource + mesh%meshRegions(regionIndex)%phi(iElement,1)
            end do

            if(applySource < 0.5) then
                aC = 0.0
                bC = 0.0
            end if
        end if

    end subroutine applyToSelectedRegions

!**********************************************************************************************************************

    subroutine setEquationSources(this)

    !==================================================================================================================
    ! Description:
    !! setEquationSources fills the run time processing dictionary.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName, typeValue, sourceName, path
        !! First array element lenght

        character(len=50), dimension(:), allocatable  :: splitPath
    !------------------------------------------------------------------------------------------------------------------

        integer :: iSource, nSources
    !------------------------------------------------------------------------------------------------------------------

        logical :: keyFound, typeFound, sourceFound, sourcesFound, eqnFound, nameFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, sourcesPointer, eqnPointer, sourcePointer
    !------------------------------------------------------------------------------------------------------------------

        ! Get the pointer to the dictionary
        call flubioSources%json%get(dictPointer)

        call flubioSources%sourcesTable%get(this%eqName, nSources, keyFound)
        call raiseKeyErrorJSON(this%eqName, keyFound, 'settings/volumeSources')

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'volumeSources', sourcesPointer, sourcesFound)

        ! Get sources pointer
        call jcore%get_child(sourcesPointer, this%eqName, eqnPointer, eqnFound)

        ! Allocate the monitors array
        allocate(this%fvSources(nSources))

        do iSource=1,nSources

            ! Get monitors pointer
            call jcore%get_child(eqnPointer, iSource, sourcePointer, sourceFound)

            ! Get monitor name
            call jcore%get_path(sourcePointer, path, nameFound)
            if(nameFound) then
                call split_in_array(path, splitPath, '.')
                sourceName = splitPath(3)
            else
                call flubioStopMsg('ERROR: cannot find the source name, please check if the dictionary is correct.')
            end if

            ! Get monitor type
            call jcore%get(sourcePointer, 'type', typeValue, typeFound)

            if(typeFound) then

                if(lowercase(trim(typeValue))=='constantsource') then
                    allocate(constantSourceTerm::this%fvSources(iSource)%source)
                    this%fvSources(iSource)%source%sourceName = sourceName
                elseif(lowercase(trim(typeValue))=='semiimplicitsource') then
                    allocate(semiImplicitSourceTerm::this%fvSources(iSource)%source)
                    this%fvSources(iSource)%source%sourceName = sourceName
                elseif(lowercase(trim(typeValue))=='boussinesqsource') then
                    allocate(boussinesqSourceTerm::this%fvSources(iSource)%source)
                    this%fvSources(iSource)%source%sourceName = sourceName
                elseif(lowercase(trim(typeValue))=='bouyancysource') then
                    allocate(bouyancySourceTerm::this%fvSources(iSource)%source)
                    this%fvSources(iSource)%source%sourceName = sourceName
                elseif(lowercase(trim(typeValue))=='poroussource') then
                    allocate(porousSourceTerm::this%fvSources(iSource)%source)
                    this%fvSources(iSource)%source%sourceName = sourceName
                else
                    call flubioStopMsg('ERROR: unknown source type: '//trim(typeValue))
                end if
            else
                call flubioStopMsg("ERROR: cannot find the source type. Please either specify it or check the spelling.")
            end if

        enddo

        ! Initialise sources
        do iSource =1,nSources
            call this%fvSources(iSource)%source%set(trim(this%eqName))
        enddo

        ! Clean up
        nullify(dictPointer)
        if(sourcesFound) nullify(sourcesPointer)
        if(sourceFound) nullify(sourcePointer)
        if(eqnFound) nullify(eqnPointer)
        call jcore%destroy()

    end subroutine setEquationSources

!**********************************************************************************************************************

    subroutine convertToCSR(this, V, C, R, iComp)

    !==================================================================================================================
    ! Description:
    !! convertToCSR converts flubio coefficients to CSR format.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
        !! Target row

        integer is
        !! start index

        integer :: ie
        !! end index

        integer :: nn
        !! number of neighbours

        integer :: iComp
        !! Target component

        integer :: R(numberOfElements+1)
        !! CSR row array

        integer :: C(mesh%nnz)
        !! CSR column array
    !------------------------------------------------------------------------------------------------------------------

        real :: V(mesh%nnz)
        !! matrix values
    !------------------------------------------------------------------------------------------------------------------

        C = 0
        R = 0
        V = 0.0

        ie = 0
        do iElement=1,numberOfElements

            ! Number of neighbours
            nn = mesh%numberOfNeighbours(iElement)

            ! Diagonal
            is = ie + 1
            C(is) = mesh%cellGlobalAddr(iElement)-1
            V(is) = this%aC(iElement,iComp)

            ! Take into account the diagonal
            is = is + 1
            ie = is + nn -1

            ! Off diagonal
            C(is:ie) = mesh%conn(iElement)%col(1:nn)-1
            V(is:ie) = this%anb(iElement)%coeffs(1:nn, iComp)

            ! Fill the row array (first value will be zero, size(R) = n+1)
            R(iElement+1) = ie

        enddo

    end subroutine convertToCSR

!**********************************************************************************************************************

    subroutine solveAMGCL(this, sol, iComp, niters, fieldMean)

        use iso_c_binding

    !==================================================================================================================
    ! Description:
    !! solveAMGCL test procedure for AMGCL.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iComp

        integer :: niters(1)
        !! number of solver iterations

        integer :: R(numberOfElements+1)
        !! CSR row array

        integer :: C(mesh%nnz)
        !! CSR column array

        integer :: solverOptions(14)
        !! solver options to pass amgcl

        integer :: n(2)
    !------------------------------------------------------------------------------------------------------------------

        real :: sol(numberOfElements)
        !! solution

        real :: rhs(numberOfElements)
        !! rhs

        real :: V(mesh%nnz)
        !! CSR value array

        real :: tols(4)
        !! absolute and relative tolerance

        real :: fieldMean(1)
        !! mean field to compute scaled residuals
    !------------------------------------------------------------------------------------------------------------------

        interface

            subroutine solverAMGCL(C, R, V, F, sol, n, tols, solverOptions, niters, xbar) bind(c, name="solverAMGCL")

                import c_int, c_double
                import mesh, numberOfElements

                integer(kind=c_int), dimension(2) :: n
                integer(kind=c_int), dimension(14) :: solverOptions
                integer(kind=c_int), dimension(1) :: niters
                integer(kind=c_int), dimension(mesh%nnz) :: C
                integer(kind=c_int), dimension(numberOfElements+1) :: R
                real(kind=c_double), dimension(mesh%nnz) :: V
                real(kind=c_double), dimension(numberOfElements) :: F
                real(kind=c_double), dimension(numberOfElements) :: sol
                real(kind=c_double), dimension(4) :: tols
                real(kind=c_double), dimension(1) :: xbar

            end subroutine solverAMGCL

        end interface
    !------------------------------------------------------------------------------------------------------------------

        ! Prepare parameters to pass
        n(1) = numberOfElements
        n(2) = mesh%nnz

        tols(1) = this%absTol
        tols(2) = this%relTol
        tols(3) = this%amgclSolverOptions%eps_strong
        tols(4) = this%amgclSolverOptions%eps_trunc

        solverOptions(1)  = this%amgclSolverOptions%solverType
        solverOptions(2)  = this%amgclSolverOptions%coarseningType
        solverOptions(3)  = this%amgclSolverOptions%relaxationType
        solverOptions(4)  = this%amgclSolverOptions%npre
        solverOptions(5)  = this%amgclSolverOptions%npost
        solverOptions(6)  = this%amgclSolverOptions%ncycle
        solverOptions(7)  = this%amgclSolverOptions%maxIter
        solverOptions(8)  = this%amgclSolverOptions%pre_cycles
        solverOptions(9)  = this%amgclSolverOptions%coarse_enough
        solverOptions(10) = this%amgclSolverOptions%fillIn
        solverOptions(11) = this%amgclSolverOptions%mrestart
        solverOptions(12) = this%amgclSolverOptions%allow_rebuild
        solverOptions(13) = this%amgclSolverOptions%direct_coarse
        solverOptions(14) = this%amgclSolverOptions%do_trunc

        ! Solve with AMGCL
        rhs(1:numberOfElements) = this%bC(:,iComp)
        call this%convertToCSR(V, C, R, iComp)
        call solverAMGCL(C, R, V, rhs, sol, n, tols, solverOptions, niters, fieldMean)

    end subroutine solveAMGCL

!**********************************************************************************************************************

    subroutine getAMGCLOptions(this)

        use iso_c_binding

    !==================================================================================================================
    ! Description:
    !! getAMGCLOptions parses options from the lsolver dictionary and return options.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        TYPE(JSON_CORE) :: jcore

        type(JSON_VALUE), pointer :: objPointer, dictPointer, subdict_pointer, keyPointer
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictValue, path, optionPath, petscOpt

        character(len=50), dimension(:), allocatable  :: splitPath
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, opt_size, ierr
    !------------------------------------------------------------------------------------------------------------------

        logical :: found, keyFound, found_subdict
    !------------------------------------------------------------------------------------------------------------------

        ! Set OMP_NUM_THREADS
        interface
            subroutine c_set_omp_num_threads() bind(c, name="c_set_omp_num_threads")
            end subroutine c_set_omp_num_threads
        end interface
    !------------------------------------------------------------------------------------------------------------------

        ! Set the enviroment for OMP
        call c_set_omp_num_threads()

        ! Set defaults
        call this%AMGCLDefaults()

        ! Parse the option sub-dictionary
        call jcore%initialize()
        call flubioSolvers%json%get(dictPointer)
        call jcore%get_child(dictPointer, this%eqName, objPointer, found)

        if(found) call jcore%get_child(objPointer, 'options', subdict_pointer, found_subdict)

        if(found_subdict) then

            optionPath = trim(this%eqName)//'%options'
            call flubioSolvers%json%info(optionPath, n_children=opt_size)

            do i = 1, opt_size

                call jcore%get_child(subdict_pointer, i, keyPointer, keyFound)

                if(keyFound)then

                    call jcore%get_path(keyPointer, path, keyFound)
                    call jCore%get(keyPointer, dictValue)

                    call split_in_array(path, splitPath, '.')
                    petscOpt = trim(splitPath(size(splitPath)))

                    if(petscOpt=='solver') then
                        this%amgclSolverOptions%solverType = getAMGCLSolverOption(dictValue)
                    elseif(petscOpt=='coarsening') then
                        this%amgclSolverOptions%coarseningType = getAMGCLCoarseningOption(dictValue)
                    elseif(petscOpt=='relaxation') then
                        this%amgclSolverOptions%relaxationType = getAMGCLRelaxationOption(dictValue)
                    elseif(petscOpt=='damping') then
                        call string_to_value(dictValue, this%amgclSolverOptions%damping, ierr)
                    elseif(petscOpt=='maxIter') then
                        call string_to_value(dictValue, this%amgclSolverOptions%maxIter, ierr)
                    elseif(petscOpt=='npre') then
                        call string_to_value(dictValue,  this%amgclSolverOptions%npre, ierr)
                    elseif(petscOpt=='npost') then
                        call string_to_value(dictValue, this%amgclSolverOptions%npost, ierr)
                    elseif(petscOpt=='ncycle') then
                        call string_to_value(dictValue, this%amgclSolverOptions%ncycle, ierr)
                    elseif(petscOpt=='pre_cycles') then
                        call string_to_value(dictValue, this%amgclSolverOptions%pre_cycles, ierr)
                    elseif(petscOpt=='coarse_enough') then
                        call string_to_value(dictValue, this%amgclSolverOptions%coarse_enough, ierr)
                    elseif(petscOpt=='mrestart') then
                        call string_to_value(dictValue, this%amgclSolverOptions%mrestart, ierr)
                    elseif(petscOpt=='fillin') then
                        call string_to_value(dictValue, this%amgclSolverOptions%fillIn, ierr)
                    elseif(petscOpt=='max_levels') then
                        call string_to_value(dictValue, this%amgclSolverOptions%max_levels, ierr)
                    elseif(petscOpt=='allow_rebuild') then
                        call string_to_value(dictValue, this%amgclSolverOptions%allow_rebuild, ierr)
                    elseif(petscOpt=='direct_coarse') then
                        call string_to_value(dictValue, this%amgclSolverOptions%direct_coarse, ierr)
                    elseif(petscOpt=='strong_threshold') then
                        call string_to_value(dictValue, this%amgclSolverOptions%eps_strong, ierr)
                    else
                        call flubioStopMsg("ERROR: unknown amgcl option. Try solver, coarsening, relaxation, maxIter, npre, npost, ncycle, pre_cycles")
                    endif

                else
                    call flubioStopMsg("ERROR: cannot parse option correctly. Please check your json dictionary.")
                endif

            end do
        else
            call this%AMGCLDefaults()
        end if

        ! Clean up
        nullify(dictPointer)
        if(found_subdict) nullify(objPointer)
        if(keyFound) nullify(keyPointer)
        call jcore%destroy()

    end subroutine getAMGCLOptions

!**********************************************************************************************************************

    subroutine AMGCLDefaults(this)

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        this%amgclSolverOptions%solverType = 0
        this%amgclSolverOptions%coarseningType = 1
        this%amgclSolverOptions%relaxationType = 1
        this%amgclSolverOptions%maxIter = 1000
        this%amgclSolverOptions%npre = 1
        this%amgclSolverOptions%npost = 1
        this%amgclSolverOptions%ncycle = 1
        this%amgclSolverOptions%pre_cycles = 1
        this%amgclSolverOptions%coarse_enough = -1
        this%amgclSolverOptions%max_levels = 25
        this%amgclSolverOptions%mrestart = 30
        this%amgclSolverOptions%fillIn = 1
        this%amgclSolverOptions%allow_rebuild = 0
        this%amgclSolverOptions%direct_coarse = 1
        this%amgclSolverOptions%do_trunc = 1
        this%amgclSolverOptions%do_trunc = 0
        this%amgclSolverOptions%eps_strong = 0.08
        this%amgclSolverOptions%eps_trunc = 0.2
        this%amgclSolverOptions%damping = 1.0

    end subroutine AMGCLDefaults

!**********************************************************************************************************************

    function getAMGCLSolverOption(optName) result(opt)

        character(len=*) :: optName
    !------------------------------------------------------------------------------------------------------------------

        integer :: opt
    !------------------------------------------------------------------------------------------------------------------

        if(optName=='cg') then
            opt = 0
        elseif(optName=='bicgstab') then
            opt = 1
        elseif(optName=='bicgstabl') then
            opt = 2
        elseif(optName=='gmres') then
            opt = 3
        elseif(optName=='lgmres') then
            opt = 4
        elseif(optName=='fgmres') then
            opt = 5
        elseif(optName=='idrs') then
            opt = 6
        elseif(optName=='richardson') then
            opt = 7
        elseif(optName=='preonly') then
            opt = 8
        else
            call flubioStopMsg('ERROR: unknown solver type '//optName//' for amgcl.')
        endif

    end function getAMGCLSolverOption

!**********************************************************************************************************************

    function getAMGCLCoarseningOption(optName) result(opt)

        character(len=*) :: optName
    !------------------------------------------------------------------------------------------------------------------

        integer :: opt
    !------------------------------------------------------------------------------------------------------------------

        if(optName=='aggregation') then
            opt = 0
        elseif(optName=='smoothed_aggregation') then
            opt = 1
        else
            call flubioStopMsg('ERROR: unknown coarsening type '//optName//' for amgcl.')
        endif

    end function getAMGCLCoarseningOption

 !**********************************************************************************************************************

    function getAMGCLRelaxationOption(optName) result(opt)

        character(len=*) :: optName
    !------------------------------------------------------------------------------------------------------------------

        integer :: opt
    !------------------------------------------------------------------------------------------------------------------

        if(optName=='damped_jacobi') then
            opt = 0
        elseif(optName=='gauss_seidel') then
            opt = 1
        elseif(optName=='ilu0') then
            opt = 2
        elseif(optName=='iluk') then
            opt = 3
        elseif(optName=='ilut') then
            opt = 4
        elseif(optName=='ilup') then
            opt = 5
        elseif(optName=='spai0') then
            opt = 6
        elseif(optName=='spai1') then
            opt = 7
        elseif(optName=='chebyshev') then
            opt = 8
        else
            call flubioStopMsg('ERROR: unknown relaxation type '//optName//' for amgcl.')
        endif

    end function getAMGCLRelaxationOption

!**********************************************************************************************************************

    subroutine KSPConvergenceTestFlubio(ksp, n, rnorm, flag, defaultctx, ierr)

        KSP              ksp
        PetscErrorCode ierr
        PetscInt n
        integer*8 defaultctx
        KSPConvergedReason flag
        PetscReal rnorm
    !------------------------------------------------------------------------------------------------------------------

        call KSPConvergedDefault(ksp, n, rnorm, flag, defaultctx, ierr)

        if (n<flubioSolvers%itmin) then
            flag = 0
        endif

        ierr = 0

    end subroutine KSPConvergenceTestFlubio

end module transportEq
