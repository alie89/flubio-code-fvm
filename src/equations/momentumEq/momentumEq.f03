!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module momentumEq

!==================================================================================================================
! Description:
!! This module implements the momentum equation and its data structure.
!==================================================================================================================

    use physicalConstants
    use fieldvar
    use transportEq

    implicit none

    type, public, extends(transportEqn) :: momentumEqn

        real, dimension(:,:), allocatable :: bCprime
        !! Right hand side for the momentum equation matrix in PRIME correction, storing only the diffusion term and transient term contribution

        real, dimension(:,:), allocatable :: K_1, K_2
        !! RK old porjections (for fractional step only)

        contains

            procedure :: createEq => createMomentumEq
            procedure :: destroyEq => destroyMomentumEq
            procedure :: createEqCoeffs => createMomentumEqCoeffs
            procedure :: destroyEqCoeffs => destroyMomentumEqCoeffs
            procedure :: updateDiffusionCoeffs
            procedure :: applyBoundaryConditionsMomentum
            procedure :: HbyA

    end type momentumEqn

contains

    subroutine createMomentumEq(this, eqName, nComp)

    !==================================================================================================================
    ! Description:
    !! createMomentumEq is the class constructor.
    !==================================================================================================================

        class(momentumEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: eqName
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement

        integer :: nComp
    !------------------------------------------------------------------------------------------------------------------

        this%nComp = nComp

        call this%createEqCoeffs()

        allocate(this%res(nComp))
        allocate(this%convergenceResidual(nComp))
        allocate(this%scalingRef(nComp))
        allocate(this%unscaledRes(nComp))
        allocate(this%resmax(nComp))

        this%res = 0.0
        this%scalingRef = 0.0
        this%unscaledRes = 0.0
        this%convergenceResidual = 0.0
        this%resmax = 0.0

        this%eqName = eqName

        this%isAllocated = .false.

    end subroutine createMomentumEq

! *********************************************************************************************************************

    subroutine destroyMomentumEq(this)

    !==================================================================================================================
    ! Description:
    !! destroyMomentumEq is the class de-contructor.
    !==================================================================================================================

        class(momentumEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: ierr
    !------------------------------------------------------------------------------------------------------------------

        call this%destroyEqCoeffs()

        deallocate(this%res)
        deallocate(this%convergenceResidual)
        deallocate(this%scalingRef)
        deallocate(this%unscaledRes)
        deallocate(this%resmax)

        call MatDestroy(this%A, ierr)
        call VecDestroy(this%rhs, ierr)

    end subroutine destroyMomentumEq

! *********************************************************************************************************************

    subroutine createMomentumEqCoeffs(this)

    !==================================================================================================================
    ! Description:
    !! createMomentumEqCoeffs allocates equation coefficients and rhs.
    !==================================================================================================================

        class(momentumEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        if(.not. this%coeffsAllocated) then
            allocate(this%aC(numberOfElements, this%nComp))
            allocate(this%anb(numberOfElements))
            allocate(this%bC(numberOfElements, this%nComp))
            allocate(this%bCPrime(numberOfElements, this%nComp))

            this%aC = 0.0
            this%bC = 0.0
            this%bCPrime = 0.0

            do iElement=1,numberOfElements
                this%anb(iElement)%csize_i = mesh%numberOfNeighbours(iElement)
                this%anb(iElement)%csize_j = this%nComp
                call this%anb(iElement)%initRealMat(mesh%numberOfNeighbours(iElement), this%nComp)
            end do

            this%coeffsAllocated = .true.

        endif 

    end subroutine createMomentumEqCoeffs

! *********************************************************************************************************************

    subroutine destroyMomentumEqCoeffs(this)

    !==================================================================================================================
    ! Description:
    !! destroyMomentumEq deallocates equation coefficients and rhs.
    !==================================================================================================================

        class(momentumEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        if(this%coeffsAllocated) then
            deallocate(this%aC)
            deallocate(this%anb)
            deallocate(this%bC)
            deallocate(this%bCPrime)

            this%coeffsAllocated = .false.

        endif 

    end subroutine destroyMomentumEqCoeffs

! *********************************************************************************************************************

    subroutine updateDiffusionCoeffs(this)

    !==================================================================================================================
    ! Description:
    !! updateDiffusionCoeffs stores the rhs in their current status. The reason why this subroutine is called
    !! updateDiffusionCoeffs is that it is called after the computation of the diffusion term coefficients that will be reused
    !! in the corrector step of the PISO algorithm.
    !==================================================================================================================

        class(momentumEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        this%bCPrime = this%bC

    end subroutine updateDiffusionCoeffs

! *********************************************************************************************************************


    subroutine applyBoundaryConditionsMomentum(this)

    !==================================================================================================================
    ! Description:
    !! applyBoundaryConditionsMomentum applies the boundary conditions to the momentum equation. According to the boundary flag
    !! assigned at each boundary, this subroutine will invoke the proper subrotuine to compute the diagonal and rhs corrections.
    !==================================================================================================================

        class(momentumEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iB, iBoundary, iOwner, iBFace, iComp, is ,ie, patchFace, bFlag
    !------------------------------------------------------------------------------------------------------------------

        real :: mu, aCb(3), bCb(3), phi0(3), acd(3), acc(3), SMALL

        real, dimension(:), allocatable :: slipLength

        real, dimension(:), allocatable :: phiInf
    !------------------------------------------------------------------------------------------------------------------

        patchFace = numberOfElements

        SMALL = 1.0e-12

        !--------------------------------!
        ! Interpolate physical constants !
        !--------------------------------!

        call interpolate(rho, phys%interpType, -1)
        call interpolate(nu, phys%interpType, -1)

        !---------------------------------!
        ! Loop over physical boundaries   !
        !---------------------------------!

        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
            bFlag = velocity%bcFlag(iBoundary)

            ! Loop over the faces of boundary number "iBoundary"
            do iBFace=is, ie

                patchFace = patchFace+1
                iOwner = mesh%owner(iBFace)
                mu = nu%phif(iBFace,1)

                ! void BC
                if(bFlag==0) then

                    call voidBoundary(aCb, bCb, acd, acc, 3)

                ! Wall BC
                elseif(bFlag==1) then

                    call wallBoundary(iBFace, aCb, bCb, acd, acc, velocity%phi(iOwner,:), velocity%phi(patchFace,:), mu)

                ! Slip BC
                elseif(bFlag==2) then

                    call slipBoundary(aCb, bCb, acd, acc)

                ! Symmetry BC
                elseif(bFlag==3) then

                    call symmetryBoundary(velocity%phi, iBFace, aCb, bCb, acd, acc, mu, 3)

                ! Inlet BC
                elseif(bFlag==4) then

                    call velocityInletBoundary(iBFace, aCb, bCb, acd, acc, velocity%phi(patchFace,:), mu, 1.0)

                ! Outlet BC
                elseif(bFlag==5) then

                    call neumann0Boundary(iBFace, aCb, bCb, acd, acc, 1.0, 3)

                ! Fixed Gradient BC
                elseif(bFlag==6) then

                    call neumannBoundary(iBFace, aCb, bCb, acd, acc,velocity%boundaryValue(iBoundary,:), mu, 1.0, 3)

                ! User defined inlet BC
                elseif(bFlag==7) then

                    call velocityInletBoundary(iBFace, aCb, bCb, acd, acc, velocity%phi(patchFace,:), mu, 1.0)

                ! Missing
                elseif(bFlag==8) then

                ! Dirichlet BC
                elseif(bFlag==9) then

                    call dirichletBoundary(iBFace, aCb, bCb, acd, acc, velocity%phi(patchFace,:), mu, 1.0, 3)

                ! Dirichlet BC
                elseif(bFlag==1000) then

                    call dirichletBoundary(iBFace, aCb, bCb, acd, acc, velocity%phi(patchFace,:), mu, 1.0, 3)

                ! Dirichlet BC
                elseif(bFlag==1001) then

                    call velocityInletBoundary(iBFace, aCb, bCb, acd, acc, velocity%phi(patchFace,:), mu, 1.0)

                ! Robin BC
                elseif(bFlag==10) then

                    call velocity%bcDict%get(trim(mesh%boundaries%userName(iBoundary))//'%slipLength', slipLength)
                    call velocity%bcDict%get(trim(mesh%boundaries%userName(iBoundary))//'%phiInf', phiInf)
                    call robinBoundary(iBoundary, iBFace, aCb, bCb, acd, acc, velocity%phiGrad(patchFace,:,:), velocity%phi(iBFace,:), mu, slipLength, phiInf, this%nComp)

                ! FarField
                elseif(bFlag==11) then

                    phi0 = velocity%boundaryValue(iBoundary,:)
                    call farFieldBoundary(iBFace, aCb, bCb, acd, acc, phi0, mu, 1.0, 3)

                elseif(bFlag==12) then

                    call rotatingBoundary(iBFace, iBoundary, aCb, bCb, acd, acc, mu, velocity%bcDict, 0.0)

                else

                    if(id==0) write(*,*) 'FLUBIO: Boundary Condition not found at Boundary ', trim(mesh%boundaries%userName(iBoundary))
                    call flubioStop()

                endif

                !-------------------------------!
                ! Correct boundary coefficients !
                !-------------------------------!

                this%aC(iOwner,:) = this%aC(iOwner,:) + aCb
                this%bC(iOwner,:) = this%bC(iOwner,:) + bCb

                if(this%adaptiveRelaxation==1) then
                    this%aCD(iOwner,:) = this%aCD(iOwner,:) + acd(:) 
                    this%aCC(iOwner,:) = this%aCC(iOwner,:) + acc(:)
                endif

            enddo

        enddo
        
    end subroutine applyBoundaryConditionsMomentum

! *********************************************************************************************************************

    function HbyA(this, field) result(phi)

    !==================================================================================================================
    ! Description:
    !! HbyA returns the H/aC operator, commonly in SIMPLE algorithm description, i.e. :
    !! \begin{equation}
    !!    H = [(b_C + \nabla P * vol) -\sum a_{nb}*\phi_{anb}/a_C]/a_C
    !!  \end{equation}
    !==================================================================================================================

#include "petsc/finclude/petscksp.h"
        use petscksp

        class(momentumEqn) :: this

        type(flubioField) :: field
        !! field to use in computing the H operator
    !------------------------------------------------------------------------------------------------------------------

        Vec y
        !! auxiliary vector
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
        !! target element
        
        integer :: iComp
        !! target component
        
        integer :: nn
        !! number of neighbours

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        real :: phi(numberOfElements, this%nComp)
        !! HbyA returns values  
        
        real :: sumAnb(numberOfElements,1)
        !! auxilairy variable to store the values in y

        real :: val
        !! auxiliary variable to store the rhs at cell center 

        real :: gradP
        !! varable to store the cell pressure gradient
    !------------------------------------------------------------------------------------------------------------------

        phi = 0.0

        do iComp=1,this%nComp-bdim

            !---------------------!
            ! Assemble the matrix !
            !---------------------!

            call MatZeroEntries(this%A, ierr)
            call VecZeroEntries(this%rhs, ierr)
            sumAnb = 0.0

            do iElement=1,numberOfelements

                ! Diagonal value
                call MatSetValue(this%A, mesh%cellGlobalAddr(iElement)-1, mesh%cellGlobalAddr(iElement)-1, & 
                                 this%aC(iElement,iComp), INSERT_VALUES, ierr)

                ! Neighbours values
                nn = mesh%numberOfNeighbours(iElement)
                call MatSetValues(this%A, 1, mesh%cellGlobalAddr(iElement)-1, nn, mesh%conn(iElement)%col(1:nn)-1, & 
                                  this%anb(iElement)%coeffs(1:nn,iComp), INSERT_VALUES, ierr)

            enddo

            call MatAssemblyBegin(this%A,MAT_FINAL_ASSEMBLY,ierr)
            call MatAssemblyEnd(this%A,MAT_FINAL_ASSEMBLY,ierr)

            !-------------------------------------!
            ! Fill a vector with the equation rhs !
            !-------------------------------------!

            ! duplicate the vector
            call VecDuplicate(this%rhs,y,ierr)

            call VecSetValues(this%rhs, numberOfElements, mesh%cellGlobalAddr(1:numberOfElements)-1, & 
                              field%phi(1:numberOfElements,iComp), INSERT_VALUES, ierr)

            call VecAssemblyBegin(this%rhs, ierr)
            call VecAssemblyEnd(this%rhs, ierr)

            !------------------------------------------------------------------------!
            ! Matrix multiplication, remember to subtract aC(iElement)*phi(iElement) !
            !------------------------------------------------------------------------!

            call MatMult(this%A, this%rhs, y, ierr)
            call VecGetValues(y, flubioSolvers%lm, mesh%cellGlobalAddr(1:numberOfElements)-1, sumAnb(:,1), ierr)

            ! Subtract off the diagonal term and add the pressure gradient again
            do iElement=1,numberOfElements

                gradP = pressure%phiGrad(iElement,iComp,1)*mesh%volume(iElement)

                val = (this%bC(iElement,iComp) + gradP) -(sumAnb(iElement,1) &
                        -this%aC(iElement,iComp)*field%phi(iElement,iComp))

                phi(iElement,iComp) = val/this%aC(iElement, iComp)

            enddo

            ! Destroy PETSc object
            call VecDestroy(y, ierr)

        enddo

    end function HbyA

end module momentumEq