!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

    subroutine potentialEqn

    !==================================================================================================================
    ! Description:
    !! potentialEqn implements the Laplace equation used in potential flows.
    !==================================================================================================================

        use transportEquation
        use fieldvar
        use globalMeshVar
        use gradient
        use physicalConstants

        implicit none

        type(flubioField) :: ones
    !------------------------------------------------------------------------------------------------------------------

        integer :: nCorr, reusePC
    !------------------------------------------------------------------------------------------------------------------

        real :: fluxTf(numberOfFaces, 1)

        real :: bCDdtQ(numberOfElements,1)
    !------------------------------------------------------------------------------------------------------------------

        ! reset coeffs
        call Teqn%resetCoeffs()
        reusePC = 0

        ! Interpolate density
        call interpolate(rho, 'harmonic', -1)

        ! Unitary diffusion coefficient for potential flow
        call ones%setUpCoeff('one', appendToList=.false., defaultValue=1.0)
 
        !--------------------------!
        ! Update "Old" terms       !
        !--------------------------!

        call T%updateOldFieldValues()
            
        !--------------------------!
        ! Assemble linear system   !
        !--------------------------!

        ! Boundary conditions
        call Teqn%applyBoundaryConditions(T, ones)

        ! Relax transport equation
        if(steadySim==1) call Teqn%relaxEq(T)

        ! Save the RHS with no diffusion term, reuse coeffs in non-orthogonal corrections!
        bCDdtQ = Teqn%bC

        ! Diffusion
        call Teqn%addDiffusionTerm(ones)

        !------------------------!
        ! Solve poisson equation !
        !------------------------!

        do nCorr=1, flubioOptions%nCorr+1

            call Teqn%solve(T, setGuess=1, reusePC=reusePC, reuseMat=nCorr)

            ! Update boundary fields
            call T%updateBoundaryField()

            ! Compute gradient
            call computeGradient(T)

            ! reset RHS
            Teqn%bC = bCDdtQ

            ! Update with the new cross diffusion
            call Teqn%updateRhsWithCrossDiffusion(T, ones, T%nComp)

            reusePC = 1

        end do

        call T%updateBoundaryField()

        ! Compute mass fluxes by constructing total diffusion fluxes at mesh faces.
        ! It is like computing the gradient of T and multiply by Sf
        call totalDiffusionFluxes(T, ones, fluxTf)
        vf = -fluxTf
        mf = rho%phif*vf

        ! Now, reconstruct the cell center velocity from the mass fluxes
        call recontructFromFaceValues(velocity%phi(1:numberOfElements,:), rho%phif*vf)
        
        ! Add boundaary layer if required (keyword addBoundaryLayer in flubioOptions)
        call addBL()

        ! Update at boundaries
        call velocity%updateBoundaryVelocityField()

        ! Correct mass fluxes at boundaries
        call boundaryMassFluxes(rho%phif)

    end subroutine potentialEqn

! *********************************************************************************************************************

    subroutine addBL()

    !==================================================================================================================
    ! Description:
    !! potentialEqn implements the Laplace equation used in potential flows.
    !==================================================================================================================

        use fieldvar
        use globalMeshVar
        use physicalConstants
        use wallDistance
        
       type(flubioWallDist) :: y
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: opt
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iBoundary, iBFace, is, ie, iOwner, addBLFlag
    !------------------------------------------------------------------------------------------------------------------

        real :: d, tBL
    !------------------------------------------------------------------------------------------------------------------
     
        ! Get info
        call flubioOptions%json%get('AddBoundaryLayer', opt, found)
        if(.not. found) opt='none'

        if(lowercase(opt)=='yes' .or. lowercase(opt)=='on') then
            addBLFlag=1
        else
            addBLFlag=0
            return
        end if

        call flubioMsg('FLUBIO: adding boundary layer at walls')

        call flubioOptions%json%get('BoundaryLayerThickness', tBL, found)
        if(.not. found) tBL = 1e-3
        
        ! Compute Wall Distance
        call y%computeWallDistance()

        do iElement=1,numberOfElements
            if(y%dperp(iElement) < tBL) then 
                velocity%phi(iElement,1:3) = velocity%phi(iElement,1:3)*(y%dperp(iElement)/tBL)**(1.0/7.0)
            endif
        enddo

        call velocity%updateGhosts()

    end subroutine addBL

! *********************************************************************************************************************
