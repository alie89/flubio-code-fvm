!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

    subroutine computeMassFluxesNoRC

    ! =================================================================================================================
    ! Description:
    !! computeMassFluxesNORC calculates the mass fluxes at mesh faces during the first time-step/iteration:
    !! \begin{equation*}
    !!     m_f = \textbf{u}_f \cdot \textbf{S}_f
    !! \end{equation*}
    !! The face velocities are evaluated using a standard linear interpolation without Rhie\&Chow.
    ! =================================================================================================================

        use meshvar
        use fieldvar
        use physicalConstants
        use interpolation

        implicit none

        integer :: iFace, iBFace, iOwner, iNeighbour, iBoundary ,iComp, opt
    !------------------------------------------------------------------------------------------------------------------

        real :: dot
    !------------------------------------------------------------------------------------------------------------------

        ! Interpolate density to the faces
        call interpolate(rho, interpType=phys%interpType, opt=-1)

        ! Interpolate velocity to the faces
        call interpolate(velocity, interpType='linear', opt=-1)
        vf = velocity%phif

        do iFace = 1,numberOfFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            dot = dot_product(velocity%phif(iFace,:),mesh%Sf(iFace,:))
            mf(iFace,1) = rho%phif(iFace,1)*dot

        enddo

    end subroutine computeMassFluxesNoRC

! *********************************************************************************************************************

    subroutine boundaryMassFluxes(rhof)

    ! =================================================================================================================
    ! Description:
    !! boundaryMassFluxes computes the boundary mass fluxes.
    ! =================================================================================================================

        use meshvar
        use fieldvar, only: velocity, mf

        implicit none

        integer :: iBFace, iBoundary, iOwner, is, ie, patchFace
    !------------------------------------------------------------------------------------------------------------------

        real :: rhof(numberOfFaces, 1)

        real :: Sf(3)
    !------------------------------------------------------------------------------------------------------------------

        patchFace  = numberOfElements
        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie

                patchFace = patchFace + 1

                Sf = mesh%Sf(iBFace, :)

                mf(iBFace, 1) = dot_product(velocity%phi(patchFace, :), Sf)

            enddo

        enddo

    end subroutine boundaryMassFluxes

! *********************************************************************************************************************

    subroutine writeMassFluxesToFile()

    ! =================================================================================================================
    ! Description:
    !! writeMassFluxesToFile saves mass fluxes to a binary file.
    ! =================================================================================================================

        ! Global variables
        use fieldvar
        use meshvar

        ! Local Variables
        implicit none

        character(len=:), allocatable :: postDir
        !! post processing directory

        character(len=:), allocatable :: fieldDir
        !! field directory

        character(len=20) :: timeStamp
        !! time stamp

        character(len=20) ::  procID
        !! processor ID
    !------------------------------------------------------------------------------------------------------------------

        logical :: dirExists
        !! boolean to check if the directory exists
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !! target components
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        postDir='postProc/fields/'
        write(timeStamp,'(i0)') itime
        fieldDir = postDir//trim(timeStamp)//'/'

        ! Create a directory for the field. If it does not exists create it.
        inquire(file=trim(fieldDir)//'/.', exist=dirExists)

        if(dirExists) then

            ! write mass fluxes
            open(1,file=fieldDir//'mf-d'//trim(procID)//'.bin', form='unformatted')
                write(1) mf
                !write(1) vf
            close(1)

        elseif(.not. dirExists) then

            ! create the folder
            call execute_command_line ('mkdir -p ' // adjustl(trim(fieldDir) ) )

            ! write mass fluxes
            open(1,file=fieldDir//'mf-d'//trim(procID)//'.bin', form='unformatted')
                write(1) mf
                !write(1) vf
            close(1)

        end if

    end subroutine writeMassFluxesToFile

! *********************************************************************************************************************

    subroutine readMassFluxesFromFile(timeToRead)

    !==================================================================================================================
    ! Description:
    !! readMassFluxesFromFile reads the mass fluxes from a file.
    !==================================================================================================================

        ! Global variables
        use fieldvar
        use meshvar

        ! Local Variables
        implicit none

        character(len=:), allocatable :: postDir
        !! post processing directory

        character(len=:), allocatable :: fieldDir
        !! field directory

        character(len=20) :: timeStamp
        !! time stamp

        character(len=20) ::  procID
        !! processor ID
    !------------------------------------------------------------------------------------------------------------------

        logical :: dirExists
        !! boolean to check if the directory exists
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !! target components

        integer :: timeToRead
        !! time to read
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        postDir='postProc/fields/'
        write(timeStamp,'(i0)') timeToRead
        fieldDir = postDir//trim(timeStamp)//'/'

        if(id==0) write(*,*) 'FLUBIO: recovering mass fluxes from file...'

        ! Create a directory for the field. If it does not exists create it.
        inquire(file=trim(fieldDir)//'/.', exist=dirExists)

        if(dirExists) then

            ! write mass fluxes
            open(1,file=fieldDir//'mf-d'//trim(procID)//'.bin', form='unformatted')
                read(1) mf
                !read(1) vf
            close(1)
     
        elseif(.not. dirExists) then

            if(id==0) write(*,*) 'FLUBIO: field folder does not exists... something went wrong!'

        end if

    end subroutine readMassFluxesFromFile

! *********************************************************************************************************************

    subroutine divU(div)

    !==================================================================================================================
    ! Description:
    !! divU returns the diverge of the velocity field.
    !==================================================================================================================

        use fieldvar
        use meshvar

        implicit none

        integer :: iBoundary, iFace, iBFace, iOwner, iNeighbour, iProc, is, ie, patchFace
    !------------------------------------------------------------------------------------------------------------------

        real :: div(numberOfElements + numberOfBFaces)
    !------------------------------------------------------------------------------------------------------------------

        div = 0.0

        !----------------!
        ! internal faces !
        !----------------!

        do iFace = 1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            div(iOwner) = div(iOwner) + mf(iFace,1)
            div(iNeighbour) = div(iNeighbour) - mf(iFace,1)

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary = 1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace = is,ie
                iOwner = mesh%owner(iBFace)
                div(iOwner) = div(iOwner) + mf(iBFace,1)
            enddo

        enddo

        !-----------------!
        ! Real boundaries !
        !-----------------!

        patchFace  =  numberOfElements

        do iBoundary = 1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace = is,ie

                iOwner = mesh%owner(iBFace)
                patchFace  =  patchFace+1

                div(iOwner) = div(iOwner) + mf(iBFace,1)
                div(patchFace) = div(iOwner)

            enddo

        enddo

    end subroutine divU

! *********************************************************************************************************************
