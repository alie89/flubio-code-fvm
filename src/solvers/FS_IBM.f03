!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

    program FS_Alghorithm

    !==================================================================================================================
    ! Description:
    !! PISO algorithm by Issa et al. (1986).
    !==================================================================================================================

        use globalTimeVar
        use flubioMpi
        use flubioSettings
        use meshvar
        use correctFields
        use runTimeTasks, only: runTasks
        use turbulenceModels, only: trb, les_trb
        use fractionalStep
        use flubioIBM

        implicit none

        integer :: ierr
    !------------------------------------------------------------------------------------------------------------------

        !-------------------------!
        ! Initialize parallel run !
        !-------------------------!

        call PetscInitialize(PETSC_NULL_CHARACTER,ierr)

        ! Initialize MPI variables
        call setMPIvar()

        !---------------!
        ! Read settings !
        !---------------!

        call NavierStokesSettings()

        !-----------!
        ! Read mesh !
        !-----------!

        call mesh%readMeshFromFiles()

        call mesh%createMeshSearchTree()

        !--------------------!
        ! Allocate Variables !
        !--------------------!

        call initialiseFSSolver()

        !--------------------------!
        ! Set up immersed boundary !
        !--------------------------!

        call ibm%setupImmersedBoundaryMethod()

        !-----------------!
        ! Start time loop !
        !-----------------!

        totalTime = 0.0
        do while (time < tmax)

            ! Advance one time-step
            itime = itime+1
            time = time + dtime
            dtime00 = dtime0
            dtime0 = dtime

            ! Time-step clock
            telap(1) = mpi_Wtime()

            ! Prediction step
            !call predictionStep()

            ! Compute immersed boundary forcing
            call ibm%immersedBoundaryForcing()

            ! Update predicted velocity
            !call ibm%applyForces()

            ! Projection step
            !call projectionStep()

            ! Correction step
            !call correctionStep()

            ! Turbulence models
            !if(flubioTurbModel%ransModel/=-1) call trb%turbulenceModel()
            !if(flubioTurbModel%lesModel/=-1)  call les_trb%updateEddyViscosity()

            ! Update transport model (non-newtonian flows)
            !call updateTransportModel()

            ! Run time processing
            !call runTasks()

            ! IBM body forces and state vector
            !call ibm%writeBodyForces()
            !call ibm%writeBodyStates()

            ! Update body position
            !call ibm%moveBodies()

            ! Write fields
            !if(mod(itime,flubioControls%iflds)==0 .or. abs(flubioControls%iflds)==1) then
            !    call ibm%writeBodyPosition()
            !    call FSWriteTofiles()
            !end if

            ! Stop the clock
            telap(2) = mpi_Wtime()
            totalTime = totalTime +  (telap(2)-telap(1))

            ! Print info
            call FSVerbose()

        enddo

        !-----------------------!
        ! End of the simulation !
        !-----------------------!

        if(id==0) then
            write(*,*) 'FLUBIO: Simulation compleated!',' ', ' Total run time =', totalTime
            write(*,*)
        endif

        ! Re-write fields at the end of the simulation
        call FSWriteTofiles()

        ! Finilize MPI processing
        call PetscFinalize(ierr)

    end program FS_Alghorithm

! *********************************************************************************************************************

    subroutine FSVerbose

    !==================================================================================================================
    ! Description:
    !! FSVerbose prints to screen some run time information. It can be customize to fit user's needs.
    !==================================================================================================================

        use flubioMpi
        use momentum
        use pressureCorrection
        use turbulenceModels

        implicit none
    !------------------------------------------------------------------------------------------------------------------

        ! Print info
        if(id==0) write(*,*)
        call velocity%fieldVerbose()
        call pressure%fieldVerbose()
        if(flubioTurbModel%ransModel > -1) call trb%verbose()

        call Peqn%printDelimiter()

    end subroutine FSVerbose

! *********************************************************************************************************************

    subroutine FSWriteTofiles

    !==================================================================================================================
    ! Description:
    !! FSWriteTofiles dumps fields for restart a simulation and post-processing.
    !==================================================================================================================

        use fieldvar
        use flubioSettings
        use turbulenceModels

        implicit none

        call velocity%writeToFile()
        call pcorr%writeToFile()
        call pressure%writeToFile()
        call nu%writeToFile()
        call rho%writeToFile()
        call writeMassFluxesToFile()
        if(flubioTurbModel%ransModel > -1)  call trb%writeToFile()
        call saveTimeQuantities()

        ! Delete folder you do not want to keep
        call deleteTimeFolders()

    end subroutine FSWriteTofiles