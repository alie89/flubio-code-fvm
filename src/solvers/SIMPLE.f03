!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

    program SIMPLE

    !==================================================================================================================
    ! Description:
    !! Driver fo the SIMPLE algorithm.
    !==================================================================================================================

        use globalTimeVar
        use flubioMpi
        use flubioSettings
        use correctFields
        use momentum
        use pressureCorrection
        use energyEq
        use runTimeTasks, only: runTasks
        use turbulenceModels, only: trb

        implicit none

        integer :: ierr
    !------------------------------------------------------------------------------------------------------------------

        !-------------------------!
        ! Initialize parallel run !
        !-------------------------!

        call PetscInitialize(PETSC_NULL_CHARACTER,ierr)

        ! Initialize MPI variables
        call setMPIvar()

        !---------------!
        ! Read settings !
        !---------------!

        call NavierStokesSettings()

        !-----------!
        ! Read mesh !
        !-----------!

        call mesh%readMeshFromFiles()

        call mesh%createMeshSearchTree()

        !--------------------!
        ! Allocate Variables !
        !--------------------!

        call initialiseNavierStokesSolver()

        !-----------------!
        ! Start time loop !
        !-----------------!

        totalTime = 0.0

        call SIMPLEWriteTofiles()

        do while (flubioControls%convergence/=1)

            ! Perform one SIMPLE iteration
            itime = itime+1
            time = time+1.0

            call Ueqn%printCurrentIteration()

            if(itime>nint(tmax)) exit

            ! Time-step clock
            telap(1) = mpi_Wtime()

            ! Momentum Equation
            call momentumEquation()

            ! Pressure Equation
            call pressureCorrectionEquation()

            ! correct fields
            call correctVelocityAndPressure()

            ! Turbulence models
            call trb%turbulenceModel()

            ! Energy equation, if required
            if(flubioControls%activateEnergyEquation==1) call energyEquation()

            ! update transport model (non-newtonian flows)
            call updateTransportModel()

            ! write residual to file
            call Ueqn%writeresidual()
            call Peqn%writeMassImbalance()

            ! Run time processing
            call runTasks()

            ! write fields
            if(mod(itime,flubioControls%iflds)==0 .or. abs(flubioControls%iflds)==1) then
                call SIMPLEWriteTofiles()
            end if

            ! Stop the clock
            telap(2) = mpi_Wtime()
            totalTime = totalTime +  (telap(2)-telap(1))

            ! Print info
            call SIMPLEVerbose()

            ! check convergence
            if(itime>5) call checkConvergenceSIMPLE()

        enddo

        ! Re-write fields at the end of the simulation
        call SIMPLEWriteTofiles()

        ! Finilize MPI processing
        call PetscFinalize(ierr)

    end program SIMPLE

! *********************************************************************************************************************

    subroutine checkConvergenceSIMPLE

    !==================================================================================================================
    ! Description:
    !! checkConvergence checks if the convergence criteria are satified.
    !==================================================================================================================

        use flubioDictionaries
        use momentum
        use pressureCorrection

        implicit none
    !------------------------------------------------------------------------------------------------------------------

        !-------------------!
        ! Check Convergence !
        !-------------------!

        if(itime>=nint(tmax)) then

            flubioControls%convergence=1

            if(id==0) then
                write(*,*)
                write(*,*) 'SIMPLE has reached the maxium number of Iterations, simulation will be stopped at the end of the current iteration...'
                write(*,*)
            endif

        elseif(Ueqn%res(1)<=Ueqn%resmax(1) .and. Ueqn%res(2)<=Ueqn%resmax(2) &
                .and. Ueqn%res(3)<=Ueqn%resmax(3) .and. Peqn%resCont<=Peqn%resmax(1)) then

            flubioControls%convergence=1

            if(id==0) then
                write(*,*)
                write(*,*) 'SIMPLE has CONVERGED!'
                write(*,*)
                write(*,*) 'residuals for U end P are =', Ueqn%res(1:3), Peqn%resCont
                write(*,*)
            endif

        endif

    end subroutine checkConvergenceSIMPLE

! *********************************************************************************************************************

    subroutine SIMPLEVerbose

        use flubioMpi
        use momentum
        use pressureCorrection
        use turbulenceModels
        use energyEq, only : Eeqn

        implicit none
    !------------------------------------------------------------------------------------------------------------------

        ! Print info
        if(id==0) write(*,*)
        call velocity%fieldVerbose()
        call pressure%fieldVerbose()
        if(flubioTurbModel%ransModel > -1) call trb%verbose()
        if(flubioControls%activateEnergyEquation==1) call temperature%fieldVerbose()

        if(id==0) write(*,*)
        call Ueqn%printResidual()
        call Peqn%printMassImbalance()
        if(flubioTurbModel%ransModel > -1)  call trb%printresidual()
        if(flubioControls%activateEnergyEquation==1) call Eeqn%printResidual()
        call Peqn%printDelimiter()

    end subroutine SIMPLEVerbose

! *********************************************************************************************************************

    subroutine SIMPLEWriteTofiles

        use fieldvar
        use flubioSettings
        use turbulenceModels

        implicit none

        call velocity%writeToFile()
        call pcorr%writeToFile()
        call pressure%writeToFile()
        call nu%writeToFile()
        call rho%writeToFile()
        if(flubioControls%activateEnergyEquation==1) call temperature%writeToFile()
        call writeMassFluxesToFile()
        if(flubioTurbModel%ransModel > -1)  call trb%writeToFile()

        call saveTimeQuantities()

        ! Delete folder you do not want to keep
        call deleteTimeFolders()

    end subroutine SIMPLEWriteTofiles