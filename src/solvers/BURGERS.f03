!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

program burgers

!==================================================================================================================
! Description:
!! Driver for Burgers equation solver.
!==================================================================================================================

#include "petsc/finclude/petscksp.h"
	use petscksp

    use flubioMpi
    use flubioSettings
    use meshvar
    use transportEquation

    implicit none

    integer :: ierr
!------------------------------------------------------------------------------------------------------------------

    !-------------------------!
    ! Initialize parallel run !
    !-------------------------!

    call PetscInitialize(PETSC_NULL_CHARACTER,ierr)

    ! Initialize MPI variables
    call setMPIvar()

    !---------------!
    ! Read settings !
    !---------------!

    call poisson_settings()

    !-----------!
    ! Read mesh !
    !-----------!

    call mesh%readMeshFromFiles()

    call mesh%createMeshSearchTree()

    !--------------------!
    ! Allocate Variables !
    !--------------------!

    call initialiseBurgersSolver()

    !-----------------!
    ! Start time loop !
    !-----------------!

    totalTime=0.0

    do while (time<tmax)

        ! Time-step clock
        telap(1) = mpi_Wtime()

        ! Advance one time-step
        if(steadySim==0) itime = itime+1
        time = time+dtime
        dtime0 = dtime
        dtime00 = dtime0

        call burgersEqn()

        ! Time-step clock
        telap(2) = mpi_Wtime()

        ! Save field to file
        if((mod(itime,flubioControls%iflds)==0 .or. abs(flubioControls%iflds)==1) .and. steadySim==0) then
            call burgersSaveToFile()
        end if

        if(steadySim==0) then
            ! Time-step clock
            telap(2)=mpi_Wtime()
            totalTime = totalTime + telap(2)-telap(1)

            ! print info
            call Teqn%verbose(T)
            call Teqn%printDelimiter()
        endif

        ! Steady state stopping criteria, it allows to keep steady and unsteady within the same file.
        ! Evetual iteration are taken into account the equation itself.
        if(steadySim==1) then
            if(flubioControls%convergence==1) then
                call flubioMsg('FLUBIO: stopping simulation... ')
                exit
            end if
        end if

    enddo

    !---------------------!
    ! Fields and restarts !
    !---------------------!

    call burgersSaveToFile()

    !----------------------!
    ! End of Simulation    !
    !----------------------!

    if(id==0) then
        write(*,*) 'FLUBIO: Simulation compleated',' ', 'total run time =', totalTime
        write(*,*)
    endif

    ! Finilize MPI processing
    call PetscFinalize(ierr)

end program burgers

! *********************************************************************************************************************

    subroutine burgersSaveToFile

        use fieldvar

        implicit none
        
        call T%writeToFile()
        call saveTimeQuantities()

        ! Delete folder you do not want to keep
        call deleteTimeFolders()

    end subroutine

! *********************************************************************************************************************