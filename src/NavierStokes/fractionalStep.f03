!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module fractionalStep

    use globalMeshVar
    use gradient
    use fieldvar
    use m_strings
    use momentum
    use operators
    use physicalConstants
    use pressureCorrection

contains

    subroutine predictionStep

    !==================================================================================================================
    ! Description:
    !! predictionStep solves the momentum equation using a selected explicit time integration scheme.
    !==================================================================================================================

    !------------------------------------------------------------------------------------------------------------------
 
        if(lower(Ueqn%timeOptName)=='euler') then
            call Euler_FS()
        elseif(lower(Ueqn%timeOptName)=='rk3ssp') then
            call RK3SSP_FS()
        elseif(lower(Ueqn%timeOptName)=='ark3') then
            call ARK3_FS()
        elseif(lower(Ueqn%timeOptName)=='soue' .or. lower(Ueqn%timeOptName)=='impliciteuler') then
            call momentumEquation()
        else
            call ARK3_FS()
        end if

    end subroutine predictionStep

! *********************************************************************************************************************

    subroutine projectionStep

    !==================================================================================================================
    ! Description:
    !! momentumEq assembles and solves the momentum equation.
    !==================================================================================================================

        integer::  i, iCorr, reusePC, ierr
    !------------------------------------------------------------------------------------------------------------------

        real :: massResidual(numberOfElements)
        !! mass imbalance at the rhs
    !------------------------------------------------------------------------------------------------------------------

        ! Update mass fluxes (here will likely add the oscillation correction formula)
        mf = faceFlux(velocity, opt=1, coeff=1.0)

        ! Rhie and Chow interpolation correction
        call rhieChowCorrection()

        ! For DNS, where viscosity is constant, assemble the matrix once and reuse the same preconditioner
        if(itime>1 .and. lowercase(flubioTurbModel%modelName)=='dns' .and. flubioControls%restFrom==-1) then

            ! reuse PC
            reusePC = 1

        else

            ! Reset coeffs
            call pEqn%resetCoeffs()
            reusePC = 0

            ! Set up poisson equation, the cross diffusion will be overwritten by the mass residuals
            call pEqn%addDiffusionTerm(pcorr, 1.0)

            ! Boundary conditions for pressure
            call pEqn%pressureBoundaryConditionsFS()

        end if

        ! Assemble rhs, minus sign is because laplacian is assembled with a negative sign
        pEqn%bC(:,1) = -div(mf)/dtime
        massResidual = pEqn%bC(:,1)

        ! Compute gradient for non orthogoal corrections
        call computeGradient(pcorr)

        ! Non-orthogonal corrections
        do iCorr=1,flubioOptions%nCorr+1

            ! Update with the new cross diffusion
            call pEqn%updateRhsWithCrossDiffusionWithConstantDiffusivity(pcorr, nu_f=1.0, nComp=pcorr%nComp)

            ! Set pressure reference
            call Peqn%setPressureReference()

            ! Solve the equation
            call pEqn%solve(pcorr, setGuess=1, reusePC=reusePC, reuseMat=iCorr)

            ! Update boundary fields
            call pcorr%updateBoundaryField()

            ! Compute gradient
            call computeGradient(pcorr)

            ! Reset RHS
            pEqn%bC(:,1) = massResidual

            ! Reuse preconditioner
            reusePC = 1

        end do

    end subroutine projectionStep

! *********************************************************************************************************************

    subroutine correctionStep

    !==================================================================================================================
    ! Description:
    !! momentumEq assembles and solves the momentum equation.
    !==================================================================================================================

        type(flubioField) :: ones
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, iBoundary, iFace, iBFace, iProc, iComp, is, ie, ierr
    !------------------------------------------------------------------------------------------------------------------

        real :: mcorr
        !! mass flow correction

        real :: fluxTf(numberOfFaces, 1)
        !! total diffusive flux
    !------------------------------------------------------------------------------------------------------------------

        call ones%createWorkField(fieldName='ones', classType='scalar', nComp=1)
        ones%phi = 1.0

        call totalDiffusionFluxes(pcorr, ones, fluxTf)

        !--------------------!
        ! Correct velocities !
        !--------------------!

        do iElement=1,numberOfElements
            velocity%phi(iElement,:) = velocity%phi(iElement,:) - dtime*pcorr%phiGrad(iElement,:,1)
        end do

        call velocity%updateBoundaryVelocityField()
        call computeGradient(velocity)

        !---------------------!
        ! Correct mass fluxes !
        !---------------------!

        ! InternalFaces
        do iFace=1,numberOfIntFaces
            mcorr = dtime*fluxTf(iFace,1)
            mf(iFace,1) = mf(iFace,1) + mcorr
        enddo

        ! Processor Boundaries
        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)

            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace = is,ie
                mcorr = dtime*fluxTf(iBFace,1)
                mf(iBFace,1) = mf(iBFace,1) + mcorr
            enddo

        enddo

        ! Boundary mass fluxes
        call correctBoundaryMassFluxes(fluxTf)

        ! Chorin FS, pressure = pcorr
        pressure%phi = pcorr%phi
        call pressure%updateBoundaryPressureField()
        call computeGradient(pressure)

        ! Put w=0 for 2D problems
        if (bdim==1) call velocity%componentToZero(3)

        ! Clean
        call ones%destroyWorkField()

    end subroutine correctionStep

! *********************************************************************************************************************

    subroutine correctBoundaryMassFluxes(fluxTf)

    !==================================================================================================================
    ! Description:
    !! Correct mass fluxes at boundaries (e.g. outlet condition).
    !==================================================================================================================

        integer :: i, iBoundary, iBface,  iOwner, is, ie, bFlag
    !------------------------------------------------------------------------------------------------------------------

        real :: corr
        !! mass flux correction

        real :: valueFraction
        !! switch

        real :: fluxTf(numberOfFaces, 1)
        !! Total diffusion flux
    !------------------------------------------------------------------------------------------------------------------

        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
            bFlag = velocity%bcFlag(iBoundary)

            ! Loop over the faces of boundary number "iBoundary"
            do iBFace=is, ie

                iOwner = mesh%owner(iBFace)

                ! Outlet BC
                if(bFlag==5) then

                    corr = dtime*fluxTf(iBFace,1)

                    mf(iBFace,1) = mf(iBFace,1) + corr

                ! Inlet/outlet or Free stream BC
                elseif(bFlag==11) then

                    if(mf(iBFace,1) > 0.0) then
                        corr = dtime*fluxTf(iBFace,1)
                    else
                        corr = 0.0
                    end if

                    mf(iBFace,1) = mf(iBFace,1) + corr

                endif

            enddo

        enddo

    end subroutine correctBoundaryMassFluxes

! *********************************************************************************************************************

    subroutine rhieChowCorrection()

    !==================================================================================================================
    ! Description:
    !! rhieChowCorrection corrects the mass fluxes used to compute the pressure equation rhs.
    !==================================================================================================================

        integer :: iBoundary, iBFace, is, ie
    !------------------------------------------------------------------------------------------------------------------

        real :: pressGrad_f(numberOfFaces,3,1)
        !!  pressure gradient interpolated (with correction) at mesh faces

        real :: gradPbar(numberOfFaces,3,1)
        !! pressure gradient linearly interpolated at mesh faces

        real :: deltaP(3)
        !! Rhie & Chow pressure gradient difference

        real :: corr(3)
        !! auxiliary value

        real :: mcorr
        !! mass flux correction
    !------------------------------------------------------------------------------------------------------------------

        ! Interpolate rho at faces
        call interpolate(rho, interpType=phys%interpType, opt=-1)

        ! Interpolate pressure gradient at faces
        call interpolateGradientCorrected(pressGrad_f, pressure, opt=1, fComp=1)
        call interpolateGradientCorrected(gradPbar, pressure, opt=0, fComp=1)

        ! Correct face velocities with Rhie and Chow interpolation (see Computational Methods for Fluid Dynamics 4th edition, eq. (8.60) )
        do iFace=1,numberOfIntFaces
            deltaP = pressGrad_f(iFace,:,1) - gradPbar(iFace,:,1)
            mcorr = dot_product(corr, mesh%Sf(iFace,:))
            mf(iFace,1) = mf(iFace,1) - dtime*mcorr
        enddo

        ! Processor boundaries
        do iBoundary = 1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace = is,ie
                deltaP = pressGrad_f(iBFace,:,1) - gradPbar(iBFace,:,1)
                mcorr = dot_product(corr, mesh%Sf(iBFace,:))
                mf(iBFace,1) = mf(iBFace,1) - dtime*mcorr
            enddo

        enddo

    end subroutine rhieChowCorrection

! ********************************************************************************************************************!
!                                     TIME STEPPING                                                                   !
! ********************************************************************************************************************!

    subroutine Euler_FS

    !==================================================================================================================
    ! Description:
    !! predictionStep solves the momentum equation unsig RK3-SSP time integration scheme.
    !==================================================================================================================

        integer :: iComp, ierr
    !------------------------------------------------------------------------------------------------------------------

        real :: divU(numberOfElements,3)
        !! convective term

        real :: laplaU(numberOfElements,3)
        !! diffusion term

        real :: dU(numberOfElements,3)
        !! velocity increment
    !------------------------------------------------------------------------------------------------------------------

        ! Update old velocity field
        call velocity%updateOldFieldValues()

        ! Compute momentum euqation terms
        divU = div(mf, velocity, opt=Ueqn%convOpt)
        laplaU = lapla(nu, velocity)

        dU = -divU + laplaU

        ! Update velocity at time n+1
        do iComp=1,velocity%nComp

            velocity%phi(1:numberOfElements,iComp) = velocity%phio(1:numberOfElements,iComp) + &
                                                     dtime*(dU(1:numberOfElements,iComp)/mesh%volume(1:numberOfElements) - &
                                                            pressure%phiGrad(1:numberOfElements,iComp,1))
        end do

        call velocity%updateBoundaryVelocityField()
        call velocity%updateGhosts()

    end subroutine Euler_FS

! *********************************************************************************************************************

    subroutine RK3SSP_FS

    !==================================================================================================================
    ! Description:
    !! predictionStep solves the momentum equation unsig RK3-SSP time integration scheme.
    !==================================================================================================================

        integer :: iComp, ierr
    !------------------------------------------------------------------------------------------------------------------

        real :: a(3)
        !! RK3SSP coefficient

        real :: b(3)
        !! RK3SSP coefficient

        real :: divU(numberOfElements,3)
        !! convective term

        real :: laplaU(numberOfElements,3)
        !! diffusion term

        real :: dU(numberOfElements,3)
        !! velocity increment

        real :: K(numberOfElements,3,3)
        !! RK projections
    !------------------------------------------------------------------------------------------------------------------

        ! RK3-SSP coeffs
        a(1) = 1.0
        a(2) = 0.25
        a(3) = 0.25

        b(1) = 1.0/6.0
        b(2) = 1.0/6.0
        b(3) = 2.0/3.0

        ! Update old velocity field
        call velocity%updateOldFieldValues()

        !-------------!
        ! First stage !
        !-------------!

        divU = div(mf, velocity, opt=Ueqn%convOpt)
        laplaU = lapla(nu, velocity)
        K(:,:,1) = -divU + laplaU

        !--------------!
        ! Second stage !
        !--------------!

        do iComp=1,velocity%nComp
            K(1:numberOfElements,iComp,1) = K(1:numberOfElements,iComp,1)/mesh%volume(1:numberOfElements)
            velocity%phi(1:numberOfElements,iComp) = velocity%phio(1:numberOfElements,iComp) + &
                                                     dtime*a(1)*K(1:numberOfElements,iComp,1)
        end do

        call velocity%updateBoundaryVelocityField()

        mf = faceFlux(velocity, opt=1)
        divU = div(mf, velocity, opt=Ueqn%convOpt)
        laplaU = lapla(nu, velocity)
        K(:,:,2) = -divU + laplaU

        !-------------!
        ! Third stage !
        !-------------!

        do iComp=1,velocity%nComp
            K(1:numberOfElements,iComp,2) =  K(1:numberOfElements,iComp,2)/mesh%volume(1:numberOfElements)
            velocity%phi(1:numberOfElements,iComp) = velocity%phio(1:numberOfElements,iComp) + &
                                                     dtime*(a(2)*K(1:numberOfElements,iComp,1) + &
                                                            a(3)*K(1:numberOfElements,iComp,2))
        end do

        call velocity%updateBoundaryVelocityField()

        mf = faceFlux(velocity, opt=1)
        divU = div(mf, velocity, opt=Ueqn%convOpt)
        laplaU = lapla(nu, velocity)
        K(:,:,3) = -divU + laplaU

        !------------------------------!
        ! Updates solution at time n+1 !
        !------------------------------!

        do iComp=1,velocity%nComp
            velocity%phi(1:numberOfElements,iComp) = velocity%phio(1:numberOfElements,iComp) + &
                                                     dtime*(b(1)*K(1:numberOfElements,iComp,1) + &
                                                     b(2)*K(1:numberOfElements,iComp,2) + &
                                                     b(3)*K(1:numberOfElements,iComp,3)/mesh%volume(1:numberOfElements) &
                                                     -pressure%phiGrad(1:numberOfElements,iComp,1) )
        end do

        call velocity%updateBoundaryVelocityField()
        call velocity%updateGhosts()

    end subroutine RK3SSP_FS

! *********************************************************************************************************************

    subroutine ARK3_FS

    !==================================================================================================================
    ! Description:
    !! predictionStep solves the momentum equation unsig RK3-SSP time integration scheme.
    !==================================================================================================================

        integer :: iComp, ierr
    !------------------------------------------------------------------------------------------------------------------

        real :: a
        !! ark3 coefficeint

        real :: divU(numberOfElements,3)
        !! convective term

        real :: laplaU(numberOfElements,3)
        !! diffusion term

        real :: dU(numberOfElements,3)
        !! velocity increment

        real :: K(numberOfElements,3,3)
        !! RK projections
    !------------------------------------------------------------------------------------------------------------------

        a = 5.0/12.0

        ! Update old velocity field
        call velocity%updateOldFieldValues()

        !-------------------------!
        ! First stage, euler step !
        !-------------------------!

        divU = div(mf, velocity, opt=Ueqn%convOpt)
        laplaU = lapla(nu, velocity)
        K(:,:,1) = -divU + laplaU

        !--------------!
        ! Second stage !
        !--------------!

        do iComp=1,velocity%nComp
            K(1:numberOfElements,iComp,1) = K(1:numberOfElements,iComp,1)/mesh%volume(1:numberOfElements)
            velocity%phi(1:numberOfElements,iComp) = velocity%phio(1:numberOfElements,iComp) + &
                                                              dtime*a*K(1:numberOfElements,iComp,1)
        end do

        call velocity%updateBoundaryVelocityField()

        mf = faceFlux(velocity, opt=1)
        divU = div(mf, velocity, opt=Ueqn%convOpt)
        laplaU = lapla(nu, velocity)
        K(:,:,2) = -divU + laplaU

        !------------------------------!
        ! Updates solution at time n+1 !
        !------------------------------!

        do iComp=1,velocity%nComp
            velocity%phi(1:numberOfElements,iComp) = velocity%phio(1:numberOfElements,iComp) + &
                                                     0.5*dtime*(K(1:numberOfElements,iComp,1) + &
                                                     Ueqn%K_1(1:numberOfElements,iComp) + &
                                                     2.0*(K(1:numberOfElements,iComp,2) - &
                                                          Ueqn%K_2(1:numberOfElements,iComp))/mesh%volume(1:numberOfElements) &
                                                     -pressure%phiGrad(1:numberOfElements,iComp,1))
        end do

        ! Save projections for the next steps
        Ueqn%K_1 = K(:,:,1)
        Ueqn%K_2 = K(:,:,2)

        call velocity%updateBoundaryVelocityField()
        call velocity%updateGhosts()

    end subroutine ARK3_FS

! *********************************************************************************************************************

end module fractionalStep