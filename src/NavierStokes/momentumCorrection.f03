!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

    subroutine primeCorrection()

    !===================================================================================================================
    ! Description:
    !! primeCorrection corrects the solution found in the SIMPLE step (Moukalled et al., pp. 624-625). In particular:
    ! - solves explicitly the momentum equation with the newly computed values of the mass fluxes
    ! - solves again the pressure-correction equations and correct the fields
    !==================================================================================================================

        use correctFields
        use flubioDictionaries
        use fieldvar
        use momentumEq
        use pressureCorrection
        use operators, only : div

        implicit none

    !------------------------------------------------------------------------------------------------------------------

        ! Reset the preconditioner flag after N steps
        if( mod(itime,flubioSolvers%resetPC)==0)  then
            flubioSolvers%flag_pcp=0
        endif

        ! Reset equation coefficients
        call Ueqn%createEqCoeffs()
        call Ueqn%resetCoeffs()

        !------------------------------!
        ! Reassemble momentum equation !
        !------------------------------!

        ! Transient
        if(steadySim==0) call Ueqn%addTransientTerm(velocity, rho)

        ! Diffusion
        call Ueqn%addDiffusionTerm(velocity, nu, boundaryNonOrthCorr=.true.)

        !------------------------------------!
        ! assemble convection term using u** !
        !------------------------------------!

        if(trim(Ueqn%convOptName) /= 'none') then
            if(Ueqn%convImp==0) then
                call Ueqn%addConvectiveTerm(velocity)
            else
                call Ueqn%addConvectiveTermImplicit(velocity)
            endif
        end if

        !----------------------------!
        ! Update Boundary conditions !
        !----------------------------!

        call Ueqn%applyBoundaryConditionsMomentum()

        !-----------------------!
        ! Add pressure gradient !
        !-----------------------!

        call Ueqn%addBodyForce(pressure%phiGrad(:,:,1), 3)

        ! Additional source term
        if(flubioSources%customSourcesFound) call Ueqn%addCustomSourceTerm(velocity)

        if(flubioSources%sourcesFound) call Ueqn%addVolumeSourceTerms()

        !---------------------------!
        ! Solve momentum explicitly !
        !---------------------------!

        call Ueqn%explicitSolve(velocity)

        ! Clean up memory
        call Ueqn%clearMatAndRhs()

        !---------------------------------!
        ! Update velocities at boundaries !
        !---------------------------------!

        call velocity%updateBoundaryVelocityField()

        !-----------------------------------------------!
        ! Re-solve pressure equation and correct fields !
        !-----------------------------------------------!

         call pressureCorrectionEquation()

        !----------------!
        ! Correct Fields !
        !----------------!

        call correctVelocityAndPressure()

        ! Reset the preconditioner flag after N steps
        if( mod(itime,flubioSolvers%resetPC)==0)  then
            flubioSolvers%flag_pcp=0
        endif

    end subroutine primeCorrection

! *********************************************************************************************************************
