module equationRegistry

    use transportEq

    implicit none

    ! Equation registry
    type, public :: flubioEqnRegistry
        class(transportEqn), pointer :: eqn
    end type flubioEqnRegistry

    type(flubioEqnRegistry), dimension(:), allocatable :: eqnRegistry

contains

    function getEqnLocationInRegistry(eqName) result(pos)

        character(len=*) :: eqName
    !------------------------------------------------------------------------------------------------------------------

        integer :: iEqn, pos
    !------------------------------------------------------------------------------------------------------------------
        
        pos = 0
        do iEqn=1,size(eqnRegistry)
            if(trim(eqnRegistry(iEqn)%eqn%eqName)==eqName) pos=iEqn      
        enddo    

        if(pos==0) call flubioMsg("Warning: equation "//trim(eqName)//" not found in the registry.")

    end function getEqnLocationInRegistry  

!********************************************************************************************************************************

    function checkEqnRegistry(eqName, warn) result(found)

        character(len=*) :: eqName
    !------------------------------------------------------------------------------------------------------------------

        integer :: iEqn

        logical :: found, warn
    !------------------------------------------------------------------------------------------------------------------

        found = .false.
        do iEqn=1,size(eqnRegistry)
            if(trim(eqnRegistry(iEqn)%eqn%eqName)==eqName) found = .true.
        enddo    

        if(warn .and. .not. found) call flubioMsg("Warning: equation "//trim(eqName)//" not found in the registry.")

    end function checkEqnRegistry 

end module equationRegistry

