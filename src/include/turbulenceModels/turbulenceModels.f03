!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module turbulenceModels

    use baseRansModel
    use flubioDictionaries

    use DNS

    use kappaOmegaWilcox
    use kappaOmegaBSL
    use kappaOmegaSST
    use spallartAllmaras
    use qZetaModel

    use SmagorinskyModel
    use WALEModel

    use fieldvar, only: fieldRegistry

    implicit none

    ! All turbulence models go here !

    ! DNS
    type(noModel), target :: noTrb

    ! RANS
    type(SAModel), target :: sa
    type(kw1988), target :: kw_wilcox
    type(kwBSL), target :: kw_bsl
    type(kwSST), target :: kw_sst
    type(qZeta), target :: q_zeta

    ! LES
    type(smagorinsky), target :: smag
    type(WALE), target :: wallScaleAdaptive

    ! This is a pointer that will be a runtime selector
    class(baseModelClass), pointer :: trb
    class(subgridModel), pointer :: les_trb

contains

    subroutine turbModelSelector()

    !==================================================================================================================
    ! Description:
    !! turbModelSelctor assing the turbulence model pointer for the run time selection.
    !==================================================================================================================

        if(trim(flubioTurbModel%modelName)=='spalartallmaras') then
            trb => sa
           numberOfRegisteredFields = numberOfRegisteredFields + 1
        elseif(trim(flubioTurbModel%modelName)=='kappaomega') then
            trb => kw_wilcox
           numberOfRegisteredFields = numberOfRegisteredFields + 2
        elseif(trim(flubioTurbModel%modelName)=='kappaomegabsl') then
            trb => kw_bsl
           numberOfRegisteredFields = numberOfRegisteredFields + 2
        elseif(trim(flubioTurbModel%modelName)=='kappaomegasst') then
            trb => kw_sst
           numberOfRegisteredFields = numberOfRegisteredFields + 2
        elseif(trim(flubioTurbModel%modelName)=='qzeta') then
            trb => q_zeta
           numberOfRegisteredFields = numberOfRegisteredFields + 2
        elseif(trim(flubioTurbModel%modelName)=='dns') then
            trb => noTrb
        elseif(trim(flubioTurbModel%modelName)=='smagorinsky') then
            les_trb => smag
           numberOfRegisteredFields = numberOfRegisteredFields + 0
        elseif(trim(flubioTurbModel%modelName)=='wale') then
            les_trb => wallScaleAdaptive
           numberOfRegisteredFields = numberOfRegisteredFields + 0
        else
            trb => null()
            les_trb => null()
            call flubioStopMsg('FLUBIO: turbulence model not found!')
        end if

    end subroutine

end module turbulenceModels
