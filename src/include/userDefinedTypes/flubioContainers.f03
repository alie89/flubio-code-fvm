!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module flubioContainters

    use flubioMpi
    use qvector_m
    
    implicit none

!==============================================================================================================
! Description:
!! This modules implements user defined data structures.
!==============================================================================================================

    type, public :: flubioList

    integer :: m
    integer, dimension(:,:), allocatable :: intList
    real, dimension(:,:), allocatable :: realList

    contains

        ! constructors
        procedure :: initIntList
        procedure :: initRealList
        
        ! setters
        procedure :: setIntList
        procedure :: setRealList
        procedure :: setIntAt
        procedure :: setRealAt

        ! getters
        procedure :: getIntList
        procedure :: getRealList
        procedure :: getIntAt
        procedure :: getRealAt
        procedure :: getListSize
        procedure :: getUnrolledListSize

        ! aritmetics
        procedure :: addIntList
        procedure :: addRealList

        ! interfaces
        generic :: initList => initIntList, initRealList
        generic :: setList => setIntList, setRealList
        generic :: setAt => setIntAt, setRealAt
        generic :: getList => getIntList, getRealList
        generic :: getAt => getIntAt, getRealAt
        generic :: addAt => addIntList, addRealList

        ! utilities
        procedure :: copyList
        procedure :: resetList

    end type flubioList

    type, public :: flubioDynamicList
            type(qvector_t) ::list
        contains

            procedure :: initIntDynList
            procedure :: initRealDynList

            procedure :: getAsIntArray 
            procedure :: getAsRealArray

            procedure :: addLastInt

            procedure :: addLastReal

            procedure :: clearList 

            procedure :: getSize

            generic :: initDynList => initIntDynList, initRealDynList
            generic :: addLast => addLastInt, addLastReal
            generic :: getAsArray => getAsIntArray, getAsRealArray

	    ! N.B. setters and getters are already defined in qvector_m (qcontainerslib). This is just a wrapper with some handy methods wrapped for convenience.

    end type flubioDynamicList

    type, public :: flubioDynamicListList
        type(qvector_t), dimension(:), allocatable :: list
        contains
            procedure :: initIntDynListList
			procedure :: initRealDynListList
            generic :: initDynListList => initIntDynListList, initRealDynListList 
    end type flubioDynamicListList

contains

    subroutine initIntList(this, isize, jsize, intialValues)

        !==============================================================================================================
        ! Description:
        !! initIntList initialises a list of ints.
        !==============================================================================================================

            class(flubioList) :: this
        !--------------------------------------------------------------------------------------------------------------

            integer :: isize
            !! number of rows

            integer, optional :: jsize
            !! number of columns

            integer :: intialValues
            !! initial values
        !--------------------------------------------------------------------------------------------------------------

            if(present(jsize)) then
                allocate(this%intList(isize, jsize))
            else
                allocate(this%intList(isize,1))
            endif        

            this%intList = intialValues

        end subroutine initIntList

    !******************************************************************************************************************

        subroutine setIntList(this, startIndex, targetRow, targetColumn, direction, values, chunkSize)

        !==============================================================================================================
        ! Description:
        !! setIntList fills the list with some values.
        !==============================================================================================================

            class(flubioList) :: this
        !--------------------------------------------------------------------------------------------------------------

            integer :: j
            !! loop index

            integer :: startIndex
            !! start filling index

            integer :: chunkSize 
            !! chunck size

            integer, optional :: direction
            !! insert in rows or in columns

            integer, optional :: targetRow
            !! target column to set a row

            integer, optional :: targetColumn
            !! target column to set a column

            integer :: dir
            !! direction to take

            integer :: row
            !! target row to get
    
            integer :: column
            !! target column to get
        !--------------------------------------------------------------------------------------------------------------

            integer :: values(chunkSize)
            !! values to assign
        !--------------------------------------------------------------------------------------------------------------

            ! Set defaults
            if(.not. present(direction)) then 
                dir = 2
            else 
                dir = direction    
            endif 

            if(.not. present(targetRow)) then 
                row = 1
            else    
                row = targetRow 
            endif 

            if(.not. present(targetColumn)) then 
                column = 1 
            else    
                column = targetColumn
            endif 

            ! Check direction input
            if(dir > 2) call flubioStopMsg('This class allows for 2D arrays at most. Cannot access specified direction.')

            ! Assing values
            if(dir==1) then
                this%intList(row, startIndex:startIndex+chunkSize-1) = values
            else
                this%intList(startIndex:startIndex+chunkSize-1, column) = values
            endif        

        end subroutine setIntList

    !******************************************************************************************************************

        subroutine setIntAt(this, targetRow, targetColumn, val)

        !==============================================================================================================
        ! Description:
        !! setIntList fills the list with some values.
        !==============================================================================================================
    
                class(flubioList) :: this
        !--------------------------------------------------------------------------------------------------------------
    
            integer :: targetRow
            !! target column to set a row
    
            integer :: targetColumn
            !! target column to set a column
                
            integer :: val
            !! values to assign
        !--------------------------------------------------------------------------------------------------------------

            ! Assing values
            this%intList(targetRow, targetColumn) = val
       
        end subroutine setIntAt
    
    !******************************************************************************************************************

        function getIntList(this, is, ie, direction, targetRow, targetColumn, isReal) result(values)

        !===============================================================================================================
        ! Description:
        !! getIntList adds some values to the list.
        !===============================================================================================================

            class(flubioList) :: this
        !---------------------------------------------------------------------------------------------------------------
    
            integer :: n
            !! dummy index

            integer :: is
            !! start index

            integer :: ie
            !! end index

            integer, optional :: direction
            !! direction to take

            integer, optional :: targetRow
            !! target row to get

            integer, optional :: targetColumn
            !! target column to get

            integer :: dir
            !! direction to take

            integer :: row
            !! target row to get

            integer :: column
            !! target column to get

            integer :: isReal
            !! dummy flag to let the compiler know which overloaded version to get. Fortran has to do better here!
        !---------------------------------------------------------------------------------------------------------------

            integer, dimension(:), allocatable :: values
            !! returned values
        !---------------------------------------------------------------------------------------------------------------
            
            ! Allocate values, it's handy to return an allocatable
            n = ie-is+1
            allocate(values(ie-is+1))
            
            ! Set defaults
            if(.not. present(direction)) then 
                dir = 2
            else 
                dir = direction    
            endif 

            if(.not. present(targetRow)) then 
                row = 1
            else    
                row = targetRow 
            endif 

            if(.not. present(targetColumn)) then 
                column = 1 
            else    
                column = targetColumn
            endif 

            ! Check direction input
            if(dir > 2) call flubioStopMsg('This class allows for 2D arrays at most. Cannot access specified direction.')

            if(dir==1) then
                values(1:n) = this%intList(row,is:ie)
            else
                values(1:n) = this%intList(is:ie,column)
            endif        

        end function getIntList

    !******************************************************************************************************************

        function getIntAt(this, targetRow, targetColumn, isReal) result(val)

        !===============================================================================================================
        ! Description:
        !! getIntAt returns a value from the list.
        !===============================================================================================================
            
            class(flubioList) :: this
        !---------------------------------------------------------------------------------------------------------------
            
            integer :: targetRow
            !! target row to get
        
            integer :: targetColumn
            !! target column to get
            
            integer :: isReal
            !! dummy flag to let the compiler know which overloaded version to get. Fortran has to do better here!
    
            integer :: val
            !! returned values
        !---------------------------------------------------------------------------------------------------------------
        
            val = this%IntList(targetRow, targetColumn)
             
        end function getIntAt
    
    !**************************************************************************************************************
        
        subroutine addIntList(this, startIndex, direction, targetRow, targetColumn, operation, values, chunkSize)

        !===============================================================================================================
        ! Description:
        !! getIntList returns values from the list.
        !===============================================================================================================
    
            class(flubioList) :: this
        !---------------------------------------------------------------------------------------------------------------
            
            character(len=*) :: operation
        !---------------------------------------------------------------------------------------------------------------

            integer :: startIndex
            !! start index
    
            integer :: chunkSize
            !! size of the chunk

            integer, optional:: direction
            !! direction to take
    
            integer, optional :: targetRow
            !! target row to get
    
            integer, optional :: targetColumn
            !! target column to get
    
            integer :: dir
            !! direction to take

            integer :: row
            !! target row to get
    
            integer :: column
            !! target column to get
        !---------------------------------------------------------------------------------------------------------------
    
            integer :: values(chunkSize)
            !! returned values
        !---------------------------------------------------------------------------------------------------------------
                    
            ! Set defaults
            if(.not. present(direction)) then 
                dir = 2
            else 
                dir = direction    
            endif 

            if(.not. present(targetRow)) then 
                row = 1
            else    
                row = targetRow 
            endif 

            if(.not. present(targetColumn)) then 
                column = 1 
            else    
                column = targetColumn
            endif 

            ! Check direction input
            if(dir > 2) call flubioStopMsg('This class allows for 2D arrays at most. Cannot access specified direction.')
    
            if(dir==1) then
                if(trim(operation)=='+') then
                this%intList(row, startIndex:startIndex+chunkSize-1) = this%intList(row, startIndex:startIndex+chunkSize-1) + values
                else
                    this%intList(row, startIndex:startIndex+chunkSize-1) = this%intList(row, startIndex:startIndex+chunkSize-1) - values
                endif    
            else
                if(trim(operation)=='+') then
                    this%intList(startIndex:startIndex+chunkSize-1, column) = this%intList(startIndex:startIndex+chunkSize-1, column) + values
                else
                    this%intList(startIndex:startIndex+chunkSize-1, column) = this%intList(startIndex:startIndex+chunkSize-1, column) - values
                endif        
            endif      
    
        end subroutine addIntList

    !*******************************************************************************************************************

        subroutine initRealList(this, isize, jsize, intialValues)

        !==============================================================================================================
        ! Description:
        !! initRealList initialises a list of reals.
        !==============================================================================================================

            class(flubioList) :: this
        !--------------------------------------------------------------------------------------------------------------

            integer :: isize
            !! number of rows

            integer, optional :: jsize
            !! number of columns

            real :: intialValues
            !! initial values
        !--------------------------------------------------------------------------------------------------------------

            if(present(jsize)) then
                allocate(this%realList(isize, jsize))
            else
                allocate(this%realList(isize,1))
            endif        

            this%realList = intialValues

        end subroutine initRealList

    !******************************************************************************************************************

        subroutine setRealList(this, startIndex, targetRow, targetColumn, direction, values, chunkSize)

        !==============================================================================================================
        ! Description:
        !! setRealList fills the list with some values.
        !==============================================================================================================

            class(flubioList) :: this
        !--------------------------------------------------------------------------------------------------------------

            integer :: j
            !! loop index

            integer :: startIndex
            !! start filling index

            integer :: chunkSize 
            !! chunck size

            integer, optional :: direction
            !! insert in rows or in columns

            integer, optional :: targetRow
            !! target column to set a row

            integer, optional :: targetColumn
            !! target column to set a column

            integer :: dir
            !! insert in rows or in columns

            integer :: row
            !! target column to set a row

            integer :: column
            !! target column to set a column
        !--------------------------------------------------------------------------------------------------------------

            real :: values(chunkSize)
            !! values to assign
        !--------------------------------------------------------------------------------------------------------------

            ! Set defaults
            if(.not. present(direction)) then 
                dir = 2
            else 
                dir = direction    
            endif 

            if(.not. present(targetRow)) then 
                row = 1
            else    
                row = targetRow 
            endif 

            if(.not. present(targetColumn)) then 
                column = 1 
            else    
                column = targetColumn
            endif 

            ! Check direction input
            if(dir > 2) call flubioStopMsg('This class allows for 2D arrays at most. Cannot access specified direction.')
 
            ! Assing values
            if(dir==1) then
                this%realList(row, startIndex:startIndex+chunkSize-1) = values
            else
                this%realList(startIndex:startIndex+chunkSize-1, column) = values
            endif        

        end subroutine setRealList

    !******************************************************************************************************************

        subroutine setRealAt(this, targetRow, targetColumn, val)

        !==============================================================================================================
        ! Description:
        !! setRealAt sets a value in the list.
        !==============================================================================================================
        
            class(flubioList) :: this
        !--------------------------------------------------------------------------------------------------------------
        
            integer :: targetRow
            !! target column to set a row
        
            integer :: targetColumn
            !! target column to set a column
        !--------------------------------------------------------------------------------------------------------------     

            real :: val
            !! values to assign
        !--------------------------------------------------------------------------------------------------------------
    
            ! Assing values
            this%realList(targetRow, targetColumn) = val
           
        end subroutine setRealAt

    !******************************************************************************************************************

        function getRealList(this, is, ie, direction, targetRow, targetColumn, isReal) result(values)

        !===============================================================================================================
        ! Description:
        !! getRealList returns values from the list.
        !===============================================================================================================

            class(flubioList) :: this
        !---------------------------------------------------------------------------------------------------------------
    
            integer :: n
            !! dummy index

            integer :: is
            !! start index

            integer :: ie
            !! end index

            integer, optional :: direction
            !! direction to take

            integer, optional :: targetRow
            !! target row to get

            integer, optional :: targetColumn
            !! target column to get

            integer :: dir
            !! direction to take

            integer :: row
            !! target row to get

            integer :: column
            !! target column to get

            real :: isReal
            !! dummy flag to let the compiler know which overloaded version to get. Fortran has to do better here!
        !---------------------------------------------------------------------------------------------------------------

            real, dimension(:), allocatable :: values
            !! returned values
        !---------------------------------------------------------------------------------------------------------------

            ! Allocate values, it's handy to return an allocatable
            n = ie-is+1
            allocate(values(ie-is+1))
            
            if(.not. present(direction)) then 
                dir = 2
            else 
                dir = direction    
            endif 

            if(.not. present(targetRow)) then 
                row = 1
            else    
                row = targetRow 
            endif 

            if(.not. present(targetColumn)) then 
                column = 1 
            else    
                column = targetColumn
            endif 

            ! Check direction input
            if(dir > 2) call flubioStopMsg('This class allows for 2D arrays at most. Cannot access specified direction.')

            if(dir==1) then
                values(1:n) = this%realList(row,is:ie)
            else
                values(1:n) = this%realList(is:ie,column)
            endif        

        end function getRealList

    !******************************************************************************************************************

        function getRealAt(this, targetRow, targetColumn, isReal) result(val)

        !===============================================================================================================
        ! Description:
        !! getRealAt returns a value from the list.
        !===============================================================================================================
        
            class(flubioList) :: this
        !---------------------------------------------------------------------------------------------------------------
        
            integer, optional :: targetRow
            !! target row to get
    
            integer, optional :: targetColumn
            !! target column to get
        
            real :: isReal
            !! dummy flag to let the compiler know which overloaded version to get. Fortran has to do better here!
    
            real :: val
            !! returned values
        !---------------------------------------------------------------------------------------------------------------
    
            val= this%realList(targetRow, targetColumn)
         
        end function getRealAt

    !******************************************************************************************************************

        subroutine addRealList(this, startIndex, direction, targetRow, targetColumn, operation, values, chunkSize)

        !===============================================================================================================
        ! Description:
        !! addRealList adds some values to the list.
        !===============================================================================================================
    
            class(flubioList) :: this
        !---------------------------------------------------------------------------------------------------------------
            
            character(len=*) :: operation
        !---------------------------------------------------------------------------------------------------------------

            integer :: startIndex
            !! start index
    
            integer :: chunkSize
            !! size of the chunk
    
            integer, optional :: direction
            !! direction to take

            integer, optional :: targetRow
            !! target row to get
    
            integer, optional :: targetColumn
            !! target column to get

            integer :: dir
            !! direction to take

            integer :: row
            !! target row to get
    
            integer :: column
            !! target column to get
        !---------------------------------------------------------------------------------------------------------------
    
            real :: values(chunkSize)
            !! values to add
        !---------------------------------------------------------------------------------------------------------------
       
            

            if(.not. present(direction)) then 
                dir = 2
            else 
                dir = direction    
            endif 

            if(.not. present(targetRow)) then 
                row = 1
            else    
                row = targetRow 
            endif 

            if(.not. present(targetColumn)) then 
                column = 1 
            else    
                column = targetColumn
            endif 
                
            ! Check direction input
            if(dir > 2) call flubioStopMsg('This class allows for 2D arrays at most. Cannot access specified direction.')
    
            if(dir==1) then
                if(trim(operation)=='+') then
                this%realList(row, startIndex:startIndex+chunkSize-1) = this%realList(row, startIndex:startIndex+chunkSize-1) + values
                else
                    this%realList(row, startIndex:startIndex+chunkSize-1) = this%realList(row, startIndex:startIndex+chunkSize-1) - values
                endif    
            else
                if(trim(operation)=='+') then
                    this%realList(startIndex:startIndex+chunkSize-1, column) = this%realList(startIndex:startIndex+chunkSize-1, column) + values
                else
                    this%realList(startIndex:startIndex+chunkSize-1, column) = this%realList(startIndex:startIndex+chunkSize-1, column) - values
                endif        
            endif      
    
        end subroutine addRealList

    !******************************************************************************************************************
    
        function getListSize(this, direction) result(listSize)
        
        !==============================================================================================================
        ! Description:
        !! getListSize returns a list size.
        !==============================================================================================================
            
            class(flubioList) :: this
        !--------------------------------------------------------------------------------------------------------------
            
            integer :: listSize
            !! list size

            integer, optional :: direction
            !! direction to take

            integer :: dir
        !--------------------------------------------------------------------------------------------------------------
     
            if(present(direction)) then
                dir = direction
            else   
                dir = 2 
            endif

            ! Check direction
            if(dir==1) then

                ! Check allocation
                if(allocated(this%intList)) then
                    listSize = size(this%intList(1,:))
                else
                    listSize = size(this%realList(1,:))
                endif        

            else

                ! Check allocation
                if(allocated(this%intList)) then
                    listSize = size(this%intList(:,1))
                else
                    listSize = size(this%realList(:,1))
                endif       

            endif    

        end function getListSize


!******************************************************************************************************************
    
        function getUnrolledListSize(this) result(listSize)
        
        !==============================================================================================================
        ! Description:
        !! getUnrolledListSize returns iszie*jsize.
        !==============================================================================================================
                
            class(flubioList) :: this
        !--------------------------------------------------------------------------------------------------------------
                
            integer :: listSize
            !! list size
        !--------------------------------------------------------------------------------------------------------------
         
            ! Check allocation
            if(allocated(this%intList)) then
                listSize = size(this%intList(1,:))*size(this%intList(:,1))
            else
                listSize = size(this%realList(1,:))*size(this%realList(:,1))
            endif        

        end function getUnrolledListSize

!**********************************************************************************************************************

	    subroutine copyList(this, copy)

        !==============================================================================================================
        ! Description:
        !! copyList copies the 2D list from another 2d list.
        !==============================================================================================================
    
            class(flubioList) :: this
    
            type(flubioList) :: copy
        !--------------------------------------------------------------------------------------------------------------
    
            if(allocated(this%intList)) this%intList = copy%intList
            if(allocated(this%realList)) this%realList = copy%realList
    
        end subroutine copyList

!**********************************************************************************************************************

	    subroutine resetList(this)

        !==============================================================================================================
        ! Description:
        !! resetList reset list to 0 values.
        !==============================================================================================================
    
            class(flubioList) :: this
        !--------------------------------------------------------------------------------------------------------------
    
            if(allocated(this%intList)) this%intList(:,:) = 0
            if(allocated(this%realList)) this%realList(:,:) = 0.0
    
        end subroutine resetList

!**********************************************************************************************************************

    subroutine initIntDynList(this, listSizeOf)

    !==============================================================================================================
    ! Description:
    !! initIntDynList initialises to zero a dynamic vector of ints.
    !==============================================================================================================

        class(flubioDynamicList) :: this
    !--------------------------------------------------------------------------------------------------------------

        integer :: listSizeOf
        !! Input integer to get the size of an int in bytes.

        integer :: isize
    !--------------------------------------------------------------------------------------------------------------

        isize = storage_size(listSizeOf)/8
        call this%list%new(1, isize, QVECTOR_RESIZE_LINEAR)
        
    end subroutine initIntDynList    

!**********************************************************************************************************************

    subroutine initRealDynList(this, listSizeOf)

    !==============================================================================================================
    ! Description:
    !! initIntDynList initialises to zero a dynamicvector of reals.
    !==============================================================================================================

        class(flubioDynamicList) :: this
    !--------------------------------------------------------------------------------------------------------------

        integer :: isize
    !--------------------------------------------------------------------------------------------------------------

        real :: listSizeOf
    !--------------------------------------------------------------------------------------------------------------

        isize = storage_size(listSizeOf)/8
        call this%list%new(1, isize, QVECTOR_RESIZE_LINEAR)

    end subroutine initRealDynList    

!******************************************************************************************************************

    subroutine clearList(this)

    !==============================================================================================================
    ! Description:
    !! clearList clears the list of values.
    !==============================================================================================================

        class(flubioDynamicList) :: this
    !--------------------------------------------------------------------------------------------------------------

        call this%list%clear()

    end subroutine clearList    

!******************************************************************************************************************   

    function getSize(this) result(listSize)

    !==============================================================================================================
    ! Description:
    !! getSize clears the list size.
    !==============================================================================================================

        class(flubioDynamicList) :: this
    !--------------------------------------------------------------------------------------------------------------

        integer :: listSize
    !--------------------------------------------------------------------------------------------------------------

        listSize  = this%list%size()

    end function getSize    
    
!******************************************************************************************************************   

        subroutine addLastInt(this, val)

        !==============================================================================================================
        ! Description:
        !! addLast adds a integer value at the back of the array.
        !==============================================================================================================
    
            class(flubioDynamicList) :: this
        !--------------------------------------------------------------------------------------------------------------
    
            integer :: val
        !--------------------------------------------------------------------------------------------------------------
    
            call this%list%addlast(val)
        
        end subroutine addLastInt  

    !******************************************************************************************************************   

    subroutine addLastReal(this, val)

    !==============================================================================================================
    ! Description:
    !! addLastReal adds a real value at the back of the array.
    !==============================================================================================================
    
        class(flubioDynamicList) :: this
    !--------------------------------------------------------------------------------------------------------------
    
        real :: val
    !--------------------------------------------------------------------------------------------------------------
    
            call this%list%addlast(val)
        
        end subroutine addLastReal 

    !**********************************************************************************************************************    

    subroutine getAsIntArray(this, varr)

    !==============================================================================================================
    ! Description:
    !! getAsIntArray returns the list as an array of ints.
    !==============================================================================================================

        class(flubioDynamicList) :: this
    !--------------------------------------------------------------------------------------------------------------

        integer, dimension(:), allocatable :: varr
    !--------------------------------------------------------------------------------------------------------------

        if(allocated(varr)) deallocate(varr)

        allocate(varr(this%list%size()))
        call this%list%toarray(varr)

    end subroutine getAsIntArray    

!**********************************************************************************************************************

    subroutine getAsRealArray(this, varr)

    !==============================================================================================================
    ! Description:
    !! getAsIntArray returns the list as an array of reals.
    !==============================================================================================================

        class(flubioDynamicList) :: this
    !--------------------------------------------------------------------------------------------------------------

        real, dimension(:), allocatable :: varr
    !--------------------------------------------------------------------------------------------------------------

        if(allocated(varr)) deallocate(varr)

        allocate(varr(this%list%size()))
        call this%list%toarray(varr)

    end subroutine getAsRealArray   

!**********************************************************************************************************************

    subroutine initIntDynListList(this, lsize, listSizeOf)

    !==============================================================================================================
    ! Description:
    !! initIntDynListList initialises a dynamic vector of ints.
    !==============================================================================================================

        class(flubioDynamicListList) :: this
    !--------------------------------------------------------------------------------------------------------------

        integer :: listSizeOf
        !! Input integer to get the size of an int in bytes.

        integer :: i, isize, lsize
    !--------------------------------------------------------------------------------------------------------------

        allocate(this%list(lsize))

        isize = storage_size(listSizeOf)/8
        
        do i=1,lsize
            call this%list(i)%new(1, isize, QVECTOR_RESIZE_LINEAR)
        enddo
        
    end subroutine initIntDynListList    

!**********************************************************************************************************************

    subroutine initRealDynListList(this, lsize, listSizeOf)

    !==============================================================================================================
    ! Description:
    !! initIntDynList initialises a dynamic vector of reals.
    !==============================================================================================================

        class(flubioDynamicListList) :: this
    !--------------------------------------------------------------------------------------------------------------

        integer :: i, isize, lsize
    !--------------------------------------------------------------------------------------------------------------

        real :: listSizeOf
    !--------------------------------------------------------------------------------------------------------------

        allocate(this%list(lsize))

        isize = storage_size(listSizeOf)/8
        
        do i=1,lsize
            call this%list(i)%new(1, isize, QVECTOR_RESIZE_LINEAR)
        enddo

    end subroutine initRealDynListList    

!**********************************************************************************************************************   

end module flubioContainters
