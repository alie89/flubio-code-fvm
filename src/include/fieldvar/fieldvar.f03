!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module fieldvar

    use flubioFields
    use massFlowVar

    implicit none

    type(flubioField), target :: pressure

    type(flubioField), target :: pcorr

    type(flubioField), target :: T

    type(flubioField), target :: velocity

    type(flubioField), target :: temperature

    type(flubioFieldRegistry), dimension(:), allocatable :: fieldRegistry

contains

    function getFieldLocationInRegistry(fieldName) result(pos)

        character(len=*) :: fieldName
    !------------------------------------------------------------------------------------------------------------------

        integer :: iField, pos
    !------------------------------------------------------------------------------------------------------------------
        
        pos = 0
        do iField=1,size(fieldRegistry)
            if(trim(fieldRegistry(iField)%field%fieldName(1))==fieldName) pos=iField      
        enddo    

        if(pos==0) call flubioMsg("Warning: field "//fieldName//" not found in the registry.")

    end function getFieldLocationInRegistry  

!********************************************************************************************************************************
    
    function getFieldComponentsInRegistry(fieldName) result(nComp)

        character(len=*) :: fieldName
    !------------------------------------------------------------------------------------------------------------------
 
        integer :: iField, nComp
    !------------------------------------------------------------------------------------------------------------------
   
        logical :: found
    !------------------------------------------------------------------------------------------------------------------    

        do iField=1,size(fieldRegistry)
            if(fieldRegistry(iField)%field%fieldName(1)=='fieldName') nComp = fieldRegistry(iField)%field%nComp  
        enddo    

        if(.not.found) call flubioMsg("Warning: field "//fieldName//" not found in the registry.")

    end function getFieldComponentsInRegistry 

!********************************************************************************************************************************
    
    function checkRegistry(fieldName) result(found)

        character(len=*) :: fieldName
    !------------------------------------------------------------------------------------------------------------------

        integer :: iField

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        found = .false.
        do iField=1,size(fieldRegistry)
            if(trim(fieldRegistry(iField)%field%fieldName(1))==fieldName) found = .true.
        enddo    

        if(.not. found) call flubioMsg("Warning: field "//fieldName//" not found in the registry.")

    end function checkRegistry 

end module fieldvar