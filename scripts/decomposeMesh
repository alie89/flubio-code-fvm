#!/bin/bash

case_folder=${PWD}
econn=${PWD}/grid/econn.msh
econn_graph=${PWD}/grid/econn.graph
PROGNAME=decomposeMesh

echo
echo "!-------------------------------------------------------------------------------------------------------------!"
echo "! WARNING: FLUBIO's mesh partitioner is still experimental and might have hidden issues or not work properly. !"
echo "!-------------------------------------------------------------------------------------------------------------!"
echo

usage()
{
  cat << EO
        Usage: $PROGNAME [options]

        Decompose the mesh in N partitions

        Options:
EO
  cat <<EO | column -s\& -t

        -h|--help & show usage
        -e|--etype & mesh type to feed metis (1 or 2 usually)
        -n|--np & number of processors to run with in parallel partitions (parmetis and kahip only)
        -p|--parallel & decompose the mesh graph in parallel using parmetis or parhip
        -s|--subdivisions & number of partitions
        -v|--variant & define the variant type (parmetis: 1 2 3 5 6, for kahip do parhip --help or kaffpa --help)
        -t|--tool & tool to use (metis/parmetis/kahip)
        -f|--format & format to save the mesh
EO
}

SHORTOPTS="e:s:n:t:f:v:hp"
LONGOPTS="help,subdivisions:,tool:,format:,np:,etype:,variant:,parallel"

ARGS=$(getopt -s bash --options $SHORTOPTS  \
  --longoptions $LONGOPTS --name $PROGNAME -- "$@" )

eval set -- "$ARGS"

while true; do
   case $1 in
      -h|--help)
         usage
         exit 0
         ;;
      -e| --etype)
         shift
         etype="$1"
         ;;
      -n| --np)
         shift
         np="$1"
         ;;
      -v| --variant)
         shift
         var="$1"
         ;;
      -p| --parallel)
         parallel=true
         ;;
      -s| --subdivisions)
         shift
         parts="$1"
         ;;
      -t| --tool)
         shift
         tool="$1"
         ;;
      -f| --format)
         shift
         format="$1"
         ;;
      --)
         shift
         break
         ;;
      *)
         shift
         break
         ;;
   esac
   shift
done

# Check
if [ -z "$tool" ]; then
  tool=metis
fi

if [ -z "$format" ]; then
  format=ascii
fi

if [ $parallel ] && [ -z "$np" ]; then
  echo "Error: please specify the number of processors to run with"
  exit 1
fi

if [ "$tool" == "metis" ] && [ -z "$var" ]; then
  var=1
fi

if [ "$tool" == "kahip" ] && [ -z "$var" ]; then
  if [ $parallel ]; then
    var="fastmesh"
  else
    var="fast"
  fi
fi

if [ -z "$etype" ];
then
  etype=1
fi

# Start processing

# Generate econn
echo "Decomposing mesh: $econn..."
flubio_createMetisMesh -etype $etype

if [ $parallel ]; then
  echo "converting mesh to graph..."
  m2gmetis grid/econn.msh  grid/econn.graph

  echo "partitioning graph..."

  if [ $tool == "kahip" ]; then
    mpirun -np $np parhip $econn_graph --k=$parts --preconfiguration=$var --save_partition
    mv tmppartition.txtp grid/econn.msh.epart.$parts
  else
    mpirun -np $np parmetis grid/econn.graph $var $parts 1 1 1 1
    mv grid/econn.graph.part grid/econn.msh.epart.$parts
  fi

else

  if [ $tool == "kahip" ]; then
      echo "converting mesh to graph"
      m2gmetis grid/econn.msh  grid/econn.graph

      echo "Partitioning mesh graph..."
      kaffpa $econn_graph --k=$parts --preconfiguration=$var
      mv tmppartition$parts grid/econn.msh.epart.$parts
  else
      mpmetis $econn $parts
  fi
fi

# write mesh files
echo $format
flubio_decomposeMesh -np $parts -format $format