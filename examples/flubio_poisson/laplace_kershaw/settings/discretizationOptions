!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

{

"Default":
{

!-----------------!
! Gradient method !
!-----------------!

    "gradient": "greenGauss",
    !"gradient": "leastSquares",

!-----------------------!
! Diffusion term method !
!-----------------------!

    !"diffusionTerm": "orthogonalCorrection",
    !"diffusionTerm": "minimumCorrection",
    "diffusionTerm": "overRelaxed",

!-------------!
! Time Scheme !
!-------------!

    "timeScheme": "Steady",

!This must be forced between 0 and 1.
!Very experimental - Do not use for the moment
    !"crossDiffusionLimiter":1,

},

"SteadyState": "yes",

!----------------------------!
! Non-orthogonal corrections !
!----------------------------!

"NonOrthogonalCorr": 2,

}
