!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

	program meshOperations

!==================================================================================================================
! Description:
!! Ttutorial on meshing operations
!==================================================================================================================

#include "petsc/finclude/petscsys.h"
		use petscsys
		use flubioMpi
		use meshvar

		implicit none

		integer :: ierr
		!! error flag
	!------------------------------------------------------------------------------------------------------------------

		!-------------------------!
		! Initialize parallel run !
		!-------------------------!

		call PetscInitialize(PETSC_NULL_CHARACTER,ierr)

			call flubioMsg('Hello, welcome to this tutorial!')

			call setMpiVar()

			call mesh%readMeshFromFiles()

		    call doSomethingWithTheMesh()

		! Finilize MPI processing
		call PetscFinalize(ierr)

	end program meshOperations

! *********************************************************************************************************************

	subroutine doSomethingWithTheMesh()

		use meshvar

		integer :: ierr, iElement, iFace, iPoint, iBoundary, is, ie
		!! error flag
	!------------------------------------------------------------------------------------------------------------------

		! Sizes
		write(*,*) 'number of points', numberOfPoints
		write(*,*) 'number of faces', numberOfFaces, numberOfIntFaces
		write(*,*) 'number of cells', numberOfElements

		write(*,*)
		write(*,*) '************************************************************************************************'
		write(*,*)

		! Loops
		do iFace=1,numberOfFaces
			write(*,*) 'face centroid: ', mesh%fcentroid(iFace,1:3)
			write(*,*) 'surface area: ', norm2(mesh%Sf(iFace,:))
			write(*,*) 'surface normal vector', mesh%Sf(iFace,:)/norm2(mesh%Sf(iFace,:))
			write(*,*) 'surface area vector', mesh%Sf(iFace,:)
			write(*,*) 'cell owner:', mesh%owner(iFace) !, mesh%neighbour(iFace)
		end do

		write(*,*)
		write(*,*) '************************************************************************************************'
		write(*,*)

		do iElement=1,numberOfElements
			write(*,*) 'cell center: ', mesh%centroid(1:3, iElement)
			write(*,*) 'volume:  ', mesh%volume(iElement)
		end do

		write(*,*)
		write(*,*) '************************************************************************************************'
		write(*,*)

		do iPoint=1,numberOfPoints
			write(*,*) 'point:', mesh%vertex(iPoint,:)
		end do

		write(*,*)
		write(*,*) '************************************************************************************************'
		write(*,*)

		! numberOfBoundaries
		! numberOfRealBound
		! numberOfProcBound

		do iBoundary=1,numberOfBoundaries
			write(*,*) 'boundary name: ', mesh%boundaries%userName(iBoundary)
			write(*,*) 'start face:', mesh%boundaries%startFace(iBoundary)
			write(*,*) 'number of boundary faces:', mesh%boundaries%nFace(iBoundary)
		end do

		do iBoundary=1,numberOfBoundaries

			is = mesh%boundaries%startFace(iBoundary)
			ie = is + mesh%boundaries%nFace(iBoundary)-1
			write(*,*) '*** iBoundary = ', iBoundary, '***'
			do iFace=is,ie
				write(*,*) mesh%owner(iFace)
				write(*,*) mesh%fcentroid(iFace,1:3)
			end do

		end do

	end subroutine doSomethingWithTheMesh