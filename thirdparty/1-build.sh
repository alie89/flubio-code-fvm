# 1-build.sh

# define env vars: home_dir_flubio and home_dir_thirdparty
source ../flubio_env.sh

./bash/build-qcontainers.sh
./bash/build-json.sh
./bash/build-vtkfortran.sh
./bash/build-penf.sh
./bash/build-vecfor.sh
./bash/build-fossil.sh
./bash/build-parmetis.sh
./bash/build-kahip.sh
./bash/build-amgcl.sh
./bash/build-ttb.sh
./bash/build-coretran.sh
../scripts/1-build.sh
