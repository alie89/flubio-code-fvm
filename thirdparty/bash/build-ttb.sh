
# build-ttb

unzip ./tarballs/ttb.zip 

mv ttb 0-ttb

cp bash/CMakeLists.txt-ttb 0-ttb/CMakeLists.txt

cd 0-ttb

rm -rf 0--build && mkdir 0--build 

cd 0--build

cmake  .. \
 -DCMAKE_INSTALL_PREFIX=${thirdparty_install_dir} \
 -DCMAKE_BUILD_TYPE=Release \

make 

mkdir -p        $thirdparty_install_dir/ttb/lib
cp libttb.a     $thirdparty_install_dir/ttb/lib
mkdir -p        $thirdparty_install_dir/ttb/mod
cp mod/*        $thirdparty_install_dir/ttb/mod
