
# build-coretran

unzip ./tarballs/coretran.zip 

mv coretran 0-coretran

#cp bash/CMakeLists.txt-coretran 0-coretran/CMakeLists.txt

cd 0-coretran

rm -rf 0--build && mkdir 0--build && cd 0--build

cmake  ../src \
 -DCMAKE_INSTALL_PREFIX=${thirdparty_install_dir} \
 -DCMAKE_BUILD_TYPE=RELEASE

make -j${njobs}
make install

mkdir -p                  $thirdparty_install_dir/coretran/lib
cp ../lib/libcoretran.so  $thirdparty_install_dir/coretran/lib
mkdir -p                  $thirdparty_install_dir/coretran/include
cp ../include/*           $thirdparty_install_dir/coretran/include
