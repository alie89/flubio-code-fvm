
# build-qcontainers

unzip ./tarballs/qcontainers.zip 

mv qcontainers 0-qcontainers

cp bash/CMakeLists.txt-qcontainers 0-qcontainers/CMakeLists.txt

cd 0-qcontainers

rm -rf 0--build && mkdir 0--build && cd 0--build

cmake  .. \
 -DCMAKE_INSTALL_PREFIX=${thirdparty_install_dir} \
 -DCMAKE_BUILD_TYPE=Release \

make -j${njobs}
make install
