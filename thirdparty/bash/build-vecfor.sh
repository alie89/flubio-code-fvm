
unzip ./tarballs/VecFor.zip 

mv VecFor 0-vecfor

cp bash/CMakeLists.txt-vecfor 0-vecfor/CMakeLists.txt

cd 0-vecfor

cmake -S . -B 0--build \
 -DCMAKE_INSTALL_PREFIX=${thirdparty_install_dir}/vecfor \
 -DCMAKE_BUILD_TYPE=Release \

cmake --build   0--build
#cmake --install 0--build

mkdir -p                 $thirdparty_install_dir/vecfor/lib
cp 0--build/libvecfor.a  $thirdparty_install_dir/vecfor/lib
mkdir -p                 $thirdparty_install_dir/vecfor/mod
cp 0--build/*.mod        $thirdparty_install_dir/vecfor/mod
