
# build-json

unzip ./tarballs/json-fortran.zip 

mv json-fortran 0-json

cp bash/CMakeLists.txt-json 0-json/CMakeLists.txt

cd 0-json

rm -rf 0--build && mkdir 0--build && cd 0--build

cmake  .. \
 -DCMAKE_INSTALL_PREFIX=${thirdparty_install_dir} \
 -DCMAKE_BUILD_TYPE=Release \
 -DSKIP_DOC_GEN=TRUE \

make -j${njobs}
make install
